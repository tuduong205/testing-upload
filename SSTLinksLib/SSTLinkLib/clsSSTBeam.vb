﻿Imports System.Globalization
Imports System.Windows.Forms
Imports System.Windows.Forms.VisualStyles
Imports Microsoft.VisualBasic.CompilerServices

Public Class clsSSTBeam
    Inherits clsSEFrame

    Public LKInfo As LinkInfo
    Public BRPInfo As BRPInfo
    Public Sheartab As clsSSTSheartab
    '
    ''Public endI_SMFSide As Integer = 1
    ''Public endJ_SMFSide As Integer = 1
    Public IsAsignEndI_K As Boolean = True
    Public IsAsignEndJ_K As Boolean = True
    Public K_rot As Double
    Public EndI_ColBottom As clsSEFrame
    Public EndJ_ColBottom As clsSEFrame
    Public EndI_ColTop As clsSEFrame
    Public EndJ_ColTop As clsSEFrame
    Public EndI_IsWeak As Boolean
    Public EndJ_IsWeak As Boolean
    Public NodeBelow_user As String
    'Step 1
    Public status_tbf As String
    Public status_bf As String
    Public status_Lyield As String
    Public status_endIPz As String
    Public status_endJPz As String
    Public status_ClipWasher As String
    'Step 2
    Public status_linkDCR As String
    Public status_L_stem_DCR As String
    Public status_bmFlgThk As String
    '
    Public status_brptDCR As String
    Public status_brpBolts As String
    Public status_SeismicDrift As String
    Public status_WindDrift As String
    '
    Public status_b_t As String
    Public status_h_w As String
    '
    Public Mu_max As Double  'Max Mdesign face of column
    Public Mu_Link_max As Double  'Max M for link strength
    Public Pu_bm As Double  'combine shear of upper and lower column Vu_col
    Public V_bmGravity As Double
    '
    ''Public DCR_limit As Double = 1.03
    ''Private DCRLimit1_03 As Double = 1.03
    'step 5
    Public aSeismicDriftLimit As Double ' aLimit
    Public aWindDriftLimit As Double ' aLimit
    Public NoConn As Integer
    Public HeightDrift As Double ' height for drift check
    Public Height_user As Double = -1 ' user input height for drift check

    Sub New()
    End Sub

    Sub New(ByRef curJob As clsMRSJob, uniqueName$, bmSize$, iPt$, jPt$, ST As Integer, boltSize As Integer, boltGrade$, horizBolt As Integer,
            Lcc#, _sX#, _sY#, lk_Left$, lk_Right$, _ElevID$, _styID$, _LevelHeight#)
        If curJob Is Nothing Then
            Dim Arr() As String
            curJob = New clsMRSJob(Arr)
        End If

        Wsec = curJob.AllWsections.Wsections.Find(Function(x) x.Size.Equals(bmSize$, StringComparison.CurrentCultureIgnoreCase))

        _uName = uniqueName
        Pt_I = iPt
        Pt_J = jPt
        Len_LCC = Lcc
        sPt_X = _sX
        sPt_Y = _sY
        ElevID = _ElevID
        StyID = _styID
        LevelHeight = _LevelHeight
        Dim tmpLk_L = curJob.SSTLinks.Links.Find(Function(x) x.Size.Equals(lk_Left, StringComparison.CurrentCultureIgnoreCase))
        If tmpLk_L Is Nothing Then
            tmpLk_L = curJob.SSTLinks.Links(0)
            IsAsignEndI_K = False
        Else
            IsAsignEndI_K = (tmpLk_L.Size <> "Pinned")
            LKInfo = tmpLk_L
        End If
        '
        Dim tmpLk_R = curJob.SSTLinks.Links.Find(Function(x) x.Size.Equals(lk_Right, StringComparison.CurrentCultureIgnoreCase))
        If tmpLk_R Is Nothing Then
            tmpLk_R = curJob.SSTLinks.Links(0)
            IsAsignEndJ_K = False
        Else
            IsAsignEndJ_K = (tmpLk_R.Size <> "Pinned")
            If tmpLk_R.Size <> "Pinned" Then
                LKInfo = tmpLk_R
            End If
        End If
        '
        Dim stBolt As SheartabBolt = FindShearTabBolt(Wsec.Size, curJob, Wsec.Depth) '
        Dim btyp As bType
        If boltGrade = "A325X" Or boltGrade = "A325-X" Then
            btyp = bType.A325X
        ElseIf boltGrade = "A490N" Or boltGrade = "A490-N" Then
            btyp = bType.A490N
        ElseIf boltGrade = "A490X" Or boltGrade = "A490-X" Then
            btyp = bType.A490X
        Else
            btyp = bType.A325N
        End If
        Sheartab = New clsSSTSheartab(Wsec, LKInfo, stBolt, horizBolt, boltSize, btyp, Double2eThickness(ST / 8), eThickness.T1_4, 0, 0)
    End Sub

    Sub New(_StyID$, _ElevID$, _GridID_LeftSide$, _GridID_RightSide$, in_uName As String, _Len#, _Bmsec As Wsec, _iPoint As String, _jPoint As String, _Link As LinkInfo, _BRPInfo As BRPInfo,
            _EndI_ColBottom As clsSEFrame, _EndJ_ColBottom As clsSEFrame, _EndI_ColTop As clsSEFrame, _EndJ_ColTop As clsSEFrame,
            _IsEndI_K As Boolean, _IsEndJ_K As Boolean, _LevelH As Double, _NoConn As Integer, _StpX#, _StpY#, _EndI_IsWeak As Boolean, _EndJ_IsWeak As Boolean, _angle As Double)
        StyID = _StyID
        ElevID = _ElevID
        GridID_LeftSide = _GridID_LeftSide
        GridID_RightSide = _GridID_RightSide
        _uName = in_uName
        Len_LCC = _Len
        Wsec = _Bmsec
        Pt_I = _iPoint
        Pt_J = _jPoint
        LKInfo = _Link
        BRPInfo = _BRPInfo
        LevelHeight = _LevelH
        NoConn = _NoConn
        sPt_X = _StpX
        sPt_Y = _StpY
        '
        EndI_ColBottom = _EndI_ColBottom
        Dim dist_I, dist_J As Double
        If Not EndI_ColBottom Is Nothing Then
            dist_I = (EndI_ColBottom.Wsec.Depth / 2 + IIf(IsAsignEndI_K, LKInfo.a, 0))
        End If
        '
        EndJ_ColBottom = _EndJ_ColBottom
        If Not EndJ_ColBottom Is Nothing Then
            dist_J = (EndJ_ColBottom.Wsec.Depth / 2 + IIf(IsAsignEndJ_K, LKInfo.a, 0))
        End If '
        Len_Cal = Len_LCC - dist_I - dist_J
        '
        EndI_ColTop = _EndI_ColTop
        EndJ_ColTop = _EndJ_ColTop
        '
        IsAsignEndI_K = _IsEndI_K
        IsAsignEndJ_K = _IsEndJ_K
        EndI_IsWeak = _EndI_IsWeak
        EndJ_IsWeak = _EndJ_IsWeak
        Angle = _angle
    End Sub

    Sub UpdateBeamData(_StyID$, _ElevID$, _GridID_LeftSide$, _GridID_RightSide$, in_uName As String, _Len#, _Bmsec As Wsec, _iPoint As String, _jPoint As String, _Link As LinkInfo, _BRPInfo As BRPInfo,
            _EndI_ColBottom As clsSEFrame, _EndJ_ColBottom As clsSEFrame, _EndI_ColTop As clsSEFrame, _EndJ_ColTop As clsSEFrame,
              _IsEndI_K As Boolean, _IsEndJ_K As Boolean, _LevelH As Double, _NoConn As Integer, _StpX#, _StpY#, _EndI_IsWeak As Boolean, _EndJ_IsWeak As Boolean)
        StyID = _StyID
        ElevID = _ElevID
        GridID_LeftSide = _GridID_LeftSide
        GridID_RightSide = _GridID_RightSide
        _uName = in_uName
        Len_LCC = _Len
        Wsec = _Bmsec
        Pt_I = _iPoint
        Pt_J = _jPoint
        LKInfo = _Link
        BRPInfo = _BRPInfo
        LevelHeight = _LevelH
        NoConn = _NoConn
        sPt_X = _StpX
        sPt_Y = _StpY
        '
        EndI_ColBottom = _EndI_ColBottom
        Dim dist_I, dist_J As Double
        If Not EndI_ColBottom Is Nothing Then
            dist_I = (EndI_ColBottom.Wsec.Depth / 2 + IIf(IsAsignEndI_K, LKInfo.a, 0))
        End If
        '
        EndJ_ColBottom = _EndJ_ColBottom
        If Not EndJ_ColBottom Is Nothing Then
            dist_J = (EndJ_ColBottom.Wsec.Depth / 2 + IIf(IsAsignEndJ_K, LKInfo.a, 0))
        End If '
        Len_Cal = Len_LCC - dist_I - dist_J
        '
        EndI_ColTop = _EndI_ColTop
        EndJ_ColTop = _EndJ_ColTop
        '
        IsAsignEndI_K = _IsEndI_K
        IsAsignEndJ_K = _IsEndJ_K
        EndI_IsWeak = _EndI_IsWeak
        EndJ_IsWeak = _EndJ_IsWeak
    End Sub

    Public Sub Step1_InitialDesign(ByRef curJob As clsMRSJob)
        Try
            K_rot = 0
            'Cal K_rot
            If LKInfo.Size <> "Pinned" Then
                ''Dim AF11 = LKInfo.Py_link   / LKInfo.Keff
                ''Dim AE11 As Double = LKInfo.Py_link   * (LKInfo.tstem + Wsec.Depth) 'Py_link or Pr_link?
                ''Dim AG11 As DoublLKInfoe = AF11 / ((LKInfo.tstem + Wsec.Depth) / 2)
                K_rot = Cal_Krot(LKInfo, Wsec.Depth)
            End If
            '
            ILS_Detail(curJob)
            ' ''Check Panelzone
            'status_S1 = IIf(status_tbf = "OK" AndAlso status_bf = "OK" AndAlso status_Lyield = "OK", "OK", "NG")
        Catch ex As Exception
        End Try
    End Sub

    Public Sub Step2_BeamLinkCheck(ByRef curJob As clsMRSJob)
        BLC_Detail(curJob)
        ILS_Detail(curJob)
        ''status_S2 = IIf(status_linkDCR = "OK" AndAlso status_L_stem_DCR = "OK" AndAlso status_bmFlgThk = "OK" AndAlso status_brptDCR = "OK" AndAlso status_brpBolts = "OK", "OK", "NG")
    End Sub

    Public Sub ILS_Detail(ByRef curJob As clsMRSJob)
        If IsAsignEndI_K = False AndAlso IsAsignEndJ_K = False Then
            status_tbf = NA
            status_bf = NA
            status_Lyield = NA
            status_ClipWasher = NA
            Exit Sub
        End If

        status_tbf = NumberFormat(0.4 / Wsec.tf, 3) ' IIf(Wsec.tf >= 0.4, "OK", "NG")
        Dim K1 As Double = 0.75 * 192 * gES * (LKInfo.Wcol_side * LKInfo.t_flange ^ 3 / 12) / LKInfo.g_flange ^ 3
        Dim K2 As Double = LKInfo.tstem * LKInfo.Wcol_side * gES / (LKInfo.Lcol_side + LKInfo.sc + IIf(LKInfo.n_bolt > 4, LKInfo.s_stem / 2, 0))
        Dim K3 As Double = LKInfo.tstem * LKInfo.b_yield * gES / LKInfo.Ly_yield
        Dim Pye_Link As Double = 1.1 * LKInfo.Py_link
        Dim Mye_link As Double = Pye_Link * (Wsec.Depth + LKInfo.tstem)
        Dim Dy As Double = Pye_Link / LKInfo.Keff
        Dim Qy As Double = Dy / ((Wsec.Depth + LKInfo.tstem) / 2)

        Dim tmpVal As Double
        Dim W_k1 = Math.Max(EndI_ColBottom.Wsec.k1, EndJ_ColBottom.Wsec.k1)
        Dim B_washer = Find_WasherInfo(LKInfo.boltDia_Flange, True)
        Dim E_washer = Find_WasherInfo(LKInfo.boltDia_Flange, False)
        Dim ColGageReq = (2 * W_k1 + B_washer)
        Dim ColGageReq_washer = 2 * E_washer + 2 * W_k1

        tmpVal = MaxAll(tmpVal, LKInfo.bbf_min / Wsec.bf, LKInfo.bcf_min / EndI_ColBottom.Wsec.bf, LKInfo.bcf_min / EndJ_ColBottom.Wsec.bf) ', ColGageReq / LKInfo.S_flange)
        'check w/o clip washer
        Dim ck1 As String = If((LKInfo.S_flange + 0.5) < ColGageReq, "NG", "OK")
        'check w clip washer
        Dim ck2 As String = If((LKInfo.S_flange + 0.5) < ColGageReq_washer, "NG", "OK")
        '
        If tmpVal <= curJob.DCRLimits.bf_check Then
            If ck1 = "OK" Then
                status_ClipWasher = "Not Required"
                status_bf = "OK"
            Else
                If ck2 = "OK" Then
                    status_ClipWasher = "Required"
                    status_bf = "OK"
                Else
                    status_ClipWasher = "NG"
                    status_bf = "NG"
                End If
            End If
        Else
            status_ClipWasher = NA
            status_bf = "NG"
        End If

        '
        Dim C54 As Double = (Wsec.Depth + LKInfo.tstem) / 2
        Dim C56 As Double = 0.05 * C54 / 0.085 + 2 * LKInfo.tstem
        Dim DCR As Double = C56 / LKInfo.Ly_yield
        status_L_stem_DCR = IIf(DCR > curJob.DCRLimits.Lyield_check, Format(DCR, "0.###"), "OK")
        status_Lyield = NumberFormat(DCR, 3) ' IIf(DCR <= 1.03, "OK", "NG")

        If Not curJob.stlData Is Nothing Then
            curJob.stlData.Clear()
            Dim cnt As Long
            Dim prefix As String = "lb"
            '1.1 CURRENT MEMBER:
            cnt = 0 : prefix = "S11_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), _uName)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), Wsec.Size)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Size)
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), EndI_ColBottom.Wsec.Size)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), EndI_ColBottom._uName)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IsAsignEndI_K)

            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), EndJ_ColBottom.Wsec.Size)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), EndJ_ColBottom._uName)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IsAsignEndJ_K)
            '1.2 LINK STEM GEOMETRY:
            cnt = 0 : prefix = "S12_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Lcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Ly_yield)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Lbm_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Lcol_side + LKInfo.Ly_yield + LKInfo.Lbm_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.tstem)
            '                                                                                                           
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.tstem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Wcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.b_yield)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Wcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), FormatNumber(LKInfo.tstem * LKInfo.b_yield, 2))
            '1.3 LINK STEM BOLTS:     
            cnt = 0 : prefix = "S13_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.n_bolt)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "A490")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.boltDia_stem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), Ceiling(Wsec.tf + LKInfo.tstem + 1 + 0.15625 + 0.375, 1 / 8))
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.g_stem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.s_stem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.sc)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.sb)
            '1.4 LINK FLANGE GEOMETRY:
            cnt = 0 : prefix = "S14_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.t_flange)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Wcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.h_flange)
            '1.5 LINK FLANGE BOLTS:
            cnt = 0 : prefix = "S15_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "4")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "A325")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.boltDia_Flange)
            '
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.g_flange)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.S_flange)
            '1.6 LINK MATERIAL:
            cnt = 0 : prefix = "S16_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), g50)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), g65)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "1.2")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "1.1")
            '1.7 LINK K_ROT:
            cnt = 0 : prefix = "S17_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Pye_Link, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(K1, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(K2, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(K3, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(LKInfo.Keff, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Mye_link, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Dy, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Qy, 6))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Mye_link / Qy, 0))
            '1.8 BEAM FLANGE THICKNESS CHECK:
            cnt = 0 : prefix = "S18_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), Wsec.tf)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(0.4 / Wsec.tf, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(Wsec.tf >= 0.4, "OK", "NG"))
            '1.9 BEAM & COLUMN FLANGE WIDTH CHECK
            cnt = 0 : prefix = "S19_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(LKInfo.bbf_min, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), Wsec.bf)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), EndI_ColBottom.Wsec.bf)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), EndJ_ColBottom.Wsec.bf)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(W_k1, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), B_washer)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E_washer)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(ColGageReq, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.S_flange)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(tmpVal, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(ColGageReq_washer, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), status_ClipWasher)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), status_bf)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(LKInfo.bcf_min, 2))
            '1.10 LINK STEM YIELDING LENGTH CHECK:
            cnt = 0 : prefix = "S110_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "0.05")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "8.50%")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C54, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(0.05 * C54, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C56, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(DCR, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(DCR <= curJob.DCRLimits.Lyield_check, "OK", "NG"))
            'DCR Summary
            cnt = 0 : prefix = "SUM_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(0.4 / Wsec.tf, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(tmpVal, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(DCR, 3))
            cnt = cnt + 2
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.tbf_check, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.bf_check, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Lyield_check, 2))
            cnt = cnt + 2
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(Wsec.tf >= 0.4, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(tmpVal <= curJob.DCRLimits.bf_check, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(DCR <= curJob.DCRLimits.Lyield_check, "OK", "NG"))

            Dim aa = curJob.stlData.Keys
        End If

    End Sub

    Public Sub BLC_Detail(ByRef curJob As clsMRSJob)

        Dim R_use = If(Angle > 45 And Angle < 135, curJob.RY, curJob.R)

        '2.1 CURRENT MEMBER:
        Dim F3 As Double = Mu_max

        Dim ckVal = Wsec.bf_2tf
        Dim lamda_p = 0.38 * Math.Sqrt(29000 / g50)
        Dim lamda_r = 1 * Math.Sqrt(29000 / g50)
        status_b_t = IIf(ckVal <= lamda_p, "Compact", IIf(ckVal >= lamda_r, "Slender", "Non-Compact"))
        '
        ckVal = Wsec.h_tw
        lamda_p = 3.76 * Math.Sqrt(29000 / g50)
        lamda_r = 5.7 * Math.Sqrt(29000 / g50)
        status_h_w = IIf(ckVal <= lamda_p, "Compact", IIf(ckVal >= lamda_r, "Slender", "Non-Compact"))

        '2.2 LINK STEM GEOMETRY:
        Dim I5 As Double = LKInfo.tstem
        Dim C6 As Double = LKInfo.Ly_yield
        Dim I7 As Double = LKInfo.b_yield
        Dim C9 As Double = LKInfo.tstem
        '2.3 LINK STEM BOLTS:
        '2.4 LINK FLANGE GEOMETRY:
        '2.6 LINK MATERIAL:
        Dim C27 As Double = g50
        Dim I29 As Double = 1.1
        '2.7 LINK CHECK:
        Dim C32 As Double = 0.05
        Dim C33 As Double = 0.08
        Dim C34 As Double = (Wsec.Depth + LKInfo.tstem) / 2
        Dim C35 As Double = C34 * C32
        Dim C36 As Double = C32 * C34 / 0.085 + 2 * C9
        Dim C37 As Double = C6
        Dim C38 As Double = C36 / C37
        Dim C39 As String = IIf(C38 < curJob.DCRLimits.Link_strength_DCR, "OK", "NG")

        Dim C41 As Double = F3
        Dim C42 As Double = Wsec.Depth
        Dim C43 As Double = C42 + I5
        Dim C44 As Double = C41 / C43
        Dim C45 As Double = LKInfo.Py_link
        Dim C46 As Double = 0.9 * C45
        Dim C47 As Double = C44 / (0.9 * C45)
        Dim C48 As String = IIf(C47 <= curJob.DCRLimits.Link_strength_DCR, "OK", "NG")
        status_linkDCR = NumberFormat(C47, 3)
        '2.8 BUCKLING RESTRAINT PLATE AND SPACER CHECK:
        Dim C51 As Double = BRPInfo.D_brp - 1 / 16
        Dim C52 As Double = BRPInfo.n_brp / 2
        Dim C56 As Double = BRPInfo.L_brp - BRPInfo.L_be - BRPInfo.S_brp
        '2.9 BRP THICKNESS CHECK:
        Dim C64 As Double = IIf(R_use <= 3, LKInfo.Py_link, LKInfo.Pr_link)
        Dim C65 As Double = 0.05 * (LKInfo.tstem + Wsec.Depth) / 2
        Dim C66 As Double = LKInfo.Ly_yield / 2 - BRPInfo.S_brp / 2
        Dim C67 As Double = C66 + C65 - (BRPInfo.D_brp) / 2
        Dim C68 As Double = BRPInfo.W_brp - 2 * (C51 + 1 / 8)
        Dim C69 As Double = 0.51 * Math.Sqrt(C67 * C64 / (C27 * I29 * C68))
        Dim C70 As Double = BRPInfo.t_brp
        Dim C71 As Double = C69 / C70
        Dim C72 As String = IIf(C71 <= curJob.DCRLimits.t_BRP_DCR, "OK", "NG")
        status_brptDCR = NumberFormat(C71, 3) ' IIf(C53 > DCR_limit, Format(C53, "0.###"), "OK")
        '2.10 BEAM FLANGE THICKNESS CHECK:
        Dim C75 As Double = 0.04
        Dim C76 As Double = C75 * (Wsec.Depth + I5) / 2 / (C6 - 2 * C9)
        Dim C77 As Double = 0.25 * C76 * I5
        Dim C78 As Double = I7 * I5 ^ 3 / 12
        Dim C79 As Double
        C79 = Math.Sqrt((1900 * C78 / C64) * (1 + (I7 / (2 * C77) + 1.013) ^ -1))
        Dim C80 As Double = 4 * C77 * C64 / C79
        Dim C81 As Double = C6 / C79
        Dim C82 As Double = Floor(C81, 1)
        Dim C83 As Double = Ceiling(C82 / 2, 1)
        Dim C84 As Double = C83 * C80
        Dim C85 As Double = C84 / (C52 * 2)
        Dim C86 As Double = 1.0
        Dim C87 As Double = C56
        Dim C88 As Double = I7 + 2 * (0.125 + C51)
        Dim C89 As Double = BRPInfo.g_brp
        Dim C90 As Double = (C89 - Wsec.tw) / 2
        Dim C91 As Double = (Wsec.bf - Wsec.tw) / 2
        Dim C92 As Double = C91 - C90
        Dim C93 As Double = Math.Sqrt(C90 * C91)
        Dim C94 As Double = 4 * C93
        Dim C95 As Double = 2 * C93
        Dim C96 As Double = BRPInfo.S_brp
        Dim F95 As Double = IIf(C93 < C87, C94, C95 + C87)
        Dim I95 As Double = IIf(C93 <= C87, C95 + C96 / 2, C87 + C96 / 2)
        Dim I96 As Double = IIf(C93 > 2 * C96, C94, C95 + C96 / 2)
        Dim I97 As Double = Math.Min(I95, I96)
        Dim I99 As Double = I95
        Dim I100 As Double = If(C96 < C93, C96 / 2, C95) + If(C96 < C93, C93 / 2, C95)
        Dim I101 As Double = I97
        Dim I102 As Double = (I95 + I100 + I101) / 3
        '
        Dim C97 As Double = If(C52 = 1, F95, If(C52 = 2, I97, I102))
        Dim C98 As Double = C89 / 2 - Wsec.tw / 2 - C51 / 2
        Dim C99 As Double = Math.Max(Math.Sqrt(4 * C98 * C85 / (C86 * C97 * g65)), 0.4)
        Dim C100 As Double = Wsec.tf
        Dim C101 As Double = C99 / C100

        '2.11 BRP BOLT SIZE AND QUANTITY CHECK:
        Dim C105 As Double = I5 * (I7 ^ 3) / 12
        Dim clearGap As Double = 0.125
        Dim C106 As Double = 0.5 * C64 / Math.Sqrt((1900 * C105 / C64) * (1 + (4 * I5 / (2 * clearGap) + 1.013) ^ -1))
        Dim C107 As Double = C106 / (C52 * 1.0)
        Dim C110 As Double = C85
        Dim D110 As Double = 0.3 * C110
        Dim E110 As Double = 0
        Dim F110 As Double = C107
        Dim C112 As String = "A325"
        Dim C113 As String = "X"
        Dim C114 As Double = 0.75
        Dim C115 As Double = g68
        Dim C116 As Double = Math.PI * (C51 / 2) ^ 2
        Dim C117 As Double = C116 * C115 * C114
        Dim C118 As Double = g90
        Dim C119 As Double = C114 * C118 * C116
        Dim C123 As Double = C110 / C119
        Dim C124 As Double = D110 / C117
        Dim C125 As Double = Math.Min(C117, (1.3 - (Math.Abs(C110 / C119))) * C117)
        Dim C126 As Double = D110 / C125
        Dim D123 As Double = E110 / C119
        Dim D124 As Double = F110 / C117
        Dim D125 As Double = Math.Min(C117, (1.3 - (Math.Abs(E110 / C119))) * C117)
        Dim D126 As Double = F110 / D125
        Dim C128 As Double = Math.Max(C126, D126)
        Dim C129 As String = IIf(C128 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        status_brpBolts = NumberFormat(C128, 3) ' IIf(C108 > DCR_limit, Format(C108, "0.###"), "OK")
        '2.12 LINK STEM HOLE/BRP HOLE FIT ON BEAM FLANGE
        Dim C131 As Double = Wsec.bf
        Dim C132 As Double = BRPInfo.bbf_BRP
        Dim C133 As Double = BRPInfo.bbf_stem
        Dim C134 As Double = Math.Max(C132, C133)
        Dim C135 As Double = C134 / C131
        Dim C136 As String = IIf(C135 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        '2.13 BRP BOLT BEAM FLANGE BEARING AND TEAROUT IN WEAK-AXIS DIRECTION
        Dim C138 As Double = 0.75
        Dim C139 As Double = Wsec.bf
        Dim C140 As Double = BRPInfo.g_brp
        Dim C141 As Double = BRPInfo.D_brp - 1 / 16
        Dim C142 As Double = (C139 - C140) / 2
        Dim C143 As Double = BRPInfo.BRP_bolt_edge2
        Dim C144 As String = IIf(C142 > C141, "OK", "NG")
        Dim C145 As String = IIf(C142 > C143, "OK", "NG, Check bearing and tear out")
        Dim C146 As Double = F110
        Dim C148 As Double = 2.4 * C141 * C100 * g65
        Dim C149 As Double = C138 * C148
        Dim C150 As Double = C146 / C149
        Dim C151 As String = IIf(C150 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        Dim C152 As Double = C142
        Dim C153 As Double = 1.2 * C152 * g65 * C100
        Dim C154 As Double = C153 * C138
        Dim C155 As Double = C146 / C154
        Dim C156 As String = IIf(C155 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        '2.14 BRP BOLT BEAM FLANGE BLOCK SHEAR IN WEAK-AXIS DIRECTION
        Dim C158 As Double = 0.75
        Dim C159 As Double = BRPInfo.S_brp
        Dim C160 As Double = BRPInfo.D_brp
        Dim C161 As Double = C142
        Dim C162 As Double = C100
        Dim C163 As Double = C106
        Dim C165 As Double = 2 * C161
        Dim C166 As Double = C165 * C162
        Dim C167 As Double = 0.6 * C166 * g50
        Dim C168 As Double = C167 * C158
        Dim C169 As Double = C163 / C168
        Dim C170 As String = IIf(C169 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")

        Dim C172 As Double = C165 - C160
        Dim C173 As Double = C172 * C162
        Dim C174 As Double = 0.6 * C173 * g65
        Dim C175 As Double = C174 * C158
        Dim C176 As Double = C163 / C175
        Dim C177 As String = IIf(C176 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")

        Dim G169 As Double = IIf(Left(LKInfo.Size, 3) = "YL4", 0, C159 - C160)
        Dim G170 As Double = G169 * C162
        Dim G173 As Double = 0.6 * g65 * C173 + 1 * g65 * G170
        Dim G174 As Double = 0.6 * g50 * C166 + 1 * g65 * G170
        Dim G175 As Double = Math.Min(G173, G174)
        Dim G178 As Double = 0.75 * G175
        Dim G176 As Double = C163 / G178
        Dim G177 As String = IIf(G176 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        '2.15 BEAM FLANGE CHECK FOR LINK STEM-TO-BEAM FLANGE BOLT BEARING AND TEAROUT
        Dim C179 As Double = C64
        Dim C180 As Double = C162
        Dim C181 As Double = LKInfo.boltDia_stem
        Dim C182 As Double = LKInfo.n_bolt
        Dim C183 As Double = LKInfo.s_stem
        Dim C184 As Double = C183 - C181 - 1 / 16
        Dim C185 As Double = g50
        Dim C186 As Double = g65
        Dim C187 As Double = 2.4 * C181 * C180 * C186 * C182
        Dim C188 As Double = C187 * C158
        Dim C189 As Double = C179 / C188
        Dim C190 As String = IIf(C189 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        Dim C191 As Double = 1.2 * C184 * C180 * C186 * C182
        Dim C192 As Double = C191 * C158
        Dim C193 As Double = C179 / C192
        Dim C194 As String = IIf(C193 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        '2.16 LINK STEM-TO-BEAM FLANGE BOLT BLOCK SHEAR CHECK
        Dim C197 As Double = BRPInfo.L1_brp
        Dim C198 As Double = BRPInfo.S_brp
        Dim C199 As Double = BRPInfo.L2_brp
        Dim C200 As Double = LKInfo.s_stem
        Dim C201 As Double = C182
        Dim C202 As Double = LKInfo.g_stem
        Dim C203 As Double = BRPInfo.g_brp
        Dim C204 As Double = BRPInfo.D_brp
        Dim C205 As Double = LKInfo.boltDia_stem + 1 / 16
        Dim C206 As Double = C139
        Dim C207 As Double = (C206 - C202) / 2
        Dim C208 As Double = BRPInfo.Stem_bolt_edge
        Dim C209 As String = IIf(C207 > C205, "OK", "NG")
        Dim C210 As String = IIf(C207 >= C208, "OK", "NG, Check blockshear")
        Dim C212 As Double = C197 + C203 + C199 + C200 * (C201 / 2 - 1)
        Dim C213 As Double = C212 * C180
        Dim C214 As Double = 0.6 * C213 * 2 * C185
        Dim C215 As Double = 0.75 * C214
        Dim C216 As Double = C179 / C215
        Dim C217 As String = IIf(C216 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")
        Dim C218 As Double = C212 - (C201 / 2 - 0.5) * (C205) - C52 * (C204)
        Dim C219 As Double = C218 * C180
        Dim C220 As Double = 0.6 * C219 * C186
        Dim C221 As Double = 0.75 * C220
        Dim C222 As Double = C179 / C221
        Dim C223 As String = IIf(C222 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")

        Dim G212 As Double = (C207 - 0.5 * C205) * 2
        Dim G213 As Double = G212 * C162
        Dim G216 As Double = 0.6 * g65 * C219 + 1 * g65 * G213
        Dim G217 As Double = 0.6 * g50 * C213 + 1 * g65 * G213
        Dim G218 As Double = Math.Min(G216, G217)
        Dim G221 As Double = 0.75 * G218
        Dim G219 As Double = C179 / G221
        Dim G220 As String = IIf(G219 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG")

        '2.17 BEAM FLANGE NET SECTION CHECK
        Dim C226 As Double = Wsec.Z33
        Dim C227 As Double = 2 * C205 * Wsec.tf * (Wsec.Depth - Wsec.tf)
        Dim C228 As Double = C226 - C227
        Dim C229 As Double = C228 * g50 * 1.1
        Dim C230 As Double = LKInfo.Pr_link
        Dim C231 As Double = Wsec.Depth + LKInfo.tstem
        Dim C232 As Double = C230 * C231
        Dim C233 As Double = C232 / C229

        'DCR summary
        Dim F228 As Double = MaxAll(C101, C155, C150, C169, C176, G176, C189, C193, C216, C222, G219, C233)
        Dim F229 As Double = C47
        Dim F230 As Double = C38
        Dim F231 As Double = C71
        Dim F232 As Double = C128
        Dim F233 As String = C144
        Dim F234 As String = C209
        status_bmFlgThk = NumberFormat(F228, 3) 'IIf(C82 > DCR_limit, Format(C82, "0.###"), "OK")
        '
        ''DCR summary
        'Dim E135 As Double = C101
        'Dim E136 As Double = C47
        'Dim E137 As Double = C38
        'Dim E138 As Double = C71
        'Dim E139 As Double = C128

        If Not curJob.stlData Is Nothing Then
            curJob.stlData.Clear()
            Dim cnt As Long
            Dim prefix As String = "lb"
            '2.1 CURRENT MEMBER:
            cnt = 0 : prefix = "S21_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), _uName)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), Wsec.Size)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Size)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Mu_max, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Wsec.bf_2tf, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Wsec.h_tw, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), status_b_t)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), status_h_w)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), R_use)
            '2.2 LINK STEM GEOMETRY:
            cnt = 0 : prefix = "S22_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Lcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C6)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Lbm_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Lcol_side + LKInfo.Ly_yield + LKInfo.Lbm_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C9)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I5)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Wcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I7)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Wcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), FormatNumber(LKInfo.tstem * LKInfo.b_yield, 2))
            '2.3 LINK STEM BOLTS:
            cnt = 0 : prefix = "S23_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.n_bolt)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "A490")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.boltDia_stem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), Ceiling(Wsec.tf + LKInfo.tstem + 1 + 0.15625 + 0.375, 1 / 8))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.g_stem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.s_stem)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.sc)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.sb)
            '2.4 LINK FLANGE GEOMETRY:
            cnt = 0 : prefix = "S24_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.t_flange)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.Wcol_side)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.h_flange)
            '2.5 LINK FLANGE BOLTS:
            cnt = 0 : prefix = "S25_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "4")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "A325")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.boltDia_Flange)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.g_flange)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), LKInfo.S_flange)
            '2.6 LINK MATERIAL:
            cnt = 0 : prefix = "S26_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C27)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "65")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "1.2")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I29)
            '2.7 LINK CHECK:
            cnt = 0 : prefix = "S27_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C32, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C33, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C34, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C35, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C36, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C37, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C38, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C39)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C41, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C42, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C43, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C44, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C45, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C46, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C47, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C48)
            '2.8 BUCKLING RESTRAINT PLATE AND SPACER CHECK:
            cnt = 0 : prefix = "S28_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C51, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C52, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), BRPInfo.S_brp)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), BRPInfo.t_brp)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), BRPInfo.L_be)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C56, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), BRPInfo.L_brp)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), BRPInfo.W_brp)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "50.00")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "1.10")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "65.00")
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "1.20")
            '2.9 BRP THICKNESS CHECK:
            cnt = 0 : prefix = "S29_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C64, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C65, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C66, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C67, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C68, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C69, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C70, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C69 / C70, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C72)
            '2.10 BEAM FLANGE THICKNESS CHECK:
            cnt = 0 : prefix = "S210_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C75, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C76, 5))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C77, 5))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C78, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C79, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C80, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C81, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C82, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C83, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C84, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C85, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C86, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C87, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C88, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C89, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C90, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C91, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C92, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C93, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C94, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C95, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C96, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C97, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C98, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C99, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C100, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C101, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(C101 <= curJob.DCRLimits.Beam_tf_DCR, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F95, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I95, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I96, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I97, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I99, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I100, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I101, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(I102, 3))
            '2.11 BRP BOLT SIZE AND QUANTITY CHECK:
            cnt = 0 : prefix = "S211_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C105, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C106, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C107, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C110, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C112)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C113)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C114)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C115)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C116, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C117, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C118)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C119, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C123, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C124, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C125, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C126, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C128, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C129)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D110, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D123, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D124, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D125, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(D126, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(E110, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F110, 3))

            '2.12 LINK STEM HOLE/BRP HOLE FIT ON BEAM FLANGE
            cnt = 0 : prefix = "S212_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C131, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C132, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C133, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C134, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C135, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C136)
            '2.13 BRP BOLT BEAM FLANGE BEARING AND TEAROUT IN WEAK-AXIS DIRECTION
            cnt = 0 : prefix = "S213_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C138, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C139)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C140, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C141, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C142, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C143, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C144)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C145)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C146, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C148)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C149, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C150, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C151)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C152, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C153, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C154, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C155, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C156)
            '2.14 BRP BOLT BEAM FLANGE BLOCK SHEAR IN WEAK-AXIS DIRECTION
            cnt = 0 : prefix = "S214_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C158, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C159, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C160, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C161, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C162, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C163, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C165, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C166, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C167, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C168, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C169, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G169, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G170, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C172, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C173, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C174, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C175, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C176, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G173, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G174, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G175, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G178, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C170)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C177)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G177)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G176, 3))
            '2.15 BEAM FLANGE CHECK FOR LINK STEM-TO-BEAM FLANGE BOLT BEARING AND TEAROUT
            cnt = 0 : prefix = "S215_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C179, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C180, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C181, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C182, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C183, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C184, 4))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C185, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C186, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C187, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C188, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C189, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C191, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C192, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C193, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C190)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C194)
            '2.16 LINK STEM-TO-BEAM FLANGE BOLT BLOCK SHEAR CHECK
            cnt = 0 : prefix = "S216_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C197, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C198, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C199, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C200, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C201, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C202, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C203, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C204, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C205, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C206, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C207, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C208, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C212, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C213, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C214, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C215, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C216, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C222, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G212, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G213, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C218, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C219, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C220, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C221, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G216, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G217, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G218, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G221, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C209)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C210)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C217)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C223)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), G220)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(G219, 3))

            '2.17 BEAM FLANGE NET SECTION CHECK
            cnt = 0 : prefix = "S217_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C226, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C227, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C228, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C229, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C230, 1))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C231, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C232, 0))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C233, 3))

            'DCR Summary
            cnt = 0 : prefix = "SUM_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F228, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F229, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F230, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F231, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F232, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Beam_tf_DCR, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Link_strength_DCR, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Lyield_check, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.t_BRP_DCR, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.BRP_Bolt_DCR, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(F228 <= curJob.DCRLimits.Beam_tf_DCR, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(F229 <= curJob.DCRLimits.Link_strength_DCR, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(F230 <= curJob.DCRLimits.Lyield_check, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(F231 <= curJob.DCRLimits.t_BRP_DCR, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(F232 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG"))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F233)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F234)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F233)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F234)
            ''DCR Summary
            'cnt = 0 : prefix = "SUM_"
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(E135, 3))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(E136, 3))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(E137, 3))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(E138, 3))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(E139, 3))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Beam_tf_DCR, 2))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Link_strength_DCR, 2))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.Lyield_check, 2))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.t_BRP_DCR, 2))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(curJob.DCRLimits.BRP_Bolt_DCR, 2))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(E135 <= curJob.DCRLimits.Beam_tf_DCR, "OK", "NG"))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(E136 <= curJob.DCRLimits.Link_strength_DCR, "OK", "NG"))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(E137 <= curJob.DCRLimits.Lyield_check, "OK", "NG"))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(E138 <= curJob.DCRLimits.t_BRP_DCR, "OK", "NG"))
            'cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(E139 <= curJob.DCRLimits.BRP_Bolt_DCR, "OK", "NG"))
        End If
    End Sub

    Public Sub SPC_Detail(ByRef curJob As clsMRSJob)
        Dim R_use = If(Angle > 45 And Angle < 135, curJob.RY, curJob.R)
        '4.1 CURRENT MEMBER:
        Dim maxDCRBmweb, MaxDCRSp As Double
        Dim C2 As String = _uName
        Dim C3 As String = Wsec.Size
        Dim C4 As Double = Pu_bm
        Dim C5 As Double = V_bmGravity ' + 2 * LKInfo.Pr_link * (Wsec.Depth + LKInfo.tstem) / Len_Cal
        Dim C6 As String = LKInfo.Size
        Dim C7 As Double = LKInfo.tstem
        '
        Dim F3 As Double = Sheartab.Thk / 16
        Dim F4 As Double = Sheartab.bSize / 8 '  bolt size giu nguyen
        Dim F5 As String = GetbolttypeStr(Sheartab.bType)
        Dim F6 As Double = Sheartab.stBolt.n_Vbolts_SST
        Dim F7 As Double = Sheartab.N_horzBolt
        '
        Dim I3 As Double = g50
        Dim I4 As Double = g65
        '
        Dim I5 As String = IIf(IsAsignEndI_K, "YES", "NO")
        Dim I6 As String = IIf(IsAsignEndJ_K, "YES", "NO")
        Dim I7 As Double = IIf(I5 = "YES" And I6 = "YES", 2, 1)

        '4.2 SHEAR PLATE BOLT SIZE:
        Dim C10 As String = C3
        Dim C11 As Double = Wsec.Depth
        Dim C12 As Double = Wsec.tf
        Dim C13 As Double = Wsec.tw
        Dim C14 As Double = curJob.MaterialInfo.ShearPlate_Fy
        Dim C15 As Double = curJob.MaterialInfo.ShearPlate_Fu
        Dim C16 As Double = C4
        Dim C17 As Double = Len_LCC
        Dim C18 As Double = Len_Cal
        Dim C19 As Double = IIf(R_use <= 3, LKInfo.Py_link, LKInfo.Pr_link)
        Dim C20 As Double = C19 * (C11 + C7)
        Dim C21 As Double = I7 * C20 / C18
        Dim C22 As Double = C5 + C21
        Dim C23 As Double = F6
        Dim C24 As Double = F7
        Dim C25 As Double = Math.Sqrt((C16 / C24) ^ 2 + (C22 / C23) ^ 2)
        Dim C26 As Double = (0.75)
        Dim C27 As String = F5.Replace("_", "-")
        Dim C28 As Double = F4
        Dim C29 As Double = Find_Fnv(Sheartab.bType)
        Dim C30 As Double = Math.PI * (C28 / 2) ^ 2
        Dim C31 As Double = C29 * C30
        Dim C32 As Double = C31 * C26
        Dim C33 As Double = C25 / C32
        '
        Dim C34 As String = IIf(C33 <= curJob.DCRLimits.Bolt_DCR, "OK", "NG")

        '4.3 SHEAR PLATE GEOMETRY:
        Dim C55 As Double = LKInfo.a
        Dim C56 As Double = F3
        Dim C57 As Double = C11
        Dim C58 As Double = 2 * (C28 + 1 / 8)
        Dim C60 As Double = Sheartab.stBolt.Svert  ' Ceiling(2.6667 * C28, 1 / 16)
        Dim C61 As Double = Sheartab.stBolt.Shorz  ' Ceiling(3 * C28, 1 / 16)
        Dim C62 As Double = LKInfo.h_flange
        Dim C63 As Double = LKInfo.tstem
        Dim C64 As Double
        If C3 Like "W12*" Then
            C64 = IIf(C3 = "W12X45" Or C3 = "W12X53" Or C3 = "W12X65", 0, 0.125)
        Else
            C64 = 0.125
        End If
        Dim C65 As Double = Sheartab.stBolt.Lv_sp
        Dim C66 As Double = C65
        Dim C67 As Double = C57 + C63 - C62 - 2 * C64
        Dim C59 As Double = (C67 - 2 * C66) / (C23 - 1)
        Dim C68 As Double = (C23 - 1) * C60 + 2 * C66
        Dim C69 As Double = Lvmin(F4)
        Dim C70 As Double = Sheartab.stBolt.Lh_sp

        Dim C71 As Double = (C24 - 1) * C61 + C70 + C55
        Dim C72 As Double = C28 + 1 / 8 + (2 * 0.07 * C60 * ((C23 - 1) / 2))
        Dim C73 As Double = Ceiling(C72, 0.125)

        Dim E60 As String = IIf(C60 <= C59 And C60 >= C58, "OK", "NG")
        Dim E68 As String = IIf(C68 / C67 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        Dim E70 As String = IIf(C69 / C70 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        Dim E73 As String = IIf(C72 / C73 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        Dim C74 As String = IIf(E68 = "OK" And E70 = "OK" And E73 = "OK", "OK", "NG")

        '4.4 SHEAR PLATE YIELDING (VERTICAL):
        Dim C77 As Double = 1
        Dim C78 As Double
        C78 = C28 + 1 / 8
        Dim C79 As Double = C56 * C68
        Dim C80 As Double
        C80 = C77 * 0.6 * C79 * C14
        Dim C81 As Double = C22 / C80
        Dim C82 As String = IIf(C81 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        '4.5 SHEAR PLATE RUPTURE (VERTICAL):
        Dim C85 As Double = 0.75
        Dim C86 As Double = C68 * C56 - C78 * C23 * C56
        Dim C87 As Double = C85 * 0.6 * C15 * C86
        Dim C88 As Double = C22 / C87
        Dim C89 As String = IIf(C88 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        '4.6 SHEAR PLATE CHECK FOR AXIAL AND MOMENT:
        Dim C120 As Double = 0.9
        Dim C121 As Double = C55
        Dim C122 As Double = C22
        Dim C123 As Double = C122 * C121
        Dim C124 As Double = 30
        Dim C125 As Double = Math.Tan(C124 * Math.PI / 180) * C121 * 2 + C28
        Dim C126 As Double = C125 * C56
        Dim C127 As Double = Math.Min(C68, Math.Tan(C124 * Math.PI / 180) * (C121 + C61) * 2 + C28)
        Dim C128 As Double = C127 * C56
        Dim C129 As Double = Math.Min(C68, Math.Tan(C124 * Math.PI / 180) * (C121 + 2 * C61) * 2 + C28)
        Dim C130 As Double = C129 * C56
        Dim Lwhitmore4 As Double = Math.Min(C68, Math.Tan(C124 * Math.PI / 180) * (C121 + 3 * C61) * 2 + C28)
        Dim Awhitmore4 As Double = Lwhitmore4 * C56
        Dim C131 As Double = (C56 * C68 ^ 2) / 6
        Dim C132 As Double = (C56 * C68 ^ 3) / 12
        Dim C133 As Double = C123 / C131
        Dim C134 As Double = IIf(C24 = 1, C126, IIf(C24 = 2, C128, IIf(C24 = 3, C130, Awhitmore4)))
        Dim C135 As Double = C125 / 2
        Dim C136 As Double = C123 * C135 / C132
        Dim C137 As Double = C16 / C134
        Dim C138 As Double = C137 + C136
        Dim C139 As Double = Math.Max(C138, C133)
        Dim C140 As Double = C120 * C14
        Dim C141 As Double = C139 / C140
        Dim C142 As String = IIf(C141 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        '4.7 SHEAR PLATE TO COLUMN FLANGE FILLET WELD:
        Dim C145 As Double = 0.75
        Dim C146 As Double = C56
        Dim C147 As Double = 5 / 8 * C146
        Dim C148 As Double
        C148 = Ceiling(C147, 1 / 16)
        'Dim C149 As String = IIf(C147 <= C148, "OK", "NG")
        Dim C150 As Double = C122
        Dim C151 As Double = C68 - 0.25 * 2
        Dim C152 As Double = 70
        Dim C153 As Double = 0.6 * C152
        Dim C154 As Double = 0.707 * C151 * 2 * C148
        Dim C155 As Double = C145 * C154 * C153
        Dim C156 As Double = C150 / C155
        Dim C157 As String = IIf(C156 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        '4.8 BEAM WEB AND SHEAR TAB BEARING:
        'CASE 1: HORIZONTAL REACTIONS
        'BEAM WEB:
        Dim C180 As Double = C26
        Dim C181 As Double = C13
        Dim C182 As Double = 1.75
        Dim C183 As Double = C182 - (C28 + 1 / 16) * 0.5
        Dim C184 As Double = IIf(C24 = 1, 0, C61 - (C28 + 1 / 16))
        Dim C185 As Double = IIf(C24 = 2, 0, C184)
        Dim L_cw4 As Double = IIf(C24 = 3, 0, C185)
        Dim C186 As Double = C183 + C184 + C185 + L_cw4
        Dim C187 As Double = C180 * 1.2 * C186 * C181 * I4
        Dim C188 As Double = C180 * 2.4 * C28 * C181 * I4 * C24
        Dim C189 As Double = Math.Min(C187, C188)
        Dim C190 As Double = C16 / C189
        Dim C191 As String = IIf(C190 <= curJob.DCRLimits.Beam_Web_DCR, "OK", "NG")
        'SHEAR PLATE:
        Dim C193 As Double = C56
        Dim C194 As Double = C70
        Dim C195 As Double = C194 - (C28 + 1 / 16) * 0.5
        Dim C196 As Double = IIf(C24 = 1, 0, C61 - (C28 + (1 / 16)))
        Dim C197 As Double = IIf(C24 = 2, 0, C196)
        Dim L_sp4 As Double = IIf(C24 = 3, 0, C197)
        Dim C198 As Double = C195 + C196 + C197 + L_sp4
        Dim C199 As Double = C180 * 1.2 * C198 * C56 * C15
        Dim C200 As Double = C180 * 2.4 * C28 * C56 * I4 * C24
        Dim C201 As Double = Math.Min(C199, C200)
        Dim C202 As Double = C16 / C201
        Dim C203 As String = IIf(C202 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        'CASE 2: VERTICAL REACTIONS
        'BEAM WEB:
        Dim C224 As Double = C26
        Dim C225 As Double = C60 - (C28 + (1 / 16))
        Dim C226 As Double = (C11 - 2 * C12 - (C23 - 1) * C60) * 0.5
        Dim C227 As Double = C226 + (C23 - 1) * C225
        Dim C228 As Double = C180 * 1.2 * C227 * C13 * I4
        Dim C229 As Double = C180 * 2.4 * C28 * C23 * C13 * I4
        Dim C230 As Double = Math.Min(C228, C229)
        Dim C231 As Double = C22 / C230
        Dim C232 As String = IIf(C231 <= curJob.DCRLimits.Beam_Web_DCR, "OK", "NG")
        'SHEAR PLATE:
        Dim C234 As Double = C66 - (C28 + (1 / 16)) * 0.5
        Dim C235 As Double = C234 + (C23 - 1) * C225
        Dim C236 As Double = C180 * 1.2 * C235 * C56 * C15
        Dim C237 As Double = C180 * 2.4 * C28 * C23 * C56 * C15
        Dim C238 As Double = Math.Min(C236, C237)
        Dim C239 As Double = C22 / C238
        Dim C240 As String = IIf(C239 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        'CASE 3: COMBINED AXIAL AND VERTICAL REACTIONS
        Dim C260 As Double = C16 / C24
        Dim C261 As Double = C22 / C23
        Dim C262 As Double = Math.Sqrt(C260 ^ 2 + C261 ^ 2)
        Dim C263 As Double = Math.Min(Math.Atan(C261 / C260), 1.571)
        'BEAM WEB:
        Dim C267 As Double = C190 ^ 2 + C231 ^ 2
        Dim C270 As Double = C182 / Math.Cos(C263)
        Dim C271 As Double = C270 - C78 / 2
        Dim C272 As Double = C180 * 1.2 * C271 * C13 * I4
        Dim C273 As Double = C180 * 2.4 * C28 * C13 * I4 * C24
        Dim C274 As Double = Math.Min(C272, C273)
        Dim C275 As Double = C262 / C274
        Dim C277 As Double = Math.Max(C267, C275)
        Dim C278 As String = IIf(C277 <= curJob.DCRLimits.Beam_Web_DCR, "OK", "NG")
        'SHEAR PLATE:
        Dim C282 As Double = C202 ^ 2 + C239 ^ 2
        Dim C285 As Double = Math.Atan((0.5 * C73) / C61)
        Dim C286 As Double = Math.Atan(C68 / 2 / (C71 - C55))
        Dim C287 As Double = Math.Atan((0.5 * C73 / C60))
        Dim C288 As Double = Math.PI * 0.5 - C287
        Dim C289 As Double = C263
        Dim C292 As Double = 0
        Dim C294 As String = IIf(C289 >= 0 And C289 < C285, "YES", "NO")
        Dim C295 As String = IIf(C294 = "NO", "-", NumberFormat((C61 - C78), 3))
        Dim D294 As String = IIf(C289 >= C285 And C289 < C286, "YES", "NO")
        Dim D295 As String = IIf(D294 = "NO", "-", NumberFormat((C71 - C55) / Math.Cos(C289), 3))
        Dim E294 As String = IIf(C289 >= C286 And C289 < C288, "YES", "NO")
        Dim E295 As String = IIf(E294 = "NO", "-", NumberFormat((0.5 * C68) / Math.Cos(1.571 - C289), 3))
        Dim F293 As Double = 1.571
        Dim F294 As String = IIf(C289 >= C288 And C289 < 1.571, "YES", "NO")
        Dim F295 As String = IIf(F294 = "NO", "-", C60 - C78)
        Dim C295D, D295D, E295D, F295D As Double
        If C295 = "-" Then
            C295D = 0
        Else
            Double.TryParse(C295, C295D)
        End If
        If D295 = "-" Then
            D295D = 0
        Else
            Double.TryParse(D295, D295D)
        End If
        If E295 = "-" Then
            E295D = 0
        Else
            Double.TryParse(E295, E295D)
        End If
        If F295 = "-" Then
            F295D = 0
        Else
            Double.TryParse(F295, F295D)
        End If
        Dim C296 As Double = C295D + D295D + E295D + F295D
        Dim C297 As Double = C180 * 1.2 * C296 * C56 * C15
        Dim C298 As Double = C180 * 2.4 * C28 * C56 * C15 * C24
        Dim C299 As Double = Math.Min(C297, C298)
        Dim C300 As Double = C262 / C299
        Dim C302 As Double = Math.Max(C300, C282)
        Dim C303 As String = IIf(C302 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        '4.9 BEAM WEB AND SHEAR TAB BLOCKSHEAR CHECK:
        'CASE 1: HORIZONTAL REACTIONS
        'CASE 1
        'BEAM WEB:
        Dim C336 As Double = 0.75
        Dim C337 As Double = 1
        Dim C338 As Double = C182
        Dim C339 As Double = C186
        Dim C340 As Double = 2 * (C338 + C61 * (C24 - 1))
        Dim C341 As Double = C340 * C13
        Dim C342 As Double = 0
        Dim C343 As Double = C341
        Dim C344 As Double = 0.6 * I4 * C343 + C337 * I4 * C342
        Dim C345 As Double = 0.6 * I3 * C341 + C337 * C342 * I4
        Dim C346 As Double = C336 * Math.Min(C344, C345)
        Dim C347 As Double = C16 / C346
        Dim C348 As String = IIf(C347 <= curJob.DCRLimits.Beam_Web_DCR, "OK", "NG")
        'SHEAR PLATE:
        Dim C351 As Double = C194
        Dim C352 As Double = C198
        Dim C353 As Double = 2 * (C351 + C61 * (C24 - 1))
        Dim C354 As Double = C353 * C56
        Dim C355 As Double = 0 ' 0 * 2 * Math.PI * C28 / 2 * 0.5 * C56
        Dim C356 As Double = C354
        Dim C357 As Double = 0.6 * C15 * C356 + C337 * C15 * C355
        Dim C358 As Double = 0.6 * C14 * C354 + C337 * C15 * C355
        Dim C359 As Double = C336 * Math.Min(C357, C358)
        Dim C360 As Double = C16 / C359
        '
        Dim C363 As Double = C68 - (C23 * C78)
        Dim C364 As Double = 0
        Dim C365 As Double = C363 * F3
        Dim C366 As Double = 0
        Dim C367 As Double = 0.6 * C15 * C366 + C337 * C15 * C365
        Dim C368 As Double = 0.6 * C14 * C364 + C337 * C15 * C365
        Dim C369 As Double = C336 * Math.Min(C367, C368)
        Dim C370 As Double = C16 / C369
        Dim C372 As Double = Math.Max(C360, C370)
        Dim C373 As String = IIf(C360 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        'CASE 2: VERTICAL REACTIONS
        'BEAM WEB:
        Dim C414 As Double = C60
        Dim C415 As Double = C182
        Dim C416 As Double = (C11 - (C23 - 1) * C60 - 2 * C12) * 0.5 + (C23 - 1) * C60
        Dim C417 As Double = C13 * C416
        Dim C418 As Double = C13 * (C416 - (C23 - 0.5) * C78)
        Dim C419 As Double = (C415 - C78 / 2) * C13
        Dim C420 As Double = 0.6 * I4 * C418 + C337 * I4 * C419
        Dim C421 As Double = 0.6 * I3 * C417 + C337 * I4 * C419
        Dim C422 As Double = C336 * Math.Min(C420, C421)
        Dim C423 As Double = C22 / C422
        Dim C424 As String = IIf(C423 <= curJob.DCRLimits.Beam_Web_DCR, "OK", "NG")
        'SHEAR PLATE:
        Dim C428 As Double = C68
        Dim C429 As Double = C428 * C56
        Dim C430 As Double = C56 * (C428 - (C23 * C78))
        Dim C431 As Double = 0
        Dim C432 As Double = 0.6 * C15 * C430 + C337 * C15 * C431
        Dim C433 As Double = 0.6 * C14 * C429 + C337 * C15 * C431
        Dim C434 As Double = C336 * Math.Min(C432, C433)
        Dim C435 As Double = C22 / C434
        '
        Dim C438 As Double = C66
        Dim C439 As Double = C68
        Dim C440 As Double = C73
        Dim C441 As Double = (C439 - C66 - (0.5 * C78)) * C56
        Dim C442 As Double = C56 * (C439 - (C23 - 0.5) * C78)
        Dim C443 As Double = (C71 - C55 - (0.5 * C73)) * C56
        Dim C444 As Double = 0.6 * C15 * C442 + C337 * C15 * C443
        Dim C445 As Double = 0.6 * C14 * C441 + C337 * C15 * C443
        Dim C446 As Double = C336 * Math.Min(C444, C445)
        Dim C447 As Double = C22 / C446
        Dim C449 As Double = Math.Max(C435, C447)
        Dim C450 As String = IIf(C449 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        'CASE 3: COMBINED AXIAL AND VERTICAL REACTIONS
        'BEAM WEB:
        Dim C509 As Double = C347 ^ 2 + C423 ^ 2
        Dim C512 As Double = Math.Sqrt((C16 ^ 2) + (C22 ^ 2))
        Dim C513 As Double = C263
        Dim C514 As Double = C182
        Dim C515 As Double = 2 * C514 + (C24 - 1) * C61 + Math.Sqrt(C60 ^ 2 + ((C24 - 1) * C61) ^ 2)
        Dim C516 As Double = 2 * C514 + (C24 - 1) * C61 + Math.Sqrt(C60 ^ 2 + ((C24 - 1) * C61) ^ 2)
        Dim C517 As Double = C516 - 2 * C78 - (F7 - 1) * C78 ' IIf(C515 = (C514 + C61) / Math.Cos(C513), C516 - C78 * 2, C516 - C78 * 3)
        Dim C518 As Double = (C23 - 2) * C60
        Dim C519 As Double = C518 - (C23 - 2) * C78
        Dim C520 As Double = C516 * C13
        Dim C521 As Double = C517 * C13
        Dim C522 As Double = C518 * C13
        Dim C523 As Double = C519 * C13
        Dim C524 As Double = 0.6 * I4 * C521 + C337 * I4 * C523
        Dim C525 As Double = 0.6 * I3 * C520 + C337 * I4 * C523
        Dim C526 As Double = C336 * Math.Min(C524, C525)
        Dim C527 As Double = C512 / C526
        Dim C529 As Double = Math.Max(C527, C509)
        Dim C530 As String = IIf(C529 <= curJob.DCRLimits.Beam_Web_DCR, "OK", "NG")
        'SHEAR PLATE:
        Dim C534 As Double = C372 ^ 2 + C449 ^ 2
        Dim C539 As Double = C68
        Dim C540 As Double = C539 - C23 * C78
        Dim C541 As Double = 0
        Dim C542 As Double = C539 * C56
        Dim C543 As Double = C540 * C56
        Dim C544 As Double = C541 * C56
        Dim C545 As Double = 0.6 * C15 * C543 + C337 * C15 * C544
        Dim C546 As Double = 0.6 * C14 * C542 + C337 * C15 * C544
        Dim C547 As Double = C336 * Math.Min(C545, C546)
        Dim C548 As Double = C512 / C547
        '
        Dim C551 As Double = Ceiling(C23 / 2, 1) / C23 * C22
        Dim C552 As Double = Math.Sqrt(C551 ^ 2 + C16 ^ 2)
        Dim C553 As Double = C539 / 2
        Dim C554 As Double = C553 - ((C23 - 1) / 2 * C78) - (0.5 * C78)
        Dim C555 As Double = (C71 - C55) - ((C24 - 1) * C78) - (0.5 * C78)
        Dim C556 As Double = C553 * C56
        Dim C557 As Double = C554 * C56
        Dim C558 As Double = C555 * F3
        Dim C559 As Double = ((0.6 * C15 * C557) + (C337 * C15 * C558))
        Dim C560 As Double = ((0.6 * C14 * C556) + (C337 * C15 * C558))
        Dim C561 As Double = C336 * Math.Min(C559, C560)
        Dim C562 As Double = C552 / C561
        '
        Dim C565 As Double = C71 - C55
        Dim C566 As Double = C555
        Dim C567 As Double = C554
        Dim C568 As Double = C565 * F3
        Dim C569 As Double = C566 * F3
        Dim C570 As Double = C567 * F3
        Dim C571 As Double = ((0.6 * C15 * C569) + (C337 * C15 * C570))
        Dim C572 As Double = ((0.6 * C14 * C568) + (C337 * C15 * C570))
        Dim C573 As Double = C336 * Math.Min(C571, C572)
        Dim C574 As Double = C552 / C573
        Dim C576 As Double = MaxAll(C548, C534, C562, C574)
        Dim C577 As String = IIf(C576 <= curJob.DCRLimits.Shear_Plate_DCR, "OK", "NG")
        'DCR Summary
        'Beam Web
        Dim G583 As Double = MaxAll(C190, C231, C277)
        Dim H583 As Double = MaxAll(C347, C423, C529)
        Sheartab.sDCR1_BmWeb = NumberFormat(MaxAll(G583, H583), 3)
        'SHEAR PLATE
        Dim D584 As Double = C81
        Dim E584 As Double = C88
        Dim F584 As Double = C141
        Dim G584 As Double = MaxAll(C202, C239, C302)
        Dim H584 As Double = MaxAll(C372, C449, C576)
        Sheartab.sDCR3_ShearPL = NumberFormat(MaxAll(D584, E584, F584, G584, H584), 3)
        'shear plate geometry
        Sheartab.sDCR2_ShearPLGeometry = C74
        'shear plate bolt
        Sheartab.sDCR4_Bolt = NumberFormat(C33, 3)
        'sp fillet weld
        Sheartab.sDCR5_Weld = NumberFormat(C156, 3)

        If Not curJob.stlData Is Nothing Then
            curJob.stlData.Clear()
            Dim cnt As Long = 0
            '4.1 CURRENT MEMBER:
            Dim prefix As String = "S41_"
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C2)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C3)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C4, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C5, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C6)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C7)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F3, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F4, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F5)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F6, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F7, 2))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I3)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I4)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I5)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I6)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), I7)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), R_use)
            '4.2 SHEAR PLATE BOLT SIZE:
            cnt = 0 : prefix = "S42_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C10)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C11, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C12, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C13, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C14)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C15)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C16, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C17, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C18, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C19, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C20, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C21, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C22, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C23)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C24)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C25, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C26, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C27)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C28, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C29, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C30, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C31, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C32, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C33, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C34)

                '4.3 SHEAR PLATE GEOMETRY:
                cnt = 0 : prefix = "S43_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C55, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C56, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C57, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C58, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C59, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C60, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C61, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C62, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C63, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C64, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C65, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C66, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C67, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C68, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C69, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C70, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C71, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C72, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C73, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C74)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E60)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E68)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E70)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E73)
                '4.4 SHEAR PLATE YIELDING (VERTICAL):
                cnt = 0 : prefix = "S44_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C77)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C78, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C79, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C80, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C81, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C82)
                '4.5 SHEAR PLATE RUPTURE (VERTICAL):
                cnt = 0 : prefix = "S45_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C85, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C86, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C87, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C88, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C89)

                '4.6 SHEAR PLATE CHECK FOR AXIAL AND MOMENT:
                cnt = 0 : prefix = "S46_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C120, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C121, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C122, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C123, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C124)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C125, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C126, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C127, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C128, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C129, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C130, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C131, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C132, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C133, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C134, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C135, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C136, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C137, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C138, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C139, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C140, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C141, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C142)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Lwhitmore4, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(Awhitmore4, 3))
            '4.7 SHEAR PLATE TO COLUMN FLANGE FILLET WELD:
            cnt = 0 : prefix = "S47_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C145, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C146, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C147, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C148, 3))

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C150, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C151, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C152, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C153, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C154, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C155, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C156, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C157)

                '4.8 BEAM WEB AND SHEAR TAB BEARING:
                'case 1
                cnt = 0 : prefix = "S48_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C180, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C181, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C182, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C183, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C184, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C185, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C186, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C187, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C188, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C189, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C190, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C191)

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C193, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C194, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C195, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C196, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C197, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C198, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C199, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C200, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C201, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C202, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C203)
                'case 2
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C224, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C225, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C226, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C227, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C228, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C229, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C230, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C231, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C232)

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C234, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C235, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C236, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C237, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C238, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C239, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C240)
                'case 3
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C260, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C261, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C262, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C263, 3))

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C267, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C270, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C271, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C272, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C273, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C274, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C275, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C277, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C278)

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C282, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C285, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C286, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C287, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C288, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C289, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C292)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C294)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C295)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C296, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C297, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C298, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C299, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C300, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C302, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C303)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D294)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), D295)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E294)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), E295)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(F293, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F294)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), F295)
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(L_cw4, 3))
            cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(L_sp4, 3))
            '4.9 BEAM WEB AND SHEAR TAB BLOCKSHEAR CHECK:
            'CASE 1
            cnt = 0 : prefix = "S49_C1_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C336, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C337, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C338, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C339, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C340, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C341, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C342, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C343, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C344, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C345, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C346, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C347, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C348)

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C351, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C352, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C353, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C354, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C355, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C356, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C357, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C358, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C359, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C360, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C363, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C364, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C365, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C366, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C367, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C368, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C369, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C370, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C372, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C373)
                'CASE 2
                cnt = 0 : prefix = "S49_C2_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C414, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C415, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C416, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C417, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C418, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C419, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C420, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C421, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C422, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C423, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C424)

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C428, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C429, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C430, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C431)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C432, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C433, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C434, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C435, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C438, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C439, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C440, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C441, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C442, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C443, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C444, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C445, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C446, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C447, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C449, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C450)
                'CASE 3
                cnt = 0 : prefix = "S49_C3_"
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C509, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C512, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C513, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C514, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C515, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C516, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C517, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C518, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C519, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C520, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C521, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C522, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C523, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C524, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C525, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C526, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C527, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C529, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C530)

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C534, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C539, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C540, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C541, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C542, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C543, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C544, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C545, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C546, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C547, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C548, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C551, 1))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C552, 1))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C553, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C554, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C555, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C556, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C557, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C558, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C559, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C560, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C561, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C562, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C565, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C566, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C567, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C568, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C569, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C570, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C571, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C572, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C573, 2))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C574, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(C576, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), C577)
                'DCR Summary
                cnt = 0 : prefix = "SUM_"
                'Shear
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                Dim rc31 As Double = IIf(IsNumeric(C33), C33, 0)
                Dim rc41 As Double = IIf(IsNumeric(C33), C156, 0)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc31, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc41, 3))
                'Yielding
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                Dim rc22 As Double = IIf(IsNumeric(C81), C81, 0)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc22, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                'Rupture
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                Dim rc23 As Double = IIf(IsNumeric(C88), C88, 0)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc23, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                'P+M
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                Dim rc24 As Double = IIf(IsNumeric(C141), C141, 0)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc24, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                'Bearing
                Dim rc15 As Double = MaxAll(C190, C231, C277)
                Dim rc25 As Double = MaxAll(C202, C239, C302)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc15, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc25, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                'BS
                Dim rc16 As Double = MaxAll(C347, C423, C529)
                Dim rc26 As Double = MaxAll(C372, C449, C576)
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc16, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc26, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), "-")
                'Max
                Dim rc17 = MaxAll(rc15, rc16)
                Dim rc27 = MaxAll(rc22, rc23, rc24, rc25, rc26)
                Dim rc37 = rc31
                Dim rc47 = rc41

                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc17, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc27, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc37, 3))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), NumberFormat(rc47, 3))
                'Check
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(rc17 > 1, "NG", "OK"))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(rc27 > 1, "NG", "OK"))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(rc37 > 1, "NG", "OK"))
                cnt += 1 : curJob.stlData.Add(prefix & Format(cnt, "00#"), IIf(rc47 > 1, "NG", "OK"))
            End If
    End Sub
    'Private Function MinBeamEdg(lksize As String) As Double
    '    Select Case True
    '        Case lksize = "Pinned"
    '            Return 0
    '        Case lksize Like "YL4*"
    '            Return 1.125
    '        Case lksize Like "YL6-3*", lksize = "YL6-4", lksize = "YL6-4-13"
    '            Return 1.25
    '        Case Else
    '            Return 1.625
    '    End Select
    'End Function
    'Private Function MinColumnEdg(lksize As String) As Double
    '    Select Case True
    '        Case lksize = "Pinned"
    '            Return 0
    '        Case lksize Like "YL4*"
    '            Return 1.125
    '        Case lksize = "YL6-3", lksize = "YL6-3.5", lksize = "YL6-3-13", lksize = "YL6-3.5-13"
    '            Return 1.25
    '        Case lksize Like "YL6-4*"
    '            Return 1.625
    '        Case lksize Like "YL6-6*" ', "YL8-4.5", "YL8-5"
    '            Return 1.71875
    '        Case lksize = "YL8-4"
    '            Return 1.625
    '        Case Else
    '            Return 1.875
    '    End Select
    'End Function
    Private Function FindMinBeamFlange(lksize As String) As Double
        Select Case lksize
            Case "Pinned" : Return -1
            Case "YL4-2.25" : Return 6.125
            Case "YL4-2.875" : Return 6.75
            Case "YL4-3.5" : Return 7.5
            Case "YL4-3.75" : Return 7.625
            Case "YL4-4" : Return 7.875
            Case "YL4-2.25-10" : Return 6.125
            Case "YL4-2.875-10" : Return 6.75
            Case "YL4-3.5-10" : Return 7.5
            Case "YL4-3.75-10" : Return 7.625
            Case "YL4-4-10" : Return 7.875
            Case "YL6-3" : Return 7.25
            Case "YL6-3.5" : Return 7.75
            Case "YL6-4" : Return 8.25
            Case "YL6-4.5" : Return 9.25
            Case "YL6-5" : Return 9.25
            Case "YL6-5.5" : Return 10
            Case "YL6-6" : Return 10.5
            Case "YL6-3-13" : Return 7.25
            Case "YL6-3.5-13" : Return 7.75
            Case "YL6-4-13" : Return 8.25
            Case "YL6-4.5-13" : Return 9.25
            Case "YL6-5-13" : Return 9.5
            Case "YL6-5.5-13" : Return 10
            Case "YL6-6-13" : Return 10.5
            Case "YL8-4" : Return 8.5
            Case "YL8-4.5" : Return 9
            Case "YL8-5" : Return 10
            Case "YL8-5.5" : Return 10
            Case "YL8-6" : Return 10.5
            Case Else : Return 0
        End Select
    End Function

    Private Function FindMinColumnFlange(lksize As String) As Double
        Select Case lksize
            Case "Pinned" : Return -1
            Case "YL4-2.25" : Return 6.75
            Case "YL4-2.875" : Return 6.75
            Case "YL4-3.5" : Return 7.5
            Case "YL4-3.75" : Return 7.5
            Case "YL4-4" : Return 7.5
            Case "YL4-2.25-10" : Return 6.75
            Case "YL4-2.875-10" : Return 6.75
            Case "YL4-3.5-10" : Return 7.5
            Case "YL4-3.75-10" : Return 7.625
            Case "YL4-4-10" : Return 7.875
            Case "YL6-3" : Return 7.25
            Case "YL6-3.5" : Return 7.75
            Case "YL6-4" : Return 8.25
            Case "YL6-4.5" : Return 9.25
            Case "YL6-5" : Return 9.25
            Case "YL6-5.5" : Return 10
            Case "YL6-6" : Return 10.5
            Case "YL6-3-13" : Return 7.25
            Case "YL6-3.5-13" : Return 7.5
            Case "YL6-4-13" : Return 8.25
            Case "YL6-4.5-13" : Return 9.25
            Case "YL6-5-13" : Return 9.5
            Case "YL6-5.5-13" : Return 10
            Case "YL6-6-13" : Return 10.5
            Case "YL8-4" : Return 8.5
            Case "YL8-4.5" : Return 9
            Case "YL8-5" : Return 10
            Case "YL8-5.5" : Return 10
            Case "YL8-6" : Return 10.5
            Case Else : Return 0
        End Select
    End Function

End Class
