﻿Imports System.Xml.Serialization

''Public Class SSTBRP
''    Public BRPs As New List(Of BRPInfo)

''    Sub New()
''    End Sub
''End Class

Public Class SSTLinks
    Public Links As List(Of LinkInfo)
    Public BRPs As List(Of BRPInfo)

    Sub New()
    End Sub
End Class

Public Class SSTOthers
    Public SheartabBolts As List(Of SheartabBolt)

    Sub New()
    End Sub
End Class


Public Class BRPInfo
    <XmlAttributeAttribute> Public Size As String
    Public Size_BRP As String
    Public t_brp As Double
    Public W_brp As Double
    Public L_brp As Double
    Public D_brp As Double
    Public n_brp As Integer
    Public S_brp As Double
    Public L_be As Double
    Public g_brp As Double
    Public bbf_BRP As Double
    Public bbf_stem As Double
    Public BRP_bolt_edge2 As Double
    Public Stem_bolt_edge As Double
    Public L1_brp As Double
    Public L2_brp As Double

    Sub New()
    End Sub
End Class

Public Class LinkInfo
    <XmlAttributeAttribute> Public Size As String
    Public tstem As Double
    Public Lcol_side As Double
    Public Ly_yield As Double
    Public sc As Double
    Public s_stem As Double
    Public sb As Double
    Public n_bolt As Integer
    Public boltDia_stem As Double
    Public Lbm_side As Double
    Public Wcol_side As Double
    Public b_yield As Double
    Public g_stem As Double
    Public t_flange As Double
    Public S_flange As Double
    Public boltDia_Flange As Double
    Public h_flange As Double
    Public g_flange As Double
    Public Py_link As Double
    Public Pr_link As Double
    Public Keff As Double
    Public a As Double
    Public bbf_min As Double
    Public bcf_min As Double
    Public L_edge As Double
    Public Pu_Rigid As Double
    Public Pu_Rigid_R3 As Double
    Sub New()
    End Sub
End Class

Public Class SheartabBolt
    <XmlAttributeAttribute> Public Size As String
    Public n_Vbolts_SST As Integer
    Public db_sp_default As Double
    Public Svert As Double
    Public Shorz As Double
    Public Lv_sp As Double
    Public Lh_sp As Double
    Public Db_min As Double
    Public Db_max As Double

    Sub New()
    End Sub
End Class

Public Class AllWsections
    Public Wsections As New List(Of Wsec)
    Sub New()
    End Sub
End Class

Public Class Wsec
    <XmlAttributeAttribute> Public Size As String
    Public Area As Double
    Public Depth As Double
    Public D_det As Double
    Public bf As Double
    Public bf_det As Double
    Public tw As Double
    Public twdet_2 As Double
    Public tf As Double
    Public tfdet As Double
    Public kdes As Double
    Public kdet As Double
    Public k1 As Double
    Public Z33 As Double
    Public S33 As Double
    Public bf_2tf As Double
    Public h_tw As Double
    Sub New()
    End Sub

    Sub New(_Size$, _Depth#, _D_det#, _bf_det#, _tw#, _tfdet#)
        Size = _Size
        Depth = _Depth
        D_det = _D_det
        bf_det = _bf_det
        tw = _tw
        tfdet = _tfdet
    End Sub

End Class

Public Class GridInfo
    Implements IEquatable(Of GridInfo)
    '
    Public X0 As Double 'origin X of Grid system
    Public Y0 As Double 'origin Y of Grid system
    Public Rx As Double 'Rotation of Grid system
    '
    Public GridName As String
    Public GridX1 As Double
    Public GridY1 As Double
    Public GridX2 As Double
    Public GridY2 As Double
    '
    Sub New(_GridName As String, _GridX1 As Double, _GridY1 As Double)
        GridName = _GridName
        GridX1 = _GridX1
        GridY1 = _GridY1
    End Sub
    Sub New(_GridName As String, _GridX1 As Double, _GridY1 As Double, _GridX2 As Double, _GridY2 As Double)
        GridName = _GridName
        GridX1 = _GridX1
        GridY1 = _GridY1
        GridX2 = _GridX2
        GridY2 = _GridY2
        'Dim GridAngle = Math.Atan2(_GridY2 - _GridY1, _GridX2 - _GridX1) * 180 / Math.PI
        '
    End Sub
    '
    Public Overloads Function Equals(ByVal other As GridInfo) As Boolean Implements IEquatable(Of GridInfo).Equals
        If Me.GridName.Equals(other.GridName, StringComparison.CurrentCultureIgnoreCase) Then
            Return True
        Else
            Return False
        End If
    End Function

End Class