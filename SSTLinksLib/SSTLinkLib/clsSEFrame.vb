﻿Imports YieldLinkLib
Public Class clsSEFrame
    Implements IEquatable(Of clsSEFrame)

    Public GridID_LeftSide As String
    Public GridID_RightSide As String
    Public ElevID As String
    Public ElevID_WeakAxis As String
    Public StyID As String
    Public LevelHeight As Double
    Public _uName As String
    Public oldKi As Double
    Public oldKj As Double
    Public isColumn As Boolean
    Public Len_Cal As Double
    Public Len_LCC As Double
    Public Wsec As Wsec
    '
    Public StyAbove As String
    Public curSty As String
    Public topSty As String
    '
    Public Pt_I As String
    Public Pt_J As String
    'Start point location
    Public sPt_X As Double
    Public sPt_Y As Double
    '
    Public Angle As Double
    Sub New()
    End Sub

    Sub New(_StyID As String, in_uName$, _isColumn As Boolean, _Len_Cal#, _iPoint$, _jPoint$)
        StyID = _StyID
        _uName = in_uName
        isColumn = _isColumn
        Len_Cal = _Len_Cal
        Pt_I = _iPoint
        Pt_J = _jPoint
    End Sub

    Sub New(_StyID$, _LevelHeight#, _ElevID$, _ElevID_WeakAxis$, _GridID_LeftSide$, _GridID_RightSide$, in_uName$, _isColumn As Boolean, _frAngle As Double,
            _Len_Cal#, _iPoint$, _jPoint$, _Wsec As Wsec, _oldKi#, _oldKj#, _StyAblove$, _StyTop$, _StPX#, _StPY#)
        StyID = _StyID
        LevelHeight = _LevelHeight
        ElevID = _ElevID
        ElevID_WeakAxis = _ElevID_WeakAxis
        GridID_LeftSide = _GridID_LeftSide
        GridID_RightSide = _GridID_RightSide
        _uName = in_uName
        isColumn = _isColumn
        Len_Cal = _Len_Cal
        Pt_I = _iPoint
        Pt_J = _jPoint
        '
        Wsec = _Wsec
        oldKi = _oldKi
        oldKj = _oldKj
        '
        StyAbove = _StyAblove
        topSty = _StyTop
        sPt_X = _StPX
        sPt_Y = _StPY
        Angle = _frAngle
    End Sub

    Sub UpdateFrame(_StyID As String, _LevelHeight As Double, _ElevID As String, _GridID_LeftSide As String, _GridID_RightSide As String, in_uName$, _isColumn As Boolean,
            _Len_Cal#, _iPoint$, _jPoint$, _Wsec As Wsec, _oldKi As Double, _oldKj As Double)
        StyID = _StyID
        LevelHeight = _LevelHeight
        ElevID = _ElevID
        GridID_LeftSide = _GridID_LeftSide
        GridID_RightSide = _GridID_RightSide
        _uName = in_uName
        isColumn = _isColumn
        Len_Cal = _Len_Cal
        Pt_I = _iPoint
        Pt_J = _jPoint
        '
        Wsec = _Wsec
        oldKi = _oldKi
        oldKj = _oldKj
    End Sub



    Public Overloads Function Equals(ByVal other As clsSEFrame) As Boolean Implements IEquatable(Of clsSEFrame).Equals
        Return Me._uName = other._uName
    End Function

End Class
