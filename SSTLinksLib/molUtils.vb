﻿Imports System.IO
Imports System.Text
Imports System.Reflection

Public Module molUtils
    Public gES As Double = 29000 'Ksi
    Public g50 As Double = 50 ' 448.2 'Ksi
    Public g65 As Double = 65 ' 344.7 'Ksi
    Public g68 As Double = 68 ' 344.7 'Ksi
    Public g90 As Double = 90 ' 344.7 'Ksi

    Enum EntType As Short
        mLine
        mPolyline
        mMtext
        mDtext
        mBlk
        mAttBlk
        mDimension
        mCircle
        mSlottedHole
        mLeader
        mHatch
    End Enum
    Enum AllignType As Short
        Any = 0
        TopLeft = 1
        TopCenter = 2
        TopRight = 3
        MiddleLeft = 4
        MiddleCenter = 5
        MiddleRight = 6
        BottomLeft = 7
        BottomCenter = 8
        BottomRight = 9
    End Enum
    Enum LinkLoc
        TopLeft
        TopRight
        BotLeft
        BotRight
    End Enum
    Enum ColumnLoc
        Any
        TopLeft
        TopCenter
        TopRight
        BotLeft
        BotCenter
        BotRight
    End Enum
    Public Enum StiffStyle
        NotRequired
        Full_Depth
        Partial_Depth
        Required
        PS_LeftFLG
        PS_RightFLG
    End Enum
    Public Enum IFactor As Short
        Low = 1
        Normal = 2
        High = 3
        Post_Disaster = 4
    End Enum
    Public Enum LoadType As Short
        Snow = 1
        Wind = 2
        Earthquake = 3
    End Enum

    Enum mChar As Short
        Any
        A
        B
        C
        D
        E
        F
        G
        H
        I
        J
        K
        L
        M
        N
        O
        P
        Q
        R
        S
        T
        U
        V
        W
        X
        Y
        Z
    End Enum

    Public Sub Find_STP_Style(ByRef topSTP As StiffStyle, ByRef botSTP As StiffStyle, col As clsSSTColumn, topOfCol As Boolean, def1SidedConnStiff As StiffStyle)
        'reset
        topSTP = StiffStyle.NotRequired
        botSTP = StiffStyle.NotRequired
        '
        Dim Left_MFconn, Right_MFconn, userSelectYes, bracingAtBmBotFLG As Boolean
        Dim hasYL68_Left, hasYL68_Right As Boolean
        Try
            Left_MFconn = col.BmLeft.IsAsignEndJ_K
        Catch ex As Exception
        End Try
        Try
            Right_MFconn = col.BmRight.IsAsignEndI_K
        Catch ex As Exception
        End Try
        'Check if has YL68
        If Left_MFconn Then
            hasYL68_Left = (col.BmLeft.LKInfo.Size Like "YL6*" Or col.BmLeft.LKInfo.Size Like "YL8*")
        End If
        If Right_MFconn Then
            hasYL68_Right = (col.BmRight.LKInfo.Size Like "YL6*" Or col.BmRight.LKInfo.Size Like "YL8*")
        End If
        '
        userSelectYes = col.ProvideStiff.Equals("YES", StringComparison.CurrentCultureIgnoreCase)
        bracingAtBmBotFLG = col.isBraceAtBmBotFKG

        If Left_MFconn And Right_MFconn Then
            If userSelectYes = True And bracingAtBmBotFLG = False Then
                topSTP = StiffStyle.Full_Depth
                botSTP = StiffStyle.Full_Depth
            ElseIf userSelectYes = True And bracingAtBmBotFLG = True Then
                topSTP = StiffStyle.Full_Depth
                botSTP = StiffStyle.Full_Depth
            ElseIf userSelectYes = False And bracingAtBmBotFLG = True Then
                If topOfCol = False Or hasYL68_Left Or hasYL68_Right Then
                    topSTP = StiffStyle.Full_Depth
                End If
                botSTP = StiffStyle.Full_Depth
            ElseIf topOfCol = False Or hasYL68_Left Or hasYL68_Right Then 'cap plate can replace top stiff
                topSTP = StiffStyle.Full_Depth
            End If
        ElseIf Left_MFconn Or Right_MFconn Then
            If userSelectYes = True And bracingAtBmBotFLG = False Then
                If def1SidedConnStiff = StiffStyle.Partial_Depth Then
                    'topSTP = IIf(Left_MFconn, StiffStyle.PS_LeftFLG, StiffStyle.PS_RightFLG)
                    topSTP = StiffStyle.Full_Depth
                    botSTP = IIf(Left_MFconn, StiffStyle.PS_LeftFLG, StiffStyle.PS_RightFLG)
                Else
                    topSTP = StiffStyle.Full_Depth
                    botSTP = StiffStyle.Full_Depth
                End If
            ElseIf userSelectYes = True And bracingAtBmBotFLG = True Then
                topSTP = StiffStyle.Full_Depth
                botSTP = StiffStyle.Full_Depth
            ElseIf userSelectYes = False And bracingAtBmBotFLG = True Then
                If topOfCol = False Or hasYL68_Left Or hasYL68_Right Then
                    topSTP = StiffStyle.Full_Depth
                End If
                botSTP = StiffStyle.Full_Depth
            ElseIf userSelectYes = False And bracingAtBmBotFLG = False Then
                If topOfCol = False Or hasYL68_Left Or hasYL68_Right Then
                    If def1SidedConnStiff = StiffStyle.Partial_Depth Then
                        'topSTP = IIf(Left_MFconn, StiffStyle.PS_LeftFLG, StiffStyle.PS_RightFLG)
                        topSTP = StiffStyle.Full_Depth
                    Else
                        topSTP = StiffStyle.Full_Depth
                    End If
                End If
            End If
        End If
    End Sub

    Public Function Find_Stiff_Thickness(minCal_Stiff_Thk As Double, defThickInput As Double, minSTPThk_perLink As Double, stiff_use As Double) As Double
        Return MaxAll(minCal_Stiff_Thk, defThickInput, minSTPThk_perLink, stiff_use)
    End Function
    Public Function Find_MinThk_perLink(lk As String) As Double
        If lk Like "YL8*" Then : Return 0.5 '0.625
        ElseIf lk Like "YL6*" Then : Return 0.375 '0.5
        ElseIf lk Like "YL4*" Then : Return 0.25 '0.375
        Else : Return 0
        End If
    End Function

    Public Function Find_Dblr_Thickness(col As clsSSTColumn, defThick As Double) As Double
        Dim Left_MFconn, Right_MFconn As Boolean
        Try
            Left_MFconn = col.BmLeft.IsAsignEndJ_K
        Catch ex As Exception
        End Try
        Try
            Right_MFconn = col.BmRight.IsAsignEndI_K
        Catch ex As Exception
        End Try
        If Left_MFconn Or Right_MFconn Then
            If col.DoublerPLThk < 0.1 And col.ProvideDblPL = "YES" Then
                Return MaxAll(col.DoublerPLThk_Geometry, defThick)
            ElseIf col.DoublerPLThk > 0 Then
                Return MaxAll(col.DoublerPLThk, defThick)
            Else
                Return 0
            End If
        Else
            Return 0
        End If
    End Function
    Public Function ExportToFile(lst As List(Of String), fileName As String) As Boolean
        Try
            Dim wr As New StreamWriter(fileName, False)
            'write rows to excel file
            For i As Integer = 0 To (lst.Count) - 1
                wr.WriteLine(lst(i))
            Next
            'close file
            wr.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ReadTXT_Resrc(strFileName As String) As String
        Try
            Dim codeFilename As String = Assembly.GetExecutingAssembly().GetName.Name & "." & strFileName
            Dim ass = Assembly.GetExecutingAssembly()
            Dim str As String = ""
            Dim ret As String = ""
            '
            Using stream As Stream = ass.GetManifestResourceStream(codeFilename)
                Using reader = New StreamReader(stream)
                    Do
                        str = reader.ReadLine
                        ret += "|" & str
                    Loop Until str Is Nothing
                    reader.Close()
                End Using
            End Using
            '
            Return ret
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function ReadTXT_Resrc(ByRef curJob As clsMRSJob, strFileName As String, lineID As String, ByRef lst As List(Of String)) As Long
        Try
            Dim codeFilename As String = Assembly.GetExecutingAssembly().GetName.Name & "." & strFileName
            Dim ass = Assembly.GetExecutingAssembly()
            Dim str As String = ""
            Dim ret As String = ""
            '
            Using stream As Stream = ass.GetManifestResourceStream(codeFilename)
                Using reader = New StreamReader(stream)
                    Do
                        str = reader.ReadLine
                        If str.StartsWith("_") Then
                            If str = "_05/2019" Then
                                str = Format(Today(), "MM/dd/yyyy")
                            ElseIf str = "_ES#" Then
                                str = IIf(curJob.JobID <> "", curJob.JobID, "ES#")
                            ElseIf str = "_YL-LnID" Then
                                str = "YL-Ln" & lineID
                            ElseIf str = "_FRAME_LINEID" Then
                                str = "FRAME LINE " & lineID
                            ElseIf str = "_B.C." Then
                                str = IIf(curJob.EngName <> "", curJob.EngName, " ")
                            ElseIf str = "_EOR" Then
                                str = IIf(curJob.EOR <> "", curJob.EOR, " ")
                            ElseIf str = "_01/2019" Then
                                str = " "
                            End If
                        End If
                        lst.Add(str)
                    Loop Until str Is Nothing
                    reader.Close()
                End Using
            End Using
            '
            For i = lst.Count - 1 To 0 Step -1
                If lst(lst.Count - 1) Is Nothing Then
                    lst.RemoveAt(i)
                Else
                    Exit For
                End If
            Next
            '
            Return 0
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Public Function ReadTXT_File(strFile As String) As String
        Try
            Dim reader As New StreamReader(strFile, Encoding.Default)
            Dim str As String = ""
            Dim ret As String = ""
            Do
                str = reader.ReadLine
                ret += "|" & str
            Loop Until str Is Nothing
            reader.Close()
            Return ret
        Catch ex As Exception
            Return ""
        End Try
    End Function

    'Private Function Read_SSTLinks() As SSTLinks
    '    Dim codeFilename As String = Reflection.Assembly.GetExecutingAssembly().GetName.Name & ".xml_SSTLinks.xml"
    '    Dim XmlSer As New System.Xml.Serialization.XmlSerializer(GetType(SSTLinks))
    '    Dim Reader As System.IO.Stream = GetType(SSTLinks).Assembly.GetManifestResourceStream(codeFilename)
    '    Try
    '        Dim code As SSTLinks = DirectCast(XmlSer.Deserialize(Reader), SSTLinks)
    '        Return code
    '    Finally
    '        Reader.Close()
    '    End Try
    'End Function

    Public Function Find_CapPL_Thicknees(ColSection As String) As Double
        If ColSection Like "W8*" Or ColSection = "W10X30" Or ColSection = "W12X35" Then
            Return 0.375
        Else
            Return 0.5
        End If
    End Function

    Public Function Dec2Frac(ByVal dbl As Decimal) As String
        dbl = Ceiling(dbl, 1 / 16)
        '
        If dbl = 0 Then Return NA
        '
        Dim int As Integer = Math.Truncate(dbl)
        Dim dec As Double = dbl - int
        Dim strInt As String
        Dim strFraction As String
        If int = 0 Then
            strInt = ""
        Else
            strInt = int.ToString & " "
        End If
        If dec = 0 Then
            Return strInt
        Else
            dec *= 16
            If (dec Mod 8) = 0 Then
                strFraction = strInt & (dec / 8) & "/2"
            ElseIf (dec Mod 4) = 0 Then
                strFraction = strInt & (dec / 4) & "/4"
            ElseIf (dec Mod 2) = 0 Then
                strFraction = strInt & (dec / 2) & "/8"
            Else
                strFraction = strInt & (dec / 1) & "/16"
            End If
            '
            Return strFraction
        End If
    End Function

    Public Function RoundAngle(ang As Double) As Double
        If ang >= 180 Then
            ang -= 180
        ElseIf ang < 0 Then
            ang += 180
        End If
        Return ang
    End Function

    Public Function FindFactor(iFactor As IFactor, isStrength As Boolean, LTyp As LoadType) As Double
        Dim ret As Double = 0
        Select Case iFactor
            Case IFactor.Low
                If LTyp = LoadType.Snow Then : ret = IIf(isStrength, 0.8, 0.9)
                ElseIf LTyp = LoadType.Wind Then : ret = IIf(isStrength, 0.8, 0.75)
                ElseIf LTyp = LoadType.Earthquake Then : ret = 0.8
                End If
            Case IFactor.Normal
                If LTyp = LoadType.Snow Then : ret = IIf(isStrength, 1, 0.9)
                ElseIf LTyp = LoadType.Wind Then : ret = IIf(isStrength, 1, 0.75)
                ElseIf LTyp = LoadType.Earthquake Then : ret = 1
                End If
            Case IFactor.High
                If LTyp = LoadType.Snow Then : ret = IIf(isStrength, 1.15, 0.9)
                ElseIf LTyp = LoadType.Wind Then : ret = IIf(isStrength, 1.15, 0.75)
                ElseIf LTyp = LoadType.Earthquake Then : ret = 1.3
                End If
            Case IFactor.Post_Disaster
                If LTyp = LoadType.Snow Then : ret = IIf(isStrength, 1.25, 0.9)
                ElseIf LTyp = LoadType.Wind Then : ret = IIf(isStrength, 1.25, 0.75)
                ElseIf LTyp = LoadType.Earthquake Then : ret = 1.5
                End If
        End Select
        Return ret
    End Function

    Public Function Cal_Krot(LKInfo As LinkInfo, Dbm As Double) As Double
        Dim AF11 = LKInfo.Py_link / LKInfo.Keff
        Dim AE11 As Double = LKInfo.Py_link * (LKInfo.tstem + Dbm) 'Py_link or Pr_link?
        Dim AG11 As Double = AF11 / ((LKInfo.tstem + Dbm) / 2)
        Return Math.Round(AE11 / AG11, 0)
        '
    End Function
    '
    Function Find_WasherInfo(dia As Double, isB As Boolean) As Double
        If dia = 0.5 Then : Return IIf(isB, 1.0625, 0.4375)
        ElseIf dia = 0.625 Then : Return IIf(isB, 1.3125, 0.5625)
        ElseIf dia = 0.75 Then : Return IIf(isB, 1.46875, 0.65625)
        ElseIf dia = 0.875 Then : Return IIf(isB, 1.75, 0.78125)
        ElseIf dia = 1 Then : Return IIf(isB, 2, 0.875)
        ElseIf dia = 1.125 Then : Return IIf(isB, 2.25, 1)
        ElseIf dia = 1.25 Then : Return IIf(isB, 2.5, 1.09375)
        ElseIf dia = 1.375 Then : Return IIf(isB, 2.75, 1.21875)
        ElseIf dia = 1.5 Then : Return IIf(isB, 3, 1.3125)
        End If
        Return 999
    End Function

End Module
