﻿<Serializable()> Public Class DXFJob
    Public JobID As String
    Public JobName As String
    Public usedSections As New List(Of SecInfo)
    Public lstElevs As New List(Of sstElev)
    Public lstSlabs As New List(Of sstSlabDepth)
    Public isFixedbase As Boolean = True
    Public BaseExtension As Double = 24
    Public isFullDepth_1sideConn As Boolean = False
    Sub New()
    End Sub
    Sub New(_JobID$, _JobName$, _usedSections As List(Of SecInfo), _lstElevs As List(Of sstElev), _lstSlabs As List(Of sstSlabDepth),
            _isFixedbase As Boolean, _BaseExtension#, _isFullDepth_1sideConn As Boolean)
        JobID = _JobID
        JobName = _JobName
        usedSections = _usedSections
        lstElevs = _lstElevs
        lstSlabs = _lstSlabs
        isFixedbase = _isFixedbase
        BaseExtension = _BaseExtension
        isFullDepth_1sideConn = _isFullDepth_1sideConn
    End Sub

End Class
<Serializable()> Public Class sstElev
    Public elevID As String = "Elev 1"
    Public lstBeams As New List(Of sstBeam)
    Public lstColumns As New List(Of sstColumn)
    Public lstWelds As New List(Of sstWeld)

    Sub New()
    End Sub

    Sub New(_elevID$, _lstBeams As List(Of sstBeam), _lstColumns As List(Of sstColumn), _lstWelds As List(Of sstWeld))
        elevID = _elevID
        lstBeams = _lstBeams
        lstColumns = _lstColumns
        lstWelds = _lstWelds
    End Sub

End Class
<Serializable()> Public Class SecInfo
    Public Size As String = "W14X38"
    Public d As Double = 14.125
    Public d_det As Double = 14
    Public bf As Double = 6.02
    Public bf_det As Double = 6
    Public tw As Double = 0.51
    Public tw_det As Double = 0.5
    Public tf As Double = 0.742
    Public tf_det As Double = 0.75
    Sub New()
    End Sub

    Sub New(_Size$, _d#, _d_det#, _bf#, _bf_det#, _tw#, _tw_det#, _tf#, _tf_det#)
        Size = _Size
        d = _d
        d_det = _d_det
        bf = _bf
        tw = _tw
        tw_det = _tw_det
        tf = _tf
        tf_det = _tf_det
    End Sub

End Class

<Serializable()> Public Class sstBeam
    Public uName As String = 1
    Public Size As String = "W12X35"
    Public GridID As String = "Grid A"
    Public StyID As String = "Story 1"
    Public Lcc As Double = 120
    Public LevelHeight As Double = 132
    Public iPt As String = 1
    Public jPt As String = 2
    Public ST_thk As Double = 0.875
    Public ST_boltSize As Double = 1
    Public ST_boltGrade As String = "A325"
    Public ST_NoHorizBolt As Integer = 2
    Public sX As Double = 0
    Public sY As Double = 120
    Public YL_left As String = "YL6-4"
    Public YL_right As String = "YL6-5.5"
    Sub New()
    End Sub

    Sub New(_uName$, _Size$, _GridID$, _StyID$, _Lcc@, _LevelHeight@, _iPt$, _jPt$, _ST_thk#, _ST_boltSize#, _ST_boltGrade$,
            _ST_NoHorizBolt%, _sX#, _sY#, _YL_left$, _YL_right$)
        uName = _uName
        Size = _Size
        GridID = _GridID
        StyID = _StyID
        Lcc = _Lcc
        LevelHeight = _LevelHeight
        iPt = _iPt
        jPt = _jPt
        ST_thk = _ST_thk
        ST_boltSize = _ST_boltSize
        ST_boltGrade = _ST_boltGrade
        ST_NoHorizBolt = _ST_NoHorizBolt
        sX = _sX
        sY = _sY
        YL_left = _YL_left
        YL_right = _YL_right
    End Sub
End Class

<Serializable()> Public Class sstColumn
    Public uName As String = 11
    Public Size As String = "W14X38"
    Public GridID As String = "Grid A"
    Public StyID As String = "Story 1"
    Public Hcc As Double = 120
    Public iPt As String = 5
    Public jPt As String = 6
    Public doubler_thk As Double = 0.75
    Public stiff_thk As Double = 0.625
    Public rotDeg As Double = 1
    Public Bm_id_left As String = 1
    Public Bm_id_right As String = 2
    Public sX As Double = 0
    Public sY As Double = 0
    Public isBraceAtBmBotFlg As Boolean = True

    Sub New()
    End Sub

    Sub New(_uName$, _Size$, _GridID$, _StyID$, _Hcc#, _iPt$, _jPt$, _doubler_thk#, _stiff_thk#, _rotDeg#, _Bm_id_left$, _Bm_id_right$,
            _sX#, _sY#, _isBraceAtBmBotFlg As Boolean)
        uName = _uName
        Size = _Size
        GridID = _GridID
        StyID = _StyID
        Hcc = _Hcc
        iPt = _iPt
        jPt = _jPt
        doubler_thk = _doubler_thk
        stiff_thk = _stiff_thk
        rotDeg = _rotDeg
        Bm_id_left = _Bm_id_left
        Bm_id_right = _Bm_id_right
        sX = _sX
        sY = _sY
        isBraceAtBmBotFlg = _isBraceAtBmBotFlg
    End Sub
End Class

<Serializable()> Public Class sstWeld
    Public GridID As String = "Grid A"
    Public StyID As String = "Story 1"
    Public Col_uName As String = "11"
    Public Col_Size As String = "W14X38"
    Public Left_ST_Thk As String = "5/8"
    Public Right_ST_Thk As String = "3/4"
    Public Stiff_Thk As String = "3/4"
    Public DP_Thk As String = "3/4"
    Public W1A_Thk As String = "3/4"
    Public W1A_N_Sides As Integer = 2
    Public W1B_Thk As String = "3/4"
    Public W1B_N_Sides As Integer = 2

    Public W2_Thk As String = "3/4"
    Public W2_N_Sides As Integer = 2
    Public W3_Thk As String = "3/4"
    Public W3_N_Sides As Integer = 2
    Public W4_Thk As String = "3/4"
    Public W4_N_Sides As Integer = 2
    Public W4A_Thk As String = "3/4"
    Public W4A_N_Sides As Integer = 2
    Public W5_Thk As String = "3/4"
    Public W5_N_Sides As Integer = 2

    Sub New()
    End Sub
    Sub New(_GridID$, _StyID$, _Col_uName$, _Col_Size$, _Left_ST_Thk$, _Right_ST_Thk$, _Stiff_Thk$, _DP_Thk$,
            _W1A_Thk$, _W1A_N_Sides%, _W1B_Thk$, _W1B_N_Sides%,
            _W2_Thk$, _W2_N_Sides%, _W3_Thk$, _W3_N_Sides%,
            _W4_Thk$, _W4_N_Sides%, _W4A_Thk$, _W4A_N_Sides%,
            _W5_Thk$, _W5_N_Sides%)
        GridID = _GridID
        StyID = _StyID
        Col_uName = _Col_uName
        Col_Size = _Col_Size
        Left_ST_Thk = _Left_ST_Thk
        Right_ST_Thk = _Right_ST_Thk
        Stiff_Thk = _Stiff_Thk
        DP_Thk = _DP_Thk
        W1A_Thk = _W1A_Thk
        W1A_N_Sides = _W1A_N_Sides
        W1B_Thk = _W1B_Thk
        W1B_N_Sides = _W1B_N_Sides

        W2_Thk = _W2_Thk
        W2_N_Sides = _W2_N_Sides
        W3_Thk = _W3_Thk
        W3_N_Sides = _W3_N_Sides
        W4_Thk = _W4_Thk
        W4_N_Sides = _W4_N_Sides
        W4A_Thk = _W4A_Thk
        W4A_N_Sides = _W4A_N_Sides
        W5_Thk = _W5_Thk
        W5_N_Sides = _W5_N_Sides
    End Sub
End Class
