﻿Imports YL_DXF_US_Lib

Public Class clsExportDXF
    Private strPath As String
    Private SelectedBeams As List(Of clsSSTBeam)
    Private SelectedColumns As List(Of clsSSTColumn)
    Private lstWeld As List(Of String)
    Private curJob As clsMRSJob
    '
    Public strStatus As String


    Sub New()
    End Sub
    Sub New(_job As clsMRSJob, _sPath As String, _SelectedBeams As List(Of clsSSTBeam), _SelectedColumns As List(Of clsSSTColumn),
            _lstWeld As List(Of String))
        curJob = _job
        strPath = _sPath
        ''SelectedFrames = _SelectedFrames
        SelectedBeams = _SelectedBeams
        SelectedColumns = _SelectedColumns
        lstWeld = _lstWeld
    End Sub

    Public Function ExportDXFFromExcel(_sPath As String, elev As String, Arr() As String, ArrWeld() As String,
                                       typicalFloorDepth As Double, roofFloorDepth As Double, baseExtension As Double,
                                       isFullDepth_1sidedConn As Boolean, isBracingAtBmBotFLG As Boolean, isOption2B As Boolean,
                                       isPlugWeld As Boolean, isFixedBase As Boolean, _isCan As Boolean, ArrSections() As String) As Boolean ', ArrWeld() As String
        If curJob Is Nothing Then
            curJob = New clsMRSJob(ArrSections)
        End If
        '
        If Arr Is Nothing Or ArrWeld Is Nothing Then
            MsgBox("Error: Invalid Data. Please check the inputs", MsgBoxStyle.Critical, toolName)
            Return False
        End If
        strPath = _sPath
        SelectedBeams = New List(Of clsSSTBeam)
        SelectedColumns = New List(Of clsSSTColumn)
        Dim Lcc, Hcc As Double
        '
        Dim tmpArr() As String
        For Each item In Arr
            tmpArr = item.Split(curJob.splChar)
            If tmpArr.Length < 20 Then Continue For

            Lcc = tmpArr(18) - tmpArr(16)
            Hcc = tmpArr(19) - tmpArr(17)
            '
            If String.Equals(tmpArr(2), "Beam", StringComparison.CurrentCultureIgnoreCase) Then
                SelectedBeams.Add(New clsSSTBeam(curJob, tmpArr(1), tmpArr(3), tmpArr(6), tmpArr(7), tmpArr(8), tmpArr(10), tmpArr(11), tmpArr(12), Lcc,
                                                 tmpArr(16), tmpArr(17), tmpArr(4), tmpArr(5), elev, "", 0))
            ElseIf String.Equals(tmpArr(2), "Column", StringComparison.CurrentCultureIgnoreCase) Then
                SelectedColumns.Add(New clsSSTColumn(curJob, tmpArr(1), tmpArr(3), tmpArr(6), tmpArr(7), Hcc, tmpArr(16), tmpArr(17), "1", elev, tmpArr(14) / 8, tmpArr(15) / 8, False, ""))
            End If
        Next
        '
        'Find EndI_bottomColumn
        'Find beam story
        Dim lst As New List(Of Double)
        Dim tmpVal As Double
        '
        For Each bm In SelectedBeams
            bm.EndI_ColBottom = SelectedColumns.Find(Function(x) x.Pt_J = bm.Pt_I)
            bm.EndJ_ColBottom = SelectedColumns.Find(Function(x) x.Pt_J = bm.Pt_J)
            '
            bm.EndI_ColTop = SelectedColumns.Find(Function(x) x.Pt_I = bm.Pt_I)
            bm.EndJ_ColTop = SelectedColumns.Find(Function(x) x.Pt_I = bm.Pt_J)
            '
            tmpVal = Math.Round(bm.sPt_Y, 2)
            If Not lst.Contains(tmpVal) Then
                lst.Add(tmpVal)
            End If
        Next
        'Determine beam on column
        Dim cnt As Integer
        cnt = 1
        Dim curX As Double
        SelectedColumns = SelectedColumns.OrderBy(Function(x) x.sPt_X).ToList
        curX = SelectedColumns(0).sPt_X
        For Each col In SelectedColumns
            col.BmLeft = SelectedBeams.Find(Function(x) x.Pt_J = col.Pt_J)
            col.BmRight = SelectedBeams.Find(Function(x) x.Pt_I = col.Pt_J)
            col.isBraceAtBmBotFKG = isBracingAtBmBotFLG
            '
            If col.sPt_X - curX > 10 Then
                cnt = cnt + 1
                curX = col.sPt_X
            End If
            '
            If IsNumeric(elev) Then
                Dim tmpChar As mChar = cnt
                col.GridID_LeftSide = tmpChar.ToString
            Else
                col.GridID_LeftSide = cnt
            End If
        Next
        'Initial level
        lst.Sort()
        curJob.SlabDepths.Add(New sstSlabDepth("Base", 0, baseExtension))
        For Each bm In SelectedBeams
            tmpVal = Math.Round(bm.sPt_Y, 2)
            bm.StyID = "Level " & (lst.IndexOf(tmpVal) + 1).ToString()
            bm.LevelHeight = tmpVal
            If curJob.SlabDepths.Find(Function(x) x.StoryID = bm.StyID) Is Nothing Then
                curJob.SlabDepths.Add(New sstSlabDepth(bm.StyID, tmpVal, typicalFloorDepth))
            End If
        Next
        '
        curJob.SlabDepths = curJob.SlabDepths.OrderByDescending(Function(x) x.StoryElevation).ToList
        curJob.SlabDepths(0).SlabDepth = roofFloorDepth
        '
        curJob.MaterialInfo.StiffPL_1SidedConn = IIf(isFullDepth_1sidedConn, StiffStyle.Full_Depth, StiffStyle.Partial_Depth)
        If isOption2B Then
            curJob.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_2B
        Else
            curJob.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_2A
        End If
        curJob.WeldingInfo.UsePlug_weld = isPlugWeld
        '
        curJob.isFixedBase = isFixedBase
        '
        lstWeld = ArrWeld.ToList
        '
        Export_Elevation_To_DXF()
    End Function

    Public Function ExportDXFFromRSS(dxfJob As DXFJob, savePath As String) As Boolean ', ArrWeld() As String
        If curJob Is Nothing Then
            curJob = New clsMRSJob(dxfJob.usedSections)
        End If
        '
        strPath = savePath
        SelectedBeams = New List(Of clsSSTBeam)
        SelectedColumns = New List(Of clsSSTColumn)
        Dim Lcc, Hcc As Double
        '
        For Each el In dxfJob.lstElevs
            For Each m In el.lstBeams
                SelectedBeams.Add(New clsSSTBeam(curJob, m.uName, m.Size, m.iPt, m.jPt, m.ST_thk, m.ST_boltSize * 8, m.ST_boltGrade,
                                                 m.ST_NoHorizBolt, m.Lcc, m.sX, m.sY, m.YL_left, m.YL_right, el.elevID, m.StyID, m.LevelHeight))
            Next
            For Each m In el.lstColumns
                SelectedColumns.Add(New clsSSTColumn(curJob, m.uName, m.Size, m.iPt, m.jPt, m.Hcc, m.sX, m.sY, m.GridID, el.elevID, m.doubler_thk, m.stiff_thk,
                                                     m.isBraceAtBmBotFlg, m.GridID)) ' tmpArr(1), tmpArr(3), tmpArr(6), tmpArr(7), Hcc, tmpArr(16), tmpArr(17), "1", elev, tmpArr(14) / 8, tmpArr(15) / 8))
            Next
            '
            lstWeld = New List(Of String)

            lstWeld.Add("Elev. ID" & curJob.splChar & "Grid ID" & curJob.splChar & "Story" & curJob.splChar & "Column Unique Name" & curJob.splChar &
                        "Column Size" & curJob.splChar & "Left ST Thk (in)" & curJob.splChar & "Right ST Thk (in)" & curJob.splChar & "STP Thk (in)" & curJob.splChar &
                        "DP Thk (in)" & curJob.splChar & "Left ST Weld W1A (in)" & curJob.splChar & "N_sides of W1A" & curJob.splChar & "Right ST Weld W1B (in)" & curJob.splChar &
                        "N_sides of W1B" & curJob.splChar & "STP to Col. Web W2 (in)" & curJob.splChar & "N_sides of W2" & curJob.splChar &
                        "STP to Col. Flg. W3 (in)" & curJob.splChar & "N_sides of W3" & curJob.splChar & "DP to Col. Web W4 (in)" & curJob.splChar & "N_sides of W4" & curJob.splChar &
                        "Plug weld Dia. W4A (in)" & curJob.splChar & "Plug weld Depth W4A (in)" & curJob.splChar & "DP to Col. Flg. W5 (in)" & curJob.splChar & "N_sides of W5")
            For Each m In el.lstWelds
                lstWeld.Add(el.elevID & curJob.splChar & m.GridID & curJob.splChar & m.StyID & curJob.splChar & m.Col_uName & curJob.splChar &
                            m.Col_Size & curJob.splChar & m.Left_ST_Thk & curJob.splChar & m.Right_ST_Thk & curJob.splChar & m.Stiff_Thk & curJob.splChar & m.DP_Thk &
                            curJob.splChar & m.W1A_Thk & curJob.splChar & m.W1A_N_Sides & curJob.splChar & m.W1B_Thk & curJob.splChar & m.W1B_N_Sides &
                            curJob.splChar & m.W2_Thk & curJob.splChar & m.W2_N_Sides & curJob.splChar & m.W3_Thk & curJob.splChar & m.W3_N_Sides &
                            curJob.splChar & m.W4A_Thk & curJob.splChar & m.W4_N_Sides & curJob.splChar & m.W4A_Thk & curJob.splChar & m.W4A_N_Sides &
                            curJob.splChar & m.W5_Thk & curJob.splChar & m.W5_N_Sides)
            Next
        Next
        'Find EndI_bottomColumn
        For Each bm In SelectedBeams
            bm.EndI_ColBottom = SelectedColumns.Find(Function(x) x.Pt_J = bm.Pt_I)
            bm.EndJ_ColBottom = SelectedColumns.Find(Function(x) x.Pt_J = bm.Pt_J)
            '
            bm.EndI_ColTop = SelectedColumns.Find(Function(x) x.Pt_I = bm.Pt_I)
            bm.EndJ_ColTop = SelectedColumns.Find(Function(x) x.Pt_I = bm.Pt_J)
        Next
        '
        For Each col In SelectedColumns
            col.BmLeft = SelectedBeams.Find(Function(x) x.Pt_J = col.Pt_J)
            col.BmRight = SelectedBeams.Find(Function(x) x.Pt_I = col.Pt_J)
        Next
        '
        curJob.SlabDepths = dxfJob.lstSlabs
        curJob.MaterialInfo.StiffPL_1SidedConn = IIf(dxfJob.isFullDepth_1sideConn, StiffStyle.Full_Depth, StiffStyle.Partial_Depth)
        '
        Export_Elevation_To_DXF()
    End Function

    Public Sub Export_Elevation_To_DXF()
        Dim lstAllEnts As New List(Of clsEntity)
        Dim cnt As Long = 1, ColID As Long = 1
        Dim CurX, CurY, curW As Double
        Dim BmTopNailerThk As Double
        Dim a_Shearplate As Double
        Dim Lc_BmVertical As Double
        Dim LNailerOffset As Double
        Dim AlReadyDrawCol As New List(Of String)
        ''Dim lstColumnSplice As New SortedList(Of String, Double)
        Dim Stiff_Thk As Double, DP_Thk As Double
        Dim topSTP, botSTP As StiffStyle
        Dim MinW, MaxW, MinH, MaxH, embededLen As Double
        Dim ret As Long, i As Long, bm As clsSSTBeam
        Dim sortedBeams As List(Of clsSSTBeam)
        Dim ElvID As String
        Dim tmpEndI_ColBottom, tmpEndJ_ColBottom As clsSSTColumn
        Dim basePt As clsPt2D
        Dim isNewFrame As Boolean = True
        Dim hasYL68 As Boolean

        ''PushDataToGjob(False)
        '
        Dim stlLinks As New SortedList(Of String, Integer)
        ' loop through all beam
        Try
            Dim lstColumnSplice = CreateColumnSplicelist()
            'sort by story then by bay
            sortedBeams = SelectedBeams.OrderBy(Function(x) x.ElevID).ThenBy(Function(x) x.sPt_Y).ThenBy(Function(x) x.sPt_X).ToList
            'adjust beam location for diagonal grid line
            Dim tmpBm As clsSSTBeam
            For Each bm In sortedBeams
                tmpBm = sortedBeams.Find(Function(x) x.Pt_I = bm.Pt_J)
                If Not tmpBm Is Nothing Then
                    tmpBm.sPt_X = bm.sPt_X + bm.Len_LCC
                End If
            Next
            '
            ElvID = sortedBeams(0).ElevID
            MinW = sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_X)
            MaxW = sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_X + x.Len_LCC)
            MinH = sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_Y - x.EndI_ColBottom.Len_Cal)
            MaxH = sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_Y)
            embededLen = curJob.SlabDepths(curJob.SlabDepths.Count - 1).SlabDepth
            '
            For i = 0 To sortedBeams.Count - 1
                bm = sortedBeams(i)
                If bm.ElevID <> ElvID Then
                    CreateLeaderWithText(lstAllEnts, New clsPt2D(sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_X + x.Wsec.D_det / 2) + 50, MinH), New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 55, MinH + 5), New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 60, MinH + 5), "T.O. FTG", AllignType.MiddleLeft)
                    lstAllEnts.AddRange(CreateLevelLine(New clsPt2D(MinW, MinH), New clsPt2D(sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_X + x.Len_LCC + x.EndJ_ColBottom.Wsec.D_det / 2 + 25), MinH), lyHidden, curJob.SlabDepths(curJob.SlabDepths.Count - 1).StoryID, Inches2Fraction(MinH)))
                    '
                    basePt = New clsPt2D(MinW, -100)
                    If Not stlLinks Is Nothing Then
                        lstAllEnts.AddRange(CreateTableLink(basePt, "0", stlLinks))
                    End If
                    If Not lstWeld Is Nothing Then
                        lstAllEnts.AddRange(CreateTableWeld(clsMRSJob.splChar, basePt, "0", lstWeld.FindAll(Function(x) (x Like "Elev.*" Or x Like ElvID & "*")).ToList))
                    End If
                    '
                    strStatus = Add_All_Enities_To_File(strPath, ElvID, MinW, MaxW, MinH, MaxH, lstAllEnts)
                    '
                    cnt = 1 : ColID = 1
                    AlReadyDrawCol.Clear()
                    lstAllEnts.Clear()
                    stlLinks.Clear()
                    '
                    ElvID = bm.ElevID
                    isNewFrame = True
                    '
                    MinW = sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_X)
                    MaxW = sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_X + x.Len_LCC)
                    MinH = sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_Y - x.EndI_ColBottom.Len_Cal)
                    MaxH = sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_Y)
                End If
                'Next
                BmTopNailerThk = curJob.SlabDepths.Find(Function(x) x.StoryID = bm.StyID).SlabDepth


                Dim topOffset As Double
                'For Each bm As clsSSTBeam In SelectedBeams
                If bm.LKInfo Is Nothing Then
                    bm.LKInfo = curJob.SSTLinks.Links(0)
                End If

                If bm.LKInfo.Size Like "YL8*" Then
                    a_Shearplate = 4
                    Lc_BmVertical = 5.5
                    LNailerOffset = bm.LKInfo.t_flange + bm.LKInfo.Lcol_side + bm.LKInfo.Ly_yield + bm.LKInfo.Lbm_side - bm.LKInfo.sb - a_Shearplate + 1.75 + 10 - 5.5
                    topOffset = (6.25 - BmTopNailerThk)
                ElseIf bm.LKInfo.Size Like "YL6*" Then
                    a_Shearplate = 3.5
                    Lc_BmVertical = 5
                    LNailerOffset = (bm.LKInfo.t_flange + bm.LKInfo.Lcol_side + bm.LKInfo.Ly_yield + bm.LKInfo.Lbm_side - bm.LKInfo.sb - a_Shearplate + 1.75 + 10 - 5.5)
                    topOffset = 5.5 - BmTopNailerThk
                ElseIf bm.LKInfo.Size Like "YL4*" Then
                    a_Shearplate = 2.75
                    Lc_BmVertical = 3.5
                    LNailerOffset = (bm.LKInfo.t_flange + bm.LKInfo.Lcol_side + bm.LKInfo.Ly_yield + bm.LKInfo.Lbm_side - bm.LKInfo.sb - a_Shearplate + 1.75 + 8 - 5.5)
                    topOffset = (3.5 - BmTopNailerThk)
                Else
                    a_Shearplate = 2.75
                    Lc_BmVertical = 3.5
                    LNailerOffset = 1.75
                End If
                'Calculation Beam information
                CurX = bm.sPt_X + If(bm.EndI_IsWeak, bm.EndI_ColBottom.Wsec.bf_det / 2, bm.EndI_ColBottom.Wsec.D_det / 2) + a_Shearplate - 1.75 : CurY = bm.sPt_Y - BmTopNailerThk - bm.Wsec.D_det / 2
                curW = bm.Len_LCC - If(bm.EndI_IsWeak, bm.EndI_ColBottom.Wsec.bf_det / 2, bm.EndI_ColBottom.Wsec.D_det / 2) - a_Shearplate + 1.75 -
                                    If(bm.EndJ_IsWeak, bm.EndJ_ColBottom.Wsec.bf_det / 2, bm.EndJ_ColBottom.Wsec.D_det / 2) - a_Shearplate + 1.75
                'Add Beam
                lstAllEnts.AddRange(CreateBeam("BM" & cnt, New clsPt2D(CurX, CurY), bm.Wsec.Size, bm.Wsec.D_det, bm.Wsec.tfdet, curW,
                                               1.75, Lc_BmVertical, LNailerOffset, BmTopNailerThk, bm.IsAsignEndI_K, bm.IsAsignEndJ_K))
                ' ''Link @beam
                'I_end link
                If bm.IsAsignEndI_K = True AndAlso bm.EndI_IsWeak = False Then
                    CurX = bm.sPt_X + bm.EndI_ColBottom.Wsec.D_det / 2 : CurY = bm.sPt_Y - BmTopNailerThk + bm.LKInfo.tstem / 2
                    lstAllEnts.AddRange(CreateLinks(New clsPt2D(CurX, CurY), bm.Wsec.tfdet, bm.EndI_ColBottom.Wsec.tfdet, bm.LKInfo.Size, LinkLoc.TopLeft))
                    CurX = bm.sPt_X + bm.EndI_ColBottom.Wsec.D_det / 2 : CurY = bm.sPt_Y - BmTopNailerThk - bm.Wsec.D_det - bm.LKInfo.tstem / 2
                    lstAllEnts.AddRange(CreateLinks(New clsPt2D(CurX, CurY), bm.Wsec.tfdet, bm.EndI_ColBottom.Wsec.tfdet, bm.LKInfo.Size, LinkLoc.BotLeft))
                    '
                    If Not stlLinks.ContainsKey(bm.LKInfo.Size) Then
                        stlLinks.Add(bm.LKInfo.Size, 1) 'change to kits=group of 2 links
                    Else
                        stlLinks(bm.LKInfo.Size) += 1
                    End If
                End If
                'J_End Link
                If bm.IsAsignEndJ_K = True AndAlso bm.EndJ_IsWeak = False Then
                    CurX = bm.sPt_X + bm.Len_LCC - bm.EndJ_ColBottom.Wsec.D_det / 2 : CurY = bm.sPt_Y - BmTopNailerThk + bm.LKInfo.tstem / 2
                    lstAllEnts.AddRange(CreateLinks(New clsPt2D(CurX, CurY), bm.Wsec.tfdet, bm.EndJ_ColBottom.Wsec.tfdet, bm.LKInfo.Size, LinkLoc.TopRight))
                    CurX = bm.sPt_X + bm.Len_LCC - bm.EndJ_ColBottom.Wsec.D_det / 2 : CurY = bm.sPt_Y - BmTopNailerThk - bm.Wsec.D_det - bm.LKInfo.tstem / 2
                    lstAllEnts.AddRange(CreateLinks(New clsPt2D(CurX, CurY), bm.Wsec.tfdet, bm.EndJ_ColBottom.Wsec.tfdet, bm.LKInfo.Size, LinkLoc.BotRight))
                    '
                    If Not stlLinks.ContainsKey(bm.LKInfo.Size) Then
                        stlLinks.Add(bm.LKInfo.Size, 1)
                    Else
                        stlLinks(bm.LKInfo.Size) += 1
                    End If
                End If
                'Add Shear Plate
                CurX = bm.sPt_X + If(bm.EndI_IsWeak, bm.EndI_ColBottom.Wsec.bf_det / 2, bm.EndI_ColBottom.Wsec.D_det / 2)
                CurY = bm.sPt_Y - BmTopNailerThk - bm.Wsec.D_det / 2
                curW = bm.Len_LCC - If(bm.EndI_IsWeak, bm.EndI_ColBottom.Wsec.bf_det / 2, bm.EndI_ColBottom.Wsec.D_det / 2) -
                                    If(bm.EndJ_IsWeak, bm.EndJ_ColBottom.Wsec.bf_det / 2, bm.EndJ_ColBottom.Wsec.D_det / 2)
                Dim shearW_Left, shearW_Right, ShearH, SlotLength As Double
                ShearH = (bm.Sheartab.stBolt.n_Vbolts_SST - 1) * bm.Sheartab.stBolt.Svert + 2 * bm.Sheartab.stBolt.Lv_sp
                'ShearH *= unitFactor
                'shearW_Left = a_Shearplate + IIf(bm.IsAsignEndI_K, (bm.Sheartab.N_horzBolt - 1) * bm.Sheartab.stBolt.Shorz, 0) + bm.Sheartab.stBolt.Lh_sp
                'shearW_Right = a_Shearplate + IIf(bm.IsAsignEndJ_K, (bm.Sheartab.N_horzBolt - 1) * bm.Sheartab.stBolt.Shorz, 0) + bm.Sheartab.stBolt.Lh_sp
                shearW_Left = a_Shearplate + (bm.Sheartab.N_horzBolt - 1) * bm.Sheartab.stBolt.Shorz + bm.Sheartab.stBolt.Lh_sp
                'shearW_Left *= unitFactor
                shearW_Right = a_Shearplate + (bm.Sheartab.N_horzBolt - 1) * bm.Sheartab.stBolt.Shorz + bm.Sheartab.stBolt.Lh_sp
                'shearW_Right *= unitFactor
                Dim SP_boltsize As Double = bm.Sheartab.bSize / 8 ' Math.Max(bm.Sheartab.stBolt.db_sp_default, bm.Sheartab.bSize / 8)
                ''SP_boltsize *= unitFactor
                If SP_boltsize = 1.5625 Then
                    SlotLength = 2 ' 
                ElseIf SP_boltsize = 1.3125 Or SP_boltsize = 1.4375 Then
                    SlotLength = 1.75 ' 
                Else
                    SlotLength = 1.5 ' 
                End If
                lstAllEnts.AddRange(CreateShearPlate(New clsPt2D(CurX, CurY), bm.Sheartab.Thk / 16, curW, shearW_Left, shearW_Right, ShearH, a_Shearplate, (SP_boltsize + 1 / 16),
                                                     SlotLength, bm.Sheartab.stBolt.Svert, bm.Sheartab.stBolt.Shorz, bm.Sheartab.stBolt.n_Vbolts_SST,
                                                     bm.Sheartab.N_horzBolt, GetBoltPrefix(bm.Sheartab.bType), bm.IsAsignEndI_K, bm.IsAsignEndJ_K))
                cnt += 1
                ' add dim X centerline
                If bm.EndI_ColTop Is Nothing Then
                    CurX = bm.sPt_X
                    CurY = bm.EndI_ColBottom.sPt_Y + bm.EndI_ColBottom.Len_Cal
                    lstAllEnts.AddRange(CreateDim(New clsPt2D(CurX, MinH - embededLen), New clsPt2D(CurX + bm.Len_LCC, MinH - embededLen), 15 + embededLen, 0))
                End If
                'add column
                Dim bottom_offset, offsetAbove, ColLength As Double
                Dim botcut, topcut As Boolean
                Dim curH As Double
                'Dim HLeftNailerext As Double, HRightNailerExt As Double
                Dim ColumnLoc As ColumnLoc
                Dim LeftBeamDepth, RightBeamDepth As Double
                Dim topCapPL As Double = 999999999999
                'Left column
                If Not AlReadyDrawCol.Contains(bm.EndI_ColBottom._uName) Then
                    ColLength = 0
                    LeftBeamDepth = 0
                    RightBeamDepth = 0
                    ret = ChangeColumnPoint(lstColumnSplice, bm.EndI_ColBottom, bottom_offset, offsetAbove, botcut, topcut, ColLength)
                    CurX = bm.sPt_X
                    '
                    CurY = bm.EndI_ColBottom.sPt_Y + bottom_offset
                    curH = bm.EndI_ColBottom.Len_Cal '- bottom_offset + TopOffset
                    'Draw column to stop and not extend to top of slab at the column
                    If bm.EndI_ColTop Is Nothing Then
                        offsetAbove = topOffset
                        topCapPL = CurY + curH - bottom_offset + offsetAbove
                    End If
                    lstAllEnts.AddRange(CreateColumn("COL" & ColID, New clsPt2D(CurX, CurY), bm.EndI_ColBottom.Wsec.Size, bm.EndI_IsWeak, ColLength,
                                                     bm.EndI_ColBottom.Wsec.D_det, bm.EndI_ColBottom.Wsec.bf_det, bm.EndI_ColBottom.Wsec.tfdet,
                                                     bm.EndI_ColBottom.Wsec.tw, curH, bottom_offset, offsetAbove, botcut, topcut, bm.EndI_ColTop Is Nothing))
                    If botcut = True Then ColID += 1
                    AlReadyDrawCol.Add(New String(bm.EndI_ColBottom._uName))
                    'determind column location
                    tmpEndI_ColBottom = SelectedColumns.Find(Function(x) x._uName = bm.EndI_ColBottom._uName)
                    '
                    If tmpEndI_ColBottom.BmLeft Is Nothing Then
                        ColumnLoc = IIf(bm.EndI_ColTop Is Nothing, ColumnLoc.TopLeft, ColumnLoc.BotLeft)
                        RightBeamDepth = bm.Wsec.D_det
                        LeftBeamDepth = RightBeamDepth
                    ElseIf tmpEndI_ColBottom.BmRight Is Nothing Then
                        ColumnLoc = IIf(bm.EndI_ColTop Is Nothing, ColumnLoc.TopRight, ColumnLoc.BotRight)
                        LeftBeamDepth = bm.Wsec.D_det
                        RightBeamDepth = LeftBeamDepth
                    Else
                        ColumnLoc = IIf(bm.EndI_ColTop Is Nothing, ColumnLoc.TopCenter, ColumnLoc.BotCenter)
                        If tmpEndI_ColBottom.BmLeft.IsAsignEndJ_K And tmpEndI_ColBottom.BmRight.IsAsignEndI_K Then
                            'YL at both ends
                            LeftBeamDepth = tmpEndI_ColBottom.BmLeft.Wsec.D_det
                            RightBeamDepth = tmpEndI_ColBottom.BmRight.Wsec.D_det
                        ElseIf tmpEndI_ColBottom.BmLeft.IsAsignEndJ_K = False And tmpEndI_ColBottom.BmRight.IsAsignEndI_K Then
                            RightBeamDepth = tmpEndI_ColBottom.BmRight.Wsec.D_det
                            LeftBeamDepth = RightBeamDepth 'avoid diagonal stiff.
                        ElseIf tmpEndI_ColBottom.BmLeft.IsAsignEndJ_K And tmpEndI_ColBottom.BmRight.IsAsignEndI_K = False Then
                            LeftBeamDepth = tmpEndI_ColBottom.BmLeft.Wsec.D_det
                            RightBeamDepth = LeftBeamDepth 'avoid diagonal stiff.
                        End If
                    End If
                    'Create Grid
                    If bm.EndI_ColTop Is Nothing Then
                        CurX = bm.sPt_X
                        CurY = bm.EndI_ColBottom.sPt_Y + bm.EndI_ColBottom.Len_Cal
                        lstAllEnts.AddRange(CreateGrid(New clsPt2D(CurX, CurY), New clsPt2D(CurX, CurY + 20 + bubbleDia / 2), tmpEndI_ColBottom.ElevID, tmpEndI_ColBottom.GridID_LeftSide))
                    End If
                    'Add dim Y
                    If tmpEndI_ColBottom.BmLeft Is Nothing Then
                        CurX = bm.sPt_X
                        CurY = bm.EndI_ColBottom.sPt_Y
                        'Add dim for embeded part of column
                        If isNewFrame Then
                            lstAllEnts.AddRange(CreateDim(New clsPt2D(MinW, CurY - embededLen), New clsPt2D(MinW, CurY), -30, 90))
                            '
                            isNewFrame = False
                        End If
                        '
                        lstAllEnts.AddRange(CreateDim(New clsPt2D(MinW, CurY), New clsPt2D(MinW, CurY + bm.EndI_ColBottom.Len_Cal), -30, 90))
                        'add slab leader
                        CreateLeaderWithText(lstAllEnts, New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 50, CurY + bm.EndI_ColBottom.Len_Cal),
                                             New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 55, CurY + bm.EndI_ColBottom.Len_Cal + 5),
                                             New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 60, CurY + bm.EndI_ColBottom.Len_Cal + 5), "T.O. SLAB", AllignType.MiddleLeft)
                    End If
                    'add stiffener
                    If bm.EndI_IsWeak = False Then
                        CurX = bm.sPt_X
                        CurY = bm.EndI_ColBottom.sPt_Y + bm.EndI_ColBottom.Len_Cal
                        '
                        Stiff_Thk = Find_Stiff_Thickness(tmpEndI_ColBottom.StiffMinThk, curJob.MaterialInfo.StiffPL_DefThk, tmpEndI_ColBottom.StiffMinThk_perLink, tmpEndI_ColBottom.Stiff_use) ' Math.Max(tmpEndI_ColBottom.StiffMinThk, curJob.MaterialInfo.StiffPL_DefThk)
                        Find_STP_Style(topSTP, botSTP, tmpEndI_ColBottom, ColumnLoc = ColumnLoc.TopLeft Or ColumnLoc = ColumnLoc.TopCenter Or ColumnLoc = ColumnLoc.TopRight,
                                       curJob.MaterialInfo.StiffPL_1SidedConn)
                        DP_Thk = Find_Dblr_Thickness(tmpEndI_ColBottom, curJob.MaterialInfo.DoublerPL_DefThk)
                        '
                        lstAllEnts.AddRange(CreateStiff_Doubler_Plates(New clsPt2D(CurX, CurY), bm.EndI_ColBottom.Wsec.D_det, bm.EndI_ColBottom.Wsec.tfdet, LeftBeamDepth,
                                                                       RightBeamDepth, bm.LKInfo.tstem, ColumnLoc, topCapPL, BmTopNailerThk, Stiff_Thk, topSTP, botSTP,
                                                                       DP_Thk, curJob.WeldingInfo.DoublerPL2ColWeb, curJob.WeldingInfo.UsePlug_weld,
                                                                       Find_CapPL_Thicknees(bm.EndI_ColBottom.Wsec.Size)))
                    End If
                End If
                'Right column
                If Not AlReadyDrawCol.Contains(bm.EndJ_ColBottom._uName) Then
                    ColLength = 0
                    LeftBeamDepth = 0
                    RightBeamDepth = 0
                    ret = ChangeColumnPoint(lstColumnSplice, bm.EndJ_ColBottom, bottom_offset, offsetAbove, botcut, topcut, ColLength)
                    CurX = bm.sPt_X + bm.Len_LCC
                    CurY = bm.EndJ_ColBottom.sPt_Y + bottom_offset
                    curH = bm.EndJ_ColBottom.Len_Cal ' - bottom_offset + TopOffset
                    'Draw column to stop and not extend to top of slab at the column
                    If bm.EndJ_ColTop Is Nothing Then
                        offsetAbove = topOffset
                        topCapPL = CurY + curH - bottom_offset + offsetAbove
                    End If
                    lstAllEnts.AddRange(CreateColumn("COL" & ColID, New clsPt2D(CurX, CurY), bm.EndJ_ColBottom.Wsec.Size, bm.EndJ_IsWeak, ColLength,
                                                     bm.EndJ_ColBottom.Wsec.D_det, bm.EndJ_ColBottom.Wsec.bf_det, bm.EndJ_ColBottom.Wsec.tfdet,
                                                     bm.EndJ_ColBottom.Wsec.tw, curH, bottom_offset, offsetAbove, botcut, topcut, bm.EndJ_ColTop Is Nothing))
                    If botcut = True Then ColID += 1
                    AlReadyDrawCol.Add(New String(bm.EndJ_ColBottom._uName))
                    'determind column location
                    tmpEndJ_ColBottom = SelectedColumns.Find(Function(x) x._uName = bm.EndJ_ColBottom._uName)
                    'Determine column locaytion and Adj. beam depth
                    If tmpEndJ_ColBottom.BmLeft Is Nothing Then
                        ColumnLoc = IIf(bm.EndJ_ColTop Is Nothing, ColumnLoc.TopLeft, ColumnLoc.BotLeft)
                        RightBeamDepth = bm.Wsec.D_det
                        LeftBeamDepth = RightBeamDepth
                    ElseIf tmpEndJ_ColBottom.BmRight Is Nothing Then
                        ColumnLoc = IIf(bm.EndJ_ColTop Is Nothing, ColumnLoc.TopRight, ColumnLoc.BotRight)
                        LeftBeamDepth = bm.Wsec.D_det
                        RightBeamDepth = LeftBeamDepth
                    Else
                        ColumnLoc = IIf(bm.EndJ_ColTop Is Nothing, ColumnLoc.TopCenter, ColumnLoc.BotCenter)
                        '
                        If tmpEndJ_ColBottom.BmLeft.IsAsignEndJ_K And tmpEndJ_ColBottom.BmRight.IsAsignEndI_K Then
                            'YL at both ends
                            LeftBeamDepth = tmpEndJ_ColBottom.BmLeft.Wsec.D_det
                            RightBeamDepth = tmpEndJ_ColBottom.BmRight.Wsec.D_det
                        ElseIf tmpEndJ_ColBottom.BmLeft.IsAsignEndJ_K = False And tmpEndJ_ColBottom.BmRight.IsAsignEndI_K Then
                            RightBeamDepth = tmpEndJ_ColBottom.BmRight.Wsec.D_det
                            LeftBeamDepth = RightBeamDepth 'avoid diagonal stiff.
                        ElseIf tmpEndJ_ColBottom.BmLeft.IsAsignEndJ_K And tmpEndJ_ColBottom.BmRight.IsAsignEndI_K = False Then
                            LeftBeamDepth = tmpEndJ_ColBottom.BmLeft.Wsec.D_det
                            RightBeamDepth = LeftBeamDepth 'avoid diagonal stiff.
                        End If
                    End If

                    'add nailer
                    'Add Grid
                    If bm.EndJ_ColTop Is Nothing Then
                        CurX = bm.sPt_X + bm.Len_LCC
                        CurY = bm.EndJ_ColBottom.sPt_Y + bm.EndJ_ColBottom.Len_Cal
                        lstAllEnts.AddRange(CreateGrid(New clsPt2D(CurX, CurY), New clsPt2D(CurX, CurY + 20 + bubbleDia / 2), tmpEndJ_ColBottom.ElevID, tmpEndJ_ColBottom.GridID_LeftSide))
                    End If
                    '
                    If tmpEndJ_ColBottom.BmRight Is Nothing Then
                        CurY = bm.EndJ_ColBottom.sPt_Y
                        'add line at top column
                        lstAllEnts.AddRange(CreateLevelLine(New clsPt2D(MinW, CurY + bm.EndJ_ColBottom.Len_Cal), New clsPt2D(MaxW + bm.EndJ_ColBottom.Wsec.D_det / 2 + 25, CurY + bm.EndJ_ColBottom.Len_Cal), lyHidden, bm.StyID, Inches2Fraction(bm.LevelHeight)))
                    End If
                    'add stiffener
                    If bm.EndJ_IsWeak = False Then
                        CurX = bm.sPt_X + bm.Len_LCC
                        CurY = bm.EndJ_ColBottom.sPt_Y + bm.EndJ_ColBottom.Len_Cal
                        '
                        Stiff_Thk = Find_Stiff_Thickness(tmpEndJ_ColBottom.StiffMinThk, curJob.MaterialInfo.StiffPL_DefThk, tmpEndJ_ColBottom.StiffMinThk_perLink, tmpEndJ_ColBottom.Stiff_use) ' Math.Max(tmpEndJ_ColBottom.StiffMinThk, curJob.MaterialInfo.StiffPL_DefThk)
                        Find_STP_Style(topSTP, botSTP, tmpEndJ_ColBottom, ColumnLoc = ColumnLoc.TopLeft Or ColumnLoc = ColumnLoc.TopCenter Or ColumnLoc = ColumnLoc.TopRight, curJob.MaterialInfo.StiffPL_1SidedConn)
                        DP_Thk = Find_Dblr_Thickness(tmpEndJ_ColBottom, curJob.MaterialInfo.DoublerPL_DefThk)
                        '
                        lstAllEnts.AddRange(CreateStiff_Doubler_Plates(New clsPt2D(CurX, CurY), bm.EndJ_ColBottom.Wsec.D_det, bm.EndJ_ColBottom.Wsec.tfdet, LeftBeamDepth,
                                                                       RightBeamDepth, bm.LKInfo.tstem, ColumnLoc, topCapPL, BmTopNailerThk, Stiff_Thk, topSTP, botSTP,
                                                                       DP_Thk, curJob.WeldingInfo.DoublerPL2ColWeb, curJob.WeldingInfo.UsePlug_weld,
                                                                       Find_CapPL_Thicknees(bm.EndJ_ColBottom.Wsec.Size)))
                    End If
                End If
            Next
            'Export end frame
            MinW = sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_X)
            MaxW = sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_X + x.Len_LCC)
            MinH = sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_Y - x.EndI_ColBottom.Len_Cal)
            MaxH = sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_Y)
            '
            CreateLeaderWithText(lstAllEnts, New clsPt2D(sortedBeams.Where(Function(x) x.ElevID = ElvID).Min(Function(x) x.sPt_X + x.Wsec.D_det / 2) + 50, MinH),
                                 New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 55, MinH + 5), New clsPt2D(MinW + bm.EndI_ColBottom.Wsec.D_det / 2 + 60, MinH + 5),
                                 "T.O. FTG. (" & IIf(curJob.isFixedBase, "FIXED BASE)", "PINNED BASE)"), AllignType.MiddleLeft)
            lstAllEnts.AddRange(CreateLevelLine(New clsPt2D(MinW, MinH), New clsPt2D(sortedBeams.Where(Function(x) x.ElevID = ElvID).Max(Function(x) x.sPt_X + x.Len_LCC + x.EndJ_ColBottom.Wsec.D_det / 2 + 25), MinH),
                                                lyHidden, curJob.SlabDepths(curJob.SlabDepths.Count - 1).StoryID, Inches2Fraction(MinH)))
            '
            basePt = New clsPt2D(MinW, -100)
            If Not stlLinks Is Nothing Then
                lstAllEnts.AddRange(CreateTableLink(basePt, "0", stlLinks))
            End If
            If Not lstWeld Is Nothing Then
                lstAllEnts.AddRange(CreateTableWeld(clsMRSJob.splChar, basePt, "0", lstWeld.FindAll(Function(x) (x Like "Elev.*" Or x Like ElvID & "*")).ToList))
            End If
            '
            strStatus = Add_All_Enities_To_File(strPath, ElvID, MinW, MaxW, MinH, MaxH, lstAllEnts)
        Catch ex As Exception
            ' MsgBox(ex.Message, MsgBoxStyle.Critical, "Yield-Link® Connection")
            MsgBox(ex.Message & ex.StackTrace, MsgBoxStyle.Critical, "Yield-Link® Connection")
        Finally
            tmpEndI_ColBottom = Nothing
            tmpEndJ_ColBottom = Nothing
            lstAllEnts = Nothing
            SelectedBeams = Nothing
            SelectedColumns = Nothing
            ''SelectedFrames = Nothing
        End Try
    End Sub

    Public Function ChangeColumnPoint(lstColumnSplice As SortedList(Of String, Double), col As clsSEFrame, ByRef Bottom_Offset As Double, ByRef OffsetAbove As Double,
                                      ByRef botcut As Boolean, ByRef topcut As Boolean, ByRef ColLength As Double) As Long
        Try
            Dim splicelst As New List(Of String)
            Dim botcolumn, topcolumn As clsSEFrame
            ''Bottom Column
            'botcolumn = SelectedFrames.Find(Function(x) x.isColumn AndAlso x.Pt_J = col.Pt_I)
            'topcolumn = SelectedFrames.Find(Function(x) x.isColumn AndAlso x.Pt_I = col.Pt_J)
            botcolumn = SelectedColumns.Find(Function(x) x.Pt_J = col.Pt_I)
            topcolumn = SelectedColumns.Find(Function(x) x.Pt_I = col.Pt_J)

            If lstColumnSplice.ContainsKey(col._uName) Then
                If botcolumn Is Nothing Then
                    Bottom_Offset = -curJob.SlabDepths(curJob.SlabDepths.Count - 1).SlabDepth
                    botcut = True
                    ColLength = lstColumnSplice(col._uName) - Bottom_Offset
                ElseIf lstColumnSplice(col._uName) <> 0 Then
                    Bottom_Offset = 48
                    botcut = True
                    ColLength = lstColumnSplice(col._uName)
                Else
                    Bottom_Offset = 0
                    botcut = False
                End If
            Else
                Bottom_Offset = 0
                botcut = False
            End If
            If topcolumn Is Nothing Then
                OffsetAbove = 0
                topcut = True
            Else
                If lstColumnSplice.ContainsKey(topcolumn._uName) Then
                    If lstColumnSplice(topcolumn._uName) <> 0 Then
                        OffsetAbove = 48
                        topcut = True
                    Else
                        OffsetAbove = 0
                        topcut = False
                    End If
                Else
                    OffsetAbove = 0
                    topcut = False
                End If
            End If

            Return 0
        Catch ex As Exception
            Return 1
        End Try
    End Function

    Private Function CreateColumnSplicelist() As SortedList(Of String, Double)
        ''Dim flag As New List(Of clsSEFrame)
        Dim lstColumnSplice As New SortedList(Of String, Double)
        '
        ''flag = SelectedFrames.Where(Function(x) x.isColumn = True).ToList
        Dim tmpCols = SelectedColumns
        '
        Dim CurrElv As String
        Dim lastCol As String
        Dim curCol As clsSEFrame
        CurrElv = tmpCols(0).ElevID
        Dim CurrColLength As Double
        Dim botcolumn, topcolumn, top_topcolumn As clsSEFrame
        For i As Integer = 0 To tmpCols.Count - 1
            botcolumn = SelectedColumns.Find(Function(x) x.Pt_J = tmpCols(i).Pt_I)
            topcolumn = SelectedColumns.Find(Function(x) x.Pt_I = tmpCols(i).Pt_J)
            '
            curCol = tmpCols(i)
            If botcolumn Is Nothing Then
                If Not lstColumnSplice.ContainsKey(tmpCols(i)._uName) Then lstColumnSplice.Add(tmpCols(i)._uName, 0)
                '
                lastCol = tmpCols(i)._uName
                CurrColLength = curCol.Len_Cal
                '
                Do While topcolumn IsNot Nothing
                    '
                    top_topcolumn = SelectedColumns.Find(Function(x) x.Pt_I = topcolumn.Pt_J)
                    '
                    If topcolumn.Wsec.Size <> curCol.Wsec.Size Or CurrColLength + topcolumn.Len_Cal + 48 > 480 Then
                        CurrColLength = CurrColLength + 48
                        lstColumnSplice(lastCol) = CurrColLength
                        If Not lstColumnSplice.ContainsKey(curCol._uName) Then lstColumnSplice.Add(curCol._uName, 0)
                        If Not lstColumnSplice.ContainsKey(topcolumn._uName) Then lstColumnSplice.Add(topcolumn._uName, 0)
                        lastCol = topcolumn._uName
                        CurrColLength = topcolumn.Len_Cal - 48
                    Else
                        CurrColLength = CurrColLength + topcolumn.Len_Cal
                    End If
                    curCol = topcolumn
                    topcolumn = SelectedColumns.Find(Function(x) x.Pt_I = curCol.Pt_J)
                Loop
                lstColumnSplice(lastCol) = CurrColLength
            End If
        Next
        tmpCols = Nothing
        '
        Return lstColumnSplice
    End Function

    Private Function Add_All_Enities_To_File(_etabPath As String, ElvID As String, MinW As Double, MaxW As Double, MinH As Double, MaxH As Double,
                          _lstAllEnts As List(Of clsEntity)) As String

        Try
            'add overal dim
            _lstAllEnts.AddRange(CreateDim(New clsPt2D(MinW, MaxH), New clsPt2D(MaxW, MaxH), 15, 0))
            _lstAllEnts.AddRange(CreateDim(New clsPt2D(MinW, MinH), New clsPt2D(MinW, MaxH), -47, 90))
            Dim dxfHelper As New clsDXFHelper()
            Dim fName, fPath As String

            fPath = IO.Path.Combine(_etabPath, "Elevation Drawings")
            If Not IO.Directory.Exists(fPath) Then
                IO.Directory.CreateDirectory(fPath)
            End If
            fName = IO.Path.Combine(fPath, "Elevation " & ElvID & ".dxf")
            Dim ret = dxfHelper.Add_AllEntities(_lstAllEnts, fName, ElvID, curJob.JobID, curJob.EngName, curJob.EOR)
            If ret = "" Then
                Process.Start(fPath)
                Return fName
            Else
                Return "Error: " & ret '.Split(". ")(0) & "."
            End If
        Catch ex As Exception
            Return "Error: " & ex.Message '.Split(". ")(0) & "."
        End Try
    End Function

    Private Function GetBoltPrefix(btype As bType) As String
        If btype = bType.A325X Then
            Return "B"
        ElseIf btype = bType.A490N Then
            Return "C"
        ElseIf btype = bType.A490X Then
            Return "D"
        Else
            Return "A"
        End If
    End Function

End Class
