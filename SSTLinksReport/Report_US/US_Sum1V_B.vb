﻿Imports GrapeCity.ActiveReports
Imports YieldLinkLib

Public Class US_Sum1V_B

    Sub New(_job As clsMRSJob)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.
        '
        lbJobName.Text = _job.Name
        lbJobID.Text = _job.JobID
        lbPrintedDate.Text = "Date Printed: " & _job.PrintedDate
        '
        If _job.DCRLimits.DriftLimitSelect = DriftLimit.Hx10 Then
            lbSeismicDriftLimit.Text = "0.01Hx"
        ElseIf _job.DCRLimits.DriftLimitSelect = DriftLimit.Hx15 Then
            lbSeismicDriftLimit.Text = "0.015Hx"
        ElseIf _job.DCRLimits.DriftLimitSelect = DriftLimit.Hx20 Then
            lbSeismicDriftLimit.Text = "0.02Hx"
        ElseIf _job.DCRLimits.DriftLimitSelect = DriftLimit.Hx25 Then
            lbSeismicDriftLimit.Text = "0.025Hx"
        Else
            lbSeismicDriftLimit.Text = "N/A"
        End If
        '
        'If _job.DCRLimits.WindDriftLimitSelect <> WDriftLimit.None Then
        lbWindDriftLimit.Text = "Hx/" & _job.DCRLimits.WindDriftLimitSelect.ToString.Remove(0, 2)
            'Else
            '    lbWindDriftLimit.Text = "N/A"
            'End If
            '
            lbILS1.Text = NumberFormat(_job.DCRLimits.tbf_check, -2)
        lbILS2.Text = NumberFormat(_job.DCRLimits.bf_check, -2)
        lbILS3.Text = NumberFormat(_job.DCRLimits.Lyield_check, -2)
        lbILS4.Text = NumberFormat(_job.DCRLimits.Panel_Zone_DCR, -2)
        lbILS5.Text = NumberFormat(_job.DCRLimits.Drift_check, -2)
        '
        lbBLC1.Text = NumberFormat(_job.DCRLimits.Beam_tf_DCR, -2)
        lbBLC2.Text = NumberFormat(_job.DCRLimits.Link_strength_DCR, -2)
        lbBLC3.Text = NumberFormat(_job.DCRLimits.Lyield_check, -2)
        lbBLC4.Text = NumberFormat(_job.DCRLimits.t_BRP_DCR, -2)
        lbBLC5.Text = NumberFormat(_job.DCRLimits.BRP_Bolt_DCR, -2)
        '
        lbCC1.Text = NumberFormat(_job.DCRLimits.SCWB_DCR, -2)
        lbCC2.Text = NumberFormat(_job.DCRLimits.Panel_Zone_DCR, -2)
        lbCC3.Text = NumberFormat(_job.DCRLimits.Column_Flange_DCR, -2)
        lbCC4.Text = NumberFormat(_job.DCRLimits.Stiffener_DCR, -2)
        '
        lbST1.Text = NumberFormat(_job.DCRLimits.Beam_Web_DCR, -2)
        lbST2.Text = NumberFormat(_job.DCRLimits.Shear_Plate_DCR, -2)
        lbST3.Text = NumberFormat(_job.DCRLimits.Bolt_DCR, -2)
        lbST4.Text = NumberFormat(_job.DCRLimits.Fillet_weld_DCR, -2)
        '
        If _job.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_1 Then
            lbWeld1_B.Text = "Doubler without continuity plates"
            '
            If _job.WeldingInfo.UsePlug_weld = False Then
                pic1.Image = New Bitmap(My.Resources.O1_No)
            Else
                pic1.Image = New Bitmap(My.Resources.O1_Yes)
            End If
        ElseIf _job.WeldingInfo.DoublerPL2ColWeb = WeldingOption.Option_2A Then
            lbWeld1_B.Text = "Doubler extended beyond continuity plates"
            '
            If _job.WeldingInfo.UsePlug_weld = False Then
                pic1.Image = New Bitmap(My.Resources.O2A_No)
            Else
                pic1.Image = New Bitmap(My.Resources.O2A_Yes)
            End If
        Else
            lbWeld1_B.Text = "Doubler placed between continuity plates"
            '
            If _job.WeldingInfo.UsePlug_weld = False Then
                pic1.Image = New Bitmap(My.Resources.O2B_No)
            Else
                pic1.Image = New Bitmap(My.Resources.O2B_Yes)
            End If
        End If
        '
        lbWeldWeb.Text = _job.WeldingInfo.DoublerPL2ColWeb.ToString
        lbWeldFLG.Text = _job.WeldingInfo.DoublerPL2ColFLG.ToString
        lbWeld3.Text = IIf(_job.WeldingInfo.UsePlug_weld, "YES", "NO")

        If _job.WeldingInfo.DoublerPL2ColFLG = WeldingOption.Option_2 Then
            lbWeld2_B.Text = "Fillet Weld"
        Else
            lbWeld2_B.Text = "CJP Weld"
        End If
    End Sub

End Class
