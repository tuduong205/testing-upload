﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_Detail_BLC
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_Detail_BLC))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S217_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label301 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label302 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.r21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S22_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label50 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label55 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label62 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label67 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label68 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S23_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label71 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label72 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S25_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S25_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label77 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label78 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S25_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label80 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S25_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S25_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label86 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S26_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label88 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S26_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label90 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S26_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S26_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label94 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label96 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label99 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label101 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label102 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label104 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label105 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label108 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label110 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label111 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label113 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label114 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label116 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label117 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label119 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label120 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label122 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label152 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label154 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label155 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label157 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label158 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label160 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label161 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label163 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S24_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S24_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label168 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label169 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S24_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label171 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label172 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label174 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label175 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label177 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label178 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S28_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label180 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label182 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label184 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label185 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label187 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label188 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label190 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label191 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label193 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label194 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label196 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label197 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label199 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label200 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label202 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label203 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S29_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label205 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS210 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label208 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label210 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label211 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label213 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label123 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label124 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label126 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label127 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label129 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label130 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label132 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label133 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label135 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label136 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label138 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label139 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label142 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label144 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label145 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label147 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label148 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label150 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label151 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label165 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label166 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label215 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label216 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label218 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label219 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label221 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label222 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label224 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label141 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label226 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label227 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label229 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label230 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label232 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label233 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label235 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label236 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label238 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label239 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label241 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label242 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label244 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label246 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label247 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label249 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label250 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label251 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label253 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label254 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label255 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label257 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label258 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label260 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS211 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label262 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label264 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label265 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label267 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label268 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label270 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label274 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label271 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label273 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label277 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label279 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label280 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label283 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label286 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label289 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label291 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label292 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label294 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label282 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label288 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label295 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label297 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label298 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S211_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label300 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label303 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label304 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label305 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label306 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label315 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label317 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label319 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.headerS28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label53 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label61 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label69 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label70 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label75 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label84 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label89 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label91 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label95 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label97 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label103 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label106 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label109 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label115 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S27_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label93 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label112 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label118 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label121 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label128 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label131 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_035 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label137 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label140 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S210_036 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label146 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label100 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label134 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label125 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture1 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape4 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S212_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S212_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label143 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label149 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S212_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label153 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label156 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S212_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label159 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S212_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label162 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label164 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S212_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label167 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape7 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S213_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label176 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label179 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label181 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label183 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label186 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label189 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label192 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label195 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label198 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label201 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label204 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label206 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label207 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label209 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label214 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label217 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label223 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label231 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label245 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label259 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label261 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label263 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label266 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label275 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label237 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label240 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label284 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label285 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label287 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S213_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label293 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape8 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape9 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape11 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape12 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S214_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS214 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label316 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label320 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label322 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label323 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label325 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label326 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label328 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label329 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label331 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label334 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label337 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label338 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label340 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label341 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label343 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label344 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label346 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label347 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label350 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label353 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label355 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label356 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label358 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label359 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label361 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label362 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label365 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label368 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label370 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label371 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label373 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label375 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label385 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label387 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label388 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label390 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape17 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape19 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S215_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS215 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label392 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label394 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label396 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label397 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label399 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label400 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label402 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label403 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label405 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label412 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label414 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label415 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label417 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label418 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label420 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label421 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label423 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label424 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label426 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label427 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label429 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label430 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label432 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label433 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label435 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label436 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label439 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label442 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label444 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S215_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label378 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape23 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape25 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S216_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label406 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label408 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label409 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label411 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label438 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label445 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label446 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label448 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS216 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label450 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label452 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label454 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label455 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label457 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label458 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label460 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label461 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label463 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label464 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label467 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label470 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label472 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label473 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label475 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label476 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label478 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label479 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label481 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label482 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label485 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label487 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label488 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label490 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label491 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label493 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label494 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label497 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label500 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label502 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label503 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label505 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label507 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label517 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label519 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label520 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label522 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label508 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label510 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label511 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label513 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label514 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label516 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label523 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label525 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label526 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label528 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label529 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label531 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label532 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label534 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape20 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape21 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape31 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label228 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label212 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label44 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label220 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label51 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label73 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label87 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label225 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line21 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line23 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line24 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line26 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.SUM_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label234 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label243 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line20 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line19 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line18 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line27 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label170 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label173 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S216_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label252 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label256 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape27 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label248 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label269 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label272 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label276 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label278 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label281 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label290 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label296 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label299 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label307 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S214_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape15 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label308 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label309 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label310 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label312 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label314 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label318 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label324 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label327 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label332 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label333 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label336 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label339 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label345 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label348 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S217_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label351 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label367 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label352 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.Shape29 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape10 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label311 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S21_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        CType(Me.S217_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_032, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_025, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_033, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_018, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_028, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_020, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_021, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_022, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_023, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_017, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label301, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label302, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.r21, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS21, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS22, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S22_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS23, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S23_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS25, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S25_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S25_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S25_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S25_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S25_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS26, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S26_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S26_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S26_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S26_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS27, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label152, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label155, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label157, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label161, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS24, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S24_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S24_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label168, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label169, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S24_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label171, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label172, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label174, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label175, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label177, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label178, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S28_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label180, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS29, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label182, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label184, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label185, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label187, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label188, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label190, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label194, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label197, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label200, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label202, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label203, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S29_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label205, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS210, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label208, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label210, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label211, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label213, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label126, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label129, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label132, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label138, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label139, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label142, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label145, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label147, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label150, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_017, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label165, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label166, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_018, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label215, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label216, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_019, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label218, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label219, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_020, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label221, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label222, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_025, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label224, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label226, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label227, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_021, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label229, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label230, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_022, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label232, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label233, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_023, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label235, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label236, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_024, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label238, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label239, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_026, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label241, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label242, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_027, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label244, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label246, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label247, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_029, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label249, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label250, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label251, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_030, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label253, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label254, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label255, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_031, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label257, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label258, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_032, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label260, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS211, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label262, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label264, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label265, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label267, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label268, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label270, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label274, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_019, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_024, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label277, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_025, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label280, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label283, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label286, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label289, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label291, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label292, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label294, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label282, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label288, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label295, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label297, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label298, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S211_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label300, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label303, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label304, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label305, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label306, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label315, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label317, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label319, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS28, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S27_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_033, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_034, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_035, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label137, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label140, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S210_036, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label134, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S212_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S212_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label143, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label149, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S212_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label153, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S212_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label159, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S212_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label162, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S212_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label167, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_018, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label176, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label181, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label183, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label186, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label189, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label192, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label195, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label198, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label201, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label204, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label206, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label207, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label209, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label214, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label217, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label223, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label231, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label245, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label259, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label261, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label263, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label266, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label275, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label237, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label240, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label284, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label285, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label287, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S213_017, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label293, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_023, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_024, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS214, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label316, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label320, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label322, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label323, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label325, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label326, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label328, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label329, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label331, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label334, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label337, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label338, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label340, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label341, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label343, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label344, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label346, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label347, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label350, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label353, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label355, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label356, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label358, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label359, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label361, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label362, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label365, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label368, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_017, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label370, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label371, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label373, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label375, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_019, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_020, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_021, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label385, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label387, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label388, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label390, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_018, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_022, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS215, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label392, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label394, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label396, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label397, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label399, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label400, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label402, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label403, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label405, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label412, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label414, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label415, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label417, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label418, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label420, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label421, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label423, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label424, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label426, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label427, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label429, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label430, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label432, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label433, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label435, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label436, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label439, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label442, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label444, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S215_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label378, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_029, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_030, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_031, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label406, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label408, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label409, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label411, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label438, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label445, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label446, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label448, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.headerS216, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label450, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label452, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label454, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label455, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label457, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label458, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label460, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label461, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label463, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label464, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label467, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label470, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label472, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label473, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label475, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label476, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label478, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label479, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label481, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label482, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label485, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label487, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label488, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label490, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label491, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label493, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label494, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label497, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label500, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label502, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label503, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label505, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label507, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label517, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_019, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label519, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label520, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_020, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label522, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label508, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_021, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label510, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label511, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_022, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label513, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label514, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_023, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label516, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label523, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_024, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label525, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label526, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_025, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label528, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label529, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_026, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label531, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label532, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_027, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label534, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_017, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_018, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_028, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label228, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label212, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label220, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_008, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_010, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_011, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_012, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label225, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_013, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_014, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_015, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_016, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label234, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label243, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_017, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_018, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.SUM_019, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label170, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label173, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S216_034, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label252, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label256, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label248, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label269, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label272, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label276, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label278, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label281, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label290, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label296, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label299, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label307, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S214_026, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label308, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label309, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label310, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_001, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label312, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_002, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label314, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label318, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_003, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label324, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label327, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_004, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label332, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label333, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_005, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label336, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label339, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_006, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label345, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label348, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S217_007, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label351, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label367, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label352, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.Label311, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.S21_009, System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S217_008, Me.S216_032, Me.S214_025, Me.S216_033, Me.S211_018, Me.S210_028, Me.S29_009, Me.S27_016, Me.S27_008, Me.S21_001, Me.S211_013, Me.S211_014, Me.S211_015, Me.S211_016, Me.S211_020, Me.S211_021, Me.S211_022, Me.S211_023, Me.S211_017, Me.Label301, Me.Label302, Me.r21, Me.Label3, Me.headerS21, Me.Label5, Me.Label6, Me.S21_003, Me.Label8, Me.S21_004, Me.Label14, Me.S21_002, Me.headerS22, Me.S22_001, Me.Label21, Me.S22_006, Me.Label23, Me.Label24, Me.Label25, Me.S22_002, Me.Label27, Me.S22_007, Me.Label29, Me.Label30, Me.Label31, Me.S22_003, Me.Label33, Me.S22_008, Me.Label35, Me.Label36, Me.Label37, Me.S22_004, Me.Label39, Me.S22_009, Me.Label41, Me.Label42, Me.Label43, Me.S22_005, Me.Label45, Me.S22_010, Me.Label47, Me.headerS23, Me.Label49, Me.Label50, Me.S23_001, Me.Label52, Me.S23_005, Me.Label55, Me.Label56, Me.S23_002, Me.Label58, Me.S23_006, Me.Label19, Me.Label54, Me.S23_003, Me.Label62, Me.Label63, Me.S23_007, Me.Label59, Me.Label65, Me.S23_004, Me.Label67, Me.Label68, Me.S23_008, Me.headerS25, Me.Label71, Me.Label72, Me.S25_001, Me.Label74, Me.S25_004, Me.Label77, Me.Label78, Me.S25_002, Me.Label80, Me.S25_005, Me.Label83, Me.S25_003, Me.Label85, Me.headerS26, Me.Label86, Me.S26_001, Me.Label88, Me.S26_003, Me.Label90, Me.Label81, Me.S26_002, Me.Label92, Me.S26_004, Me.Label94, Me.headerS27, Me.Label96, Me.S28_001, Me.Label98, Me.Label99, Me.S28_002, Me.Label101, Me.Label102, Me.S28_003, Me.Label104, Me.Label105, Me.S28_004, Me.Label107, Me.Label108, Me.S28_005, Me.Label110, Me.Label111, Me.S28_006, Me.Label113, Me.Label114, Me.S28_007, Me.Label116, Me.Label117, Me.S28_008, Me.Label119, Me.Label120, Me.S28_009, Me.Label122, Me.Label152, Me.S210_003, Me.Label154, Me.Label155, Me.S210_004, Me.Label157, Me.Label158, Me.S210_005, Me.Label160, Me.Label161, Me.S210_006, Me.Label163, Me.Label10, Me.headerS24, Me.Label12, Me.S24_001, Me.Label16, Me.Label17, Me.S24_002, Me.Label168, Me.Label169, Me.S24_003, Me.Label171, Me.Label172, Me.S28_010, Me.Label174, Me.Label175, Me.S28_011, Me.Label177, Me.Label178, Me.S28_012, Me.Label180, Me.headerS29, Me.Label182, Me.S29_001, Me.Label184, Me.Label185, Me.S29_002, Me.Label187, Me.Label188, Me.S29_003, Me.Label190, Me.Label191, Me.S29_004, Me.Label193, Me.Label194, Me.S29_005, Me.Label196, Me.Label197, Me.S29_006, Me.Label199, Me.Label200, Me.S29_007, Me.Label202, Me.Label203, Me.S29_008, Me.Label205, Me.headerS210, Me.Label208, Me.S210_001, Me.Label210, Me.Label211, Me.S210_002, Me.Label213, Me.Label123, Me.Label124, Me.S210_007, Me.Label126, Me.Label127, Me.S210_008, Me.Label129, Me.Label130, Me.S210_009, Me.Label132, Me.Label133, Me.S210_010, Me.Label135, Me.Label136, Me.S210_011, Me.Label138, Me.Label139, Me.S210_012, Me.Label142, Me.S210_013, Me.Label144, Me.Label145, Me.S210_014, Me.Label147, Me.Label148, Me.S210_016, Me.Label150, Me.Label151, Me.S210_017, Me.Label165, Me.Label166, Me.S210_018, Me.Label215, Me.Label216, Me.S210_019, Me.Label218, Me.Label219, Me.S210_020, Me.Label221, Me.Label222, Me.S210_025, Me.Label224, Me.Label141, Me.S210_015, Me.Label226, Me.Label227, Me.S210_021, Me.Label229, Me.Label230, Me.S210_022, Me.Label232, Me.Label233, Me.S210_023, Me.Label235, Me.Label236, Me.S210_024, Me.Label238, Me.Label239, Me.S210_026, Me.Label241, Me.Label242, Me.S210_027, Me.Label244, Me.Label246, Me.Label247, Me.S210_029, Me.Label249, Me.Label250, Me.Label251, Me.S210_030, Me.Label253, Me.Label254, Me.Label255, Me.S210_031, Me.Label257, Me.Label258, Me.S210_032, Me.Label260, Me.headerS211, Me.Label262, Me.S211_001, Me.Label264, Me.Label265, Me.S211_002, Me.Label267, Me.Label268, Me.S211_003, Me.Label270, Me.Label274, Me.S211_004, Me.Label271, Me.S211_019, Me.Label273, Me.S211_024, Me.Label277, Me.S211_025, Me.Label279, Me.Label280, Me.S211_005, Me.Label283, Me.S211_006, Me.Label286, Me.S211_007, Me.Label289, Me.S211_008, Me.Label291, Me.Label292, Me.S211_009, Me.Label294, Me.Label282, Me.S211_010, Me.Label288, Me.Label295, Me.S211_011, Me.Label297, Me.Label298, Me.S211_012, Me.Label300, Me.Line1, Me.Line2, Me.Line3, Me.Line4, Me.Line5, Me.Line6, Me.Line7, Me.Line8, Me.Line9, Me.Line10, Me.Line11, Me.Line12, Me.Label303, Me.Label304, Me.Label305, Me.Label306, Me.Line13, Me.Line14, Me.Line15, Me.Label315, Me.Label317, Me.Label319, Me.Line16, Me.headerS28, Me.Label2, Me.S27_001, Me.Label7, Me.Label9, Me.S27_002, Me.Label13, Me.Label15, Me.S27_003, Me.Label20, Me.Label22, Me.S27_004, Me.Label28, Me.Label32, Me.S27_005, Me.Label38, Me.Label48, Me.S27_007, Me.Label53, Me.Label57, Me.Label61, Me.Label64, Me.S27_010, Me.Label69, Me.Label70, Me.S27_011, Me.Label75, Me.Label76, Me.S27_012, Me.Label82, Me.Label84, Me.S27_013, Me.Label89, Me.Label91, Me.S27_015, Me.Label95, Me.Label97, Me.Label103, Me.Label106, Me.Label109, Me.S27_009, Me.Label115, Me.Shape6, Me.Label1, Me.S27_014, Me.Label11, Me.Label93, Me.S210_033, Me.Label112, Me.Label118, Me.Label121, Me.S210_034, Me.Label128, Me.Label131, Me.S210_035, Me.Label137, Me.Label140, Me.S210_036, Me.Label146, Me.Label100, Me.S21_005, Me.Label134, Me.S21_006, Me.S21_008, Me.S21_007, Me.Label125, Me.Picture1, Me.Shape5, Me.Shape4, Me.Shape2, Me.Shape1, Me.Shape3, Me.S212_006, Me.Label40, Me.Label46, Me.S212_001, Me.Label143, Me.Label149, Me.S212_002, Me.Label153, Me.Label156, Me.S212_003, Me.Label159, Me.S212_004, Me.Label162, Me.Label164, Me.S212_005, Me.Label167, Me.Shape7, Me.S213_007, Me.S213_008, Me.S213_013, Me.S213_018, Me.Label176, Me.Label179, Me.S213_001, Me.Label181, Me.S213_002, Me.Label183, Me.Label186, Me.S213_003, Me.Label189, Me.S213_004, Me.Label192, Me.Label195, Me.S213_005, Me.Label198, Me.Label201, Me.Label204, Me.Label206, Me.S213_006, Me.Label207, Me.Label209, Me.Label214, Me.S213_009, Me.Label217, Me.Label223, Me.S213_014, Me.Label231, Me.S213_010, Me.Label245, Me.S213_011, Me.Label259, Me.Label261, Me.Label263, Me.Label266, Me.S213_012, Me.Label275, Me.S213_015, Me.Label237, Me.Label240, Me.S213_016, Me.Label284, Me.Label285, Me.Label287, Me.S213_017, Me.Label293, Me.Shape8, Me.Shape9, Me.Shape11, Me.Shape12, Me.S214_023, Me.S214_024, Me.headerS214, Me.Label316, Me.S214_001, Me.Label320, Me.S214_002, Me.Label322, Me.Label323, Me.S214_003, Me.Label325, Me.Label326, Me.S214_004, Me.Label328, Me.Label329, Me.S214_005, Me.Label331, Me.Label334, Me.Label337, Me.Label338, Me.S214_006, Me.Label340, Me.Label341, Me.S214_007, Me.Label343, Me.Label344, Me.S214_009, Me.Label346, Me.Label347, Me.Label350, Me.Label353, Me.S214_010, Me.Label355, Me.Label356, Me.S214_008, Me.Label358, Me.Label359, Me.S214_016, Me.Label361, Me.Label362, Me.Label365, Me.Label368, Me.S214_017, Me.Label370, Me.Label371, Me.S214_014, Me.Label373, Me.S214_015, Me.Label375, Me.S214_019, Me.S214_020, Me.S214_021, Me.Label385, Me.S214_012, Me.Label387, Me.Label388, Me.S214_013, Me.Label390, Me.S214_011, Me.S214_018, Me.S214_022, Me.Shape17, Me.Shape19, Me.S215_015, Me.S215_016, Me.headerS215, Me.Label392, Me.S215_001, Me.Label394, Me.S215_002, Me.Label396, Me.Label397, Me.S215_003, Me.Label399, Me.Label400, Me.S215_004, Me.Label402, Me.Label403, Me.S215_005, Me.Label405, Me.Label412, Me.S215_006, Me.Label414, Me.Label415, Me.S215_007, Me.Label417, Me.Label418, Me.S215_009, Me.Label420, Me.Label421, Me.Label423, Me.Label424, Me.Label426, Me.Label427, Me.S215_010, Me.Label429, Me.Label430, Me.S215_008, Me.Label432, Me.Label433, Me.S215_012, Me.Label435, Me.Label436, Me.Label439, Me.Label442, Me.S215_013, Me.Label444, Me.S215_011, Me.S215_014, Me.Label378, Me.Shape23, Me.Shape25, Me.S216_029, Me.S216_030, Me.S216_031, Me.Label406, Me.S216_011, Me.Label408, Me.Label409, Me.Label411, Me.Label438, Me.Label445, Me.Label446, Me.S216_012, Me.Label448, Me.headerS216, Me.Label450, Me.S216_001, Me.Label452, Me.S216_002, Me.Label454, Me.Label455, Me.S216_003, Me.Label457, Me.Label458, Me.S216_004, Me.Label460, Me.Label461, Me.S216_005, Me.Label463, Me.Label464, Me.Label467, Me.Label470, Me.S216_006, Me.Label472, Me.Label473, Me.S216_007, Me.Label475, Me.Label476, Me.S216_009, Me.Label478, Me.Label479, Me.Label481, Me.Label482, Me.Label485, Me.S216_010, Me.Label487, Me.Label488, Me.S216_008, Me.Label490, Me.Label491, Me.S216_015, Me.Label493, Me.Label494, Me.Label497, Me.Label500, Me.S216_016, Me.Label502, Me.Label503, Me.S216_013, Me.Label505, Me.S216_014, Me.Label507, Me.Label517, Me.S216_019, Me.Label519, Me.Label520, Me.S216_020, Me.Label522, Me.Label508, Me.S216_021, Me.Label510, Me.Label511, Me.S216_022, Me.Label513, Me.Label514, Me.S216_023, Me.Label516, Me.Label523, Me.S216_024, Me.Label525, Me.Label526, Me.S216_025, Me.Label528, Me.Label529, Me.S216_026, Me.Label531, Me.Label532, Me.S216_027, Me.Label534, Me.S216_017, Me.S216_018, Me.S216_028, Me.Shape20, Me.Shape21, Me.Shape31, Me.Label228, Me.SUM_004, Me.Label4, Me.SUM_006, Me.SUM_007, Me.SUM_002, Me.SUM_001, Me.Label212, Me.Label18, Me.Label26, Me.Label34, Me.Label44, Me.Label220, Me.Label51, Me.Label60, Me.Label66, Me.SUM_003, Me.SUM_008, Me.Label73, Me.Label79, Me.SUM_009, Me.Label87, Me.SUM_005, Me.SUM_010, Me.SUM_011, Me.SUM_012, Me.Label225, Me.SUM_013, Me.SUM_014, Me.SUM_015, Me.Line30, Me.Line17, Me.Line21, Me.Line23, Me.Line24, Me.Line26, Me.Line31, Me.Line32, Me.SUM_016, Me.Label234, Me.Label243, Me.SUM_017, Me.SUM_018, Me.SUM_019, Me.Line20, Me.Line29, Me.Line19, Me.Line18, Me.Line33, Me.Line27, Me.Line28, Me.Label170, Me.Label173, Me.S216_034, Me.Label252, Me.Label256, Me.Shape27, Me.Label248, Me.Label269, Me.Label272, Me.Label276, Me.Label278, Me.Label281, Me.Label290, Me.Label296, Me.Label299, Me.Label307, Me.S214_026, Me.Shape15, Me.Label308, Me.Label309, Me.Label310, Me.S217_001, Me.Label312, Me.S217_002, Me.Label314, Me.Label318, Me.S217_003, Me.Label324, Me.Label327, Me.S217_004, Me.Label332, Me.Label333, Me.S217_005, Me.Label336, Me.Label339, Me.S217_006, Me.Label345, Me.Label348, Me.S217_007, Me.Label351, Me.Label367, Me.Label352, Me.PageBreak1, Me.Shape29, Me.Shape10, Me.Label311, Me.S21_009})
        Me.Detail.Height = 45.27067!
        Me.Detail.Name = "Detail"
        '
        'S217_008
        '
        Me.S217_008.Height = 0.1875!
        Me.S217_008.HyperLink = Nothing
        Me.S217_008.Left = 2.572!
        Me.S217_008.Name = "S217_008"
        Me.S217_008.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S217_008.Text = "OK"
        Me.S217_008.Top = 43.088!
        Me.S217_008.Width = 1.0!
        '
        'S216_032
        '
        Me.S216_032.Height = 0.1875!
        Me.S216_032.HyperLink = Nothing
        Me.S216_032.Left = 2.583!
        Me.S216_032.Name = "S216_032"
        Me.S216_032.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S216_032.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_032.Top = 41.246!
        Me.S216_032.Width = 1.0!
        '
        'S214_025
        '
        Me.S214_025.Height = 0.1875!
        Me.S214_025.HyperLink = Nothing
        Me.S214_025.Left = 5.019999!
        Me.S214_025.Name = "S214_025"
        Me.S214_025.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S214_025.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_025.Top = 32.24403!
        Me.S214_025.Width = 1.0!
        '
        'S216_033
        '
        Me.S216_033.Height = 0.1875!
        Me.S216_033.HyperLink = Nothing
        Me.S216_033.Left = 5.012!
        Me.S216_033.Name = "S216_033"
        Me.S216_033.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S216_033.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_033.Top = 40.78902!
        Me.S216_033.Width = 1.0!
        '
        'S211_018
        '
        Me.S211_018.Height = 0.1875!
        Me.S211_018.HyperLink = Nothing
        Me.S211_018.Left = 2.5!
        Me.S211_018.Name = "S211_018"
        Me.S211_018.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S211_018.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_018.Top = 22.437!
        Me.S211_018.Width = 1.0!
        '
        'S210_028
        '
        Me.S210_028.Height = 0.1875!
        Me.S210_028.HyperLink = Nothing
        Me.S210_028.Left = 2.5!
        Me.S210_028.Name = "S210_028"
        Me.S210_028.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S210_028.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_028.Top = 18.312!
        Me.S210_028.Width = 1.0!
        '
        'S29_009
        '
        Me.S29_009.Height = 0.1875!
        Me.S29_009.HyperLink = Nothing
        Me.S29_009.Left = 2.5!
        Me.S29_009.Name = "S29_009"
        Me.S29_009.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S29_009.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_009.Top = 12.814!
        Me.S29_009.Width = 1.0!
        '
        'S27_016
        '
        Me.S27_016.Height = 0.1875!
        Me.S27_016.HyperLink = Nothing
        Me.S27_016.Left = 2.52!
        Me.S27_016.Name = "S27_016"
        Me.S27_016.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S27_016.Text = "OK"
        Me.S27_016.Top = 8.146002!
        Me.S27_016.Width = 1.0!
        '
        'S27_008
        '
        Me.S27_008.Height = 0.1875!
        Me.S27_008.HyperLink = Nothing
        Me.S27_008.Left = 2.5!
        Me.S27_008.Name = "S27_008"
        Me.S27_008.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S27_008.Text = "OK"
        Me.S27_008.Top = 6.375!
        Me.S27_008.Width = 1.0!
        '
        'S21_001
        '
        Me.S21_001.Height = 0.1875!
        Me.S21_001.HyperLink = Nothing
        Me.S21_001.Left = 1.4!
        Me.S21_001.Name = "S21_001"
        Me.S21_001.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.S21_001.Text = "299"
        Me.S21_001.Top = 0.188!
        Me.S21_001.Width = 1.0!
        '
        'S211_013
        '
        Me.S211_013.Height = 0.1875!
        Me.S211_013.HyperLink = Nothing
        Me.S211_013.Left = 2.5!
        Me.S211_013.Name = "S211_013"
        Me.S211_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_013.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_013.Top = 21.437!
        Me.S211_013.Width = 1.0!
        '
        'S211_014
        '
        Me.S211_014.Height = 0.1875!
        Me.S211_014.HyperLink = Nothing
        Me.S211_014.Left = 2.5!
        Me.S211_014.Name = "S211_014"
        Me.S211_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_014.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_014.Top = 21.624!
        Me.S211_014.Width = 1.0!
        '
        'S211_015
        '
        Me.S211_015.Height = 0.1875!
        Me.S211_015.HyperLink = Nothing
        Me.S211_015.Left = 2.5!
        Me.S211_015.Name = "S211_015"
        Me.S211_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_015.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_015.Top = 21.813!
        Me.S211_015.Width = 1.0!
        '
        'S211_016
        '
        Me.S211_016.Height = 0.1875!
        Me.S211_016.HyperLink = Nothing
        Me.S211_016.Left = 2.5!
        Me.S211_016.Name = "S211_016"
        Me.S211_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_016.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_016.Top = 22.001!
        Me.S211_016.Width = 1.0!
        '
        'S211_020
        '
        Me.S211_020.Height = 0.1875!
        Me.S211_020.HyperLink = Nothing
        Me.S211_020.Left = 3.5!
        Me.S211_020.Name = "S211_020"
        Me.S211_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_020.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_020.Top = 21.437!
        Me.S211_020.Width = 1.0!
        '
        'S211_021
        '
        Me.S211_021.Height = 0.1875!
        Me.S211_021.HyperLink = Nothing
        Me.S211_021.Left = 3.5!
        Me.S211_021.Name = "S211_021"
        Me.S211_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_021.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_021.Top = 21.625!
        Me.S211_021.Width = 1.0!
        '
        'S211_022
        '
        Me.S211_022.Height = 0.1875!
        Me.S211_022.HyperLink = Nothing
        Me.S211_022.Left = 3.5!
        Me.S211_022.Name = "S211_022"
        Me.S211_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_022.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_022.Top = 21.813!
        Me.S211_022.Width = 1.0!
        '
        'S211_023
        '
        Me.S211_023.Height = 0.1875!
        Me.S211_023.HyperLink = Nothing
        Me.S211_023.Left = 3.5!
        Me.S211_023.Name = "S211_023"
        Me.S211_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_023.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_023.Top = 22.001!
        Me.S211_023.Width = 1.0!
        '
        'S211_017
        '
        Me.S211_017.Height = 0.1875!
        Me.S211_017.HyperLink = Nothing
        Me.S211_017.Left = 2.5!
        Me.S211_017.Name = "S211_017"
        Me.S211_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_017.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_017.Top = 22.25!
        Me.S211_017.Width = 1.0!
        '
        'Label301
        '
        Me.Label301.Height = 0.1875!
        Me.Label301.HyperLink = Nothing
        Me.Label301.Left = 2.5!
        Me.Label301.Name = "Label301"
        Me.Label301.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label301.Text = "X-Dir DCR"
        Me.Label301.Top = 21.25!
        Me.Label301.Width = 1.0!
        '
        'Label302
        '
        Me.Label302.Height = 0.1879997!
        Me.Label302.HyperLink = Nothing
        Me.Label302.Left = 3.5!
        Me.Label302.Name = "Label302"
        Me.Label302.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label302.Text = "Y-Dir DCR"
        Me.Label302.Top = 21.25!
        Me.Label302.Width = 0.9999998!
        '
        'r21
        '
        Me.r21.Height = 0.188!
        Me.r21.HyperLink = Nothing
        Me.r21.Left = 0!
        Me.r21.Name = "r21"
        Me.r21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.r21.Text = "Beam Unique Name:"
        Me.r21.Top = 0.187!
        Me.r21.Width = 1.4!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 7.312!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label3.Text = "in"
        Me.Label3.Top = 0.812!
        Me.Label3.Width = 0.25!
        '
        'headerS21
        '
        Me.headerS21.Height = 0.1875!
        Me.headerS21.HyperLink = Nothing
        Me.headerS21.Left = 0!
        Me.headerS21.Name = "headerS21"
        Me.headerS21.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS21.Text = "2.1 CURRENT MEMBER:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS21.Top = 0!
        Me.headerS21.Width = 3.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "NY Length ColSide (Lcol_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 0.812!
        Me.Label5.Width = 2.5!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 2.4!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label6.Text = "Link ID:"
        Me.Label6.Top = 0.188!
        Me.Label6.Width = 0.9369998!
        '
        'S21_003
        '
        Me.S21_003.Height = 0.1875!
        Me.S21_003.HyperLink = Nothing
        Me.S21_003.Left = 3.337!
        Me.S21_003.Name = "S21_003"
        Me.S21_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S21_003.Text = "YL4-3.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S21_003.Top = 0.188!
        Me.S21_003.Width = 1.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 2.4!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label8.Text = "Mu_max:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 0.375!
        Me.Label8.Width = 0.9369998!
        '
        'S21_004
        '
        Me.S21_004.Height = 0.1875!
        Me.S21_004.HyperLink = Nothing
        Me.S21_004.Left = 3.337!
        Me.S21_004.Name = "S21_004"
        Me.S21_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S21_004.Text = "5010.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S21_004.Top = 0.375!
        Me.S21_004.Width = 1.0!
        '
        'Label14
        '
        Me.Label14.Height = 0.188!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label14.Text = "Beam Size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 0.374!
        Me.Label14.Width = 1.4!
        '
        'S21_002
        '
        Me.S21_002.Height = 0.1875!
        Me.S21_002.HyperLink = Nothing
        Me.S21_002.Left = 1.4!
        Me.S21_002.Name = "S21_002"
        Me.S21_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S21_002.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S21_002.Top = 0.375!
        Me.S21_002.Width = 1.0!
        '
        'headerS22
        '
        Me.headerS22.Height = 0.1875!
        Me.headerS22.HyperLink = Nothing
        Me.headerS22.Left = 0!
        Me.headerS22.Name = "headerS22"
        Me.headerS22.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS22.Text = "2.2 LINK STEM GEOMETRY:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS22.Top = 0.625!
        Me.headerS22.Width = 3.0!
        '
        'S22_001
        '
        Me.S22_001.Height = 0.1875!
        Me.S22_001.HyperLink = Nothing
        Me.S22_001.Left = 2.5!
        Me.S22_001.Name = "S22_001"
        Me.S22_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_001.Top = 0.812!
        Me.S22_001.Width = 1.0!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 3.75!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "Thickness (t_stem) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 0.812!
        Me.Label21.Width = 2.562!
        '
        'S22_006
        '
        Me.S22_006.Height = 0.1875!
        Me.S22_006.HyperLink = Nothing
        Me.S22_006.Left = 6.312!
        Me.S22_006.Name = "S22_006"
        Me.S22_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_006.Top = 0.812!
        Me.S22_006.Width = 1.0!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 3.5!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label23.Text = "in"
        Me.Label23.Top = 0.812!
        Me.Label23.Width = 0.25!
        '
        'Label24
        '
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 7.312!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label24.Text = "in"
        Me.Label24.Top = 1.0!
        Me.Label24.Width = 0.25!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = "Yield Length, incl. fillets (L_stemYield) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 1.0!
        Me.Label25.Width = 2.5!
        '
        'S22_002
        '
        Me.S22_002.Height = 0.1875!
        Me.S22_002.HyperLink = Nothing
        Me.S22_002.Left = 2.5!
        Me.S22_002.Name = "S22_002"
        Me.S22_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_002.Top = 1.0!
        Me.S22_002.Width = 1.0!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.75!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label27.Text = "NY Width ColSide (Wcol_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label27.Top = 1.0!
        Me.Label27.Width = 2.562!
        '
        'S22_007
        '
        Me.S22_007.Height = 0.1875!
        Me.S22_007.HyperLink = Nothing
        Me.S22_007.Left = 6.312!
        Me.S22_007.Name = "S22_007"
        Me.S22_007.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_007.Top = 1.0!
        Me.S22_007.Width = 1.0!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 3.5!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label29.Text = "in"
        Me.Label29.Top = 1.0!
        Me.Label29.Width = 0.25!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 7.312!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label30.Text = "in"
        Me.Label30.Top = 1.187!
        Me.Label30.Width = 0.25!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 0!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label31.Text = "NY Length BeamSide (Lbm_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label31.Top = 1.187!
        Me.Label31.Width = 2.5!
        '
        'S22_003
        '
        Me.S22_003.Height = 0.1875!
        Me.S22_003.HyperLink = Nothing
        Me.S22_003.Left = 2.5!
        Me.S22_003.Name = "S22_003"
        Me.S22_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_003.Top = 1.187!
        Me.S22_003.Width = 1.0!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 3.75!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "Central Neck Yield Width (w_stemYield)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label33.Top = 1.187!
        Me.Label33.Width = 2.562!
        '
        'S22_008
        '
        Me.S22_008.Height = 0.1875!
        Me.S22_008.HyperLink = Nothing
        Me.S22_008.Left = 6.312!
        Me.S22_008.Name = "S22_008"
        Me.S22_008.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_008.Top = 1.187!
        Me.S22_008.Width = 1.0!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 3.5!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label35.Text = "in"
        Me.Label35.Top = 1.187!
        Me.Label35.Width = 0.25!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 7.312!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label36.Text = "in"
        Me.Label36.Top = 1.375!
        Me.Label36.Width = 0.25!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label37.Text = "L_stem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label37.Top = 1.375!
        Me.Label37.Width = 2.5!
        '
        'S22_004
        '
        Me.S22_004.Height = 0.1875!
        Me.S22_004.HyperLink = Nothing
        Me.S22_004.Left = 2.5!
        Me.S22_004.Name = "S22_004"
        Me.S22_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S22_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_004.Top = 1.375!
        Me.S22_004.Width = 1.0!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 3.75!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label39.Text = "NY Width BeamSide (Wbm_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label39.Top = 1.375!
        Me.Label39.Width = 2.562!
        '
        'S22_009
        '
        Me.S22_009.Height = 0.1875!
        Me.S22_009.HyperLink = Nothing
        Me.S22_009.Left = 6.312!
        Me.S22_009.Name = "S22_009"
        Me.S22_009.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S22_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_009.Top = 1.375!
        Me.S22_009.Width = 1.0!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 3.5!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label41.Text = "in"
        Me.Label41.Top = 1.375!
        Me.Label41.Width = 0.25!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 7.312!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "in^2"
        Me.Label42.Top = 1.563!
        Me.Label42.Width = 0.25!
        '
        'Label43
        '
        Me.Label43.Height = 0.1875!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 0!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label43.Text = "Fillet Radius (r_fillet)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label43.Top = 1.563!
        Me.Label43.Width = 2.5!
        '
        'S22_005
        '
        Me.S22_005.Height = 0.1875!
        Me.S22_005.HyperLink = Nothing
        Me.S22_005.Left = 2.5!
        Me.S22_005.Name = "S22_005"
        Me.S22_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S22_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_005.Top = 1.563!
        Me.S22_005.Width = 1.0!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 3.75!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label45.Text = "Yielding Area (A_stemYield) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label45.Top = 1.563!
        Me.Label45.Width = 2.562!
        '
        'S22_010
        '
        Me.S22_010.Height = 0.1875!
        Me.S22_010.HyperLink = Nothing
        Me.S22_010.Left = 6.312!
        Me.S22_010.Name = "S22_010"
        Me.S22_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S22_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S22_010.Top = 1.563!
        Me.S22_010.Width = 1.0!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 3.5!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label47.Text = "in"
        Me.Label47.Top = 1.563!
        Me.Label47.Width = 0.25!
        '
        'headerS23
        '
        Me.headerS23.Height = 0.1875!
        Me.headerS23.HyperLink = Nothing
        Me.headerS23.Left = 0!
        Me.headerS23.Name = "headerS23"
        Me.headerS23.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS23.Text = "2.3 LINK STEM BOLTS:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS23.Top = 1.812!
        Me.headerS23.Width = 3.0!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 7.312!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label49.Text = "in"
        Me.Label49.Top = 2.0!
        Me.Label49.Width = 0.25!
        '
        'Label50
        '
        Me.Label50.Height = 0.1875!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 0!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label50.Text = "Num. Bolts (n_bolt_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label50.Top = 2.0!
        Me.Label50.Width = 2.5!
        '
        'S23_001
        '
        Me.S23_001.Height = 0.1875!
        Me.S23_001.HyperLink = Nothing
        Me.S23_001.Left = 2.5!
        Me.S23_001.Name = "S23_001"
        Me.S23_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S23_001.Text = "6"
        Me.S23_001.Top = 2.0!
        Me.S23_001.Width = 1.0!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 3.749999!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label52.Text = "Gauge Along Width (bolt_g_stem)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label52.Top = 2.0!
        Me.Label52.Width = 2.562!
        '
        'S23_005
        '
        Me.S23_005.Height = 0.1875!
        Me.S23_005.HyperLink = Nothing
        Me.S23_005.Left = 6.312!
        Me.S23_005.Name = "S23_005"
        Me.S23_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S23_005.Text = "4.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S23_005.Top = 2.0!
        Me.S23_005.Width = 1.0!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 7.312!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label55.Text = "in"
        Me.Label55.Top = 2.187!
        Me.Label55.Width = 0.25!
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 0!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label56.Text = "Bolt Type (Bolt_Gr_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label56.Top = 2.187!
        Me.Label56.Width = 2.5!
        '
        'S23_002
        '
        Me.S23_002.Height = 0.1875!
        Me.S23_002.HyperLink = Nothing
        Me.S23_002.Left = 2.5!
        Me.S23_002.Name = "S23_002"
        Me.S23_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S23_002.Text = "A490" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S23_002.Top = 2.187!
        Me.S23_002.Width = 1.0!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 3.749999!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label58.Text = "Spacing Along Length (bolt_s_stem) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label58.Top = 2.187!
        Me.Label58.Width = 2.562!
        '
        'S23_006
        '
        Me.S23_006.Height = 0.1875!
        Me.S23_006.HyperLink = Nothing
        Me.S23_006.Left = 6.312!
        Me.S23_006.Name = "S23_006"
        Me.S23_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S23_006.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S23_006.Top = 2.187!
        Me.S23_006.Width = 1.0!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 7.312!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label19.Text = "in"
        Me.Label19.Top = 2.375!
        Me.Label19.Width = 0.25!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 0!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label54.Text = "Bolt Dia (boltD_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label54.Top = 2.375!
        Me.Label54.Width = 2.5!
        '
        'S23_003
        '
        Me.S23_003.Height = 0.1875!
        Me.S23_003.HyperLink = Nothing
        Me.S23_003.Left = 2.5!
        Me.S23_003.Name = "S23_003"
        Me.S23_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S23_003.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S23_003.Top = 2.375!
        Me.S23_003.Width = 1.0!
        '
        'Label62
        '
        Me.Label62.Height = 0.1875!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 3.749999!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label62.Text = "First Bolt distance to Neck (Sc)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label62.Top = 2.375!
        Me.Label62.Width = 2.562!
        '
        'Label63
        '
        Me.Label63.Height = 0.1875!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 3.5!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label63.Text = "in"
        Me.Label63.Top = 2.375!
        Me.Label63.Width = 0.25!
        '
        'S23_007
        '
        Me.S23_007.Height = 0.1875!
        Me.S23_007.HyperLink = Nothing
        Me.S23_007.Left = 6.312!
        Me.S23_007.Name = "S23_007"
        Me.S23_007.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S23_007.Text = "1.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S23_007.Top = 2.375!
        Me.S23_007.Width = 1.0!
        '
        'Label59
        '
        Me.Label59.Height = 0.1875!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 7.312!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label59.Text = "in"
        Me.Label59.Top = 2.562!
        Me.Label59.Width = 0.25!
        '
        'Label65
        '
        Me.Label65.Height = 0.1875!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 0!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label65.Text = "Min. Bolt length =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label65.Top = 2.562!
        Me.Label65.Width = 2.5!
        '
        'S23_004
        '
        Me.S23_004.Height = 0.1875!
        Me.S23_004.HyperLink = Nothing
        Me.S23_004.Left = 2.5!
        Me.S23_004.Name = "S23_004"
        Me.S23_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S23_004.Text = "3.875"
        Me.S23_004.Top = 2.562!
        Me.S23_004.Width = 1.0!
        '
        'Label67
        '
        Me.Label67.Height = 0.1875!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 3.749999!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label67.Text = "Last Bolt distance to Edge (Sb) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label67.Top = 2.562!
        Me.Label67.Width = 2.562!
        '
        'Label68
        '
        Me.Label68.Height = 0.1875!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 3.5!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label68.Text = "in"
        Me.Label68.Top = 2.562!
        Me.Label68.Width = 0.25!
        '
        'S23_008
        '
        Me.S23_008.Height = 0.1875!
        Me.S23_008.HyperLink = Nothing
        Me.S23_008.Left = 6.312!
        Me.S23_008.Name = "S23_008"
        Me.S23_008.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S23_008.Text = "1.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S23_008.Top = 2.562!
        Me.S23_008.Width = 1.0!
        '
        'headerS25
        '
        Me.headerS25.Height = 0.1875!
        Me.headerS25.HyperLink = Nothing
        Me.headerS25.Left = 0!
        Me.headerS25.Name = "headerS25"
        Me.headerS25.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS25.Text = "2.5 LINK FLANGE BOLTS:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS25.Top = 3.625!
        Me.headerS25.Width = 3.0!
        '
        'Label71
        '
        Me.Label71.Height = 0.1875!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 7.312!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label71.Text = "in"
        Me.Label71.Top = 3.812!
        Me.Label71.Width = 0.25!
        '
        'Label72
        '
        Me.Label72.Height = 0.1875!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 0!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label72.Text = "Num. Bolts (n_bolt_linkCol) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label72.Top = 3.812!
        Me.Label72.Width = 2.5!
        '
        'S25_001
        '
        Me.S25_001.Height = 0.1875!
        Me.S25_001.HyperLink = Nothing
        Me.S25_001.Left = 2.5!
        Me.S25_001.Name = "S25_001"
        Me.S25_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S25_001.Text = "4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S25_001.Top = 3.812!
        Me.S25_001.Width = 1.0!
        '
        'Label74
        '
        Me.Label74.Height = 0.1875!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 3.812999!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label74.Text = "Gauge Along Width (vertical) (bolt_g_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label74.Top = 3.812!
        Me.Label74.Width = 2.499!
        '
        'S25_004
        '
        Me.S25_004.Height = 0.1875!
        Me.S25_004.HyperLink = Nothing
        Me.S25_004.Left = 6.312!
        Me.S25_004.Name = "S25_004"
        Me.S25_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S25_004.Text = "3.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S25_004.Top = 3.812!
        Me.S25_004.Width = 1.0!
        '
        'Label77
        '
        Me.Label77.Height = 0.1875!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 7.312!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label77.Text = "in"
        Me.Label77.Top = 4.0!
        Me.Label77.Width = 0.25!
        '
        'Label78
        '
        Me.Label78.Height = 0.1875!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 0!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label78.Text = "Bolt Type (Bolt_Gr_linkCol)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label78.Top = 4.0!
        Me.Label78.Width = 2.5!
        '
        'S25_002
        '
        Me.S25_002.Height = 0.1875!
        Me.S25_002.HyperLink = Nothing
        Me.S25_002.Left = 2.5!
        Me.S25_002.Name = "S25_002"
        Me.S25_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S25_002.Text = "A325" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S25_002.Top = 4.0!
        Me.S25_002.Width = 1.0!
        '
        'Label80
        '
        Me.Label80.Height = 0.1875!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 3.812999!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label80.Text = "Spacing Along Length (horiz)  (bolt_s_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label80.Top = 4.0!
        Me.Label80.Width = 2.499!
        '
        'S25_005
        '
        Me.S25_005.Height = 0.1875!
        Me.S25_005.HyperLink = Nothing
        Me.S25_005.Left = 6.312!
        Me.S25_005.Name = "S25_005"
        Me.S25_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S25_005.Text = "5.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S25_005.Top = 4.0!
        Me.S25_005.Width = 1.0!
        '
        'Label83
        '
        Me.Label83.Height = 0.1875!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 0!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label83.Text = "Bolt Dia (boltD_linkCol)_=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label83.Top = 4.187!
        Me.Label83.Width = 2.5!
        '
        'S25_003
        '
        Me.S25_003.Height = 0.1875!
        Me.S25_003.HyperLink = Nothing
        Me.S25_003.Left = 2.5!
        Me.S25_003.Name = "S25_003"
        Me.S25_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S25_003.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S25_003.Top = 4.187!
        Me.S25_003.Width = 1.0!
        '
        'Label85
        '
        Me.Label85.Height = 0.1875!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 3.5!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label85.Text = "in"
        Me.Label85.Top = 4.187!
        Me.Label85.Width = 0.25!
        '
        'headerS26
        '
        Me.headerS26.Height = 0.1875!
        Me.headerS26.HyperLink = Nothing
        Me.headerS26.Left = 0!
        Me.headerS26.Name = "headerS26"
        Me.headerS26.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS26.Text = "2.6 LINK MATERIAL:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS26.Top = 4.437!
        Me.headerS26.Width = 3.0!
        '
        'Label86
        '
        Me.Label86.Height = 0.1875!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 0.0000002384186!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label86.Text = "Fy_link =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label86.Top = 4.625!
        Me.Label86.Width = 2.5!
        '
        'S26_001
        '
        Me.S26_001.Height = 0.1875!
        Me.S26_001.HyperLink = Nothing
        Me.S26_001.Left = 2.5!
        Me.S26_001.Name = "S26_001"
        Me.S26_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S26_001.Text = "50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S26_001.Top = 4.625!
        Me.S26_001.Width = 1.0!
        '
        'Label88
        '
        Me.Label88.Height = 0.1875!
        Me.Label88.HyperLink = Nothing
        Me.Label88.Left = 3.812999!
        Me.Label88.Name = "Label88"
        Me.Label88.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label88.Text = "Material Overstrength Factor (Ry_link) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label88.Top = 4.625!
        Me.Label88.Width = 2.499001!
        '
        'S26_003
        '
        Me.S26_003.Height = 0.1875!
        Me.S26_003.HyperLink = Nothing
        Me.S26_003.Left = 6.312!
        Me.S26_003.Name = "S26_003"
        Me.S26_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S26_003.Text = "1.2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S26_003.Top = 4.812!
        Me.S26_003.Width = 1.0!
        '
        'Label90
        '
        Me.Label90.Height = 0.1875!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 3.5!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label90.Text = "ksi"
        Me.Label90.Top = 4.625!
        Me.Label90.Width = 0.25!
        '
        'Label81
        '
        Me.Label81.Height = 0.1875!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 0!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label81.Text = "Fu_link =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label81.Top = 4.812!
        Me.Label81.Width = 2.5!
        '
        'S26_002
        '
        Me.S26_002.Height = 0.1875!
        Me.S26_002.HyperLink = Nothing
        Me.S26_002.Left = 2.5!
        Me.S26_002.Name = "S26_002"
        Me.S26_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S26_002.Text = "65"
        Me.S26_002.Top = 4.812!
        Me.S26_002.Width = 1.0!
        '
        'Label92
        '
        Me.Label92.Height = 0.1875!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 3.812999!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label92.Text = "Rt_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label92.Top = 4.812!
        Me.Label92.Width = 2.499002!
        '
        'S26_004
        '
        Me.S26_004.Height = 0.1875!
        Me.S26_004.HyperLink = Nothing
        Me.S26_004.Left = 6.312!
        Me.S26_004.Name = "S26_004"
        Me.S26_004.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S26_004.Text = "1.1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S26_004.Top = 4.625!
        Me.S26_004.Width = 1.0!
        '
        'Label94
        '
        Me.Label94.Height = 0.1875!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 3.5!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label94.Text = "ksi"
        Me.Label94.Top = 4.812!
        Me.Label94.Width = 0.25!
        '
        'headerS27
        '
        Me.headerS27.Height = 0.1875!
        Me.headerS27.HyperLink = Nothing
        Me.headerS27.Left = 0!
        Me.headerS27.Name = "headerS27"
        Me.headerS27.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS27.Text = "2.7 LINK STEM STRAIN DEMAND CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS27.Top = 5.062!
        Me.headerS27.Width = 5.0!
        '
        'Label96
        '
        Me.Label96.Height = 0.1875!
        Me.Label96.HyperLink = Nothing
        Me.Label96.Left = 0.01!
        Me.Label96.Name = "Label96"
        Me.Label96.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label96.Text = "D_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label96.Top = 8.728001!
        Me.Label96.Width = 2.5!
        '
        'S28_001
        '
        Me.S28_001.Height = 0.1875!
        Me.S28_001.HyperLink = Nothing
        Me.S28_001.Left = 2.51!
        Me.S28_001.Name = "S28_001"
        Me.S28_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_001.Top = 8.728001!
        Me.S28_001.Width = 1.0!
        '
        'Label98
        '
        Me.Label98.Height = 0.188!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 3.51!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label98.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "from Database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label98.Top = 8.728001!
        Me.Label98.Width = 1.5!
        '
        'Label99
        '
        Me.Label99.Height = 0.1875!
        Me.Label99.HyperLink = Nothing
        Me.Label99.Left = 0.01!
        Me.Label99.Name = "Label99"
        Me.Label99.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label99.Text = "nbolt_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label99.Top = 8.915001!
        Me.Label99.Width = 2.5!
        '
        'S28_002
        '
        Me.S28_002.Height = 0.1875!
        Me.S28_002.HyperLink = Nothing
        Me.S28_002.Left = 2.51!
        Me.S28_002.Name = "S28_002"
        Me.S28_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_002.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_002.Top = 8.915001!
        Me.S28_002.Width = 1.0!
        '
        'Label101
        '
        Me.Label101.Height = 0.188!
        Me.Label101.HyperLink = Nothing
        Me.Label101.Left = 3.51!
        Me.Label101.Name = "Label101"
        Me.Label101.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label101.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "per Spacer plate" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label101.Top = 8.915001!
        Me.Label101.Width = 1.5!
        '
        'Label102
        '
        Me.Label102.Height = 0.1875!
        Me.Label102.HyperLink = Nothing
        Me.Label102.Left = 0.01!
        Me.Label102.Name = "Label102"
        Me.Label102.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label102.Text = "S_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label102.Top = 9.103001!
        Me.Label102.Width = 2.5!
        '
        'S28_003
        '
        Me.S28_003.Height = 0.1875!
        Me.S28_003.HyperLink = Nothing
        Me.S28_003.Left = 2.51!
        Me.S28_003.Name = "S28_003"
        Me.S28_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_003.Top = 9.103001!
        Me.S28_003.Width = 1.0!
        '
        'Label104
        '
        Me.Label104.Height = 0.188!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 3.51!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label104.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "from Database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label104.Top = 9.103003!
        Me.Label104.Width = 1.5!
        '
        'Label105
        '
        Me.Label105.Height = 0.1875!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 0.01!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label105.Text = "t_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label105.Top = 9.291!
        Me.Label105.Width = 2.5!
        '
        'S28_004
        '
        Me.S28_004.Height = 0.1875!
        Me.S28_004.HyperLink = Nothing
        Me.S28_004.Left = 2.51!
        Me.S28_004.Name = "S28_004"
        Me.S28_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_004.Top = 9.291!
        Me.S28_004.Width = 1.0!
        '
        'Label107
        '
        Me.Label107.Height = 0.188!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 3.51!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label107.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "from Database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label107.Top = 9.291!
        Me.Label107.Width = 1.5!
        '
        'Label108
        '
        Me.Label108.Height = 0.1875!
        Me.Label108.HyperLink = Nothing
        Me.Label108.Left = 0.01!
        Me.Label108.Name = "Label108"
        Me.Label108.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label108.Text = "L_be=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label108.Top = 9.479!
        Me.Label108.Width = 2.5!
        '
        'S28_005
        '
        Me.S28_005.Height = 0.1875!
        Me.S28_005.HyperLink = Nothing
        Me.S28_005.Left = 2.51!
        Me.S28_005.Name = "S28_005"
        Me.S28_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_005.Top = 9.479!
        Me.S28_005.Width = 1.0!
        '
        'Label110
        '
        Me.Label110.Height = 0.188!
        Me.Label110.HyperLink = Nothing
        Me.Label110.Left = 3.51!
        Me.Label110.Name = "Label110"
        Me.Label110.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label110.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "from Database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label110.Top = 9.479!
        Me.Label110.Width = 1.5!
        '
        'Label111
        '
        Me.Label111.Height = 0.1875!
        Me.Label111.HyperLink = Nothing
        Me.Label111.Left = 0.01!
        Me.Label111.Name = "Label111"
        Me.Label111.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label111.Text = "L_brp_cant=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label111.Top = 9.666!
        Me.Label111.Width = 2.5!
        '
        'S28_006
        '
        Me.S28_006.Height = 0.1875!
        Me.S28_006.HyperLink = Nothing
        Me.S28_006.Left = 2.51!
        Me.S28_006.Name = "S28_006"
        Me.S28_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S28_006.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_006.Top = 9.666!
        Me.S28_006.Width = 1.0!
        '
        'Label113
        '
        Me.Label113.Height = 0.188!
        Me.Label113.HyperLink = Nothing
        Me.Label113.Left = 3.51!
        Me.Label113.Name = "Label113"
        Me.Label113.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label113.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=Lbrp - L_be - S_brp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label113.Top = 9.666!
        Me.Label113.Width = 1.5!
        '
        'Label114
        '
        Me.Label114.Height = 0.1875!
        Me.Label114.HyperLink = Nothing
        Me.Label114.Left = 0.01!
        Me.Label114.Name = "Label114"
        Me.Label114.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label114.Text = "L_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label114.Top = 9.853001!
        Me.Label114.Width = 2.5!
        '
        'S28_007
        '
        Me.S28_007.Height = 0.1875!
        Me.S28_007.HyperLink = Nothing
        Me.S28_007.Left = 2.51!
        Me.S28_007.Name = "S28_007"
        Me.S28_007.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_007.Top = 9.853001!
        Me.S28_007.Width = 1.0!
        '
        'Label116
        '
        Me.Label116.Height = 0.188!
        Me.Label116.HyperLink = Nothing
        Me.Label116.Left = 3.51!
        Me.Label116.Name = "Label116"
        Me.Label116.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label116.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "from Database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label116.Top = 9.853001!
        Me.Label116.Width = 1.5!
        '
        'Label117
        '
        Me.Label117.Height = 0.1875!
        Me.Label117.HyperLink = Nothing
        Me.Label117.Left = 0.01!
        Me.Label117.Name = "Label117"
        Me.Label117.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label117.Text = "Wbrp_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label117.Top = 10.041!
        Me.Label117.Width = 2.5!
        '
        'S28_008
        '
        Me.S28_008.Height = 0.1875!
        Me.S28_008.HyperLink = Nothing
        Me.S28_008.Left = 2.51!
        Me.S28_008.Name = "S28_008"
        Me.S28_008.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S28_008.Text = "8"
        Me.S28_008.Top = 10.041!
        Me.S28_008.Width = 1.0!
        '
        'Label119
        '
        Me.Label119.Height = 0.188!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 3.51!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label119.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Wbm_side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label119.Top = 10.041!
        Me.Label119.Width = 1.5!
        '
        'Label120
        '
        Me.Label120.Height = 0.1875!
        Me.Label120.HyperLink = Nothing
        Me.Label120.Left = 0.01!
        Me.Label120.Name = "Label120"
        Me.Label120.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label120.Text = "Fy_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label120.Top = 10.228!
        Me.Label120.Width = 2.5!
        '
        'S28_009
        '
        Me.S28_009.Height = 0.1875!
        Me.S28_009.HyperLink = Nothing
        Me.S28_009.Left = 2.51!
        Me.S28_009.Name = "S28_009"
        Me.S28_009.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S28_009.Text = "50.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_009.Top = 10.228!
        Me.S28_009.Width = 1.0!
        '
        'Label122
        '
        Me.Label122.Height = 0.188!
        Me.Label122.HyperLink = Nothing
        Me.Label122.Left = 3.51!
        Me.Label122.Name = "Label122"
        Me.Label122.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label122.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "User Input" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label122.Top = 10.228!
        Me.Label122.Width = 1.5!
        '
        'Label152
        '
        Me.Label152.Height = 0.1875!
        Me.Label152.HyperLink = Nothing
        Me.Label152.Left = 0!
        Me.Label152.Name = "Label152"
        Me.Label152.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label152.Text = "g=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label152.Top = 13.625!
        Me.Label152.Width = 2.5!
        '
        'S210_003
        '
        Me.S210_003.Height = 0.1875!
        Me.S210_003.HyperLink = Nothing
        Me.S210_003.Left = 2.5!
        Me.S210_003.Name = "S210_003"
        Me.S210_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_003.Text = "0.0087" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_003.Top = 13.625!
        Me.S210_003.Width = 1.0!
        '
        'Label154
        '
        Me.Label154.Height = 0.1875!
        Me.Label154.HyperLink = Nothing
        Me.Label154.Left = 3.5!
        Me.Label154.Name = "Label154"
        Me.Label154.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label154.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "gap between BRP and beam flange" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label154.Top = 13.625!
        Me.Label154.Width = 4.5!
        '
        'Label155
        '
        Me.Label155.Height = 0.1875!
        Me.Label155.HyperLink = Nothing
        Me.Label155.Left = 0!
        Me.Label155.Name = "Label155"
        Me.Label155.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label155.Text = "Iy=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label155.Top = 13.812!
        Me.Label155.Width = 2.5!
        '
        'S210_004
        '
        Me.S210_004.Height = 0.1875!
        Me.S210_004.HyperLink = Nothing
        Me.S210_004.Left = 2.5!
        Me.S210_004.Name = "S210_004"
        Me.S210_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_004.Text = "0.036" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_004.Top = 13.812!
        Me.S210_004.Width = 1.0!
        '
        'Label157
        '
        Me.Label157.Height = 0.1875!
        Me.Label157.HyperLink = Nothing
        Me.Label157.Left = 3.5!
        Me.Label157.Name = "Label157"
        Me.Label157.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label157.Text = "in^4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label157.Top = 13.812!
        Me.Label157.Width = 4.5!
        '
        'Label158
        '
        Me.Label158.Height = 0.1875!
        Me.Label158.HyperLink = Nothing
        Me.Label158.Left = 0!
        Me.Label158.Name = "Label158"
        Me.Label158.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label158.Text = "lo=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label158.Top = 14.0!
        Me.Label158.Width = 2.5!
        '
        'S210_005
        '
        Me.S210_005.Height = 0.1875!
        Me.S210_005.HyperLink = Nothing
        Me.S210_005.Left = 2.5!
        Me.S210_005.Name = "S210_005"
        Me.S210_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_005.Text = "0.714" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_005.Top = 14.0!
        Me.S210_005.Width = 1.0!
        '
        'Label160
        '
        Me.Label160.Height = 0.1875!
        Me.Label160.HyperLink = Nothing
        Me.Label160.Left = 3.5!
        Me.Label160.Name = "Label160"
        Me.Label160.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label160.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "effective buckling wave length" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label160.Top = 14.0!
        Me.Label160.Width = 4.5!
        '
        'Label161
        '
        Me.Label161.Height = 0.1875!
        Me.Label161.HyperLink = Nothing
        Me.Label161.Left = 0!
        Me.Label161.Name = "Label161"
        Me.Label161.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label161.Text = "Qi=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label161.Top = 14.187!
        Me.Label161.Width = 2.5!
        '
        'S210_006
        '
        Me.S210_006.Height = 0.1875!
        Me.S210_006.HyperLink = Nothing
        Me.S210_006.Left = 2.5!
        Me.S210_006.Name = "S210_006"
        Me.S210_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_006.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_006.Top = 14.187!
        Me.S210_006.Width = 1.0!
        '
        'Label163
        '
        Me.Label163.Height = 0.1875!
        Me.Label163.HyperLink = Nothing
        Me.Label163.Left = 3.5!
        Me.Label163.Name = "Label163"
        Me.Label163.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label163.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label163.Top = 14.187!
        Me.Label163.Width = 4.5!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 4.337!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label10.Text = "kips.in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 0.375!
        Me.Label10.Width = 0.5010004!
        '
        'headerS24
        '
        Me.headerS24.Height = 0.1875!
        Me.headerS24.HyperLink = Nothing
        Me.headerS24.Left = 0!
        Me.headerS24.Name = "headerS24"
        Me.headerS24.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS24.Text = "2.4 LINK FLANGE GEOMETRY:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS24.Top = 2.812!
        Me.headerS24.Width = 3.0!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "Thickness (t_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label12.Top = 3.0!
        Me.Label12.Width = 2.5!
        '
        'S24_001
        '
        Me.S24_001.Height = 0.1875!
        Me.S24_001.HyperLink = Nothing
        Me.S24_001.Left = 2.5!
        Me.S24_001.Name = "S24_001"
        Me.S24_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S24_001.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S24_001.Top = 3.0!
        Me.S24_001.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.5!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label16.Text = "in"
        Me.Label16.Top = 3.0!
        Me.Label16.Width = 0.25!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 0!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label17.Text = "Flange Width (W_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label17.Top = 3.188!
        Me.Label17.Width = 2.5!
        '
        'S24_002
        '
        Me.S24_002.Height = 0.1875!
        Me.S24_002.HyperLink = Nothing
        Me.S24_002.Left = 2.5!
        Me.S24_002.Name = "S24_002"
        Me.S24_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S24_002.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S24_002.Top = 3.188!
        Me.S24_002.Width = 1.0!
        '
        'Label168
        '
        Me.Label168.Height = 0.1875!
        Me.Label168.HyperLink = Nothing
        Me.Label168.Left = 3.5!
        Me.Label168.Name = "Label168"
        Me.Label168.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label168.Text = "in"
        Me.Label168.Top = 3.188!
        Me.Label168.Width = 0.25!
        '
        'Label169
        '
        Me.Label169.Height = 0.1875!
        Me.Label169.HyperLink = Nothing
        Me.Label169.Left = 0!
        Me.Label169.Name = "Label169"
        Me.Label169.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label169.Text = "Flange height (H_flange) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label169.Top = 3.375!
        Me.Label169.Width = 2.5!
        '
        'S24_003
        '
        Me.S24_003.Height = 0.1875!
        Me.S24_003.HyperLink = Nothing
        Me.S24_003.Left = 2.5!
        Me.S24_003.Name = "S24_003"
        Me.S24_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S24_003.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S24_003.Top = 3.375!
        Me.S24_003.Width = 1.0!
        '
        'Label171
        '
        Me.Label171.Height = 0.1875!
        Me.Label171.HyperLink = Nothing
        Me.Label171.Left = 3.5!
        Me.Label171.Name = "Label171"
        Me.Label171.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label171.Text = "in"
        Me.Label171.Top = 3.375!
        Me.Label171.Width = 0.25!
        '
        'Label172
        '
        Me.Label172.Height = 0.1875!
        Me.Label172.HyperLink = Nothing
        Me.Label172.Left = 0.01!
        Me.Label172.Name = "Label172"
        Me.Label172.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label172.Text = "Ry_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label172.Top = 10.416!
        Me.Label172.Width = 2.5!
        '
        'S28_010
        '
        Me.S28_010.Height = 0.1875!
        Me.S28_010.HyperLink = Nothing
        Me.S28_010.Left = 2.51!
        Me.S28_010.Name = "S28_010"
        Me.S28_010.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S28_010.Text = "1.100" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_010.Top = 10.416!
        Me.S28_010.Width = 1.0!
        '
        'Label174
        '
        Me.Label174.Height = 0.188!
        Me.Label174.HyperLink = Nothing
        Me.Label174.Left = 3.51!
        Me.Label174.Name = "Label174"
        Me.Label174.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label174.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "User Input" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label174.Top = 10.416!
        Me.Label174.Width = 1.5!
        '
        'Label175
        '
        Me.Label175.Height = 0.1875!
        Me.Label175.HyperLink = Nothing
        Me.Label175.Left = 0.01!
        Me.Label175.Name = "Label175"
        Me.Label175.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label175.Text = "Fu_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label175.Top = 10.602!
        Me.Label175.Width = 2.5!
        '
        'S28_011
        '
        Me.S28_011.Height = 0.1875!
        Me.S28_011.HyperLink = Nothing
        Me.S28_011.Left = 2.51!
        Me.S28_011.Name = "S28_011"
        Me.S28_011.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S28_011.Text = "65.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_011.Top = 10.602!
        Me.S28_011.Width = 1.0!
        '
        'Label177
        '
        Me.Label177.Height = 0.188!
        Me.Label177.HyperLink = Nothing
        Me.Label177.Left = 3.51!
        Me.Label177.Name = "Label177"
        Me.Label177.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label177.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "User Input" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label177.Top = 10.602!
        Me.Label177.Width = 1.5!
        '
        'Label178
        '
        Me.Label178.Height = 0.1875!
        Me.Label178.HyperLink = Nothing
        Me.Label178.Left = 0.01!
        Me.Label178.Name = "Label178"
        Me.Label178.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label178.Text = "Rt_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label178.Top = 10.79!
        Me.Label178.Width = 2.5!
        '
        'S28_012
        '
        Me.S28_012.Height = 0.1875!
        Me.S28_012.HyperLink = Nothing
        Me.S28_012.Left = 2.51!
        Me.S28_012.Name = "S28_012"
        Me.S28_012.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S28_012.Text = "1.200" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S28_012.Top = 10.79!
        Me.S28_012.Width = 1.0!
        '
        'Label180
        '
        Me.Label180.Height = 0.188!
        Me.Label180.HyperLink = Nothing
        Me.Label180.Left = 3.51!
        Me.Label180.Name = "Label180"
        Me.Label180.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label180.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "User Input" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label180.Top = 10.79!
        Me.Label180.Width = 1.5!
        '
        'headerS29
        '
        Me.headerS29.Height = 0.1875!
        Me.headerS29.HyperLink = Nothing
        Me.headerS29.Left = 0!
        Me.headerS29.Name = "headerS29"
        Me.headerS29.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS29.Text = "2.9 BRP THICKNESS CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS29.Top = 11.125!
        Me.headerS29.Width = 5.0!
        '
        'Label182
        '
        Me.Label182.Height = 0.1875!
        Me.Label182.HyperLink = Nothing
        Me.Label182.Left = 0!
        Me.Label182.Name = "Label182"
        Me.Label182.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label182.Text = "Pcap_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label182.Top = 11.312!
        Me.Label182.Width = 2.5!
        '
        'S29_001
        '
        Me.S29_001.Height = 0.1875!
        Me.S29_001.HyperLink = Nothing
        Me.S29_001.Left = 2.5!
        Me.S29_001.Name = "S29_001"
        Me.S29_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S29_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_001.Top = 11.312!
        Me.S29_001.Width = 1.0!
        '
        'Label184
        '
        Me.Label184.Height = 0.1875!
        Me.Label184.HyperLink = Nothing
        Me.Label184.Left = 3.5!
        Me.Label184.Name = "Label184"
        Me.Label184.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label184.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=if(R=3,Py_link, Pr_link)"
        Me.Label184.Top = 11.312!
        Me.Label184.Width = 4.5!
        '
        'Label185
        '
        Me.Label185.Height = 0.1875!
        Me.Label185.HyperLink = Nothing
        Me.Label185.Left = 0!
        Me.Label185.Name = "Label185"
        Me.Label185.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label185.Text = "Lelong=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label185.Top = 11.5!
        Me.Label185.Width = 2.5!
        '
        'S29_002
        '
        Me.S29_002.Height = 0.1875!
        Me.S29_002.HyperLink = Nothing
        Me.S29_002.Left = 2.5!
        Me.S29_002.Name = "S29_002"
        Me.S29_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S29_002.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_002.Top = 11.5!
        Me.S29_002.Width = 1.0!
        '
        'Label187
        '
        Me.Label187.Height = 0.1875!
        Me.Label187.HyperLink = Nothing
        Me.Label187.Left = 3.5!
        Me.Label187.Name = "Label187"
        Me.Label187.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label187.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Elongated distance at 0.05 rad" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label187.Top = 11.5!
        Me.Label187.Width = 4.5!
        '
        'Label188
        '
        Me.Label188.Height = 0.1875!
        Me.Label188.HyperLink = Nothing
        Me.Label188.Left = 0.0000002384186!
        Me.Label188.Name = "Label188"
        Me.Label188.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label188.Text = "L1st_bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label188.Top = 11.687!
        Me.Label188.Width = 2.5!
        '
        'S29_003
        '
        Me.S29_003.Height = 0.1875!
        Me.S29_003.HyperLink = Nothing
        Me.S29_003.Left = 2.5!
        Me.S29_003.Name = "S29_003"
        Me.S29_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S29_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_003.Top = 11.687!
        Me.S29_003.Width = 1.0!
        '
        'Label190
        '
        Me.Label190.Height = 0.1875!
        Me.Label190.HyperLink = Nothing
        Me.Label190.Left = 3.5!
        Me.Label190.Name = "Label190"
        Me.Label190.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label190.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Start of yielding region to 1st BRP bolt centerline" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label190.Top = 11.687!
        Me.Label190.Width = 4.5!
        '
        'Label191
        '
        Me.Label191.Height = 0.1875!
        Me.Label191.HyperLink = Nothing
        Me.Label191.Left = 0!
        Me.Label191.Name = "Label191"
        Me.Label191.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label191.Text = "Lcant=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label191.Top = 11.875!
        Me.Label191.Width = 2.5!
        '
        'S29_004
        '
        Me.S29_004.Height = 0.1875!
        Me.S29_004.HyperLink = Nothing
        Me.S29_004.Left = 2.5!
        Me.S29_004.Name = "S29_004"
        Me.S29_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S29_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_004.Top = 11.875!
        Me.S29_004.Width = 1.0!
        '
        'Label193
        '
        Me.Label193.Height = 0.1875!
        Me.Label193.HyperLink = Nothing
        Me.Label193.Left = 3.5!
        Me.Label193.Name = "Label193"
        Me.Label193.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label193.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Lever arm from edge of yield link to edge of bolt hole + elongated dist at 0.0" &
    "5 rad" & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label193.Top = 11.875!
        Me.Label193.Width = 5.0!
        '
        'Label194
        '
        Me.Label194.Height = 0.1875!
        Me.Label194.HyperLink = Nothing
        Me.Label194.Left = 0!
        Me.Label194.Name = "Label194"
        Me.Label194.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label194.Text = "bn_brp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label194.Top = 12.063!
        Me.Label194.Width = 2.5!
        '
        'S29_005
        '
        Me.S29_005.Height = 0.1875!
        Me.S29_005.HyperLink = Nothing
        Me.S29_005.Left = 2.5!
        Me.S29_005.Name = "S29_005"
        Me.S29_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S29_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_005.Top = 12.063!
        Me.S29_005.Width = 1.0!
        '
        'Label196
        '
        Me.Label196.Height = 0.1875!
        Me.Label196.HyperLink = Nothing
        Me.Label196.Left = 3.5!
        Me.Label196.Name = "Label196"
        Me.Label196.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label196.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Net width of BRP, without bolt holes" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label196.Top = 12.063!
        Me.Label196.Width = 4.5!
        '
        'Label197
        '
        Me.Label197.Height = 0.1875!
        Me.Label197.HyperLink = Nothing
        Me.Label197.Left = 0!
        Me.Label197.Name = "Label197"
        Me.Label197.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label197.Text = "tbrp,min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label197.Top = 12.251!
        Me.Label197.Width = 2.5!
        '
        'S29_006
        '
        Me.S29_006.Height = 0.1875!
        Me.S29_006.HyperLink = Nothing
        Me.S29_006.Left = 2.5!
        Me.S29_006.Name = "S29_006"
        Me.S29_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S29_006.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_006.Top = 12.251!
        Me.S29_006.Width = 1.0!
        '
        'Label199
        '
        Me.Label199.Height = 0.1875!
        Me.Label199.HyperLink = Nothing
        Me.Label199.Left = 3.5!
        Me.Label199.Name = "Label199"
        Me.Label199.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label199.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "EQ 10.1-1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label199.Top = 12.251!
        Me.Label199.Width = 4.5!
        '
        'Label200
        '
        Me.Label200.Height = 0.1875!
        Me.Label200.HyperLink = Nothing
        Me.Label200.Left = 0!
        Me.Label200.Name = "Label200"
        Me.Label200.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label200.Text = "tbrp_use=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label200.Top = 12.438!
        Me.Label200.Width = 2.5!
        '
        'S29_007
        '
        Me.S29_007.Height = 0.1875!
        Me.S29_007.HyperLink = Nothing
        Me.S29_007.Left = 2.5!
        Me.S29_007.Name = "S29_007"
        Me.S29_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S29_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_007.Top = 12.438!
        Me.S29_007.Width = 1.0!
        '
        'Label202
        '
        Me.Label202.Height = 0.1875!
        Me.Label202.HyperLink = Nothing
        Me.Label202.Left = 3.5!
        Me.Label202.Name = "Label202"
        Me.Label202.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label202.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label202.Top = 12.438!
        Me.Label202.Width = 4.5!
        '
        'Label203
        '
        Me.Label203.Height = 0.1875!
        Me.Label203.HyperLink = Nothing
        Me.Label203.Left = 0!
        Me.Label203.Name = "Label203"
        Me.Label203.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label203.Text = "tbrp_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label203.Top = 12.626!
        Me.Label203.Width = 2.5!
        '
        'S29_008
        '
        Me.S29_008.Height = 0.1875!
        Me.S29_008.HyperLink = Nothing
        Me.S29_008.Left = 2.5!
        Me.S29_008.Name = "S29_008"
        Me.S29_008.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S29_008.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S29_008.Top = 12.626!
        Me.S29_008.Width = 1.0!
        '
        'Label205
        '
        Me.Label205.Height = 0.1875!
        Me.Label205.HyperLink = Nothing
        Me.Label205.Left = 0!
        Me.Label205.Name = "Label205"
        Me.Label205.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label205.Text = "Check="
        Me.Label205.Top = 12.814!
        Me.Label205.Width = 2.5!
        '
        'headerS210
        '
        Me.headerS210.Height = 0.1875!
        Me.headerS210.HyperLink = Nothing
        Me.headerS210.Left = 0!
        Me.headerS210.Name = "headerS210"
        Me.headerS210.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS210.Text = "2.10 BEAM FLANGE THICKNESS CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS210.Top = 13.062!
        Me.headerS210.Width = 5.0!
        '
        'Label208
        '
        Me.Label208.Height = 0.1875!
        Me.Label208.HyperLink = Nothing
        Me.Label208.Left = 0!
        Me.Label208.Name = "Label208"
        Me.Label208.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label208.Text = "Joint Rotation=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label208.Top = 13.25!
        Me.Label208.Width = 2.5!
        '
        'S210_001
        '
        Me.S210_001.Height = 0.1875!
        Me.S210_001.HyperLink = Nothing
        Me.S210_001.Left = 2.5!
        Me.S210_001.Name = "S210_001"
        Me.S210_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S210_001.Text = "0.04" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_001.Top = 13.25!
        Me.S210_001.Width = 1.0!
        '
        'Label210
        '
        Me.Label210.Height = 0.1875!
        Me.Label210.HyperLink = Nothing
        Me.Label210.Left = 3.5!
        Me.Label210.Name = "Label210"
        Me.Label210.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label210.Text = "rad" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label210.Top = 13.25!
        Me.Label210.Width = 4.5!
        '
        'Label211
        '
        Me.Label211.Height = 0.1875!
        Me.Label211.HyperLink = Nothing
        Me.Label211.Left = 0!
        Me.Label211.Name = "Label211"
        Me.Label211.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label211.Text = "e0.04=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label211.Top = 13.437!
        Me.Label211.Width = 2.5!
        '
        'S210_002
        '
        Me.S210_002.Height = 0.1875!
        Me.S210_002.HyperLink = Nothing
        Me.S210_002.Left = 2.5!
        Me.S210_002.Name = "S210_002"
        Me.S210_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_002.Text = "0.0697" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_002.Top = 13.437!
        Me.S210_002.Width = 1.0!
        '
        'Label213
        '
        Me.Label213.Height = 0.1875!
        Me.Label213.HyperLink = Nothing
        Me.Label213.Left = 3.5!
        Me.Label213.Name = "Label213"
        Me.Label213.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label213.Text = "in/in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label213.Top = 13.437!
        Me.Label213.Width = 4.5!
        '
        'Label123
        '
        Me.Label123.Height = 0.1875!
        Me.Label123.HyperLink = Nothing
        Me.Label123.Left = 3.5!
        Me.Label123.Name = "Label123"
        Me.Label123.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label123.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if tbrp_DCR < SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label123.Top = 12.814!
        Me.Label123.Width = 4.5!
        '
        'Label124
        '
        Me.Label124.Height = 0.1875!
        Me.Label124.HyperLink = Nothing
        Me.Label124.Left = 0!
        Me.Label124.Name = "Label124"
        Me.Label124.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label124.Text = "N=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label124.Top = 14.375!
        Me.Label124.Width = 2.5!
        '
        'S210_007
        '
        Me.S210_007.Height = 0.1875!
        Me.S210_007.HyperLink = Nothing
        Me.S210_007.Left = 2.5!
        Me.S210_007.Name = "S210_007"
        Me.S210_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_007.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_007.Top = 14.375!
        Me.S210_007.Width = 1.0!
        '
        'Label126
        '
        Me.Label126.Height = 0.1875!
        Me.Label126.HyperLink = Nothing
        Me.Label126.Left = 3.5!
        Me.Label126.Name = "Label126"
        Me.Label126.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label126.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=LstemYield / lo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label126.Top = 14.375!
        Me.Label126.Width = 4.5!
        '
        'Label127
        '
        Me.Label127.Height = 0.1875!
        Me.Label127.HyperLink = Nothing
        Me.Label127.Left = 0!
        Me.Label127.Name = "Label127"
        Me.Label127.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label127.Text = "N=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label127.Top = 14.562!
        Me.Label127.Width = 2.5!
        '
        'S210_008
        '
        Me.S210_008.Height = 0.1875!
        Me.S210_008.HyperLink = Nothing
        Me.S210_008.Left = 2.5!
        Me.S210_008.Name = "S210_008"
        Me.S210_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_008.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_008.Top = 14.562!
        Me.S210_008.Width = 1.0!
        '
        'Label129
        '
        Me.Label129.Height = 0.1875!
        Me.Label129.HyperLink = Nothing
        Me.Label129.Left = 3.5!
        Me.Label129.Name = "Label129"
        Me.Label129.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label129.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Round down to next integer" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label129.Top = 14.562!
        Me.Label129.Width = 4.5!
        '
        'Label130
        '
        Me.Label130.Height = 0.1875!
        Me.Label130.HyperLink = Nothing
        Me.Label130.Left = 0!
        Me.Label130.Name = "Label130"
        Me.Label130.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label130.Text = "Ndesign=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label130.Top = 14.75!
        Me.Label130.Width = 2.5!
        '
        'S210_009
        '
        Me.S210_009.Height = 0.1875!
        Me.S210_009.HyperLink = Nothing
        Me.S210_009.Left = 2.5!
        Me.S210_009.Name = "S210_009"
        Me.S210_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_009.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_009.Top = 14.75!
        Me.S210_009.Width = 1.0!
        '
        'Label132
        '
        Me.Label132.Height = 0.1875!
        Me.Label132.HyperLink = Nothing
        Me.Label132.Left = 3.5!
        Me.Label132.Name = "Label132"
        Me.Label132.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label132.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "N used for design (Contact points with BRP)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label132.Top = 14.75!
        Me.Label132.Width = 4.5!
        '
        'Label133
        '
        Me.Label133.Height = 0.1875!
        Me.Label133.HyperLink = Nothing
        Me.Label133.Left = 0!
        Me.Label133.Name = "Label133"
        Me.Label133.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label133.Text = "Tux=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label133.Top = 14.937!
        Me.Label133.Width = 2.5!
        '
        'S210_010
        '
        Me.S210_010.Height = 0.1875!
        Me.S210_010.HyperLink = Nothing
        Me.S210_010.Left = 2.5!
        Me.S210_010.Name = "S210_010"
        Me.S210_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_010.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_010.Top = 14.937!
        Me.S210_010.Width = 1.0!
        '
        'Label135
        '
        Me.Label135.Height = 0.1875!
        Me.Label135.HyperLink = Nothing
        Me.Label135.Left = 3.5!
        Me.Label135.Name = "Label135"
        Me.Label135.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label135.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Ndesign * Qi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label135.Top = 14.937!
        Me.Label135.Width = 4.5!
        '
        'Label136
        '
        Me.Label136.Height = 0.1875!
        Me.Label136.HyperLink = Nothing
        Me.Label136.Left = 0!
        Me.Label136.Name = "Label136"
        Me.Label136.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label136.Text = "Tux_bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label136.Top = 15.125!
        Me.Label136.Width = 2.5!
        '
        'S210_011
        '
        Me.S210_011.Height = 0.1875!
        Me.S210_011.HyperLink = Nothing
        Me.S210_011.Left = 2.5!
        Me.S210_011.Name = "S210_011"
        Me.S210_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_011.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_011.Top = 15.125!
        Me.S210_011.Width = 1.0!
        '
        'Label138
        '
        Me.Label138.Height = 0.1875!
        Me.Label138.HyperLink = Nothing
        Me.Label138.Left = 3.5!
        Me.Label138.Name = "Label138"
        Me.Label138.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label138.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Tux/Nbolts" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label138.Top = 15.125!
        Me.Label138.Width = 4.5!
        '
        'Label139
        '
        Me.Label139.Height = 0.1875!
        Me.Label139.HyperLink = Nothing
        Me.Label139.Left = 0!
        Me.Label139.Name = "Label139"
        Me.Label139.Style = "font-family: Arial; font-size: 8.25pt; text-align: right; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label139.Text = "Φ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label139.Top = 15.312!
        Me.Label139.Width = 2.5!
        '
        'S210_012
        '
        Me.S210_012.Height = 0.1875!
        Me.S210_012.HyperLink = Nothing
        Me.S210_012.Left = 2.5!
        Me.S210_012.Name = "S210_012"
        Me.S210_012.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S210_012.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_012.Top = 15.312!
        Me.S210_012.Width = 1.0!
        '
        'Label142
        '
        Me.Label142.Height = 0.1875!
        Me.Label142.HyperLink = Nothing
        Me.Label142.Left = 0!
        Me.Label142.Name = "Label142"
        Me.Label142.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label142.Text = "Ledge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label142.Top = 15.5!
        Me.Label142.Width = 2.5!
        '
        'S210_013
        '
        Me.S210_013.Height = 0.1875!
        Me.S210_013.HyperLink = Nothing
        Me.S210_013.Left = 2.5!
        Me.S210_013.Name = "S210_013"
        Me.S210_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S210_013.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_013.Top = 15.5!
        Me.S210_013.Width = 1.0!
        '
        'Label144
        '
        Me.Label144.Height = 0.1875!
        Me.Label144.HyperLink = Nothing
        Me.Label144.Left = 3.5!
        Me.Label144.Name = "Label144"
        Me.Label144.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label144.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "1st bolt to edge of BRP" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label144.Top = 15.5!
        Me.Label144.Width = 4.5!
        '
        'Label145
        '
        Me.Label145.Height = 0.1875!
        Me.Label145.HyperLink = Nothing
        Me.Label145.Left = 0!
        Me.Label145.Name = "Label145"
        Me.Label145.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label145.Text = "S_brpBolt_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label145.Top = 15.688!
        Me.Label145.Width = 2.5!
        '
        'S210_014
        '
        Me.S210_014.Height = 0.1875!
        Me.S210_014.HyperLink = Nothing
        Me.S210_014.Left = 2.5!
        Me.S210_014.Name = "S210_014"
        Me.S210_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_014.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_014.Top = 15.688!
        Me.S210_014.Width = 1.0!
        '
        'Label147
        '
        Me.Label147.Height = 0.1875!
        Me.Label147.HyperLink = Nothing
        Me.Label147.Left = 3.5!
        Me.Label147.Name = "Label147"
        Me.Label147.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label147.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=byield + 2*(0.125 + d_brp_bolt)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label147.Top = 15.688!
        Me.Label147.Width = 4.5!
        '
        'Label148
        '
        Me.Label148.Height = 0.1875!
        Me.Label148.HyperLink = Nothing
        Me.Label148.Left = 0!
        Me.Label148.Name = "Label148"
        Me.Label148.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label148.Text = "b=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label148.Top = 16.063!
        Me.Label148.Width = 2.5!
        '
        'S210_016
        '
        Me.S210_016.Height = 0.1875!
        Me.S210_016.HyperLink = Nothing
        Me.S210_016.Left = 2.5!
        Me.S210_016.Name = "S210_016"
        Me.S210_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_016.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_016.Top = 16.063!
        Me.S210_016.Width = 1.0!
        '
        'Label150
        '
        Me.Label150.Height = 0.1875!
        Me.Label150.HyperLink = Nothing
        Me.Label150.Left = 3.5!
        Me.Label150.Name = "Label150"
        Me.Label150.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label150.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "BRP bolt to face of web" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label150.Top = 16.063!
        Me.Label150.Width = 4.5!
        '
        'Label151
        '
        Me.Label151.Height = 0.1875!
        Me.Label151.HyperLink = Nothing
        Me.Label151.Left = 0!
        Me.Label151.Name = "Label151"
        Me.Label151.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label151.Text = "c=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label151.Top = 16.25!
        Me.Label151.Width = 2.5!
        '
        'S210_017
        '
        Me.S210_017.Height = 0.1875!
        Me.S210_017.HyperLink = Nothing
        Me.S210_017.Left = 2.5!
        Me.S210_017.Name = "S210_017"
        Me.S210_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_017.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_017.Top = 16.25!
        Me.S210_017.Width = 1.0!
        '
        'Label165
        '
        Me.Label165.Height = 0.1875!
        Me.Label165.HyperLink = Nothing
        Me.Label165.Left = 3.5!
        Me.Label165.Name = "Label165"
        Me.Label165.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label165.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "edge of beam flange to face of web" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label165.Top = 16.25!
        Me.Label165.Width = 4.5!
        '
        'Label166
        '
        Me.Label166.Height = 0.1875!
        Me.Label166.HyperLink = Nothing
        Me.Label166.Left = 0!
        Me.Label166.Name = "Label166"
        Me.Label166.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label166.Text = "a=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label166.Top = 16.437!
        Me.Label166.Width = 2.5!
        '
        'S210_018
        '
        Me.S210_018.Height = 0.1875!
        Me.S210_018.HyperLink = Nothing
        Me.S210_018.Left = 2.5!
        Me.S210_018.Name = "S210_018"
        Me.S210_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_018.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_018.Top = 16.437!
        Me.S210_018.Width = 1.0!
        '
        'Label215
        '
        Me.Label215.Height = 0.1875!
        Me.Label215.HyperLink = Nothing
        Me.Label215.Left = 3.5!
        Me.Label215.Name = "Label215"
        Me.Label215.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label215.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "center of BRP bolt to edge of beam flange" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label215.Top = 16.437!
        Me.Label215.Width = 4.5!
        '
        'Label216
        '
        Me.Label216.Height = 0.1875!
        Me.Label216.HyperLink = Nothing
        Me.Label216.Left = 0!
        Me.Label216.Name = "Label216"
        Me.Label216.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label216.Text = "x=sqrt(b*c)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label216.Top = 16.625!
        Me.Label216.Width = 2.5!
        '
        'S210_019
        '
        Me.S210_019.Height = 0.1875!
        Me.S210_019.HyperLink = Nothing
        Me.S210_019.Left = 2.5!
        Me.S210_019.Name = "S210_019"
        Me.S210_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_019.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_019.Top = 16.625!
        Me.S210_019.Width = 1.0!
        '
        'Label218
        '
        Me.Label218.Height = 0.1875!
        Me.Label218.HyperLink = Nothing
        Me.Label218.Left = 3.5!
        Me.Label218.Name = "Label218"
        Me.Label218.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label218.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label218.Top = 16.625!
        Me.Label218.Width = 0.25!
        '
        'Label219
        '
        Me.Label219.Height = 0.1875!
        Me.Label219.HyperLink = Nothing
        Me.Label219.Left = 0!
        Me.Label219.Name = "Label219"
        Me.Label219.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label219.Text = "4*x=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label219.Top = 16.812!
        Me.Label219.Width = 2.5!
        '
        'S210_020
        '
        Me.S210_020.Height = 0.1875!
        Me.S210_020.HyperLink = Nothing
        Me.S210_020.Left = 2.5!
        Me.S210_020.Name = "S210_020"
        Me.S210_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_020.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_020.Top = 16.812!
        Me.S210_020.Width = 1.0!
        '
        'Label221
        '
        Me.Label221.Height = 0.1875!
        Me.Label221.HyperLink = Nothing
        Me.Label221.Left = 3.5!
        Me.Label221.Name = "Label221"
        Me.Label221.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label221.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label221.Top = 16.812!
        Me.Label221.Width = 0.25!
        '
        'Label222
        '
        Me.Label222.Height = 0.1875!
        Me.Label222.HyperLink = Nothing
        Me.Label222.Left = 0!
        Me.Label222.Name = "Label222"
        Me.Label222.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label222.Text = "tf_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label222.Top = 17.75!
        Me.Label222.Width = 2.5!
        '
        'S210_025
        '
        Me.S210_025.Height = 0.1875!
        Me.S210_025.HyperLink = Nothing
        Me.S210_025.Left = 2.5!
        Me.S210_025.Name = "S210_025"
        Me.S210_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_025.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_025.Top = 17.75!
        Me.S210_025.Width = 1.0!
        '
        'Label224
        '
        Me.Label224.Height = 0.188!
        Me.Label224.HyperLink = Nothing
        Me.Label224.Left = 3.5!
        Me.Label224.Name = "Label224"
        Me.Label224.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label224.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=max(sqrt(4*b'*T/(Φ*pe*Fu_bm)),0.4)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label224.Top = 17.75!
        Me.Label224.Width = 2.25!
        '
        'Label141
        '
        Me.Label141.Height = 0.1875!
        Me.Label141.HyperLink = Nothing
        Me.Label141.Left = 0!
        Me.Label141.Name = "Label141"
        Me.Label141.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label141.Text = "gbrp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label141.Top = 15.876!
        Me.Label141.Width = 2.5!
        '
        'S210_015
        '
        Me.S210_015.Height = 0.1875!
        Me.S210_015.HyperLink = Nothing
        Me.S210_015.Left = 2.5!
        Me.S210_015.Name = "S210_015"
        Me.S210_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S210_015.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_015.Top = 15.876!
        Me.S210_015.Width = 1.0!
        '
        'Label226
        '
        Me.Label226.Height = 0.1875!
        Me.Label226.HyperLink = Nothing
        Me.Label226.Left = 3.5!
        Me.Label226.Name = "Label226"
        Me.Label226.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label226.Text = "in"
        Me.Label226.Top = 15.876!
        Me.Label226.Width = 4.5!
        '
        'Label227
        '
        Me.Label227.Height = 0.1875!
        Me.Label227.HyperLink = Nothing
        Me.Label227.Left = 0!
        Me.Label227.Name = "Label227"
        Me.Label227.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label227.Text = "2*x=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label227.Top = 17.0!
        Me.Label227.Width = 2.5!
        '
        'S210_021
        '
        Me.S210_021.Height = 0.1875!
        Me.S210_021.HyperLink = Nothing
        Me.S210_021.Left = 2.5!
        Me.S210_021.Name = "S210_021"
        Me.S210_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_021.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_021.Top = 17.0!
        Me.S210_021.Width = 1.0!
        '
        'Label229
        '
        Me.Label229.Height = 0.1875!
        Me.Label229.HyperLink = Nothing
        Me.Label229.Left = 3.5!
        Me.Label229.Name = "Label229"
        Me.Label229.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label229.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label229.Top = 17.0!
        Me.Label229.Width = 0.25!
        '
        'Label230
        '
        Me.Label230.Height = 0.1875!
        Me.Label230.HyperLink = Nothing
        Me.Label230.Left = 0!
        Me.Label230.Name = "Label230"
        Me.Label230.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label230.Text = "S_brp_bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label230.Top = 17.187!
        Me.Label230.Width = 2.5!
        '
        'S210_022
        '
        Me.S210_022.Height = 0.1875!
        Me.S210_022.HyperLink = Nothing
        Me.S210_022.Left = 2.5!
        Me.S210_022.Name = "S210_022"
        Me.S210_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_022.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_022.Top = 17.187!
        Me.S210_022.Width = 1.0!
        '
        'Label232
        '
        Me.Label232.Height = 0.1875!
        Me.Label232.HyperLink = Nothing
        Me.Label232.Left = 3.5!
        Me.Label232.Name = "Label232"
        Me.Label232.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label232.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label232.Top = 17.187!
        Me.Label232.Width = 0.25!
        '
        'Label233
        '
        Me.Label233.Height = 0.1875!
        Me.Label233.HyperLink = Nothing
        Me.Label233.Left = 0!
        Me.Label233.Name = "Label233"
        Me.Label233.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label233.Text = "pe=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label233.Top = 17.375!
        Me.Label233.Width = 2.5!
        '
        'S210_023
        '
        Me.S210_023.Height = 0.1875!
        Me.S210_023.HyperLink = Nothing
        Me.S210_023.Left = 2.5!
        Me.S210_023.Name = "S210_023"
        Me.S210_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_023.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_023.Top = 17.375!
        Me.S210_023.Width = 1.0!
        '
        'Label235
        '
        Me.Label235.Height = 0.1875!
        Me.Label235.HyperLink = Nothing
        Me.Label235.Left = 3.5!
        Me.Label235.Name = "Label235"
        Me.Label235.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label235.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label235.Top = 17.375!
        Me.Label235.Width = 0.25!
        '
        'Label236
        '
        Me.Label236.Height = 0.1875!
        Me.Label236.HyperLink = Nothing
        Me.Label236.Left = 0!
        Me.Label236.Name = "Label236"
        Me.Label236.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label236.Text = "b'=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label236.Top = 17.562!
        Me.Label236.Width = 2.5!
        '
        'S210_024
        '
        Me.S210_024.Height = 0.1875!
        Me.S210_024.HyperLink = Nothing
        Me.S210_024.Left = 2.5!
        Me.S210_024.Name = "S210_024"
        Me.S210_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_024.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_024.Top = 17.562!
        Me.S210_024.Width = 1.0!
        '
        'Label238
        '
        Me.Label238.Height = 0.1875!
        Me.Label238.HyperLink = Nothing
        Me.Label238.Left = 3.5!
        Me.Label238.Name = "Label238"
        Me.Label238.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label238.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label238.Top = 17.562!
        Me.Label238.Width = 0.25!
        '
        'Label239
        '
        Me.Label239.Height = 0.1875!
        Me.Label239.HyperLink = Nothing
        Me.Label239.Left = 0!
        Me.Label239.Name = "Label239"
        Me.Label239.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label239.Text = "tbf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label239.Top = 17.938!
        Me.Label239.Width = 2.5!
        '
        'S210_026
        '
        Me.S210_026.Height = 0.1875!
        Me.S210_026.HyperLink = Nothing
        Me.S210_026.Left = 2.5!
        Me.S210_026.Name = "S210_026"
        Me.S210_026.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S210_026.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_026.Top = 17.938!
        Me.S210_026.Width = 1.0!
        '
        'Label241
        '
        Me.Label241.Height = 0.1875!
        Me.Label241.HyperLink = Nothing
        Me.Label241.Left = 3.5!
        Me.Label241.Name = "Label241"
        Me.Label241.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label241.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "beam flange thickness" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label241.Top = 17.938!
        Me.Label241.Width = 2.0!
        '
        'Label242
        '
        Me.Label242.Height = 0.1875!
        Me.Label242.HyperLink = Nothing
        Me.Label242.Left = 0!
        Me.Label242.Name = "Label242"
        Me.Label242.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label242.Text = "tbf_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label242.Top = 18.125!
        Me.Label242.Width = 2.5!
        '
        'S210_027
        '
        Me.S210_027.Height = 0.1875!
        Me.S210_027.HyperLink = Nothing
        Me.S210_027.Left = 2.5!
        Me.S210_027.Name = "S210_027"
        Me.S210_027.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S210_027.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_027.Top = 18.125!
        Me.S210_027.Width = 1.0!
        '
        'Label244
        '
        Me.Label244.Height = 0.1875!
        Me.Label244.HyperLink = Nothing
        Me.Label244.Left = 0!
        Me.Label244.Name = "Label244"
        Me.Label244.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label244.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label244.Top = 18.312!
        Me.Label244.Width = 2.5!
        '
        'Label246
        '
        Me.Label246.Height = 0.188!
        Me.Label246.HyperLink = Nothing
        Me.Label246.Left = 3.5!
        Me.Label246.Name = "Label246"
        Me.Label246.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label246.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if tbrp_DCR < SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label246.Top = 18.312!
        Me.Label246.Width = 2.0!
        '
        'Label247
        '
        Me.Label247.Height = 0.1875!
        Me.Label247.HyperLink = Nothing
        Me.Label247.Left = 3.813!
        Me.Label247.Name = "Label247"
        Me.Label247.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label247.Text = "pe_1row="
        Me.Label247.Top = 16.812!
        Me.Label247.Width = 0.7810001!
        '
        'S210_029
        '
        Me.S210_029.Height = 0.1875!
        Me.S210_029.HyperLink = Nothing
        Me.S210_029.Left = 4.594!
        Me.S210_029.Name = "S210_029"
        Me.S210_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_029.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_029.Top = 16.812!
        Me.S210_029.Width = 0.9059997!
        '
        'Label249
        '
        Me.Label249.Height = 0.1875!
        Me.Label249.HyperLink = Nothing
        Me.Label249.Left = 5.5!
        Me.Label249.Name = "Label249"
        Me.Label249.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label249.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label249.Top = 16.812!
        Me.Label249.Width = 0.25!
        '
        'Label250
        '
        Me.Label250.Height = 0.1875!
        Me.Label250.HyperLink = Nothing
        Me.Label250.Left = 4.0!
        Me.Label250.Name = "Label250"
        Me.Label250.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label250.Text = "1 row of BRP bolts:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label250.Top = 16.625!
        Me.Label250.Width = 1.500001!
        '
        'Label251
        '
        Me.Label251.Height = 0.1875!
        Me.Label251.HyperLink = Nothing
        Me.Label251.Left = 5.875001!
        Me.Label251.Name = "Label251"
        Me.Label251.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label251.Text = "pe1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label251.Top = 16.812!
        Me.Label251.Width = 0.625!
        '
        'S210_030
        '
        Me.S210_030.Height = 0.1875!
        Me.S210_030.HyperLink = Nothing
        Me.S210_030.Left = 6.500001!
        Me.S210_030.Name = "S210_030"
        Me.S210_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_030.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_030.Top = 16.812!
        Me.S210_030.Width = 1.0!
        '
        'Label253
        '
        Me.Label253.Height = 0.1875!
        Me.Label253.HyperLink = Nothing
        Me.Label253.Left = 7.5!
        Me.Label253.Name = "Label253"
        Me.Label253.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label253.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label253.Top = 16.811!
        Me.Label253.Width = 0.25!
        '
        'Label254
        '
        Me.Label254.Height = 0.1875!
        Me.Label254.HyperLink = Nothing
        Me.Label254.Left = 5.875001!
        Me.Label254.Name = "Label254"
        Me.Label254.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label254.Text = "2 rows of BRP bolts:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label254.Top = 16.625!
        Me.Label254.Width = 1.625001!
        '
        'Label255
        '
        Me.Label255.Height = 0.1875!
        Me.Label255.HyperLink = Nothing
        Me.Label255.Left = 5.875001!
        Me.Label255.Name = "Label255"
        Me.Label255.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label255.Text = "pe2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label255.Top = 17.0!
        Me.Label255.Width = 0.625!
        '
        'S210_031
        '
        Me.S210_031.Height = 0.1875!
        Me.S210_031.HyperLink = Nothing
        Me.S210_031.Left = 6.500001!
        Me.S210_031.Name = "S210_031"
        Me.S210_031.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_031.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_031.Top = 17.0!
        Me.S210_031.Width = 1.0!
        '
        'Label257
        '
        Me.Label257.Height = 0.1875!
        Me.Label257.HyperLink = Nothing
        Me.Label257.Left = 7.5!
        Me.Label257.Name = "Label257"
        Me.Label257.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label257.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label257.Top = 16.999!
        Me.Label257.Width = 0.25!
        '
        'Label258
        '
        Me.Label258.Height = 0.1875!
        Me.Label258.HyperLink = Nothing
        Me.Label258.Left = 5.875001!
        Me.Label258.Name = "Label258"
        Me.Label258.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label258.Text = "pe_2row="
        Me.Label258.Top = 17.187!
        Me.Label258.Width = 0.625!
        '
        'S210_032
        '
        Me.S210_032.Height = 0.1875!
        Me.S210_032.HyperLink = Nothing
        Me.S210_032.Left = 6.500001!
        Me.S210_032.Name = "S210_032"
        Me.S210_032.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_032.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_032.Top = 17.187!
        Me.S210_032.Width = 1.0!
        '
        'Label260
        '
        Me.Label260.Height = 0.1875!
        Me.Label260.HyperLink = Nothing
        Me.Label260.Left = 7.5!
        Me.Label260.Name = "Label260"
        Me.Label260.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label260.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label260.Top = 17.186!
        Me.Label260.Width = 0.25!
        '
        'headerS211
        '
        Me.headerS211.Height = 0.1875!
        Me.headerS211.HyperLink = Nothing
        Me.headerS211.Left = 0!
        Me.headerS211.Name = "headerS211"
        Me.headerS211.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS211.Text = "2.11 BRP BOLT SIZE AND QUANTITY CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS211.Top = 18.562!
        Me.headerS211.Width = 5.0!
        '
        'Label262
        '
        Me.Label262.Height = 0.1875!
        Me.Label262.HyperLink = Nothing
        Me.Label262.Left = 0!
        Me.Label262.Name = "Label262"
        Me.Label262.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label262.Text = "Ix=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label262.Top = 18.75!
        Me.Label262.Width = 2.5!
        '
        'S211_001
        '
        Me.S211_001.Height = 0.1875!
        Me.S211_001.HyperLink = Nothing
        Me.S211_001.Left = 2.5!
        Me.S211_001.Name = "S211_001"
        Me.S211_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_001.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_001.Top = 18.75!
        Me.S211_001.Width = 1.0!
        '
        'Label264
        '
        Me.Label264.Height = 0.1875!
        Me.Label264.HyperLink = Nothing
        Me.Label264.Left = 3.5!
        Me.Label264.Name = "Label264"
        Me.Label264.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label264.Text = "in4" & Global.Microsoft.VisualBasic.ChrW(9) & "=byield^3 * tstem /12, strong axis moment of inertia of reduced link region" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) &
    ""
        Me.Label264.Top = 18.75!
        Me.Label264.Width = 5.0!
        '
        'Label265
        '
        Me.Label265.Height = 0.1875!
        Me.Label265.HyperLink = Nothing
        Me.Label265.Left = 0!
        Me.Label265.Name = "Label265"
        Me.Label265.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label265.Text = "Vuy=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label265.Top = 18.938!
        Me.Label265.Width = 2.5!
        '
        'S211_002
        '
        Me.S211_002.Height = 0.1875!
        Me.S211_002.HyperLink = Nothing
        Me.S211_002.Left = 2.5!
        Me.S211_002.Name = "S211_002"
        Me.S211_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_002.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_002.Top = 18.938!
        Me.S211_002.Width = 1.0!
        '
        'Label267
        '
        Me.Label267.Height = 0.1875!
        Me.Label267.HyperLink = Nothing
        Me.Label267.Left = 3.5!
        Me.Label267.Name = "Label267"
        Me.Label267.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label267.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label267.Top = 18.938!
        Me.Label267.Width = 1.875!
        '
        'Label268
        '
        Me.Label268.Height = 0.1875!
        Me.Label268.HyperLink = Nothing
        Me.Label268.Left = 0!
        Me.Label268.Name = "Label268"
        Me.Label268.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label268.Text = "Vuy per bolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label268.Top = 19.126!
        Me.Label268.Width = 2.5!
        '
        'S211_003
        '
        Me.S211_003.Height = 0.1875!
        Me.S211_003.HyperLink = Nothing
        Me.S211_003.Left = 2.5!
        Me.S211_003.Name = "S211_003"
        Me.S211_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_003.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_003.Top = 19.126!
        Me.S211_003.Width = 1.0!
        '
        'Label270
        '
        Me.Label270.Height = 0.1875!
        Me.Label270.HyperLink = Nothing
        Me.Label270.Left = 3.5!
        Me.Label270.Name = "Label270"
        Me.Label270.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label270.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label270.Top = 19.126!
        Me.Label270.Width = 4.5!
        '
        'Label274
        '
        Me.Label274.Height = 0.1875!
        Me.Label274.HyperLink = Nothing
        Me.Label274.Left = 0!
        Me.Label274.Name = "Label274"
        Me.Label274.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label274.Text = "Bolt Forces=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label274.Top = 19.5!
        Me.Label274.Width = 2.5!
        '
        'S211_004
        '
        Me.S211_004.Height = 0.1875!
        Me.S211_004.HyperLink = Nothing
        Me.S211_004.Left = 2.5!
        Me.S211_004.Name = "S211_004"
        Me.S211_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S211_004.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_004.Top = 19.5!
        Me.S211_004.Width = 1.0!
        '
        'Label271
        '
        Me.Label271.Height = 0.1875!
        Me.Label271.HyperLink = Nothing
        Me.Label271.Left = 2.5!
        Me.Label271.Name = "Label271"
        Me.Label271.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label271.Text = "Tx (kips)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label271.Top = 19.312!
        Me.Label271.Width = 1.0!
        '
        'S211_019
        '
        Me.S211_019.Height = 0.1875!
        Me.S211_019.HyperLink = Nothing
        Me.S211_019.Left = 3.5!
        Me.S211_019.Name = "S211_019"
        Me.S211_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_019.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_019.Top = 19.5!
        Me.S211_019.Width = 1.0!
        '
        'Label273
        '
        Me.Label273.Height = 0.1875!
        Me.Label273.HyperLink = Nothing
        Me.Label273.Left = 3.5!
        Me.Label273.Name = "Label273"
        Me.Label273.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label273.Text = "Vx (kips)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label273.Top = 19.312!
        Me.Label273.Width = 1.0!
        '
        'S211_024
        '
        Me.S211_024.Height = 0.1875!
        Me.S211_024.HyperLink = Nothing
        Me.S211_024.Left = 4.5!
        Me.S211_024.Name = "S211_024"
        Me.S211_024.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_024.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_024.Top = 19.5!
        Me.S211_024.Width = 1.0!
        '
        'Label277
        '
        Me.Label277.Height = 0.1875!
        Me.Label277.HyperLink = Nothing
        Me.Label277.Left = 4.5!
        Me.Label277.Name = "Label277"
        Me.Label277.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label277.Text = "Ty (kips)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label277.Top = 19.312!
        Me.Label277.Width = 1.0!
        '
        'S211_025
        '
        Me.S211_025.Height = 0.1875!
        Me.S211_025.HyperLink = Nothing
        Me.S211_025.Left = 5.5!
        Me.S211_025.Name = "S211_025"
        Me.S211_025.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S211_025.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_025.Top = 19.5!
        Me.S211_025.Width = 1.0!
        '
        'Label279
        '
        Me.Label279.Height = 0.1875!
        Me.Label279.HyperLink = Nothing
        Me.Label279.Left = 5.5!
        Me.Label279.Name = "Label279"
        Me.Label279.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label279.Text = "Vy (kips)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label279.Top = 19.312!
        Me.Label279.Width = 1.0!
        '
        'Label280
        '
        Me.Label280.Height = 0.1875!
        Me.Label280.HyperLink = Nothing
        Me.Label280.Left = 0!
        Me.Label280.Name = "Label280"
        Me.Label280.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label280.Text = "Bolt Type (Bolt_Gr_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label280.Top = 19.687!
        Me.Label280.Width = 2.5!
        '
        'S211_005
        '
        Me.S211_005.Height = 0.1875!
        Me.S211_005.HyperLink = Nothing
        Me.S211_005.Left = 2.5!
        Me.S211_005.Name = "S211_005"
        Me.S211_005.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_005.Text = "A325" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_005.Top = 19.687!
        Me.S211_005.Width = 1.0!
        '
        'Label283
        '
        Me.Label283.Height = 0.1875!
        Me.Label283.HyperLink = Nothing
        Me.Label283.Left = 0!
        Me.Label283.Name = "Label283"
        Me.Label283.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label283.Text = "Thread Condition=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label283.Top = 19.875!
        Me.Label283.Width = 2.5!
        '
        'S211_006
        '
        Me.S211_006.Height = 0.1875!
        Me.S211_006.HyperLink = Nothing
        Me.S211_006.Left = 2.5!
        Me.S211_006.Name = "S211_006"
        Me.S211_006.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_006.Text = "X" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_006.Top = 19.875!
        Me.S211_006.Width = 1.0!
        '
        'Label286
        '
        Me.Label286.Height = 0.1875!
        Me.Label286.HyperLink = Nothing
        Me.Label286.Left = 0!
        Me.Label286.Name = "Label286"
        Me.Label286.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label286.Text = "Φbolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label286.Top = 20.063!
        Me.Label286.Width = 2.5!
        '
        'S211_007
        '
        Me.S211_007.Height = 0.1875!
        Me.S211_007.HyperLink = Nothing
        Me.S211_007.Left = 2.5!
        Me.S211_007.Name = "S211_007"
        Me.S211_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_007.Text = "0.750" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_007.Top = 20.063!
        Me.S211_007.Width = 1.0!
        '
        'Label289
        '
        Me.Label289.Height = 0.1875!
        Me.Label289.HyperLink = Nothing
        Me.Label289.Left = 0!
        Me.Label289.Name = "Label289"
        Me.Label289.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label289.Text = "Fnv_325" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label289.Top = 20.25!
        Me.Label289.Width = 2.5!
        '
        'S211_008
        '
        Me.S211_008.Height = 0.1875!
        Me.S211_008.HyperLink = Nothing
        Me.S211_008.Left = 2.5!
        Me.S211_008.Name = "S211_008"
        Me.S211_008.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_008.Text = "68.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_008.Top = 20.25!
        Me.S211_008.Width = 1.0!
        '
        'Label291
        '
        Me.Label291.Height = 0.1875!
        Me.Label291.HyperLink = Nothing
        Me.Label291.Left = 3.5!
        Me.Label291.Name = "Label291"
        Me.Label291.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label291.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "AISC 360 Table J3.2" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label291.Top = 20.25!
        Me.Label291.Width = 1.875001!
        '
        'Label292
        '
        Me.Label292.Height = 0.1875!
        Me.Label292.HyperLink = Nothing
        Me.Label292.Left = 0!
        Me.Label292.Name = "Label292"
        Me.Label292.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label292.Text = "Ab_stem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label292.Top = 20.438!
        Me.Label292.Width = 2.5!
        '
        'S211_009
        '
        Me.S211_009.Height = 0.1875!
        Me.S211_009.HyperLink = Nothing
        Me.S211_009.Left = 2.5!
        Me.S211_009.Name = "S211_009"
        Me.S211_009.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_009.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_009.Top = 20.438!
        Me.S211_009.Width = 1.0!
        '
        'Label294
        '
        Me.Label294.Height = 0.1875!
        Me.Label294.HyperLink = Nothing
        Me.Label294.Left = 3.5!
        Me.Label294.Name = "Label294"
        Me.Label294.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label294.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pi* (boltD_linkBm/2)^ 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label294.Top = 20.438!
        Me.Label294.Width = 1.875001!
        '
        'Label282
        '
        Me.Label282.Height = 0.1875!
        Me.Label282.HyperLink = Nothing
        Me.Label282.Left = 0!
        Me.Label282.Name = "Label282"
        Me.Label282.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label282.Text = "Strength per Bolt (ΦVbolt) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label282.Top = 20.626!
        Me.Label282.Width = 2.5!
        '
        'S211_010
        '
        Me.S211_010.Height = 0.1875!
        Me.S211_010.HyperLink = Nothing
        Me.S211_010.Left = 2.5!
        Me.S211_010.Name = "S211_010"
        Me.S211_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_010.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_010.Top = 20.626!
        Me.S211_010.Width = 1.0!
        '
        'Label288
        '
        Me.Label288.Height = 0.1875!
        Me.Label288.HyperLink = Nothing
        Me.Label288.Left = 3.5!
        Me.Label288.Name = "Label288"
        Me.Label288.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label288.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Φbolt* Fnv_325* Ab_stem" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label288.Top = 20.626!
        Me.Label288.Width = 1.875001!
        '
        'Label295
        '
        Me.Label295.Height = 0.1875!
        Me.Label295.HyperLink = Nothing
        Me.Label295.Left = 0!
        Me.Label295.Name = "Label295"
        Me.Label295.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label295.Text = "Fnt_325" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label295.Top = 20.813!
        Me.Label295.Width = 2.5!
        '
        'S211_011
        '
        Me.S211_011.Height = 0.1875!
        Me.S211_011.HyperLink = Nothing
        Me.S211_011.Left = 2.5!
        Me.S211_011.Name = "S211_011"
        Me.S211_011.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S211_011.Text = "90"
        Me.S211_011.Top = 20.813!
        Me.S211_011.Width = 1.0!
        '
        'Label297
        '
        Me.Label297.Height = 0.1875!
        Me.Label297.HyperLink = Nothing
        Me.Label297.Left = 3.5!
        Me.Label297.Name = "Label297"
        Me.Label297.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label297.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "AISC 360 Table J3.2" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label297.Top = 20.813!
        Me.Label297.Width = 1.875001!
        '
        'Label298
        '
        Me.Label298.Height = 0.1875!
        Me.Label298.HyperLink = Nothing
        Me.Label298.Left = 0!
        Me.Label298.Name = "Label298"
        Me.Label298.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label298.Text = "ΦTbolt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label298.Top = 21.001!
        Me.Label298.Width = 2.5!
        '
        'S211_012
        '
        Me.S211_012.Height = 0.1875!
        Me.S211_012.HyperLink = Nothing
        Me.S211_012.Left = 2.5!
        Me.S211_012.Name = "S211_012"
        Me.S211_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S211_012.Text = "25.06" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S211_012.Top = 21.001!
        Me.S211_012.Width = 1.0!
        '
        'Label300
        '
        Me.Label300.Height = 0.1875!
        Me.Label300.HyperLink = Nothing
        Me.Label300.Left = 3.5!
        Me.Label300.Name = "Label300"
        Me.Label300.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label300.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Φbolt* Fnt_325* Ab_stem" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label300.Top = 21.001!
        Me.Label300.Width = 1.875001!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 2.5!
        Me.Line1.LineColor = System.Drawing.Color.DimGray
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 19.5!
        Me.Line1.Width = 4.0!
        Me.Line1.X1 = 2.5!
        Me.Line1.X2 = 6.5!
        Me.Line1.Y1 = 19.5!
        Me.Line1.Y2 = 19.5!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 2.5!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 21.437!
        Me.Line2.Width = 2.0!
        Me.Line2.X1 = 2.5!
        Me.Line2.X2 = 4.5!
        Me.Line2.Y1 = 21.437!
        Me.Line2.Y2 = 21.437!
        '
        'Line3
        '
        Me.Line3.Height = 0.1875!
        Me.Line3.Left = 2.5!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 19.5!
        Me.Line3.Width = 0!
        Me.Line3.X1 = 2.5!
        Me.Line3.X2 = 2.5!
        Me.Line3.Y1 = 19.5!
        Me.Line3.Y2 = 19.6875!
        '
        'Line4
        '
        Me.Line4.Height = 0.1875!
        Me.Line4.Left = 3.5!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 19.5!
        Me.Line4.Width = 0!
        Me.Line4.X1 = 3.5!
        Me.Line4.X2 = 3.5!
        Me.Line4.Y1 = 19.5!
        Me.Line4.Y2 = 19.6875!
        '
        'Line5
        '
        Me.Line5.Height = 0.1875!
        Me.Line5.Left = 4.5!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 19.5!
        Me.Line5.Width = 0!
        Me.Line5.X1 = 4.5!
        Me.Line5.X2 = 4.5!
        Me.Line5.Y1 = 19.5!
        Me.Line5.Y2 = 19.6875!
        '
        'Line6
        '
        Me.Line6.Height = 0.1875!
        Me.Line6.Left = 5.5!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 19.5!
        Me.Line6.Width = 0!
        Me.Line6.X1 = 5.5!
        Me.Line6.X2 = 5.5!
        Me.Line6.Y1 = 19.5!
        Me.Line6.Y2 = 19.6875!
        '
        'Line7
        '
        Me.Line7.Height = 0.1875!
        Me.Line7.Left = 6.499!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 19.5!
        Me.Line7.Width = 0!
        Me.Line7.X1 = 6.499!
        Me.Line7.X2 = 6.499!
        Me.Line7.Y1 = 19.5!
        Me.Line7.Y2 = 19.6875!
        '
        'Line8
        '
        Me.Line8.Height = 0!
        Me.Line8.Left = 2.5!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 21.625!
        Me.Line8.Width = 2.0!
        Me.Line8.X1 = 2.5!
        Me.Line8.X2 = 4.5!
        Me.Line8.Y1 = 21.625!
        Me.Line8.Y2 = 21.625!
        '
        'Line9
        '
        Me.Line9.Height = 0!
        Me.Line9.Left = 2.5!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 21.813!
        Me.Line9.Width = 2.0!
        Me.Line9.X1 = 2.5!
        Me.Line9.X2 = 4.5!
        Me.Line9.Y1 = 21.813!
        Me.Line9.Y2 = 21.813!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 2.5!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 22.001!
        Me.Line10.Width = 2.0!
        Me.Line10.X1 = 2.5!
        Me.Line10.X2 = 4.5!
        Me.Line10.Y1 = 22.001!
        Me.Line10.Y2 = 22.001!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 2.5!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 22.189!
        Me.Line11.Width = 2.0!
        Me.Line11.X1 = 2.5!
        Me.Line11.X2 = 4.5!
        Me.Line11.Y1 = 22.189!
        Me.Line11.Y2 = 22.189!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 2.5!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 21.25!
        Me.Line12.Width = 2.0!
        Me.Line12.X1 = 2.5!
        Me.Line12.X2 = 4.5!
        Me.Line12.Y1 = 21.25!
        Me.Line12.Y2 = 21.25!
        '
        'Label303
        '
        Me.Label303.Height = 0.1875!
        Me.Label303.HyperLink = Nothing
        Me.Label303.Left = 0!
        Me.Label303.Name = "Label303"
        Me.Label303.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label303.Text = "Tension="
        Me.Label303.Top = 21.438!
        Me.Label303.Width = 2.5!
        '
        'Label304
        '
        Me.Label304.Height = 0.1875!
        Me.Label304.HyperLink = Nothing
        Me.Label304.Left = 0!
        Me.Label304.Name = "Label304"
        Me.Label304.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label304.Text = "Shear= "
        Me.Label304.Top = 21.626!
        Me.Label304.Width = 2.5!
        '
        'Label305
        '
        Me.Label305.Height = 0.1875!
        Me.Label305.HyperLink = Nothing
        Me.Label305.Left = 0!
        Me.Label305.Name = "Label305"
        Me.Label305.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label305.Text = "RnV_(V+T), kips=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label305.Top = 21.814!
        Me.Label305.Width = 2.5!
        '
        'Label306
        '
        Me.Label306.Height = 0.1875!
        Me.Label306.HyperLink = Nothing
        Me.Label306.Left = 0!
        Me.Label306.Name = "Label306"
        Me.Label306.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label306.Text = "T+V=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label306.Top = 22.001!
        Me.Label306.Width = 2.5!
        '
        'Line13
        '
        Me.Line13.Height = 0.9389992!
        Me.Line13.Left = 2.5!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 21.25!
        Me.Line13.Width = 0!
        Me.Line13.X1 = 2.5!
        Me.Line13.X2 = 2.5!
        Me.Line13.Y1 = 21.25!
        Me.Line13.Y2 = 22.189!
        '
        'Line14
        '
        Me.Line14.Height = 0.9389992!
        Me.Line14.Left = 3.5!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 21.25!
        Me.Line14.Width = 0!
        Me.Line14.X1 = 3.5!
        Me.Line14.X2 = 3.5!
        Me.Line14.Y1 = 21.25!
        Me.Line14.Y2 = 22.189!
        '
        'Line15
        '
        Me.Line15.Height = 0.9389992!
        Me.Line15.Left = 4.5!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 21.25!
        Me.Line15.Width = 0!
        Me.Line15.X1 = 4.5!
        Me.Line15.X2 = 4.5!
        Me.Line15.Y1 = 21.25!
        Me.Line15.Y2 = 22.189!
        '
        'Label315
        '
        Me.Label315.Height = 0.1875!
        Me.Label315.HyperLink = Nothing
        Me.Label315.Left = 0!
        Me.Label315.Name = "Label315"
        Me.Label315.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label315.Text = "Max DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label315.Top = 22.25!
        Me.Label315.Width = 2.5!
        '
        'Label317
        '
        Me.Label317.Height = 0.1875!
        Me.Label317.HyperLink = Nothing
        Me.Label317.Left = 0!
        Me.Label317.Name = "Label317"
        Me.Label317.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label317.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label317.Top = 22.438!
        Me.Label317.Width = 2.5!
        '
        'Label319
        '
        Me.Label319.Height = 0.1875!
        Me.Label319.HyperLink = Nothing
        Me.Label319.Left = 3.5!
        Me.Label319.Name = "Label319"
        Me.Label319.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label319.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if tbrp_DCR < SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label319.Top = 22.438!
        Me.Label319.Width = 3.594!
        '
        'Line16
        '
        Me.Line16.Height = 0!
        Me.Line16.Left = 2.5!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 19.687!
        Me.Line16.Width = 4.0!
        Me.Line16.X1 = 2.5!
        Me.Line16.X2 = 6.5!
        Me.Line16.Y1 = 19.687!
        Me.Line16.Y2 = 19.687!
        '
        'headerS28
        '
        Me.headerS28.Height = 0.1875!
        Me.headerS28.HyperLink = Nothing
        Me.headerS28.Left = 0.01!
        Me.headerS28.Name = "headerS28"
        Me.headerS28.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS28.Text = "2.8 BUCKLING RESTRAINT PLATE AND SPACER CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS28.Top = 8.541!
        Me.headerS28.Width = 5.0!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0.0000002384186!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label2.Text = "Limit Rotation (r_linkLimit) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 5.25!
        Me.Label2.Width = 2.5!
        '
        'S27_001
        '
        Me.S27_001.Height = 0.1875!
        Me.S27_001.HyperLink = Nothing
        Me.S27_001.Left = 2.5!
        Me.S27_001.Name = "S27_001"
        Me.S27_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S27_001.Text = "0.05" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_001.Top = 5.25!
        Me.S27_001.Width = 1.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 3.5!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label7.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "Connection rotation per ESR-2802" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 5.25!
        Me.Label7.Width = 3.999001!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Limit Strain ε (e_linkLimit) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 5.437!
        Me.Label9.Width = 2.5!
        '
        'S27_002
        '
        Me.S27_002.Height = 0.1875!
        Me.S27_002.HyperLink = Nothing
        Me.S27_002.Left = 2.5!
        Me.S27_002.Name = "S27_002"
        Me.S27_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S27_002.Text = "8.00%" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_002.Top = 5.437!
        Me.S27_002.Width = 1.0!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 3.5!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label13.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Link strain limit per ESR-2802" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 5.437!
        Me.Label13.Width = 4.0!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label15.Text = "Rotation Arm (d_arm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 5.625!
        Me.Label15.Width = 2.5!
        '
        'S27_003
        '
        Me.S27_003.Height = 0.1875!
        Me.S27_003.HyperLink = Nothing
        Me.S27_003.Left = 2.5!
        Me.S27_003.Name = "S27_003"
        Me.S27_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S27_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_003.Top = 5.625!
        Me.S27_003.Width = 1.0!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 3.5!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label20.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (db + t_stem)/2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 5.625!
        Me.Label20.Width = 4.5!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 0!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label22.Text = "Link Extension (linkDelta)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label22.Top = 5.813!
        Me.Label22.Width = 2.5!
        '
        'S27_004
        '
        Me.S27_004.Height = 0.1875!
        Me.S27_004.HyperLink = Nothing
        Me.S27_004.Left = 2.5!
        Me.S27_004.Name = "S27_004"
        Me.S27_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S27_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_004.Top = 5.813!
        Me.S27_004.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 3.5!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label28.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=d_arm* r_linkLimit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 5.813!
        Me.Label28.Width = 4.5!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 0!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label32.Text = "Lyield_req=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label32.Top = 6.0!
        Me.Label32.Width = 2.5!
        '
        'S27_005
        '
        Me.S27_005.Height = 0.1875!
        Me.S27_005.HyperLink = Nothing
        Me.S27_005.Left = 2.5!
        Me.S27_005.Name = "S27_005"
        Me.S27_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S27_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_005.Top = 6.0!
        Me.S27_005.Width = 1.0!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 3.5!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label38.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= r_linkLimit* d_arm/ 0.085 + 2* r_fillet" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label38.Top = 6.0!
        Me.Label38.Width = 4.5!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 0!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label48.Text = "Lyield_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label48.Top = 6.187!
        Me.Label48.Width = 2.5!
        '
        'S27_007
        '
        Me.S27_007.Height = 0.1875!
        Me.S27_007.HyperLink = Nothing
        Me.S27_007.Left = 2.5!
        Me.S27_007.Name = "S27_007"
        Me.S27_007.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S27_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_007.Top = 6.187!
        Me.S27_007.Width = 1.0!
        '
        'Label53
        '
        Me.Label53.Height = 0.1875!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 3.5!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label53.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lstem_yield_req/ L_stemYield" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label53.Top = 6.187!
        Me.Label53.Width = 4.5!
        '
        'Label57
        '
        Me.Label57.Height = 0.1875!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 0!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label57.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label57.Top = 6.375!
        Me.Label57.Width = 2.5!
        '
        'Label61
        '
        Me.Label61.Height = 0.1875!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 3.5!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label61.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if L_stem_DCR < SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label61.Top = 6.375!
        Me.Label61.Width = 4.5!
        '
        'Label64
        '
        Me.Label64.Height = 0.1875!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 0.02000018!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label64.Text = "db=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label64.Top = 7.02!
        Me.Label64.Width = 2.5!
        '
        'S27_010
        '
        Me.S27_010.Height = 0.1875!
        Me.S27_010.HyperLink = Nothing
        Me.S27_010.Left = 2.52!
        Me.S27_010.Name = "S27_010"
        Me.S27_010.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S27_010.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_010.Top = 7.02!
        Me.S27_010.Width = 1.0!
        '
        'Label69
        '
        Me.Label69.Height = 0.1875!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 3.52!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label69.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Beam Depth from database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label69.Top = 7.02!
        Me.Label69.Width = 4.5!
        '
        'Label70
        '
        Me.Label70.Height = 0.1875!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 0.02000018!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label70.Text = "db+tstem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label70.Top = 7.208001!
        Me.Label70.Width = 2.5!
        '
        'S27_011
        '
        Me.S27_011.Height = 0.1875!
        Me.S27_011.HyperLink = Nothing
        Me.S27_011.Left = 2.52!
        Me.S27_011.Name = "S27_011"
        Me.S27_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S27_011.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_011.Top = 7.208001!
        Me.S27_011.Width = 1.0!
        '
        'Label75
        '
        Me.Label75.Height = 0.1875!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 3.52!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label75.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label75.Top = 7.208001!
        Me.Label75.Width = 4.5!
        '
        'Label76
        '
        Me.Label76.Height = 0.1875!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0.02000018!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label76.Text = "Pu_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label76.Top = 7.395002!
        Me.Label76.Width = 2.5!
        '
        'S27_012
        '
        Me.S27_012.Height = 0.1875!
        Me.S27_012.HyperLink = Nothing
        Me.S27_012.Left = 2.52!
        Me.S27_012.Name = "S27_012"
        Me.S27_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S27_012.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_012.Top = 7.395002!
        Me.S27_012.Width = 1.0!
        '
        'Label82
        '
        Me.Label82.Height = 0.1875!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 3.52!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label82.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Link Axial demand" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label82.Top = 7.395002!
        Me.Label82.Width = 4.5!
        '
        'Label84
        '
        Me.Label84.Height = 0.1875!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 0.02000018!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label84.Text = "Py_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label84.Top = 7.583001!
        Me.Label84.Width = 2.5!
        '
        'S27_013
        '
        Me.S27_013.Height = 0.1875!
        Me.S27_013.HyperLink = Nothing
        Me.S27_013.Left = 2.52!
        Me.S27_013.Name = "S27_013"
        Me.S27_013.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S27_013.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_013.Top = 7.583001!
        Me.S27_013.Width = 1.0!
        '
        'Label89
        '
        Me.Label89.Height = 0.1875!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 3.52!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label89.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Link Capacity from database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label89.Top = 7.583001!
        Me.Label89.Width = 4.5!
        '
        'Label91
        '
        Me.Label91.Height = 0.1875!
        Me.Label91.HyperLink = Nothing
        Me.Label91.Left = 0.02000018!
        Me.Label91.Name = "Label91"
        Me.Label91.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label91.Text = "Link stem DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label91.Top = 7.958001!
        Me.Label91.Width = 2.5!
        '
        'S27_015
        '
        Me.S27_015.Height = 0.1875!
        Me.S27_015.HyperLink = Nothing
        Me.S27_015.Left = 2.52!
        Me.S27_015.Name = "S27_015"
        Me.S27_015.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S27_015.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_015.Top = 7.958001!
        Me.S27_015.Width = 1.0!
        '
        'Label95
        '
        Me.Label95.Height = 0.1875!
        Me.Label95.HyperLink = Nothing
        Me.Label95.Left = 3.52!
        Me.Label95.Name = "Label95"
        Me.Label95.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label95.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_link/(0.9*Py_link)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label95.Top = 7.958001!
        Me.Label95.Width = 4.5!
        '
        'Label97
        '
        Me.Label97.Height = 0.1875!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 0.01999998!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label97.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label97.Top = 8.146!
        Me.Label97.Width = 2.5!
        '
        'Label103
        '
        Me.Label103.Height = 0.1875!
        Me.Label103.HyperLink = Nothing
        Me.Label103.Left = 3.52!
        Me.Label103.Name = "Label103"
        Me.Label103.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label103.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if Link stem DCR < SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label103.Top = 8.146002!
        Me.Label103.Width = 4.5!
        '
        'Label106
        '
        Me.Label106.Height = 0.1875!
        Me.Label106.HyperLink = Nothing
        Me.Label106.Left = 0.3330002!
        Me.Label106.Name = "Label106"
        Me.Label106.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label106.Text = "Strength Check:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label106.Top = 6.645!
        Me.Label106.Width = 1.0!
        '
        'Label109
        '
        Me.Label109.Height = 0.1875!
        Me.Label109.HyperLink = Nothing
        Me.Label109.Left = 0.02000018!
        Me.Label109.Name = "Label109"
        Me.Label109.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label109.Text = "Mu=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label109.Top = 6.832002!
        Me.Label109.Width = 2.5!
        '
        'S27_009
        '
        Me.S27_009.Height = 0.1875!
        Me.S27_009.HyperLink = Nothing
        Me.S27_009.Left = 2.52!
        Me.S27_009.Name = "S27_009"
        Me.S27_009.Style = "color: black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S27_009.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_009.Top = 6.832002!
        Me.S27_009.Width = 1.0!
        '
        'Label115
        '
        Me.Label115.Height = 0.1875!
        Me.Label115.HyperLink = Nothing
        Me.Label115.Left = 3.52!
        Me.Label115.Name = "Label115"
        Me.Label115.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label115.Text = "k-in" & Global.Microsoft.VisualBasic.ChrW(9) & "Moment Demand" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label115.Top = 6.832002!
        Me.Label115.Width = 4.5!
        '
        'Shape6
        '
        Me.Shape6.Height = 0.1875!
        Me.Shape6.Left = 1.4!
        Me.Shape6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 0.188!
        Me.Shape6.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.02000018!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Φ*Py_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 7.77!
        Me.Label1.Width = 2.5!
        '
        'S27_014
        '
        Me.S27_014.Height = 0.1875!
        Me.S27_014.HyperLink = Nothing
        Me.S27_014.Left = 2.52!
        Me.S27_014.Name = "S27_014"
        Me.S27_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S27_014.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S27_014.Top = 7.77!
        Me.S27_014.Width = 1.0!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 3.52!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label11.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Φ = 0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label11.Top = 7.77!
        Me.Label11.Width = 4.5!
        '
        'Label93
        '
        Me.Label93.Height = 0.1875!
        Me.Label93.HyperLink = Nothing
        Me.Label93.Left = 5.875!
        Me.Label93.Name = "Label93"
        Me.Label93.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label93.Text = "pe1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label93.Top = 17.564!
        Me.Label93.Width = 0.625!
        '
        'S210_033
        '
        Me.S210_033.Height = 0.1875!
        Me.S210_033.HyperLink = Nothing
        Me.S210_033.Left = 6.5!
        Me.S210_033.Name = "S210_033"
        Me.S210_033.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_033.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_033.Top = 17.564!
        Me.S210_033.Width = 1.0!
        '
        'Label112
        '
        Me.Label112.Height = 0.1875!
        Me.Label112.HyperLink = Nothing
        Me.Label112.Left = 7.5!
        Me.Label112.Name = "Label112"
        Me.Label112.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label112.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label112.Top = 17.563!
        Me.Label112.Width = 0.25!
        '
        'Label118
        '
        Me.Label118.Height = 0.1875!
        Me.Label118.HyperLink = Nothing
        Me.Label118.Left = 5.875!
        Me.Label118.Name = "Label118"
        Me.Label118.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label118.Text = "3 rows of BRP bolts:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label118.Top = 17.377!
        Me.Label118.Width = 1.625001!
        '
        'Label121
        '
        Me.Label121.Height = 0.1875!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 5.875!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label121.Text = "pe2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label121.Top = 17.752!
        Me.Label121.Width = 0.625!
        '
        'S210_034
        '
        Me.S210_034.Height = 0.1875!
        Me.S210_034.HyperLink = Nothing
        Me.S210_034.Left = 6.5!
        Me.S210_034.Name = "S210_034"
        Me.S210_034.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_034.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_034.Top = 17.752!
        Me.S210_034.Width = 1.0!
        '
        'Label128
        '
        Me.Label128.Height = 0.1875!
        Me.Label128.HyperLink = Nothing
        Me.Label128.Left = 7.5!
        Me.Label128.Name = "Label128"
        Me.Label128.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label128.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label128.Top = 17.751!
        Me.Label128.Width = 0.25!
        '
        'Label131
        '
        Me.Label131.Height = 0.1875!
        Me.Label131.HyperLink = Nothing
        Me.Label131.Left = 5.875!
        Me.Label131.Name = "Label131"
        Me.Label131.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label131.Text = "pe3="
        Me.Label131.Top = 17.939!
        Me.Label131.Width = 0.625!
        '
        'S210_035
        '
        Me.S210_035.Height = 0.1875!
        Me.S210_035.HyperLink = Nothing
        Me.S210_035.Left = 6.5!
        Me.S210_035.Name = "S210_035"
        Me.S210_035.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_035.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_035.Top = 17.939!
        Me.S210_035.Width = 1.0!
        '
        'Label137
        '
        Me.Label137.Height = 0.1875!
        Me.Label137.HyperLink = Nothing
        Me.Label137.Left = 7.5!
        Me.Label137.Name = "Label137"
        Me.Label137.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label137.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label137.Top = 17.938!
        Me.Label137.Width = 0.25!
        '
        'Label140
        '
        Me.Label140.Height = 0.1875!
        Me.Label140.HyperLink = Nothing
        Me.Label140.Left = 5.874!
        Me.Label140.Name = "Label140"
        Me.Label140.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label140.Text = "pe_3row="
        Me.Label140.Top = 18.127!
        Me.Label140.Width = 0.625!
        '
        'S210_036
        '
        Me.S210_036.Height = 0.1875!
        Me.S210_036.HyperLink = Nothing
        Me.S210_036.Left = 6.499!
        Me.S210_036.Name = "S210_036"
        Me.S210_036.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S210_036.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S210_036.Top = 18.127!
        Me.S210_036.Width = 1.0!
        '
        'Label146
        '
        Me.Label146.Height = 0.1875!
        Me.Label146.HyperLink = Nothing
        Me.Label146.Left = 7.499!
        Me.Label146.Name = "Label146"
        Me.Label146.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label146.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label146.Top = 18.126!
        Me.Label146.Width = 0.25!
        '
        'Label100
        '
        Me.Label100.Height = 0.1875!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 4.838!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label100.Text = "Beam b/2/t = "
        Me.Label100.Top = 0.188!
        Me.Label100.Width = 0.937!
        '
        'S21_005
        '
        Me.S21_005.Height = 0.1875!
        Me.S21_005.HyperLink = Nothing
        Me.S21_005.Left = 5.775001!
        Me.S21_005.Name = "S21_005"
        Me.S21_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S21_005.Text = "YL4-3.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S21_005.Top = 0.188!
        Me.S21_005.Width = 1.0!
        '
        'Label134
        '
        Me.Label134.Height = 0.1875!
        Me.Label134.HyperLink = Nothing
        Me.Label134.Left = 4.838!
        Me.Label134.Name = "Label134"
        Me.Label134.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label134.Text = "Beam h/tw ="
        Me.Label134.Top = 0.375!
        Me.Label134.Width = 0.937!
        '
        'S21_006
        '
        Me.S21_006.Height = 0.1875!
        Me.S21_006.HyperLink = Nothing
        Me.S21_006.Left = 5.775001!
        Me.S21_006.Name = "S21_006"
        Me.S21_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S21_006.Text = "5010.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S21_006.Top = 0.375!
        Me.S21_006.Width = 1.0!
        '
        'S21_008
        '
        Me.S21_008.Height = 0.1875!
        Me.S21_008.HyperLink = Nothing
        Me.S21_008.Left = 6.775001!
        Me.S21_008.Name = "S21_008"
        Me.S21_008.Style = "color: Blue; font-size: 8.25pt; font-style: normal; vertical-align: middle; ddo-c" &
    "har-set: 1"
        Me.S21_008.Text = "Non-Compact"
        Me.S21_008.Top = 0.375!
        Me.S21_008.Width = 0.974!
        '
        'S21_007
        '
        Me.S21_007.Height = 0.1875!
        Me.S21_007.HyperLink = Nothing
        Me.S21_007.Left = 6.775!
        Me.S21_007.Name = "S21_007"
        Me.S21_007.Style = "color: Blue; font-size: 8.25pt; font-style: normal; vertical-align: middle; ddo-c" &
    "har-set: 1"
        Me.S21_007.Text = "Non-Compact"
        Me.S21_007.Top = 0.187!
        Me.S21_007.Width = 0.974!
        '
        'Label125
        '
        Me.Label125.Height = 0.1875!
        Me.Label125.HyperLink = Nothing
        Me.Label125.Left = 5.775!
        Me.Label125.Name = "Label125"
        Me.Label125.Style = "color: Black; font-size: 8.25pt; font-style: normal; text-align: justify; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label125.Text = "AISC 360 Table B4.1b"
        Me.Label125.Top = 0!
        Me.Label125.Width = 1.232!
        '
        'Picture1
        '
        Me.Picture1.Height = 2.299!
        Me.Picture1.ImageData = CType(resources.GetObject("Picture1.ImageData"), System.IO.Stream)
        Me.Picture1.Left = 5.135!
        Me.Picture1.Name = "Picture1"
        Me.Picture1.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture1.Top = 8.614!
        Me.Picture1.Width = 2.563!
        '
        'Shape5
        '
        Me.Shape5.Height = 0.1875!
        Me.Shape5.Left = 2.5!
        Me.Shape5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 6.374!
        Me.Shape5.Width = 1.0!
        '
        'Shape4
        '
        Me.Shape4.Height = 0.1875!
        Me.Shape4.Left = 2.52!
        Me.Shape4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape4.Name = "Shape4"
        Me.Shape4.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape4.Top = 8.146!
        Me.Shape4.Width = 1.0!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 2.5!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 12.812!
        Me.Shape2.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.5!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 18.312!
        Me.Shape1.Width = 1.0!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.1875!
        Me.Shape3.Left = 2.5!
        Me.Shape3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 22.438!
        Me.Shape3.Width = 1.0!
        '
        'S212_006
        '
        Me.S212_006.Height = 0.1875!
        Me.S212_006.HyperLink = Nothing
        Me.S212_006.Left = 2.5!
        Me.S212_006.Name = "S212_006"
        Me.S212_006.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S212_006.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S212_006.Top = 24.004!
        Me.S212_006.Width = 1.0!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0.00000002235174!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label40.Text = "2.12 LINK STEM HOLE/BRP HOLE FIT ON BEAM FLANGE"
        Me.Label40.Top = 22.817!
        Me.Label40.Width = 5.0!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0.00000002235174!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label46.Text = "bff="
        Me.Label46.Top = 23.005!
        Me.Label46.Width = 2.5!
        '
        'S212_001
        '
        Me.S212_001.Height = 0.1875!
        Me.S212_001.HyperLink = Nothing
        Me.S212_001.Left = 2.5!
        Me.S212_001.Name = "S212_001"
        Me.S212_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S212_001.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S212_001.Top = 23.005!
        Me.S212_001.Width = 1.0!
        '
        'Label143
        '
        Me.Label143.Height = 0.1875!
        Me.Label143.HyperLink = Nothing
        Me.Label143.Left = 3.5!
        Me.Label143.Name = "Label143"
        Me.Label143.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label143.Text = "in"
        Me.Label143.Top = 23.005!
        Me.Label143.Width = 4.094!
        '
        'Label149
        '
        Me.Label149.Height = 0.1875!
        Me.Label149.HyperLink = Nothing
        Me.Label149.Left = 0.00000002235174!
        Me.Label149.Name = "Label149"
        Me.Label149.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label149.Text = "bff_BRP_bolt="
        Me.Label149.Top = 23.193!
        Me.Label149.Width = 2.5!
        '
        'S212_002
        '
        Me.S212_002.Height = 0.1875!
        Me.S212_002.HyperLink = Nothing
        Me.S212_002.Left = 2.5!
        Me.S212_002.Name = "S212_002"
        Me.S212_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S212_002.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S212_002.Top = 23.193!
        Me.S212_002.Width = 1.0!
        '
        'Label153
        '
        Me.Label153.Height = 0.1875!
        Me.Label153.HyperLink = Nothing
        Me.Label153.Left = 3.5!
        Me.Label153.Name = "Label153"
        Me.Label153.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label153.Text = "in                 =minimum flnage width for BRP bolt spacing + min. bolt edge di" &
    "stance" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label153.Top = 23.193!
        Me.Label153.Width = 3.594!
        '
        'Label156
        '
        Me.Label156.Height = 0.1875!
        Me.Label156.HyperLink = Nothing
        Me.Label156.Left = 0.00000002235174!
        Me.Label156.Name = "Label156"
        Me.Label156.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label156.Text = "bff_stem_bolt="
        Me.Label156.Top = 23.381!
        Me.Label156.Width = 2.5!
        '
        'S212_003
        '
        Me.S212_003.Height = 0.1875!
        Me.S212_003.HyperLink = Nothing
        Me.S212_003.Left = 2.5!
        Me.S212_003.Name = "S212_003"
        Me.S212_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S212_003.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S212_003.Top = 23.381!
        Me.S212_003.Width = 1.0!
        '
        'Label159
        '
        Me.Label159.Height = 0.1870003!
        Me.Label159.HyperLink = Nothing
        Me.Label159.Left = 3.5!
        Me.Label159.Name = "Label159"
        Me.Label159.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label159.Text = "in                 =minimum flange width for stem bolt spacing + min. bolt edge d" &
    "istance" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label159.Top = 23.381!
        Me.Label159.Width = 3.594!
        '
        'S212_004
        '
        Me.S212_004.Height = 0.1875!
        Me.S212_004.HyperLink = Nothing
        Me.S212_004.Left = 2.5!
        Me.S212_004.Name = "S212_004"
        Me.S212_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S212_004.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S212_004.Top = 23.588!
        Me.S212_004.Width = 1.0!
        '
        'Label162
        '
        Me.Label162.Height = 0.1875!
        Me.Label162.HyperLink = Nothing
        Me.Label162.Left = 0.00000002235174!
        Me.Label162.Name = "Label162"
        Me.Label162.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label162.Text = "bff_min="
        Me.Label162.Top = 23.588!
        Me.Label162.Width = 2.5!
        '
        'Label164
        '
        Me.Label164.Height = 0.1875!
        Me.Label164.HyperLink = Nothing
        Me.Label164.Left = 0.00000002235174!
        Me.Label164.Name = "Label164"
        Me.Label164.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label164.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label164.Top = 24.005!
        Me.Label164.Width = 2.5!
        '
        'S212_005
        '
        Me.S212_005.Height = 0.1875!
        Me.S212_005.HyperLink = Nothing
        Me.S212_005.Left = 2.5!
        Me.S212_005.Name = "S212_005"
        Me.S212_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S212_005.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S212_005.Top = 23.786!
        Me.S212_005.Width = 1.0!
        '
        'Label167
        '
        Me.Label167.Height = 0.1875!
        Me.Label167.HyperLink = Nothing
        Me.Label167.Left = 0.00000002235174!
        Me.Label167.Name = "Label167"
        Me.Label167.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label167.Text = "bf_DCR="
        Me.Label167.Top = 23.786!
        Me.Label167.Width = 2.5!
        '
        'Shape7
        '
        Me.Shape7.Height = 0.1875!
        Me.Shape7.Left = 2.5!
        Me.Shape7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape7.Name = "Shape7"
        Me.Shape7.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape7.Top = 24.005!
        Me.Shape7.Width = 1.0!
        '
        'S213_007
        '
        Me.S213_007.Height = 0.1875!
        Me.S213_007.HyperLink = Nothing
        Me.S213_007.Left = 2.5!
        Me.S213_007.Name = "S213_007"
        Me.S213_007.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S213_007.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_007.Top = 25.658!
        Me.S213_007.Width = 1.0!
        '
        'S213_008
        '
        Me.S213_008.Height = 0.1875!
        Me.S213_008.HyperLink = Nothing
        Me.S213_008.Left = 2.5!
        Me.S213_008.Name = "S213_008"
        Me.S213_008.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S213_008.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_008.Top = 25.83401!
        Me.S213_008.Width = 1.0!
        '
        'S213_013
        '
        Me.S213_013.Height = 0.1875!
        Me.S213_013.HyperLink = Nothing
        Me.S213_013.Left = 2.51!
        Me.S213_013.Name = "S213_013"
        Me.S213_013.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S213_013.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_013.Top = 26.92801!
        Me.S213_013.Width = 1.0!
        '
        'S213_018
        '
        Me.S213_018.Height = 0.1875!
        Me.S213_018.HyperLink = Nothing
        Me.S213_018.Left = 2.5!
        Me.S213_018.Name = "S213_018"
        Me.S213_018.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S213_018.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_018.Top = 28.08999!
        Me.S213_018.Width = 1.0!
        '
        'Label176
        '
        Me.Label176.Height = 0.1875!
        Me.Label176.HyperLink = Nothing
        Me.Label176.Left = 0.01000023!
        Me.Label176.Name = "Label176"
        Me.Label176.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label176.Text = "2.13 BRP BOLT BEAM FLANGE BEARING AND TEAROUT IN WEAK-AXIS DIRECTION (AISC 360, J" &
    "3.10)"
        Me.Label176.Top = 24.314!
        Me.Label176.Width = 6.584!
        '
        'Label179
        '
        Me.Label179.Height = 0.1875!
        Me.Label179.HyperLink = Nothing
        Me.Label179.Left = 0.01000023!
        Me.Label179.Name = "Label179"
        Me.Label179.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label179.Text = "Φbr="
        Me.Label179.Top = 24.502!
        Me.Label179.Width = 2.5!
        '
        'S213_001
        '
        Me.S213_001.Height = 0.1875!
        Me.S213_001.HyperLink = Nothing
        Me.S213_001.Left = 2.51!
        Me.S213_001.Name = "S213_001"
        Me.S213_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_001.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_001.Top = 24.502!
        Me.S213_001.Width = 1.0!
        '
        'Label181
        '
        Me.Label181.Height = 0.1875!
        Me.Label181.HyperLink = Nothing
        Me.Label181.Left = 0.01000023!
        Me.Label181.Name = "Label181"
        Me.Label181.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label181.Text = "bbf="
        Me.Label181.Top = 24.68999!
        Me.Label181.Width = 2.5!
        '
        'S213_002
        '
        Me.S213_002.Height = 0.1875!
        Me.S213_002.HyperLink = Nothing
        Me.S213_002.Left = 2.51!
        Me.S213_002.Name = "S213_002"
        Me.S213_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S213_002.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_002.Top = 24.68999!
        Me.S213_002.Width = 1.0!
        '
        'Label183
        '
        Me.Label183.Height = 0.1875!
        Me.Label183.HyperLink = Nothing
        Me.Label183.Left = 3.51!
        Me.Label183.Name = "Label183"
        Me.Label183.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label183.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label183.Top = 24.68999!
        Me.Label183.Width = 2.594!
        '
        'Label186
        '
        Me.Label186.Height = 0.1875!
        Me.Label186.HyperLink = Nothing
        Me.Label186.Left = 0.01000023!
        Me.Label186.Name = "Label186"
        Me.Label186.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label186.Text = "g_brp="
        Me.Label186.Top = 24.878!
        Me.Label186.Width = 2.5!
        '
        'S213_003
        '
        Me.S213_003.Height = 0.1875!
        Me.S213_003.HyperLink = Nothing
        Me.S213_003.Left = 2.51!
        Me.S213_003.Name = "S213_003"
        Me.S213_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S213_003.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_003.Top = 24.888!
        Me.S213_003.Width = 1.0!
        '
        'Label189
        '
        Me.Label189.Height = 0.1875!
        Me.Label189.HyperLink = Nothing
        Me.Label189.Left = 3.51!
        Me.Label189.Name = "Label189"
        Me.Label189.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label189.Text = "in"
        Me.Label189.Top = 24.878!
        Me.Label189.Width = 2.594!
        '
        'S213_004
        '
        Me.S213_004.Height = 0.1875!
        Me.S213_004.HyperLink = Nothing
        Me.S213_004.Left = 2.51!
        Me.S213_004.Name = "S213_004"
        Me.S213_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S213_004.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_004.Top = 25.085!
        Me.S213_004.Width = 1.0!
        '
        'Label192
        '
        Me.Label192.Height = 0.1875!
        Me.Label192.HyperLink = Nothing
        Me.Label192.Left = 0.01000023!
        Me.Label192.Name = "Label192"
        Me.Label192.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label192.Text = "d_brp="
        Me.Label192.Top = 25.085!
        Me.Label192.Width = 2.5!
        '
        'Label195
        '
        Me.Label195.Height = 0.1875!
        Me.Label195.HyperLink = Nothing
        Me.Label195.Left = 0!
        Me.Label195.Name = "Label195"
        Me.Label195.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label195.Text = "Ledge>1 bolt Dia?"
        Me.Label195.Top = 25.659!
        Me.Label195.Width = 2.5!
        '
        'S213_005
        '
        Me.S213_005.Height = 0.1875!
        Me.S213_005.HyperLink = Nothing
        Me.S213_005.Left = 2.51!
        Me.S213_005.Name = "S213_005"
        Me.S213_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_005.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_005.Top = 25.283!
        Me.S213_005.Width = 1.0!
        '
        'Label198
        '
        Me.Label198.Height = 0.1875!
        Me.Label198.HyperLink = Nothing
        Me.Label198.Left = 0.01000023!
        Me.Label198.Name = "Label198"
        Me.Label198.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label198.Text = "Ledge="
        Me.Label198.Top = 25.283!
        Me.Label198.Width = 2.5!
        '
        'Label201
        '
        Me.Label201.Height = 0.1875!
        Me.Label201.HyperLink = Nothing
        Me.Label201.Left = 3.5!
        Me.Label201.Name = "Label201"
        Me.Label201.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label201.Text = "in"
        Me.Label201.Top = 25.085!
        Me.Label201.Width = 2.594!
        '
        'Label204
        '
        Me.Label204.Height = 0.1875!
        Me.Label204.HyperLink = Nothing
        Me.Label204.Left = 3.5!
        Me.Label204.Name = "Label204"
        Me.Label204.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label204.Text = "in                "
        Me.Label204.Top = 25.273!
        Me.Label204.Width = 2.594!
        '
        'Label206
        '
        Me.Label206.Height = 0.1875!
        Me.Label206.HyperLink = Nothing
        Me.Label206.Left = 0.0000002384186!
        Me.Label206.Name = "Label206"
        Me.Label206.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label206.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label206.Top = 25.835!
        Me.Label206.Width = 2.5!
        '
        'S213_006
        '
        Me.S213_006.Height = 0.1875!
        Me.S213_006.HyperLink = Nothing
        Me.S213_006.Left = 2.51!
        Me.S213_006.Name = "S213_006"
        Me.S213_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S213_006.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_006.Top = 25.46!
        Me.S213_006.Width = 1.0!
        '
        'Label207
        '
        Me.Label207.Height = 0.1875!
        Me.Label207.HyperLink = Nothing
        Me.Label207.Left = 0.01000047!
        Me.Label207.Name = "Label207"
        Me.Label207.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label207.Text = "Ledge_min="
        Me.Label207.Top = 25.46!
        Me.Label207.Width = 2.5!
        '
        'Label209
        '
        Me.Label209.Height = 0.1875!
        Me.Label209.HyperLink = Nothing
        Me.Label209.Left = 3.5!
        Me.Label209.Name = "Label209"
        Me.Label209.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label209.Text = "in                "
        Me.Label209.Top = 25.45!
        Me.Label209.Width = 2.594!
        '
        'Label214
        '
        Me.Label214.Height = 0.1875!
        Me.Label214.HyperLink = Nothing
        Me.Label214.Left = 0.01!
        Me.Label214.Name = "Label214"
        Me.Label214.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label214.Text = "Vy="
        Me.Label214.Top = 26.189!
        Me.Label214.Width = 2.5!
        '
        'S213_009
        '
        Me.S213_009.Height = 0.1875!
        Me.S213_009.HyperLink = Nothing
        Me.S213_009.Left = 2.5!
        Me.S213_009.Name = "S213_009"
        Me.S213_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S213_009.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_009.Top = 26.189!
        Me.S213_009.Width = 1.0!
        '
        'Label217
        '
        Me.Label217.Height = 0.1875!
        Me.Label217.HyperLink = Nothing
        Me.Label217.Left = 3.5!
        Me.Label217.Name = "Label217"
        Me.Label217.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label217.Text = "kips/bolt"
        Me.Label217.Top = 26.189!
        Me.Label217.Width = 3.094!
        '
        'Label223
        '
        Me.Label223.Height = 0.1875!
        Me.Label223.HyperLink = Nothing
        Me.Label223.Left = 0!
        Me.Label223.Name = "Label223"
        Me.Label223.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label223.Text = "Lc="
        Me.Label223.Top = 27.33!
        Me.Label223.Width = 2.5!
        '
        'S213_014
        '
        Me.S213_014.Height = 0.1875!
        Me.S213_014.HyperLink = Nothing
        Me.S213_014.Left = 2.5!
        Me.S213_014.Name = "S213_014"
        Me.S213_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_014.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_014.Top = 27.33!
        Me.S213_014.Width = 1.0!
        '
        'Label231
        '
        Me.Label231.Height = 0.1875!
        Me.Label231.HyperLink = Nothing
        Me.Label231.Left = 3.5!
        Me.Label231.Name = "Label231"
        Me.Label231.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label231.Text = "in"
        Me.Label231.Top = 27.32899!
        Me.Label231.Width = 2.594!
        '
        'S213_010
        '
        Me.S213_010.Height = 0.1875!
        Me.S213_010.HyperLink = Nothing
        Me.S213_010.Left = 2.51!
        Me.S213_010.Name = "S213_010"
        Me.S213_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S213_010.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_010.Top = 26.377!
        Me.S213_010.Width = 1.0!
        '
        'Label245
        '
        Me.Label245.Height = 0.1875!
        Me.Label245.HyperLink = Nothing
        Me.Label245.Left = 0.01000023!
        Me.Label245.Name = "Label245"
        Me.Label245.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label245.Text = "Rn_bearing="
        Me.Label245.Top = 26.377!
        Me.Label245.Width = 2.5!
        '
        'S213_011
        '
        Me.S213_011.Height = 0.1875!
        Me.S213_011.HyperLink = Nothing
        Me.S213_011.Left = 2.51!
        Me.S213_011.Name = "S213_011"
        Me.S213_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_011.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_011.Top = 26.575!
        Me.S213_011.Width = 1.0!
        '
        'Label259
        '
        Me.Label259.Height = 0.1875!
        Me.Label259.HyperLink = Nothing
        Me.Label259.Left = 0.01000023!
        Me.Label259.Name = "Label259"
        Me.Label259.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label259.Text = "ΦRn_bearing="
        Me.Label259.Top = 26.575!
        Me.Label259.Width = 2.5!
        '
        'Label261
        '
        Me.Label261.Height = 0.1875!
        Me.Label261.HyperLink = Nothing
        Me.Label261.Left = 3.5!
        Me.Label261.Name = "Label261"
        Me.Label261.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label261.Text = "kips"
        Me.Label261.Top = 26.377!
        Me.Label261.Width = 2.594!
        '
        'Label263
        '
        Me.Label263.Height = 0.1875!
        Me.Label263.HyperLink = Nothing
        Me.Label263.Left = 3.5!
        Me.Label263.Name = "Label263"
        Me.Label263.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label263.Text = "kips"
        Me.Label263.Top = 26.565!
        Me.Label263.Width = 2.594!
        '
        'Label266
        '
        Me.Label266.Height = 0.1875!
        Me.Label266.HyperLink = Nothing
        Me.Label266.Left = 0.01000001!
        Me.Label266.Name = "Label266"
        Me.Label266.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label266.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label266.Top = 26.929!
        Me.Label266.Width = 2.5!
        '
        'S213_012
        '
        Me.S213_012.Height = 0.1875!
        Me.S213_012.HyperLink = Nothing
        Me.S213_012.Left = 2.51!
        Me.S213_012.Name = "S213_012"
        Me.S213_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_012.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_012.Top = 26.752!
        Me.S213_012.Width = 1.0!
        '
        'Label275
        '
        Me.Label275.Height = 0.1875!
        Me.Label275.HyperLink = Nothing
        Me.Label275.Left = 0.01000047!
        Me.Label275.Name = "Label275"
        Me.Label275.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label275.Text = "DCR_bearing="
        Me.Label275.Top = 26.752!
        Me.Label275.Width = 2.5!
        '
        'S213_015
        '
        Me.S213_015.Height = 0.1875!
        Me.S213_015.HyperLink = Nothing
        Me.S213_015.Left = 2.51!
        Me.S213_015.Name = "S213_015"
        Me.S213_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S213_015.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_015.Top = 27.517!
        Me.S213_015.Width = 1.0!
        '
        'Label237
        '
        Me.Label237.Height = 0.1875!
        Me.Label237.HyperLink = Nothing
        Me.Label237.Left = 0.01000023!
        Me.Label237.Name = "Label237"
        Me.Label237.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label237.Text = "Rn_tearout="
        Me.Label237.Top = 27.517!
        Me.Label237.Width = 2.5!
        '
        'Label240
        '
        Me.Label240.Height = 0.1875!
        Me.Label240.HyperLink = Nothing
        Me.Label240.Left = 0!
        Me.Label240.Name = "Label240"
        Me.Label240.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label240.Text = "Check="
        Me.Label240.Top = 28.091!
        Me.Label240.Width = 2.5!
        '
        'S213_016
        '
        Me.S213_016.Height = 0.1875!
        Me.S213_016.HyperLink = Nothing
        Me.S213_016.Left = 2.51!
        Me.S213_016.Name = "S213_016"
        Me.S213_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_016.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_016.Top = 27.71499!
        Me.S213_016.Width = 1.0!
        '
        'Label284
        '
        Me.Label284.Height = 0.1875!
        Me.Label284.HyperLink = Nothing
        Me.Label284.Left = 0.01000023!
        Me.Label284.Name = "Label284"
        Me.Label284.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label284.Text = "ΦRn_tearout="
        Me.Label284.Top = 27.71499!
        Me.Label284.Width = 2.5!
        '
        'Label285
        '
        Me.Label285.Height = 0.1875!
        Me.Label285.HyperLink = Nothing
        Me.Label285.Left = 3.5!
        Me.Label285.Name = "Label285"
        Me.Label285.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label285.Text = "kips               =1.2*Lc*tbf*Fu_bm          (AISC 360)"
        Me.Label285.Top = 27.517!
        Me.Label285.Width = 2.594!
        '
        'Label287
        '
        Me.Label287.Height = 0.1875!
        Me.Label287.HyperLink = Nothing
        Me.Label287.Left = 3.5!
        Me.Label287.Name = "Label287"
        Me.Label287.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label287.Text = "kips               =Φbr*Rn_tearout"
        Me.Label287.Top = 27.705!
        Me.Label287.Width = 2.594!
        '
        'S213_017
        '
        Me.S213_017.Height = 0.1875!
        Me.S213_017.HyperLink = Nothing
        Me.S213_017.Left = 2.51!
        Me.S213_017.Name = "S213_017"
        Me.S213_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S213_017.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S213_017.Top = 27.892!
        Me.S213_017.Width = 1.0!
        '
        'Label293
        '
        Me.Label293.Height = 0.1875!
        Me.Label293.HyperLink = Nothing
        Me.Label293.Left = 0.01000047!
        Me.Label293.Name = "Label293"
        Me.Label293.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label293.Text = "DCR_tearout="
        Me.Label293.Top = 27.892!
        Me.Label293.Width = 2.5!
        '
        'Shape8
        '
        Me.Shape8.Height = 0.1875!
        Me.Shape8.Left = 2.5!
        Me.Shape8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape8.Name = "Shape8"
        Me.Shape8.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape8.Top = 25.659!
        Me.Shape8.Width = 1.0!
        '
        'Shape9
        '
        Me.Shape9.Height = 0.1875!
        Me.Shape9.Left = 2.5!
        Me.Shape9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape9.Name = "Shape9"
        Me.Shape9.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape9.Top = 25.835!
        Me.Shape9.Width = 1.0!
        '
        'Shape11
        '
        Me.Shape11.Height = 0.1875!
        Me.Shape11.Left = 2.51!
        Me.Shape11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape11.Name = "Shape11"
        Me.Shape11.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape11.Top = 26.929!
        Me.Shape11.Width = 1.0!
        '
        'Shape12
        '
        Me.Shape12.Height = 0.1875!
        Me.Shape12.Left = 2.5!
        Me.Shape12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape12.Name = "Shape12"
        Me.Shape12.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape12.Top = 28.091!
        Me.Shape12.Width = 1.0!
        '
        'S214_023
        '
        Me.S214_023.Height = 0.1875!
        Me.S214_023.HyperLink = Nothing
        Me.S214_023.Left = 2.5!
        Me.S214_023.Name = "S214_023"
        Me.S214_023.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S214_023.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_023.Top = 30.932!
        Me.S214_023.Width = 1.0!
        '
        'S214_024
        '
        Me.S214_024.Height = 0.1875!
        Me.S214_024.HyperLink = Nothing
        Me.S214_024.Left = 2.5!
        Me.S214_024.Name = "S214_024"
        Me.S214_024.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S214_024.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_024.Top = 32.246!
        Me.S214_024.Width = 1.0!
        '
        'headerS214
        '
        Me.headerS214.Height = 0.1875!
        Me.headerS214.HyperLink = Nothing
        Me.headerS214.Left = 0!
        Me.headerS214.Name = "headerS214"
        Me.headerS214.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS214.Text = "2.14 BRP BOLT BEAM FLANGE BLOCK SHEAR IN WEAK-AXIS DIRECTION (AISC 360, J4)"
        Me.headerS214.Top = 28.535!
        Me.headerS214.Width = 5.0!
        '
        'Label316
        '
        Me.Label316.Height = 0.1875!
        Me.Label316.HyperLink = Nothing
        Me.Label316.Left = 0!
        Me.Label316.Name = "Label316"
        Me.Label316.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label316.Text = "Φu="
        Me.Label316.Top = 28.764!
        Me.Label316.Width = 2.5!
        '
        'S214_001
        '
        Me.S214_001.Height = 0.1875!
        Me.S214_001.HyperLink = Nothing
        Me.S214_001.Left = 2.5!
        Me.S214_001.Name = "S214_001"
        Me.S214_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_001.Top = 28.764!
        Me.S214_001.Width = 1.0!
        '
        'Label320
        '
        Me.Label320.Height = 0.1875!
        Me.Label320.HyperLink = Nothing
        Me.Label320.Left = 0!
        Me.Label320.Name = "Label320"
        Me.Label320.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label320.Text = "Sbrp="
        Me.Label320.Top = 28.951!
        Me.Label320.Width = 2.5!
        '
        'S214_002
        '
        Me.S214_002.Height = 0.1875!
        Me.S214_002.HyperLink = Nothing
        Me.S214_002.Left = 2.5!
        Me.S214_002.Name = "S214_002"
        Me.S214_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S214_002.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_002.Top = 28.952!
        Me.S214_002.Width = 1.0!
        '
        'Label322
        '
        Me.Label322.Height = 0.1875!
        Me.Label322.HyperLink = Nothing
        Me.Label322.Left = 3.5!
        Me.Label322.Name = "Label322"
        Me.Label322.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label322.Text = "in                " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label322.Top = 28.951!
        Me.Label322.Width = 2.594!
        '
        'Label323
        '
        Me.Label323.Height = 0.1875!
        Me.Label323.HyperLink = Nothing
        Me.Label323.Left = 0.0000002384186!
        Me.Label323.Name = "Label323"
        Me.Label323.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label323.Text = "d_brp="
        Me.Label323.Top = 29.138!
        Me.Label323.Width = 2.5!
        '
        'S214_003
        '
        Me.S214_003.Height = 0.1875!
        Me.S214_003.HyperLink = Nothing
        Me.S214_003.Left = 2.5!
        Me.S214_003.Name = "S214_003"
        Me.S214_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S214_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_003.Top = 29.138!
        Me.S214_003.Width = 1.0!
        '
        'Label325
        '
        Me.Label325.Height = 0.1875!
        Me.Label325.HyperLink = Nothing
        Me.Label325.Left = 3.5!
        Me.Label325.Name = "Label325"
        Me.Label325.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label325.Text = "in              " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label325.Top = 29.138!
        Me.Label325.Width = 2.594!
        '
        'Label326
        '
        Me.Label326.Height = 0.1875!
        Me.Label326.HyperLink = Nothing
        Me.Label326.Left = 0!
        Me.Label326.Name = "Label326"
        Me.Label326.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label326.Text = "Ledge="
        Me.Label326.Top = 29.326!
        Me.Label326.Width = 2.5!
        '
        'S214_004
        '
        Me.S214_004.Height = 0.1875!
        Me.S214_004.HyperLink = Nothing
        Me.S214_004.Left = 2.5!
        Me.S214_004.Name = "S214_004"
        Me.S214_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S214_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_004.Top = 29.326!
        Me.S214_004.Width = 1.0!
        '
        'Label328
        '
        Me.Label328.Height = 0.1875!
        Me.Label328.HyperLink = Nothing
        Me.Label328.Left = 3.5!
        Me.Label328.Name = "Label328"
        Me.Label328.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label328.Text = "in               " & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label328.Top = 29.326!
        Me.Label328.Width = 2.593002!
        '
        'Label329
        '
        Me.Label329.Height = 0.1875!
        Me.Label329.HyperLink = Nothing
        Me.Label329.Left = 0!
        Me.Label329.Name = "Label329"
        Me.Label329.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label329.Text = "tbf="
        Me.Label329.Top = 29.514!
        Me.Label329.Width = 2.5!
        '
        'S214_005
        '
        Me.S214_005.Height = 0.1875!
        Me.S214_005.HyperLink = Nothing
        Me.S214_005.Left = 2.5!
        Me.S214_005.Name = "S214_005"
        Me.S214_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S214_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_005.Top = 29.514!
        Me.S214_005.Width = 1.0!
        '
        'Label331
        '
        Me.Label331.Height = 0.1875!
        Me.Label331.HyperLink = Nothing
        Me.Label331.Left = 3.5!
        Me.Label331.Name = "Label331"
        Me.Label331.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label331.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label331.Top = 29.514!
        Me.Label331.Width = 2.594!
        '
        'Label334
        '
        Me.Label334.Height = 0.1875!
        Me.Label334.HyperLink = Nothing
        Me.Label334.Left = 5.996999!
        Me.Label334.Name = "Label334"
        Me.Label334.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label334.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label334.Top = 32.054!
        Me.Label334.Width = 0.937!
        '
        'Label337
        '
        Me.Label337.Height = 0.1875!
        Me.Label337.HyperLink = Nothing
        Me.Label337.Left = 6.019999!
        Me.Label337.Name = "Label337"
        Me.Label337.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label337.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label337.Top = 32.24403!
        Me.Label337.Width = 0.937!
        '
        'Label338
        '
        Me.Label338.Height = 0.1875!
        Me.Label338.HyperLink = Nothing
        Me.Label338.Left = 0!
        Me.Label338.Name = "Label338"
        Me.Label338.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label338.Text = "Vuy="
        Me.Label338.Top = 29.701!
        Me.Label338.Width = 2.5!
        '
        'S214_006
        '
        Me.S214_006.Height = 0.1875!
        Me.S214_006.HyperLink = Nothing
        Me.S214_006.Left = 2.5!
        Me.S214_006.Name = "S214_006"
        Me.S214_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S214_006.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_006.Top = 29.701!
        Me.S214_006.Width = 1.0!
        '
        'Label340
        '
        Me.Label340.Height = 0.1875!
        Me.Label340.HyperLink = Nothing
        Me.Label340.Left = 3.5!
        Me.Label340.Name = "Label340"
        Me.Label340.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label340.Text = "kips"
        Me.Label340.Top = 29.701!
        Me.Label340.Width = 2.594!
        '
        'Label341
        '
        Me.Label341.Height = 0.1875!
        Me.Label341.HyperLink = Nothing
        Me.Label341.Left = 0.0000004768372!
        Me.Label341.Name = "Label341"
        Me.Label341.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label341.Text = "Lgv="
        Me.Label341.Top = 29.972!
        Me.Label341.Width = 2.5!
        '
        'S214_007
        '
        Me.S214_007.Height = 0.1875!
        Me.S214_007.HyperLink = Nothing
        Me.S214_007.Left = 2.5!
        Me.S214_007.Name = "S214_007"
        Me.S214_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_007.Top = 29.972!
        Me.S214_007.Width = 1.0!
        '
        'Label343
        '
        Me.Label343.Height = 0.1875!
        Me.Label343.HyperLink = Nothing
        Me.Label343.Left = 3.5!
        Me.Label343.Name = "Label343"
        Me.Label343.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label343.Text = "in"
        Me.Label343.Top = 29.972!
        Me.Label343.Width = 2.594!
        '
        'Label344
        '
        Me.Label344.Height = 0.1875!
        Me.Label344.HyperLink = Nothing
        Me.Label344.Left = 0.0000002384186!
        Me.Label344.Name = "Label344"
        Me.Label344.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label344.Text = "Rn_bearing="
        Me.Label344.Top = 30.347!
        Me.Label344.Width = 2.5!
        '
        'S214_009
        '
        Me.S214_009.Height = 0.1875!
        Me.S214_009.HyperLink = Nothing
        Me.S214_009.Left = 2.5!
        Me.S214_009.Name = "S214_009"
        Me.S214_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_009.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_009.Top = 30.347!
        Me.S214_009.Width = 1.0!
        '
        'Label346
        '
        Me.Label346.Height = 0.1875!
        Me.Label346.HyperLink = Nothing
        Me.Label346.Left = 3.5!
        Me.Label346.Name = "Label346"
        Me.Label346.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label346.Text = "kips"
        Me.Label346.Top = 30.347!
        Me.Label346.Width = 2.594!
        '
        'Label347
        '
        Me.Label347.Height = 0.1875!
        Me.Label347.HyperLink = Nothing
        Me.Label347.Left = 0!
        Me.Label347.Name = "Label347"
        Me.Label347.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label347.Text = "DCR_yield="
        Me.Label347.Top = 30.743!
        Me.Label347.Width = 2.5!
        '
        'Label350
        '
        Me.Label350.Height = 0.1875!
        Me.Label350.HyperLink = Nothing
        Me.Label350.Left = 0.0000002384186!
        Me.Label350.Name = "Label350"
        Me.Label350.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label350.Text = "Check="
        Me.Label350.Top = 30.932!
        Me.Label350.Width = 2.5!
        '
        'Label353
        '
        Me.Label353.Height = 0.1875!
        Me.Label353.HyperLink = Nothing
        Me.Label353.Left = 0.0000002384186!
        Me.Label353.Name = "Label353"
        Me.Label353.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label353.Text = "ΦRn_yield="
        Me.Label353.Top = 30.534!
        Me.Label353.Width = 2.5!
        '
        'S214_010
        '
        Me.S214_010.Height = 0.1875!
        Me.S214_010.HyperLink = Nothing
        Me.S214_010.Left = 2.5!
        Me.S214_010.Name = "S214_010"
        Me.S214_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_010.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_010.Top = 30.534!
        Me.S214_010.Width = 1.0!
        '
        'Label355
        '
        Me.Label355.Height = 0.1875!
        Me.Label355.HyperLink = Nothing
        Me.Label355.Left = 3.5!
        Me.Label355.Name = "Label355"
        Me.Label355.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label355.Text = "kips"
        Me.Label355.Top = 30.534!
        Me.Label355.Width = 0.5!
        '
        'Label356
        '
        Me.Label356.Height = 0.1875!
        Me.Label356.HyperLink = Nothing
        Me.Label356.Left = 0!
        Me.Label356.Name = "Label356"
        Me.Label356.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label356.Text = "Agv="
        Me.Label356.Top = 30.157!
        Me.Label356.Width = 2.5!
        '
        'S214_008
        '
        Me.S214_008.Height = 0.1875!
        Me.S214_008.HyperLink = Nothing
        Me.S214_008.Left = 2.5!
        Me.S214_008.Name = "S214_008"
        Me.S214_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_008.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_008.Top = 30.157!
        Me.S214_008.Width = 1.0!
        '
        'Label358
        '
        Me.Label358.Height = 0.1875!
        Me.Label358.HyperLink = Nothing
        Me.Label358.Left = 3.5!
        Me.Label358.Name = "Label358"
        Me.Label358.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label358.Text = "in^2            " & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label358.Top = 30.16!
        Me.Label358.Width = 2.593002!
        '
        'Label359
        '
        Me.Label359.Height = 0.1875!
        Me.Label359.HyperLink = Nothing
        Me.Label359.Left = 0!
        Me.Label359.Name = "Label359"
        Me.Label359.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label359.Text = "Rn_tearout="
        Me.Label359.Top = 31.66101!
        Me.Label359.Width = 2.5!
        '
        'S214_016
        '
        Me.S214_016.Height = 0.1875!
        Me.S214_016.HyperLink = Nothing
        Me.S214_016.Left = 2.5!
        Me.S214_016.Name = "S214_016"
        Me.S214_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_016.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_016.Top = 31.66101!
        Me.S214_016.Width = 1.0!
        '
        'Label361
        '
        Me.Label361.Height = 0.1875!
        Me.Label361.HyperLink = Nothing
        Me.Label361.Left = 3.5!
        Me.Label361.Name = "Label361"
        Me.Label361.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label361.Text = "kips"
        Me.Label361.Top = 31.65301!
        Me.Label361.Width = 0.5!
        '
        'Label362
        '
        Me.Label362.Height = 0.1875!
        Me.Label362.HyperLink = Nothing
        Me.Label362.Left = 0!
        Me.Label362.Name = "Label362"
        Me.Label362.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label362.Text = "DCR_tearout="
        Me.Label362.Top = 32.035!
        Me.Label362.Width = 2.5!
        '
        'Label365
        '
        Me.Label365.Height = 0.1875!
        Me.Label365.HyperLink = Nothing
        Me.Label365.Left = 0!
        Me.Label365.Name = "Label365"
        Me.Label365.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label365.Text = "Check="
        Me.Label365.Top = 32.246!
        Me.Label365.Width = 2.5!
        '
        'Label368
        '
        Me.Label368.Height = 0.1875!
        Me.Label368.HyperLink = Nothing
        Me.Label368.Left = 0!
        Me.Label368.Name = "Label368"
        Me.Label368.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label368.Text = "ΦRn_tearout="
        Me.Label368.Top = 31.84801!
        Me.Label368.Width = 2.5!
        '
        'S214_017
        '
        Me.S214_017.Height = 0.1875!
        Me.S214_017.HyperLink = Nothing
        Me.S214_017.Left = 2.5!
        Me.S214_017.Name = "S214_017"
        Me.S214_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_017.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_017.Top = 31.84801!
        Me.S214_017.Width = 1.0!
        '
        'Label370
        '
        Me.Label370.Height = 0.1875!
        Me.Label370.HyperLink = Nothing
        Me.Label370.Left = 3.5!
        Me.Label370.Name = "Label370"
        Me.Label370.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label370.Text = "kips "
        Me.Label370.Top = 31.84001!
        Me.Label370.Width = 0.6870003!
        '
        'Label371
        '
        Me.Label371.Height = 0.1875!
        Me.Label371.HyperLink = Nothing
        Me.Label371.Left = 0!
        Me.Label371.Name = "Label371"
        Me.Label371.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label371.Text = "Lnv="
        Me.Label371.Top = 31.285!
        Me.Label371.Width = 2.5!
        '
        'S214_014
        '
        Me.S214_014.Height = 0.1875!
        Me.S214_014.HyperLink = Nothing
        Me.S214_014.Left = 2.5!
        Me.S214_014.Name = "S214_014"
        Me.S214_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_014.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_014.Top = 31.284!
        Me.S214_014.Width = 1.0!
        '
        'Label373
        '
        Me.Label373.Height = 0.1875!
        Me.Label373.HyperLink = Nothing
        Me.Label373.Left = 0!
        Me.Label373.Name = "Label373"
        Me.Label373.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label373.Text = "Anv="
        Me.Label373.Top = 31.472!
        Me.Label373.Width = 2.5!
        '
        'S214_015
        '
        Me.S214_015.Height = 0.1875!
        Me.S214_015.HyperLink = Nothing
        Me.S214_015.Left = 2.5!
        Me.S214_015.Name = "S214_015"
        Me.S214_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_015.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_015.Top = 31.47301!
        Me.S214_015.Width = 1.0!
        '
        'Label375
        '
        Me.Label375.Height = 0.1875!
        Me.Label375.HyperLink = Nothing
        Me.Label375.Left = 3.5!
        Me.Label375.Name = "Label375"
        Me.Label375.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label375.Text = "in^2                " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label375.Top = 31.464!
        Me.Label375.Width = 0.4370003!
        '
        'S214_019
        '
        Me.S214_019.Height = 0.1875!
        Me.S214_019.HyperLink = Nothing
        Me.S214_019.Left = 4.996999!
        Me.S214_019.Name = "S214_019"
        Me.S214_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_019.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_019.Top = 31.334!
        Me.S214_019.Width = 1.0!
        '
        'S214_020
        '
        Me.S214_020.Height = 0.1875!
        Me.S214_020.HyperLink = Nothing
        Me.S214_020.Left = 4.996999!
        Me.S214_020.Name = "S214_020"
        Me.S214_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_020.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_020.Top = 31.522!
        Me.S214_020.Width = 1.0!
        '
        'S214_021
        '
        Me.S214_021.Height = 0.1875!
        Me.S214_021.HyperLink = Nothing
        Me.S214_021.Left = 4.996999!
        Me.S214_021.Name = "S214_021"
        Me.S214_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_021.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_021.Top = 31.71001!
        Me.S214_021.Width = 1.0!
        '
        'Label385
        '
        Me.Label385.Height = 0.1875!
        Me.Label385.HyperLink = Nothing
        Me.Label385.Left = 4.508!
        Me.Label385.Name = "Label385"
        Me.Label385.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label385.Text = "Lnt="
        Me.Label385.Top = 30.79599!
        Me.Label385.Width = 0.4999995!
        '
        'S214_012
        '
        Me.S214_012.Height = 0.1875!
        Me.S214_012.HyperLink = Nothing
        Me.S214_012.Left = 5.008!
        Me.S214_012.Name = "S214_012"
        Me.S214_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_012.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_012.Top = 30.79599!
        Me.S214_012.Width = 1.0!
        '
        'Label387
        '
        Me.Label387.Height = 0.1875!
        Me.Label387.HyperLink = Nothing
        Me.Label387.Left = 6.008!
        Me.Label387.Name = "Label387"
        Me.Label387.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label387.Text = "in"
        Me.Label387.Top = 30.79599!
        Me.Label387.Width = 0!
        '
        'Label388
        '
        Me.Label388.Height = 0.1875!
        Me.Label388.HyperLink = Nothing
        Me.Label388.Left = 4.489!
        Me.Label388.Name = "Label388"
        Me.Label388.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label388.Text = "Ant="
        Me.Label388.Top = 30.998!
        Me.Label388.Width = 0.5190005!
        '
        'S214_013
        '
        Me.S214_013.Height = 0.1875!
        Me.S214_013.HyperLink = Nothing
        Me.S214_013.Left = 5.008!
        Me.S214_013.Name = "S214_013"
        Me.S214_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S214_013.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_013.Top = 30.998!
        Me.S214_013.Width = 1.0!
        '
        'Label390
        '
        Me.Label390.Height = 0.1875!
        Me.Label390.HyperLink = Nothing
        Me.Label390.Left = 6.008!
        Me.Label390.Name = "Label390"
        Me.Label390.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label390.Text = "in^2"
        Me.Label390.Top = 30.998!
        Me.Label390.Width = 0.9990007!
        '
        'S214_011
        '
        Me.S214_011.Height = 0.1875!
        Me.S214_011.HyperLink = Nothing
        Me.S214_011.Left = 2.5!
        Me.S214_011.Name = "S214_011"
        Me.S214_011.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S214_011.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_011.Top = 30.722!
        Me.S214_011.Width = 1.0!
        '
        'S214_018
        '
        Me.S214_018.Height = 0.1875!
        Me.S214_018.HyperLink = Nothing
        Me.S214_018.Left = 2.5!
        Me.S214_018.Name = "S214_018"
        Me.S214_018.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S214_018.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_018.Top = 32.035!
        Me.S214_018.Width = 1.0!
        '
        'S214_022
        '
        Me.S214_022.Height = 0.1875!
        Me.S214_022.HyperLink = Nothing
        Me.S214_022.Left = 4.996999!
        Me.S214_022.Name = "S214_022"
        Me.S214_022.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S214_022.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_022.Top = 31.896!
        Me.S214_022.Width = 1.0!
        '
        'Shape17
        '
        Me.Shape17.Height = 0.1875!
        Me.Shape17.Left = 2.5!
        Me.Shape17.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape17.Name = "Shape17"
        Me.Shape17.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape17.Top = 30.93!
        Me.Shape17.Width = 1.0!
        '
        'Shape19
        '
        Me.Shape19.Height = 0.1875!
        Me.Shape19.Left = 2.5!
        Me.Shape19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape19.Name = "Shape19"
        Me.Shape19.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape19.Top = 32.244!
        Me.Shape19.Width = 1.0!
        '
        'S215_015
        '
        Me.S215_015.Height = 0.1875!
        Me.S215_015.HyperLink = Nothing
        Me.S215_015.Left = 2.5!
        Me.S215_015.Name = "S215_015"
        Me.S215_015.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S215_015.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_015.Top = 35.00701!
        Me.S215_015.Width = 1.0!
        '
        'S215_016
        '
        Me.S215_016.Height = 0.1875!
        Me.S215_016.HyperLink = Nothing
        Me.S215_016.Left = 2.5!
        Me.S215_016.Name = "S215_016"
        Me.S215_016.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S215_016.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_016.Top = 35.81999!
        Me.S215_016.Width = 1.0!
        '
        'headerS215
        '
        Me.headerS215.Height = 0.1875!
        Me.headerS215.HyperLink = Nothing
        Me.headerS215.Left = 0!
        Me.headerS215.Name = "headerS215"
        Me.headerS215.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS215.Text = "2.15 BEAM FLANGE CHECK FOR LINK STEM-TO-BEAM FLANGE BOLT BEARING AND TEAROUT (AIS" &
    "C 360, J3.10)"
        Me.headerS215.Top = 32.683!
        Me.headerS215.Width = 6.5!
        '
        'Label392
        '
        Me.Label392.Height = 0.1875!
        Me.Label392.HyperLink = Nothing
        Me.Label392.Left = 0!
        Me.Label392.Name = "Label392"
        Me.Label392.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label392.Text = "Pr_link="
        Me.Label392.Top = 32.912!
        Me.Label392.Width = 2.5!
        '
        'S215_001
        '
        Me.S215_001.Height = 0.1875!
        Me.S215_001.HyperLink = Nothing
        Me.S215_001.Left = 2.5!
        Me.S215_001.Name = "S215_001"
        Me.S215_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S215_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_001.Top = 32.911!
        Me.S215_001.Width = 1.0!
        '
        'Label394
        '
        Me.Label394.Height = 0.1875!
        Me.Label394.HyperLink = Nothing
        Me.Label394.Left = 0!
        Me.Label394.Name = "Label394"
        Me.Label394.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label394.Text = "tbf="
        Me.Label394.Top = 33.099!
        Me.Label394.Width = 2.5!
        '
        'S215_002
        '
        Me.S215_002.Height = 0.1875!
        Me.S215_002.HyperLink = Nothing
        Me.S215_002.Left = 2.5!
        Me.S215_002.Name = "S215_002"
        Me.S215_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S215_002.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_002.Top = 33.10001!
        Me.S215_002.Width = 1.0!
        '
        'Label396
        '
        Me.Label396.Height = 0.1875!
        Me.Label396.HyperLink = Nothing
        Me.Label396.Left = 3.5!
        Me.Label396.Name = "Label396"
        Me.Label396.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label396.Text = "in                 Beam Flange thickness     " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label396.Top = 33.099!
        Me.Label396.Width = 2.521!
        '
        'Label397
        '
        Me.Label397.Height = 0.1875!
        Me.Label397.HyperLink = Nothing
        Me.Label397.Left = 0.0000002384186!
        Me.Label397.Name = "Label397"
        Me.Label397.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label397.Text = "bolt_Dia="
        Me.Label397.Top = 33.286!
        Me.Label397.Width = 2.5!
        '
        'S215_003
        '
        Me.S215_003.Height = 0.1875!
        Me.S215_003.HyperLink = Nothing
        Me.S215_003.Left = 2.5!
        Me.S215_003.Name = "S215_003"
        Me.S215_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S215_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_003.Top = 33.286!
        Me.S215_003.Width = 1.0!
        '
        'Label399
        '
        Me.Label399.Height = 0.1875!
        Me.Label399.HyperLink = Nothing
        Me.Label399.Left = 3.5!
        Me.Label399.Name = "Label399"
        Me.Label399.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label399.Text = "in                 Link stem-to-beam flange bolt diameter  " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label399.Top = 33.286!
        Me.Label399.Width = 2.521!
        '
        'Label400
        '
        Me.Label400.Height = 0.1875!
        Me.Label400.HyperLink = Nothing
        Me.Label400.Left = 0!
        Me.Label400.Name = "Label400"
        Me.Label400.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label400.Text = "n_bolts="
        Me.Label400.Top = 33.474!
        Me.Label400.Width = 2.5!
        '
        'S215_004
        '
        Me.S215_004.Height = 0.1875!
        Me.S215_004.HyperLink = Nothing
        Me.S215_004.Left = 2.5!
        Me.S215_004.Name = "S215_004"
        Me.S215_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S215_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_004.Top = 33.474!
        Me.S215_004.Width = 1.0!
        '
        'Label402
        '
        Me.Label402.Height = 0.1875!
        Me.Label402.HyperLink = Nothing
        Me.Label402.Left = 3.5!
        Me.Label402.Name = "Label402"
        Me.Label402.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label402.Text = "in                 Number of link-stem bolts    " & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label402.Top = 33.474!
        Me.Label402.Width = 2.521!
        '
        'Label403
        '
        Me.Label403.Height = 0.1875!
        Me.Label403.HyperLink = Nothing
        Me.Label403.Left = 0!
        Me.Label403.Name = "Label403"
        Me.Label403.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label403.Text = "S_stem_bolts="
        Me.Label403.Top = 33.662!
        Me.Label403.Width = 2.5!
        '
        'S215_005
        '
        Me.S215_005.Height = 0.1875!
        Me.S215_005.HyperLink = Nothing
        Me.S215_005.Left = 2.5!
        Me.S215_005.Name = "S215_005"
        Me.S215_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S215_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_005.Top = 33.662!
        Me.S215_005.Width = 1.0!
        '
        'Label405
        '
        Me.Label405.Height = 0.1875!
        Me.Label405.HyperLink = Nothing
        Me.Label405.Left = 3.5!
        Me.Label405.Name = "Label405"
        Me.Label405.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label405.Text = "in                 stem bolt spacing" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label405.Top = 33.662!
        Me.Label405.Width = 2.521!
        '
        'Label412
        '
        Me.Label412.Height = 0.1875!
        Me.Label412.HyperLink = Nothing
        Me.Label412.Left = 0!
        Me.Label412.Name = "Label412"
        Me.Label412.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label412.Text = "Lc="
        Me.Label412.Top = 33.849!
        Me.Label412.Width = 2.5!
        '
        'S215_006
        '
        Me.S215_006.Height = 0.1875!
        Me.S215_006.HyperLink = Nothing
        Me.S215_006.Left = 2.5!
        Me.S215_006.Name = "S215_006"
        Me.S215_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S215_006.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_006.Top = 33.849!
        Me.S215_006.Width = 1.0!
        '
        'Label414
        '
        Me.Label414.Height = 0.1875!
        Me.Label414.HyperLink = Nothing
        Me.Label414.Left = 3.5!
        Me.Label414.Name = "Label414"
        Me.Label414.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label414.Text = "in"
        Me.Label414.Top = 33.849!
        Me.Label414.Width = 2.521!
        '
        'Label415
        '
        Me.Label415.Height = 0.1875!
        Me.Label415.HyperLink = Nothing
        Me.Label415.Left = 0.0000007152557!
        Me.Label415.Name = "Label415"
        Me.Label415.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label415.Text = "Fy_bm="
        Me.Label415.Top = 34.047!
        Me.Label415.Width = 2.5!
        '
        'S215_007
        '
        Me.S215_007.Height = 0.1875!
        Me.S215_007.HyperLink = Nothing
        Me.S215_007.Left = 2.51!
        Me.S215_007.Name = "S215_007"
        Me.S215_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S215_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_007.Top = 34.047!
        Me.S215_007.Width = 1.01!
        '
        'Label417
        '
        Me.Label417.Height = 0.1875!
        Me.Label417.HyperLink = Nothing
        Me.Label417.Left = 3.5!
        Me.Label417.Name = "Label417"
        Me.Label417.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label417.Text = "ksi               from Design_Limits" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label417.Top = 34.047!
        Me.Label417.Width = 2.521!
        '
        'Label418
        '
        Me.Label418.Height = 0.1875!
        Me.Label418.HyperLink = Nothing
        Me.Label418.Left = 0.0000004768372!
        Me.Label418.Name = "Label418"
        Me.Label418.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label418.Text = "Rn_bearing="
        Me.Label418.Top = 34.422!
        Me.Label418.Width = 2.5!
        '
        'S215_009
        '
        Me.S215_009.Height = 0.1875!
        Me.S215_009.HyperLink = Nothing
        Me.S215_009.Left = 2.5!
        Me.S215_009.Name = "S215_009"
        Me.S215_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S215_009.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_009.Top = 34.422!
        Me.S215_009.Width = 1.0!
        '
        'Label420
        '
        Me.Label420.Height = 0.1875!
        Me.Label420.HyperLink = Nothing
        Me.Label420.Left = 3.5!
        Me.Label420.Name = "Label420"
        Me.Label420.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label420.Text = "kips              =2.4*bolt_Dia*tbf*Fu_bm*n_bolts" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label420.Top = 34.422!
        Me.Label420.Width = 2.521!
        '
        'Label421
        '
        Me.Label421.Height = 0.1875!
        Me.Label421.HyperLink = Nothing
        Me.Label421.Left = 0.0000007152557!
        Me.Label421.Name = "Label421"
        Me.Label421.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label421.Text = "DCR_bearing="
        Me.Label421.Top = 34.81799!
        Me.Label421.Width = 2.5!
        '
        'Label423
        '
        Me.Label423.Height = 0.1875!
        Me.Label423.HyperLink = Nothing
        Me.Label423.Left = 3.5!
        Me.Label423.Name = "Label423"
        Me.Label423.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label423.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label423.Top = 34.81799!
        Me.Label423.Width = 2.521!
        '
        'Label424
        '
        Me.Label424.Height = 0.1875!
        Me.Label424.HyperLink = Nothing
        Me.Label424.Left = 0.0000004768372!
        Me.Label424.Name = "Label424"
        Me.Label424.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label424.Text = "Check="
        Me.Label424.Top = 35.00701!
        Me.Label424.Width = 2.5!
        '
        'Label426
        '
        Me.Label426.Height = 0.1875!
        Me.Label426.HyperLink = Nothing
        Me.Label426.Left = 3.5!
        Me.Label426.Name = "Label426"
        Me.Label426.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label426.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label426.Top = 35.00701!
        Me.Label426.Width = 2.521!
        '
        'Label427
        '
        Me.Label427.Height = 0.1875!
        Me.Label427.HyperLink = Nothing
        Me.Label427.Left = 0.0000004768372!
        Me.Label427.Name = "Label427"
        Me.Label427.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label427.Text = "ΦRn_bearing="
        Me.Label427.Top = 34.609!
        Me.Label427.Width = 2.5!
        '
        'S215_010
        '
        Me.S215_010.Height = 0.1875!
        Me.S215_010.HyperLink = Nothing
        Me.S215_010.Left = 2.5!
        Me.S215_010.Name = "S215_010"
        Me.S215_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S215_010.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_010.Top = 34.609!
        Me.S215_010.Width = 1.0!
        '
        'Label429
        '
        Me.Label429.Height = 0.1875!
        Me.Label429.HyperLink = Nothing
        Me.Label429.Left = 3.5!
        Me.Label429.Name = "Label429"
        Me.Label429.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label429.Text = "kips             =fu*Rn_bearing" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label429.Top = 34.609!
        Me.Label429.Width = 2.521!
        '
        'Label430
        '
        Me.Label430.Height = 0.1875!
        Me.Label430.HyperLink = Nothing
        Me.Label430.Left = 0.0000002384186!
        Me.Label430.Name = "Label430"
        Me.Label430.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label430.Text = "Fu_bm="
        Me.Label430.Top = 34.232!
        Me.Label430.Width = 2.5!
        '
        'S215_008
        '
        Me.S215_008.Height = 0.1875!
        Me.S215_008.HyperLink = Nothing
        Me.S215_008.Left = 2.5!
        Me.S215_008.Name = "S215_008"
        Me.S215_008.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S215_008.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_008.Top = 34.232!
        Me.S215_008.Width = 1.0!
        '
        'Label432
        '
        Me.Label432.Height = 0.1875!
        Me.Label432.HyperLink = Nothing
        Me.Label432.Left = 3.5!
        Me.Label432.Name = "Label432"
        Me.Label432.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label432.Text = "ksi               from Design_Limits" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label432.Top = 34.235!
        Me.Label432.Width = 2.521!
        '
        'Label433
        '
        Me.Label433.Height = 0.1875!
        Me.Label433.HyperLink = Nothing
        Me.Label433.Left = 0!
        Me.Label433.Name = "Label433"
        Me.Label433.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label433.Text = "Rn_tearout="
        Me.Label433.Top = 35.235!
        Me.Label433.Width = 2.5!
        '
        'S215_012
        '
        Me.S215_012.Height = 0.1875!
        Me.S215_012.HyperLink = Nothing
        Me.S215_012.Left = 2.5!
        Me.S215_012.Name = "S215_012"
        Me.S215_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S215_012.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_012.Top = 35.235!
        Me.S215_012.Width = 1.0!
        '
        'Label435
        '
        Me.Label435.Height = 0.1875!
        Me.Label435.HyperLink = Nothing
        Me.Label435.Left = 3.5!
        Me.Label435.Name = "Label435"
        Me.Label435.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label435.Text = "kips              =1.2*Lc*tbf*Fu_bm*n_bolts" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label435.Top = 35.235!
        Me.Label435.Width = 2.521!
        '
        'Label436
        '
        Me.Label436.Height = 0.1875!
        Me.Label436.HyperLink = Nothing
        Me.Label436.Left = 0.0000002384186!
        Me.Label436.Name = "Label436"
        Me.Label436.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label436.Text = "DCR_tearout="
        Me.Label436.Top = 35.631!
        Me.Label436.Width = 2.5!
        '
        'Label439
        '
        Me.Label439.Height = 0.1875!
        Me.Label439.HyperLink = Nothing
        Me.Label439.Left = 0!
        Me.Label439.Name = "Label439"
        Me.Label439.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label439.Text = "Check="
        Me.Label439.Top = 35.81999!
        Me.Label439.Width = 2.5!
        '
        'Label442
        '
        Me.Label442.Height = 0.1875!
        Me.Label442.HyperLink = Nothing
        Me.Label442.Left = 0!
        Me.Label442.Name = "Label442"
        Me.Label442.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label442.Text = "ΦRn_tearout="
        Me.Label442.Top = 35.422!
        Me.Label442.Width = 2.5!
        '
        'S215_013
        '
        Me.S215_013.Height = 0.1875!
        Me.S215_013.HyperLink = Nothing
        Me.S215_013.Left = 2.5!
        Me.S215_013.Name = "S215_013"
        Me.S215_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S215_013.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_013.Top = 35.422!
        Me.S215_013.Width = 1.0!
        '
        'Label444
        '
        Me.Label444.Height = 0.1875!
        Me.Label444.HyperLink = Nothing
        Me.Label444.Left = 3.489!
        Me.Label444.Name = "Label444"
        Me.Label444.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label444.Text = "kips              =fu*Rn_tearout" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label444.Top = 35.444!
        Me.Label444.Width = 2.521!
        '
        'S215_011
        '
        Me.S215_011.Height = 0.1875!
        Me.S215_011.HyperLink = Nothing
        Me.S215_011.Left = 2.499!
        Me.S215_011.Name = "S215_011"
        Me.S215_011.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S215_011.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_011.Top = 34.81999!
        Me.S215_011.Width = 1.0!
        '
        'S215_014
        '
        Me.S215_014.Height = 0.1875!
        Me.S215_014.HyperLink = Nothing
        Me.S215_014.Left = 2.499!
        Me.S215_014.Name = "S215_014"
        Me.S215_014.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S215_014.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S215_014.Top = 35.632!
        Me.S215_014.Width = 1.0!
        '
        'Label378
        '
        Me.Label378.Height = 0.1875!
        Me.Label378.HyperLink = Nothing
        Me.Label378.Left = 3.469!
        Me.Label378.Name = "Label378"
        Me.Label378.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label378.Text = "kips"
        Me.Label378.Top = 32.923!
        Me.Label378.Width = 2.521!
        '
        'Shape23
        '
        Me.Shape23.Height = 0.1875!
        Me.Shape23.Left = 2.5!
        Me.Shape23.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape23.Name = "Shape23"
        Me.Shape23.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape23.Top = 35.005!
        Me.Shape23.Width = 1.0!
        '
        'Shape25
        '
        Me.Shape25.Height = 0.1875!
        Me.Shape25.Left = 2.5!
        Me.Shape25.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape25.Name = "Shape25"
        Me.Shape25.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape25.Top = 35.81799!
        Me.Shape25.Width = 1.0!
        '
        'S216_029
        '
        Me.S216_029.Height = 0.1875!
        Me.S216_029.HyperLink = Nothing
        Me.S216_029.Left = 2.583!
        Me.S216_029.Name = "S216_029"
        Me.S216_029.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S216_029.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_029.Top = 38.641!
        Me.S216_029.Width = 1.0!
        '
        'S216_030
        '
        Me.S216_030.Height = 0.1875!
        Me.S216_030.HyperLink = Nothing
        Me.S216_030.Left = 2.583!
        Me.S216_030.Name = "S216_030"
        Me.S216_030.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S216_030.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_030.Top = 38.83!
        Me.S216_030.Width = 1.0!
        '
        'S216_031
        '
        Me.S216_031.Height = 0.1875!
        Me.S216_031.HyperLink = Nothing
        Me.S216_031.Left = 2.583!
        Me.S216_031.Name = "S216_031"
        Me.S216_031.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S216_031.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_031.Top = 40.081!
        Me.S216_031.Width = 1.0!
        '
        'Label406
        '
        Me.Label406.Height = 0.1875!
        Me.Label406.HyperLink = Nothing
        Me.Label406.Left = 0.083!
        Me.Label406.Name = "Label406"
        Me.Label406.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label406.Text = "h_min="
        Me.Label406.Top = 38.432!
        Me.Label406.Width = 2.5!
        '
        'S216_011
        '
        Me.S216_011.Height = 0.1875!
        Me.S216_011.HyperLink = Nothing
        Me.S216_011.Left = 2.583!
        Me.S216_011.Name = "S216_011"
        Me.S216_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_011.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_011.Top = 38.245!
        Me.S216_011.Width = 1.0!
        '
        'Label408
        '
        Me.Label408.Height = 0.1875!
        Me.Label408.HyperLink = Nothing
        Me.Label408.Left = 3.583!
        Me.Label408.Name = "Label408"
        Me.Label408.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label408.Text = "in"
        Me.Label408.Top = 38.245!
        Me.Label408.Width = 2.459002!
        '
        'Label409
        '
        Me.Label409.Height = 0.1875!
        Me.Label409.HyperLink = Nothing
        Me.Label409.Left = 0.08300048!
        Me.Label409.Name = "Label409"
        Me.Label409.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label409.Text = "h>1 bolt Dia?"
        Me.Label409.Top = 38.641!
        Me.Label409.Width = 2.5!
        '
        'Label411
        '
        Me.Label411.Height = 0.1875!
        Me.Label411.HyperLink = Nothing
        Me.Label411.Left = 3.583!
        Me.Label411.Name = "Label411"
        Me.Label411.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label411.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label411.Top = 38.641!
        Me.Label411.Width = 2.459002!
        '
        'Label438
        '
        Me.Label438.Height = 0.1875!
        Me.Label438.HyperLink = Nothing
        Me.Label438.Left = 0.08300027!
        Me.Label438.Name = "Label438"
        Me.Label438.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label438.Text = "h>=h_min?"
        Me.Label438.Top = 38.83!
        Me.Label438.Width = 2.5!
        '
        'Label445
        '
        Me.Label445.Height = 0.1875!
        Me.Label445.HyperLink = Nothing
        Me.Label445.Left = 3.583!
        Me.Label445.Name = "Label445"
        Me.Label445.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label445.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label445.Top = 38.83!
        Me.Label445.Width = 2.459002!
        '
        'Label446
        '
        Me.Label446.Height = 0.1875!
        Me.Label446.HyperLink = Nothing
        Me.Label446.Left = 0.08300027!
        Me.Label446.Name = "Label446"
        Me.Label446.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label446.Text = "d_brp="
        Me.Label446.Top = 37.682!
        Me.Label446.Width = 2.5!
        '
        'S216_012
        '
        Me.S216_012.Height = 0.1875!
        Me.S216_012.HyperLink = Nothing
        Me.S216_012.Left = 2.583!
        Me.S216_012.Name = "S216_012"
        Me.S216_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_012.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_012.Top = 38.432!
        Me.S216_012.Width = 1.0!
        '
        'Label448
        '
        Me.Label448.Height = 0.1875!
        Me.Label448.HyperLink = Nothing
        Me.Label448.Left = 3.583!
        Me.Label448.Name = "Label448"
        Me.Label448.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label448.Text = "in"
        Me.Label448.Top = 38.431!
        Me.Label448.Width = 2.459002!
        '
        'headerS216
        '
        Me.headerS216.Height = 0.1875!
        Me.headerS216.HyperLink = Nothing
        Me.headerS216.Left = 0.021!
        Me.headerS216.Name = "headerS216"
        Me.headerS216.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS216.Text = "2.16 LINK STEM-TO-BEAM FLANGE BOLT BLOCK SHEAR CHECK (AISC 360, J4)"
        Me.headerS216.Top = 36.18!
        Me.headerS216.Width = 5.0!
        '
        'Label450
        '
        Me.Label450.Height = 0.1875!
        Me.Label450.HyperLink = Nothing
        Me.Label450.Left = 0.083!
        Me.Label450.Name = "Label450"
        Me.Label450.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label450.Text = "L1="
        Me.Label450.Top = 36.369!
        Me.Label450.Width = 2.5!
        '
        'S216_001
        '
        Me.S216_001.Height = 0.1875!
        Me.S216_001.HyperLink = Nothing
        Me.S216_001.Left = 2.583!
        Me.S216_001.Name = "S216_001"
        Me.S216_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_001.Top = 36.368!
        Me.S216_001.Width = 1.0!
        '
        'Label452
        '
        Me.Label452.Height = 0.1875!
        Me.Label452.HyperLink = Nothing
        Me.Label452.Left = 0.083!
        Me.Label452.Name = "Label452"
        Me.Label452.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label452.Text = "Sbrp="
        Me.Label452.Top = 36.556!
        Me.Label452.Width = 2.5!
        '
        'S216_002
        '
        Me.S216_002.Height = 0.1875!
        Me.S216_002.HyperLink = Nothing
        Me.S216_002.Left = 2.583!
        Me.S216_002.Name = "S216_002"
        Me.S216_002.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_002.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_002.Top = 36.557!
        Me.S216_002.Width = 1.0!
        '
        'Label454
        '
        Me.Label454.Height = 0.1875!
        Me.Label454.HyperLink = Nothing
        Me.Label454.Left = 3.583!
        Me.Label454.Name = "Label454"
        Me.Label454.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label454.Text = "in                " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label454.Top = 36.556!
        Me.Label454.Width = 2.459001!
        '
        'Label455
        '
        Me.Label455.Height = 0.1875!
        Me.Label455.HyperLink = Nothing
        Me.Label455.Left = 0.08300021!
        Me.Label455.Name = "Label455"
        Me.Label455.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label455.Text = "L2="
        Me.Label455.Top = 36.743!
        Me.Label455.Width = 2.5!
        '
        'S216_003
        '
        Me.S216_003.Height = 0.1875!
        Me.S216_003.HyperLink = Nothing
        Me.S216_003.Left = 2.583!
        Me.S216_003.Name = "S216_003"
        Me.S216_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_003.Top = 36.743!
        Me.S216_003.Width = 1.0!
        '
        'Label457
        '
        Me.Label457.Height = 0.1875!
        Me.Label457.HyperLink = Nothing
        Me.Label457.Left = 3.583!
        Me.Label457.Name = "Label457"
        Me.Label457.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label457.Text = "in              " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label457.Top = 36.743!
        Me.Label457.Width = 2.459001!
        '
        'Label458
        '
        Me.Label458.Height = 0.1875!
        Me.Label458.HyperLink = Nothing
        Me.Label458.Left = 0.083!
        Me.Label458.Name = "Label458"
        Me.Label458.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label458.Text = "Stem="
        Me.Label458.Top = 36.931!
        Me.Label458.Width = 2.5!
        '
        'S216_004
        '
        Me.S216_004.Height = 0.1875!
        Me.S216_004.HyperLink = Nothing
        Me.S216_004.Left = 2.583!
        Me.S216_004.Name = "S216_004"
        Me.S216_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_004.Top = 36.931!
        Me.S216_004.Width = 1.0!
        '
        'Label460
        '
        Me.Label460.Height = 0.1875!
        Me.Label460.HyperLink = Nothing
        Me.Label460.Left = 3.583!
        Me.Label460.Name = "Label460"
        Me.Label460.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label460.Text = "in               " & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label460.Top = 36.931!
        Me.Label460.Width = 2.459001!
        '
        'Label461
        '
        Me.Label461.Height = 0.1875!
        Me.Label461.HyperLink = Nothing
        Me.Label461.Left = 0.083!
        Me.Label461.Name = "Label461"
        Me.Label461.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label461.Text = "nstem_bolts=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label461.Top = 37.119!
        Me.Label461.Width = 2.5!
        '
        'S216_005
        '
        Me.S216_005.Height = 0.1875!
        Me.S216_005.HyperLink = Nothing
        Me.S216_005.Left = 2.583!
        Me.S216_005.Name = "S216_005"
        Me.S216_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_005.Top = 37.119!
        Me.S216_005.Width = 1.0!
        '
        'Label463
        '
        Me.Label463.Height = 0.1875!
        Me.Label463.HyperLink = Nothing
        Me.Label463.Left = 3.583!
        Me.Label463.Name = "Label463"
        Me.Label463.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label463.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label463.Top = 36.37!
        Me.Label463.Width = 2.459001!
        '
        'Label464
        '
        Me.Label464.Height = 0.1875!
        Me.Label464.HyperLink = Nothing
        Me.Label464.Left = 4.428!
        Me.Label464.Name = "Label464"
        Me.Label464.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label464.Text = "ΦRn_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label464.Top = 40.414!
        Me.Label464.Width = 0.5950003!
        '
        'Label467
        '
        Me.Label467.Height = 0.1875!
        Me.Label467.HyperLink = Nothing
        Me.Label467.Left = 3.928!
        Me.Label467.Name = "Label467"
        Me.Label467.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label467.Text = "Check="
        Me.Label467.Top = 40.787!
        Me.Label467.Width = 1.095!
        '
        'Label470
        '
        Me.Label470.Height = 0.1875!
        Me.Label470.HyperLink = Nothing
        Me.Label470.Left = 0.083!
        Me.Label470.Name = "Label470"
        Me.Label470.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label470.Text = "Gstem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label470.Top = 37.306!
        Me.Label470.Width = 2.5!
        '
        'S216_006
        '
        Me.S216_006.Height = 0.1875!
        Me.S216_006.HyperLink = Nothing
        Me.S216_006.Left = 2.583!
        Me.S216_006.Name = "S216_006"
        Me.S216_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_006.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_006.Top = 37.306!
        Me.S216_006.Width = 1.0!
        '
        'Label472
        '
        Me.Label472.Height = 0.1875!
        Me.Label472.HyperLink = Nothing
        Me.Label472.Left = 3.583!
        Me.Label472.Name = "Label472"
        Me.Label472.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label472.Text = "in"
        Me.Label472.Top = 37.306!
        Me.Label472.Width = 2.459001!
        '
        'Label473
        '
        Me.Label473.Height = 0.1875!
        Me.Label473.HyperLink = Nothing
        Me.Label473.Left = 0.08300069!
        Me.Label473.Name = "Label473"
        Me.Label473.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label473.Text = "gbrp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label473.Top = 37.495!
        Me.Label473.Width = 2.5!
        '
        'S216_007
        '
        Me.S216_007.Height = 0.1875!
        Me.S216_007.HyperLink = Nothing
        Me.S216_007.Left = 2.583!
        Me.S216_007.Name = "S216_007"
        Me.S216_007.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S216_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_007.Top = 37.495!
        Me.S216_007.Width = 1.0!
        '
        'Label475
        '
        Me.Label475.Height = 0.1875!
        Me.Label475.HyperLink = Nothing
        Me.Label475.Left = 3.583!
        Me.Label475.Name = "Label475"
        Me.Label475.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label475.Text = "in"
        Me.Label475.Top = 37.495!
        Me.Label475.Width = 2.459001!
        '
        'Label476
        '
        Me.Label476.Height = 0.1875!
        Me.Label476.HyperLink = Nothing
        Me.Label476.Left = 0.08300021!
        Me.Label476.Name = "Label476"
        Me.Label476.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label476.Text = "bbf="
        Me.Label476.Top = 38.057!
        Me.Label476.Width = 2.5!
        '
        'S216_009
        '
        Me.S216_009.Height = 0.1875!
        Me.S216_009.HyperLink = Nothing
        Me.S216_009.Left = 2.583!
        Me.S216_009.Name = "S216_009"
        Me.S216_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S216_009.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_009.Top = 37.87!
        Me.S216_009.Width = 1.0!
        '
        'Label478
        '
        Me.Label478.Height = 0.1875!
        Me.Label478.HyperLink = Nothing
        Me.Label478.Left = 3.583!
        Me.Label478.Name = "Label478"
        Me.Label478.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label478.Text = "in"
        Me.Label478.Top = 37.87!
        Me.Label478.Width = 2.399!
        '
        'Label479
        '
        Me.Label479.Height = 0.1875!
        Me.Label479.HyperLink = Nothing
        Me.Label479.Left = 0.08300048!
        Me.Label479.Name = "Label479"
        Me.Label479.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label479.Text = "DCR_rupture=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label479.Top = 41.057!
        Me.Label479.Width = 2.5!
        '
        'Label481
        '
        Me.Label481.Height = 0.1875!
        Me.Label481.HyperLink = Nothing
        Me.Label481.Left = 3.521!
        Me.Label481.Name = "Label481"
        Me.Label481.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label481.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label481.Top = 39.12!
        Me.Label481.Width = 0.9789996!
        '
        'Label482
        '
        Me.Label482.Height = 0.1875!
        Me.Label482.HyperLink = Nothing
        Me.Label482.Left = 0.08300027!
        Me.Label482.Name = "Label482"
        Me.Label482.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label482.Text = "Check="
        Me.Label482.Top = 41.246!
        Me.Label482.Width = 2.5!
        '
        'Label485
        '
        Me.Label485.Height = 0.1875!
        Me.Label485.HyperLink = Nothing
        Me.Label485.Left = 0.08300021!
        Me.Label485.Name = "Label485"
        Me.Label485.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label485.Text = "h="
        Me.Label485.Top = 38.244!
        Me.Label485.Width = 2.5!
        '
        'S216_010
        '
        Me.S216_010.Height = 0.1875!
        Me.S216_010.HyperLink = Nothing
        Me.S216_010.Left = 2.583!
        Me.S216_010.Name = "S216_010"
        Me.S216_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S216_010.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_010.Top = 38.057!
        Me.S216_010.Width = 1.0!
        '
        'Label487
        '
        Me.Label487.Height = 0.1875!
        Me.Label487.HyperLink = Nothing
        Me.Label487.Left = 3.583!
        Me.Label487.Name = "Label487"
        Me.Label487.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label487.Text = "in"
        Me.Label487.Top = 38.057!
        Me.Label487.Width = 2.397!
        '
        'Label488
        '
        Me.Label488.Height = 0.1875!
        Me.Label488.HyperLink = Nothing
        Me.Label488.Left = 0.083!
        Me.Label488.Name = "Label488"
        Me.Label488.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label488.Text = "d_stem="
        Me.Label488.Top = 37.867!
        Me.Label488.Width = 2.5!
        '
        'S216_008
        '
        Me.S216_008.Height = 0.1875!
        Me.S216_008.HyperLink = Nothing
        Me.S216_008.Left = 2.583!
        Me.S216_008.Name = "S216_008"
        Me.S216_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S216_008.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_008.Top = 37.68!
        Me.S216_008.Width = 1.0!
        '
        'Label490
        '
        Me.Label490.Height = 0.1875!
        Me.Label490.HyperLink = Nothing
        Me.Label490.Left = 3.583!
        Me.Label490.Name = "Label490"
        Me.Label490.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label490.Text = "in"
        Me.Label490.Top = 37.683!
        Me.Label490.Width = 2.459001!
        '
        'Label491
        '
        Me.Label491.Height = 0.1875!
        Me.Label491.HyperLink = Nothing
        Me.Label491.Left = 0.08300027!
        Me.Label491.Name = "Label491"
        Me.Label491.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label491.Text = "Rn_yield="
        Me.Label491.Top = 39.496!
        Me.Label491.Width = 2.5!
        '
        'S216_015
        '
        Me.S216_015.Height = 0.1875!
        Me.S216_015.HyperLink = Nothing
        Me.S216_015.Left = 2.583!
        Me.S216_015.Name = "S216_015"
        Me.S216_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_015.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_015.Top = 39.496!
        Me.S216_015.Width = 1.0!
        '
        'Label493
        '
        Me.Label493.Height = 0.1875!
        Me.Label493.HyperLink = Nothing
        Me.Label493.Left = 3.583!
        Me.Label493.Name = "Label493"
        Me.Label493.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label493.Text = "in^2"
        Me.Label493.Top = 39.496!
        Me.Label493.Width = 0.3540001!
        '
        'Label494
        '
        Me.Label494.Height = 0.1875!
        Me.Label494.HyperLink = Nothing
        Me.Label494.Left = 0.08300048!
        Me.Label494.Name = "Label494"
        Me.Label494.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label494.Text = "DCR_yield=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label494.Top = 39.89201!
        Me.Label494.Width = 2.5!
        '
        'Label497
        '
        Me.Label497.Height = 0.1875!
        Me.Label497.HyperLink = Nothing
        Me.Label497.Left = 0.08300027!
        Me.Label497.Name = "Label497"
        Me.Label497.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label497.Text = "Check="
        Me.Label497.Top = 40.081!
        Me.Label497.Width = 2.5!
        '
        'Label500
        '
        Me.Label500.Height = 0.1875!
        Me.Label500.HyperLink = Nothing
        Me.Label500.Left = 0.08300027!
        Me.Label500.Name = "Label500"
        Me.Label500.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label500.Text = "ΦRn_yield="
        Me.Label500.Top = 39.683!
        Me.Label500.Width = 2.5!
        '
        'S216_016
        '
        Me.S216_016.Height = 0.1875!
        Me.S216_016.HyperLink = Nothing
        Me.S216_016.Left = 2.583!
        Me.S216_016.Name = "S216_016"
        Me.S216_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_016.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_016.Top = 39.683!
        Me.S216_016.Width = 1.0!
        '
        'Label502
        '
        Me.Label502.Height = 0.1875!
        Me.Label502.HyperLink = Nothing
        Me.Label502.Left = 3.583!
        Me.Label502.Name = "Label502"
        Me.Label502.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label502.Text = "kips "
        Me.Label502.Top = 39.682!
        Me.Label502.Width = 0.313!
        '
        'Label503
        '
        Me.Label503.Height = 0.1875!
        Me.Label503.HyperLink = Nothing
        Me.Label503.Left = 0.08300027!
        Me.Label503.Name = "Label503"
        Me.Label503.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label503.Text = "Lgv="
        Me.Label503.Top = 39.12003!
        Me.Label503.Width = 2.5!
        '
        'S216_013
        '
        Me.S216_013.Height = 0.1875!
        Me.S216_013.HyperLink = Nothing
        Me.S216_013.Left = 2.583!
        Me.S216_013.Name = "S216_013"
        Me.S216_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_013.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_013.Top = 39.119!
        Me.S216_013.Width = 1.0!
        '
        'Label505
        '
        Me.Label505.Height = 0.1875!
        Me.Label505.HyperLink = Nothing
        Me.Label505.Left = 0.08300027!
        Me.Label505.Name = "Label505"
        Me.Label505.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label505.Text = "Agv="
        Me.Label505.Top = 39.307!
        Me.Label505.Width = 2.5!
        '
        'S216_014
        '
        Me.S216_014.Height = 0.1875!
        Me.S216_014.HyperLink = Nothing
        Me.S216_014.Left = 2.583!
        Me.S216_014.Name = "S216_014"
        Me.S216_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_014.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_014.Top = 39.30801!
        Me.S216_014.Width = 1.0!
        '
        'Label507
        '
        Me.Label507.Height = 0.1875!
        Me.Label507.HyperLink = Nothing
        Me.Label507.Left = 3.583!
        Me.Label507.Name = "Label507"
        Me.Label507.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label507.Text = "in                " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label507.Top = 39.307!
        Me.Label507.Width = 0.2709999!
        '
        'Label517
        '
        Me.Label517.Height = 0.1875!
        Me.Label517.HyperLink = Nothing
        Me.Label517.Left = 4.698!
        Me.Label517.Name = "Label517"
        Me.Label517.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label517.Text = "Lnt="
        Me.Label517.Top = 39.105!
        Me.Label517.Width = 0.3129997!
        '
        'S216_019
        '
        Me.S216_019.Height = 0.1875!
        Me.S216_019.HyperLink = Nothing
        Me.S216_019.Left = 5.011!
        Me.S216_019.Name = "S216_019"
        Me.S216_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_019.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_019.Top = 39.105!
        Me.S216_019.Width = 1.0!
        '
        'Label519
        '
        Me.Label519.Height = 0.1875!
        Me.Label519.HyperLink = Nothing
        Me.Label519.Left = 6.011!
        Me.Label519.Name = "Label519"
        Me.Label519.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label519.Text = "in"
        Me.Label519.Top = 39.105!
        Me.Label519.Width = 0.937!
        '
        'Label520
        '
        Me.Label520.Height = 0.1875!
        Me.Label520.HyperLink = Nothing
        Me.Label520.Left = 4.698!
        Me.Label520.Name = "Label520"
        Me.Label520.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label520.Text = "Ant=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label520.Top = 39.30701!
        Me.Label520.Width = 0.322!
        '
        'S216_020
        '
        Me.S216_020.Height = 0.1875!
        Me.S216_020.HyperLink = Nothing
        Me.S216_020.Left = 5.011!
        Me.S216_020.Name = "S216_020"
        Me.S216_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_020.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_020.Top = 39.30701!
        Me.S216_020.Width = 1.0!
        '
        'Label522
        '
        Me.Label522.Height = 0.1875!
        Me.Label522.HyperLink = Nothing
        Me.Label522.Left = 6.011!
        Me.Label522.Name = "Label522"
        Me.Label522.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label522.Text = "in^2"
        Me.Label522.Top = 39.30701!
        Me.Label522.Width = 0.938!
        '
        'Label508
        '
        Me.Label508.Height = 0.1875!
        Me.Label508.HyperLink = Nothing
        Me.Label508.Left = 0.08300027!
        Me.Label508.Name = "Label508"
        Me.Label508.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label508.Text = "Lnv=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label508.Top = 40.307!
        Me.Label508.Width = 2.5!
        '
        'S216_021
        '
        Me.S216_021.Height = 0.1875!
        Me.S216_021.HyperLink = Nothing
        Me.S216_021.Left = 2.583!
        Me.S216_021.Name = "S216_021"
        Me.S216_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_021.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_021.Top = 40.308!
        Me.S216_021.Width = 1.0!
        '
        'Label510
        '
        Me.Label510.Height = 0.1875!
        Me.Label510.HyperLink = Nothing
        Me.Label510.Left = 3.583!
        Me.Label510.Name = "Label510"
        Me.Label510.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label510.Text = "in                " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label510.Top = 40.307!
        Me.Label510.Width = 1.0!
        '
        'Label511
        '
        Me.Label511.Height = 0.1875!
        Me.Label511.HyperLink = Nothing
        Me.Label511.Left = 0.08300048!
        Me.Label511.Name = "Label511"
        Me.Label511.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label511.Text = "Anv=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label511.Top = 40.494!
        Me.Label511.Width = 2.5!
        '
        'S216_022
        '
        Me.S216_022.Height = 0.1875!
        Me.S216_022.HyperLink = Nothing
        Me.S216_022.Left = 2.583!
        Me.S216_022.Name = "S216_022"
        Me.S216_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_022.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_022.Top = 40.494!
        Me.S216_022.Width = 1.0!
        '
        'Label513
        '
        Me.Label513.Height = 0.1875!
        Me.Label513.HyperLink = Nothing
        Me.Label513.Left = 3.583!
        Me.Label513.Name = "Label513"
        Me.Label513.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label513.Text = "in^2          " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label513.Top = 40.494!
        Me.Label513.Width = 1.0!
        '
        'Label514
        '
        Me.Label514.Height = 0.1875!
        Me.Label514.HyperLink = Nothing
        Me.Label514.Left = 0.08300027!
        Me.Label514.Name = "Label514"
        Me.Label514.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label514.Text = "Rn_rupture=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label514.Top = 40.682!
        Me.Label514.Width = 2.5!
        '
        'S216_023
        '
        Me.S216_023.Height = 0.1875!
        Me.S216_023.HyperLink = Nothing
        Me.S216_023.Left = 2.583!
        Me.S216_023.Name = "S216_023"
        Me.S216_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_023.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_023.Top = 40.682!
        Me.S216_023.Width = 1.0!
        '
        'Label516
        '
        Me.Label516.Height = 0.1875!
        Me.Label516.HyperLink = Nothing
        Me.Label516.Left = 3.583!
        Me.Label516.Name = "Label516"
        Me.Label516.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label516.Text = "kips"
        Me.Label516.Top = 40.682!
        Me.Label516.Width = 1.063!
        '
        'Label523
        '
        Me.Label523.Height = 0.1875!
        Me.Label523.HyperLink = Nothing
        Me.Label523.Left = 0.08300027!
        Me.Label523.Name = "Label523"
        Me.Label523.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label523.Text = "ΦRn_rupture=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label523.Top = 40.87!
        Me.Label523.Width = 2.5!
        '
        'S216_024
        '
        Me.S216_024.Height = 0.1875!
        Me.S216_024.HyperLink = Nothing
        Me.S216_024.Left = 2.594!
        Me.S216_024.Name = "S216_024"
        Me.S216_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_024.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_024.Top = 40.869!
        Me.S216_024.Width = 0.989!
        '
        'Label525
        '
        Me.Label525.Height = 0.1875!
        Me.Label525.HyperLink = Nothing
        Me.Label525.Left = 3.583!
        Me.Label525.Name = "Label525"
        Me.Label525.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label525.Text = "kips"
        Me.Label525.Top = 40.87!
        Me.Label525.Width = 1.0!
        '
        'Label526
        '
        Me.Label526.Height = 0.1875!
        Me.Label526.HyperLink = Nothing
        Me.Label526.Left = 4.114001!
        Me.Label526.Name = "Label526"
        Me.Label526.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label526.Text = "Rn_BS1="
        Me.Label526.Top = 39.853!
        Me.Label526.Width = 0.8969998!
        '
        'S216_025
        '
        Me.S216_025.Height = 0.1875!
        Me.S216_025.HyperLink = Nothing
        Me.S216_025.Left = 5.011!
        Me.S216_025.Name = "S216_025"
        Me.S216_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_025.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_025.Top = 39.853!
        Me.S216_025.Width = 1.0!
        '
        'Label528
        '
        Me.Label528.Height = 0.1875!
        Me.Label528.HyperLink = Nothing
        Me.Label528.Left = 3.583!
        Me.Label528.Name = "Label528"
        Me.Label528.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label528.Text = "kips"
        Me.Label528.Top = 39.869!
        Me.Label528.Width = 1.0!
        '
        'Label529
        '
        Me.Label529.Height = 0.1875!
        Me.Label529.HyperLink = Nothing
        Me.Label529.Left = 4.479!
        Me.Label529.Name = "Label529"
        Me.Label529.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label529.Text = "Rn_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label529.Top = 40.04201!
        Me.Label529.Width = 0.5320015!
        '
        'S216_026
        '
        Me.S216_026.Height = 0.1875!
        Me.S216_026.HyperLink = Nothing
        Me.S216_026.Left = 5.011!
        Me.S216_026.Name = "S216_026"
        Me.S216_026.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_026.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_026.Top = 40.04201!
        Me.S216_026.Width = 1.0!
        '
        'Label531
        '
        Me.Label531.Height = 0.1875!
        Me.Label531.HyperLink = Nothing
        Me.Label531.Left = 6.011!
        Me.Label531.Name = "Label531"
        Me.Label531.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label531.Text = "kips"
        Me.Label531.Top = 40.04201!
        Me.Label531.Width = 0.8749993!
        '
        'Label532
        '
        Me.Label532.Height = 0.1875!
        Me.Label532.HyperLink = Nothing
        Me.Label532.Left = 4.272001!
        Me.Label532.Name = "Label532"
        Me.Label532.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label532.Text = "Rn_BS="
        Me.Label532.Top = 40.226!
        Me.Label532.Width = 0.7499996!
        '
        'S216_027
        '
        Me.S216_027.Height = 0.1875!
        Me.S216_027.HyperLink = Nothing
        Me.S216_027.Left = 5.011!
        Me.S216_027.Name = "S216_027"
        Me.S216_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S216_027.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_027.Top = 40.227!
        Me.S216_027.Width = 1.0!
        '
        'Label534
        '
        Me.Label534.Height = 0.1875!
        Me.Label534.HyperLink = Nothing
        Me.Label534.Left = 6.011!
        Me.Label534.Name = "Label534"
        Me.Label534.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label534.Text = "kips        min(Rn_BS1, Rn_BS2)"
        Me.Label534.Top = 40.23!
        Me.Label534.Width = 1.478999!
        '
        'S216_017
        '
        Me.S216_017.Height = 0.1875!
        Me.S216_017.HyperLink = Nothing
        Me.S216_017.Left = 2.583!
        Me.S216_017.Name = "S216_017"
        Me.S216_017.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S216_017.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_017.Top = 39.894!
        Me.S216_017.Width = 1.0!
        '
        'S216_018
        '
        Me.S216_018.Height = 0.1875!
        Me.S216_018.HyperLink = Nothing
        Me.S216_018.Left = 2.583!
        Me.S216_018.Name = "S216_018"
        Me.S216_018.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S216_018.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_018.Top = 41.057!
        Me.S216_018.Width = 1.0!
        '
        'S216_028
        '
        Me.S216_028.Height = 0.1875!
        Me.S216_028.HyperLink = Nothing
        Me.S216_028.Left = 5.011!
        Me.S216_028.Name = "S216_028"
        Me.S216_028.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S216_028.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_028.Top = 40.415!
        Me.S216_028.Width = 1.0!
        '
        'Shape20
        '
        Me.Shape20.Height = 0.1875!
        Me.Shape20.Left = 2.583!
        Me.Shape20.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape20.Name = "Shape20"
        Me.Shape20.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape20.Top = 38.639!
        Me.Shape20.Width = 1.0!
        '
        'Shape21
        '
        Me.Shape21.Height = 0.1875!
        Me.Shape21.Left = 2.583!
        Me.Shape21.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape21.Name = "Shape21"
        Me.Shape21.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape21.Top = 38.828!
        Me.Shape21.Width = 1.0!
        '
        'Shape31
        '
        Me.Shape31.Height = 0.1875!
        Me.Shape31.Left = 2.583!
        Me.Shape31.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape31.Name = "Shape31"
        Me.Shape31.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape31.Top = 40.079!
        Me.Shape31.Width = 1.0!
        '
        'Label228
        '
        Me.Label228.Height = 0.1875!
        Me.Label228.HyperLink = Nothing
        Me.Label228.Left = 3.584!
        Me.Label228.Name = "Label228"
        Me.Label228.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label228.Text = "in"
        Me.Label228.Top = 39.13!
        Me.Label228.Width = 0.937!
        '
        'SUM_004
        '
        Me.SUM_004.Height = 0.188!
        Me.SUM_004.HyperLink = Nothing
        Me.SUM_004.Left = 3.624!
        Me.SUM_004.Name = "SUM_004"
        Me.SUM_004.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_004.Top = 44.43198!
        Me.SUM_004.Width = 0.75!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.3120013!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: right; vertical-al" &
    "ign: middle"
        Me.Label4.Text = "DCR Summary:"
        Me.Label4.Top = 43.49!
        Me.Label4.Width = 1.187!
        '
        'SUM_006
        '
        Me.SUM_006.Height = 0.188!
        Me.SUM_006.HyperLink = Nothing
        Me.SUM_006.Left = 4.373999!
        Me.SUM_006.Name = "SUM_006"
        Me.SUM_006.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_006.Top = 43.86599!
        Me.SUM_006.Width = 0.75!
        '
        'SUM_007
        '
        Me.SUM_007.Height = 0.188!
        Me.SUM_007.HyperLink = Nothing
        Me.SUM_007.Left = 4.373999!
        Me.SUM_007.Name = "SUM_007"
        Me.SUM_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_007.Top = 44.05099!
        Me.SUM_007.Width = 0.75!
        '
        'SUM_002
        '
        Me.SUM_002.Height = 0.188!
        Me.SUM_002.HyperLink = Nothing
        Me.SUM_002.Left = 3.624!
        Me.SUM_002.Name = "SUM_002"
        Me.SUM_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_002.Top = 44.05099!
        Me.SUM_002.Width = 0.75!
        '
        'SUM_001
        '
        Me.SUM_001.Height = 0.188!
        Me.SUM_001.HyperLink = Nothing
        Me.SUM_001.Left = 3.624!
        Me.SUM_001.Name = "SUM_001"
        Me.SUM_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_001.Top = 43.86599!
        Me.SUM_001.Width = 0.75!
        '
        'Label212
        '
        Me.Label212.Height = 0.188!
        Me.Label212.HyperLink = Nothing
        Me.Label212.Left = 2.374!
        Me.Label212.Name = "Label212"
        Me.Label212.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label212.Text = "Link Strength DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label212.Top = 44.05099!
        Me.Label212.Width = 1.25!
        '
        'Label18
        '
        Me.Label18.Height = 0.188!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 2.374!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label18.Text = "Beam tf_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 43.86599!
        Me.Label18.Width = 1.25!
        '
        'Label26
        '
        Me.Label26.Height = 0.188!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 1.499!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label26.Text = "Beam"
        Me.Label26.Top = 43.865!
        Me.Label26.Width = 0.875!
        '
        'Label34
        '
        Me.Label34.Height = 0.188!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 2.499!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label34.Text = "Check"
        Me.Label34.Top = 43.678!
        Me.Label34.Width = 1.0!
        '
        'Label44
        '
        Me.Label44.Height = 0.188!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 1.499!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label44.Text = "Component"
        Me.Label44.Top = 43.678!
        Me.Label44.Width = 0.864!
        '
        'Label220
        '
        Me.Label220.Height = 0.188!
        Me.Label220.HyperLink = Nothing
        Me.Label220.Left = 3.624!
        Me.Label220.Name = "Label220"
        Me.Label220.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label220.Text = "DCR"
        Me.Label220.Top = 43.678!
        Me.Label220.Width = 0.75!
        '
        'Label51
        '
        Me.Label51.Height = 0.188!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 4.373999!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label51.Text = "Limit"
        Me.Label51.Top = 43.678!
        Me.Label51.Width = 0.75!
        '
        'Label60
        '
        Me.Label60.Height = 0.3769992!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 1.499!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label60.Text = "Link"
        Me.Label60.Top = 44.05399!
        Me.Label60.Width = 0.875!
        '
        'Label66
        '
        Me.Label66.Height = 0.188!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 2.374!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label66.Text = "Ly_yield DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label66.Top = 44.24198!
        Me.Label66.Width = 1.25!
        '
        'SUM_003
        '
        Me.SUM_003.Height = 0.188!
        Me.SUM_003.HyperLink = Nothing
        Me.SUM_003.Left = 3.624!
        Me.SUM_003.Name = "SUM_003"
        Me.SUM_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_003.Top = 44.24198!
        Me.SUM_003.Width = 0.75!
        '
        'SUM_008
        '
        Me.SUM_008.Height = 0.188!
        Me.SUM_008.HyperLink = Nothing
        Me.SUM_008.Left = 4.373999!
        Me.SUM_008.Name = "SUM_008"
        Me.SUM_008.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_008.Top = 44.24198!
        Me.SUM_008.Width = 0.75!
        '
        'Label73
        '
        Me.Label73.Height = 0.379!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 1.499!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label73.Text = "BRP"
        Me.Label73.Top = 44.43298!
        Me.Label73.Width = 0.875!
        '
        'Label79
        '
        Me.Label79.Height = 0.188!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 2.385!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label79.Text = "t_BRP DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label79.Top = 44.43198!
        Me.Label79.Width = 1.239!
        '
        'SUM_009
        '
        Me.SUM_009.Height = 0.188!
        Me.SUM_009.HyperLink = Nothing
        Me.SUM_009.Left = 4.373999!
        Me.SUM_009.Name = "SUM_009"
        Me.SUM_009.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_009.Top = 44.43198!
        Me.SUM_009.Width = 0.75!
        '
        'Label87
        '
        Me.Label87.Height = 0.188!
        Me.Label87.HyperLink = Nothing
        Me.Label87.Left = 2.374!
        Me.Label87.Name = "Label87"
        Me.Label87.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label87.Text = "BRP Bolt DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label87.Top = 44.623!
        Me.Label87.Width = 1.25!
        '
        'SUM_005
        '
        Me.SUM_005.Height = 0.1459925!
        Me.SUM_005.HyperLink = Nothing
        Me.SUM_005.Left = 3.634!
        Me.SUM_005.Name = "SUM_005"
        Me.SUM_005.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_005.Top = 44.64398!
        Me.SUM_005.Width = 0.75!
        '
        'SUM_010
        '
        Me.SUM_010.Height = 0.1459925!
        Me.SUM_010.HyperLink = Nothing
        Me.SUM_010.Left = 4.373999!
        Me.SUM_010.Name = "SUM_010"
        Me.SUM_010.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_010.Top = 44.63298!
        Me.SUM_010.Width = 0.75!
        '
        'SUM_011
        '
        Me.SUM_011.Height = 0.188!
        Me.SUM_011.HyperLink = Nothing
        Me.SUM_011.Left = 5.123!
        Me.SUM_011.Name = "SUM_011"
        Me.SUM_011.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_011.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_011.Top = 43.86298!
        Me.SUM_011.Width = 0.75!
        '
        'SUM_012
        '
        Me.SUM_012.Height = 0.188!
        Me.SUM_012.HyperLink = Nothing
        Me.SUM_012.Left = 5.123!
        Me.SUM_012.Name = "SUM_012"
        Me.SUM_012.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_012.Top = 44.04798!
        Me.SUM_012.Width = 0.75!
        '
        'Label225
        '
        Me.Label225.Height = 0.188!
        Me.Label225.HyperLink = Nothing
        Me.Label225.Left = 5.123!
        Me.Label225.Name = "Label225"
        Me.Label225.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label225.Text = "Result"
        Me.Label225.Top = 43.67499!
        Me.Label225.Width = 0.75!
        '
        'SUM_013
        '
        Me.SUM_013.Height = 0.188!
        Me.SUM_013.HyperLink = Nothing
        Me.SUM_013.Left = 5.123!
        Me.SUM_013.Name = "SUM_013"
        Me.SUM_013.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_013.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_013.Top = 44.23898!
        Me.SUM_013.Width = 0.75!
        '
        'SUM_014
        '
        Me.SUM_014.Height = 0.188!
        Me.SUM_014.HyperLink = Nothing
        Me.SUM_014.Left = 5.123!
        Me.SUM_014.Name = "SUM_014"
        Me.SUM_014.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_014.Top = 44.42899!
        Me.SUM_014.Width = 0.7500007!
        '
        'SUM_015
        '
        Me.SUM_015.Height = 0.188!
        Me.SUM_015.HyperLink = Nothing
        Me.SUM_015.Left = 5.123!
        Me.SUM_015.Name = "SUM_015"
        Me.SUM_015.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_015.Top = 44.61999!
        Me.SUM_015.Width = 0.75!
        '
        'Line30
        '
        Me.Line30.Height = 0.001018524!
        Me.Line30.Left = 1.499!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 43.68598!
        Me.Line30.Width = 4.374002!
        Me.Line30.X1 = 1.499!
        Me.Line30.X2 = 5.873002!
        Me.Line30.Y1 = 43.68598!
        Me.Line30.Y2 = 43.687!
        '
        'Line17
        '
        Me.Line17.Height = 0.0009880066!
        Me.Line17.Left = 1.499!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 43.865!
        Me.Line17.Width = 4.374002!
        Me.Line17.X1 = 1.499!
        Me.Line17.X2 = 5.873002!
        Me.Line17.Y1 = 43.865!
        Me.Line17.Y2 = 43.86599!
        '
        'Line21
        '
        Me.Line21.Height = 0!
        Me.Line21.Left = 1.489!
        Me.Line21.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line21.LineWeight = 1.0!
        Me.Line21.Name = "Line21"
        Me.Line21.Top = 44.05399!
        Me.Line21.Width = 4.384002!
        Me.Line21.X1 = 1.489!
        Me.Line21.X2 = 5.873002!
        Me.Line21.Y1 = 44.05399!
        Me.Line21.Y2 = 44.05399!
        '
        'Line23
        '
        Me.Line23.Height = 0.003997803!
        Me.Line23.Left = 2.374!
        Me.Line23.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line23.LineWeight = 1.0!
        Me.Line23.Name = "Line23"
        Me.Line23.Top = 44.23598!
        Me.Line23.Width = 3.499!
        Me.Line23.X1 = 2.374!
        Me.Line23.X2 = 5.873!
        Me.Line23.Y1 = 44.23998!
        Me.Line23.Y2 = 44.23598!
        '
        'Line24
        '
        Me.Line24.Height = 0.001010895!
        Me.Line24.Left = 1.499!
        Me.Line24.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line24.LineWeight = 1.0!
        Me.Line24.Name = "Line24"
        Me.Line24.Top = 44.42699!
        Me.Line24.Width = 4.374002!
        Me.Line24.X1 = 1.499!
        Me.Line24.X2 = 5.873002!
        Me.Line24.Y1 = 44.42699!
        Me.Line24.Y2 = 44.428!
        '
        'Line26
        '
        Me.Line26.Height = 0.00001144409!
        Me.Line26.Left = 2.384!
        Me.Line26.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line26.LineWeight = 1.0!
        Me.Line26.Name = "Line26"
        Me.Line26.Top = 44.62299!
        Me.Line26.Width = 3.489!
        Me.Line26.X1 = 2.384!
        Me.Line26.X2 = 5.873!
        Me.Line26.Y1 = 44.62299!
        Me.Line26.Y2 = 44.623!
        '
        'Line31
        '
        Me.Line31.Height = 1.521988!
        Me.Line31.Left = 4.373999!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 43.67699!
        Me.Line31.Width = 0!
        Me.Line31.X1 = 4.373999!
        Me.Line31.X2 = 4.373999!
        Me.Line31.Y1 = 43.67699!
        Me.Line31.Y2 = 45.19898!
        '
        'Line32
        '
        Me.Line32.Height = 1.510998!
        Me.Line32.Left = 5.123999!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 43.68598!
        Me.Line32.Width = 0!
        Me.Line32.X1 = 5.123999!
        Me.Line32.X2 = 5.123999!
        Me.Line32.Y1 = 43.68598!
        Me.Line32.Y2 = 45.19698!
        '
        'SUM_016
        '
        Me.SUM_016.Height = 0.1459964!
        Me.SUM_016.HyperLink = Nothing
        Me.SUM_016.Left = 3.739!
        Me.SUM_016.Name = "SUM_016"
        Me.SUM_016.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_016.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_016.Top = 44.83598!
        Me.SUM_016.Width = 0.563!
        '
        'Label234
        '
        Me.Label234.Height = 0.188!
        Me.Label234.HyperLink = Nothing
        Me.Label234.Left = 2.375!
        Me.Label234.Name = "Label234"
        Me.Label234.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label234.Text = "BRP Bolt Edge Dist>1d"
        Me.Label234.Top = 44.81499!
        Me.Label234.Width = 1.251!
        '
        'Label243
        '
        Me.Label243.Height = 0.188!
        Me.Label243.HyperLink = Nothing
        Me.Label243.Left = 2.374!
        Me.Label243.Name = "Label243"
        Me.Label243.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label243.Text = "Stem Bolt Edge Dist>1d"
        Me.Label243.Top = 45.00598!
        Me.Label243.Width = 1.251!
        '
        'SUM_017
        '
        Me.SUM_017.Height = 0.188!
        Me.SUM_017.HyperLink = Nothing
        Me.SUM_017.Left = 3.770999!
        Me.SUM_017.Name = "SUM_017"
        Me.SUM_017.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_017.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_017.Top = 45.00598!
        Me.SUM_017.Width = 0.4889998!
        '
        'SUM_018
        '
        Me.SUM_018.Height = 0.188!
        Me.SUM_018.HyperLink = Nothing
        Me.SUM_018.Left = 5.134!
        Me.SUM_018.Name = "SUM_018"
        Me.SUM_018.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_018.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_018.Top = 44.81198!
        Me.SUM_018.Width = 0.739!
        '
        'SUM_019
        '
        Me.SUM_019.Height = 0.188!
        Me.SUM_019.HyperLink = Nothing
        Me.SUM_019.Left = 5.134!
        Me.SUM_019.Name = "SUM_019"
        Me.SUM_019.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_019.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_019.Top = 45.00298!
        Me.SUM_019.Width = 0.739!
        '
        'Line20
        '
        Me.Line20.Height = 0!
        Me.Line20.Left = 1.5!
        Me.Line20.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line20.LineWeight = 1.0!
        Me.Line20.Name = "Line20"
        Me.Line20.Top = 45.19399!
        Me.Line20.Width = 4.373999!
        Me.Line20.X1 = 1.5!
        Me.Line20.X2 = 5.873999!
        Me.Line20.Y1 = 45.19399!
        Me.Line20.Y2 = 45.19399!
        '
        'Line29
        '
        Me.Line29.Height = 1.510998!
        Me.Line29.Left = 3.624!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 43.68598!
        Me.Line29.Width = 0!
        Me.Line29.X1 = 3.624!
        Me.Line29.X2 = 3.624!
        Me.Line29.Y1 = 43.68598!
        Me.Line29.Y2 = 45.19698!
        '
        'Line19
        '
        Me.Line19.Height = 0!
        Me.Line19.Left = 1.5!
        Me.Line19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line19.LineWeight = 1.0!
        Me.Line19.Name = "Line19"
        Me.Line19.Top = 45.00598!
        Me.Line19.Width = 4.373!
        Me.Line19.X1 = 1.5!
        Me.Line19.X2 = 5.873!
        Me.Line19.Y1 = 45.00598!
        Me.Line19.Y2 = 45.00598!
        '
        'Line18
        '
        Me.Line18.Height = 0!
        Me.Line18.Left = 1.489!
        Me.Line18.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line18.LineWeight = 1.0!
        Me.Line18.Name = "Line18"
        Me.Line18.Top = 44.81098!
        Me.Line18.Width = 4.384!
        Me.Line18.X1 = 1.489!
        Me.Line18.X2 = 5.873!
        Me.Line18.Y1 = 44.81098!
        Me.Line18.Y2 = 44.81098!
        '
        'Line33
        '
        Me.Line33.Height = 1.505009!
        Me.Line33.Left = 5.873!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 43.68598!
        Me.Line33.Width = 0.0000009536743!
        Me.Line33.X1 = 5.873!
        Me.Line33.X2 = 5.873001!
        Me.Line33.Y1 = 43.68598!
        Me.Line33.Y2 = 45.19099!
        '
        'Line27
        '
        Me.Line27.Height = 1.511997!
        Me.Line27.Left = 1.499!
        Me.Line27.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line27.LineWeight = 1.0!
        Me.Line27.Name = "Line27"
        Me.Line27.Top = 43.68698!
        Me.Line27.Width = 0.001000047!
        Me.Line27.X1 = 1.5!
        Me.Line27.X2 = 1.499!
        Me.Line27.Y1 = 43.68698!
        Me.Line27.Y2 = 45.19898!
        '
        'Line28
        '
        Me.Line28.Height = 1.522007!
        Me.Line28.Left = 2.374!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 43.68598!
        Me.Line28.Width = 0!
        Me.Line28.X1 = 2.374!
        Me.Line28.X2 = 2.374!
        Me.Line28.Y1 = 43.68598!
        Me.Line28.Y2 = 45.20799!
        '
        'Label170
        '
        Me.Label170.Height = 0.2910004!
        Me.Label170.HyperLink = Nothing
        Me.Label170.Left = 3.5!
        Me.Label170.Name = "Label170"
        Me.Label170.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label170.Text = "in                 =max (bbf_BRP_bolt, bbf_stem_bolt)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(10) & "    "
        Me.Label170.Top = 23.588!
        Me.Label170.Width = 3.594!
        '
        'Label173
        '
        Me.Label173.Height = 0.1875!
        Me.Label173.HyperLink = Nothing
        Me.Label173.Left = 4.229!
        Me.Label173.Name = "Label173"
        Me.Label173.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label173.Text = "DCR_BS="
        Me.Label173.Top = 40.6!
        Me.Label173.Width = 0.7819996!
        '
        'S216_034
        '
        Me.S216_034.Height = 0.1875!
        Me.S216_034.HyperLink = Nothing
        Me.S216_034.Left = 5.012!
        Me.S216_034.Name = "S216_034"
        Me.S216_034.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S216_034.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S216_034.Top = 40.601!
        Me.S216_034.Width = 1.0!
        '
        'Label252
        '
        Me.Label252.Height = 0.1875!
        Me.Label252.HyperLink = Nothing
        Me.Label252.Left = 6.012!
        Me.Label252.Name = "Label252"
        Me.Label252.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label252.Text = "kips"
        Me.Label252.Top = 39.852!
        Me.Label252.Width = 0.8749993!
        '
        'Label256
        '
        Me.Label256.Height = 0.1875!
        Me.Label256.HyperLink = Nothing
        Me.Label256.Left = 6.012!
        Me.Label256.Name = "Label256"
        Me.Label256.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label256.Text = "kips"
        Me.Label256.Top = 40.417!
        Me.Label256.Width = 0.8749993!
        '
        'Shape27
        '
        Me.Shape27.Height = 0.1875!
        Me.Shape27.Left = 5.012!
        Me.Shape27.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape27.Name = "Shape27"
        Me.Shape27.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape27.Top = 40.787!
        Me.Shape27.Width = 1.0!
        '
        'Label248
        '
        Me.Label248.Height = 0.1875!
        Me.Label248.HyperLink = Nothing
        Me.Label248.Left = 4.438!
        Me.Label248.Name = "Label248"
        Me.Label248.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label248.Text = "ΦRn_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label248.Top = 31.89299!
        Me.Label248.Width = 0.5819998!
        '
        'Label269
        '
        Me.Label269.Height = 0.1875!
        Me.Label269.HyperLink = Nothing
        Me.Label269.Left = 3.938!
        Me.Label269.Name = "Label269"
        Me.Label269.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label269.Text = "Check="
        Me.Label269.Top = 32.26599!
        Me.Label269.Width = 1.062!
        '
        'Label272
        '
        Me.Label272.Height = 0.1875!
        Me.Label272.HyperLink = Nothing
        Me.Label272.Left = 4.124!
        Me.Label272.Name = "Label272"
        Me.Label272.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label272.Text = "Rn_BS1="
        Me.Label272.Top = 31.33199!
        Me.Label272.Width = 0.897!
        '
        'Label276
        '
        Me.Label276.Height = 0.1875!
        Me.Label276.HyperLink = Nothing
        Me.Label276.Left = 4.488999!
        Me.Label276.Name = "Label276"
        Me.Label276.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label276.Text = "Rn_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label276.Top = 31.521!
        Me.Label276.Width = 0.5320016!
        '
        'Label278
        '
        Me.Label278.Height = 0.1875!
        Me.Label278.HyperLink = Nothing
        Me.Label278.Left = 6.021!
        Me.Label278.Name = "Label278"
        Me.Label278.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label278.Text = "kips"
        Me.Label278.Top = 31.521!
        Me.Label278.Width = 0.8749993!
        '
        'Label281
        '
        Me.Label281.Height = 0.1875!
        Me.Label281.HyperLink = Nothing
        Me.Label281.Left = 4.282!
        Me.Label281.Name = "Label281"
        Me.Label281.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label281.Text = "Rn_BS="
        Me.Label281.Top = 31.70499!
        Me.Label281.Width = 0.7389994!
        '
        'Label290
        '
        Me.Label290.Height = 0.1875!
        Me.Label290.HyperLink = Nothing
        Me.Label290.Left = 6.021!
        Me.Label290.Name = "Label290"
        Me.Label290.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label290.Text = "kips        min(Rn_BS1, Rn_BS2)"
        Me.Label290.Top = 31.70899!
        Me.Label290.Width = 1.478999!
        '
        'Label296
        '
        Me.Label296.Height = 0.1875!
        Me.Label296.HyperLink = Nothing
        Me.Label296.Left = 4.238999!
        Me.Label296.Name = "Label296"
        Me.Label296.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label296.Text = "DCR_BS="
        Me.Label296.Top = 32.079!
        Me.Label296.Width = 0.7819993!
        '
        'Label299
        '
        Me.Label299.Height = 0.1875!
        Me.Label299.HyperLink = Nothing
        Me.Label299.Left = 6.021999!
        Me.Label299.Name = "Label299"
        Me.Label299.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label299.Text = "kips"
        Me.Label299.Top = 31.304!
        Me.Label299.Width = 0.8749993!
        '
        'Label307
        '
        Me.Label307.Height = 0.1875!
        Me.Label307.HyperLink = Nothing
        Me.Label307.Left = 6.021999!
        Me.Label307.Name = "Label307"
        Me.Label307.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label307.Text = "kips"
        Me.Label307.Top = 31.896!
        Me.Label307.Width = 0.8749993!
        '
        'S214_026
        '
        Me.S214_026.Height = 0.1875!
        Me.S214_026.HyperLink = Nothing
        Me.S214_026.Left = 4.999999!
        Me.S214_026.Name = "S214_026"
        Me.S214_026.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S214_026.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S214_026.Top = 32.057!
        Me.S214_026.Width = 1.0!
        '
        'Shape15
        '
        Me.Shape15.Height = 0.1875!
        Me.Shape15.Left = 5.019999!
        Me.Shape15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape15.Name = "Shape15"
        Me.Shape15.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape15.Top = 32.242!
        Me.Shape15.Width = 1.0!
        '
        'Label308
        '
        Me.Label308.Height = 0.1875!
        Me.Label308.HyperLink = Nothing
        Me.Label308.Left = 0.07200028!
        Me.Label308.Name = "Label308"
        Me.Label308.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label308.Text = "DCR_NetSection="
        Me.Label308.Top = 43.09!
        Me.Label308.Width = 2.5!
        '
        'Label309
        '
        Me.Label309.Height = 0.1875!
        Me.Label309.HyperLink = Nothing
        Me.Label309.Left = 0.01!
        Me.Label309.Name = "Label309"
        Me.Label309.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label309.Text = "2.17 BEAM FLANGE NET SECTION CHECK"
        Me.Label309.Top = 41.588!
        Me.Label309.Width = 5.0!
        '
        'Label310
        '
        Me.Label310.Height = 0.1875!
        Me.Label310.HyperLink = Nothing
        Me.Label310.Left = 0.072!
        Me.Label310.Name = "Label310"
        Me.Label310.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label310.Text = "Zbx="
        Me.Label310.Top = 41.777!
        Me.Label310.Width = 2.5!
        '
        'S217_001
        '
        Me.S217_001.Height = 0.1875!
        Me.S217_001.HyperLink = Nothing
        Me.S217_001.Left = 2.572!
        Me.S217_001.Name = "S217_001"
        Me.S217_001.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S217_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_001.Top = 41.776!
        Me.S217_001.Width = 1.0!
        '
        'Label312
        '
        Me.Label312.Height = 0.1875!
        Me.Label312.HyperLink = Nothing
        Me.Label312.Left = 0.072!
        Me.Label312.Name = "Label312"
        Me.Label312.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label312.Text = "Zholes="
        Me.Label312.Top = 41.964!
        Me.Label312.Width = 2.5!
        '
        'S217_002
        '
        Me.S217_002.Height = 0.1875!
        Me.S217_002.HyperLink = Nothing
        Me.S217_002.Left = 2.572!
        Me.S217_002.Name = "S217_002"
        Me.S217_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S217_002.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_002.Top = 41.965!
        Me.S217_002.Width = 1.0!
        '
        'Label314
        '
        Me.Label314.Height = 0.1875!
        Me.Label314.HyperLink = Nothing
        Me.Label314.Left = 3.572!
        Me.Label314.Name = "Label314"
        Me.Label314.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label314.Text = "in^3             =2*Dholes*tbf*(d-tbf)"
        Me.Label314.Top = 41.964!
        Me.Label314.Width = 1.469!
        '
        'Label318
        '
        Me.Label318.Height = 0.1875!
        Me.Label318.HyperLink = Nothing
        Me.Label318.Left = 0.07200022!
        Me.Label318.Name = "Label318"
        Me.Label318.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label318.Text = "Znet="
        Me.Label318.Top = 42.151!
        Me.Label318.Width = 2.5!
        '
        'S217_003
        '
        Me.S217_003.Height = 0.1875!
        Me.S217_003.HyperLink = Nothing
        Me.S217_003.Left = 2.572!
        Me.S217_003.Name = "S217_003"
        Me.S217_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S217_003.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_003.Top = 42.151!
        Me.S217_003.Width = 1.0!
        '
        'Label324
        '
        Me.Label324.Height = 0.1875!
        Me.Label324.HyperLink = Nothing
        Me.Label324.Left = 3.572!
        Me.Label324.Name = "Label324"
        Me.Label324.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label324.Text = "in^3           =Zbx-Zholes " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label324.Top = 42.151!
        Me.Label324.Width = 1.469!
        '
        'Label327
        '
        Me.Label327.Height = 0.1875!
        Me.Label327.HyperLink = Nothing
        Me.Label327.Left = 0.072!
        Me.Label327.Name = "Label327"
        Me.Label327.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label327.Text = "Mpb_net="
        Me.Label327.Top = 42.339!
        Me.Label327.Width = 2.5!
        '
        'S217_004
        '
        Me.S217_004.Height = 0.1875!
        Me.S217_004.HyperLink = Nothing
        Me.S217_004.Left = 2.572!
        Me.S217_004.Name = "S217_004"
        Me.S217_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S217_004.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_004.Top = 42.339!
        Me.S217_004.Width = 1.0!
        '
        'Label332
        '
        Me.Label332.Height = 0.1875!
        Me.Label332.HyperLink = Nothing
        Me.Label332.Left = 3.572!
        Me.Label332.Name = "Label332"
        Me.Label332.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label332.Text = "k-in            =Znet*Fy*Ry"
        Me.Label332.Top = 42.339!
        Me.Label332.Width = 1.469!
        '
        'Label333
        '
        Me.Label333.Height = 0.1875!
        Me.Label333.HyperLink = Nothing
        Me.Label333.Left = 0.072!
        Me.Label333.Name = "Label333"
        Me.Label333.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label333.Text = "Pr_link="
        Me.Label333.Top = 42.527!
        Me.Label333.Width = 2.5!
        '
        'S217_005
        '
        Me.S217_005.Height = 0.1875!
        Me.S217_005.HyperLink = Nothing
        Me.S217_005.Left = 2.572!
        Me.S217_005.Name = "S217_005"
        Me.S217_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S217_005.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_005.Top = 42.527!
        Me.S217_005.Width = 1.0!
        '
        'Label336
        '
        Me.Label336.Height = 0.1875!
        Me.Label336.HyperLink = Nothing
        Me.Label336.Left = 3.572!
        Me.Label336.Name = "Label336"
        Me.Label336.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label336.Text = "in^3" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label336.Top = 41.778!
        Me.Label336.Width = 1.469!
        '
        'Label339
        '
        Me.Label339.Height = 0.1875!
        Me.Label339.HyperLink = Nothing
        Me.Label339.Left = 0.072!
        Me.Label339.Name = "Label339"
        Me.Label339.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label339.Text = "d+tstem="
        Me.Label339.Top = 42.714!
        Me.Label339.Width = 2.5!
        '
        'S217_006
        '
        Me.S217_006.Height = 0.1875!
        Me.S217_006.HyperLink = Nothing
        Me.S217_006.Left = 2.572!
        Me.S217_006.Name = "S217_006"
        Me.S217_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S217_006.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_006.Top = 42.714!
        Me.S217_006.Width = 1.0!
        '
        'Label345
        '
        Me.Label345.Height = 0.1875!
        Me.Label345.HyperLink = Nothing
        Me.Label345.Left = 3.572!
        Me.Label345.Name = "Label345"
        Me.Label345.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label345.Text = "in"
        Me.Label345.Top = 42.714!
        Me.Label345.Width = 1.469!
        '
        'Label348
        '
        Me.Label348.Height = 0.1875!
        Me.Label348.HyperLink = Nothing
        Me.Label348.Left = 0.0720007!
        Me.Label348.Name = "Label348"
        Me.Label348.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label348.Text = "Mpr_link="
        Me.Label348.Top = 42.903!
        Me.Label348.Width = 2.5!
        '
        'S217_007
        '
        Me.S217_007.Height = 0.1875!
        Me.S217_007.HyperLink = Nothing
        Me.S217_007.Left = 2.572!
        Me.S217_007.Name = "S217_007"
        Me.S217_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S217_007.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S217_007.Top = 42.903!
        Me.S217_007.Width = 1.0!
        '
        'Label351
        '
        Me.Label351.Height = 0.1875!
        Me.Label351.HyperLink = Nothing
        Me.Label351.Left = 3.572!
        Me.Label351.Name = "Label351"
        Me.Label351.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label351.Text = "k-in"
        Me.Label351.Top = 42.903!
        Me.Label351.Width = 1.469!
        '
        'Label367
        '
        Me.Label367.Height = 0.1875!
        Me.Label367.HyperLink = Nothing
        Me.Label367.Left = 3.561!
        Me.Label367.Name = "Label367"
        Me.Label367.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label367.Text = "kips"
        Me.Label367.Top = 42.527!
        Me.Label367.Width = 1.469!
        '
        'Label352
        '
        Me.Label352.Height = 0.1875!
        Me.Label352.HyperLink = Nothing
        Me.Label352.Left = 3.572!
        Me.Label352.Name = "Label352"
        Me.Label352.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label352.Text = "                =Mpr_link/Mpb_net"
        Me.Label352.Top = 43.088!
        Me.Label352.Width = 1.469!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.01!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(6.5!, 0.01!)
        Me.PageBreak1.Top = 43.406!
        Me.PageBreak1.Width = 6.5!
        '
        'Shape29
        '
        Me.Shape29.Height = 0.1875!
        Me.Shape29.Left = 2.583!
        Me.Shape29.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape29.Name = "Shape29"
        Me.Shape29.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape29.Top = 41.244!
        Me.Shape29.Width = 1.0!
        '
        'Shape10
        '
        Me.Shape10.Height = 0.1889992!
        Me.Shape10.Left = 2.572!
        Me.Shape10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape10.Name = "Shape10"
        Me.Shape10.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape10.Top = 43.088!
        Me.Shape10.Width = 1.012!
        '
        'Label311
        '
        Me.Label311.Height = 0.1875!
        Me.Label311.HyperLink = Nothing
        Me.Label311.Left = 2.4!
        Me.Label311.Name = "Label311"
        Me.Label311.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label311.Text = "R= "
        Me.Label311.Top = 0!
        Me.Label311.Width = 0.937!
        '
        'S21_009
        '
        Me.S21_009.Height = 0.1875!
        Me.S21_009.HyperLink = Nothing
        Me.S21_009.Left = 3.328!
        Me.S21_009.Name = "S21_009"
        Me.S21_009.Style = "color: Blue; font-size: 8.25pt; font-style: normal; text-align: center; vertical-" &
    "align: middle; ddo-char-set: 1"
        Me.S21_009.Text = "Non-Compact"
        Me.S21_009.Top = 0!
        Me.S21_009.Width = 1.009!
        '
        'US_Detail_BLC
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.979167!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S217_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_032, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_025, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_033, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_018, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_028, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_020, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_021, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_022, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_023, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_017, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label301, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label302, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.r21, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS21, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS22, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S22_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS23, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S23_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS25, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S25_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S25_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S25_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S25_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S25_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS26, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S26_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S26_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S26_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S26_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS27, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label152, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label155, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label157, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label161, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS24, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S24_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S24_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label168, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label169, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S24_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label171, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label172, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label174, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label175, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label177, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label178, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S28_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label180, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS29, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label182, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label184, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label185, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label187, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label188, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label190, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label194, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label197, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label200, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label202, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label203, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S29_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label205, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS210, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label208, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label210, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label211, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label213, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label126, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label129, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label132, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label138, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label139, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label142, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label145, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label147, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label150, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_017, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label165, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label166, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_018, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label215, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label216, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_019, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label218, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label219, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_020, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label221, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label222, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_025, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label224, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label226, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label227, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_021, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label229, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label230, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_022, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label232, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label233, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_023, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label235, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label236, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_024, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label238, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label239, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_026, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label241, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label242, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_027, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label244, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label246, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label247, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_029, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label249, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label250, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label251, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_030, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label253, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label254, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label255, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_031, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label257, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label258, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_032, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label260, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS211, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label262, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label264, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label265, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label267, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label268, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label270, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label274, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_019, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_024, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label277, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_025, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label280, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label283, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label286, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label289, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label291, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label292, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label294, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label282, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label288, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label295, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label297, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label298, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S211_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label300, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label303, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label304, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label305, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label306, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label315, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label317, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label319, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS28, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label115, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S27_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_033, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_034, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_035, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label137, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label140, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S210_036, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label134, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Picture1, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S212_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S212_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label143, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label149, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S212_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label153, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S212_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label159, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S212_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label162, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S212_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label167, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_018, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label176, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label181, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label183, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label186, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label189, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label192, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label195, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label198, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label201, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label204, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label206, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label207, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label209, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label214, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label217, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label223, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label231, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label245, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label259, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label261, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label263, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label266, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label275, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label237, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label240, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label284, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label285, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label287, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S213_017, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label293, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_023, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_024, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS214, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label316, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label320, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label322, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label323, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label325, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label326, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label328, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label329, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label331, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label334, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label337, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label338, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label340, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label341, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label343, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label344, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label346, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label347, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label350, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label353, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label355, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label356, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label358, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label359, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label361, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label362, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label365, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label368, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_017, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label370, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label371, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label373, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label375, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_019, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_020, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_021, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label385, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label387, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label388, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label390, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_018, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_022, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS215, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label392, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label394, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label396, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label397, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label399, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label400, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label402, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label403, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label405, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label412, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label414, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label415, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label417, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label418, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label420, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label421, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label423, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label424, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label426, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label427, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label429, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label430, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label432, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label433, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label435, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label436, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label439, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label442, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label444, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S215_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label378, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_029, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_030, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_031, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label406, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label408, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label409, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label411, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label438, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label445, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label446, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label448, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.headerS216, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label450, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label452, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label454, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label455, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label457, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label458, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label460, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label461, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label463, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label464, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label467, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label470, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label472, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label473, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label475, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label476, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label478, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label479, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label481, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label482, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label485, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label487, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label488, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label490, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label491, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label493, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label494, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label497, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label500, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label502, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label503, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label505, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label507, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label517, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_019, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label519, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label520, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_020, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label522, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label508, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_021, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label510, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label511, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_022, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label513, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label514, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_023, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label516, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label523, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_024, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label525, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label526, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_025, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label528, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label529, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_026, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label531, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label532, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_027, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label534, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_017, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_018, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_028, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label228, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label212, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label220, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_008, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_010, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_011, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_012, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label225, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_013, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_014, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_015, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_016, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label234, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label243, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_017, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_018, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.SUM_019, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label170, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label173, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S216_034, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label252, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label256, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label248, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label269, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label272, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label276, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label278, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label281, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label290, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label296, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label299, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label307, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S214_026, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label308, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label309, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label310, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_001, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label312, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_002, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label314, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label318, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_003, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label324, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label327, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_004, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label332, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label333, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_005, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label336, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label339, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_006, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label345, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label348, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S217_007, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label351, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label367, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label352, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.Label311, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.S21_009, System.ComponentModel.ISupportInitialize).EndInit
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit

    End Sub
    Private WithEvents r21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S22_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label50 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label55 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label62 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label67 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label68 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S23_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label71 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label72 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S25_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S25_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label77 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label78 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S25_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label80 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S25_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S25_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label86 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S26_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label88 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S26_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label90 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S26_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S26_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label94 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label96 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label99 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label101 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label102 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label104 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label105 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label108 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label110 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label111 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label113 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label114 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label116 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label117 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label119 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label120 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label122 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label152 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label154 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label155 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label157 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label158 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label160 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label161 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label163 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S24_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S24_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label168 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label169 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S24_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label171 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label172 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label174 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label175 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label177 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label178 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S28_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label180 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label182 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label184 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label185 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label187 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label188 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label190 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label191 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label193 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label194 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label196 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label197 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label199 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label200 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label202 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label203 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label205 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S29_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS210 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label208 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label210 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label211 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label213 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label123 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label124 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label126 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label127 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label129 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label130 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label132 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label133 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label135 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label136 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label138 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label139 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label142 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label144 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label145 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label147 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label148 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label150 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label151 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label165 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label166 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label215 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label216 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label218 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label219 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label221 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label222 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label224 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label141 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label226 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label227 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label229 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label230 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label232 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label233 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label235 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label236 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label238 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label239 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label241 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label242 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label244 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label246 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label247 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label249 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label250 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label251 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label253 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label254 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label255 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label257 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label258 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label260 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS211 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label262 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label264 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label265 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label267 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label268 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label270 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label274 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label271 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label273 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label277 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label279 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label280 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label283 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label286 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label289 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label291 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label292 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label294 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label282 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label288 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label295 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label297 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label298 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label300 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label301 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label302 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label303 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label304 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label305 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label306 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S211_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label315 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label317 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents headerS28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label53 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label61 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label69 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label70 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label75 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label84 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label89 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label91 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label95 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label97 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label103 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label106 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label109 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label115 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label319 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S211_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape4 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S27_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label93 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label112 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label118 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label121 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label128 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label131 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_035 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label137 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label140 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S210_036 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label146 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label100 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label134 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label125 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture1 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents S212_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S212_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label143 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label149 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S212_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label153 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label156 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S212_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label159 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S212_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label162 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label164 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S212_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label167 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape7 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S213_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label176 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label179 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label181 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label183 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label186 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label189 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label192 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label195 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label198 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label201 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label204 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label206 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label207 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label209 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label214 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label217 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label223 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label231 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label245 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label259 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label261 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label263 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label266 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label275 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label237 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label240 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label284 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label285 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label287 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S213_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label293 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape8 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape9 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape11 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape12 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S214_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS214 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label316 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label320 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label322 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label323 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label325 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label326 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label328 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label329 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label331 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label334 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label337 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label338 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label340 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label341 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label343 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label344 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label346 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label347 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label350 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label353 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label355 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label356 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label358 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label359 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label361 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label362 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label365 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label368 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label370 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label371 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label373 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label375 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label385 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label387 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label388 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label390 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape17 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape19 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape15 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S215_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS215 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label392 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label394 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label396 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label397 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label399 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label400 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label402 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label403 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label405 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label412 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label414 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label415 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label417 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label418 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label420 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label421 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label423 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label424 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label426 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label427 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label429 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label430 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label432 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label433 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label435 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label436 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label439 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label442 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label444 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S215_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label378 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape23 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape25 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S216_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label406 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label408 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label409 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label411 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label438 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label445 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label446 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label448 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS216 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label450 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label452 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label454 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label455 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label457 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label458 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label460 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label461 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label463 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label464 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label467 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label470 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label472 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label473 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label475 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label476 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label478 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label479 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label481 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label482 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label485 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label487 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label488 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label490 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label491 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label493 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label494 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label497 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label500 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label502 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label503 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label505 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label507 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label517 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label519 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label520 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label522 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label508 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label510 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label511 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label513 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label514 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label516 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label523 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label525 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label526 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label528 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label529 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label531 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label532 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape20 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape21 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape31 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape29 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape27 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label228 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label212 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label44 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label220 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label51 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label73 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label87 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label225 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line21 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line23 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line24 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line26 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents SUM_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label234 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label243 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line20 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line19 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line18 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line27 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label170 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label173 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S216_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label252 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label256 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label534 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label248 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label269 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label272 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label276 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label278 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label281 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label290 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label296 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label299 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label307 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S214_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label308 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label309 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label310 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label312 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label314 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label318 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label324 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label327 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label332 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label333 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label336 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label339 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label345 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label348 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label351 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S217_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label367 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label352 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents Shape10 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label311 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S21_009 As GrapeCity.ActiveReports.SectionReportModel.Label
End Class
