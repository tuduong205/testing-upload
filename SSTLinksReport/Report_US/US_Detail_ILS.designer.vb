﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_Detail_ILS
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_Detail_ILS))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.SUM_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label212 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label214 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S18_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S12_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label50 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label55 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label62 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label67 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label68 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S13_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label71 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label72 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S15_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S15_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label77 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label78 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S15_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label80 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S15_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S15_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label86 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S16_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label88 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S16_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label90 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S16_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S16_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label94 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label96 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label99 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label101 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label102 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label104 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label105 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label108 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label110 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label111 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label113 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label114 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label116 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label117 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S17_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label119 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label120 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label122 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label124 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S18_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label126 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label127 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S18_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label129 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label131 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label133 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label135 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label137 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label139 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label141 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label143 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label144 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS110 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label146 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label148 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label149 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label151 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label152 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label154 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label155 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label157 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label158 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label160 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label161 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S110_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label163 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label164 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label166 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.headerS14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S14_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S14_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S14_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S11_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape7 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape8 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label53 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line48 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line47 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line46 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label61 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label69 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label91 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label95 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label106 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label112 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label123 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label128 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label136 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label140 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label156 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label167 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label173 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label177 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label179 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label184 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label185 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S1S111_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label190 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label191 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label196 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label202 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label203 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label208 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label209 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label210 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line20 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line49 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S2S111_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S2S111_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label199 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label213 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label215 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label218 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label219 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label220 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label221 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label224 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label226 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label230 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label232 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label238 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label225 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SUM_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line21 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line24 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line25 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line26 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line27 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line18 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line19 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label44 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label51 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label70 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label75 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label87 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label89 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label97 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label100 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label103 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label109 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label118 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label121 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label125 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label130 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S19_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape4 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape9 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape11 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape10 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.Line23 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        CType(Me.SUM_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label212, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label214, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S18_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S12_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S13_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S15_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S15_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S15_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S15_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S15_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S16_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S16_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S16_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S16_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S17_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S18_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label126, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S18_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label129, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label137, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label139, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label143, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS110, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label149, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label152, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label155, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label157, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label161, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S110_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label166, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.headerS14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S14_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S14_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S14_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S11_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label140, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label167, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label173, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label177, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label184, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label185, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S1S111_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label190, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label202, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label203, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label208, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label209, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label210, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S2S111_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label213, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label215, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label218, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label219, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label220, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label221, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label224, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label226, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label230, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label232, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label238, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label225, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SUM_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S19_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.SUM_013, Me.SUM_012, Me.SUM_003, Me.SUM_007, Me.SUM_002, Me.Label212, Me.Label214, Me.S2S111_017, Me.S1S111_017, Me.S110_007, Me.S19_013, Me.S19_012, Me.S18_003, Me.S17_009, Me.S13_004, Me.S1S111_009, Me.S1S111_008, Me.S1S111_011, Me.S1S111_010, Me.S1S111_007, Me.S1S111_006, Me.S1S111_005, Me.S1S111_004, Me.S1S111_003, Me.S1S111_026, Me.S1S111_016, Me.S1S111_015, Me.S1S111_014, Me.S1S111_013, Me.S1S111_012, Me.SUM_004, Me.S2S111_018, Me.S2S111_019, Me.S2S111_003, Me.S2S111_002, Me.S1S111_019, Me.S1S111_018, Me.S1S111_002, Me.S2S111_001, Me.S1S111_001, Me.S11_008, Me.Label1, Me.S11_001, Me.Label3, Me.headerS11, Me.Label5, Me.Label6, Me.S11_004, Me.Label8, Me.S11_005, Me.Label10, Me.S11_007, Me.Label12, Me.Label14, Me.S11_002, Me.Label16, Me.S11_003, Me.headerS12, Me.S12_001, Me.Label21, Me.S12_006, Me.Label23, Me.Label24, Me.Label25, Me.S12_002, Me.Label27, Me.S12_007, Me.Label29, Me.Label30, Me.Label31, Me.S12_003, Me.Label33, Me.S12_008, Me.Label35, Me.Label36, Me.Label37, Me.S12_004, Me.Label39, Me.S12_009, Me.Label41, Me.Label42, Me.Label43, Me.S12_005, Me.Label45, Me.S12_010, Me.Label47, Me.headerS13, Me.Label49, Me.Label50, Me.S13_001, Me.Label52, Me.S13_005, Me.Label55, Me.Label56, Me.S13_002, Me.Label58, Me.S13_006, Me.Label19, Me.Label54, Me.S13_003, Me.Label62, Me.Label63, Me.S13_007, Me.Label59, Me.Label65, Me.Label67, Me.Label68, Me.S13_008, Me.headerS15, Me.Label71, Me.Label72, Me.S15_001, Me.Label74, Me.S15_004, Me.Label77, Me.Label78, Me.S15_002, Me.Label80, Me.S15_005, Me.Label83, Me.S15_003, Me.Label85, Me.headerS16, Me.Label86, Me.S16_001, Me.Label88, Me.S16_003, Me.Label90, Me.Label81, Me.S16_002, Me.Label92, Me.S16_004, Me.Label94, Me.headerS17, Me.Label96, Me.S17_001, Me.Label98, Me.Label99, Me.S17_002, Me.Label101, Me.Label102, Me.S17_003, Me.Label104, Me.Label105, Me.S17_004, Me.Label107, Me.Label108, Me.S17_005, Me.Label110, Me.Label111, Me.S17_006, Me.Label113, Me.Label114, Me.S17_007, Me.Label116, Me.Label117, Me.S17_008, Me.Label119, Me.Label120, Me.Label122, Me.headerS18, Me.Label124, Me.S18_001, Me.Label126, Me.Label127, Me.S18_002, Me.Label129, Me.headerS19, Me.Label131, Me.S19_001, Me.Label133, Me.S19_002, Me.Label135, Me.S19_003, Me.Label137, Me.S19_004, Me.Label139, Me.S19_010, Me.Label141, Me.Label143, Me.Label144, Me.headerS110, Me.Label146, Me.S110_001, Me.Label148, Me.Label149, Me.S110_002, Me.Label151, Me.Label152, Me.S110_003, Me.Label154, Me.Label155, Me.S110_004, Me.Label157, Me.Label158, Me.S110_005, Me.Label160, Me.Label161, Me.S110_006, Me.Label163, Me.Label164, Me.Label166, Me.headerS14, Me.Label7, Me.S14_001, Me.Label11, Me.Label13, Me.S14_002, Me.Label17, Me.Label20, Me.S14_003, Me.Label26, Me.Shape6, Me.Label2, Me.S11_006, Me.Label9, Me.S11_009, Me.Shape7, Me.Shape8, Me.Label4, Me.Label18, Me.Label15, Me.Label22, Me.Label34, Me.Label38, Me.Label40, Me.Label46, Me.Label53, Me.Line48, Me.Line47, Me.Line46, Me.Line4, Me.Label61, Me.Label64, Me.Label69, Me.S1S111_020, Me.S2S111_004, Me.S2S111_020, Me.Label76, Me.Label82, Me.S1S111_021, Me.S2S111_005, Me.S2S111_021, Me.Label91, Me.Label95, Me.S1S111_022, Me.S2S111_006, Me.S2S111_022, Me.Label106, Me.Label112, Me.S1S111_023, Me.S2S111_007, Me.S2S111_023, Me.Label123, Me.Label128, Me.S1S111_024, Me.S2S111_008, Me.S2S111_024, Me.Label136, Me.Label140, Me.S1S111_025, Me.S2S111_009, Me.S2S111_025, Me.Label156, Me.S2S111_010, Me.S2S111_026, Me.Label167, Me.Label173, Me.S1S111_027, Me.S2S111_011, Me.S2S111_027, Me.Label177, Me.Label179, Me.S1S111_028, Me.S2S111_012, Me.S2S111_028, Me.Label184, Me.Label185, Me.S1S111_029, Me.S2S111_013, Me.S2S111_029, Me.Label190, Me.Label191, Me.Label196, Me.Label202, Me.Label203, Me.Label208, Me.Label209, Me.Label210, Me.Line20, Me.Line49, Me.S2S111_016, Me.S2S111_015, Me.S2S111_014, Me.Label199, Me.SUM_006, Me.SUM_001, Me.Label213, Me.Label215, Me.Label218, Me.Label219, Me.Label220, Me.Label221, Me.Label224, Me.Label226, Me.SUM_008, Me.Label230, Me.Label232, Me.SUM_009, Me.Label238, Me.SUM_005, Me.SUM_010, Me.SUM_011, Me.Label225, Me.SUM_014, Me.SUM_015, Me.Line30, Me.Line2, Me.Line21, Me.Line24, Me.Line25, Me.Line26, Me.Line27, Me.Line28, Me.Line29, Me.Line31, Me.Line32, Me.Line33, Me.Line17, Me.Line18, Me.Line19, Me.Line1, Me.Line13, Me.Line14, Me.Line3, Me.Line16, Me.Line15, Me.Line11, Me.Line5, Me.Line6, Me.Line7, Me.Line8, Me.Line9, Me.Line12, Me.Line10, Me.Label28, Me.S19_005, Me.Label44, Me.S19_006, Me.Label51, Me.Label57, Me.S19_007, Me.Label66, Me.Label70, Me.S19_008, Me.Label75, Me.Label79, Me.S19_009, Me.Label87, Me.Label89, Me.Label97, Me.Label100, Me.Label103, Me.Label109, Me.S19_011, Me.Label118, Me.Label121, Me.Label125, Me.Label130, Me.Label32, Me.S19_014, Me.Label60, Me.Shape5, Me.Shape4, Me.Shape9, Me.Shape11, Me.Shape1, Me.Shape3, Me.Shape10, Me.Shape2, Me.PageBreak1, Me.Line23})
        Me.Detail.Height = 17.447!
        Me.Detail.Name = "Detail"
        '
        'SUM_013
        '
        Me.SUM_013.Height = 0.188!
        Me.SUM_013.HyperLink = Nothing
        Me.SUM_013.Left = 5.624!
        Me.SUM_013.Name = "SUM_013"
        Me.SUM_013.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_013.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_013.Top = 16.83!
        Me.SUM_013.Width = 0.75!
        '
        'SUM_012
        '
        Me.SUM_012.Height = 0.188!
        Me.SUM_012.HyperLink = Nothing
        Me.SUM_012.Left = 5.624!
        Me.SUM_012.Name = "SUM_012"
        Me.SUM_012.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_012.Top = 16.639!
        Me.SUM_012.Width = 0.75!
        '
        'SUM_003
        '
        Me.SUM_003.Height = 0.188!
        Me.SUM_003.HyperLink = Nothing
        Me.SUM_003.Left = 4.125!
        Me.SUM_003.Name = "SUM_003"
        Me.SUM_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_003.Top = 16.833!
        Me.SUM_003.Width = 0.75!
        '
        'SUM_007
        '
        Me.SUM_007.Height = 0.188!
        Me.SUM_007.HyperLink = Nothing
        Me.SUM_007.Left = 4.875!
        Me.SUM_007.Name = "SUM_007"
        Me.SUM_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_007.Top = 16.642!
        Me.SUM_007.Width = 0.75!
        '
        'SUM_002
        '
        Me.SUM_002.Height = 0.188!
        Me.SUM_002.HyperLink = Nothing
        Me.SUM_002.Left = 4.125!
        Me.SUM_002.Name = "SUM_002"
        Me.SUM_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_002.Top = 16.642!
        Me.SUM_002.Width = 0.75!
        '
        'Label212
        '
        Me.Label212.Height = 0.188!
        Me.Label212.HyperLink = Nothing
        Me.Label212.Left = 3.0!
        Me.Label212.Name = "Label212"
        Me.Label212.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label212.Text = "Initial bf Check="
        Me.Label212.Top = 16.642!
        Me.Label212.Width = 1.125!
        '
        'Label214
        '
        Me.Label214.Height = 0.188!
        Me.Label214.HyperLink = Nothing
        Me.Label214.Left = 2.0!
        Me.Label214.Name = "Label214"
        Me.Label214.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label214.Text = "Beam/Column"
        Me.Label214.Top = 16.643!
        Me.Label214.Width = 0.9999999!
        '
        'S2S111_017
        '
        Me.S2S111_017.Height = 0.1875!
        Me.S2S111_017.HyperLink = Nothing
        Me.S2S111_017.Left = 3.5!
        Me.S2S111_017.Name = "S2S111_017"
        Me.S2S111_017.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S2S111_017.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_017.Top = 15.893!
        Me.S2S111_017.Width = 1.5!
        '
        'S1S111_017
        '
        Me.S1S111_017.Height = 0.1875!
        Me.S1S111_017.HyperLink = Nothing
        Me.S1S111_017.Left = 2.0!
        Me.S1S111_017.Name = "S1S111_017"
        Me.S1S111_017.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S1S111_017.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_017.Top = 15.894!
        Me.S1S111_017.Width = 1.5!
        '
        'S110_007
        '
        Me.S110_007.Height = 0.1875!
        Me.S110_007.HyperLink = Nothing
        Me.S110_007.Left = 2.5!
        Me.S110_007.Name = "S110_007"
        Me.S110_007.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S110_007.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_007.Top = 12.229!
        Me.S110_007.Width = 1.0!
        '
        'S19_013
        '
        Me.S19_013.Height = 0.1875!
        Me.S19_013.HyperLink = Nothing
        Me.S19_013.Left = 2.5!
        Me.S19_013.Name = "S19_013"
        Me.S19_013.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S19_013.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_013.Top = 10.593!
        Me.S19_013.Width = 1.0!
        '
        'S19_012
        '
        Me.S19_012.Height = 0.1875!
        Me.S19_012.HyperLink = Nothing
        Me.S19_012.Left = 2.5!
        Me.S19_012.Name = "S19_012"
        Me.S19_012.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S19_012.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_012.Top = 10.406!
        Me.S19_012.Width = 1.0!
        '
        'S18_003
        '
        Me.S18_003.Height = 0.1875!
        Me.S18_003.HyperLink = Nothing
        Me.S18_003.Left = 2.51!
        Me.S18_003.Name = "S18_003"
        Me.S18_003.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S18_003.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S18_003.Top = 7.759999!
        Me.S18_003.Width = 1.0!
        '
        'S17_009
        '
        Me.S17_009.Height = 0.1875!
        Me.S17_009.HyperLink = Nothing
        Me.S17_009.Left = 2.5!
        Me.S17_009.Name = "S17_009"
        Me.S17_009.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: normal; te" &
    "xt-align: center; vertical-align: middle; ddo-char-set: 1"
        Me.S17_009.Text = "974045" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_009.Top = 6.938!
        Me.S17_009.Width = 1.0!
        '
        'S13_004
        '
        Me.S13_004.Height = 0.1875!
        Me.S13_004.HyperLink = Nothing
        Me.S13_004.Left = 2.5!
        Me.S13_004.Name = "S13_004"
        Me.S13_004.Style = "background-color: Gainsboro; color: Blue; font-size: 8.25pt; font-weight: normal;" &
    " text-align: center; vertical-align: middle; ddo-char-set: 1"
        Me.S13_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_004.Top = 2.748!
        Me.S13_004.Width = 1.0!
        '
        'S1S111_009
        '
        Me.S1S111_009.Height = 0.188!
        Me.S1S111_009.HyperLink = Nothing
        Me.S1S111_009.Left = 2.0!
        Me.S1S111_009.Name = "S1S111_009"
        Me.S1S111_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_009.Top = 14.378!
        Me.S1S111_009.Width = 0.75!
        '
        'S1S111_008
        '
        Me.S1S111_008.Height = 0.188!
        Me.S1S111_008.HyperLink = Nothing
        Me.S1S111_008.Left = 2.0!
        Me.S1S111_008.Name = "S1S111_008"
        Me.S1S111_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S1S111_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_008.Top = 14.189!
        Me.S1S111_008.Width = 0.75!
        '
        'S1S111_011
        '
        Me.S1S111_011.Height = 0.188!
        Me.S1S111_011.HyperLink = Nothing
        Me.S1S111_011.Left = 2.0!
        Me.S1S111_011.Name = "S1S111_011"
        Me.S1S111_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S1S111_011.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_011.Top = 14.759!
        Me.S1S111_011.Width = 0.75!
        '
        'S1S111_010
        '
        Me.S1S111_010.Height = 0.188!
        Me.S1S111_010.HyperLink = Nothing
        Me.S1S111_010.Left = 2.0!
        Me.S1S111_010.Name = "S1S111_010"
        Me.S1S111_010.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S1S111_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_010.Top = 14.568!
        Me.S1S111_010.Width = 0.75!
        '
        'S1S111_007
        '
        Me.S1S111_007.Height = 0.188!
        Me.S1S111_007.HyperLink = Nothing
        Me.S1S111_007.Left = 2.0!
        Me.S1S111_007.Name = "S1S111_007"
        Me.S1S111_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_007.Top = 13.999!
        Me.S1S111_007.Width = 0.75!
        '
        'S1S111_006
        '
        Me.S1S111_006.Height = 0.188!
        Me.S1S111_006.HyperLink = Nothing
        Me.S1S111_006.Left = 2.0!
        Me.S1S111_006.Name = "S1S111_006"
        Me.S1S111_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S1S111_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_006.Top = 13.809!
        Me.S1S111_006.Width = 0.75!
        '
        'S1S111_005
        '
        Me.S1S111_005.Height = 0.188!
        Me.S1S111_005.HyperLink = Nothing
        Me.S1S111_005.Left = 2.0!
        Me.S1S111_005.Name = "S1S111_005"
        Me.S1S111_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S1S111_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_005.Top = 13.618!
        Me.S1S111_005.Width = 0.75!
        '
        'S1S111_004
        '
        Me.S1S111_004.Height = 0.188!
        Me.S1S111_004.HyperLink = Nothing
        Me.S1S111_004.Left = 2.0!
        Me.S1S111_004.Name = "S1S111_004"
        Me.S1S111_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S1S111_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_004.Top = 13.428!
        Me.S1S111_004.Width = 0.75!
        '
        'S1S111_003
        '
        Me.S1S111_003.Height = 0.188!
        Me.S1S111_003.HyperLink = Nothing
        Me.S1S111_003.Left = 2.0!
        Me.S1S111_003.Name = "S1S111_003"
        Me.S1S111_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S1S111_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_003.Top = 13.237!
        Me.S1S111_003.Width = 0.75!
        '
        'S1S111_026
        '
        Me.S1S111_026.Height = 0.188!
        Me.S1S111_026.HyperLink = Nothing
        Me.S1S111_026.Left = 2.75!
        Me.S1S111_026.Name = "S1S111_026"
        Me.S1S111_026.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S1S111_026.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_026.Top = 14.567!
        Me.S1S111_026.Width = 0.75!
        '
        'S1S111_016
        '
        Me.S1S111_016.Height = 0.188!
        Me.S1S111_016.HyperLink = Nothing
        Me.S1S111_016.Left = 2.0!
        Me.S1S111_016.Name = "S1S111_016"
        Me.S1S111_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_016.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_016.Top = 15.705!
        Me.S1S111_016.Width = 1.5!
        '
        'S1S111_015
        '
        Me.S1S111_015.Height = 0.188!
        Me.S1S111_015.HyperLink = Nothing
        Me.S1S111_015.Left = 2.0!
        Me.S1S111_015.Name = "S1S111_015"
        Me.S1S111_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_015.Top = 15.516!
        Me.S1S111_015.Width = 1.5!
        '
        'S1S111_014
        '
        Me.S1S111_014.Height = 0.188!
        Me.S1S111_014.HyperLink = Nothing
        Me.S1S111_014.Left = 2.0!
        Me.S1S111_014.Name = "S1S111_014"
        Me.S1S111_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_014.Top = 15.328!
        Me.S1S111_014.Width = 1.5!
        '
        'S1S111_013
        '
        Me.S1S111_013.Height = 0.188!
        Me.S1S111_013.HyperLink = Nothing
        Me.S1S111_013.Left = 2.0!
        Me.S1S111_013.Name = "S1S111_013"
        Me.S1S111_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_013.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_013.Top = 15.139!
        Me.S1S111_013.Width = 0.75!
        '
        'S1S111_012
        '
        Me.S1S111_012.Height = 0.188!
        Me.S1S111_012.HyperLink = Nothing
        Me.S1S111_012.Left = 2.0!
        Me.S1S111_012.Name = "S1S111_012"
        Me.S1S111_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_012.Top = 14.95!
        Me.S1S111_012.Width = 0.75!
        '
        'SUM_004
        '
        Me.SUM_004.Height = 0.188!
        Me.SUM_004.HyperLink = Nothing
        Me.SUM_004.Left = 4.125!
        Me.SUM_004.Name = "SUM_004"
        Me.SUM_004.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_004.Top = 17.023!
        Me.SUM_004.Width = 0.75!
        '
        'S2S111_018
        '
        Me.S2S111_018.Height = 0.188!
        Me.S2S111_018.HyperLink = Nothing
        Me.S2S111_018.Left = 4.25!
        Me.S2S111_018.Name = "S2S111_018"
        Me.S2S111_018.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S2S111_018.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_018.Top = 13.051!
        Me.S2S111_018.Width = 0.75!
        '
        'S2S111_019
        '
        Me.S2S111_019.Height = 0.188!
        Me.S2S111_019.HyperLink = Nothing
        Me.S2S111_019.Left = 4.25!
        Me.S2S111_019.Name = "S2S111_019"
        Me.S2S111_019.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S2S111_019.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_019.Top = 13.236!
        Me.S2S111_019.Width = 0.75!
        '
        'S2S111_003
        '
        Me.S2S111_003.Height = 0.188!
        Me.S2S111_003.HyperLink = Nothing
        Me.S2S111_003.Left = 3.5!
        Me.S2S111_003.Name = "S2S111_003"
        Me.S2S111_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S2S111_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_003.Top = 13.236!
        Me.S2S111_003.Width = 0.75!
        '
        'S2S111_002
        '
        Me.S2S111_002.Height = 0.188!
        Me.S2S111_002.HyperLink = Nothing
        Me.S2S111_002.Left = 3.5!
        Me.S2S111_002.Name = "S2S111_002"
        Me.S2S111_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S2S111_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_002.Top = 13.051!
        Me.S2S111_002.Width = 0.75!
        '
        'S1S111_019
        '
        Me.S1S111_019.Height = 0.188!
        Me.S1S111_019.HyperLink = Nothing
        Me.S1S111_019.Left = 2.75!
        Me.S1S111_019.Name = "S1S111_019"
        Me.S1S111_019.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S1S111_019.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_019.Top = 13.236!
        Me.S1S111_019.Width = 0.75!
        '
        'S1S111_018
        '
        Me.S1S111_018.Height = 0.188!
        Me.S1S111_018.HyperLink = Nothing
        Me.S1S111_018.Left = 2.75!
        Me.S1S111_018.Name = "S1S111_018"
        Me.S1S111_018.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S1S111_018.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_018.Top = 13.051!
        Me.S1S111_018.Width = 0.75!
        '
        'S1S111_002
        '
        Me.S1S111_002.Height = 0.188!
        Me.S1S111_002.HyperLink = Nothing
        Me.S1S111_002.Left = 2.0!
        Me.S1S111_002.Name = "S1S111_002"
        Me.S1S111_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S1S111_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_002.Top = 13.05!
        Me.S1S111_002.Width = 0.75!
        '
        'S2S111_001
        '
        Me.S2S111_001.Height = 0.188!
        Me.S2S111_001.HyperLink = Nothing
        Me.S2S111_001.Left = 3.5!
        Me.S2S111_001.Name = "S2S111_001"
        Me.S2S111_001.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.S2S111_001.Text = "25"
        Me.S2S111_001.Top = 12.675!
        Me.S2S111_001.Width = 1.5!
        '
        'S1S111_001
        '
        Me.S1S111_001.Height = 0.188!
        Me.S1S111_001.HyperLink = Nothing
        Me.S1S111_001.Left = 2.0!
        Me.S1S111_001.Name = "S1S111_001"
        Me.S1S111_001.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.S1S111_001.Text = "21"
        Me.S1S111_001.Top = 12.675!
        Me.S1S111_001.Width = 1.5!
        '
        'S11_008
        '
        Me.S11_008.Height = 0.1875!
        Me.S11_008.HyperLink = Nothing
        Me.S11_008.Left = 6.314!
        Me.S11_008.Name = "S11_008"
        Me.S11_008.Style = "background-color: Gainsboro; color: DarkGreen; font-size: 8.25pt; font-weight: no" &
    "rmal; text-align: center; vertical-align: middle; ddo-char-set: 1"
        Me.S11_008.Tag = "1"
        Me.S11_008.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_008.Top = 0.374!
        Me.S11_008.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.188!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.125!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Beam Unique Name:"
        Me.Label1.Top = 0.187!
        Me.Label1.Width = 1.25!
        '
        'S11_001
        '
        Me.S11_001.Height = 0.1875!
        Me.S11_001.HyperLink = Nothing
        Me.S11_001.Left = 1.375!
        Me.S11_001.Name = "S11_001"
        Me.S11_001.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.S11_001.Tag = "1"
        Me.S11_001.Text = "299"
        Me.S11_001.Top = 0.187!
        Me.S11_001.Width = 1.0!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 7.312!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label3.Text = "in"
        Me.Label3.Top = 1.0!
        Me.Label3.Width = 0.25!
        '
        'headerS11
        '
        Me.headerS11.Height = 0.1875!
        Me.headerS11.HyperLink = Nothing
        Me.headerS11.Left = 0!
        Me.headerS11.Name = "headerS11"
        Me.headerS11.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS11.Text = "1.1 CURRENT MEMBER:"
        Me.headerS11.Top = 0!
        Me.headerS11.Width = 3.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "NY Length ColSide (Lcol_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 1.0!
        Me.Label5.Width = 2.5!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 2.438!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label6.Text = "I_End Column Size:"
        Me.Label6.Top = 0.187!
        Me.Label6.Width = 1.375!
        '
        'S11_004
        '
        Me.S11_004.Height = 0.1875!
        Me.S11_004.HyperLink = Nothing
        Me.S11_004.Left = 3.813!
        Me.S11_004.Name = "S11_004"
        Me.S11_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S11_004.Tag = "1"
        Me.S11_004.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_004.Top = 0.187!
        Me.S11_004.Width = 1.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 2.438!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label8.Text = "I_End Col. Unique Name:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 0.374!
        Me.Label8.Width = 1.375!
        '
        'S11_005
        '
        Me.S11_005.Height = 0.1875!
        Me.S11_005.HyperLink = Nothing
        Me.S11_005.Left = 3.813!
        Me.S11_005.Name = "S11_005"
        Me.S11_005.Style = "background-color: Gainsboro; color: DarkGreen; font-size: 8.25pt; font-weight: no" &
    "rmal; text-align: center; vertical-align: middle; ddo-char-set: 1"
        Me.S11_005.Tag = "1"
        Me.S11_005.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_005.Top = 0.374!
        Me.S11_005.Width = 1.0!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 4.939!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label10.Text = "J_End Column Size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 0.187!
        Me.Label10.Width = 1.375!
        '
        'S11_007
        '
        Me.S11_007.Height = 0.1875!
        Me.S11_007.HyperLink = Nothing
        Me.S11_007.Left = 6.314!
        Me.S11_007.Name = "S11_007"
        Me.S11_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S11_007.Tag = "1"
        Me.S11_007.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_007.Top = 0.187!
        Me.S11_007.Width = 1.0!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 4.939!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "J_End Col. Unique Name:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label12.Top = 0.374!
        Me.Label12.Width = 1.375!
        '
        'Label14
        '
        Me.Label14.Height = 0.188!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.125!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label14.Text = "Beam Size:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 0.374!
        Me.Label14.Width = 1.25!
        '
        'S11_002
        '
        Me.S11_002.Height = 0.1875!
        Me.S11_002.HyperLink = Nothing
        Me.S11_002.Left = 1.375!
        Me.S11_002.Name = "S11_002"
        Me.S11_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S11_002.Tag = "1"
        Me.S11_002.Text = "W18X192" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_002.Top = 0.374!
        Me.S11_002.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.188!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0.125!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "LINK ID:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 0.562!
        Me.Label16.Width = 1.25!
        '
        'S11_003
        '
        Me.S11_003.Height = 0.1875!
        Me.S11_003.HyperLink = Nothing
        Me.S11_003.Left = 1.375!
        Me.S11_003.Name = "S11_003"
        Me.S11_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S11_003.Tag = "1"
        Me.S11_003.Text = "YL4-3.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_003.Top = 0.562!
        Me.S11_003.Width = 1.0!
        '
        'headerS12
        '
        Me.headerS12.Height = 0.1875!
        Me.headerS12.HyperLink = Nothing
        Me.headerS12.Left = 0!
        Me.headerS12.Name = "headerS12"
        Me.headerS12.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS12.Text = "1.2 LINK STEM GEOMETRY:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS12.Top = 0.813!
        Me.headerS12.Width = 3.0!
        '
        'S12_001
        '
        Me.S12_001.Height = 0.1875!
        Me.S12_001.HyperLink = Nothing
        Me.S12_001.Left = 2.5!
        Me.S12_001.Name = "S12_001"
        Me.S12_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_001.Top = 1.0!
        Me.S12_001.Width = 1.0!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 3.75!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "Thickness (t_stem) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 1.0!
        Me.Label21.Width = 2.562!
        '
        'S12_006
        '
        Me.S12_006.Height = 0.1875!
        Me.S12_006.HyperLink = Nothing
        Me.S12_006.Left = 6.312!
        Me.S12_006.Name = "S12_006"
        Me.S12_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_006.Top = 1.0!
        Me.S12_006.Width = 1.0!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 3.5!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label23.Text = "in"
        Me.Label23.Top = 1.0!
        Me.Label23.Width = 0.25!
        '
        'Label24
        '
        Me.Label24.Height = 0.1875!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 7.312!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label24.Text = "in"
        Me.Label24.Top = 1.188!
        Me.Label24.Width = 0.25!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = "Yield Length, incl. fillets (L_stemYield) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 1.188!
        Me.Label25.Width = 2.5!
        '
        'S12_002
        '
        Me.S12_002.Height = 0.1875!
        Me.S12_002.HyperLink = Nothing
        Me.S12_002.Left = 2.5!
        Me.S12_002.Name = "S12_002"
        Me.S12_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_002.Top = 1.188!
        Me.S12_002.Width = 1.0!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.75!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label27.Text = "NY Width ColSide (Wcol_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label27.Top = 1.188!
        Me.Label27.Width = 2.562!
        '
        'S12_007
        '
        Me.S12_007.Height = 0.1875!
        Me.S12_007.HyperLink = Nothing
        Me.S12_007.Left = 6.312!
        Me.S12_007.Name = "S12_007"
        Me.S12_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_007.Top = 1.188!
        Me.S12_007.Width = 1.0!
        '
        'Label29
        '
        Me.Label29.Height = 0.1875!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 3.5!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label29.Text = "in"
        Me.Label29.Top = 1.188!
        Me.Label29.Width = 0.25!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 7.312!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label30.Text = "in"
        Me.Label30.Top = 1.376!
        Me.Label30.Width = 0.25!
        '
        'Label31
        '
        Me.Label31.Height = 0.1875!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 0!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label31.Text = "NY Length BeamSide (Lbm_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label31.Top = 1.376!
        Me.Label31.Width = 2.5!
        '
        'S12_003
        '
        Me.S12_003.Height = 0.1875!
        Me.S12_003.HyperLink = Nothing
        Me.S12_003.Left = 2.5!
        Me.S12_003.Name = "S12_003"
        Me.S12_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_003.Top = 1.376!
        Me.S12_003.Width = 1.0!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 3.75!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "Central Neck Yield Width (w_stemYield)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label33.Top = 1.376!
        Me.Label33.Width = 2.562!
        '
        'S12_008
        '
        Me.S12_008.Height = 0.1875!
        Me.S12_008.HyperLink = Nothing
        Me.S12_008.Left = 6.312!
        Me.S12_008.Name = "S12_008"
        Me.S12_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_008.Top = 1.376!
        Me.S12_008.Width = 1.0!
        '
        'Label35
        '
        Me.Label35.Height = 0.1875!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 3.5!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label35.Text = "in"
        Me.Label35.Top = 1.376!
        Me.Label35.Width = 0.25!
        '
        'Label36
        '
        Me.Label36.Height = 0.1875!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 7.312!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label36.Text = "in"
        Me.Label36.Top = 1.563!
        Me.Label36.Width = 0.25!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label37.Text = "L_stem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label37.Top = 1.563!
        Me.Label37.Width = 2.5!
        '
        'S12_004
        '
        Me.S12_004.Height = 0.1875!
        Me.S12_004.HyperLink = Nothing
        Me.S12_004.Left = 2.5!
        Me.S12_004.Name = "S12_004"
        Me.S12_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S12_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_004.Top = 1.563!
        Me.S12_004.Width = 1.0!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 3.75!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label39.Text = "NY Width BeamSide (Wbm_side)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label39.Top = 1.563!
        Me.Label39.Width = 2.562!
        '
        'S12_009
        '
        Me.S12_009.Height = 0.1875!
        Me.S12_009.HyperLink = Nothing
        Me.S12_009.Left = 6.312!
        Me.S12_009.Name = "S12_009"
        Me.S12_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_009.Top = 1.563!
        Me.S12_009.Width = 1.0!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 3.5!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label41.Text = "in"
        Me.Label41.Top = 1.563!
        Me.Label41.Width = 0.25!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 7.312!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "in^2"
        Me.Label42.Top = 1.751!
        Me.Label42.Width = 0.25!
        '
        'Label43
        '
        Me.Label43.Height = 0.1875!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 0!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label43.Text = "Fillet Radius (r_fillet)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label43.Top = 1.751!
        Me.Label43.Width = 2.5!
        '
        'S12_005
        '
        Me.S12_005.Height = 0.1875!
        Me.S12_005.HyperLink = Nothing
        Me.S12_005.Left = 2.5!
        Me.S12_005.Name = "S12_005"
        Me.S12_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S12_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_005.Top = 1.751!
        Me.S12_005.Width = 1.0!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 3.75!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label45.Text = "Yielding Area (A_stemYield) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label45.Top = 1.751!
        Me.Label45.Width = 2.562!
        '
        'S12_010
        '
        Me.S12_010.Height = 0.1875!
        Me.S12_010.HyperLink = Nothing
        Me.S12_010.Left = 6.312!
        Me.S12_010.Name = "S12_010"
        Me.S12_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S12_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S12_010.Top = 1.751!
        Me.S12_010.Width = 1.0!
        '
        'Label47
        '
        Me.Label47.Height = 0.1875!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 3.5!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label47.Text = "in"
        Me.Label47.Top = 1.751!
        Me.Label47.Width = 0.25!
        '
        'headerS13
        '
        Me.headerS13.Height = 0.1875!
        Me.headerS13.HyperLink = Nothing
        Me.headerS13.Left = 0!
        Me.headerS13.Name = "headerS13"
        Me.headerS13.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS13.Text = "1.3 LINK STEM BOLTS:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS13.Top = 2.0!
        Me.headerS13.Width = 3.0!
        '
        'Label49
        '
        Me.Label49.Height = 0.1875!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 7.312!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label49.Text = "in"
        Me.Label49.Top = 2.187!
        Me.Label49.Width = 0.25!
        '
        'Label50
        '
        Me.Label50.Height = 0.1875!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 0!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label50.Text = "Num. Bolts (n_bolt_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label50.Top = 2.187!
        Me.Label50.Width = 2.5!
        '
        'S13_001
        '
        Me.S13_001.Height = 0.1875!
        Me.S13_001.HyperLink = Nothing
        Me.S13_001.Left = 2.5!
        Me.S13_001.Name = "S13_001"
        Me.S13_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_001.Top = 2.187!
        Me.S13_001.Width = 1.0!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 3.749999!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label52.Text = "Gauge Along Width (bolt_g_stem)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label52.Top = 2.187!
        Me.Label52.Width = 2.562!
        '
        'S13_005
        '
        Me.S13_005.Height = 0.1875!
        Me.S13_005.HyperLink = Nothing
        Me.S13_005.Left = 6.312!
        Me.S13_005.Name = "S13_005"
        Me.S13_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_005.Top = 2.187!
        Me.S13_005.Width = 1.0!
        '
        'Label55
        '
        Me.Label55.Height = 0.1875!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 7.312!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label55.Text = "in"
        Me.Label55.Top = 2.374!
        Me.Label55.Width = 0.25!
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 0!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label56.Text = "Bolt Type (Bolt_Gr_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label56.Top = 2.374!
        Me.Label56.Width = 2.5!
        '
        'S13_002
        '
        Me.S13_002.Height = 0.1875!
        Me.S13_002.HyperLink = Nothing
        Me.S13_002.Left = 2.5!
        Me.S13_002.Name = "S13_002"
        Me.S13_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_002.Top = 2.374!
        Me.S13_002.Width = 1.0!
        '
        'Label58
        '
        Me.Label58.Height = 0.1875!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 3.749999!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label58.Text = "Spacing Along Length (bolt_s_stem) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label58.Top = 2.374!
        Me.Label58.Width = 2.562!
        '
        'S13_006
        '
        Me.S13_006.Height = 0.1875!
        Me.S13_006.HyperLink = Nothing
        Me.S13_006.Left = 6.312!
        Me.S13_006.Name = "S13_006"
        Me.S13_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_006.Top = 2.374!
        Me.S13_006.Width = 1.0!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 7.312!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label19.Text = "in"
        Me.Label19.Top = 2.561!
        Me.Label19.Width = 0.25!
        '
        'Label54
        '
        Me.Label54.Height = 0.1875!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 0!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label54.Text = "Bolt Dia (boltD_linkBm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label54.Top = 2.561!
        Me.Label54.Width = 2.5!
        '
        'S13_003
        '
        Me.S13_003.Height = 0.1875!
        Me.S13_003.HyperLink = Nothing
        Me.S13_003.Left = 2.5!
        Me.S13_003.Name = "S13_003"
        Me.S13_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_003.Top = 2.561!
        Me.S13_003.Width = 1.0!
        '
        'Label62
        '
        Me.Label62.Height = 0.1875!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 3.749999!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label62.Text = "First Bolt distance to Neck (Sc)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label62.Top = 2.561!
        Me.Label62.Width = 2.562!
        '
        'Label63
        '
        Me.Label63.Height = 0.1875!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 3.5!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label63.Text = "in"
        Me.Label63.Top = 2.561!
        Me.Label63.Width = 0.25!
        '
        'S13_007
        '
        Me.S13_007.Height = 0.1875!
        Me.S13_007.HyperLink = Nothing
        Me.S13_007.Left = 6.312!
        Me.S13_007.Name = "S13_007"
        Me.S13_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_007.Top = 2.561!
        Me.S13_007.Width = 1.0!
        '
        'Label59
        '
        Me.Label59.Height = 0.1875!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 7.312!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label59.Text = "in"
        Me.Label59.Top = 2.748!
        Me.Label59.Width = 0.25!
        '
        'Label65
        '
        Me.Label65.Height = 0.1875!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 0!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label65.Text = "Min. Bolt length =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label65.Top = 2.748!
        Me.Label65.Width = 2.5!
        '
        'Label67
        '
        Me.Label67.Height = 0.1875!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 3.749999!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label67.Text = "Last Bolt distance to Edge (Sb) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label67.Top = 2.748!
        Me.Label67.Width = 2.562!
        '
        'Label68
        '
        Me.Label68.Height = 0.1875!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 3.5!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label68.Text = "in"
        Me.Label68.Top = 2.748!
        Me.Label68.Width = 0.25!
        '
        'S13_008
        '
        Me.S13_008.Height = 0.1875!
        Me.S13_008.HyperLink = Nothing
        Me.S13_008.Left = 6.312!
        Me.S13_008.Name = "S13_008"
        Me.S13_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S13_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S13_008.Top = 2.748!
        Me.S13_008.Width = 1.0!
        '
        'headerS15
        '
        Me.headerS15.Height = 0.1875!
        Me.headerS15.HyperLink = Nothing
        Me.headerS15.Left = 0!
        Me.headerS15.Name = "headerS15"
        Me.headerS15.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS15.Text = "1.5 LINK FLANGE BOLTS:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS15.Top = 3.875!
        Me.headerS15.Width = 3.0!
        '
        'Label71
        '
        Me.Label71.Height = 0.1875!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 7.312!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label71.Text = "in"
        Me.Label71.Top = 4.062!
        Me.Label71.Width = 0.25!
        '
        'Label72
        '
        Me.Label72.Height = 0.1875!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 0!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label72.Text = "Num. Bolts (n_bolt_linkCol) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label72.Top = 4.062!
        Me.Label72.Width = 2.5!
        '
        'S15_001
        '
        Me.S15_001.Height = 0.1875!
        Me.S15_001.HyperLink = Nothing
        Me.S15_001.Left = 2.5!
        Me.S15_001.Name = "S15_001"
        Me.S15_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S15_001.Text = "4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S15_001.Top = 4.062!
        Me.S15_001.Width = 1.0!
        '
        'Label74
        '
        Me.Label74.Height = 0.1875!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 3.312999!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label74.Text = "Gauge Along Width (vertical) (bolt_g_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label74.Top = 4.062!
        Me.Label74.Width = 2.999!
        '
        'S15_004
        '
        Me.S15_004.Height = 0.1875!
        Me.S15_004.HyperLink = Nothing
        Me.S15_004.Left = 6.312!
        Me.S15_004.Name = "S15_004"
        Me.S15_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S15_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S15_004.Top = 4.062!
        Me.S15_004.Width = 1.0!
        '
        'Label77
        '
        Me.Label77.Height = 0.1875!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 7.312!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label77.Text = "in"
        Me.Label77.Top = 4.249001!
        Me.Label77.Width = 0.25!
        '
        'Label78
        '
        Me.Label78.Height = 0.1875!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 0!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label78.Text = "Bolt Type (Bolt_Gr_linkCol)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label78.Top = 4.249001!
        Me.Label78.Width = 2.5!
        '
        'S15_002
        '
        Me.S15_002.Height = 0.1875!
        Me.S15_002.HyperLink = Nothing
        Me.S15_002.Left = 2.5!
        Me.S15_002.Name = "S15_002"
        Me.S15_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S15_002.Text = "A325" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S15_002.Top = 4.249001!
        Me.S15_002.Width = 1.0!
        '
        'Label80
        '
        Me.Label80.Height = 0.1875!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 3.312999!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label80.Text = "Spacing Along Length (horiz)  (bolt_s_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label80.Top = 4.249001!
        Me.Label80.Width = 2.999!
        '
        'S15_005
        '
        Me.S15_005.Height = 0.1875!
        Me.S15_005.HyperLink = Nothing
        Me.S15_005.Left = 6.312!
        Me.S15_005.Name = "S15_005"
        Me.S15_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S15_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S15_005.Top = 4.249001!
        Me.S15_005.Width = 1.0!
        '
        'Label83
        '
        Me.Label83.Height = 0.1875!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 0!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label83.Text = "Bolt Dia (boltD_linkCol)_=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label83.Top = 4.437001!
        Me.Label83.Width = 2.5!
        '
        'S15_003
        '
        Me.S15_003.Height = 0.1875!
        Me.S15_003.HyperLink = Nothing
        Me.S15_003.Left = 2.5!
        Me.S15_003.Name = "S15_003"
        Me.S15_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S15_003.Text = "0.875" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S15_003.Top = 4.437001!
        Me.S15_003.Width = 1.0!
        '
        'Label85
        '
        Me.Label85.Height = 0.1875!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 3.5!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label85.Text = "in"
        Me.Label85.Top = 4.437001!
        Me.Label85.Width = 0.25!
        '
        'headerS16
        '
        Me.headerS16.Height = 0.1875!
        Me.headerS16.HyperLink = Nothing
        Me.headerS16.Left = 0!
        Me.headerS16.Name = "headerS16"
        Me.headerS16.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS16.Text = "1.6 LINK MATERIAL:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS16.Top = 4.687!
        Me.headerS16.Width = 3.0!
        '
        'Label86
        '
        Me.Label86.Height = 0.1875!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 0.0000002384186!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label86.Text = "Fy_link =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label86.Top = 4.874001!
        Me.Label86.Width = 2.5!
        '
        'S16_001
        '
        Me.S16_001.Height = 0.1875!
        Me.S16_001.HyperLink = Nothing
        Me.S16_001.Left = 2.5!
        Me.S16_001.Name = "S16_001"
        Me.S16_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S16_001.Text = "50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S16_001.Top = 4.874001!
        Me.S16_001.Width = 1.0!
        '
        'Label88
        '
        Me.Label88.Height = 0.1875!
        Me.Label88.HyperLink = Nothing
        Me.Label88.Left = 3.812999!
        Me.Label88.Name = "Label88"
        Me.Label88.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label88.Text = "Material Overstrength Factor (Rt_link) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label88.Top = 4.874001!
        Me.Label88.Width = 2.499001!
        '
        'S16_003
        '
        Me.S16_003.Height = 0.1875!
        Me.S16_003.HyperLink = Nothing
        Me.S16_003.Left = 6.312999!
        Me.S16_003.Name = "S16_003"
        Me.S16_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S16_003.Text = "1.2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S16_003.Top = 4.874001!
        Me.S16_003.Width = 1.0!
        '
        'Label90
        '
        Me.Label90.Height = 0.1875!
        Me.Label90.HyperLink = Nothing
        Me.Label90.Left = 3.5!
        Me.Label90.Name = "Label90"
        Me.Label90.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label90.Text = "ksi"
        Me.Label90.Top = 4.874001!
        Me.Label90.Width = 0.25!
        '
        'Label81
        '
        Me.Label81.Height = 0.1875!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 0!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label81.Text = "Fu_link =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label81.Top = 5.061001!
        Me.Label81.Width = 2.5!
        '
        'S16_002
        '
        Me.S16_002.Height = 0.1875!
        Me.S16_002.HyperLink = Nothing
        Me.S16_002.Left = 2.5!
        Me.S16_002.Name = "S16_002"
        Me.S16_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S16_002.Text = "65"
        Me.S16_002.Top = 5.061001!
        Me.S16_002.Width = 1.0!
        '
        'Label92
        '
        Me.Label92.Height = 0.1875!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 3.812999!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label92.Text = "Ry_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label92.Top = 5.061001!
        Me.Label92.Width = 2.5!
        '
        'S16_004
        '
        Me.S16_004.Height = 0.1875!
        Me.S16_004.HyperLink = Nothing
        Me.S16_004.Left = 6.312!
        Me.S16_004.Name = "S16_004"
        Me.S16_004.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S16_004.Text = "1.1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S16_004.Top = 5.061001!
        Me.S16_004.Width = 1.0!
        '
        'Label94
        '
        Me.Label94.Height = 0.1875!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 3.5!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label94.Text = "ksi"
        Me.Label94.Top = 5.061001!
        Me.Label94.Width = 0.25!
        '
        'headerS17
        '
        Me.headerS17.Height = 0.1875!
        Me.headerS17.HyperLink = Nothing
        Me.headerS17.Left = 0.0000002384186!
        Me.headerS17.Name = "headerS17"
        Me.headerS17.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS17.Text = "1.7 LINK K_ROT:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS17.Top = 5.249999!
        Me.headerS17.Width = 3.0!
        '
        'Label96
        '
        Me.Label96.Height = 0.1875!
        Me.Label96.HyperLink = Nothing
        Me.Label96.Left = 0.0000002384186!
        Me.Label96.Name = "Label96"
        Me.Label96.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label96.Text = "Pye_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label96.Top = 5.437999!
        Me.Label96.Width = 2.5!
        '
        'S17_001
        '
        Me.S17_001.Height = 0.1875!
        Me.S17_001.HyperLink = Nothing
        Me.S17_001.Left = 2.5!
        Me.S17_001.Name = "S17_001"
        Me.S17_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_001.Text = "96.250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_001.Top = 5.437999!
        Me.S17_001.Width = 1.0!
        '
        'Label98
        '
        Me.Label98.Height = 0.188!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 3.5!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label98.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Expected yield strength of link" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label98.Top = 5.437999!
        Me.Label98.Width = 4.25!
        '
        'Label99
        '
        Me.Label99.Height = 0.1875!
        Me.Label99.HyperLink = Nothing
        Me.Label99.Left = 0.0000002384186!
        Me.Label99.Name = "Label99"
        Me.Label99.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label99.Text = "Link Flange (K1) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label99.Top = 5.625!
        Me.Label99.Width = 2.5!
        '
        'S17_002
        '
        Me.S17_002.Height = 0.1875!
        Me.S17_002.HyperLink = Nothing
        Me.S17_002.Left = 2.5!
        Me.S17_002.Name = "S17_002"
        Me.S17_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_002.Text = "54330" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_002.Top = 5.625!
        Me.S17_002.Width = 1.0!
        '
        'Label101
        '
        Me.Label101.Height = 0.188!
        Me.Label101.HyperLink = Nothing
        Me.Label101.Left = 3.5!
        Me.Label101.Name = "Label101"
        Me.Label101.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label101.Text = "kips/in" & Global.Microsoft.VisualBasic.ChrW(9) & "=0.75 * 192 *E *(Wcol_side*t_flange^3/ 12) / bolt_g_flange^3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label101.Top = 5.625!
        Me.Label101.Width = 4.25!
        '
        'Label102
        '
        Me.Label102.Height = 0.1875!
        Me.Label102.HyperLink = Nothing
        Me.Label102.Left = 0.0000002384186!
        Me.Label102.Name = "Label102"
        Me.Label102.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label102.Text = "Link Stem NY Portion (K2) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label102.Top = 5.813!
        Me.Label102.Width = 2.5!
        '
        'S17_003
        '
        Me.S17_003.Height = 0.1875!
        Me.S17_003.HyperLink = Nothing
        Me.S17_003.Left = 2.5!
        Me.S17_003.Name = "S17_003"
        Me.S17_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_003.Text = "14730" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_003.Top = 5.813!
        Me.S17_003.Width = 1.0!
        '
        'Label104
        '
        Me.Label104.Height = 0.188!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 3.5!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label104.Text = "kips/in" & Global.Microsoft.VisualBasic.ChrW(9) & "=t_stem*Wcol_side * E /  [ (Lcol_side + Sc + if (n_bolt_linkBm > 4, bolt_" &
    "s_stem/2,0) ]" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label104.Top = 5.813!
        Me.Label104.Width = 4.25!
        '
        'Label105
        '
        Me.Label105.Height = 0.1875!
        Me.Label105.HyperLink = Nothing
        Me.Label105.Left = 0.0000002384186!
        Me.Label105.Name = "Label105"
        Me.Label105.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label105.Text = "Link Stem Neck (K3) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label105.Top = 6.000999!
        Me.Label105.Width = 2.5!
        '
        'S17_004
        '
        Me.S17_004.Height = 0.1875!
        Me.S17_004.HyperLink = Nothing
        Me.S17_004.Left = 2.5!
        Me.S17_004.Name = "S17_004"
        Me.S17_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_004.Text = "7250" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_004.Top = 6.000999!
        Me.S17_004.Width = 1.0!
        '
        'Label107
        '
        Me.Label107.Height = 0.188!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 3.5!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label107.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=t_stem * w_stemYield * E / L_stemYield" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label107.Top = 6.000999!
        Me.Label107.Width = 4.25!
        '
        'Label108
        '
        Me.Label108.Height = 0.1875!
        Me.Label108.HyperLink = Nothing
        Me.Label108.Left = 0.0000002384186!
        Me.Label108.Name = "Label108"
        Me.Label108.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label108.Text = "Effictive Stiffness (K_eff)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label108.Top = 6.188!
        Me.Label108.Width = 2.5!
        '
        'S17_005
        '
        Me.S17_005.Height = 0.1875!
        Me.S17_005.HyperLink = Nothing
        Me.S17_005.Left = 2.5!
        Me.S17_005.Name = "S17_005"
        Me.S17_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_005.Text = "4460" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_005.Top = 6.188!
        Me.S17_005.Width = 1.0!
        '
        'Label110
        '
        Me.Label110.Height = 0.188!
        Me.Label110.HyperLink = Nothing
        Me.Label110.Left = 3.5!
        Me.Label110.Name = "Label110"
        Me.Label110.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label110.Text = "kips/in" & Global.Microsoft.VisualBasic.ChrW(9) & "=K1*K2*K3 / (K1*K2 + K2*K3 + K1*K3)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label110.Top = 6.188!
        Me.Label110.Width = 4.25!
        '
        'Label111
        '
        Me.Label111.Height = 0.1875!
        Me.Label111.HyperLink = Nothing
        Me.Label111.Left = 0.0000002384186!
        Me.Label111.Name = "Label111"
        Me.Label111.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label111.Text = "Mye_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label111.Top = 6.375999!
        Me.Label111.Width = 2.5!
        '
        'S17_006
        '
        Me.S17_006.Height = 0.1875!
        Me.S17_006.HyperLink = Nothing
        Me.S17_006.Left = 2.5!
        Me.S17_006.Name = "S17_006"
        Me.S17_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_006.Text = "2012" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_006.Top = 6.375999!
        Me.S17_006.Width = 1.0!
        '
        'Label113
        '
        Me.Label113.Height = 0.188!
        Me.Label113.HyperLink = Nothing
        Me.Label113.Left = 3.5!
        Me.Label113.Name = "Label113"
        Me.Label113.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label113.Text = "k-in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label113.Top = 6.375999!
        Me.Label113.Width = 4.25!
        '
        'Label114
        '
        Me.Label114.Height = 0.1875!
        Me.Label114.HyperLink = Nothing
        Me.Label114.Left = 0.0000002384186!
        Me.Label114.Name = "Label114"
        Me.Label114.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label114.Text = "Dy=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label114.Top = 6.563!
        Me.Label114.Width = 2.5!
        '
        'S17_007
        '
        Me.S17_007.Height = 0.1875!
        Me.S17_007.HyperLink = Nothing
        Me.S17_007.Left = 2.5!
        Me.S17_007.Name = "S17_007"
        Me.S17_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_007.Text = "0.0216" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_007.Top = 6.563!
        Me.S17_007.Width = 1.0!
        '
        'Label116
        '
        Me.Label116.Height = 0.188!
        Me.Label116.HyperLink = Nothing
        Me.Label116.Left = 3.5!
        Me.Label116.Name = "Label116"
        Me.Label116.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label116.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label116.Top = 6.563!
        Me.Label116.Width = 4.25!
        '
        'Label117
        '
        Me.Label117.Height = 0.1875!
        Me.Label117.HyperLink = Nothing
        Me.Label117.Left = 0.0000002384186!
        Me.Label117.Name = "Label117"
        Me.Label117.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label117.Text = "qy=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label117.Top = 6.750999!
        Me.Label117.Width = 2.5!
        '
        'S17_008
        '
        Me.S17_008.Height = 0.1875!
        Me.S17_008.HyperLink = Nothing
        Me.S17_008.Left = 2.5!
        Me.S17_008.Name = "S17_008"
        Me.S17_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S17_008.Text = "0.002065" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S17_008.Top = 6.750999!
        Me.S17_008.Width = 1.0!
        '
        'Label119
        '
        Me.Label119.Height = 0.188!
        Me.Label119.HyperLink = Nothing
        Me.Label119.Left = 3.5!
        Me.Label119.Name = "Label119"
        Me.Label119.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label119.Text = "rad" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label119.Top = 6.750999!
        Me.Label119.Width = 4.25!
        '
        'Label120
        '
        Me.Label120.Height = 0.1875!
        Me.Label120.HyperLink = Nothing
        Me.Label120.Left = 0.0000002384186!
        Me.Label120.Name = "Label120"
        Me.Label120.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label120.Text = "Krot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label120.Top = 6.938!
        Me.Label120.Width = 2.5!
        '
        'Label122
        '
        Me.Label122.Height = 0.188!
        Me.Label122.HyperLink = Nothing
        Me.Label122.Left = 3.5!
        Me.Label122.Name = "Label122"
        Me.Label122.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label122.Text = "k-in/rad" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label122.Top = 6.938!
        Me.Label122.Width = 4.25!
        '
        'headerS18
        '
        Me.headerS18.Height = 0.1875!
        Me.headerS18.HyperLink = Nothing
        Me.headerS18.Left = 0.01000024!
        Me.headerS18.Name = "headerS18"
        Me.headerS18.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS18.Text = "1.8 BEAM FLANGE THICKNESS CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS18.Top = 7.135!
        Me.headerS18.Width = 3.0!
        '
        'Label124
        '
        Me.Label124.Height = 0.1875!
        Me.Label124.HyperLink = Nothing
        Me.Label124.Left = 0.01000024!
        Me.Label124.Name = "Label124"
        Me.Label124.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label124.Text = "Beam flange thickness (tbf)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label124.Top = 7.385!
        Me.Label124.Width = 2.5!
        '
        'S18_001
        '
        Me.S18_001.Height = 0.1875!
        Me.S18_001.HyperLink = Nothing
        Me.S18_001.Left = 2.51!
        Me.S18_001.Name = "S18_001"
        Me.S18_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S18_001.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S18_001.Top = 7.385!
        Me.S18_001.Width = 1.0!
        '
        'Label126
        '
        Me.Label126.Height = 0.1875!
        Me.Label126.HyperLink = Nothing
        Me.Label126.Left = 3.51!
        Me.Label126.Name = "Label126"
        Me.Label126.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label126.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label126.Top = 7.385!
        Me.Label126.Width = 4.5!
        '
        'Label127
        '
        Me.Label127.Height = 0.1875!
        Me.Label127.HyperLink = Nothing
        Me.Label127.Left = 0.01000024!
        Me.Label127.Name = "Label127"
        Me.Label127.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label127.Text = "SUM_bf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label127.Top = 7.572002!
        Me.Label127.Width = 2.5!
        '
        'S18_002
        '
        Me.S18_002.Height = 0.1875!
        Me.S18_002.HyperLink = Nothing
        Me.S18_002.Left = 2.51!
        Me.S18_002.Name = "S18_002"
        Me.S18_002.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S18_002.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S18_002.Top = 7.572002!
        Me.S18_002.Width = 1.0!
        '
        'Label129
        '
        Me.Label129.Height = 0.188!
        Me.Label129.HyperLink = Nothing
        Me.Label129.Left = 3.51!
        Me.Label129.Name = "Label129"
        Me.Label129.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label129.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.4 / (tbf)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "(AISC 358-16, Chapter 12.3.1.3)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label129.Top = 7.572002!
        Me.Label129.Width = 4.25!
        '
        'headerS19
        '
        Me.headerS19.Height = 0.1875!
        Me.headerS19.HyperLink = Nothing
        Me.headerS19.Left = 0.01000024!
        Me.headerS19.Name = "headerS19"
        Me.headerS19.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS19.Text = "1.9 BEAM & COLUMN FLANGE WIDTH CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS19.Top = 8.02!
        Me.headerS19.Width = 3.0!
        '
        'Label131
        '
        Me.Label131.Height = 0.1875!
        Me.Label131.HyperLink = Nothing
        Me.Label131.Left = 0!
        Me.Label131.Name = "Label131"
        Me.Label131.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label131.Text = "bbf_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label131.Top = 8.186001!
        Me.Label131.Width = 2.5!
        '
        'S19_001
        '
        Me.S19_001.Height = 0.1875!
        Me.S19_001.HyperLink = Nothing
        Me.S19_001.Left = 2.5!
        Me.S19_001.Name = "S19_001"
        Me.S19_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S19_001.Text = "3.500" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_001.Top = 8.186001!
        Me.S19_001.Width = 1.0!
        '
        'Label133
        '
        Me.Label133.Height = 0.1875!
        Me.Label133.HyperLink = Nothing
        Me.Label133.Left = 0!
        Me.Label133.Name = "Label133"
        Me.Label133.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label133.Text = "Beam flange width (bbf)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label133.Top = 8.529001!
        Me.Label133.Width = 2.5!
        '
        'S19_002
        '
        Me.S19_002.Height = 0.1875!
        Me.S19_002.HyperLink = Nothing
        Me.S19_002.Left = 2.5!
        Me.S19_002.Name = "S19_002"
        Me.S19_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_002.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_002.Top = 8.529001!
        Me.S19_002.Width = 1.0!
        '
        'Label135
        '
        Me.Label135.Height = 0.1875!
        Me.Label135.HyperLink = Nothing
        Me.Label135.Left = 0!
        Me.Label135.Name = "Label135"
        Me.Label135.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label135.Text = "I_End Column flange width (bc1f)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label135.Top = 8.717003!
        Me.Label135.Width = 2.5!
        '
        'S19_003
        '
        Me.S19_003.Height = 0.1875!
        Me.S19_003.HyperLink = Nothing
        Me.S19_003.Left = 2.5!
        Me.S19_003.Name = "S19_003"
        Me.S19_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_003.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_003.Top = 8.717003!
        Me.S19_003.Width = 1.0!
        '
        'Label137
        '
        Me.Label137.Height = 0.1875!
        Me.Label137.HyperLink = Nothing
        Me.Label137.Left = 0!
        Me.Label137.Name = "Label137"
        Me.Label137.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label137.Text = "J_End Column flange width (bc2f)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label137.Top = 8.905003!
        Me.Label137.Width = 2.5!
        '
        'S19_004
        '
        Me.S19_004.Height = 0.1875!
        Me.S19_004.HyperLink = Nothing
        Me.S19_004.Left = 2.5!
        Me.S19_004.Name = "S19_004"
        Me.S19_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_004.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_004.Top = 8.905003!
        Me.S19_004.Width = 1.0!
        '
        'Label139
        '
        Me.Label139.Height = 0.1875!
        Me.Label139.HyperLink = Nothing
        Me.Label139.Left = 0!
        Me.Label139.Name = "Label139"
        Me.Label139.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label139.Text = "DCR_bf="
        Me.Label139.Top = 10.031!
        Me.Label139.Width = 2.5!
        '
        'S19_010
        '
        Me.S19_010.Height = 0.1875!
        Me.S19_010.HyperLink = Nothing
        Me.S19_010.Left = 2.5!
        Me.S19_010.Name = "S19_010"
        Me.S19_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_010.Text = "0.61" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_010.Top = 10.031!
        Me.S19_010.Width = 1.0!
        '
        'Label141
        '
        Me.Label141.Height = 0.1875!
        Me.Label141.HyperLink = Nothing
        Me.Label141.Left = 0!
        Me.Label141.Name = "Label141"
        Me.Label141.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label141.Text = "Adequate =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label141.Top = 10.593!
        Me.Label141.Width = 2.5!
        '
        'Label143
        '
        Me.Label143.Height = 0.188!
        Me.Label143.HyperLink = Nothing
        Me.Label143.Left = 3.5!
        Me.Label143.Name = "Label143"
        Me.Label143.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label143.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if DCR_bbf <= DCR_allowed and Clip washers<> N/G" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label143.Top = 10.593!
        Me.Label143.Width = 4.25!
        '
        'Label144
        '
        Me.Label144.Height = 0.188!
        Me.Label144.HyperLink = Nothing
        Me.Label144.Left = 3.5!
        Me.Label144.Name = "Label144"
        Me.Label144.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label144.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=max(bbf_min/bbf, bcf_min/bc1f, bcf_min/bc2f)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label144.Top = 10.031!
        Me.Label144.Width = 4.25!
        '
        'headerS110
        '
        Me.headerS110.Height = 0.1875!
        Me.headerS110.HyperLink = Nothing
        Me.headerS110.Left = 0!
        Me.headerS110.Name = "headerS110"
        Me.headerS110.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS110.Text = "1.10 LINK STEM YIELDING LENGTH CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS110.Top = 10.853!
        Me.headerS110.Width = 3.0!
        '
        'Label146
        '
        Me.Label146.Height = 0.1875!
        Me.Label146.HyperLink = Nothing
        Me.Label146.Left = 0!
        Me.Label146.Name = "Label146"
        Me.Label146.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label146.Text = "Limit Rotation (r_linkLimit) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label146.Top = 11.103!
        Me.Label146.Width = 2.5!
        '
        'S110_001
        '
        Me.S110_001.Height = 0.1875!
        Me.S110_001.HyperLink = Nothing
        Me.S110_001.Left = 2.5!
        Me.S110_001.Name = "S110_001"
        Me.S110_001.Style = "color: Black; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-" &
    "align: middle; ddo-char-set: 0"
        Me.S110_001.Text = "0.05" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_001.Top = 11.103!
        Me.S110_001.Width = 1.0!
        '
        'Label148
        '
        Me.Label148.Height = 0.188!
        Me.Label148.HyperLink = Nothing
        Me.Label148.Left = 3.5!
        Me.Label148.Name = "Label148"
        Me.Label148.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label148.Text = "radians" & Global.Microsoft.VisualBasic.ChrW(9) & "Connection rotation per ESR-2802" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label148.Top = 11.103!
        Me.Label148.Width = 4.25!
        '
        'Label149
        '
        Me.Label149.Height = 0.1875!
        Me.Label149.HyperLink = Nothing
        Me.Label149.Left = 0!
        Me.Label149.Name = "Label149"
        Me.Label149.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label149.Text = "Limit Strain ε (e_linkLimit) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label149.Top = 11.291!
        Me.Label149.Width = 2.5!
        '
        'S110_002
        '
        Me.S110_002.Height = 0.1875!
        Me.S110_002.HyperLink = Nothing
        Me.S110_002.Left = 2.5!
        Me.S110_002.Name = "S110_002"
        Me.S110_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S110_002.Text = "8.00%" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_002.Top = 11.291!
        Me.S110_002.Width = 1.0!
        '
        'Label151
        '
        Me.Label151.Height = 0.188!
        Me.Label151.HyperLink = Nothing
        Me.Label151.Left = 3.5!
        Me.Label151.Name = "Label151"
        Me.Label151.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label151.Text = "                    Link strain limit per ESR-2802" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label151.Top = 11.291!
        Me.Label151.Width = 4.25!
        '
        'Label152
        '
        Me.Label152.Height = 0.1875!
        Me.Label152.HyperLink = Nothing
        Me.Label152.Left = 0!
        Me.Label152.Name = "Label152"
        Me.Label152.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label152.Text = "Rotation Arm (d_arm)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label152.Top = 11.478!
        Me.Label152.Width = 2.5!
        '
        'S110_003
        '
        Me.S110_003.Height = 0.1875!
        Me.S110_003.HyperLink = Nothing
        Me.S110_003.Left = 2.5!
        Me.S110_003.Name = "S110_003"
        Me.S110_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S110_003.Text = "10.450" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_003.Top = 11.478!
        Me.S110_003.Width = 1.0!
        '
        'Label154
        '
        Me.Label154.Height = 0.188!
        Me.Label154.HyperLink = Nothing
        Me.Label154.Left = 3.5!
        Me.Label154.Name = "Label154"
        Me.Label154.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label154.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (db + t_stem)/2" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label154.Top = 11.478!
        Me.Label154.Width = 4.25!
        '
        'Label155
        '
        Me.Label155.Height = 0.1875!
        Me.Label155.HyperLink = Nothing
        Me.Label155.Left = 0!
        Me.Label155.Name = "Label155"
        Me.Label155.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label155.Text = "Link Extension (linkDelta)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label155.Top = 11.666!
        Me.Label155.Width = 2.5!
        '
        'S110_004
        '
        Me.S110_004.Height = 0.1875!
        Me.S110_004.HyperLink = Nothing
        Me.S110_004.Left = 2.5!
        Me.S110_004.Name = "S110_004"
        Me.S110_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S110_004.Text = "0.523" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_004.Top = 11.666!
        Me.S110_004.Width = 1.0!
        '
        'Label157
        '
        Me.Label157.Height = 0.188!
        Me.Label157.HyperLink = Nothing
        Me.Label157.Left = 3.5!
        Me.Label157.Name = "Label157"
        Me.Label157.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label157.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=d_arm* r_linkLimit" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label157.Top = 11.666!
        Me.Label157.Width = 4.25!
        '
        'Label158
        '
        Me.Label158.Height = 0.1875!
        Me.Label158.HyperLink = Nothing
        Me.Label158.Left = 0!
        Me.Label158.Name = "Label158"
        Me.Label158.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label158.Text = "Lstem_yield_req=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label158.Top = 11.854!
        Me.Label158.Width = 2.5!
        '
        'S110_005
        '
        Me.S110_005.Height = 0.1875!
        Me.S110_005.HyperLink = Nothing
        Me.S110_005.Left = 2.5!
        Me.S110_005.Name = "S110_005"
        Me.S110_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S110_005.Text = "7.15" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_005.Top = 11.854!
        Me.S110_005.Width = 1.0!
        '
        'Label160
        '
        Me.Label160.Height = 0.188!
        Me.Label160.HyperLink = Nothing
        Me.Label160.Left = 3.5!
        Me.Label160.Name = "Label160"
        Me.Label160.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label160.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= r_linkLimit* d_arm/ 0.085 + 2* r_fillet" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label160.Top = 11.854!
        Me.Label160.Width = 4.25!
        '
        'Label161
        '
        Me.Label161.Height = 0.1875!
        Me.Label161.HyperLink = Nothing
        Me.Label161.Left = 0!
        Me.Label161.Name = "Label161"
        Me.Label161.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label161.Text = "L_stem_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label161.Top = 12.042!
        Me.Label161.Width = 2.5!
        '
        'S110_006
        '
        Me.S110_006.Height = 0.1875!
        Me.S110_006.HyperLink = Nothing
        Me.S110_006.Left = 2.5!
        Me.S110_006.Name = "S110_006"
        Me.S110_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S110_006.Text = "1.02" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S110_006.Top = 12.042!
        Me.S110_006.Width = 1.0!
        '
        'Label163
        '
        Me.Label163.Height = 0.188!
        Me.Label163.HyperLink = Nothing
        Me.Label163.Left = 3.5!
        Me.Label163.Name = "Label163"
        Me.Label163.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label163.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lstem_yield_req/ L_stemYield" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label163.Top = 12.042!
        Me.Label163.Width = 4.25!
        '
        'Label164
        '
        Me.Label164.Height = 0.1875!
        Me.Label164.HyperLink = Nothing
        Me.Label164.Left = 0!
        Me.Label164.Name = "Label164"
        Me.Label164.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label164.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label164.Top = 12.229!
        Me.Label164.Width = 2.5!
        '
        'Label166
        '
        Me.Label166.Height = 0.188!
        Me.Label166.HyperLink = Nothing
        Me.Label166.Left = 3.5!
        Me.Label166.Name = "Label166"
        Me.Label166.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label166.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "OK, if L_stem_DCR < SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label166.Top = 12.229!
        Me.Label166.Width = 4.25!
        '
        'headerS14
        '
        Me.headerS14.Height = 0.1875!
        Me.headerS14.HyperLink = Nothing
        Me.headerS14.Left = 0!
        Me.headerS14.Name = "headerS14"
        Me.headerS14.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.headerS14.Text = "1.4 LINK FLANGE GEOMETRY:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.headerS14.Top = 3.0!
        Me.headerS14.Width = 3.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "Thickness (t_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 3.25!
        Me.Label7.Width = 2.5!
        '
        'S14_001
        '
        Me.S14_001.Height = 0.1875!
        Me.S14_001.HyperLink = Nothing
        Me.S14_001.Left = 2.5!
        Me.S14_001.Name = "S14_001"
        Me.S14_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S14_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S14_001.Top = 3.25!
        Me.S14_001.Width = 1.0!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 3.5!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label11.Text = "in"
        Me.Label11.Top = 3.25!
        Me.Label11.Width = 0.25!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label13.Text = "Flange Width (W_flange)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 3.437001!
        Me.Label13.Width = 2.5!
        '
        'S14_002
        '
        Me.S14_002.Height = 0.1875!
        Me.S14_002.HyperLink = Nothing
        Me.S14_002.Left = 2.5!
        Me.S14_002.Name = "S14_002"
        Me.S14_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S14_002.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S14_002.Top = 3.437001!
        Me.S14_002.Width = 1.0!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 3.5!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label17.Text = "in"
        Me.Label17.Top = 3.437001!
        Me.Label17.Width = 0.25!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label20.Text = "Flange height (H_flange) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label20.Top = 3.624!
        Me.Label20.Width = 2.5!
        '
        'S14_003
        '
        Me.S14_003.Height = 0.1875!
        Me.S14_003.HyperLink = Nothing
        Me.S14_003.Left = 2.5!
        Me.S14_003.Name = "S14_003"
        Me.S14_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S14_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S14_003.Top = 3.624!
        Me.S14_003.Width = 1.0!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.5!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label26.Text = "in"
        Me.Label26.Top = 3.624!
        Me.Label26.Width = 0.25!
        '
        'Shape6
        '
        Me.Shape6.Height = 0.1875!
        Me.Shape6.Left = 1.375!
        Me.Shape6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 0.187!
        Me.Shape6.Width = 1.0!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 2.438!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label2.Text = "Assign K at I_End:"
        Me.Label2.Top = 0.562!
        Me.Label2.Width = 1.375!
        '
        'S11_006
        '
        Me.S11_006.Height = 0.1875!
        Me.S11_006.HyperLink = Nothing
        Me.S11_006.Left = 3.813!
        Me.S11_006.Name = "S11_006"
        Me.S11_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S11_006.Tag = "1"
        Me.S11_006.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_006.Top = 0.562!
        Me.S11_006.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 4.94!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Assign K at J_End:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 0.561!
        Me.Label9.Width = 1.375!
        '
        'S11_009
        '
        Me.S11_009.Height = 0.1875!
        Me.S11_009.HyperLink = Nothing
        Me.S11_009.Left = 6.314999!
        Me.S11_009.Name = "S11_009"
        Me.S11_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S11_009.Tag = "1"
        Me.S11_009.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S11_009.Top = 0.561!
        Me.S11_009.Width = 1.0!
        '
        'Shape7
        '
        Me.Shape7.Height = 0.1875!
        Me.Shape7.Left = 3.813!
        Me.Shape7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape7.Name = "Shape7"
        Me.Shape7.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape7.Top = 0.375!
        Me.Shape7.Width = 1.0!
        '
        'Shape8
        '
        Me.Shape8.Height = 0.1875!
        Me.Shape8.Left = 6.314999!
        Me.Shape8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape8.Name = "Shape8"
        Me.Shape8.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape8.Top = 0.375!
        Me.Shape8.Width = 1.0!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.01000024!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "Adequate =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 7.759999!
        Me.Label4.Width = 2.5!
        '
        'Label18
        '
        Me.Label18.Height = 0.188!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 3.51!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label18.Text = "= OK, if tbf>=0.4''" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 7.759999!
        Me.Label18.Width = 4.25!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label15.Text = "1.11 PRELIMINARY COLUMN PANEL ZONE CHECK"
        Me.Label15.Top = 12.487!
        Me.Label15.Width = 3.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.188!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 0!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label22.Text = "Column Unique Name="
        Me.Label22.Top = 12.675!
        Me.Label22.Width = 2.0!
        '
        'Label34
        '
        Me.Label34.Height = 0.188!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 5.0!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label34.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label34.Top = 13.239!
        Me.Label34.Width = 2.75!
        '
        'Label38
        '
        Me.Label38.Height = 0.188!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 2.75!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label38.Text = "Right Side"
        Me.Label38.Top = 12.863!
        Me.Label38.Width = 0.75!
        '
        'Label40
        '
        Me.Label40.Height = 0.188!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 2.0!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label40.Text = "Left Side"
        Me.Label40.Top = 12.863!
        Me.Label40.Width = 0.75!
        '
        'Label46
        '
        Me.Label46.Height = 0.188!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 3.5!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label46.Text = "Left Side"
        Me.Label46.Top = 12.863!
        Me.Label46.Width = 0.75!
        '
        'Label53
        '
        Me.Label53.Height = 0.188!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 4.25!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label53.Text = "Right Side"
        Me.Label53.Top = 12.863!
        Me.Label53.Width = 0.75!
        '
        'Line48
        '
        Me.Line48.Height = 0!
        Me.Line48.Left = 2.0!
        Me.Line48.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line48.LineWeight = 1.0!
        Me.Line48.Name = "Line48"
        Me.Line48.Top = 12.863!
        Me.Line48.Width = 3.0!
        Me.Line48.X1 = 2.0!
        Me.Line48.X2 = 5.0!
        Me.Line48.Y1 = 12.863!
        Me.Line48.Y2 = 12.863!
        '
        'Line47
        '
        Me.Line47.Height = 0!
        Me.Line47.Left = 2.0!
        Me.Line47.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line47.LineWeight = 1.0!
        Me.Line47.Name = "Line47"
        Me.Line47.Top = 13.05!
        Me.Line47.Width = 3.0!
        Me.Line47.X1 = 2.0!
        Me.Line47.X2 = 5.0!
        Me.Line47.Y1 = 13.05!
        Me.Line47.Y2 = 13.05!
        '
        'Line46
        '
        Me.Line46.Height = 0!
        Me.Line46.Left = 2.0!
        Me.Line46.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line46.LineWeight = 1.0!
        Me.Line46.Name = "Line46"
        Me.Line46.Top = 13.238!
        Me.Line46.Width = 3.0!
        Me.Line46.X1 = 2.0!
        Me.Line46.X2 = 5.0!
        Me.Line46.Y1 = 13.238!
        Me.Line46.Y2 = 13.238!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 2.0!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 12.675!
        Me.Line4.Width = 3.0!
        Me.Line4.X1 = 2.0!
        Me.Line4.X2 = 5.0!
        Me.Line4.Y1 = 12.675!
        Me.Line4.Y2 = 12.675!
        '
        'Label61
        '
        Me.Label61.Height = 0.188!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 0!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label61.Text = "Φv_Pz="
        Me.Label61.Top = 13.049!
        Me.Label61.Width = 2.0!
        '
        'Label64
        '
        Me.Label64.Height = 0.188!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 0!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label64.Text = "Fyc="
        Me.Label64.Top = 13.237!
        Me.Label64.Width = 2.0!
        '
        'Label69
        '
        Me.Label69.Height = 0.188!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 5.0!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label69.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label69.Top = 13.428!
        Me.Label69.Width = 2.75!
        '
        'S1S111_020
        '
        Me.S1S111_020.Height = 0.188!
        Me.S1S111_020.HyperLink = Nothing
        Me.S1S111_020.Left = 2.75!
        Me.S1S111_020.Name = "S1S111_020"
        Me.S1S111_020.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S1S111_020.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_020.Top = 13.427!
        Me.S1S111_020.Width = 0.75!
        '
        'S2S111_004
        '
        Me.S2S111_004.Height = 0.188!
        Me.S2S111_004.HyperLink = Nothing
        Me.S2S111_004.Left = 3.5!
        Me.S2S111_004.Name = "S2S111_004"
        Me.S2S111_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S2S111_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_004.Top = 13.427!
        Me.S2S111_004.Width = 0.75!
        '
        'S2S111_020
        '
        Me.S2S111_020.Height = 0.188!
        Me.S2S111_020.HyperLink = Nothing
        Me.S2S111_020.Left = 4.25!
        Me.S2S111_020.Name = "S2S111_020"
        Me.S2S111_020.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S2S111_020.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_020.Top = 13.427!
        Me.S2S111_020.Width = 0.75!
        '
        'Label76
        '
        Me.Label76.Height = 0.188!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label76.Text = "dc="
        Me.Label76.Top = 13.428!
        Me.Label76.Width = 2.0!
        '
        'Label82
        '
        Me.Label82.Height = 0.188!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 5.0!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label82.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label82.Top = 13.618!
        Me.Label82.Width = 2.75!
        '
        'S1S111_021
        '
        Me.S1S111_021.Height = 0.188!
        Me.S1S111_021.HyperLink = Nothing
        Me.S1S111_021.Left = 2.75!
        Me.S1S111_021.Name = "S1S111_021"
        Me.S1S111_021.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S1S111_021.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_021.Top = 13.617!
        Me.S1S111_021.Width = 0.75!
        '
        'S2S111_005
        '
        Me.S2S111_005.Height = 0.188!
        Me.S2S111_005.HyperLink = Nothing
        Me.S2S111_005.Left = 3.5!
        Me.S2S111_005.Name = "S2S111_005"
        Me.S2S111_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S2S111_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_005.Top = 13.617!
        Me.S2S111_005.Width = 0.75!
        '
        'S2S111_021
        '
        Me.S2S111_021.Height = 0.188!
        Me.S2S111_021.HyperLink = Nothing
        Me.S2S111_021.Left = 4.25!
        Me.S2S111_021.Name = "S2S111_021"
        Me.S2S111_021.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S2S111_021.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_021.Top = 13.617!
        Me.S2S111_021.Width = 0.75!
        '
        'Label91
        '
        Me.Label91.Height = 0.188!
        Me.Label91.HyperLink = Nothing
        Me.Label91.Left = 0!
        Me.Label91.Name = "Label91"
        Me.Label91.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label91.Text = "tcw="
        Me.Label91.Top = 13.618!
        Me.Label91.Width = 2.0!
        '
        'Label95
        '
        Me.Label95.Height = 0.188!
        Me.Label95.HyperLink = Nothing
        Me.Label95.Left = 5.0!
        Me.Label95.Name = "Label95"
        Me.Label95.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label95.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label95.Top = 13.811!
        Me.Label95.Width = 2.75!
        '
        'S1S111_022
        '
        Me.S1S111_022.Height = 0.188!
        Me.S1S111_022.HyperLink = Nothing
        Me.S1S111_022.Left = 2.75!
        Me.S1S111_022.Name = "S1S111_022"
        Me.S1S111_022.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S1S111_022.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_022.Top = 13.808!
        Me.S1S111_022.Width = 0.75!
        '
        'S2S111_006
        '
        Me.S2S111_006.Height = 0.188!
        Me.S2S111_006.HyperLink = Nothing
        Me.S2S111_006.Left = 3.5!
        Me.S2S111_006.Name = "S2S111_006"
        Me.S2S111_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S2S111_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_006.Top = 13.808!
        Me.S2S111_006.Width = 0.75!
        '
        'S2S111_022
        '
        Me.S2S111_022.Height = 0.188!
        Me.S2S111_022.HyperLink = Nothing
        Me.S2S111_022.Left = 4.25!
        Me.S2S111_022.Name = "S2S111_022"
        Me.S2S111_022.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S2S111_022.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_022.Top = 13.808!
        Me.S2S111_022.Width = 0.75!
        '
        'Label106
        '
        Me.Label106.Height = 0.188!
        Me.Label106.HyperLink = Nothing
        Me.Label106.Left = 0!
        Me.Label106.Name = "Label106"
        Me.Label106.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label106.Text = "Agc="
        Me.Label106.Top = 13.809!
        Me.Label106.Width = 2.0!
        '
        'Label112
        '
        Me.Label112.Height = 0.188!
        Me.Label112.HyperLink = Nothing
        Me.Label112.Left = 5.0!
        Me.Label112.Name = "Label112"
        Me.Label112.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label112.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Fyc* Agc" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label112.Top = 14.001!
        Me.Label112.Width = 2.75!
        '
        'S1S111_023
        '
        Me.S1S111_023.Height = 0.188!
        Me.S1S111_023.HyperLink = Nothing
        Me.S1S111_023.Left = 2.75!
        Me.S1S111_023.Name = "S1S111_023"
        Me.S1S111_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_023.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_023.Top = 13.998!
        Me.S1S111_023.Width = 0.75!
        '
        'S2S111_007
        '
        Me.S2S111_007.Height = 0.188!
        Me.S2S111_007.HyperLink = Nothing
        Me.S2S111_007.Left = 3.5!
        Me.S2S111_007.Name = "S2S111_007"
        Me.S2S111_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_007.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_007.Top = 13.998!
        Me.S2S111_007.Width = 0.75!
        '
        'S2S111_023
        '
        Me.S2S111_023.Height = 0.188!
        Me.S2S111_023.HyperLink = Nothing
        Me.S2S111_023.Left = 4.25!
        Me.S2S111_023.Name = "S2S111_023"
        Me.S2S111_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_023.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_023.Top = 13.998!
        Me.S2S111_023.Width = 0.75!
        '
        'Label123
        '
        Me.Label123.Height = 0.188!
        Me.Label123.HyperLink = Nothing
        Me.Label123.Left = 0!
        Me.Label123.Name = "Label123"
        Me.Label123.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label123.Text = "Pc="
        Me.Label123.Top = 13.999!
        Me.Label123.Width = 2.0!
        '
        'Label128
        '
        Me.Label128.Height = 0.2!
        Me.Label128.HyperLink = Nothing
        Me.Label128.Left = 5.0!
        Me.Label128.Name = "Label128"
        Me.Label128.Style = "font-size: 6pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label128.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Max Axial Force of column per Omega Combo" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                    (SST_LC31-34)" &
    " From ETABS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label128.Top = 14.191!
        Me.Label128.Width = 2.75!
        '
        'S1S111_024
        '
        Me.S1S111_024.Height = 0.188!
        Me.S1S111_024.HyperLink = Nothing
        Me.S1S111_024.Left = 2.75!
        Me.S1S111_024.Name = "S1S111_024"
        Me.S1S111_024.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S1S111_024.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_024.Top = 14.188!
        Me.S1S111_024.Width = 0.75!
        '
        'S2S111_008
        '
        Me.S2S111_008.Height = 0.188!
        Me.S2S111_008.HyperLink = Nothing
        Me.S2S111_008.Left = 3.5!
        Me.S2S111_008.Name = "S2S111_008"
        Me.S2S111_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S2S111_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_008.Top = 14.188!
        Me.S2S111_008.Width = 0.75!
        '
        'S2S111_024
        '
        Me.S2S111_024.Height = 0.188!
        Me.S2S111_024.HyperLink = Nothing
        Me.S2S111_024.Left = 4.25!
        Me.S2S111_024.Name = "S2S111_024"
        Me.S2S111_024.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S2S111_024.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_024.Top = 14.188!
        Me.S2S111_024.Width = 0.75!
        '
        'Label136
        '
        Me.Label136.Height = 0.188!
        Me.Label136.HyperLink = Nothing
        Me.Label136.Left = 0!
        Me.Label136.Name = "Label136"
        Me.Label136.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label136.Text = "Pu="
        Me.Label136.Top = 14.189!
        Me.Label136.Width = 2.0!
        '
        'Label140
        '
        Me.Label140.Height = 0.188!
        Me.Label140.HyperLink = Nothing
        Me.Label140.Left = 5.0!
        Me.Label140.Name = "Label140"
        Me.Label140.Style = "font-size: 6pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label140.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φv_pz* if (Pu < 0.4* Pc, 0.6* Fy_col* dc* tcw," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                      0.6*" &
    " Fy_col* dc* tcw* (1.4 - Pu/Pc))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label140.Top = 14.379!
        Me.Label140.Width = 2.75!
        '
        'S1S111_025
        '
        Me.S1S111_025.Height = 0.188!
        Me.S1S111_025.HyperLink = Nothing
        Me.S1S111_025.Left = 2.75!
        Me.S1S111_025.Name = "S1S111_025"
        Me.S1S111_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_025.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_025.Top = 14.377!
        Me.S1S111_025.Width = 0.75!
        '
        'S2S111_009
        '
        Me.S2S111_009.Height = 0.188!
        Me.S2S111_009.HyperLink = Nothing
        Me.S2S111_009.Left = 3.5!
        Me.S2S111_009.Name = "S2S111_009"
        Me.S2S111_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_009.Top = 14.377!
        Me.S2S111_009.Width = 0.75!
        '
        'S2S111_025
        '
        Me.S2S111_025.Height = 0.188!
        Me.S2S111_025.HyperLink = Nothing
        Me.S2S111_025.Left = 4.25!
        Me.S2S111_025.Name = "S2S111_025"
        Me.S2S111_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_025.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_025.Top = 14.377!
        Me.S2S111_025.Width = 0.75!
        '
        'Label156
        '
        Me.Label156.Height = 0.188!
        Me.Label156.HyperLink = Nothing
        Me.Label156.Left = 5.0!
        Me.Label156.Name = "Label156"
        Me.Label156.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label156.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Column Shear (= 0 , Before Analysis)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label156.Top = 14.57!
        Me.Label156.Width = 2.75!
        '
        'S2S111_010
        '
        Me.S2S111_010.Height = 0.188!
        Me.S2S111_010.HyperLink = Nothing
        Me.S2S111_010.Left = 3.5!
        Me.S2S111_010.Name = "S2S111_010"
        Me.S2S111_010.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S2S111_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_010.Top = 14.567!
        Me.S2S111_010.Width = 0.75!
        '
        'S2S111_026
        '
        Me.S2S111_026.Height = 0.188!
        Me.S2S111_026.HyperLink = Nothing
        Me.S2S111_026.Left = 4.25!
        Me.S2S111_026.Name = "S2S111_026"
        Me.S2S111_026.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S2S111_026.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_026.Top = 14.567!
        Me.S2S111_026.Width = 0.75!
        '
        'Label167
        '
        Me.Label167.Height = 0.188!
        Me.Label167.HyperLink = Nothing
        Me.Label167.Left = 0!
        Me.Label167.Name = "Label167"
        Me.Label167.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label167.Text = "Vu_c="
        Me.Label167.Top = 14.568!
        Me.Label167.Width = 2.0!
        '
        'Label173
        '
        Me.Label173.Height = 0.188!
        Me.Label173.HyperLink = Nothing
        Me.Label173.Left = 5.0!
        Me.Label173.Name = "Label173"
        Me.Label173.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label173.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if(R=3,Py_link, Pr_link)"
        Me.Label173.Top = 14.761!
        Me.Label173.Width = 2.75!
        '
        'S1S111_027
        '
        Me.S1S111_027.Height = 0.188!
        Me.S1S111_027.HyperLink = Nothing
        Me.S1S111_027.Left = 2.75!
        Me.S1S111_027.Name = "S1S111_027"
        Me.S1S111_027.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S1S111_027.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_027.Top = 14.758!
        Me.S1S111_027.Width = 0.75!
        '
        'S2S111_011
        '
        Me.S2S111_011.Height = 0.188!
        Me.S2S111_011.HyperLink = Nothing
        Me.S2S111_011.Left = 3.5!
        Me.S2S111_011.Name = "S2S111_011"
        Me.S2S111_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S2S111_011.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_011.Top = 14.758!
        Me.S2S111_011.Width = 0.75!
        '
        'S2S111_027
        '
        Me.S2S111_027.Height = 0.188!
        Me.S2S111_027.HyperLink = Nothing
        Me.S2S111_027.Left = 4.25!
        Me.S2S111_027.Name = "S2S111_027"
        Me.S2S111_027.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S2S111_027.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_027.Top = 14.758!
        Me.S2S111_027.Width = 0.75!
        '
        'Label177
        '
        Me.Label177.Height = 0.188!
        Me.Label177.HyperLink = Nothing
        Me.Label177.Left = 0.0000002384186!
        Me.Label177.Name = "Label177"
        Me.Label177.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label177.Text = "Pcap_link="
        Me.Label177.Top = 14.759!
        Me.Label177.Width = 2.0!
        '
        'Label179
        '
        Me.Label179.Height = 0.188!
        Me.Label179.HyperLink = Nothing
        Me.Label179.Left = 0!
        Me.Label179.Name = "Label179"
        Me.Label179.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label179.Text = "ΦRn_PZ="
        Me.Label179.Top = 14.378!
        Me.Label179.Width = 2.0!
        '
        'S1S111_028
        '
        Me.S1S111_028.Height = 0.188!
        Me.S1S111_028.HyperLink = Nothing
        Me.S1S111_028.Left = 2.75!
        Me.S1S111_028.Name = "S1S111_028"
        Me.S1S111_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_028.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_028.Top = 14.949!
        Me.S1S111_028.Width = 0.75!
        '
        'S2S111_012
        '
        Me.S2S111_012.Height = 0.188!
        Me.S2S111_012.HyperLink = Nothing
        Me.S2S111_012.Left = 3.5!
        Me.S2S111_012.Name = "S2S111_012"
        Me.S2S111_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_012.Top = 14.949!
        Me.S2S111_012.Width = 0.75!
        '
        'S2S111_028
        '
        Me.S2S111_028.Height = 0.188!
        Me.S2S111_028.HyperLink = Nothing
        Me.S2S111_028.Left = 4.25!
        Me.S2S111_028.Name = "S2S111_028"
        Me.S2S111_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_028.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_028.Top = 14.949!
        Me.S2S111_028.Width = 0.75!
        '
        'Label184
        '
        Me.Label184.Height = 0.188!
        Me.Label184.HyperLink = Nothing
        Me.Label184.Left = 0.0000002384186!
        Me.Label184.Name = "Label184"
        Me.Label184.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label184.Text = "Ru="
        Me.Label184.Top = 14.95!
        Me.Label184.Width = 2.0!
        '
        'Label185
        '
        Me.Label185.Height = 0.188!
        Me.Label185.HyperLink = Nothing
        Me.Label185.Left = 5.0!
        Me.Label185.Name = "Label185"
        Me.Label185.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label185.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pcap_link + Vu_c" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label185.Top = 14.95!
        Me.Label185.Width = 2.75!
        '
        'S1S111_029
        '
        Me.S1S111_029.Height = 0.188!
        Me.S1S111_029.HyperLink = Nothing
        Me.S1S111_029.Left = 2.75!
        Me.S1S111_029.Name = "S1S111_029"
        Me.S1S111_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S1S111_029.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S1S111_029.Top = 15.138!
        Me.S1S111_029.Width = 0.75!
        '
        'S2S111_013
        '
        Me.S2S111_013.Height = 0.188!
        Me.S2S111_013.HyperLink = Nothing
        Me.S2S111_013.Left = 3.5!
        Me.S2S111_013.Name = "S2S111_013"
        Me.S2S111_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_013.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_013.Top = 15.138!
        Me.S2S111_013.Width = 0.75!
        '
        'S2S111_029
        '
        Me.S2S111_029.Height = 0.188!
        Me.S2S111_029.HyperLink = Nothing
        Me.S2S111_029.Left = 4.25!
        Me.S2S111_029.Name = "S2S111_029"
        Me.S2S111_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_029.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_029.Top = 15.138!
        Me.S2S111_029.Width = 0.75!
        '
        'Label190
        '
        Me.Label190.Height = 0.188!
        Me.Label190.HyperLink = Nothing
        Me.Label190.Left = 0.0000004768372!
        Me.Label190.Name = "Label190"
        Me.Label190.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label190.Text = "SUM_PZ="
        Me.Label190.Top = 15.139!
        Me.Label190.Width = 2.0!
        '
        'Label191
        '
        Me.Label191.Height = 0.188!
        Me.Label191.HyperLink = Nothing
        Me.Label191.Left = 5.0!
        Me.Label191.Name = "Label191"
        Me.Label191.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label191.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pcap_link/ ΦRn_PZ" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label191.Top = 15.139!
        Me.Label191.Width = 2.75!
        '
        'Label196
        '
        Me.Label196.Height = 0.188!
        Me.Label196.HyperLink = Nothing
        Me.Label196.Left = 0.0000007152557!
        Me.Label196.Name = "Label196"
        Me.Label196.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label196.Text = "Total_SUM_PZ="
        Me.Label196.Top = 15.328!
        Me.Label196.Width = 2.0!
        '
        'Label202
        '
        Me.Label202.Height = 0.188!
        Me.Label202.HyperLink = Nothing
        Me.Label202.Left = 0.0000009536741!
        Me.Label202.Name = "Label202"
        Me.Label202.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label202.Text = "SUM_PZ_geometry="
        Me.Label202.Top = 15.516!
        Me.Label202.Width = 2.0!
        '
        'Label203
        '
        Me.Label203.Height = 0.188!
        Me.Label203.HyperLink = Nothing
        Me.Label203.Left = 5.0!
        Me.Label203.Name = "Label203"
        Me.Label203.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label203.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=[(dz+wz)/90] / tcw, N/A for R=3" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label203.Top = 15.516!
        Me.Label203.Width = 2.75!
        '
        'Label208
        '
        Me.Label208.Height = 0.188!
        Me.Label208.HyperLink = Nothing
        Me.Label208.Left = 0.000001192093!
        Me.Label208.Name = "Label208"
        Me.Label208.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label208.Text = "Final_SUM_PZ="
        Me.Label208.Top = 15.705!
        Me.Label208.Width = 2.0!
        '
        'Label209
        '
        Me.Label209.Height = 0.188!
        Me.Label209.HyperLink = Nothing
        Me.Label209.Left = 5.0!
        Me.Label209.Name = "Label209"
        Me.Label209.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle"
        Me.Label209.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Ok, if SUM_PZ <= DCR_PZ_Allowable"
        Me.Label209.Top = 15.892!
        Me.Label209.Width = 2.75!
        '
        'Label210
        '
        Me.Label210.Height = 0.1875!
        Me.Label210.HyperLink = Nothing
        Me.Label210.Left = 0.8130001!
        Me.Label210.Name = "Label210"
        Me.Label210.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label210.Text = "Adequate ="
        Me.Label210.Top = 15.894!
        Me.Label210.Width = 1.187!
        '
        'Line20
        '
        Me.Line20.Height = 0!
        Me.Line20.Left = 2.0!
        Me.Line20.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line20.LineWeight = 1.0!
        Me.Line20.Name = "Line20"
        Me.Line20.Top = 15.894!
        Me.Line20.Width = 3.0!
        Me.Line20.X1 = 2.0!
        Me.Line20.X2 = 5.0!
        Me.Line20.Y1 = 15.894!
        Me.Line20.Y2 = 15.894!
        '
        'Line49
        '
        Me.Line49.Height = 3.250999!
        Me.Line49.Left = 2.0!
        Me.Line49.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line49.LineWeight = 1.0!
        Me.Line49.Name = "Line49"
        Me.Line49.Top = 12.675!
        Me.Line49.Width = 0!
        Me.Line49.X1 = 2.0!
        Me.Line49.X2 = 2.0!
        Me.Line49.Y1 = 12.675!
        Me.Line49.Y2 = 15.926!
        '
        'S2S111_016
        '
        Me.S2S111_016.Height = 0.188!
        Me.S2S111_016.HyperLink = Nothing
        Me.S2S111_016.Left = 3.5!
        Me.S2S111_016.Name = "S2S111_016"
        Me.S2S111_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_016.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_016.Top = 15.704!
        Me.S2S111_016.Width = 1.5!
        '
        'S2S111_015
        '
        Me.S2S111_015.Height = 0.188!
        Me.S2S111_015.HyperLink = Nothing
        Me.S2S111_015.Left = 3.5!
        Me.S2S111_015.Name = "S2S111_015"
        Me.S2S111_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_015.Top = 15.516!
        Me.S2S111_015.Width = 1.5!
        '
        'S2S111_014
        '
        Me.S2S111_014.Height = 0.188!
        Me.S2S111_014.HyperLink = Nothing
        Me.S2S111_014.Left = 3.5!
        Me.S2S111_014.Name = "S2S111_014"
        Me.S2S111_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S2S111_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S2S111_014.Top = 15.326!
        Me.S2S111_014.Width = 1.5!
        '
        'Label199
        '
        Me.Label199.Height = 0.1875!
        Me.Label199.HyperLink = Nothing
        Me.Label199.Left = 0.813!
        Me.Label199.Name = "Label199"
        Me.Label199.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: right; vertical-al" &
    "ign: middle"
        Me.Label199.Text = "DCR Summary:"
        Me.Label199.Top = 16.269!
        Me.Label199.Width = 1.187!
        '
        'SUM_006
        '
        Me.SUM_006.Height = 0.188!
        Me.SUM_006.HyperLink = Nothing
        Me.SUM_006.Left = 4.875!
        Me.SUM_006.Name = "SUM_006"
        Me.SUM_006.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_006.Top = 16.457!
        Me.SUM_006.Width = 0.75!
        '
        'SUM_001
        '
        Me.SUM_001.Height = 0.188!
        Me.SUM_001.HyperLink = Nothing
        Me.SUM_001.Left = 4.125!
        Me.SUM_001.Name = "SUM_001"
        Me.SUM_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_001.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_001.Top = 16.457!
        Me.SUM_001.Width = 0.75!
        '
        'Label213
        '
        Me.Label213.Height = 0.188!
        Me.Label213.HyperLink = Nothing
        Me.Label213.Left = 3.0!
        Me.Label213.Name = "Label213"
        Me.Label213.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label213.Text = "Initial tbf Check="
        Me.Label213.Top = 16.457!
        Me.Label213.Width = 1.125!
        '
        'Label215
        '
        Me.Label215.Height = 0.188!
        Me.Label215.HyperLink = Nothing
        Me.Label215.Left = 2.0!
        Me.Label215.Name = "Label215"
        Me.Label215.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label215.Text = "Beam"
        Me.Label215.Top = 16.456!
        Me.Label215.Width = 0.9999999!
        '
        'Label218
        '
        Me.Label218.Height = 0.188!
        Me.Label218.HyperLink = Nothing
        Me.Label218.Left = 3.0!
        Me.Label218.Name = "Label218"
        Me.Label218.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label218.Text = "Check"
        Me.Label218.Top = 16.269!
        Me.Label218.Width = 1.0!
        '
        'Label219
        '
        Me.Label219.Height = 0.188!
        Me.Label219.HyperLink = Nothing
        Me.Label219.Left = 2.0!
        Me.Label219.Name = "Label219"
        Me.Label219.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label219.Text = "Component"
        Me.Label219.Top = 16.269!
        Me.Label219.Width = 0.9999999!
        '
        'Label220
        '
        Me.Label220.Height = 0.188!
        Me.Label220.HyperLink = Nothing
        Me.Label220.Left = 4.125!
        Me.Label220.Name = "Label220"
        Me.Label220.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label220.Text = "DCR"
        Me.Label220.Top = 16.269!
        Me.Label220.Width = 0.75!
        '
        'Label221
        '
        Me.Label221.Height = 0.188!
        Me.Label221.HyperLink = Nothing
        Me.Label221.Left = 4.875!
        Me.Label221.Name = "Label221"
        Me.Label221.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label221.Text = "Limit"
        Me.Label221.Top = 16.269!
        Me.Label221.Width = 0.75!
        '
        'Label224
        '
        Me.Label224.Height = 0.188!
        Me.Label224.HyperLink = Nothing
        Me.Label224.Left = 2.0!
        Me.Label224.Name = "Label224"
        Me.Label224.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label224.Text = "Link"
        Me.Label224.Top = 16.834!
        Me.Label224.Width = 0.9999999!
        '
        'Label226
        '
        Me.Label226.Height = 0.188!
        Me.Label226.HyperLink = Nothing
        Me.Label226.Left = 3.0!
        Me.Label226.Name = "Label226"
        Me.Label226.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label226.Text = "Inittial Lyield Check="
        Me.Label226.Top = 16.833!
        Me.Label226.Width = 1.125!
        '
        'SUM_008
        '
        Me.SUM_008.Height = 0.188!
        Me.SUM_008.HyperLink = Nothing
        Me.SUM_008.Left = 4.875!
        Me.SUM_008.Name = "SUM_008"
        Me.SUM_008.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_008.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_008.Top = 16.833!
        Me.SUM_008.Width = 0.75!
        '
        'Label230
        '
        Me.Label230.Height = 0.379!
        Me.Label230.HyperLink = Nothing
        Me.Label230.Left = 2.0!
        Me.Label230.Name = "Label230"
        Me.Label230.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.Label230.Text = "Column"
        Me.Label230.Top = 17.024!
        Me.Label230.Width = 0.9999999!
        '
        'Label232
        '
        Me.Label232.Height = 0.188!
        Me.Label232.HyperLink = Nothing
        Me.Label232.Left = 3.0!
        Me.Label232.Name = "Label232"
        Me.Label232.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label232.Text = "Pz I_End column="
        Me.Label232.Top = 17.023!
        Me.Label232.Width = 1.125!
        '
        'SUM_009
        '
        Me.SUM_009.Height = 0.188!
        Me.SUM_009.HyperLink = Nothing
        Me.SUM_009.Left = 4.875!
        Me.SUM_009.Name = "SUM_009"
        Me.SUM_009.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_009.Top = 17.023!
        Me.SUM_009.Width = 0.75!
        '
        'Label238
        '
        Me.Label238.Height = 0.188!
        Me.Label238.HyperLink = Nothing
        Me.Label238.Left = 3.0!
        Me.Label238.Name = "Label238"
        Me.Label238.Style = "background-color: Moccasin; color: Black; font-size: 8.25pt; font-weight: normal;" &
    " text-align: right; vertical-align: middle; ddo-char-set: 1"
        Me.Label238.Text = "Pz J_End column="
        Me.Label238.Top = 17.214!
        Me.Label238.Width = 1.125!
        '
        'SUM_005
        '
        Me.SUM_005.Height = 0.188!
        Me.SUM_005.HyperLink = Nothing
        Me.SUM_005.Left = 4.125!
        Me.SUM_005.Name = "SUM_005"
        Me.SUM_005.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_005.Top = 17.214!
        Me.SUM_005.Width = 0.75!
        '
        'SUM_010
        '
        Me.SUM_010.Height = 0.188!
        Me.SUM_010.HyperLink = Nothing
        Me.SUM_010.Left = 4.875!
        Me.SUM_010.Name = "SUM_010"
        Me.SUM_010.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.SUM_010.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_010.Top = 17.214!
        Me.SUM_010.Width = 0.75!
        '
        'SUM_011
        '
        Me.SUM_011.Height = 0.188!
        Me.SUM_011.HyperLink = Nothing
        Me.SUM_011.Left = 5.624!
        Me.SUM_011.Name = "SUM_011"
        Me.SUM_011.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_011.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_011.Top = 16.454!
        Me.SUM_011.Width = 0.75!
        '
        'Label225
        '
        Me.Label225.Height = 0.188!
        Me.Label225.HyperLink = Nothing
        Me.Label225.Left = 5.624!
        Me.Label225.Name = "Label225"
        Me.Label225.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle"
        Me.Label225.Text = "Result"
        Me.Label225.Top = 16.266!
        Me.Label225.Width = 0.75!
        '
        'SUM_014
        '
        Me.SUM_014.Height = 0.188!
        Me.SUM_014.HyperLink = Nothing
        Me.SUM_014.Left = 5.624!
        Me.SUM_014.Name = "SUM_014"
        Me.SUM_014.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_014.Top = 17.02!
        Me.SUM_014.Width = 0.75!
        '
        'SUM_015
        '
        Me.SUM_015.Height = 0.188!
        Me.SUM_015.HyperLink = Nothing
        Me.SUM_015.Left = 5.624!
        Me.SUM_015.Name = "SUM_015"
        Me.SUM_015.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.SUM_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.SUM_015.Top = 17.211!
        Me.SUM_015.Width = 0.75!
        '
        'Line30
        '
        Me.Line30.Height = 0.0009994507!
        Me.Line30.Left = 2.0!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 16.268!
        Me.Line30.Width = 4.374!
        Me.Line30.X1 = 2.0!
        Me.Line30.X2 = 6.374!
        Me.Line30.Y1 = 16.268!
        Me.Line30.Y2 = 16.269!
        '
        'Line2
        '
        Me.Line2.Height = 0.001001358!
        Me.Line2.Left = 2.0!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 16.456!
        Me.Line2.Width = 4.374001!
        Me.Line2.X1 = 2.0!
        Me.Line2.X2 = 6.374001!
        Me.Line2.Y1 = 16.456!
        Me.Line2.Y2 = 16.457!
        '
        'Line21
        '
        Me.Line21.Height = 0.001001358!
        Me.Line21.Left = 2.0!
        Me.Line21.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line21.LineWeight = 1.0!
        Me.Line21.Name = "Line21"
        Me.Line21.Top = 16.644!
        Me.Line21.Width = 4.374001!
        Me.Line21.X1 = 2.0!
        Me.Line21.X2 = 6.374001!
        Me.Line21.Y1 = 16.644!
        Me.Line21.Y2 = 16.645!
        '
        'Line24
        '
        Me.Line24.Height = 0.0009994507!
        Me.Line24.Left = 2.0!
        Me.Line24.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line24.LineWeight = 1.0!
        Me.Line24.Name = "Line24"
        Me.Line24.Top = 17.018!
        Me.Line24.Width = 4.374001!
        Me.Line24.X1 = 2.0!
        Me.Line24.X2 = 6.374001!
        Me.Line24.Y1 = 17.018!
        Me.Line24.Y2 = 17.019!
        '
        'Line25
        '
        Me.Line25.Height = 0.0009994507!
        Me.Line25.Left = 2.0!
        Me.Line25.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line25.LineWeight = 1.0!
        Me.Line25.Name = "Line25"
        Me.Line25.Top = 17.393!
        Me.Line25.Width = 4.374001!
        Me.Line25.X1 = 2.0!
        Me.Line25.X2 = 6.374001!
        Me.Line25.Y1 = 17.393!
        Me.Line25.Y2 = 17.394!
        '
        'Line26
        '
        Me.Line26.Height = 0!
        Me.Line26.Left = 3.0!
        Me.Line26.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line26.LineWeight = 1.0!
        Me.Line26.Name = "Line26"
        Me.Line26.Top = 17.214!
        Me.Line26.Width = 3.374001!
        Me.Line26.X1 = 3.0!
        Me.Line26.X2 = 6.374001!
        Me.Line26.Y1 = 17.214!
        Me.Line26.Y2 = 17.214!
        '
        'Line27
        '
        Me.Line27.Height = 1.125999!
        Me.Line27.Left = 2.0!
        Me.Line27.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line27.LineWeight = 1.0!
        Me.Line27.Name = "Line27"
        Me.Line27.Top = 16.268!
        Me.Line27.Width = 0!
        Me.Line27.X1 = 2.0!
        Me.Line27.X2 = 2.0!
        Me.Line27.Y1 = 16.268!
        Me.Line27.Y2 = 17.394!
        '
        'Line28
        '
        Me.Line28.Height = 1.125999!
        Me.Line28.Left = 3.0!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 16.277!
        Me.Line28.Width = 0!
        Me.Line28.X1 = 3.0!
        Me.Line28.X2 = 3.0!
        Me.Line28.Y1 = 16.277!
        Me.Line28.Y2 = 17.403!
        '
        'Line29
        '
        Me.Line29.Height = 1.125999!
        Me.Line29.Left = 4.125!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 16.277!
        Me.Line29.Width = 0!
        Me.Line29.X1 = 4.125!
        Me.Line29.X2 = 4.125!
        Me.Line29.Y1 = 16.277!
        Me.Line29.Y2 = 17.403!
        '
        'Line31
        '
        Me.Line31.Height = 1.125999!
        Me.Line31.Left = 4.875!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 16.268!
        Me.Line31.Width = 0!
        Me.Line31.X1 = 4.875!
        Me.Line31.X2 = 4.875!
        Me.Line31.Y1 = 16.268!
        Me.Line31.Y2 = 17.394!
        '
        'Line32
        '
        Me.Line32.Height = 1.125999!
        Me.Line32.Left = 5.625!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 16.277!
        Me.Line32.Width = 0!
        Me.Line32.X1 = 5.625!
        Me.Line32.X2 = 5.625!
        Me.Line32.Y1 = 16.277!
        Me.Line32.Y2 = 17.403!
        '
        'Line33
        '
        Me.Line33.Height = 1.125999!
        Me.Line33.Left = 6.374!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 16.277!
        Me.Line33.Width = 0!
        Me.Line33.X1 = 6.374!
        Me.Line33.X2 = 6.374!
        Me.Line33.Y1 = 16.277!
        Me.Line33.Y2 = 17.403!
        '
        'Line17
        '
        Me.Line17.Height = 0!
        Me.Line17.Left = 2.0!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 15.327!
        Me.Line17.Width = 3.0!
        Me.Line17.X1 = 2.0!
        Me.Line17.X2 = 5.0!
        Me.Line17.Y1 = 15.327!
        Me.Line17.Y2 = 15.327!
        '
        'Line18
        '
        Me.Line18.Height = 0!
        Me.Line18.Left = 2.0!
        Me.Line18.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line18.LineWeight = 1.0!
        Me.Line18.Name = "Line18"
        Me.Line18.Top = 15.516!
        Me.Line18.Width = 3.0!
        Me.Line18.X1 = 2.0!
        Me.Line18.X2 = 5.0!
        Me.Line18.Y1 = 15.516!
        Me.Line18.Y2 = 15.516!
        '
        'Line19
        '
        Me.Line19.Height = 0!
        Me.Line19.Left = 2.0!
        Me.Line19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line19.LineWeight = 1.0!
        Me.Line19.Name = "Line19"
        Me.Line19.Top = 15.704!
        Me.Line19.Width = 3.0!
        Me.Line19.X1 = 2.0!
        Me.Line19.X2 = 5.0!
        Me.Line19.Y1 = 15.704!
        Me.Line19.Y2 = 15.704!
        '
        'Line1
        '
        Me.Line1.Height = 3.219!
        Me.Line1.Left = 3.5!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 12.675!
        Me.Line1.Width = 0!
        Me.Line1.X1 = 3.5!
        Me.Line1.X2 = 3.5!
        Me.Line1.Y1 = 12.675!
        Me.Line1.Y2 = 15.894!
        '
        'Line13
        '
        Me.Line13.Height = 2.464!
        Me.Line13.Left = 2.75!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 12.863!
        Me.Line13.Width = 0!
        Me.Line13.X1 = 2.75!
        Me.Line13.X2 = 2.75!
        Me.Line13.Y1 = 12.863!
        Me.Line13.Y2 = 15.327!
        '
        'Line14
        '
        Me.Line14.Height = 2.464!
        Me.Line14.Left = 4.25!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 12.863!
        Me.Line14.Width = 0!
        Me.Line14.X1 = 4.25!
        Me.Line14.X2 = 4.25!
        Me.Line14.Y1 = 12.863!
        Me.Line14.Y2 = 15.327!
        '
        'Line3
        '
        Me.Line3.Height = 3.403001!
        Me.Line3.Left = 5.0!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 12.676!
        Me.Line3.Width = 0!
        Me.Line3.X1 = 5.0!
        Me.Line3.X2 = 5.0!
        Me.Line3.Y1 = 12.676!
        Me.Line3.Y2 = 16.079!
        '
        'Line16
        '
        Me.Line16.Height = 0!
        Me.Line16.Left = 2.0!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 15.138!
        Me.Line16.Width = 3.0!
        Me.Line16.X1 = 2.0!
        Me.Line16.X2 = 5.0!
        Me.Line16.Y1 = 15.138!
        Me.Line16.Y2 = 15.138!
        '
        'Line15
        '
        Me.Line15.Height = 0!
        Me.Line15.Left = 2.0!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 14.949!
        Me.Line15.Width = 3.0!
        Me.Line15.X1 = 2.0!
        Me.Line15.X2 = 5.0!
        Me.Line15.Y1 = 14.949!
        Me.Line15.Y2 = 14.949!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 2.0!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 14.568!
        Me.Line11.Width = 3.0!
        Me.Line11.X1 = 2.0!
        Me.Line11.X2 = 5.0!
        Me.Line11.Y1 = 14.568!
        Me.Line11.Y2 = 14.568!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 2.0!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 13.427!
        Me.Line5.Width = 3.0!
        Me.Line5.X1 = 2.0!
        Me.Line5.X2 = 5.0!
        Me.Line5.Y1 = 13.427!
        Me.Line5.Y2 = 13.427!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 2.0!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 13.618!
        Me.Line6.Width = 3.0!
        Me.Line6.X1 = 2.0!
        Me.Line6.X2 = 5.0!
        Me.Line6.Y1 = 13.618!
        Me.Line6.Y2 = 13.618!
        '
        'Line7
        '
        Me.Line7.Height = 0!
        Me.Line7.Left = 2.0!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 13.808!
        Me.Line7.Width = 3.0!
        Me.Line7.X1 = 2.0!
        Me.Line7.X2 = 5.0!
        Me.Line7.Y1 = 13.808!
        Me.Line7.Y2 = 13.808!
        '
        'Line8
        '
        Me.Line8.Height = 0!
        Me.Line8.Left = 2.0!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 13.999!
        Me.Line8.Width = 3.0!
        Me.Line8.X1 = 2.0!
        Me.Line8.X2 = 5.0!
        Me.Line8.Y1 = 13.999!
        Me.Line8.Y2 = 13.999!
        '
        'Line9
        '
        Me.Line9.Height = 0!
        Me.Line9.Left = 2.0!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 14.189!
        Me.Line9.Width = 3.0!
        Me.Line9.X1 = 2.0!
        Me.Line9.X2 = 5.0!
        Me.Line9.Y1 = 14.189!
        Me.Line9.Y2 = 14.189!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 2.0!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 14.758!
        Me.Line12.Width = 3.0!
        Me.Line12.X1 = 2.0!
        Me.Line12.X2 = 5.0!
        Me.Line12.Y1 = 14.758!
        Me.Line12.Y2 = 14.758!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 2.0!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 14.379!
        Me.Line10.Width = 3.0!
        Me.Line10.X1 = 2.0!
        Me.Line10.X2 = 5.0!
        Me.Line10.Y1 = 14.379!
        Me.Line10.Y2 = 14.379!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label28.Text = "k1="
        Me.Label28.Top = 9.093!
        Me.Label28.Width = 2.5!
        '
        'S19_005
        '
        Me.S19_005.Height = 0.1875!
        Me.S19_005.HyperLink = Nothing
        Me.S19_005.Left = 2.5!
        Me.S19_005.Name = "S19_005"
        Me.S19_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_005.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_005.Top = 9.093!
        Me.S19_005.Width = 1.0!
        '
        'Label44
        '
        Me.Label44.Height = 0.1875!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 0!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label44.Text = "B="
        Me.Label44.Top = 9.280003!
        Me.Label44.Width = 2.5!
        '
        'S19_006
        '
        Me.S19_006.Height = 0.1875!
        Me.S19_006.HyperLink = Nothing
        Me.S19_006.Left = 2.5!
        Me.S19_006.Name = "S19_006"
        Me.S19_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_006.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_006.Top = 9.280003!
        Me.S19_006.Width = 1.0!
        '
        'Label51
        '
        Me.Label51.Height = 0.188!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 3.5!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label51.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label51.Top = 9.092001!
        Me.Label51.Width = 4.25!
        '
        'Label57
        '
        Me.Label57.Height = 0.188!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 3.5!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label57.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= bolt washer diameter"
        Me.Label57.Top = 9.280001!
        Me.Label57.Width = 4.25!
        '
        'S19_007
        '
        Me.S19_007.Height = 0.1875!
        Me.S19_007.HyperLink = Nothing
        Me.S19_007.Left = 2.5!
        Me.S19_007.Name = "S19_007"
        Me.S19_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_007.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_007.Top = 9.468!
        Me.S19_007.Width = 1.0!
        '
        'Label66
        '
        Me.Label66.Height = 0.188!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 3.5!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label66.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= min. edge distance"
        Me.Label66.Top = 9.468!
        Me.Label66.Width = 4.25!
        '
        'Label70
        '
        Me.Label70.Height = 0.1875!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 0!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label70.Text = "E="
        Me.Label70.Top = 9.468!
        Me.Label70.Width = 2.5!
        '
        'S19_008
        '
        Me.S19_008.Height = 0.1875!
        Me.S19_008.HyperLink = Nothing
        Me.S19_008.Left = 2.5!
        Me.S19_008.Name = "S19_008"
        Me.S19_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S19_008.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_008.Top = 9.656002!
        Me.S19_008.Width = 1.0!
        '
        'Label75
        '
        Me.Label75.Height = 0.188!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 3.5!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label75.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2*K1  + bolt washer diameter (B)"
        Me.Label75.Top = 9.656002!
        Me.Label75.Width = 4.25!
        '
        'Label79
        '
        Me.Label79.Height = 0.1875!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 0!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label79.Text = "ColGageReq ="
        Me.Label79.Top = 9.656002!
        Me.Label79.Width = 2.5!
        '
        'S19_009
        '
        Me.S19_009.Height = 0.1875!
        Me.S19_009.HyperLink = Nothing
        Me.S19_009.Left = 2.5!
        Me.S19_009.Name = "S19_009"
        Me.S19_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S19_009.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_009.Top = 9.844002!
        Me.S19_009.Width = 1.0!
        '
        'Label87
        '
        Me.Label87.Height = 0.188!
        Me.Label87.HyperLink = Nothing
        Me.Label87.Left = 3.5!
        Me.Label87.Name = "Label87"
        Me.Label87.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label87.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Previous define"
        Me.Label87.Top = 9.844002!
        Me.Label87.Width = 4.25!
        '
        'Label89
        '
        Me.Label89.Height = 0.1875!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 0!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label89.Text = "sflange="
        Me.Label89.Top = 9.844002!
        Me.Label89.Width = 2.5!
        '
        'Label97
        '
        Me.Label97.Height = 0.188!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 3.5!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label97.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2*K1  + 2*E"
        Me.Label97.Top = 10.218!
        Me.Label97.Width = 4.25!
        '
        'Label100
        '
        Me.Label100.Height = 0.1875!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 0!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label100.Text = "ColGageReq_washer ="
        Me.Label100.Top = 10.218!
        Me.Label100.Width = 2.5!
        '
        'Label103
        '
        Me.Label103.Height = 0.1875!
        Me.Label103.HyperLink = Nothing
        Me.Label103.Left = 0.0000002384186!
        Me.Label103.Name = "Label103"
        Me.Label103.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label103.Text = "Clip washers ="
        Me.Label103.Top = 10.405!
        Me.Label103.Width = 2.5!
        '
        'Label109
        '
        Me.Label109.Height = 0.188!
        Me.Label109.HyperLink = Nothing
        Me.Label109.Left = 3.5!
        Me.Label109.Name = "Label109"
        Me.Label109.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label109.Text = " " & Global.Microsoft.VisualBasic.ChrW(9) & "Required if (S_flange + 0.5)<ColGageReq, NG if (S_flange + 0.5)<ColGageReq_wash" &
    "er"
        Me.Label109.Top = 10.405!
        Me.Label109.Width = 4.427001!
        '
        'S19_011
        '
        Me.S19_011.Height = 0.1875!
        Me.S19_011.HyperLink = Nothing
        Me.S19_011.Left = 2.5!
        Me.S19_011.Name = "S19_011"
        Me.S19_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S19_011.Text = "11.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_011.Top = 10.218!
        Me.S19_011.Width = 1.0!
        '
        'Label118
        '
        Me.Label118.Height = 0.188!
        Me.Label118.HyperLink = Nothing
        Me.Label118.Left = 3.5!
        Me.Label118.Name = "Label118"
        Me.Label118.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label118.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label118.Top = 8.904001!
        Me.Label118.Width = 4.25!
        '
        'Label121
        '
        Me.Label121.Height = 0.188!
        Me.Label121.HyperLink = Nothing
        Me.Label121.Left = 3.5!
        Me.Label121.Name = "Label121"
        Me.Label121.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label121.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label121.Top = 8.716002!
        Me.Label121.Width = 4.25!
        '
        'Label125
        '
        Me.Label125.Height = 0.188!
        Me.Label125.HyperLink = Nothing
        Me.Label125.Left = 3.5!
        Me.Label125.Name = "Label125"
        Me.Label125.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label125.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label125.Top = 8.528002!
        Me.Label125.Width = 4.25!
        '
        'Label130
        '
        Me.Label130.Height = 0.188!
        Me.Label130.HyperLink = Nothing
        Me.Label130.Left = 3.5!
        Me.Label130.Name = "Label130"
        Me.Label130.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label130.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label130.Top = 8.186001!
        Me.Label130.Width = 4.25!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 0!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label32.Text = "bcf_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label32.Top = 8.348001!
        Me.Label32.Width = 2.5!
        '
        'S19_014
        '
        Me.S19_014.Height = 0.1875!
        Me.S19_014.HyperLink = Nothing
        Me.S19_014.Left = 2.5!
        Me.S19_014.Name = "S19_014"
        Me.S19_014.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S19_014.Text = "3.500" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S19_014.Top = 8.348001!
        Me.S19_014.Width = 1.0!
        '
        'Label60
        '
        Me.Label60.Height = 0.188!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 3.5!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label60.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value"
        Me.Label60.Top = 8.348001!
        Me.Label60.Width = 4.25!
        '
        'Shape5
        '
        Me.Shape5.Height = 0.1875!
        Me.Shape5.Left = 2.5!
        Me.Shape5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 2.748!
        Me.Shape5.Width = 1.0!
        '
        'Shape4
        '
        Me.Shape4.Height = 0.1875!
        Me.Shape4.Left = 2.5!
        Me.Shape4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape4.Name = "Shape4"
        Me.Shape4.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape4.Top = 6.937997!
        Me.Shape4.Width = 1.0!
        '
        'Shape9
        '
        Me.Shape9.Height = 0.1875!
        Me.Shape9.Left = 2.51!
        Me.Shape9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape9.Name = "Shape9"
        Me.Shape9.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape9.Top = 7.759999!
        Me.Shape9.Width = 1.0!
        '
        'Shape11
        '
        Me.Shape11.Height = 0.1875!
        Me.Shape11.Left = 2.5!
        Me.Shape11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape11.Name = "Shape11"
        Me.Shape11.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape11.Top = 10.406!
        Me.Shape11.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.5!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 10.593!
        Me.Shape1.Width = 1.0!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.1875!
        Me.Shape3.Left = 2.5!
        Me.Shape3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 12.228!
        Me.Shape3.Width = 1.0!
        '
        'Shape10
        '
        Me.Shape10.Height = 0.1875!
        Me.Shape10.Left = 3.5!
        Me.Shape10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape10.Name = "Shape10"
        Me.Shape10.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape10.Top = 15.892!
        Me.Shape10.Width = 1.5!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 2.0!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 15.893!
        Me.Shape2.Width = 1.5!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.1979167!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(6.5!, 0.1979167!)
        Me.PageBreak1.Top = 16.1875!
        Me.PageBreak1.Width = 6.5!
        '
        'Line23
        '
        Me.Line23.Height = 0.0009994507!
        Me.Line23.Left = 2.0!
        Me.Line23.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line23.LineWeight = 1.0!
        Me.Line23.Name = "Line23"
        Me.Line23.Top = 16.826!
        Me.Line23.Width = 4.374001!
        Me.Line23.X1 = 2.0!
        Me.Line23.X2 = 6.374001!
        Me.Line23.Y1 = 16.826!
        Me.Line23.Y2 = 16.827!
        '
        'US_Detail_ILS
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.01!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.SUM_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label212, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label214, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S18_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S12_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S13_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S15_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S15_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S15_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S15_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S15_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S16_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label88, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S16_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label90, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S16_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S16_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label102, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label105, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label108, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label111, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label113, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label114, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label116, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label117, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S17_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label119, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label120, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label122, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label124, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S18_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label126, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label127, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S18_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label129, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label133, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label135, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label137, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label139, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label143, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS110, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label149, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label152, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label155, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label157, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label161, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S110_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label166, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.headerS14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S14_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S14_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S14_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S11_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label91, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label112, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label123, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label128, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label140, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label167, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label173, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label177, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label179, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label184, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label185, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S1S111_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label190, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label202, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label203, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label208, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label209, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label210, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S2S111_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label213, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label215, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label218, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label219, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label220, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label221, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label224, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label226, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label230, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label232, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label238, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label225, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SUM_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label109, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label118, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label121, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label130, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S19_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S12_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label50 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label55 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label62 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label67 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label68 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S13_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label71 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label72 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S15_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S15_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label77 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label78 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S15_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label80 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S15_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S15_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label86 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S16_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label88 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S16_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label90 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S16_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S16_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label94 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label96 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label99 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label101 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label102 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label104 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label105 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label108 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label110 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label111 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label113 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label114 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label116 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label117 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label119 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label120 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S17_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label122 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label124 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S18_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label126 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label127 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S18_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label129 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label131 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label133 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label135 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label137 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label139 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label141 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label143 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label144 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS110 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label146 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label148 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label149 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label151 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label152 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label154 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label155 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label157 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label158 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label160 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label161 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label163 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label164 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label166 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S110_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents headerS14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S14_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S14_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S14_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape4 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S11_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape7 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape8 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S18_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape9 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line49 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label53 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line48 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line47 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line46 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label61 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label69 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label91 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label95 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label106 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label112 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label123 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label128 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label136 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label140 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S1S111_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label156 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label167 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S1S111_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label173 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label177 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label179 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label184 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label185 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label190 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label191 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label196 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line18 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S1S111_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label202 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line19 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label203 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label208 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label209 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label210 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Line20 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S2S111_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape10 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S2S111_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label199 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label212 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label213 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label214 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label215 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label218 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label219 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label220 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label221 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label224 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label226 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label230 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label232 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label238 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents SUM_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label225 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SUM_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line21 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line23 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line24 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line25 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line26 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line27 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S1S111_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S2S111_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S1S111_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label44 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label51 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label70 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label75 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label87 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label89 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label97 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label100 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label103 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label109 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape11 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S19_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label118 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label121 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label125 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label130 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S19_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
End Class
