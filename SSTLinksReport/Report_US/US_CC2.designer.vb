﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class US_CC2
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_CC2))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S343_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S343_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape8 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S341_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape4 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label205 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label206 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label189 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label193 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label196 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label199 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label201 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label336 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label337 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label338 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label340 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line57 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line58 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label125 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label129 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label131 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label134 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label136 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label139 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label141 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label142 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label144 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label146 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label148 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label151 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label153 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label154 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label156 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label158 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label160 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label163 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label164 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label165 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label166 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label191 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label208 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label212 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label216 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label217 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label223 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label213 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label225 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label233 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line63 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line67 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line73 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line70 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line61 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label241 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label243 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line18 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line19 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line20 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S34_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line69 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S341_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line21 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S342_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label53 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line22 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line23 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line24 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line25 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line26 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line27 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label68 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label69 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label71 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label72 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S342_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line34 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label75 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line36 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S343_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label78 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S343_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line35 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line37 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line38 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S343_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S343_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label87 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line39 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line40 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line41 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line42 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label89 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label84 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S343_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line43 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line44 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label92 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line45 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label93 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line46 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label95 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label96 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label97 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S344_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line47 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line48 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line49 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label100 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label101 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S344_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label103 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label104 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S344_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line50 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line51 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line52 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label94 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label245 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label246 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label248 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label252 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label253 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line64 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line66 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label254 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label255 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line76 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label256 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label259 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label260 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S35_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label263 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label264 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label267 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line74 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line77 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line78 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line79 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label106 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line221 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line53 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label107 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line68 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line54 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line62 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape10 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape9 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Line75 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S343_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S343_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label205, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label206, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label189, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label201, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label336, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label337, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label338, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label340, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label129, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label134, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label139, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label142, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label153, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label165, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label166, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label208, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label212, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label216, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label217, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label223, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label213, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label225, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label233, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label241, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label243, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S34_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S341_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S342_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S343_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S343_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S343_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S343_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S343_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S344_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S344_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S344_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label245, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label246, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label248, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label252, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label253, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label254, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label255, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label256, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label259, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label260, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S35_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label263, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label264, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label267, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S343_001, Me.S35_001, Me.S343_006, Me.Shape8, Me.S341_018, Me.S341_021, Me.S341_001, Me.S34_019, Me.Shape1, Me.Shape4, Me.Label205, Me.S342_009, Me.S342_012, Me.S342_004, Me.S341_020, Me.Label206, Me.S34_004, Me.S34_003, Me.Label189, Me.Label193, Me.S34_001, Me.Label196, Me.S34_002, Me.Label199, Me.Label201, Me.Label336, Me.Label337, Me.Label338, Me.Label340, Me.Line57, Me.Line58, Me.Label125, Me.Label129, Me.S34_005, Me.Label131, Me.Label134, Me.Label136, Me.S34_006, Me.Label139, Me.S34_007, Me.Label141, Me.Label142, Me.Label144, Me.S34_008, Me.Label146, Me.S34_020, Me.Label148, Me.S34_009, Me.S34_021, Me.Label151, Me.S34_010, Me.Label153, Me.Label154, Me.S34_022, Me.Label156, Me.S34_011, Me.Label158, Me.S34_023, Me.Label160, Me.S34_012, Me.S34_013, Me.Label163, Me.Label164, Me.Line10, Me.Line11, Me.Line12, Me.Line13, Me.Line14, Me.Label15, Me.Line2, Me.Label165, Me.Label166, Me.S341_003, Me.Label191, Me.S341_004, Me.Label208, Me.S341_005, Me.Label212, Me.Label216, Me.Label217, Me.S341_014, Me.S341_019, Me.S341_015, Me.Label223, Me.Label213, Me.S341_016, Me.Label225, Me.S341_017, Me.Label233, Me.Line63, Me.Line67, Me.Line73, Me.Line70, Me.Line61, Me.Label241, Me.Label243, Me.Line5, Me.Line18, Me.Line19, Me.Line20, Me.Label10, Me.Line3, Me.Line4, Me.Label1, Me.Label4, Me.S34_014, Me.Label18, Me.Label20, Me.S34_015, Me.Label22, Me.Label23, Me.S34_016, Me.Label25, Me.Label26, Me.S34_017, Me.Label28, Me.Label29, Me.S34_018, Me.Label16, Me.Label17, Me.Label21, Me.S341_006, Me.Label27, Me.Label30, Me.S341_009, Me.Label32, Me.Label33, Me.S341_010, Me.Label35, Me.Label36, Me.S341_013, Me.Label38, Me.Label39, Me.Label2, Me.Line1, Me.Line6, Me.Label3, Me.S341_002, Me.Label6, Me.Label7, Me.S341_007, Me.Label9, Me.Label12, Me.S341_008, Me.Label14, Me.Label11, Me.Line69, Me.Label24, Me.Label5, Me.S341_011, Me.Label13, Me.Label19, Me.S341_012, Me.Label34, Me.Label8, Me.Label31, Me.Label37, Me.Label40, Me.S342_001, Me.Line8, Me.Line9, Me.Line7, Me.Label42, Me.Label43, Me.S342_002, Me.Label45, Me.Label46, Me.S342_003, Me.Label48, Me.Label49, Me.Line15, Me.Line16, Me.Line17, Me.Line21, Me.S342_008, Me.Label52, Me.Label53, Me.Label54, Me.S342_005, Me.Line22, Me.Line23, Me.Line24, Me.Label56, Me.Label57, Me.S342_006, Me.Label59, Me.Label60, Me.S342_007, Me.Label63, Me.Line25, Me.Line26, Me.Line27, Me.Line28, Me.Label64, Me.Label65, Me.Label66, Me.Line29, Me.Line30, Me.Line31, Me.Label68, Me.Label69, Me.S342_010, Me.Label71, Me.Label72, Me.S342_011, Me.Label74, Me.Line32, Me.Line33, Me.Line34, Me.Label75, Me.Line36, Me.Label76, Me.S343_005, Me.Label78, Me.Label79, Me.S343_002, Me.Line35, Me.Line37, Me.Line38, Me.Label81, Me.Label82, Me.S343_003, Me.Label85, Me.S343_004, Me.Label87, Me.Line39, Me.Line40, Me.Line41, Me.Line42, Me.Label89, Me.Label84, Me.S343_007, Me.Line43, Me.Line44, Me.Label92, Me.Line45, Me.Label93, Me.Line46, Me.Label95, Me.Label96, Me.Label97, Me.Label98, Me.S344_001, Me.Line47, Me.Line48, Me.Line49, Me.Label100, Me.Label101, Me.S344_002, Me.Label103, Me.Label104, Me.S344_003, Me.Line50, Me.Line51, Me.Line52, Me.Label94, Me.S35_006, Me.S35_011, Me.S35_010, Me.S35_005, Me.Label245, Me.Label246, Me.S35_002, Me.Label248, Me.S35_003, Me.S35_007, Me.S35_008, Me.Label252, Me.Label253, Me.Line64, Me.Line66, Me.Label254, Me.Label255, Me.Line76, Me.Label256, Me.Label259, Me.Label260, Me.S35_004, Me.S35_009, Me.Label263, Me.Label264, Me.Label267, Me.Line74, Me.Line77, Me.Line78, Me.Line79, Me.Label106, Me.Line221, Me.Line53, Me.Label107, Me.Line68, Me.Line54, Me.Line62, Me.Label41, Me.Shape2, Me.Shape3, Me.Shape5, Me.Shape10, Me.Shape9, Me.Shape6, Me.Line75, Me.PageBreak1})
        Me.Detail.Height = 15.9165!
        Me.Detail.Name = "Detail"
        '
        'S343_001
        '
        Me.S343_001.Height = 0.1875!
        Me.S343_001.HyperLink = Nothing
        Me.S343_001.Left = 2.264!
        Me.S343_001.Name = "S343_001"
        Me.S343_001.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S343_001.Text = "YES"
        Me.S343_001.Top = 11.955!
        Me.S343_001.Width = 0.9900002!
        '
        'S35_001
        '
        Me.S35_001.Height = 0.1875!
        Me.S35_001.HyperLink = Nothing
        Me.S35_001.Left = 2.251!
        Me.S35_001.Name = "S35_001"
        Me.S35_001.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S35_001.Text = "1"
        Me.S35_001.Top = 14.53!
        Me.S35_001.Width = 1.0!
        '
        'S343_006
        '
        Me.S343_006.Height = 0.1875!
        Me.S343_006.HyperLink = Nothing
        Me.S343_006.Left = 3.243!
        Me.S343_006.Name = "S343_006"
        Me.S343_006.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S343_006.Text = "NA"
        Me.S343_006.Top = 12.52!
        Me.S343_006.Width = 1.0!
        '
        'Shape8
        '
        Me.Shape8.Height = 0.1875!
        Me.Shape8.Left = 3.243!
        Me.Shape8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape8.Name = "Shape8"
        Me.Shape8.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape8.Top = 12.52!
        Me.Shape8.Width = 1.0!
        '
        'S341_018
        '
        Me.S341_018.Height = 0.1875!
        Me.S341_018.HyperLink = Nothing
        Me.S341_018.Left = 2.257!
        Me.S341_018.Name = "S341_018"
        Me.S341_018.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S341_018.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_018.Top = 7.994001!
        Me.S341_018.Width = 1.0!
        '
        'S341_021
        '
        Me.S341_021.Height = 0.1875!
        Me.S341_021.HyperLink = Nothing
        Me.S341_021.Left = 5.576!
        Me.S341_021.Name = "S341_021"
        Me.S341_021.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S341_021.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_021.Top = 4.457!
        Me.S341_021.Width = 1.0!
        '
        'S341_001
        '
        Me.S341_001.Height = 0.1875!
        Me.S341_001.HyperLink = Nothing
        Me.S341_001.Left = 2.249!
        Me.S341_001.Name = "S341_001"
        Me.S341_001.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S341_001.Text = "YES"
        Me.S341_001.Top = 4.458!
        Me.S341_001.Width = 1.0!
        '
        'S34_019
        '
        Me.S34_019.Height = 0.1875!
        Me.S34_019.HyperLink = Nothing
        Me.S34_019.Left = 2.249!
        Me.S34_019.Name = "S34_019"
        Me.S34_019.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S34_019.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_019.Top = 4.002!
        Me.S34_019.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.249!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 4.458!
        Me.Shape1.Width = 1.0!
        '
        'Shape4
        '
        Me.Shape4.Height = 0.1875!
        Me.Shape4.Left = 5.576!
        Me.Shape4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape4.Name = "Shape4"
        Me.Shape4.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape4.Top = 4.457!
        Me.Shape4.Width = 1.0!
        '
        'Label205
        '
        Me.Label205.Height = 0.1875!
        Me.Label205.HyperLink = Nothing
        Me.Label205.Left = 3.255!
        Me.Label205.Name = "Label205"
        Me.Label205.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label205.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label205.Top = 7.056003!
        Me.Label205.Width = 1.0!
        '
        'S342_009
        '
        Me.S342_009.Height = 0.1875!
        Me.S342_009.HyperLink = Nothing
        Me.S342_009.Left = 2.244999!
        Me.S342_009.Name = "S342_009"
        Me.S342_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S342_009.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_009.Top = 10.926!
        Me.S342_009.Width = 1.0!
        '
        'S342_012
        '
        Me.S342_012.Height = 0.1875!
        Me.S342_012.HyperLink = Nothing
        Me.S342_012.Left = 2.244999!
        Me.S342_012.Name = "S342_012"
        Me.S342_012.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S342_012.Text = "1"
        Me.S342_012.Top = 11.49!
        Me.S342_012.Width = 1.0!
        '
        'S342_004
        '
        Me.S342_004.Height = 0.1875!
        Me.S342_004.HyperLink = Nothing
        Me.S342_004.Left = 2.245001!
        Me.S342_004.Name = "S342_004"
        Me.S342_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S342_004.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_004.Top = 9.219!
        Me.S342_004.Width = 1.0!
        '
        'S341_020
        '
        Me.S341_020.Height = 0.1875!
        Me.S341_020.HyperLink = Nothing
        Me.S341_020.Left = 3.255!
        Me.S341_020.Name = "S341_020"
        Me.S341_020.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S341_020.Text = "OK"
        Me.S341_020.Top = 7.432!
        Me.S341_020.Width = 1.0!
        '
        'Label206
        '
        Me.Label206.Height = 0.1875!
        Me.Label206.HyperLink = Nothing
        Me.Label206.Left = 2.255!
        Me.Label206.Name = "Label206"
        Me.Label206.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label206.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label206.Top = 7.056003!
        Me.Label206.Width = 1.0!
        '
        'S34_004
        '
        Me.S34_004.Height = 0.1875!
        Me.S34_004.HyperLink = Nothing
        Me.S34_004.Left = 2.249!
        Me.S34_004.Name = "S34_004"
        Me.S34_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S34_004.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_004.Top = 0.7500001!
        Me.S34_004.Width = 1.0!
        '
        'S34_003
        '
        Me.S34_003.Height = 0.1875!
        Me.S34_003.HyperLink = Nothing
        Me.S34_003.Left = 2.249!
        Me.S34_003.Name = "S34_003"
        Me.S34_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S34_003.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_003.Top = 0.5630002!
        Me.S34_003.Width = 1.0!
        '
        'Label189
        '
        Me.Label189.Height = 0.1875!
        Me.Label189.HyperLink = Nothing
        Me.Label189.Left = 0!
        Me.Label189.Name = "Label189"
        Me.Label189.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label189.Text = "3.4 COLUMN PANEL ZONE CHECK (WITHOUT DOUBLER PLATE):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label189.Top = 0!
        Me.Label189.Width = 5.0!
        '
        'Label193
        '
        Me.Label193.Height = 0.1875!
        Me.Label193.HyperLink = Nothing
        Me.Label193.Left = 0!
        Me.Label193.Name = "Label193"
        Me.Label193.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label193.Text = "Φv_PZ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label193.Top = 0.1870001!
        Me.Label193.Width = 2.25!
        '
        'S34_001
        '
        Me.S34_001.Height = 0.1875!
        Me.S34_001.HyperLink = Nothing
        Me.S34_001.Left = 2.249!
        Me.S34_001.Name = "S34_001"
        Me.S34_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S34_001.Text = "0.90" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_001.Top = 0.1880001!
        Me.S34_001.Width = 1.0!
        '
        'Label196
        '
        Me.Label196.Height = 0.1875!
        Me.Label196.HyperLink = Nothing
        Me.Label196.Left = 0.00000002980232!
        Me.Label196.Name = "Label196"
        Me.Label196.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label196.Text = "Fyc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label196.Top = 0.3740001!
        Me.Label196.Width = 2.25!
        '
        'S34_002
        '
        Me.S34_002.Height = 0.1875!
        Me.S34_002.HyperLink = Nothing
        Me.S34_002.Left = 2.249!
        Me.S34_002.Name = "S34_002"
        Me.S34_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S34_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_002.Top = 0.3750001!
        Me.S34_002.Width = 1.0!
        '
        'Label199
        '
        Me.Label199.Height = 0.1875!
        Me.Label199.HyperLink = Nothing
        Me.Label199.Left = 0.00000002980232!
        Me.Label199.Name = "Label199"
        Me.Label199.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label199.Text = "dc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label199.Top = 0.5620001!
        Me.Label199.Width = 2.25!
        '
        'Label201
        '
        Me.Label201.Height = 0.1875!
        Me.Label201.HyperLink = Nothing
        Me.Label201.Left = 3.263!
        Me.Label201.Name = "Label201"
        Me.Label201.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label201.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label201.Top = 0.563!
        Me.Label201.Width = 3.313!
        '
        'Label336
        '
        Me.Label336.Height = 0.1875!
        Me.Label336.HyperLink = Nothing
        Me.Label336.Left = 3.237!
        Me.Label336.Name = "Label336"
        Me.Label336.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label336.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label336.Top = 1.813!
        Me.Label336.Width = 1.0!
        '
        'Label337
        '
        Me.Label337.Height = 0.1875!
        Me.Label337.HyperLink = Nothing
        Me.Label337.Left = 2.249!
        Me.Label337.Name = "Label337"
        Me.Label337.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label337.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label337.Top = 1.813!
        Me.Label337.Width = 1.0!
        '
        'Label338
        '
        Me.Label338.Height = 0.188!
        Me.Label338.HyperLink = Nothing
        Me.Label338.Left = 0!
        Me.Label338.Name = "Label338"
        Me.Label338.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label338.Text = "tcw=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label338.Top = 0.7500001!
        Me.Label338.Width = 2.25!
        '
        'Label340
        '
        Me.Label340.Height = 0.1875!
        Me.Label340.HyperLink = Nothing
        Me.Label340.Left = 3.263!
        Me.Label340.Name = "Label340"
        Me.Label340.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label340.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label340.Top = 0.7500001!
        Me.Label340.Width = 3.313!
        '
        'Line57
        '
        Me.Line57.Height = 0!
        Me.Line57.Left = 2.249!
        Me.Line57.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line57.LineWeight = 1.0!
        Me.Line57.Name = "Line57"
        Me.Line57.Top = 2.001!
        Me.Line57.Width = 2.0!
        Me.Line57.X1 = 2.249!
        Me.Line57.X2 = 4.249!
        Me.Line57.Y1 = 2.001!
        Me.Line57.Y2 = 2.001!
        '
        'Line58
        '
        Me.Line58.Height = 0!
        Me.Line58.Left = 2.249!
        Me.Line58.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line58.LineWeight = 1.0!
        Me.Line58.Name = "Line58"
        Me.Line58.Top = 1.813!
        Me.Line58.Width = 1.988!
        Me.Line58.X1 = 2.249!
        Me.Line58.X2 = 4.237!
        Me.Line58.Y1 = 1.813!
        Me.Line58.Y2 = 1.813!
        '
        'Label125
        '
        Me.Label125.Height = 0.1875!
        Me.Label125.HyperLink = Nothing
        Me.Label125.Left = 3.263!
        Me.Label125.Name = "Label125"
        Me.Label125.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label125.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label125.Top = 0.375!
        Me.Label125.Width = 3.313!
        '
        'Label129
        '
        Me.Label129.Height = 0.1875!
        Me.Label129.HyperLink = Nothing
        Me.Label129.Left = 3.263!
        Me.Label129.Name = "Label129"
        Me.Label129.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label129.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label129.Top = 0.938!
        Me.Label129.Width = 3.313!
        '
        'S34_005
        '
        Me.S34_005.Height = 0.1875!
        Me.S34_005.HyperLink = Nothing
        Me.S34_005.Left = 2.249!
        Me.S34_005.Name = "S34_005"
        Me.S34_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S34_005.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_005.Top = 0.937!
        Me.S34_005.Width = 1.0!
        '
        'Label131
        '
        Me.Label131.Height = 0.188!
        Me.Label131.HyperLink = Nothing
        Me.Label131.Left = 0!
        Me.Label131.Name = "Label131"
        Me.Label131.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label131.Text = "Agc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label131.Top = 0.937!
        Me.Label131.Width = 2.25!
        '
        'Label134
        '
        Me.Label134.Height = 0.1875!
        Me.Label134.HyperLink = Nothing
        Me.Label134.Left = 3.263!
        Me.Label134.Name = "Label134"
        Me.Label134.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label134.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Fyc* Agc" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label134.Top = 1.125!
        Me.Label134.Width = 3.313!
        '
        'Label136
        '
        Me.Label136.Height = 0.188!
        Me.Label136.HyperLink = Nothing
        Me.Label136.Left = 0!
        Me.Label136.Name = "Label136"
        Me.Label136.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label136.Text = "Pc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label136.Top = 1.125!
        Me.Label136.Width = 2.25!
        '
        'S34_006
        '
        Me.S34_006.Height = 0.1875!
        Me.S34_006.HyperLink = Nothing
        Me.S34_006.Left = 2.249!
        Me.S34_006.Name = "S34_006"
        Me.S34_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_006.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_006.Top = 1.125!
        Me.S34_006.Width = 1.0!
        '
        'Label139
        '
        Me.Label139.Height = 0.1875!
        Me.Label139.HyperLink = Nothing
        Me.Label139.Left = 3.263!
        Me.Label139.Name = "Label139"
        Me.Label139.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label139.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label139.Top = 1.313!
        Me.Label139.Width = 3.313!
        '
        'S34_007
        '
        Me.S34_007.Height = 0.1875!
        Me.S34_007.HyperLink = Nothing
        Me.S34_007.Left = 2.249!
        Me.S34_007.Name = "S34_007"
        Me.S34_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S34_007.Text = "406.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_007.Top = 1.313!
        Me.S34_007.Width = 1.0!
        '
        'Label141
        '
        Me.Label141.Height = 0.188!
        Me.Label141.HyperLink = Nothing
        Me.Label141.Left = 0!
        Me.Label141.Name = "Label141"
        Me.Label141.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label141.Text = "Pu=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label141.Top = 1.313!
        Me.Label141.Width = 2.25!
        '
        'Label142
        '
        Me.Label142.Height = 0.312!
        Me.Label142.HyperLink = Nothing
        Me.Label142.Left = 3.763!
        Me.Label142.Name = "Label142"
        Me.Label142.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label142.Text = "= Φv_PZ* if (Pu < 0.4* Pc, 0.6* Fyc* dc* tcw, 0.6* Fyc* dc* tcw* (1.4 - Pu/Pc))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) &
    "  (J10-9 & J10-10 AISC 360-16)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label142.Top = 1.5!
        Me.Label142.Width = 3.8!
        '
        'Label144
        '
        Me.Label144.Height = 0.188!
        Me.Label144.HyperLink = Nothing
        Me.Label144.Left = 0!
        Me.Label144.Name = "Label144"
        Me.Label144.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label144.Text = "ΦRn_PZ=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label144.Top = 1.501!
        Me.Label144.Width = 2.25!
        '
        'S34_008
        '
        Me.S34_008.Height = 0.1875!
        Me.S34_008.HyperLink = Nothing
        Me.S34_008.Left = 2.249!
        Me.S34_008.Name = "S34_008"
        Me.S34_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_008.Text = "528.77" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_008.Top = 1.501!
        Me.S34_008.Width = 1.0!
        '
        'Label146
        '
        Me.Label146.Height = 0.1875!
        Me.Label146.HyperLink = Nothing
        Me.Label146.Left = 4.237!
        Me.Label146.Name = "Label146"
        Me.Label146.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label146.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=ΣMpr/((Hcc_b+Hcc_t)/2), 0 for top column"
        Me.Label146.Top = 2.0!
        Me.Label146.Width = 3.313!
        '
        'S34_020
        '
        Me.S34_020.Height = 0.1875!
        Me.S34_020.HyperLink = Nothing
        Me.S34_020.Left = 3.237!
        Me.S34_020.Name = "S34_020"
        Me.S34_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_020.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_020.Top = 2.0!
        Me.S34_020.Width = 1.0!
        '
        'Label148
        '
        Me.Label148.Height = 0.188!
        Me.Label148.HyperLink = Nothing
        Me.Label148.Left = 0!
        Me.Label148.Name = "Label148"
        Me.Label148.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label148.Text = "Vu_c=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label148.Top = 2.0!
        Me.Label148.Width = 2.25!
        '
        'S34_009
        '
        Me.S34_009.Height = 0.1875!
        Me.S34_009.HyperLink = Nothing
        Me.S34_009.Left = 2.249!
        Me.S34_009.Name = "S34_009"
        Me.S34_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_009.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_009.Top = 2.0!
        Me.S34_009.Width = 1.0!
        '
        'S34_021
        '
        Me.S34_021.Height = 0.1875!
        Me.S34_021.HyperLink = Nothing
        Me.S34_021.Left = 3.237!
        Me.S34_021.Name = "S34_021"
        Me.S34_021.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S34_021.Text = "136.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_021.Top = 2.187!
        Me.S34_021.Width = 1.0!
        '
        'Label151
        '
        Me.Label151.Height = 0.1875!
        Me.Label151.HyperLink = Nothing
        Me.Label151.Left = 4.237!
        Me.Label151.Name = "Label151"
        Me.Label151.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label151.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_link" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label151.Top = 2.187!
        Me.Label151.Width = 3.313!
        '
        'S34_010
        '
        Me.S34_010.Height = 0.1875!
        Me.S34_010.HyperLink = Nothing
        Me.S34_010.Left = 2.249!
        Me.S34_010.Name = "S34_010"
        Me.S34_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S34_010.Text = "136.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_010.Top = 2.187!
        Me.S34_010.Width = 1.0!
        '
        'Label153
        '
        Me.Label153.Height = 0.188!
        Me.Label153.HyperLink = Nothing
        Me.Label153.Left = 0!
        Me.Label153.Name = "Label153"
        Me.Label153.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label153.Text = "Pr_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label153.Top = 2.187!
        Me.Label153.Width = 2.25!
        '
        'Label154
        '
        Me.Label154.Height = 0.1875!
        Me.Label154.HyperLink = Nothing
        Me.Label154.Left = 4.237!
        Me.Label154.Name = "Label154"
        Me.Label154.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label154.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr_link - Vu_c" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label154.Top = 2.375!
        Me.Label154.Width = 3.313!
        '
        'S34_022
        '
        Me.S34_022.Height = 0.1875!
        Me.S34_022.HyperLink = Nothing
        Me.S34_022.Left = 3.237!
        Me.S34_022.Name = "S34_022"
        Me.S34_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_022.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_022.Top = 2.375!
        Me.S34_022.Width = 1.0!
        '
        'Label156
        '
        Me.Label156.Height = 0.188!
        Me.Label156.HyperLink = Nothing
        Me.Label156.Left = 0!
        Me.Label156.Name = "Label156"
        Me.Label156.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label156.Text = "Ru=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label156.Top = 2.375!
        Me.Label156.Width = 2.25!
        '
        'S34_011
        '
        Me.S34_011.Height = 0.1875!
        Me.S34_011.HyperLink = Nothing
        Me.S34_011.Left = 2.249!
        Me.S34_011.Name = "S34_011"
        Me.S34_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_011.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_011.Top = 2.375!
        Me.S34_011.Width = 1.0!
        '
        'Label158
        '
        Me.Label158.Height = 0.1875!
        Me.Label158.HyperLink = Nothing
        Me.Label158.Left = 4.237!
        Me.Label158.Name = "Label158"
        Me.Label158.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label158.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pr_link - Vu_c / ΦRn_PZ" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label158.Top = 2.562!
        Me.Label158.Width = 3.313!
        '
        'S34_023
        '
        Me.S34_023.Height = 0.1875!
        Me.S34_023.HyperLink = Nothing
        Me.S34_023.Left = 3.237!
        Me.S34_023.Name = "S34_023"
        Me.S34_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_023.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_023.Top = 2.562!
        Me.S34_023.Width = 1.0!
        '
        'Label160
        '
        Me.Label160.Height = 0.188!
        Me.Label160.HyperLink = Nothing
        Me.Label160.Left = 0!
        Me.Label160.Name = "Label160"
        Me.Label160.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label160.Text = "DCR_PZ_NODBLR="
        Me.Label160.Top = 2.562!
        Me.Label160.Width = 2.25!
        '
        'S34_012
        '
        Me.S34_012.Height = 0.1875!
        Me.S34_012.HyperLink = Nothing
        Me.S34_012.Left = 2.249!
        Me.S34_012.Name = "S34_012"
        Me.S34_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_012.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_012.Top = 2.562!
        Me.S34_012.Width = 1.0!
        '
        'S34_013
        '
        Me.S34_013.Height = 0.1875!
        Me.S34_013.HyperLink = Nothing
        Me.S34_013.Left = 2.249!
        Me.S34_013.Name = "S34_013"
        Me.S34_013.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S34_013.Text = "0.377" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_013.Top = 2.751!
        Me.S34_013.Width = 1.0!
        '
        'Label163
        '
        Me.Label163.Height = 0.188!
        Me.Label163.HyperLink = Nothing
        Me.Label163.Left = 0!
        Me.Label163.Name = "Label163"
        Me.Label163.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label163.Text = "DCR_PZ_NODBLR_Total="
        Me.Label163.Top = 2.751!
        Me.Label163.Width = 2.25!
        '
        'Label164
        '
        Me.Label164.Height = 0.1875!
        Me.Label164.HyperLink = Nothing
        Me.Label164.Left = 4.237!
        Me.Label164.Name = "Label164"
        Me.Label164.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label164.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Ok, if SUM_PZ <= DCR_PZ_Allowable" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label164.Top = 4.001!
        Me.Label164.Width = 3.313!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 2.249!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 2.0!
        Me.Line10.Width = 1.988!
        Me.Line10.X1 = 2.249!
        Me.Line10.X2 = 4.237!
        Me.Line10.Y1 = 2.0!
        Me.Line10.Y2 = 2.0!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 2.25!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 2.187!
        Me.Line11.Width = 1.988!
        Me.Line11.X1 = 2.25!
        Me.Line11.X2 = 4.238!
        Me.Line11.Y1 = 2.187!
        Me.Line11.Y2 = 2.187!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 2.249!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 2.375!
        Me.Line12.Width = 1.988!
        Me.Line12.X1 = 2.249!
        Me.Line12.X2 = 4.237!
        Me.Line12.Y1 = 2.375!
        Me.Line12.Y2 = 2.375!
        '
        'Line13
        '
        Me.Line13.Height = 0!
        Me.Line13.Left = 2.238!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 2.562!
        Me.Line13.Width = 2.0!
        Me.Line13.X1 = 2.238!
        Me.Line13.X2 = 4.238!
        Me.Line13.Y1 = 2.562!
        Me.Line13.Y2 = 2.562!
        '
        'Line14
        '
        Me.Line14.Height = 0!
        Me.Line14.Left = 2.237!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 2.75!
        Me.Line14.Width = 2.0!
        Me.Line14.X1 = 2.237!
        Me.Line14.X2 = 4.237!
        Me.Line14.Y1 = 2.75!
        Me.Line14.Y2 = 2.75!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 3.263!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label15.Text = "kips"
        Me.Label15.Top = 1.501!
        Me.Label15.Width = 0.3130009!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 2.237!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 2.75!
        Me.Line2.Width = 1.999998!
        Me.Line2.X1 = 2.237!
        Me.Line2.X2 = 4.236998!
        Me.Line2.Y1 = 2.75!
        Me.Line2.Y2 = 2.75!
        '
        'Label165
        '
        Me.Label165.Height = 0.1875!
        Me.Label165.HyperLink = Nothing
        Me.Label165.Left = 0!
        Me.Label165.Name = "Label165"
        Me.Label165.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label165.Text = "3.4.1 COLUMN WEB DOUBLER PLATE CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label165.Top = 4.271!
        Me.Label165.Width = 5.0!
        '
        'Label166
        '
        Me.Label166.Height = 0.188!
        Me.Label166.HyperLink = Nothing
        Me.Label166.Left = 0!
        Me.Label166.Name = "Label166"
        Me.Label166.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label166.Text = "ΦRn_PZ_NODBLR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label166.Top = 4.834001!
        Me.Label166.Width = 2.25!
        '
        'S341_003
        '
        Me.S341_003.Height = 0.1875!
        Me.S341_003.HyperLink = Nothing
        Me.S341_003.Left = 2.249!
        Me.S341_003.Name = "S341_003"
        Me.S341_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_003.Text = "0.90" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_003.Top = 4.834001!
        Me.S341_003.Width = 1.0!
        '
        'Label191
        '
        Me.Label191.Height = 0.188!
        Me.Label191.HyperLink = Nothing
        Me.Label191.Left = 0!
        Me.Label191.Name = "Label191"
        Me.Label191.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label191.Text = "ΦRn_PZ_req=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label191.Top = 5.022!
        Me.Label191.Width = 2.25!
        '
        'S341_004
        '
        Me.S341_004.Height = 0.1875!
        Me.S341_004.HyperLink = Nothing
        Me.S341_004.Left = 2.249!
        Me.S341_004.Name = "S341_004"
        Me.S341_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_004.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_004.Top = 5.022!
        Me.S341_004.Width = 1.0!
        '
        'Label208
        '
        Me.Label208.Height = 0.188!
        Me.Label208.HyperLink = Nothing
        Me.Label208.Left = 0!
        Me.Label208.Name = "Label208"
        Me.Label208.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label208.Text = "ΦRn_PZ_NEW=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label208.Top = 5.210001!
        Me.Label208.Width = 2.25!
        '
        'S341_005
        '
        Me.S341_005.Height = 0.1875!
        Me.S341_005.HyperLink = Nothing
        Me.S341_005.Left = 2.249!
        Me.S341_005.Name = "S341_005"
        Me.S341_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_005.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_005.Top = 5.210001!
        Me.S341_005.Width = 1.0!
        '
        'Label212
        '
        Me.Label212.Height = 0.188!
        Me.Label212.HyperLink = Nothing
        Me.Label212.Left = 3.25!
        Me.Label212.Name = "Label212"
        Me.Label212.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label212.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "= Φv_pz* if (Pu < 0.4* Pc, 0.6* Fy_col* dc* tcw, 0.6* Fy_col* dc* tcw* (1.4" &
    " - Pu/Pc))"
        Me.Label212.Top = 4.834001!
        Me.Label212.Width = 4.5!
        '
        'Label216
        '
        Me.Label216.Height = 0.1875!
        Me.Label216.HyperLink = Nothing
        Me.Label216.Left = 4.254998!
        Me.Label216.Name = "Label216"
        Me.Label216.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label216.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=Total demand on column web"
        Me.Label216.Top = 7.245!
        Me.Label216.Width = 3.514003!
        '
        'Label217
        '
        Me.Label217.Height = 0.188!
        Me.Label217.HyperLink = Nothing
        Me.Label217.Left = 0.006000519!
        Me.Label217.Name = "Label217"
        Me.Label217.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label217.Text = "DCR_PZ_NEW="
        Me.Label217.Top = 7.245!
        Me.Label217.Width = 2.25!
        '
        'S341_014
        '
        Me.S341_014.Height = 0.1875!
        Me.S341_014.HyperLink = Nothing
        Me.S341_014.Left = 2.251!
        Me.S341_014.Name = "S341_014"
        Me.S341_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_014.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_014.Top = 7.245002!
        Me.S341_014.Width = 1.0!
        '
        'S341_019
        '
        Me.S341_019.Height = 0.1875!
        Me.S341_019.HyperLink = Nothing
        Me.S341_019.Left = 3.254998!
        Me.S341_019.Name = "S341_019"
        Me.S341_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_019.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_019.Top = 7.245!
        Me.S341_019.Width = 1.0!
        '
        'S341_015
        '
        Me.S341_015.Height = 0.1875!
        Me.S341_015.HyperLink = Nothing
        Me.S341_015.Left = 2.251!
        Me.S341_015.Name = "S341_015"
        Me.S341_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_015.Text = "136.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_015.Top = 7.432003!
        Me.S341_015.Width = 1.0!
        '
        'Label223
        '
        Me.Label223.Height = 0.188!
        Me.Label223.HyperLink = Nothing
        Me.Label223.Left = 0.006000519!
        Me.Label223.Name = "Label223"
        Me.Label223.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label223.Text = "DCR_PZ_total_NEW="
        Me.Label223.Top = 7.432!
        Me.Label223.Width = 2.25!
        '
        'Label213
        '
        Me.Label213.Height = 0.188!
        Me.Label213.HyperLink = Nothing
        Me.Label213.Left = 0.006000519!
        Me.Label213.Name = "Label213"
        Me.Label213.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label213.Text = "DCR_PZ_geometry_Web="
        Me.Label213.Top = 7.62!
        Me.Label213.Width = 2.25!
        '
        'S341_016
        '
        Me.S341_016.Height = 0.1875!
        Me.S341_016.HyperLink = Nothing
        Me.S341_016.Left = 2.255001!
        Me.S341_016.Name = "S341_016"
        Me.S341_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_016.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_016.Top = 7.62!
        Me.S341_016.Width = 1.0!
        '
        'Label225
        '
        Me.Label225.Height = 0.188!
        Me.Label225.HyperLink = Nothing
        Me.Label225.Left = 0.006000519!
        Me.Label225.Name = "Label225"
        Me.Label225.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label225.Text = "DCR_PZ_geometry_Total="
        Me.Label225.Top = 7.808002!
        Me.Label225.Width = 2.25!
        '
        'S341_017
        '
        Me.S341_017.Height = 0.1875!
        Me.S341_017.HyperLink = Nothing
        Me.S341_017.Left = 2.251!
        Me.S341_017.Name = "S341_017"
        Me.S341_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_017.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_017.Top = 7.808002!
        Me.S341_017.Width = 1.0!
        '
        'Label233
        '
        Me.Label233.Height = 0.188!
        Me.Label233.HyperLink = Nothing
        Me.Label233.Left = 0.006000519!
        Me.Label233.Name = "Label233"
        Me.Label233.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label233.Text = "DCR_PZ_Total="
        Me.Label233.Top = 7.996!
        Me.Label233.Width = 2.25!
        '
        'Line63
        '
        Me.Line63.Height = 0!
        Me.Line63.Left = 2.255!
        Me.Line63.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line63.LineWeight = 1.0!
        Me.Line63.Name = "Line63"
        Me.Line63.Top = 7.244001!
        Me.Line63.Width = 1.999999!
        Me.Line63.X1 = 2.255!
        Me.Line63.X2 = 4.254999!
        Me.Line63.Y1 = 7.244001!
        Me.Line63.Y2 = 7.244001!
        '
        'Line67
        '
        Me.Line67.Height = 0!
        Me.Line67.Left = 2.255!
        Me.Line67.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line67.LineWeight = 1.0!
        Me.Line67.Name = "Line67"
        Me.Line67.Top = 7.432!
        Me.Line67.Width = 1.999998!
        Me.Line67.X1 = 2.255!
        Me.Line67.X2 = 4.254998!
        Me.Line67.Y1 = 7.432!
        Me.Line67.Y2 = 7.432!
        '
        'Line73
        '
        Me.Line73.Height = 0!
        Me.Line73.Left = 2.255!
        Me.Line73.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line73.LineWeight = 1.0!
        Me.Line73.Name = "Line73"
        Me.Line73.Top = 7.808002!
        Me.Line73.Width = 1.0!
        Me.Line73.X1 = 2.255!
        Me.Line73.X2 = 3.255!
        Me.Line73.Y1 = 7.808002!
        Me.Line73.Y2 = 7.808002!
        '
        'Line70
        '
        Me.Line70.Height = 0.00000333786!
        Me.Line70.Left = 2.255!
        Me.Line70.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line70.LineWeight = 1.0!
        Me.Line70.Name = "Line70"
        Me.Line70.Top = 7.996!
        Me.Line70.Width = 0.994!
        Me.Line70.X1 = 2.255!
        Me.Line70.X2 = 3.249!
        Me.Line70.Y1 = 7.996!
        Me.Line70.Y2 = 7.996003!
        '
        'Line61
        '
        Me.Line61.Height = 0.5639987!
        Me.Line61.Left = 4.255!
        Me.Line61.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line61.LineWeight = 1.0!
        Me.Line61.Name = "Line61"
        Me.Line61.Top = 7.056003!
        Me.Line61.Width = 0!
        Me.Line61.X1 = 4.255!
        Me.Line61.X2 = 4.255!
        Me.Line61.Y1 = 7.056003!
        Me.Line61.Y2 = 7.620002!
        '
        'Label241
        '
        Me.Label241.Height = 0.188!
        Me.Label241.HyperLink = Nothing
        Me.Label241.Left = 0.0000003576279!
        Me.Label241.Name = "Label241"
        Me.Label241.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label241.Text = "Plate Required=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label241.Top = 4.458!
        Me.Label241.Width = 2.25!
        '
        'Label243
        '
        Me.Label243.Height = 0.1875!
        Me.Label243.HyperLink = Nothing
        Me.Label243.Left = 3.257!
        Me.Label243.Name = "Label243"
        Me.Label243.Style = "color: Red; font-size: 6.75pt; font-style: normal; font-weight: bold; vertical-al" &
    "ign: middle"
        Me.Label243.Text = "<----If NO, Skip 3.4.1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label243.Top = 4.458!
        Me.Label243.Width = 1.053001!
        '
        'Line5
        '
        Me.Line5.Height = 0.9360001!
        Me.Line5.Left = 4.237!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 1.813!
        Me.Line5.Width = 0!
        Me.Line5.X1 = 4.237!
        Me.Line5.X2 = 4.237!
        Me.Line5.Y1 = 1.813!
        Me.Line5.Y2 = 2.749!
        '
        'Line18
        '
        Me.Line18.Height = 0!
        Me.Line18.Left = 2.25!
        Me.Line18.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line18.LineWeight = 1.0!
        Me.Line18.Name = "Line18"
        Me.Line18.Top = 2.749!
        Me.Line18.Width = 1.987999!
        Me.Line18.X1 = 2.25!
        Me.Line18.X2 = 4.237999!
        Me.Line18.Y1 = 2.749!
        Me.Line18.Y2 = 2.749!
        '
        'Line19
        '
        Me.Line19.Height = 0.0009994507!
        Me.Line19.Left = 2.25!
        Me.Line19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line19.LineWeight = 1.0!
        Me.Line19.Name = "Line19"
        Me.Line19.Top = 8.184001!
        Me.Line19.Width = 1.013!
        Me.Line19.X1 = 2.25!
        Me.Line19.X2 = 3.263!
        Me.Line19.Y1 = 8.185!
        Me.Line19.Y2 = 8.184001!
        '
        'Line20
        '
        Me.Line20.Height = 0!
        Me.Line20.Left = 2.249!
        Me.Line20.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line20.LineWeight = 1.0!
        Me.Line20.Name = "Line20"
        Me.Line20.Top = 2.938!
        Me.Line20.Width = 1.0!
        Me.Line20.X1 = 2.249!
        Me.Line20.X2 = 3.249!
        Me.Line20.Y1 = 2.938!
        Me.Line20.Y2 = 2.938!
        '
        'Label10
        '
        Me.Label10.Height = 0.188!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label10.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 4.002!
        Me.Label10.Width = 2.25!
        '
        'Line3
        '
        Me.Line3.Height = 1.125!
        Me.Line3.Left = 2.249!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 1.813!
        Me.Line3.Width = 0!
        Me.Line3.X1 = 2.249!
        Me.Line3.X2 = 2.249!
        Me.Line3.Y1 = 1.813!
        Me.Line3.Y2 = 2.938!
        '
        'Line4
        '
        Me.Line4.Height = 1.125!
        Me.Line4.Left = 3.249!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 1.813!
        Me.Line4.Width = 0!
        Me.Line4.X1 = 3.249!
        Me.Line4.X2 = 3.249!
        Me.Line4.Y1 = 1.813!
        Me.Line4.Y2 = 2.938!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 4.238!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label1.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=db-2tbf"
        Me.Label1.Top = 3.062!
        Me.Label1.Width = 3.313!
        '
        'Label4
        '
        Me.Label4.Height = 0.188!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.001000404!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "dz=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 3.062!
        Me.Label4.Width = 2.25!
        '
        'S34_014
        '
        Me.S34_014.Height = 0.1875!
        Me.S34_014.HyperLink = Nothing
        Me.S34_014.Left = 2.25!
        Me.S34_014.Name = "S34_014"
        Me.S34_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_014.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_014.Top = 3.062!
        Me.S34_014.Width = 1.0!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 4.237!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label18.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=dc-2tcf"
        Me.Label18.Top = 3.25!
        Me.Label18.Width = 3.313!
        '
        'Label20
        '
        Me.Label20.Height = 0.188!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label20.Text = "wz="
        Me.Label20.Top = 3.25!
        Me.Label20.Width = 2.25!
        '
        'S34_015
        '
        Me.S34_015.Height = 0.1875!
        Me.S34_015.HyperLink = Nothing
        Me.S34_015.Left = 2.249!
        Me.S34_015.Name = "S34_015"
        Me.S34_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_015.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_015.Top = 3.25!
        Me.S34_015.Width = 1.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 4.237!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label22.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined"
        Me.Label22.Top = 3.438!
        Me.Label22.Width = 3.313!
        '
        'Label23
        '
        Me.Label23.Height = 0.188!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 0!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label23.Text = "tcw="
        Me.Label23.Top = 3.438!
        Me.Label23.Width = 2.25!
        '
        'S34_016
        '
        Me.S34_016.Height = 0.1875!
        Me.S34_016.HyperLink = Nothing
        Me.S34_016.Left = 2.249001!
        Me.S34_016.Name = "S34_016"
        Me.S34_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_016.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_016.Top = 3.438!
        Me.S34_016.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 4.238!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label25.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=[(dz+wz)/90] / tcw, N/A for R=3"
        Me.Label25.Top = 3.626!
        Me.Label25.Width = 3.313!
        '
        'Label26
        '
        Me.Label26.Height = 0.188!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 0.0009996891!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label26.Text = "DCR_PZ_geometry="
        Me.Label26.Top = 3.626!
        Me.Label26.Width = 2.25!
        '
        'S34_017
        '
        Me.S34_017.Height = 0.1875!
        Me.S34_017.HyperLink = Nothing
        Me.S34_017.Left = 2.25!
        Me.S34_017.Name = "S34_017"
        Me.S34_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_017.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_017.Top = 3.626!
        Me.S34_017.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 4.238!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label28.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=ΣMpb/((Hcc_b+Hcc_t)/2), 0 for top story" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 3.814001!
        Me.Label28.Width = 3.313!
        '
        'Label29
        '
        Me.Label29.Height = 0.188!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.0009996889!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label29.Text = "DCR_PZ_total="
        Me.Label29.Top = 3.814001!
        Me.Label29.Width = 2.25!
        '
        'S34_018
        '
        Me.S34_018.Height = 0.1875!
        Me.S34_018.HyperLink = Nothing
        Me.S34_018.Left = 2.25!
        Me.S34_018.Name = "S34_018"
        Me.S34_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S34_018.Text = "5.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S34_018.Top = 3.814001!
        Me.S34_018.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.188!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 3.25!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label16.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "= Additional capcaity required"
        Me.Label16.Top = 5.022!
        Me.Label16.Width = 4.5!
        '
        'Label17
        '
        Me.Label17.Height = 0.188!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 3.25!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label17.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "= ΦRn_PZ_NODBLR + fRn_PZ_req"
        Me.Label17.Top = 5.210001!
        Me.Label17.Width = 4.5!
        '
        'Label21
        '
        Me.Label21.Height = 0.188!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.013!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "t_dblr_strength="
        Me.Label21.Top = 5.398!
        Me.Label21.Width = 2.25!
        '
        'S341_006
        '
        Me.S341_006.Height = 0.1875!
        Me.S341_006.HyperLink = Nothing
        Me.S341_006.Left = 2.261999!
        Me.S341_006.Name = "S341_006"
        Me.S341_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_006.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_006.Top = 5.398!
        Me.S341_006.Width = 1.0!
        '
        'Label27
        '
        Me.Label27.Height = 0.2790001!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.263!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label27.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9)
        Me.Label27.Top = 5.398!
        Me.Label27.Width = 4.5!
        '
        'Label30
        '
        Me.Label30.Height = 0.188!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 0.013!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label30.Text = "t_dblr_geom="
        Me.Label30.Top = 6.053!
        Me.Label30.Width = 2.25!
        '
        'S341_009
        '
        Me.S341_009.Height = 0.1875!
        Me.S341_009.HyperLink = Nothing
        Me.S341_009.Left = 2.261999!
        Me.S341_009.Name = "S341_009"
        Me.S341_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_009.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_009.Top = 6.053!
        Me.S341_009.Width = 1.0!
        '
        'Label32
        '
        Me.Label32.Height = 0.188!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 3.261999!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label32.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=if(R=3, 0, Ceiling(((dz+wz)/90),0.125))"
        Me.Label32.Top = 6.053!
        Me.Label32.Width = 4.5!
        '
        'Label33
        '
        Me.Label33.Height = 0.188!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0.013!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "t_dpl="
        Me.Label33.Top = 6.241!
        Me.Label33.Width = 2.25!
        '
        'S341_010
        '
        Me.S341_010.Height = 0.1875!
        Me.S341_010.HyperLink = Nothing
        Me.S341_010.Left = 2.261999!
        Me.S341_010.Name = "S341_010"
        Me.S341_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_010.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_010.Top = 6.241!
        Me.S341_010.Width = 1.0!
        '
        'Label35
        '
        Me.Label35.Height = 0.188!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 3.261999!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label35.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=Ceiling(((dz+wz)/90),0.125)"
        Me.Label35.Top = 6.241!
        Me.Label35.Width = 4.5!
        '
        'Label36
        '
        Me.Label36.Height = 0.188!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 0.014!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label36.Text = "ΦRn_PZ_DBLR="
        Me.Label36.Top = 6.805002!
        Me.Label36.Width = 2.25!
        '
        'S341_013
        '
        Me.S341_013.Height = 0.1875!
        Me.S341_013.HyperLink = Nothing
        Me.S341_013.Left = 2.262999!
        Me.S341_013.Name = "S341_013"
        Me.S341_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_013.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_013.Top = 6.805002!
        Me.S341_013.Width = 1.0!
        '
        'Label38
        '
        Me.Label38.Height = 0.2489994!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 3.262999!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label38.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "= Φv_pz* if (Pu < 0.4* Pc, 0.6* Fy_col* dc* (tcw+t_dblr), " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "               " &
    "                            0.6* Fy_col* dc* (tcw+t_dblr)* (1.4 - Pu/Pc))"
        Me.Label38.Top = 6.805002!
        Me.Label38.Width = 4.5!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 3.263!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label39.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=max of DCR_PZ (strength), DCR_PZ (geometry_web), DCR_PZ (geometry_DP)"
        Me.Label39.Top = 7.994001!
        Me.Label39.Width = 4.486!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 3.257!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label2.Text = "in, with plug weld           =[(dz+wz)/90] / (tcw+t_dblr) , 0 for R=3"
        Me.Label2.Top = 7.807002!
        Me.Label2.Width = 4.492001!
        '
        'Line1
        '
        Me.Line1.Height = 1.127002!
        Me.Line1.Left = 3.249!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 7.054002!
        Me.Line1.Width = 0!
        Me.Line1.X1 = 3.249!
        Me.Line1.X2 = 3.249!
        Me.Line1.Y1 = 7.054002!
        Me.Line1.Y2 = 8.181004!
        '
        'Line6
        '
        Me.Line6.Height = 1.127!
        Me.Line6.Left = 2.263!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 7.054002!
        Me.Line6.Width = 0!
        Me.Line6.X1 = 2.263!
        Me.Line6.X2 = 2.263!
        Me.Line6.Y1 = 7.054002!
        Me.Line6.Y2 = 8.181002!
        '
        'Label3
        '
        Me.Label3.Height = 0.188!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label3.Text = "Fy_dpl="
        Me.Label3.Top = 4.646!
        Me.Label3.Width = 2.25!
        '
        'S341_002
        '
        Me.S341_002.Height = 0.1875!
        Me.S341_002.HyperLink = Nothing
        Me.S341_002.Left = 2.249!
        Me.S341_002.Name = "S341_002"
        Me.S341_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S341_002.Text = "50"
        Me.S341_002.Top = 4.646!
        Me.S341_002.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.188!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.25!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label6.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=Doubler Yield Strength"
        Me.Label6.Top = 4.646!
        Me.Label6.Width = 4.5!
        '
        'Label7
        '
        Me.Label7.Height = 0.188!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "dz="
        Me.Label7.Top = 5.677001!
        Me.Label7.Width = 2.25!
        '
        'S341_007
        '
        Me.S341_007.Height = 0.1875!
        Me.S341_007.HyperLink = Nothing
        Me.S341_007.Left = 2.248999!
        Me.S341_007.Name = "S341_007"
        Me.S341_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_007.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_007.Top = 5.677001!
        Me.S341_007.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.188!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 3.248999!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label9.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=db-2tbf, if different db, use average"
        Me.Label9.Top = 5.677001!
        Me.Label9.Width = 4.5!
        '
        'Label12
        '
        Me.Label12.Height = 0.188!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "wz="
        Me.Label12.Top = 5.865!
        Me.Label12.Width = 2.25!
        '
        'S341_008
        '
        Me.S341_008.Height = 0.1875!
        Me.S341_008.HyperLink = Nothing
        Me.S341_008.Left = 2.248999!
        Me.S341_008.Name = "S341_008"
        Me.S341_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_008.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_008.Top = 5.865!
        Me.S341_008.Width = 1.0!
        '
        'Label14
        '
        Me.Label14.Height = 0.188!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 3.248999!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label14.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=dc-2tcf"
        Me.Label14.Top = 5.865!
        Me.Label14.Width = 4.5!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 3.249!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label11.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "=[(dz+wz)/90] / tcw, 0 for R=3"
        Me.Label11.Top = 7.620002!
        Me.Label11.Width = 4.492001!
        '
        'Line69
        '
        Me.Line69.Height = 0!
        Me.Line69.Left = 2.255!
        Me.Line69.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line69.LineWeight = 1.0!
        Me.Line69.Name = "Line69"
        Me.Line69.Top = 7.62!
        Me.Line69.Width = 1.999998!
        Me.Line69.X1 = 2.255!
        Me.Line69.X2 = 4.254998!
        Me.Line69.Y1 = 7.62!
        Me.Line69.Y2 = 7.62!
        '
        'Label24
        '
        Me.Label24.Height = 0.188!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 4.535!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label24.Text = "USE Plug Weld?"
        Me.Label24.Top = 4.457!
        Me.Label24.Width = 1.041!
        '
        'Label5
        '
        Me.Label5.Height = 0.188!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "t_dblr_min="
        Me.Label5.Top = 6.429!
        Me.Label5.Width = 2.25!
        '
        'S341_011
        '
        Me.S341_011.Height = 0.1875!
        Me.S341_011.HyperLink = Nothing
        Me.S341_011.Left = 2.248999!
        Me.S341_011.Name = "S341_011"
        Me.S341_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S341_011.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_011.Top = 6.429!
        Me.S341_011.Width = 1.0!
        '
        'Label13
        '
        Me.Label13.Height = 0.188!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 3.248999!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label13.Text = "in, with plug weld" & Global.Microsoft.VisualBasic.ChrW(9) & "=max([(dz+wz)/90] - tcw,0.25,t_dblr_strength)"
        Me.Label13.Top = 6.429!
        Me.Label13.Width = 4.5!
        '
        'Label19
        '
        Me.Label19.Height = 0.188!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label19.Text = "t_dblr_use="
        Me.Label19.Top = 6.617001!
        Me.Label19.Width = 2.25!
        '
        'S341_012
        '
        Me.S341_012.Height = 0.1875!
        Me.S341_012.HyperLink = Nothing
        Me.S341_012.Left = 2.248999!
        Me.S341_012.Name = "S341_012"
        Me.S341_012.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S341_012.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S341_012.Top = 6.617001!
        Me.S341_012.Width = 1.0!
        '
        'Label34
        '
        Me.Label34.Height = 0.188!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 3.248999!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label34.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(9) & "Doubler Plate thickness"
        Me.Label34.Top = 6.617001!
        Me.Label34.Width = 4.5!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.01399905!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label8.Text = "3.4.2 DOUBLER TO COLUMN WED/CONTINUITY PLATE WELD"
        Me.Label8.Top = 8.279!
        Me.Label8.Width = 5.0!
        '
        'Label31
        '
        Me.Label31.Height = 0.188!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 1.005999!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label31.Text = "Option 1: Doubler without Continuity plates"
        Me.Label31.Top = 8.467!
        Me.Label31.Width = 6.433!
        '
        'Label37
        '
        Me.Label37.Height = 0.1875!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 3.248999!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label37.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "doubler plate thickness"
        Me.Label37.Top = 8.655!
        Me.Label37.Width = 4.514003!
        '
        'Label40
        '
        Me.Label40.Height = 0.188!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0.000001907349!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label40.Text = "t_dblr="
        Me.Label40.Top = 8.655!
        Me.Label40.Width = 2.25!
        '
        'S342_001
        '
        Me.S342_001.Height = 0.1875!
        Me.S342_001.HyperLink = Nothing
        Me.S342_001.Left = 2.245001!
        Me.S342_001.Name = "S342_001"
        Me.S342_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S342_001.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_001.Top = 8.655!
        Me.S342_001.Width = 1.0!
        '
        'Line8
        '
        Me.Line8.Height = 0.7509985!
        Me.Line8.Left = 3.243002!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 8.655002!
        Me.Line8.Width = 0!
        Me.Line8.X1 = 3.243002!
        Me.Line8.X2 = 3.243002!
        Me.Line8.Y1 = 8.655002!
        Me.Line8.Y2 = 9.406!
        '
        'Line9
        '
        Me.Line9.Height = 0.7509985!
        Me.Line9.Left = 2.257002!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 8.655002!
        Me.Line9.Width = 0!
        Me.Line9.X1 = 2.257002!
        Me.Line9.X2 = 2.257002!
        Me.Line9.Y1 = 8.655002!
        Me.Line9.Y2 = 9.406!
        '
        'Line7
        '
        Me.Line7.Height = 0.0000009536743!
        Me.Line7.Left = 2.263999!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 8.654999!
        Me.Line7.Width = 0.973!
        Me.Line7.X1 = 2.263999!
        Me.Line7.X2 = 3.236999!
        Me.Line7.Y1 = 8.654999!
        Me.Line7.Y2 = 8.655!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 3.248999!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "column web thickness"
        Me.Label42.Top = 8.843!
        Me.Label42.Width = 4.514003!
        '
        'Label43
        '
        Me.Label43.Height = 0.188!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 0.000001907349!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label43.Text = "tcw="
        Me.Label43.Top = 8.843!
        Me.Label43.Width = 2.25!
        '
        'S342_002
        '
        Me.S342_002.Height = 0.1875!
        Me.S342_002.HyperLink = Nothing
        Me.S342_002.Left = 2.245001!
        Me.S342_002.Name = "S342_002"
        Me.S342_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S342_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_002.Top = 8.843!
        Me.S342_002.Width = 1.0!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 3.248999!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label45.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=min of tDP and tcw, rounded up to nearest 1/16"
        Me.Label45.Top = 9.031!
        Me.Label45.Width = 4.514003!
        '
        'Label46
        '
        Me.Label46.Height = 0.188!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0.000001907349!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label46.Text = "min t="
        Me.Label46.Top = 9.031!
        Me.Label46.Width = 2.25!
        '
        'S342_003
        '
        Me.S342_003.Height = 0.1875!
        Me.S342_003.HyperLink = Nothing
        Me.S342_003.Left = 2.245001!
        Me.S342_003.Name = "S342_003"
        Me.S342_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S342_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_003.Top = 9.031!
        Me.S342_003.Width = 1.0!
        '
        'Label48
        '
        Me.Label48.Height = 0.1875!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 3.248999!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label48.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "From AISC 360 Table J2.4"
        Me.Label48.Top = 9.219!
        Me.Label48.Width = 4.514003!
        '
        'Label49
        '
        Me.Label49.Height = 0.188!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 0.000001907349!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label49.Text = "wfillet="
        Me.Label49.Top = 9.219!
        Me.Label49.Width = 2.25!
        '
        'Line15
        '
        Me.Line15.Height = 0!
        Me.Line15.Left = 2.263999!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 8.843!
        Me.Line15.Width = 0.973!
        Me.Line15.X1 = 2.263999!
        Me.Line15.X2 = 3.236999!
        Me.Line15.Y1 = 8.843!
        Me.Line15.Y2 = 8.843!
        '
        'Line16
        '
        Me.Line16.Height = 0!
        Me.Line16.Left = 2.254999!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 9.031!
        Me.Line16.Width = 0.973!
        Me.Line16.X1 = 2.254999!
        Me.Line16.X2 = 3.227999!
        Me.Line16.Y1 = 9.031!
        Me.Line16.Y2 = 9.031!
        '
        'Line17
        '
        Me.Line17.Height = 0!
        Me.Line17.Left = 2.250999!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 9.218!
        Me.Line17.Width = 0.9729991!
        Me.Line17.X1 = 2.250999!
        Me.Line17.X2 = 3.223998!
        Me.Line17.Y1 = 9.218!
        Me.Line17.Y2 = 9.218!
        '
        'Line21
        '
        Me.Line21.Height = 0!
        Me.Line21.Left = 2.256999!
        Me.Line21.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line21.LineWeight = 1.0!
        Me.Line21.Name = "Line21"
        Me.Line21.Top = 9.406!
        Me.Line21.Width = 0.9729991!
        Me.Line21.X1 = 2.256999!
        Me.Line21.X2 = 3.229998!
        Me.Line21.Y1 = 9.406!
        Me.Line21.Y2 = 9.406!
        '
        'S342_008
        '
        Me.S342_008.Height = 0.1875!
        Me.S342_008.HyperLink = Nothing
        Me.S342_008.Left = 2.245001!
        Me.S342_008.Name = "S342_008"
        Me.S342_008.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S342_008.Text = "2"
        Me.S342_008.Top = 10.24!
        Me.S342_008.Width = 1.0!
        '
        'Label52
        '
        Me.Label52.Height = 0.188!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 1.005999!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label52.Text = "Option 2A: Extended Doubler Plate"
        Me.Label52.Top = 9.488!
        Me.Label52.Width = 6.433001!
        '
        'Label53
        '
        Me.Label53.Height = 0.1875!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 3.248999!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label53.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "doubler plate thickness"
        Me.Label53.Top = 9.676!
        Me.Label53.Width = 4.514003!
        '
        'Label54
        '
        Me.Label54.Height = 0.188!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 0.000001907349!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label54.Text = "t_dblr="
        Me.Label54.Top = 9.676!
        Me.Label54.Width = 2.25!
        '
        'S342_005
        '
        Me.S342_005.Height = 0.1875!
        Me.S342_005.HyperLink = Nothing
        Me.S342_005.Left = 2.245001!
        Me.S342_005.Name = "S342_005"
        Me.S342_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S342_005.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_005.Top = 9.676!
        Me.S342_005.Width = 1.0!
        '
        'Line22
        '
        Me.Line22.Height = 0.7510004!
        Me.Line22.Left = 3.243002!
        Me.Line22.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line22.LineWeight = 1.0!
        Me.Line22.Name = "Line22"
        Me.Line22.Top = 9.676!
        Me.Line22.Width = 0!
        Me.Line22.X1 = 3.243002!
        Me.Line22.X2 = 3.243002!
        Me.Line22.Y1 = 9.676!
        Me.Line22.Y2 = 10.427!
        '
        'Line23
        '
        Me.Line23.Height = 0.7510004!
        Me.Line23.Left = 2.257002!
        Me.Line23.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line23.LineWeight = 1.0!
        Me.Line23.Name = "Line23"
        Me.Line23.Top = 9.676!
        Me.Line23.Width = 0!
        Me.Line23.X1 = 2.257002!
        Me.Line23.X2 = 2.257002!
        Me.Line23.Y1 = 9.676!
        Me.Line23.Y2 = 10.427!
        '
        'Line24
        '
        Me.Line24.Height = 0!
        Me.Line24.Left = 2.263999!
        Me.Line24.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line24.LineWeight = 1.0!
        Me.Line24.Name = "Line24"
        Me.Line24.Top = 9.676!
        Me.Line24.Width = 0.973!
        Me.Line24.X1 = 2.263999!
        Me.Line24.X2 = 3.236999!
        Me.Line24.Y1 = 9.676!
        Me.Line24.Y2 = 9.676!
        '
        'Label56
        '
        Me.Label56.Height = 0.1875!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 3.248999!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label56.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=0.75*5/8*tDP (develop shear strenght of the doubler Plate), rounded to nearst" &
    " 1/16"
        Me.Label56.Top = 9.863999!
        Me.Label56.Width = 4.514003!
        '
        'Label57
        '
        Me.Label57.Height = 0.188!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 0.000001907349!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label57.Text = "wfillet="
        Me.Label57.Top = 9.863999!
        Me.Label57.Width = 2.25!
        '
        'S342_006
        '
        Me.S342_006.Height = 0.1875!
        Me.S342_006.HyperLink = Nothing
        Me.S342_006.Left = 2.245001!
        Me.S342_006.Name = "S342_006"
        Me.S342_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S342_006.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_006.Top = 9.863999!
        Me.S342_006.Width = 1.0!
        '
        'Label59
        '
        Me.Label59.Height = 0.1875!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 3.248999!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label59.Text = "in"
        Me.Label59.Top = 10.052!
        Me.Label59.Width = 4.514003!
        '
        'Label60
        '
        Me.Label60.Height = 0.188!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 0.000001907349!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label60.Text = "wfillet_use="
        Me.Label60.Top = 10.052!
        Me.Label60.Width = 2.25!
        '
        'S342_007
        '
        Me.S342_007.Height = 0.1875!
        Me.S342_007.HyperLink = Nothing
        Me.S342_007.Left = 2.245001!
        Me.S342_007.Name = "S342_007"
        Me.S342_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S342_007.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_007.Top = 10.052!
        Me.S342_007.Width = 1.0!
        '
        'Label63
        '
        Me.Label63.Height = 0.188!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 0.000001907349!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label63.Text = "# of sides="
        Me.Label63.Top = 10.24!
        Me.Label63.Width = 2.25!
        '
        'Line25
        '
        Me.Line25.Height = 0!
        Me.Line25.Left = 2.263999!
        Me.Line25.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line25.LineWeight = 1.0!
        Me.Line25.Name = "Line25"
        Me.Line25.Top = 9.863999!
        Me.Line25.Width = 0.973!
        Me.Line25.X1 = 2.263999!
        Me.Line25.X2 = 3.236999!
        Me.Line25.Y1 = 9.863999!
        Me.Line25.Y2 = 9.863999!
        '
        'Line26
        '
        Me.Line26.Height = 0!
        Me.Line26.Left = 2.254999!
        Me.Line26.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line26.LineWeight = 1.0!
        Me.Line26.Name = "Line26"
        Me.Line26.Top = 10.052!
        Me.Line26.Width = 0.973!
        Me.Line26.X1 = 2.254999!
        Me.Line26.X2 = 3.227999!
        Me.Line26.Y1 = 10.052!
        Me.Line26.Y2 = 10.052!
        '
        'Line27
        '
        Me.Line27.Height = 0!
        Me.Line27.Left = 2.250999!
        Me.Line27.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line27.LineWeight = 1.0!
        Me.Line27.Name = "Line27"
        Me.Line27.Top = 10.239!
        Me.Line27.Width = 0.9729991!
        Me.Line27.X1 = 2.250999!
        Me.Line27.X2 = 3.223998!
        Me.Line27.Y1 = 10.239!
        Me.Line27.Y2 = 10.239!
        '
        'Line28
        '
        Me.Line28.Height = 0!
        Me.Line28.Left = 2.256999!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 10.427!
        Me.Line28.Width = 0.9729991!
        Me.Line28.X1 = 2.256999!
        Me.Line28.X2 = 3.229998!
        Me.Line28.Y1 = 10.427!
        Me.Line28.Y2 = 10.427!
        '
        'Label64
        '
        Me.Label64.Height = 0.188!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 1.005996!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label64.Text = "Option 2B: Doubler plates placed between continuity plates"
        Me.Label64.Top = 10.55!
        Me.Label64.Width = 6.433001!
        '
        'Label65
        '
        Me.Label65.Height = 0.1875!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 3.248997!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label65.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "doubler plate thickness"
        Me.Label65.Top = 10.926!
        Me.Label65.Width = 4.514003!
        '
        'Label66
        '
        Me.Label66.Height = 0.188!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 0!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label66.Text = "t_dblr="
        Me.Label66.Top = 10.926!
        Me.Label66.Width = 2.25!
        '
        'Line29
        '
        Me.Line29.Height = 0.7510004!
        Me.Line29.Left = 3.243!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 10.926!
        Me.Line29.Width = 0!
        Me.Line29.X1 = 3.243!
        Me.Line29.X2 = 3.243!
        Me.Line29.Y1 = 10.926!
        Me.Line29.Y2 = 11.677!
        '
        'Line30
        '
        Me.Line30.Height = 0.7510004!
        Me.Line30.Left = 2.257!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 10.926!
        Me.Line30.Width = 0!
        Me.Line30.X1 = 2.257!
        Me.Line30.X2 = 2.257!
        Me.Line30.Y1 = 10.926!
        Me.Line30.Y2 = 11.677!
        '
        'Line31
        '
        Me.Line31.Height = 0!
        Me.Line31.Left = 2.263997!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 10.926!
        Me.Line31.Width = 0.9729998!
        Me.Line31.X1 = 2.263997!
        Me.Line31.X2 = 3.236997!
        Me.Line31.Y1 = 10.926!
        Me.Line31.Y2 = 10.926!
        '
        'Label68
        '
        Me.Label68.Height = 0.1875!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 3.248997!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label68.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=0.75*0.6*t_dblr * Fy * 1 inch"
        Me.Label68.Top = 11.114!
        Me.Label68.Width = 4.514003!
        '
        'Label69
        '
        Me.Label69.Height = 0.188!
        Me.Label69.HyperLink = Nothing
        Me.Label69.Left = 0!
        Me.Label69.Name = "Label69"
        Me.Label69.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label69.Text = "Vn_doubler="
        Me.Label69.Top = 11.114!
        Me.Label69.Width = 2.25!
        '
        'S342_010
        '
        Me.S342_010.Height = 0.1875!
        Me.S342_010.HyperLink = Nothing
        Me.S342_010.Left = 2.244999!
        Me.S342_010.Name = "S342_010"
        Me.S342_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S342_010.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_010.Top = 11.114!
        Me.S342_010.Width = 1.0!
        '
        'Label71
        '
        Me.Label71.Height = 0.1875!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 3.248997!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label71.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Vn_doubler/1.39, rounded up to 1/16"""
        Me.Label71.Top = 11.302!
        Me.Label71.Width = 4.514003!
        '
        'Label72
        '
        Me.Label72.Height = 0.188!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 0!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label72.Text = "wfillet_use="
        Me.Label72.Top = 11.302!
        Me.Label72.Width = 2.25!
        '
        'S342_011
        '
        Me.S342_011.Height = 0.1875!
        Me.S342_011.HyperLink = Nothing
        Me.S342_011.Left = 2.244999!
        Me.S342_011.Name = "S342_011"
        Me.S342_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S342_011.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S342_011.Top = 11.302!
        Me.S342_011.Width = 1.0!
        '
        'Label74
        '
        Me.Label74.Height = 0.188!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 0!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label74.Text = "# of sides="
        Me.Label74.Top = 11.49!
        Me.Label74.Width = 2.25!
        '
        'Line32
        '
        Me.Line32.Height = 0!
        Me.Line32.Left = 2.263997!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 11.114!
        Me.Line32.Width = 0.9729998!
        Me.Line32.X1 = 2.263997!
        Me.Line32.X2 = 3.236997!
        Me.Line32.Y1 = 11.114!
        Me.Line32.Y2 = 11.114!
        '
        'Line33
        '
        Me.Line33.Height = 0!
        Me.Line33.Left = 2.254997!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 11.302!
        Me.Line33.Width = 0.973!
        Me.Line33.X1 = 2.254997!
        Me.Line33.X2 = 3.227997!
        Me.Line33.Y1 = 11.302!
        Me.Line33.Y2 = 11.302!
        '
        'Line34
        '
        Me.Line34.Height = 0!
        Me.Line34.Left = 2.250997!
        Me.Line34.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line34.LineWeight = 1.0!
        Me.Line34.Name = "Line34"
        Me.Line34.Top = 11.489!
        Me.Line34.Width = 0.9729989!
        Me.Line34.X1 = 2.250997!
        Me.Line34.X2 = 3.223996!
        Me.Line34.Y1 = 11.489!
        Me.Line34.Y2 = 11.489!
        '
        'Label75
        '
        Me.Label75.Height = 0.188!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 1.609999!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; vertical-align: middle;" &
    " ddo-char-set: 1"
        Me.Label75.Text = "75% of the available shear yield strength of the full doubler plate thickness (AI" &
    "SC 341 E3.3(2))"
        Me.Label75.Top = 10.738!
        Me.Label75.Width = 5.829001!
        '
        'Line36
        '
        Me.Line36.Height = 0!
        Me.Line36.Left = 2.256999!
        Me.Line36.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line36.LineWeight = 1.0!
        Me.Line36.Name = "Line36"
        Me.Line36.Top = 11.677!
        Me.Line36.Width = 0.9729991!
        Me.Line36.X1 = 2.256999!
        Me.Line36.X2 = 3.229998!
        Me.Line36.Y1 = 11.677!
        Me.Line36.Y2 = 11.677!
        '
        'Label76
        '
        Me.Label76.Height = 0.1875!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0.01399976!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label76.Text = "3.4.3 DOUBLER TO WEB PLUG WELD (IF REQUIRED)"
        Me.Label76.Top = 11.768!
        Me.Label76.Width = 5.0!
        '
        'S343_005
        '
        Me.S343_005.Height = 0.1875!
        Me.S343_005.HyperLink = Nothing
        Me.S343_005.Left = 2.244999!
        Me.S343_005.Name = "S343_005"
        Me.S343_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S343_005.Text = "2"
        Me.S343_005.Top = 12.707!
        Me.S343_005.Width = 1.0!
        '
        'Label78
        '
        Me.Label78.Height = 0.1875!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 3.248997!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label78.Text = "in, 0.5*dz"
        Me.Label78.Top = 12.143!
        Me.Label78.Width = 4.514003!
        '
        'Label79
        '
        Me.Label79.Height = 0.188!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 2.842171E-14!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label79.Text = "dz_mod="
        Me.Label79.Top = 12.143!
        Me.Label79.Width = 2.25!
        '
        'S343_002
        '
        Me.S343_002.Height = 0.1875!
        Me.S343_002.HyperLink = Nothing
        Me.S343_002.Left = 2.244999!
        Me.S343_002.Name = "S343_002"
        Me.S343_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S343_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S343_002.Top = 12.143!
        Me.S343_002.Width = 1.0!
        '
        'Line35
        '
        Me.Line35.Height = 0.7510004!
        Me.Line35.Left = 3.243!
        Me.Line35.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line35.LineWeight = 1.0!
        Me.Line35.Name = "Line35"
        Me.Line35.Top = 12.143!
        Me.Line35.Width = 0!
        Me.Line35.X1 = 3.243!
        Me.Line35.X2 = 3.243!
        Me.Line35.Y1 = 12.143!
        Me.Line35.Y2 = 12.894!
        '
        'Line37
        '
        Me.Line37.Height = 0.7510004!
        Me.Line37.Left = 2.257!
        Me.Line37.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line37.LineWeight = 1.0!
        Me.Line37.Name = "Line37"
        Me.Line37.Top = 12.143!
        Me.Line37.Width = 0!
        Me.Line37.X1 = 2.257!
        Me.Line37.X2 = 2.257!
        Me.Line37.Y1 = 12.143!
        Me.Line37.Y2 = 12.894!
        '
        'Line38
        '
        Me.Line38.Height = 0!
        Me.Line38.Left = 2.263997!
        Me.Line38.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line38.LineWeight = 1.0!
        Me.Line38.Name = "Line38"
        Me.Line38.Top = 12.143!
        Me.Line38.Width = 0.9729998!
        Me.Line38.X1 = 2.263997!
        Me.Line38.X2 = 3.236997!
        Me.Line38.Y1 = 12.143!
        Me.Line38.Y2 = 12.143!
        '
        'Label81
        '
        Me.Label81.Height = 0.1875!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 3.248997!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label81.Text = "in, 0.5*wz"
        Me.Label81.Top = 12.331!
        Me.Label81.Width = 4.514003!
        '
        'Label82
        '
        Me.Label82.Height = 0.188!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 2.842171E-14!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label82.Text = "wz_mod="
        Me.Label82.Top = 12.331!
        Me.Label82.Width = 2.25!
        '
        'S343_003
        '
        Me.S343_003.Height = 0.1875!
        Me.S343_003.HyperLink = Nothing
        Me.S343_003.Left = 2.244999!
        Me.S343_003.Name = "S343_003"
        Me.S343_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S343_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S343_003.Top = 12.331!
        Me.S343_003.Width = 1.0!
        '
        'Label85
        '
        Me.Label85.Height = 0.188!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 2.842171E-14!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label85.Text = "(dz_mod+wz_mod)/90"
        Me.Label85.Top = 12.519!
        Me.Label85.Width = 2.25!
        '
        'S343_004
        '
        Me.S343_004.Height = 0.1875!
        Me.S343_004.HyperLink = Nothing
        Me.S343_004.Left = 2.244999!
        Me.S343_004.Name = "S343_004"
        Me.S343_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S343_004.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S343_004.Top = 12.519!
        Me.S343_004.Width = 1.0!
        '
        'Label87
        '
        Me.Label87.Height = 0.188!
        Me.Label87.HyperLink = Nothing
        Me.Label87.Left = 2.842171E-14!
        Me.Label87.Name = "Label87"
        Me.Label87.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label87.Text = "PlugWeld_Dia="
        Me.Label87.Top = 12.707!
        Me.Label87.Width = 2.25!
        '
        'Line39
        '
        Me.Line39.Height = 0!
        Me.Line39.Left = 2.263997!
        Me.Line39.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line39.LineWeight = 1.0!
        Me.Line39.Name = "Line39"
        Me.Line39.Top = 12.331!
        Me.Line39.Width = 0.9729998!
        Me.Line39.X1 = 2.263997!
        Me.Line39.X2 = 3.236997!
        Me.Line39.Y1 = 12.331!
        Me.Line39.Y2 = 12.331!
        '
        'Line40
        '
        Me.Line40.Height = 0!
        Me.Line40.Left = 2.254997!
        Me.Line40.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line40.LineWeight = 1.0!
        Me.Line40.Name = "Line40"
        Me.Line40.Top = 12.519!
        Me.Line40.Width = 0.973!
        Me.Line40.X1 = 2.254997!
        Me.Line40.X2 = 3.227997!
        Me.Line40.Y1 = 12.519!
        Me.Line40.Y2 = 12.519!
        '
        'Line41
        '
        Me.Line41.Height = 0!
        Me.Line41.Left = 2.250997!
        Me.Line41.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line41.LineWeight = 1.0!
        Me.Line41.Name = "Line41"
        Me.Line41.Top = 12.706!
        Me.Line41.Width = 0.9729989!
        Me.Line41.X1 = 2.250997!
        Me.Line41.X2 = 3.223996!
        Me.Line41.Y1 = 12.706!
        Me.Line41.Y2 = 12.706!
        '
        'Line42
        '
        Me.Line42.Height = 0!
        Me.Line42.Left = 2.256999!
        Me.Line42.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line42.LineWeight = 1.0!
        Me.Line42.Name = "Line42"
        Me.Line42.Top = 12.894!
        Me.Line42.Width = 0.973!
        Me.Line42.X1 = 2.256999!
        Me.Line42.X2 = 3.229999!
        Me.Line42.Y1 = 12.894!
        Me.Line42.Y2 = 12.894!
        '
        'Label89
        '
        Me.Label89.Height = 0.188!
        Me.Label89.HyperLink = Nothing
        Me.Label89.Left = 0.01500022!
        Me.Label89.Name = "Label89"
        Me.Label89.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label89.Text = "Doubler Plate Plug-Weld Used?"
        Me.Label89.Top = 11.955!
        Me.Label89.Width = 2.25!
        '
        'Label84
        '
        Me.Label84.Height = 0.1875!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 3.249!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label84.Text = "in"
        Me.Label84.Top = 12.708!
        Me.Label84.Width = 0.9879999!
        '
        'S343_007
        '
        Me.S343_007.Height = 0.1875!
        Me.S343_007.HyperLink = Nothing
        Me.S343_007.Left = 5.578!
        Me.S343_007.Name = "S343_007"
        Me.S343_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S343_007.Text = "2"
        Me.S343_007.Top = 12.705!
        Me.S343_007.Width = 1.0!
        '
        'Line43
        '
        Me.Line43.Height = 0.1859999!
        Me.Line43.Left = 6.576!
        Me.Line43.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line43.LineWeight = 1.0!
        Me.Line43.Name = "Line43"
        Me.Line43.Top = 12.706!
        Me.Line43.Width = 0!
        Me.Line43.X1 = 6.576!
        Me.Line43.X2 = 6.576!
        Me.Line43.Y1 = 12.706!
        Me.Line43.Y2 = 12.892!
        '
        'Line44
        '
        Me.Line44.Height = 0.1859999!
        Me.Line44.Left = 5.590001!
        Me.Line44.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line44.LineWeight = 1.0!
        Me.Line44.Name = "Line44"
        Me.Line44.Top = 12.706!
        Me.Line44.Width = 0!
        Me.Line44.X1 = 5.590001!
        Me.Line44.X2 = 5.590001!
        Me.Line44.Y1 = 12.706!
        Me.Line44.Y2 = 12.892!
        '
        'Label92
        '
        Me.Label92.Height = 0.188!
        Me.Label92.HyperLink = Nothing
        Me.Label92.Left = 4.249001!
        Me.Label92.Name = "Label92"
        Me.Label92.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label92.Text = "Weld Depth="
        Me.Label92.Top = 12.705!
        Me.Label92.Width = 1.334001!
        '
        'Line45
        '
        Me.Line45.Height = 0!
        Me.Line45.Left = 5.59!
        Me.Line45.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line45.LineWeight = 1.0!
        Me.Line45.Name = "Line45"
        Me.Line45.Top = 12.892!
        Me.Line45.Width = 0.9860001!
        Me.Line45.X1 = 5.59!
        Me.Line45.X2 = 6.576!
        Me.Line45.Y1 = 12.892!
        Me.Line45.Y2 = 12.892!
        '
        'Label93
        '
        Me.Label93.Height = 0.1875!
        Me.Label93.HyperLink = Nothing
        Me.Label93.Left = 6.582003!
        Me.Label93.Name = "Label93"
        Me.Label93.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label93.Text = "in"
        Me.Label93.Top = 12.706!
        Me.Label93.Width = 0.988!
        '
        'Line46
        '
        Me.Line46.Height = 0!
        Me.Line46.Left = 5.59!
        Me.Line46.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line46.LineWeight = 1.0!
        Me.Line46.Name = "Line46"
        Me.Line46.Top = 12.706!
        Me.Line46.Width = 0.9860001!
        Me.Line46.X1 = 5.59!
        Me.Line46.X2 = 6.576!
        Me.Line46.Y1 = 12.706!
        Me.Line46.Y2 = 12.706!
        '
        'Label95
        '
        Me.Label95.Height = 0.1875!
        Me.Label95.HyperLink = Nothing
        Me.Label95.Left = 0.01399714!
        Me.Label95.Name = "Label95"
        Me.Label95.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label95.Text = "3.4.4 DOUBLER TO COLUMN FLANGE WELD"
        Me.Label95.Top = 13.018!
        Me.Label95.Width = 5.0!
        '
        'Label96
        '
        Me.Label96.Height = 0.188!
        Me.Label96.HyperLink = Nothing
        Me.Label96.Left = 1.005997!
        Me.Label96.Name = "Label96"
        Me.Label96.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label96.Text = "Option 1: Fillet weld to develop doubler plate shear capacity"
        Me.Label96.Top = 13.206!
        Me.Label96.Width = 6.433001!
        '
        'Label97
        '
        Me.Label97.Height = 0.1875!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 3.248997!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label97.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "doubler plate thickness"
        Me.Label97.Top = 13.394!
        Me.Label97.Width = 4.514003!
        '
        'Label98
        '
        Me.Label98.Height = 0.188!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 2.842171E-14!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label98.Text = "t_dblr="
        Me.Label98.Top = 13.394!
        Me.Label98.Width = 2.25!
        '
        'S344_001
        '
        Me.S344_001.Height = 0.1875!
        Me.S344_001.HyperLink = Nothing
        Me.S344_001.Left = 2.244999!
        Me.S344_001.Name = "S344_001"
        Me.S344_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S344_001.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S344_001.Top = 13.394!
        Me.S344_001.Width = 1.0!
        '
        'Line47
        '
        Me.Line47.Height = 0.5629997!
        Me.Line47.Left = 3.243!
        Me.Line47.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line47.LineWeight = 1.0!
        Me.Line47.Name = "Line47"
        Me.Line47.Top = 13.394!
        Me.Line47.Width = 0!
        Me.Line47.X1 = 3.243!
        Me.Line47.X2 = 3.243!
        Me.Line47.Y1 = 13.394!
        Me.Line47.Y2 = 13.957!
        '
        'Line48
        '
        Me.Line48.Height = 0.5629997!
        Me.Line48.Left = 2.257!
        Me.Line48.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line48.LineWeight = 1.0!
        Me.Line48.Name = "Line48"
        Me.Line48.Top = 13.394!
        Me.Line48.Width = 0!
        Me.Line48.X1 = 2.257!
        Me.Line48.X2 = 2.257!
        Me.Line48.Y1 = 13.394!
        Me.Line48.Y2 = 13.957!
        '
        'Line49
        '
        Me.Line49.Height = 0!
        Me.Line49.Left = 2.263997!
        Me.Line49.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line49.LineWeight = 1.0!
        Me.Line49.Name = "Line49"
        Me.Line49.Top = 13.394!
        Me.Line49.Width = 0.9729998!
        Me.Line49.X1 = 2.263997!
        Me.Line49.X2 = 3.236997!
        Me.Line49.Y1 = 13.394!
        Me.Line49.Y2 = 13.394!
        '
        'Label100
        '
        Me.Label100.Height = 0.1875!
        Me.Label100.HyperLink = Nothing
        Me.Label100.Left = 3.248997!
        Me.Label100.Name = "Label100"
        Me.Label100.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label100.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "0.6*Fy*Agv  (per 1 in length)"
        Me.Label100.Top = 13.582!
        Me.Label100.Width = 4.514003!
        '
        'Label101
        '
        Me.Label101.Height = 0.188!
        Me.Label101.HyperLink = Nothing
        Me.Label101.Left = 2.842171E-14!
        Me.Label101.Name = "Label101"
        Me.Label101.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label101.Text = "Vn="
        Me.Label101.Top = 13.582!
        Me.Label101.Width = 2.25!
        '
        'S344_002
        '
        Me.S344_002.Height = 0.1875!
        Me.S344_002.HyperLink = Nothing
        Me.S344_002.Left = 2.244999!
        Me.S344_002.Name = "S344_002"
        Me.S344_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S344_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S344_002.Top = 13.582!
        Me.S344_002.Width = 1.0!
        '
        'Label103
        '
        Me.Label103.Height = 0.1875!
        Me.Label103.HyperLink = Nothing
        Me.Label103.Left = 3.248997!
        Me.Label103.Name = "Label103"
        Me.Label103.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label103.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Vn/1.39, rounded up to 1/16"""
        Me.Label103.Top = 13.77!
        Me.Label103.Width = 4.514003!
        '
        'Label104
        '
        Me.Label104.Height = 0.188!
        Me.Label104.HyperLink = Nothing
        Me.Label104.Left = 2.842171E-14!
        Me.Label104.Name = "Label104"
        Me.Label104.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label104.Text = "wfillet="
        Me.Label104.Top = 13.77!
        Me.Label104.Width = 2.25!
        '
        'S344_003
        '
        Me.S344_003.Height = 0.1875!
        Me.S344_003.HyperLink = Nothing
        Me.S344_003.Left = 2.244999!
        Me.S344_003.Name = "S344_003"
        Me.S344_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S344_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S344_003.Top = 13.77!
        Me.S344_003.Width = 1.0!
        '
        'Line50
        '
        Me.Line50.Height = 0!
        Me.Line50.Left = 2.263997!
        Me.Line50.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line50.LineWeight = 1.0!
        Me.Line50.Name = "Line50"
        Me.Line50.Top = 13.582!
        Me.Line50.Width = 0.9729998!
        Me.Line50.X1 = 2.263997!
        Me.Line50.X2 = 3.236997!
        Me.Line50.Y1 = 13.582!
        Me.Line50.Y2 = 13.582!
        '
        'Line51
        '
        Me.Line51.Height = 0!
        Me.Line51.Left = 2.254997!
        Me.Line51.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line51.LineWeight = 1.0!
        Me.Line51.Name = "Line51"
        Me.Line51.Top = 13.77!
        Me.Line51.Width = 0.973!
        Me.Line51.X1 = 2.254997!
        Me.Line51.X2 = 3.227997!
        Me.Line51.Y1 = 13.77!
        Me.Line51.Y2 = 13.77!
        '
        'Line52
        '
        Me.Line52.Height = 0!
        Me.Line52.Left = 2.256!
        Me.Line52.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line52.LineWeight = 1.0!
        Me.Line52.Name = "Line52"
        Me.Line52.Top = 13.957!
        Me.Line52.Width = 0.9729989!
        Me.Line52.X1 = 2.256!
        Me.Line52.X2 = 3.228999!
        Me.Line52.Y1 = 13.957!
        Me.Line52.Y2 = 13.957!
        '
        'Label94
        '
        Me.Label94.Height = 0.188!
        Me.Label94.HyperLink = Nothing
        Me.Label94.Left = 1.006!
        Me.Label94.Name = "Label94"
        Me.Label94.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label94.Text = "Option 2: Groove weld per AWS D1.8 Clause 4.3"
        Me.Label94.Top = 14.029!
        Me.Label94.Width = 6.433001!
        '
        'S35_006
        '
        Me.S35_006.Height = 0.1875!
        Me.S35_006.HyperLink = Nothing
        Me.S35_006.Left = 2.245!
        Me.S35_006.Name = "S35_006"
        Me.S35_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S35_006.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_006.Top = 15.656!
        Me.S35_006.Width = 1.0!
        '
        'S35_011
        '
        Me.S35_011.Height = 0.1875!
        Me.S35_011.HyperLink = Nothing
        Me.S35_011.Left = 3.251!
        Me.S35_011.Name = "S35_011"
        Me.S35_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S35_011.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_011.Top = 15.667!
        Me.S35_011.Width = 1.004!
        '
        'S35_010
        '
        Me.S35_010.Height = 0.1875!
        Me.S35_010.HyperLink = Nothing
        Me.S35_010.Left = 3.251!
        Me.S35_010.Name = "S35_010"
        Me.S35_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S35_010.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_010.Top = 15.468!
        Me.S35_010.Width = 1.0!
        '
        'S35_005
        '
        Me.S35_005.Height = 0.1875!
        Me.S35_005.HyperLink = Nothing
        Me.S35_005.Left = 2.251001!
        Me.S35_005.Name = "S35_005"
        Me.S35_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S35_005.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_005.Top = 15.468!
        Me.S35_005.Width = 1.0!
        '
        'Label245
        '
        Me.Label245.Height = 0.1875!
        Me.Label245.HyperLink = Nothing
        Me.Label245.Left = 0.0009999996!
        Me.Label245.Name = "Label245"
        Me.Label245.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label245.Text = "3.5 CHECK UNSTIFFENED COLUMN FLANGE AND WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label245.Top = 14.342!
        Me.Label245.Width = 5.0!
        '
        'Label246
        '
        Me.Label246.Height = 0.188!
        Me.Label246.HyperLink = Nothing
        Me.Label246.Left = 0.001000238!
        Me.Label246.Name = "Label246"
        Me.Label246.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label246.Text = "dbm=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label246.Top = 14.905!
        Me.Label246.Width = 2.25!
        '
        'S35_002
        '
        Me.S35_002.Height = 0.1875!
        Me.S35_002.HyperLink = Nothing
        Me.S35_002.Left = 2.251!
        Me.S35_002.Name = "S35_002"
        Me.S35_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S35_002.Text = "0.90" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_002.Top = 14.905!
        Me.S35_002.Width = 1.0!
        '
        'Label248
        '
        Me.Label248.Height = 0.188!
        Me.Label248.HyperLink = Nothing
        Me.Label248.Left = 0.0009999996!
        Me.Label248.Name = "Label248"
        Me.Label248.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label248.Text = "tbf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label248.Top = 15.093!
        Me.Label248.Width = 2.25!
        '
        'S35_003
        '
        Me.S35_003.Height = 0.1875!
        Me.S35_003.HyperLink = Nothing
        Me.S35_003.Left = 2.251!
        Me.S35_003.Name = "S35_003"
        Me.S35_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S35_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_003.Top = 15.093!
        Me.S35_003.Width = 1.0!
        '
        'S35_007
        '
        Me.S35_007.Height = 0.1875!
        Me.S35_007.HyperLink = Nothing
        Me.S35_007.Left = 3.250999!
        Me.S35_007.Name = "S35_007"
        Me.S35_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S35_007.Text = "0.90" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_007.Top = 14.905!
        Me.S35_007.Width = 1.0!
        '
        'S35_008
        '
        Me.S35_008.Height = 0.1875!
        Me.S35_008.HyperLink = Nothing
        Me.S35_008.Left = 3.250999!
        Me.S35_008.Name = "S35_008"
        Me.S35_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S35_008.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_008.Top = 15.093!
        Me.S35_008.Width = 1.0!
        '
        'Label252
        '
        Me.Label252.Height = 0.1875!
        Me.Label252.HyperLink = Nothing
        Me.Label252.Left = 3.251001!
        Me.Label252.Name = "Label252"
        Me.Label252.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label252.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label252.Top = 14.718!
        Me.Label252.Width = 1.0!
        '
        'Label253
        '
        Me.Label253.Height = 0.1875!
        Me.Label253.HyperLink = Nothing
        Me.Label253.Left = 2.251001!
        Me.Label253.Name = "Label253"
        Me.Label253.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label253.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label253.Top = 14.718!
        Me.Label253.Width = 1.0!
        '
        'Line64
        '
        Me.Line64.Height = 0!
        Me.Line64.Left = 2.250999!
        Me.Line64.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line64.LineWeight = 1.0!
        Me.Line64.Name = "Line64"
        Me.Line64.Top = 15.093!
        Me.Line64.Width = 2.0!
        Me.Line64.X1 = 2.250999!
        Me.Line64.X2 = 4.250999!
        Me.Line64.Y1 = 15.093!
        Me.Line64.Y2 = 15.093!
        '
        'Line66
        '
        Me.Line66.Height = 0!
        Me.Line66.Left = 2.251!
        Me.Line66.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line66.LineWeight = 1.0!
        Me.Line66.Name = "Line66"
        Me.Line66.Top = 14.905!
        Me.Line66.Width = 1.999999!
        Me.Line66.X1 = 2.251!
        Me.Line66.X2 = 4.250999!
        Me.Line66.Y1 = 14.905!
        Me.Line66.Y2 = 14.905!
        '
        'Label254
        '
        Me.Label254.Height = 0.1875!
        Me.Label254.HyperLink = Nothing
        Me.Label254.Left = 4.250999!
        Me.Label254.Name = "Label254"
        Me.Label254.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label254.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label254.Top = 15.093!
        Me.Label254.Width = 3.313!
        '
        'Label255
        '
        Me.Label255.Height = 0.1875!
        Me.Label255.HyperLink = Nothing
        Me.Label255.Left = 4.250999!
        Me.Label255.Name = "Label255"
        Me.Label255.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label255.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "dbeam" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label255.Top = 14.905!
        Me.Label255.Width = 3.75!
        '
        'Line76
        '
        Me.Line76.Height = 0!
        Me.Line76.Left = 2.251001!
        Me.Line76.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line76.LineWeight = 1.0!
        Me.Line76.Name = "Line76"
        Me.Line76.Top = 14.718!
        Me.Line76.Width = 1.999998!
        Me.Line76.X1 = 2.251001!
        Me.Line76.X2 = 4.250999!
        Me.Line76.Y1 = 14.718!
        Me.Line76.Y2 = 14.718!
        '
        'Label256
        '
        Me.Label256.Height = 0.188!
        Me.Label256.HyperLink = Nothing
        Me.Label256.Left = 0.001000477!
        Me.Label256.Name = "Label256"
        Me.Label256.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label256.Text = "tbf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label256.Top = 15.093!
        Me.Label256.Width = 2.25!
        '
        'Label259
        '
        Me.Label259.Height = 0.1875!
        Me.Label259.HyperLink = Nothing
        Me.Label259.Left = 4.250999!
        Me.Label259.Name = "Label259"
        Me.Label259.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label259.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label259.Top = 15.093!
        Me.Label259.Width = 3.313!
        '
        'Label260
        '
        Me.Label260.Height = 0.188!
        Me.Label260.HyperLink = Nothing
        Me.Label260.Left = 0.0009999996!
        Me.Label260.Name = "Label260"
        Me.Label260.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label260.Text = "d_stiffTop=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label260.Top = 15.468!
        Me.Label260.Width = 2.25!
        '
        'S35_004
        '
        Me.S35_004.Height = 0.1875!
        Me.S35_004.HyperLink = Nothing
        Me.S35_004.Left = 2.251!
        Me.S35_004.Name = "S35_004"
        Me.S35_004.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S35_004.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_004.Top = 15.281!
        Me.S35_004.Width = 1.0!
        '
        'S35_009
        '
        Me.S35_009.Height = 0.1875!
        Me.S35_009.HyperLink = Nothing
        Me.S35_009.Left = 3.250998!
        Me.S35_009.Name = "S35_009"
        Me.S35_009.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S35_009.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S35_009.Top = 15.281!
        Me.S35_009.Width = 1.0!
        '
        'Label263
        '
        Me.Label263.Height = 0.3240004!
        Me.Label263.HyperLink = Nothing
        Me.Label263.Left = 4.257!
        Me.Label263.Name = "Label263"
        Me.Label263.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label263.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Loc. of top stifferener to top of column cap," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                   assume 2 dc " &
    "if not at top story"
        Me.Label263.Top = 15.416!
        Me.Label263.Width = 3.313!
        '
        'Label264
        '
        Me.Label264.Height = 0.188!
        Me.Label264.HyperLink = Nothing
        Me.Label264.Left = 0.001000715!
        Me.Label264.Name = "Label264"
        Me.Label264.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label264.Text = "d_stiffBot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label264.Top = 15.655!
        Me.Label264.Width = 2.25!
        '
        'Label267
        '
        Me.Label267.Height = 0.1759996!
        Me.Label267.HyperLink = Nothing
        Me.Label267.Left = 4.249!
        Me.Label267.Name = "Label267"
        Me.Label267.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label267.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Loc. of bottom stiffener to top of column cap = d + d_stiffTop " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label267.Top = 15.667!
        Me.Label267.Width = 3.75!
        '
        'Line74
        '
        Me.Line74.Height = 1.125!
        Me.Line74.Left = 3.251!
        Me.Line74.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line74.LineWeight = 1.0!
        Me.Line74.Name = "Line74"
        Me.Line74.Top = 14.718!
        Me.Line74.Width = 0!
        Me.Line74.X1 = 3.251!
        Me.Line74.X2 = 3.251!
        Me.Line74.Y1 = 14.718!
        Me.Line74.Y2 = 15.843!
        '
        'Line77
        '
        Me.Line77.Height = 0!
        Me.Line77.Left = 2.251!
        Me.Line77.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line77.LineWeight = 1.0!
        Me.Line77.Name = "Line77"
        Me.Line77.Top = 15.281!
        Me.Line77.Width = 1.999999!
        Me.Line77.X1 = 2.251!
        Me.Line77.X2 = 4.250999!
        Me.Line77.Y1 = 15.281!
        Me.Line77.Y2 = 15.281!
        '
        'Line78
        '
        Me.Line78.Height = 0!
        Me.Line78.Left = 2.251001!
        Me.Line78.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line78.LineWeight = 1.0!
        Me.Line78.Name = "Line78"
        Me.Line78.Top = 15.468!
        Me.Line78.Width = 1.999998!
        Me.Line78.X1 = 2.251001!
        Me.Line78.X2 = 4.250999!
        Me.Line78.Y1 = 15.468!
        Me.Line78.Y2 = 15.468!
        '
        'Line79
        '
        Me.Line79.Height = 0!
        Me.Line79.Left = 2.251!
        Me.Line79.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line79.LineWeight = 1.0!
        Me.Line79.Name = "Line79"
        Me.Line79.Top = 15.468!
        Me.Line79.Width = 1.999999!
        Me.Line79.X1 = 2.251!
        Me.Line79.X2 = 4.250999!
        Me.Line79.Y1 = 15.468!
        Me.Line79.Y2 = 15.468!
        '
        'Label106
        '
        Me.Label106.Height = 0.188!
        Me.Label106.HyperLink = Nothing
        Me.Label106.Left = 0.001999762!
        Me.Label106.Name = "Label106"
        Me.Label106.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label106.Text = "SMF_Sides=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label106.Top = 14.53!
        Me.Label106.Width = 2.25!
        '
        'Line221
        '
        Me.Line221.Height = 0!
        Me.Line221.Left = 2.251!
        Me.Line221.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line221.LineWeight = 1.0!
        Me.Line221.Name = "Line221"
        Me.Line221.Top = 15.656!
        Me.Line221.Width = 1.999998!
        Me.Line221.X1 = 2.251!
        Me.Line221.X2 = 4.250998!
        Me.Line221.Y1 = 15.656!
        Me.Line221.Y2 = 15.656!
        '
        'Line53
        '
        Me.Line53.Height = 0!
        Me.Line53.Left = 2.251!
        Me.Line53.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line53.LineWeight = 1.0!
        Me.Line53.Name = "Line53"
        Me.Line53.Top = 15.843!
        Me.Line53.Width = 1.999998!
        Me.Line53.X1 = 2.251!
        Me.Line53.X2 = 4.250998!
        Me.Line53.Y1 = 15.843!
        Me.Line53.Y2 = 15.843!
        '
        'Label107
        '
        Me.Label107.Height = 0.188!
        Me.Label107.HyperLink = Nothing
        Me.Label107.Left = 0.0009999996!
        Me.Label107.Name = "Label107"
        Me.Label107.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label107.Text = "d_stiff_cap=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label107.Top = 15.28!
        Me.Label107.Width = 2.25!
        '
        'Line68
        '
        Me.Line68.Height = 1.125!
        Me.Line68.Left = 2.251!
        Me.Line68.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line68.LineWeight = 1.0!
        Me.Line68.Name = "Line68"
        Me.Line68.Top = 14.718!
        Me.Line68.Width = 0!
        Me.Line68.X1 = 2.251!
        Me.Line68.X2 = 2.251!
        Me.Line68.Y1 = 14.718!
        Me.Line68.Y2 = 15.843!
        '
        'Line54
        '
        Me.Line54.Height = 0!
        Me.Line54.Left = 2.263999!
        Me.Line54.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line54.LineWeight = 1.0!
        Me.Line54.Name = "Line54"
        Me.Line54.Top = 10.926!
        Me.Line54.Width = 0.973!
        Me.Line54.X1 = 2.263999!
        Me.Line54.X2 = 3.236999!
        Me.Line54.Y1 = 10.926!
        Me.Line54.Y2 = 10.926!
        '
        'Line62
        '
        Me.Line62.Height = 0!
        Me.Line62.Left = 2.255!
        Me.Line62.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line62.LineWeight = 1.0!
        Me.Line62.Name = "Line62"
        Me.Line62.Top = 7.056003!
        Me.Line62.Width = 2.0!
        Me.Line62.X1 = 2.255!
        Me.Line62.X2 = 4.255!
        Me.Line62.Y1 = 7.056003!
        Me.Line62.Y2 = 7.056003!
        '
        'Label41
        '
        Me.Label41.Height = 0.1875!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 4.251!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label41.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label41.Top = 15.281!
        Me.Label41.Width = 3.313!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 2.25!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 4.458!
        Me.Shape2.Width = 1.0!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.1875!
        Me.Shape3.Left = 2.25!
        Me.Shape3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 4.002!
        Me.Shape3.Width = 1.0!
        '
        'Shape5
        '
        Me.Shape5.Height = 0.1875!
        Me.Shape5.Left = 5.577!
        Me.Shape5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 4.457!
        Me.Shape5.Width = 1.0!
        '
        'Shape10
        '
        Me.Shape10.Height = 0.1875!
        Me.Shape10.Left = 2.251!
        Me.Shape10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape10.Name = "Shape10"
        Me.Shape10.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape10.Top = 14.529!
        Me.Shape10.Width = 1.0!
        '
        'Shape9
        '
        Me.Shape9.Height = 0.1875!
        Me.Shape9.Left = 3.244!
        Me.Shape9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape9.Name = "Shape9"
        Me.Shape9.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape9.Top = 12.52!
        Me.Shape9.Width = 1.0!
        '
        'Shape6
        '
        Me.Shape6.Height = 0.1875!
        Me.Shape6.Left = 2.25!
        Me.Shape6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 11.958!
        Me.Shape6.Width = 1.0!
        '
        'Line75
        '
        Me.Line75.Height = 1.125!
        Me.Line75.Left = 4.250999!
        Me.Line75.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line75.LineWeight = 1.0!
        Me.Line75.Name = "Line75"
        Me.Line75.Top = 14.718!
        Me.Line75.Width = 0!
        Me.Line75.X1 = 4.250999!
        Me.Line75.X2 = 4.250999!
        Me.Line75.Y1 = 14.718!
        Me.Line75.Y2 = 15.843!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.1979167!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(6.5!, 0.1979167!)
        Me.PageBreak1.Top = 8.260417!
        Me.PageBreak1.Width = 6.5!
        '
        'US_CC2
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.5!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S343_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S343_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label205, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label206, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label189, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label193, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label201, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label336, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label337, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label338, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label340, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label125, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label129, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label131, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label134, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label136, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label139, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label141, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label142, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label144, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label146, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label148, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label151, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label153, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label154, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label156, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label158, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label160, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label163, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label164, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label165, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label166, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label191, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label208, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label212, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label216, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label217, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label223, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label213, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label225, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label233, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label241, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label243, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S34_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S341_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label69, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S342_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S343_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S343_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S343_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S343_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label89, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S343_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label92, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label93, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label95, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S344_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label100, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label101, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S344_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label103, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label104, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S344_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label94, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label245, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label246, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label248, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label252, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label253, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label254, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label255, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label256, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label259, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label260, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S35_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label263, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label264, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label267, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label106, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label107, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents S34_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label189 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label193 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label196 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label199 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label201 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label336 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label337 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label338 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label340 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line57 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line58 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label125 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label129 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label131 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label134 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label136 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label139 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label141 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label142 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label144 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label146 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label148 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label151 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label153 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label154 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label156 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label158 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label160 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label163 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label164 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label165 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label166 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label191 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label205 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label206 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line62 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label208 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label212 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label216 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label217 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label223 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label213 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label225 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label233 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line63 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line67 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line69 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line73 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line70 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line61 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label241 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label243 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line18 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line19 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line20 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S34_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape4 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S341_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line21 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S342_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label53 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line22 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line23 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line24 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line25 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line26 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line27 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S342_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label68 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label69 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label71 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label72 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S342_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line34 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label75 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line36 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S343_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S343_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label78 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S343_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line35 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line37 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line38 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S343_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S343_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label87 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line39 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line40 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line41 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line42 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label89 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label84 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape8 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape9 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S343_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S343_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line43 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line44 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label92 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line45 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label93 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line46 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label95 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label96 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label97 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S344_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line47 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line48 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line49 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label100 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label101 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S344_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label103 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label104 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S344_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line50 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line51 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line52 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label94 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label245 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label246 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label248 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label252 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label253 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line64 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line66 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label254 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label255 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line76 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label256 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label259 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label260 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S35_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label263 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label264 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label267 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line74 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line75 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line77 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line78 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line79 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S35_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label106 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line221 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line53 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label107 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line68 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Shape10 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Line54 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
End Class
