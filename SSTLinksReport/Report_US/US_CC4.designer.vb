﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_CC4
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_CC4))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S39_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S312_036 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape3 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S310_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label574 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label570 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label566 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label562 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label558 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label554 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label550 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label546 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label519 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label518 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S311_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label379 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label362 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label380 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label383 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label385 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label381 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label386 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label387 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label389 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label390 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label391 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label393 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label394 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label396 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label397 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label399 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label400 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label401 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label402 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label404 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label405 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label408 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label407 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label411 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label412 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label414 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label415 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label416 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label418 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label419 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label421 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label422 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label424 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label425 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label426 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label428 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label429 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label431 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label432 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label434 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label435 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label436 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label437 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label439 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label442 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label444 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label446 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label449 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label450 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label451 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label453 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label457 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label458 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line112 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line113 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label459 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label460 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label463 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label464 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label467 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line114 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line115 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line116 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label468 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label471 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label472 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label475 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label476 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label479 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label480 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label483 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label484 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label487 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label488 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label491 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label492 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label495 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label496 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label499 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line120 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line121 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line122 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line123 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line124 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line126 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line128 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line129 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S310_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S310_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line117 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line118 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line119 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label503 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label502 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S311_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label505 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S311_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S311_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S311_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label509 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label510 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line130 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line131 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label511 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label512 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S311_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label515 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line132 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line133 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line134 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line137 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label516 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line135 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S312_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label535 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label534 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label517 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label520 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label522 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label524 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label525 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label527 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label528 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label530 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line138 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line139 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line144 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label536 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label537 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label539 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label540 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label542 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label543 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label545 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line143 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label547 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label551 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label555 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label559 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label563 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label567 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label571 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label575 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label579 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label583 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_035 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label587 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label588 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label593 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label594 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label596 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label597 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S312_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label590 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label591 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line145 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line146 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line147 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line148 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line149 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line150 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line151 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line152 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line153 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line154 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line155 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line141 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line142 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S39_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line140 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape7 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        CType(Me.S39_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_036, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label574, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label570, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label566, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label562, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label558, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label554, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label550, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label546, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label519, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label518, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S311_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label379, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label362, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label380, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label383, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label385, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label381, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label386, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label387, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label389, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label390, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label391, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label393, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label394, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label396, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label397, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label399, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label400, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label401, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label402, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label404, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label405, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label408, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label407, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label411, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label412, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label414, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label415, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label416, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label418, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label419, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label421, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label422, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label424, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label425, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label426, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label428, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label429, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label431, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label432, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label434, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label435, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label436, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label437, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label439, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label442, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label444, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label446, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label449, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label450, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label451, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label453, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label457, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label458, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label459, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label460, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label463, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label464, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label467, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label468, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label471, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label472, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label475, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label476, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label479, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label480, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label483, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label484, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label487, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label488, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label491, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label492, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label495, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label496, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label499, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S310_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label503, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label502, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S311_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label505, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S311_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S311_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S311_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label509, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label510, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label511, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label512, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S311_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label515, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label516, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label535, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label534, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label517, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label520, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label522, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label524, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label525, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label527, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label528, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label530, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label536, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label537, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label539, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label540, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label542, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label543, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label545, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label547, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label551, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label555, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label559, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label563, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label567, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label571, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label575, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label579, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_034, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label583, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_035, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label587, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label588, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label593, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label594, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label596, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label597, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S312_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label590, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label591, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S39_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S39_019, Me.S39_026, Me.S312_019, Me.Shape2, Me.S312_036, Me.Shape3, Me.S310_023, Me.S310_011, Me.S310_012, Me.S310_024, Me.Label34, Me.Label32, Me.Label574, Me.Label570, Me.Label566, Me.Label562, Me.Label558, Me.Label554, Me.Label550, Me.Label546, Me.Label519, Me.Label518, Me.S311_003, Me.Label379, Me.Label362, Me.Label380, Me.Label383, Me.S39_001, Me.Label385, Me.Label381, Me.S39_002, Me.Label386, Me.Label387, Me.S39_003, Me.Label389, Me.Label390, Me.Label391, Me.S39_013, Me.Label393, Me.Label394, Me.S39_014, Me.Label396, Me.Label397, Me.S39_015, Me.Label399, Me.Label400, Me.Label401, Me.Label402, Me.S39_004, Me.Label404, Me.Label405, Me.S39_005, Me.Label408, Me.S39_006, Me.Label407, Me.S39_016, Me.Label411, Me.Label412, Me.Label414, Me.Label415, Me.Label416, Me.S39_007, Me.Label418, Me.Label419, Me.S39_008, Me.Label421, Me.Label422, Me.S39_009, Me.Label424, Me.Label425, Me.Label426, Me.S39_020, Me.Label428, Me.Label429, Me.S39_021, Me.Label431, Me.Label432, Me.S39_022, Me.Label434, Me.Label435, Me.Label436, Me.Label437, Me.S39_010, Me.Label439, Me.Label442, Me.Label444, Me.S39_023, Me.Label446, Me.Label449, Me.Label450, Me.Label451, Me.S310_001, Me.Label453, Me.S310_002, Me.S310_013, Me.S310_014, Me.Label457, Me.Label458, Me.Line112, Me.Line113, Me.Label459, Me.Label460, Me.S310_003, Me.S310_015, Me.Label463, Me.Label464, Me.S310_004, Me.S310_016, Me.Label467, Me.Line114, Me.Line115, Me.Line116, Me.Label468, Me.Label471, Me.Label472, Me.S310_006, Me.S310_018, Me.Label475, Me.Label476, Me.S310_007, Me.S310_019, Me.Label479, Me.Label480, Me.S310_008, Me.S310_020, Me.Label483, Me.Label484, Me.S310_009, Me.S310_021, Me.Label487, Me.Label488, Me.S310_010, Me.S310_022, Me.Label491, Me.Label492, Me.Label495, Me.Label496, Me.Label499, Me.Line120, Me.Line121, Me.Line122, Me.Line123, Me.Line124, Me.Line126, Me.Line128, Me.Line129, Me.S310_005, Me.S310_017, Me.Line1, Me.Line2, Me.Line117, Me.Line118, Me.Line119, Me.Label503, Me.Label502, Me.S311_001, Me.Label505, Me.S311_002, Me.S311_004, Me.S311_005, Me.Label509, Me.Label510, Me.Line130, Me.Line131, Me.Label511, Me.Label512, Me.S311_006, Me.Label515, Me.Line132, Me.Line133, Me.Line134, Me.Line137, Me.Label516, Me.Line135, Me.Line3, Me.S312_006, Me.Label535, Me.Label534, Me.Label517, Me.Label520, Me.S312_001, Me.Label522, Me.S312_002, Me.Label524, Me.Label525, Me.S312_003, Me.Label527, Me.Label528, Me.S312_007, Me.Label530, Me.S312_008, Me.S312_024, Me.S312_025, Me.Line138, Me.Line139, Me.Line144, Me.Label536, Me.Label537, Me.S312_004, Me.Label539, Me.Label540, Me.S312_005, Me.Label542, Me.Label543, Me.Label545, Me.Line143, Me.Label547, Me.S312_009, Me.S312_026, Me.Label551, Me.S312_010, Me.S312_027, Me.Label555, Me.S312_011, Me.S312_028, Me.Label559, Me.S312_012, Me.S312_029, Me.Label563, Me.S312_013, Me.S312_030, Me.Label567, Me.S312_014, Me.S312_031, Me.Label571, Me.S312_015, Me.S312_032, Me.Label575, Me.S312_016, Me.S312_033, Me.Label579, Me.S312_017, Me.S312_034, Me.Label583, Me.S312_018, Me.S312_035, Me.S312_020, Me.Label587, Me.Label588, Me.S312_022, Me.Label593, Me.Label594, Me.S312_023, Me.Label596, Me.Label597, Me.S312_021, Me.Label590, Me.Label591, Me.Line145, Me.Line146, Me.Line147, Me.Line148, Me.Line149, Me.Line150, Me.Line151, Me.Line152, Me.Line153, Me.Line154, Me.Line155, Me.Line141, Me.Line142, Me.Label1, Me.S39_017, Me.Label3, Me.S39_018, Me.Label5, Me.S39_011, Me.Label7, Me.S39_012, Me.Label9, Me.S39_024, Me.Label11, Me.S39_025, Me.Line4, Me.Label15, Me.Line5, Me.Line140, Me.Shape1, Me.Shape7, Me.Line6})
        Me.Detail.Height = 13.146!
        Me.Detail.Name = "Detail"
        '
        'S39_019
        '
        Me.S39_019.Height = 0.1875!
        Me.S39_019.HyperLink = Nothing
        Me.S39_019.Left = 5.861!
        Me.S39_019.Name = "S39_019"
        Me.S39_019.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S39_019.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_019.Top = 1.896!
        Me.S39_019.Width = 1.0!
        '
        'S39_026
        '
        Me.S39_026.Height = 0.1875!
        Me.S39_026.HyperLink = Nothing
        Me.S39_026.Left = 5.862!
        Me.S39_026.Name = "S39_026"
        Me.S39_026.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S39_026.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_026.Top = 3.815!
        Me.S39_026.Width = 1.0!
        '
        'S312_019
        '
        Me.S312_019.Height = 0.1875!
        Me.S312_019.HyperLink = Nothing
        Me.S312_019.Left = 2.25!
        Me.S312_019.Name = "S312_019"
        Me.S312_019.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S312_019.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_019.Top = 12.0!
        Me.S312_019.Width = 1.0!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 2.249!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 12.0!
        Me.Shape2.Width = 1.006!
        '
        'S312_036
        '
        Me.S312_036.Height = 0.1875!
        Me.S312_036.HyperLink = Nothing
        Me.S312_036.Left = 3.252!
        Me.S312_036.Name = "S312_036"
        Me.S312_036.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S312_036.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_036.Top = 12.0!
        Me.S312_036.Width = 1.0!
        '
        'Shape3
        '
        Me.Shape3.Height = 0.1875!
        Me.Shape3.Left = 3.247!
        Me.Shape3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape3.Name = "Shape3"
        Me.Shape3.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape3.Top = 12.0!
        Me.Shape3.Width = 1.003!
        '
        'S310_023
        '
        Me.S310_023.Height = 0.1875!
        Me.S310_023.HyperLink = Nothing
        Me.S310_023.Left = 3.253!
        Me.S310_023.Name = "S310_023"
        Me.S310_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_023.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_023.Top = 6.563!
        Me.S310_023.Width = 1.0!
        '
        'S310_011
        '
        Me.S310_011.Height = 0.1875!
        Me.S310_011.HyperLink = Nothing
        Me.S310_011.Left = 2.254!
        Me.S310_011.Name = "S310_011"
        Me.S310_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_011.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_011.Top = 6.560999!
        Me.S310_011.Width = 1.0!
        '
        'S310_012
        '
        Me.S310_012.Height = 0.1875!
        Me.S310_012.HyperLink = Nothing
        Me.S310_012.Left = 2.254!
        Me.S310_012.Name = "S310_012"
        Me.S310_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_012.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_012.Top = 6.749!
        Me.S310_012.Width = 1.0!
        '
        'S310_024
        '
        Me.S310_024.Height = 0.1875!
        Me.S310_024.HyperLink = Nothing
        Me.S310_024.Left = 3.249!
        Me.S310_024.Name = "S310_024"
        Me.S310_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_024.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_024.Top = 6.75!
        Me.S310_024.Width = 1.0!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 4.255!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label34.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= t_stem, OK, if tstiff_max >= tstiff" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label34.Top = 11.813!
        Me.Label34.Width = 3.753!
        '
        'Label32
        '
        Me.Label32.Height = 0.1875!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 4.255001!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label32.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= max (t_stem/2, bstiff/16, t_STP_user), OK, if tstiff_min <= tstiff" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label32.Top = 11.626!
        Me.Label32.Width = 3.744999!
        '
        'Label574
        '
        Me.Label574.Height = 0.1875!
        Me.Label574.HyperLink = Nothing
        Me.Label574.Left = 4.255001!
        Me.Label574.Name = "Label574"
        Me.Label574.Style = "font-size: 6.25pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label574.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= As_min/ (bstiff_Lclip_flange)/ 2/ SMF_Sides, OK, if t_stiff_tension <= tstif" &
    "f" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label574.Top = 11.438!
        Me.Label574.Width = 3.744999!
        '
        'Label570
        '
        Me.Label570.Height = 0.1875!
        Me.Label570.HyperLink = Nothing
        Me.Label570.Left = 4.255001!
        Me.Label570.Name = "Label570"
        Me.Label570.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label570.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Input value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label570.Top = 11.251!
        Me.Label570.Width = 3.744999!
        '
        'Label566
        '
        Me.Label566.Height = 0.1875!
        Me.Label566.HyperLink = Nothing
        Me.Label566.Left = 4.255!
        Me.Label566.Name = "Label566"
        Me.Label566.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label566.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Floor (bstiff_max,0.125), Each side of column web" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label566.Top = 11.063!
        Me.Label566.Width = 3.745!
        '
        'Label562
        '
        Me.Label562.Height = 0.1875!
        Me.Label562.HyperLink = Nothing
        Me.Label562.Left = 4.255!
        Me.Label562.Name = "Label562"
        Me.Label562.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label562.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (bcf - tcw)/2" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label562.Top = 10.876!
        Me.Label562.Width = 3.745!
        '
        'Label558
        '
        Me.Label558.Height = 0.1875!
        Me.Label558.HyperLink = Nothing
        Me.Label558.Left = 4.255!
        Me.Label558.Name = "Label558"
        Me.Label558.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label558.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "AISC J10.8, = bcf/3 - tcw/2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label558.Top = 10.688!
        Me.Label558.Width = 3.745!
        '
        'Label554
        '
        Me.Label554.Height = 0.1875!
        Me.Label554.HyperLink = Nothing
        Me.Label554.Left = 4.255!
        Me.Label554.Name = "Label554"
        Me.Label554.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label554.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ceiling (Lclip_flange_min, 0.0625)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label554.Top = 10.501!
        Me.Label554.Width = 3.745!
        '
        'Label550
        '
        Me.Label550.Height = 0.1875!
        Me.Label550.HyperLink = Nothing
        Me.Label550.Left = 4.255!
        Me.Label550.Name = "Label550"
        Me.Label550.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label550.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= K1 - tcwdet/2 " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label550.Top = 10.313!
        Me.Label550.Width = 3.745!
        '
        'Label546
        '
        Me.Label546.Height = 0.1875!
        Me.Label546.HyperLink = Nothing
        Me.Label546.Left = 4.255!
        Me.Label546.Name = "Label546"
        Me.Label546.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label546.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Ceiling (Lclip_web_min, 0.0625)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label546.Top = 10.125!
        Me.Label546.Width = 3.745!
        '
        'Label519
        '
        Me.Label519.Height = 0.1875!
        Me.Label519.HyperLink = Nothing
        Me.Label519.Left = 4.255!
        Me.Label519.Name = "Label519"
        Me.Label519.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label519.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Kdet - tcf + 1.5" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label519.Top = 9.937!
        Me.Label519.Width = 3.745!
        '
        'Label518
        '
        Me.Label518.Height = 0.1875!
        Me.Label518.HyperLink = Nothing
        Me.Label518.Left = 4.255!
        Me.Label518.Name = "Label518"
        Me.Label518.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label518.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Fsu/ (Φt* Fy_Stiff)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label518.Top = 9.75!
        Me.Label518.Width = 3.745!
        '
        'S311_003
        '
        Me.S311_003.Height = 0.1875!
        Me.S311_003.HyperLink = Nothing
        Me.S311_003.Left = 2.25!
        Me.S311_003.Name = "S311_003"
        Me.S311_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S311_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S311_003.Top = 7.875!
        Me.S311_003.Width = 1.0!
        '
        'Label379
        '
        Me.Label379.Height = 0.1875!
        Me.Label379.HyperLink = Nothing
        Me.Label379.Left = 0!
        Me.Label379.Name = "Label379"
        Me.Label379.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label379.Text = "3.9 SUMMARY OF LOCAL CAPACITIES:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label379.Top = 0!
        Me.Label379.Width = 5.0!
        '
        'Label362
        '
        Me.Label362.Height = 0.1875!
        Me.Label362.HyperLink = Nothing
        Me.Label362.Left = 0.313!
        Me.Label362.Name = "Label362"
        Me.Label362.Style = "color: Black; font-size: 8.25pt; font-weight: bold; text-align: right; text-decor" &
    "ation: underline; vertical-align: middle"
        Me.Label362.Text = "Left Side:"
        Me.Label362.Top = 0.187!
        Me.Label362.Width = 0.687!
        '
        'Label380
        '
        Me.Label380.Height = 0.1875!
        Me.Label380.HyperLink = Nothing
        Me.Label380.Left = 1.0!
        Me.Label380.Name = "Label380"
        Me.Label380.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label380.Text = "At Top Stiffener:"
        Me.Label380.Top = 0.187!
        Me.Label380.Width = 1.255!
        '
        'Label383
        '
        Me.Label383.Height = 0.1875!
        Me.Label383.HyperLink = Nothing
        Me.Label383.Left = 0!
        Me.Label383.Name = "Label383"
        Me.Label383.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label383.Text = "ΦRn_WLY_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label383.Top = 0.375!
        Me.Label383.Width = 2.25!
        '
        'S39_001
        '
        Me.S39_001.Height = 0.1875!
        Me.S39_001.HyperLink = Nothing
        Me.S39_001.Left = 2.25!
        Me.S39_001.Name = "S39_001"
        Me.S39_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_001.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_001.Top = 0.375!
        Me.S39_001.Width = 1.0!
        '
        'Label385
        '
        Me.Label385.Height = 0.1875!
        Me.Label385.HyperLink = Nothing
        Me.Label385.Left = 3.247!
        Me.Label385.Name = "Label385"
        Me.Label385.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label385.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label385.Top = 0.375!
        Me.Label385.Width = 0.4400003!
        '
        'Label381
        '
        Me.Label381.Height = 0.1875!
        Me.Label381.HyperLink = Nothing
        Me.Label381.Left = 0!
        Me.Label381.Name = "Label381"
        Me.Label381.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label381.Text = "ΦRn_WLC_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label381.Top = 0.563!
        Me.Label381.Width = 2.25!
        '
        'S39_002
        '
        Me.S39_002.Height = 0.1875!
        Me.S39_002.HyperLink = Nothing
        Me.S39_002.Left = 2.25!
        Me.S39_002.Name = "S39_002"
        Me.S39_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_002.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_002.Top = 0.5630001!
        Me.S39_002.Width = 1.0!
        '
        'Label386
        '
        Me.Label386.Height = 0.1875!
        Me.Label386.HyperLink = Nothing
        Me.Label386.Left = 3.247!
        Me.Label386.Name = "Label386"
        Me.Label386.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label386.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label386.Top = 0.563!
        Me.Label386.Width = 0.4400003!
        '
        'Label387
        '
        Me.Label387.Height = 0.1875!
        Me.Label387.HyperLink = Nothing
        Me.Label387.Left = 0!
        Me.Label387.Name = "Label387"
        Me.Label387.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label387.Text = "ΦRn_WCB_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label387.Top = 0.7510002!
        Me.Label387.Width = 2.25!
        '
        'S39_003
        '
        Me.S39_003.Height = 0.1875!
        Me.S39_003.HyperLink = Nothing
        Me.S39_003.Left = 2.25!
        Me.S39_003.Name = "S39_003"
        Me.S39_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_003.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_003.Top = 0.751!
        Me.S39_003.Width = 1.0!
        '
        'Label389
        '
        Me.Label389.Height = 0.1875!
        Me.Label389.HyperLink = Nothing
        Me.Label389.Left = 3.247!
        Me.Label389.Name = "Label389"
        Me.Label389.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label389.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label389.Top = 0.7510002!
        Me.Label389.Width = 0.4400003!
        '
        'Label390
        '
        Me.Label390.Height = 0.1875!
        Me.Label390.HyperLink = Nothing
        Me.Label390.Left = 4.187!
        Me.Label390.Name = "Label390"
        Me.Label390.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label390.Text = "At Bottom Stiffener:"
        Me.Label390.Top = 0.1860001!
        Me.Label390.Width = 1.675!
        '
        'Label391
        '
        Me.Label391.Height = 0.1875!
        Me.Label391.HyperLink = Nothing
        Me.Label391.Left = 4.186998!
        Me.Label391.Name = "Label391"
        Me.Label391.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label391.Text = "ΦRn_WLY_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label391.Top = 0.3740001!
        Me.Label391.Width = 1.674999!
        '
        'S39_013
        '
        Me.S39_013.Height = 0.1875!
        Me.S39_013.HyperLink = Nothing
        Me.S39_013.Left = 5.861998!
        Me.S39_013.Name = "S39_013"
        Me.S39_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_013.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_013.Top = 0.3740001!
        Me.S39_013.Width = 1.0!
        '
        'Label393
        '
        Me.Label393.Height = 0.1875!
        Me.Label393.HyperLink = Nothing
        Me.Label393.Left = 6.862!
        Me.Label393.Name = "Label393"
        Me.Label393.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label393.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label393.Top = 0.374!
        Me.Label393.Width = 0.3869996!
        '
        'Label394
        '
        Me.Label394.Height = 0.1875!
        Me.Label394.HyperLink = Nothing
        Me.Label394.Left = 4.186998!
        Me.Label394.Name = "Label394"
        Me.Label394.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label394.Text = "ΦRn_WLC_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label394.Top = 0.562!
        Me.Label394.Width = 1.674999!
        '
        'S39_014
        '
        Me.S39_014.Height = 0.1875!
        Me.S39_014.HyperLink = Nothing
        Me.S39_014.Left = 5.861998!
        Me.S39_014.Name = "S39_014"
        Me.S39_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_014.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_014.Top = 0.562!
        Me.S39_014.Width = 1.0!
        '
        'Label396
        '
        Me.Label396.Height = 0.1875!
        Me.Label396.HyperLink = Nothing
        Me.Label396.Left = 6.862!
        Me.Label396.Name = "Label396"
        Me.Label396.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label396.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label396.Top = 0.5619999!
        Me.Label396.Width = 0.3869996!
        '
        'Label397
        '
        Me.Label397.Height = 0.1875!
        Me.Label397.HyperLink = Nothing
        Me.Label397.Left = 4.186998!
        Me.Label397.Name = "Label397"
        Me.Label397.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label397.Text = "ΦRn_WCB_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label397.Top = 0.7500002!
        Me.Label397.Width = 1.674999!
        '
        'S39_015
        '
        Me.S39_015.Height = 0.1875!
        Me.S39_015.HyperLink = Nothing
        Me.S39_015.Left = 5.861998!
        Me.S39_015.Name = "S39_015"
        Me.S39_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_015.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_015.Top = 0.7500002!
        Me.S39_015.Width = 1.0!
        '
        'Label399
        '
        Me.Label399.Height = 0.1875!
        Me.Label399.HyperLink = Nothing
        Me.Label399.Left = 6.862!
        Me.Label399.Name = "Label399"
        Me.Label399.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label399.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label399.Top = 0.7500001!
        Me.Label399.Width = 0.3869996!
        '
        'Label400
        '
        Me.Label400.Height = 0.3120022!
        Me.Label400.HyperLink = Nothing
        Me.Label400.Left = 0!
        Me.Label400.Name = "Label400"
        Me.Label400.Style = "font-size: 6.75pt; font-style: italic; text-align: center; vertical-align: middle" &
    "; ddo-char-set: 1"
        Me.Label400.Text = "if (SMF_Sides = 1, ΦRn_Stiff_top = min (ΦRn_WLY_top, ΦRn_WLC_top), min ( ΦRn_WLY_" &
    "top, ΦRn_WLC_top, ΦRn_WCB_top))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label400.Top = 1.001!
        Me.Label400.Width = 3.687!
        '
        'Label401
        '
        Me.Label401.Height = 0.3120022!
        Me.Label401.HyperLink = Nothing
        Me.Label401.Left = 4.187!
        Me.Label401.Name = "Label401"
        Me.Label401.Style = "font-size: 6.75pt; font-style: italic; text-align: center; vertical-align: middle" &
    "; ddo-char-set: 1"
        Me.Label401.Text = "if (SMF_Sides = 1, ΦRn_Stiff_bot = min (ΦRn_WLY_bot, ΦRn_WLC_bot), min ( ΦRn_WLY_" &
    "bot, ΦRn_WLC_bot, ΦRn_WCB_bot))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label401.Top = 1.0!
        Me.Label401.Width = 3.438!
        '
        'Label402
        '
        Me.Label402.Height = 0.1875!
        Me.Label402.HyperLink = Nothing
        Me.Label402.Left = 0!
        Me.Label402.Name = "Label402"
        Me.Label402.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label402.Text = "ΦRn_Stiff_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label402.Top = 1.312!
        Me.Label402.Width = 2.25!
        '
        'S39_004
        '
        Me.S39_004.Height = 0.1875!
        Me.S39_004.HyperLink = Nothing
        Me.S39_004.Left = 2.25!
        Me.S39_004.Name = "S39_004"
        Me.S39_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_004.Top = 1.313!
        Me.S39_004.Width = 1.0!
        '
        'Label404
        '
        Me.Label404.Height = 0.1875!
        Me.Label404.HyperLink = Nothing
        Me.Label404.Left = 3.255!
        Me.Label404.Name = "Label404"
        Me.Label404.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label404.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label404.Top = 1.313!
        Me.Label404.Width = 0.4320004!
        '
        'Label405
        '
        Me.Label405.Height = 0.1875!
        Me.Label405.HyperLink = Nothing
        Me.Label405.Left = 0!
        Me.Label405.Name = "Label405"
        Me.Label405.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label405.Text = "Top Stiffener Required=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label405.Top = 1.501!
        Me.Label405.Width = 2.25!
        '
        'S39_005
        '
        Me.S39_005.Height = 0.1875!
        Me.S39_005.HyperLink = Nothing
        Me.S39_005.Left = 2.25!
        Me.S39_005.Name = "S39_005"
        Me.S39_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_005.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_005.Top = 1.501!
        Me.S39_005.Width = 1.0!
        '
        'Label408
        '
        Me.Label408.Height = 0.1875!
        Me.Label408.HyperLink = Nothing
        Me.Label408.Left = 0!
        Me.Label408.Name = "Label408"
        Me.Label408.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label408.Text = "Top Stiff Provided?:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label408.Top = 1.689!
        Me.Label408.Width = 2.25!
        '
        'S39_006
        '
        Me.S39_006.Height = 0.1875!
        Me.S39_006.HyperLink = Nothing
        Me.S39_006.Left = 2.25!
        Me.S39_006.Name = "S39_006"
        Me.S39_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_006.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_006.Top = 1.689!
        Me.S39_006.Width = 1.0!
        '
        'Label407
        '
        Me.Label407.Height = 0.1875!
        Me.Label407.HyperLink = Nothing
        Me.Label407.Left = 4.185998!
        Me.Label407.Name = "Label407"
        Me.Label407.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label407.Text = "ΦRn_Stiff_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label407.Top = 1.333011!
        Me.Label407.Width = 1.674999!
        '
        'S39_016
        '
        Me.S39_016.Height = 0.1875!
        Me.S39_016.HyperLink = Nothing
        Me.S39_016.Left = 5.860998!
        Me.S39_016.Name = "S39_016"
        Me.S39_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_016.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_016.Top = 1.333011!
        Me.S39_016.Width = 1.0!
        '
        'Label411
        '
        Me.Label411.Height = 0.1875!
        Me.Label411.HyperLink = Nothing
        Me.Label411.Left = 6.860998!
        Me.Label411.Name = "Label411"
        Me.Label411.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label411.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label411.Top = 1.333011!
        Me.Label411.Width = 0.3869996!
        '
        'Label412
        '
        Me.Label412.Height = 0.1875!
        Me.Label412.HyperLink = Nothing
        Me.Label412.Left = 4.186!
        Me.Label412.Name = "Label412"
        Me.Label412.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label412.Text = "Stiff Req'd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label412.Top = 1.896!
        Me.Label412.Width = 1.674999!
        '
        'Label414
        '
        Me.Label414.Height = 0.1875!
        Me.Label414.HyperLink = Nothing
        Me.Label414.Left = 0.313!
        Me.Label414.Name = "Label414"
        Me.Label414.Style = "color: Black; font-size: 8.25pt; font-weight: bold; text-align: right; text-decor" &
    "ation: underline; vertical-align: middle"
        Me.Label414.Text = "Right Side:"
        Me.Label414.Top = 2.187!
        Me.Label414.Width = 0.687!
        '
        'Label415
        '
        Me.Label415.Height = 0.1875!
        Me.Label415.HyperLink = Nothing
        Me.Label415.Left = 1.0!
        Me.Label415.Name = "Label415"
        Me.Label415.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label415.Text = "At Top Stiffener:"
        Me.Label415.Top = 2.189!
        Me.Label415.Width = 1.247!
        '
        'Label416
        '
        Me.Label416.Height = 0.1875!
        Me.Label416.HyperLink = Nothing
        Me.Label416.Left = 0!
        Me.Label416.Name = "Label416"
        Me.Label416.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label416.Text = "ΦRn_WLY_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label416.Top = 2.376!
        Me.Label416.Width = 2.25!
        '
        'S39_007
        '
        Me.S39_007.Height = 0.1875!
        Me.S39_007.HyperLink = Nothing
        Me.S39_007.Left = 2.25!
        Me.S39_007.Name = "S39_007"
        Me.S39_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_007.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_007.Top = 2.376!
        Me.S39_007.Width = 1.0!
        '
        'Label418
        '
        Me.Label418.Height = 0.1875!
        Me.Label418.HyperLink = Nothing
        Me.Label418.Left = 3.255!
        Me.Label418.Name = "Label418"
        Me.Label418.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label418.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label418.Top = 2.376!
        Me.Label418.Width = 0.4320002!
        '
        'Label419
        '
        Me.Label419.Height = 0.1875!
        Me.Label419.HyperLink = Nothing
        Me.Label419.Left = 0!
        Me.Label419.Name = "Label419"
        Me.Label419.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label419.Text = "ΦRn_WLC_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label419.Top = 2.564!
        Me.Label419.Width = 2.25!
        '
        'S39_008
        '
        Me.S39_008.Height = 0.1875!
        Me.S39_008.HyperLink = Nothing
        Me.S39_008.Left = 2.25!
        Me.S39_008.Name = "S39_008"
        Me.S39_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_008.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_008.Top = 2.564!
        Me.S39_008.Width = 1.0!
        '
        'Label421
        '
        Me.Label421.Height = 0.1875!
        Me.Label421.HyperLink = Nothing
        Me.Label421.Left = 3.255!
        Me.Label421.Name = "Label421"
        Me.Label421.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label421.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label421.Top = 2.564!
        Me.Label421.Width = 0.4320002!
        '
        'Label422
        '
        Me.Label422.Height = 0.1875!
        Me.Label422.HyperLink = Nothing
        Me.Label422.Left = 0!
        Me.Label422.Name = "Label422"
        Me.Label422.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label422.Text = "ΦRn_WCB_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label422.Top = 2.752!
        Me.Label422.Width = 2.25!
        '
        'S39_009
        '
        Me.S39_009.Height = 0.1875!
        Me.S39_009.HyperLink = Nothing
        Me.S39_009.Left = 2.25!
        Me.S39_009.Name = "S39_009"
        Me.S39_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_009.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_009.Top = 2.752!
        Me.S39_009.Width = 1.0!
        '
        'Label424
        '
        Me.Label424.Height = 0.1875!
        Me.Label424.HyperLink = Nothing
        Me.Label424.Left = 3.255!
        Me.Label424.Name = "Label424"
        Me.Label424.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label424.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label424.Top = 2.752!
        Me.Label424.Width = 0.4320002!
        '
        'Label425
        '
        Me.Label425.Height = 0.1875!
        Me.Label425.HyperLink = Nothing
        Me.Label425.Left = 4.187!
        Me.Label425.Name = "Label425"
        Me.Label425.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label425.Text = "At Bottom Stiffener:"
        Me.Label425.Top = 2.188!
        Me.Label425.Width = 1.675!
        '
        'Label426
        '
        Me.Label426.Height = 0.1875!
        Me.Label426.HyperLink = Nothing
        Me.Label426.Left = 4.187!
        Me.Label426.Name = "Label426"
        Me.Label426.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label426.Text = "ΦRn_WLY_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label426.Top = 2.375!
        Me.Label426.Width = 1.674999!
        '
        'S39_020
        '
        Me.S39_020.Height = 0.1875!
        Me.S39_020.HyperLink = Nothing
        Me.S39_020.Left = 5.861998!
        Me.S39_020.Name = "S39_020"
        Me.S39_020.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_020.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_020.Top = 2.375!
        Me.S39_020.Width = 1.0!
        '
        'Label428
        '
        Me.Label428.Height = 0.1875!
        Me.Label428.HyperLink = Nothing
        Me.Label428.Left = 6.861998!
        Me.Label428.Name = "Label428"
        Me.Label428.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label428.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label428.Top = 2.375!
        Me.Label428.Width = 0.3869996!
        '
        'Label429
        '
        Me.Label429.Height = 0.1875!
        Me.Label429.HyperLink = Nothing
        Me.Label429.Left = 4.187!
        Me.Label429.Name = "Label429"
        Me.Label429.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label429.Text = "ΦRn_WLC_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label429.Top = 2.563!
        Me.Label429.Width = 1.674999!
        '
        'S39_021
        '
        Me.S39_021.Height = 0.1875!
        Me.S39_021.HyperLink = Nothing
        Me.S39_021.Left = 5.861998!
        Me.S39_021.Name = "S39_021"
        Me.S39_021.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_021.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_021.Top = 2.563!
        Me.S39_021.Width = 1.0!
        '
        'Label431
        '
        Me.Label431.Height = 0.1875!
        Me.Label431.HyperLink = Nothing
        Me.Label431.Left = 6.861998!
        Me.Label431.Name = "Label431"
        Me.Label431.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label431.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label431.Top = 2.563!
        Me.Label431.Width = 0.3869996!
        '
        'Label432
        '
        Me.Label432.Height = 0.1875!
        Me.Label432.HyperLink = Nothing
        Me.Label432.Left = 4.187!
        Me.Label432.Name = "Label432"
        Me.Label432.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label432.Text = "ΦRn_WCB_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label432.Top = 2.751!
        Me.Label432.Width = 1.674999!
        '
        'S39_022
        '
        Me.S39_022.Height = 0.1875!
        Me.S39_022.HyperLink = Nothing
        Me.S39_022.Left = 5.861998!
        Me.S39_022.Name = "S39_022"
        Me.S39_022.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_022.Text = "NA" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_022.Top = 2.751!
        Me.S39_022.Width = 1.0!
        '
        'Label434
        '
        Me.Label434.Height = 0.1875!
        Me.Label434.HyperLink = Nothing
        Me.Label434.Left = 6.861998!
        Me.Label434.Name = "Label434"
        Me.Label434.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label434.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label434.Top = 2.751!
        Me.Label434.Width = 0.3869996!
        '
        'Label435
        '
        Me.Label435.Height = 0.3120022!
        Me.Label435.HyperLink = Nothing
        Me.Label435.Left = 0.313!
        Me.Label435.Name = "Label435"
        Me.Label435.Style = "font-size: 6.75pt; font-style: italic; text-align: center; vertical-align: middle" &
    "; ddo-char-set: 1"
        Me.Label435.Text = "if (SMF_Sides = 1, ΦRn_Stiff_top = min (ΦRn_WLY_top, ΦRn_WLC_top), min ( ΦRn_WLY_" &
    "top, ΦRn_WLC_top, ΦRn_WCB_top))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label435.Top = 2.94!
        Me.Label435.Width = 3.374!
        '
        'Label436
        '
        Me.Label436.Height = 0.3120022!
        Me.Label436.HyperLink = Nothing
        Me.Label436.Left = 4.186!
        Me.Label436.Name = "Label436"
        Me.Label436.Style = "font-size: 6.75pt; font-style: italic; text-align: center; vertical-align: middle" &
    "; ddo-char-set: 1"
        Me.Label436.Text = "if (SMF_Sides = 1, ΦRn_Stiff_bot = min (ΦRn_WLY_bot, ΦRn_WLC_bot), min ( ΦRn_WLY_" &
    "bot, ΦRn_WLC_bot, ΦRn_WCB_bot))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label436.Top = 2.938993!
        Me.Label436.Width = 3.436998!
        '
        'Label437
        '
        Me.Label437.Height = 0.1875!
        Me.Label437.HyperLink = Nothing
        Me.Label437.Left = 0!
        Me.Label437.Name = "Label437"
        Me.Label437.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label437.Text = "ΦRn_Stiff_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label437.Top = 3.252!
        Me.Label437.Width = 2.25!
        '
        'S39_010
        '
        Me.S39_010.Height = 0.1875!
        Me.S39_010.HyperLink = Nothing
        Me.S39_010.Left = 2.25!
        Me.S39_010.Name = "S39_010"
        Me.S39_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_010.Top = 3.252!
        Me.S39_010.Width = 1.0!
        '
        'Label439
        '
        Me.Label439.Height = 0.1875!
        Me.Label439.HyperLink = Nothing
        Me.Label439.Left = 3.247!
        Me.Label439.Name = "Label439"
        Me.Label439.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label439.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label439.Top = 3.252!
        Me.Label439.Width = 0.4400003!
        '
        'Label442
        '
        Me.Label442.Height = 0.1875!
        Me.Label442.HyperLink = Nothing
        Me.Label442.Left = 4.187!
        Me.Label442.Name = "Label442"
        Me.Label442.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label442.Text = "Stiff Req'd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label442.Top = 3.815!
        Me.Label442.Width = 1.674999!
        '
        'Label444
        '
        Me.Label444.Height = 0.1875!
        Me.Label444.HyperLink = Nothing
        Me.Label444.Left = 4.185998!
        Me.Label444.Name = "Label444"
        Me.Label444.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label444.Text = "ΦRn_Stiff_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label444.Top = 3.252015!
        Me.Label444.Width = 1.674999!
        '
        'S39_023
        '
        Me.S39_023.Height = 0.1875!
        Me.S39_023.HyperLink = Nothing
        Me.S39_023.Left = 5.860998!
        Me.S39_023.Name = "S39_023"
        Me.S39_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_023.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_023.Top = 3.252015!
        Me.S39_023.Width = 1.0!
        '
        'Label446
        '
        Me.Label446.Height = 0.1875!
        Me.Label446.HyperLink = Nothing
        Me.Label446.Left = 6.860998!
        Me.Label446.Name = "Label446"
        Me.Label446.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label446.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label446.Top = 3.252015!
        Me.Label446.Width = 0.3869996!
        '
        'Label449
        '
        Me.Label449.Height = 0.1875!
        Me.Label449.HyperLink = Nothing
        Me.Label449.Left = 0.0000002384186!
        Me.Label449.Name = "Label449"
        Me.Label449.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label449.Text = "3.10 WEB SIDESWAY BUCKLING (AISC 360-J10.4):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label449.Top = 4.125001!
        Me.Label449.Width = 5.0!
        '
        'Label450
        '
        Me.Label450.Height = 0.1875!
        Me.Label450.HyperLink = Nothing
        Me.Label450.Left = 0.0000002384186!
        Me.Label450.Name = "Label450"
        Me.Label450.Style = "color: DarkRed; font-size: 8.25pt; text-align: left; vertical-align: middle"
        Me.Label450.Text = "Note: Assume compression flange is NOT restrained against rotation" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label450.Top = 4.313001!
        Me.Label450.Width = 8.555!
        '
        'Label451
        '
        Me.Label451.Height = 0.1875!
        Me.Label451.HyperLink = Nothing
        Me.Label451.Left = 0!
        Me.Label451.Name = "Label451"
        Me.Label451.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label451.Text = "ΦWSB=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label451.Top = 4.687!
        Me.Label451.Width = 2.25!
        '
        'S310_001
        '
        Me.S310_001.Height = 0.1875!
        Me.S310_001.HyperLink = Nothing
        Me.S310_001.Left = 2.25!
        Me.S310_001.Name = "S310_001"
        Me.S310_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S310_001.Text = "0.85" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_001.Top = 4.687!
        Me.S310_001.Width = 1.0!
        '
        'Label453
        '
        Me.Label453.Height = 0.1875!
        Me.Label453.HyperLink = Nothing
        Me.Label453.Left = 0!
        Me.Label453.Name = "Label453"
        Me.Label453.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label453.Text = "Hcc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label453.Top = 4.875!
        Me.Label453.Width = 2.25!
        '
        'S310_002
        '
        Me.S310_002.Height = 0.1875!
        Me.S310_002.HyperLink = Nothing
        Me.S310_002.Left = 2.25!
        Me.S310_002.Name = "S310_002"
        Me.S310_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S310_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_002.Top = 4.875!
        Me.S310_002.Width = 1.0!
        '
        'S310_013
        '
        Me.S310_013.Height = 0.1875!
        Me.S310_013.HyperLink = Nothing
        Me.S310_013.Left = 3.249999!
        Me.S310_013.Name = "S310_013"
        Me.S310_013.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S310_013.Text = "0.85" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_013.Top = 4.687!
        Me.S310_013.Width = 1.0!
        '
        'S310_014
        '
        Me.S310_014.Height = 0.1875!
        Me.S310_014.HyperLink = Nothing
        Me.S310_014.Left = 3.249999!
        Me.S310_014.Name = "S310_014"
        Me.S310_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S310_014.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_014.Top = 4.875!
        Me.S310_014.Width = 1.0!
        '
        'Label457
        '
        Me.Label457.Height = 0.1875!
        Me.Label457.HyperLink = Nothing
        Me.Label457.Left = 3.25!
        Me.Label457.Name = "Label457"
        Me.Label457.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label457.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label457.Top = 4.5!
        Me.Label457.Width = 1.0!
        '
        'Label458
        '
        Me.Label458.Height = 0.1875!
        Me.Label458.HyperLink = Nothing
        Me.Label458.Left = 2.25!
        Me.Label458.Name = "Label458"
        Me.Label458.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label458.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label458.Top = 4.5!
        Me.Label458.Width = 1.0!
        '
        'Line112
        '
        Me.Line112.Height = 0!
        Me.Line112.Left = 2.249999!
        Me.Line112.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line112.LineWeight = 1.0!
        Me.Line112.Name = "Line112"
        Me.Line112.Top = 4.873999!
        Me.Line112.Width = 2.000001!
        Me.Line112.X1 = 2.249999!
        Me.Line112.X2 = 4.25!
        Me.Line112.Y1 = 4.873999!
        Me.Line112.Y2 = 4.873999!
        '
        'Line113
        '
        Me.Line113.Height = 0!
        Me.Line113.Left = 2.25!
        Me.Line113.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line113.LineWeight = 1.0!
        Me.Line113.Name = "Line113"
        Me.Line113.Top = 4.687!
        Me.Line113.Width = 2.000001!
        Me.Line113.X1 = 2.25!
        Me.Line113.X2 = 4.250001!
        Me.Line113.Y1 = 4.687!
        Me.Line113.Y2 = 4.687!
        '
        'Label459
        '
        Me.Label459.Height = 0.1875!
        Me.Label459.HyperLink = Nothing
        Me.Label459.Left = 4.25!
        Me.Label459.Name = "Label459"
        Me.Label459.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label459.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label459.Top = 4.875!
        Me.Label459.Width = 3.313!
        '
        'Label460
        '
        Me.Label460.Height = 0.1875!
        Me.Label460.HyperLink = Nothing
        Me.Label460.Left = 0!
        Me.Label460.Name = "Label460"
        Me.Label460.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label460.Text = "bcf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label460.Top = 5.062!
        Me.Label460.Width = 2.25!
        '
        'S310_003
        '
        Me.S310_003.Height = 0.1875!
        Me.S310_003.HyperLink = Nothing
        Me.S310_003.Left = 2.25!
        Me.S310_003.Name = "S310_003"
        Me.S310_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S310_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_003.Top = 5.062!
        Me.S310_003.Width = 1.0!
        '
        'S310_015
        '
        Me.S310_015.Height = 0.1875!
        Me.S310_015.HyperLink = Nothing
        Me.S310_015.Left = 3.249999!
        Me.S310_015.Name = "S310_015"
        Me.S310_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S310_015.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_015.Top = 5.062!
        Me.S310_015.Width = 1.0!
        '
        'Label463
        '
        Me.Label463.Height = 0.1875!
        Me.Label463.HyperLink = Nothing
        Me.Label463.Left = 4.250001!
        Me.Label463.Name = "Label463"
        Me.Label463.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label463.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Column flange width" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label463.Top = 5.062!
        Me.Label463.Width = 3.313!
        '
        'Label464
        '
        Me.Label464.Height = 0.1875!
        Me.Label464.HyperLink = Nothing
        Me.Label464.Left = 0!
        Me.Label464.Name = "Label464"
        Me.Label464.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label464.Text = "Scx=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label464.Top = 5.25!
        Me.Label464.Width = 2.25!
        '
        'S310_004
        '
        Me.S310_004.Height = 0.1875!
        Me.S310_004.HyperLink = Nothing
        Me.S310_004.Left = 2.254!
        Me.S310_004.Name = "S310_004"
        Me.S310_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S310_004.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_004.Top = 5.25!
        Me.S310_004.Width = 1.0!
        '
        'S310_016
        '
        Me.S310_016.Height = 0.1875!
        Me.S310_016.HyperLink = Nothing
        Me.S310_016.Left = 3.249999!
        Me.S310_016.Name = "S310_016"
        Me.S310_016.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S310_016.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_016.Top = 5.25!
        Me.S310_016.Width = 1.0!
        '
        'Label467
        '
        Me.Label467.Height = 0.1875!
        Me.Label467.HyperLink = Nothing
        Me.Label467.Left = 4.251001!
        Me.Label467.Name = "Label467"
        Me.Label467.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label467.Text = "in^3" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label467.Top = 5.25!
        Me.Label467.Width = 3.875002!
        '
        'Line114
        '
        Me.Line114.Height = 0!
        Me.Line114.Left = 2.25!
        Me.Line114.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line114.LineWeight = 1.0!
        Me.Line114.Name = "Line114"
        Me.Line114.Top = 5.062!
        Me.Line114.Width = 2.000001!
        Me.Line114.X1 = 2.25!
        Me.Line114.X2 = 4.250001!
        Me.Line114.Y1 = 5.062!
        Me.Line114.Y2 = 5.062!
        '
        'Line115
        '
        Me.Line115.Height = 0!
        Me.Line115.Left = 2.25!
        Me.Line115.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line115.LineWeight = 1.0!
        Me.Line115.Name = "Line115"
        Me.Line115.Top = 5.25!
        Me.Line115.Width = 2.000001!
        Me.Line115.X1 = 2.25!
        Me.Line115.X2 = 4.250001!
        Me.Line115.Y1 = 5.25!
        Me.Line115.Y2 = 5.25!
        '
        'Line116
        '
        Me.Line116.Height = 0!
        Me.Line116.Left = 2.249999!
        Me.Line116.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line116.LineWeight = 1.0!
        Me.Line116.Name = "Line116"
        Me.Line116.Top = 5.437!
        Me.Line116.Width = 2.000001!
        Me.Line116.X1 = 2.249999!
        Me.Line116.X2 = 4.25!
        Me.Line116.Y1 = 5.437!
        Me.Line116.Y2 = 5.437!
        '
        'Label468
        '
        Me.Label468.Height = 0.1875!
        Me.Label468.HyperLink = Nothing
        Me.Label468.Left = 0!
        Me.Label468.Name = "Label468"
        Me.Label468.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label468.Text = "Lb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label468.Top = 5.437!
        Me.Label468.Width = 2.25!
        '
        'Label471
        '
        Me.Label471.Height = 0.1875!
        Me.Label471.HyperLink = Nothing
        Me.Label471.Left = 4.248998!
        Me.Label471.Name = "Label471"
        Me.Label471.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label471.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Hcc - d/2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label471.Top = 5.437!
        Me.Label471.Width = 3.875002!
        '
        'Label472
        '
        Me.Label472.Height = 0.1875!
        Me.Label472.HyperLink = Nothing
        Me.Label472.Left = 0!
        Me.Label472.Name = "Label472"
        Me.Label472.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label472.Text = "My=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label472.Top = 5.625!
        Me.Label472.Width = 2.25!
        '
        'S310_006
        '
        Me.S310_006.Height = 0.1875!
        Me.S310_006.HyperLink = Nothing
        Me.S310_006.Left = 2.247!
        Me.S310_006.Name = "S310_006"
        Me.S310_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_006.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_006.Top = 5.625!
        Me.S310_006.Width = 1.0!
        '
        'S310_018
        '
        Me.S310_018.Height = 0.1875!
        Me.S310_018.HyperLink = Nothing
        Me.S310_018.Left = 3.246999!
        Me.S310_018.Name = "S310_018"
        Me.S310_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_018.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_018.Top = 5.625!
        Me.S310_018.Width = 1.0!
        '
        'Label475
        '
        Me.Label475.Height = 0.1875!
        Me.Label475.HyperLink = Nothing
        Me.Label475.Left = 4.248!
        Me.Label475.Name = "Label475"
        Me.Label475.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label475.Text = "kips*in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Scx* Fyc" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label475.Top = 5.625!
        Me.Label475.Width = 3.875002!
        '
        'Label476
        '
        Me.Label476.Height = 0.1875!
        Me.Label476.HyperLink = Nothing
        Me.Label476.Left = 0!
        Me.Label476.Name = "Label476"
        Me.Label476.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label476.Text = "Mu=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label476.Top = 5.813!
        Me.Label476.Width = 2.25!
        '
        'S310_007
        '
        Me.S310_007.Height = 0.1875!
        Me.S310_007.HyperLink = Nothing
        Me.S310_007.Left = 2.25!
        Me.S310_007.Name = "S310_007"
        Me.S310_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_007.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_007.Top = 5.813!
        Me.S310_007.Width = 1.0!
        '
        'S310_019
        '
        Me.S310_019.Height = 0.1875!
        Me.S310_019.HyperLink = Nothing
        Me.S310_019.Left = 3.249999!
        Me.S310_019.Name = "S310_019"
        Me.S310_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_019.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_019.Top = 5.813!
        Me.S310_019.Width = 1.0!
        '
        'Label479
        '
        Me.Label479.Height = 0.1875!
        Me.Label479.HyperLink = Nothing
        Me.Label479.Left = 4.251001!
        Me.Label479.Name = "Label479"
        Me.Label479.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label479.Text = "kips*in" & Global.Microsoft.VisualBasic.ChrW(9) & "=Mcap" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label479.Top = 5.813!
        Me.Label479.Width = 3.875002!
        '
        'Label480
        '
        Me.Label480.Height = 0.1875!
        Me.Label480.HyperLink = Nothing
        Me.Label480.Left = 0!
        Me.Label480.Name = "Label480"
        Me.Label480.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label480.Text = "Cr=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label480.Top = 6.0!
        Me.Label480.Width = 2.25!
        '
        'S310_008
        '
        Me.S310_008.Height = 0.1875!
        Me.S310_008.HyperLink = Nothing
        Me.S310_008.Left = 2.25!
        Me.S310_008.Name = "S310_008"
        Me.S310_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_008.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_008.Top = 6.0!
        Me.S310_008.Width = 1.0!
        '
        'S310_020
        '
        Me.S310_020.Height = 0.1875!
        Me.S310_020.HyperLink = Nothing
        Me.S310_020.Left = 3.249999!
        Me.S310_020.Name = "S310_020"
        Me.S310_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_020.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_020.Top = 6.0!
        Me.S310_020.Width = 1.0!
        '
        'Label483
        '
        Me.Label483.Height = 0.1875!
        Me.Label483.HyperLink = Nothing
        Me.Label483.Left = 4.251001!
        Me.Label483.Name = "Label483"
        Me.Label483.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label483.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (Mu < My, 960000, 480000)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label483.Top = 6.0!
        Me.Label483.Width = 3.875002!
        '
        'Label484
        '
        Me.Label484.Height = 0.1875!
        Me.Label484.HyperLink = Nothing
        Me.Label484.Left = 0!
        Me.Label484.Name = "Label484"
        Me.Label484.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label484.Text = "(h/tcw)/(Lb/bcf)=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label484.Top = 6.188!
        Me.Label484.Width = 2.25!
        '
        'S310_009
        '
        Me.S310_009.Height = 0.1875!
        Me.S310_009.HyperLink = Nothing
        Me.S310_009.Left = 2.251!
        Me.S310_009.Name = "S310_009"
        Me.S310_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_009.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_009.Top = 6.188!
        Me.S310_009.Width = 1.0!
        '
        'S310_021
        '
        Me.S310_021.Height = 0.1875!
        Me.S310_021.HyperLink = Nothing
        Me.S310_021.Left = 3.250998!
        Me.S310_021.Name = "S310_021"
        Me.S310_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_021.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_021.Top = 6.188!
        Me.S310_021.Width = 1.0!
        '
        'Label487
        '
        Me.Label487.Height = 0.1875!
        Me.Label487.HyperLink = Nothing
        Me.Label487.Left = 4.252001!
        Me.Label487.Name = "Label487"
        Me.Label487.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label487.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= (h/ tcw)/ (Lb/ bcf)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label487.Top = 6.188!
        Me.Label487.Width = 3.875002!
        '
        'Label488
        '
        Me.Label488.Height = 0.1875!
        Me.Label488.HyperLink = Nothing
        Me.Label488.Left = 0!
        Me.Label488.Name = "Label488"
        Me.Label488.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label488.Text = "ΦRn_WSB=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label488.Top = 6.375!
        Me.Label488.Width = 2.25!
        '
        'S310_010
        '
        Me.S310_010.Height = 0.1875!
        Me.S310_010.HyperLink = Nothing
        Me.S310_010.Left = 2.252001!
        Me.S310_010.Name = "S310_010"
        Me.S310_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_010.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_010.Top = 6.375!
        Me.S310_010.Width = 1.0!
        '
        'S310_022
        '
        Me.S310_022.Height = 0.1875!
        Me.S310_022.HyperLink = Nothing
        Me.S310_022.Left = 3.252!
        Me.S310_022.Name = "S310_022"
        Me.S310_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_022.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_022.Top = 6.375!
        Me.S310_022.Width = 1.0!
        '
        'Label491
        '
        Me.Label491.Height = 0.1875!
        Me.Label491.HyperLink = Nothing
        Me.Label491.Left = 4.253001!
        Me.Label491.Name = "Label491"
        Me.Label491.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label491.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "AISC J10-7, = (ΦWSB* Cr* tcw^3* tcf/ h^2)/ (0.4* Cr^3)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label491.Top = 6.375!
        Me.Label491.Width = 3.875002!
        '
        'Label492
        '
        Me.Label492.Height = 0.1875!
        Me.Label492.HyperLink = Nothing
        Me.Label492.Left = 0!
        Me.Label492.Name = "Label492"
        Me.Label492.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label492.Text = "WSB_check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label492.Top = 6.563!
        Me.Label492.Width = 2.25!
        '
        'Label495
        '
        Me.Label495.Height = 0.1875!
        Me.Label495.HyperLink = Nothing
        Me.Label495.Left = 4.254!
        Me.Label495.Name = "Label495"
        Me.Label495.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label495.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Not applicable" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label495.Top = 6.563!
        Me.Label495.Width = 3.875002!
        '
        'Label496
        '
        Me.Label496.Height = 0.1875!
        Me.Label496.HyperLink = Nothing
        Me.Label496.Left = 0!
        Me.Label496.Name = "Label496"
        Me.Label496.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label496.Text = "BotStiffener_Bracing_Req'd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label496.Top = 6.75!
        Me.Label496.Width = 2.25!
        '
        'Label499
        '
        Me.Label499.Height = 0.1875!
        Me.Label499.HyperLink = Nothing
        Me.Label499.Left = 4.255!
        Me.Label499.Name = "Label499"
        Me.Label499.Style = "font-size: 6.5pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label499.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (And (ΦRn_WSB < Pr_link, WSB_check = ""Applicable""), ""YES"", ""NO"")" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label499.Top = 6.75!
        Me.Label499.Width = 3.741999!
        '
        'Line120
        '
        Me.Line120.Height = 0!
        Me.Line120.Left = 2.249999!
        Me.Line120.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line120.LineWeight = 1.0!
        Me.Line120.Name = "Line120"
        Me.Line120.Top = 5.437!
        Me.Line120.Width = 2.000001!
        Me.Line120.X1 = 2.249999!
        Me.Line120.X2 = 4.25!
        Me.Line120.Y1 = 5.437!
        Me.Line120.Y2 = 5.437!
        '
        'Line121
        '
        Me.Line121.Height = 0!
        Me.Line121.Left = 2.25!
        Me.Line121.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line121.LineWeight = 1.0!
        Me.Line121.Name = "Line121"
        Me.Line121.Top = 5.625!
        Me.Line121.Width = 2.000001!
        Me.Line121.X1 = 2.25!
        Me.Line121.X2 = 4.250001!
        Me.Line121.Y1 = 5.625!
        Me.Line121.Y2 = 5.625!
        '
        'Line122
        '
        Me.Line122.Height = 0!
        Me.Line122.Left = 2.25!
        Me.Line122.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line122.LineWeight = 1.0!
        Me.Line122.Name = "Line122"
        Me.Line122.Top = 5.813!
        Me.Line122.Width = 2.000002!
        Me.Line122.X1 = 2.25!
        Me.Line122.X2 = 4.250002!
        Me.Line122.Y1 = 5.813!
        Me.Line122.Y2 = 5.813!
        '
        'Line123
        '
        Me.Line123.Height = 0!
        Me.Line123.Left = 2.25!
        Me.Line123.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line123.LineWeight = 1.0!
        Me.Line123.Name = "Line123"
        Me.Line123.Top = 6.0!
        Me.Line123.Width = 2.000002!
        Me.Line123.X1 = 2.25!
        Me.Line123.X2 = 4.250002!
        Me.Line123.Y1 = 6.0!
        Me.Line123.Y2 = 6.0!
        '
        'Line124
        '
        Me.Line124.Height = 0!
        Me.Line124.Left = 2.25!
        Me.Line124.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line124.LineWeight = 1.0!
        Me.Line124.Name = "Line124"
        Me.Line124.Top = 6.188!
        Me.Line124.Width = 2.000001!
        Me.Line124.X1 = 2.25!
        Me.Line124.X2 = 4.250001!
        Me.Line124.Y1 = 6.188!
        Me.Line124.Y2 = 6.188!
        '
        'Line126
        '
        Me.Line126.Height = 0!
        Me.Line126.Left = 2.254!
        Me.Line126.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line126.LineWeight = 1.0!
        Me.Line126.Name = "Line126"
        Me.Line126.Top = 6.563!
        Me.Line126.Width = 2.0!
        Me.Line126.X1 = 2.254!
        Me.Line126.X2 = 4.254!
        Me.Line126.Y1 = 6.563!
        Me.Line126.Y2 = 6.563!
        '
        'Line128
        '
        Me.Line128.Height = 0!
        Me.Line128.Left = 2.254!
        Me.Line128.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line128.LineWeight = 1.0!
        Me.Line128.Name = "Line128"
        Me.Line128.Top = 6.938!
        Me.Line128.Width = 1.999999!
        Me.Line128.X1 = 2.254!
        Me.Line128.X2 = 4.253999!
        Me.Line128.Y1 = 6.938!
        Me.Line128.Y2 = 6.938!
        '
        'Line129
        '
        Me.Line129.Height = 0!
        Me.Line129.Left = 2.254!
        Me.Line129.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line129.LineWeight = 1.0!
        Me.Line129.Name = "Line129"
        Me.Line129.Top = 4.5!
        Me.Line129.Width = 1.999999!
        Me.Line129.X1 = 2.254!
        Me.Line129.X2 = 4.253999!
        Me.Line129.Y1 = 4.5!
        Me.Line129.Y2 = 4.5!
        '
        'S310_005
        '
        Me.S310_005.Height = 0.1875!
        Me.S310_005.HyperLink = Nothing
        Me.S310_005.Left = 2.254!
        Me.S310_005.Name = "S310_005"
        Me.S310_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_005.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S310_005.Top = 5.437!
        Me.S310_005.Width = 0.9979993!
        '
        'S310_017
        '
        Me.S310_017.Height = 0.1875!
        Me.S310_017.HyperLink = Nothing
        Me.S310_017.Left = 3.253999!
        Me.S310_017.Name = "S310_017"
        Me.S310_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S310_017.Text = "1"
        Me.S310_017.Top = 5.437!
        Me.S310_017.Width = 0.9990007!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 2.254!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 5.437!
        Me.Line1.Width = 1.999999!
        Me.Line1.X1 = 2.254!
        Me.Line1.X2 = 4.253999!
        Me.Line1.Y1 = 5.437!
        Me.Line1.Y2 = 5.437!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 2.254!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 6.375!
        Me.Line2.Width = 2.0!
        Me.Line2.X1 = 2.254!
        Me.Line2.X2 = 4.254!
        Me.Line2.Y1 = 6.375!
        Me.Line2.Y2 = 6.375!
        '
        'Line117
        '
        Me.Line117.Height = 2.438001!
        Me.Line117.Left = 2.255!
        Me.Line117.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line117.LineWeight = 1.0!
        Me.Line117.Name = "Line117"
        Me.Line117.Top = 4.501!
        Me.Line117.Width = 0!
        Me.Line117.X1 = 2.255!
        Me.Line117.X2 = 2.255!
        Me.Line117.Y1 = 4.501!
        Me.Line117.Y2 = 6.939001!
        '
        'Line118
        '
        Me.Line118.Height = 2.438!
        Me.Line118.Left = 3.25!
        Me.Line118.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line118.LineWeight = 1.0!
        Me.Line118.Name = "Line118"
        Me.Line118.Top = 4.5!
        Me.Line118.Width = 0!
        Me.Line118.X1 = 3.25!
        Me.Line118.X2 = 3.25!
        Me.Line118.Y1 = 4.5!
        Me.Line118.Y2 = 6.938!
        '
        'Line119
        '
        Me.Line119.Height = 2.438!
        Me.Line119.Left = 4.250003!
        Me.Line119.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line119.LineWeight = 1.0!
        Me.Line119.Name = "Line119"
        Me.Line119.Top = 4.5!
        Me.Line119.Width = 0!
        Me.Line119.X1 = 4.250003!
        Me.Line119.X2 = 4.250003!
        Me.Line119.Y1 = 4.5!
        Me.Line119.Y2 = 6.938!
        '
        'Label503
        '
        Me.Label503.Height = 0.1875!
        Me.Label503.HyperLink = Nothing
        Me.Label503.Left = 0.0000002384186!
        Me.Label503.Name = "Label503"
        Me.Label503.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label503.Text = "3.11 COLUMN STIFFENER DESIGN:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label503.Top = 7.125!
        Me.Label503.Width = 5.0!
        '
        'Label502
        '
        Me.Label502.Height = 0.1875!
        Me.Label502.HyperLink = Nothing
        Me.Label502.Left = 0!
        Me.Label502.Name = "Label502"
        Me.Label502.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label502.Text = "Fsu_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label502.Top = 7.5!
        Me.Label502.Width = 2.25!
        '
        'S311_001
        '
        Me.S311_001.Height = 0.1875!
        Me.S311_001.HyperLink = Nothing
        Me.S311_001.Left = 2.25!
        Me.S311_001.Name = "S311_001"
        Me.S311_001.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S311_001.Text = "0.85" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S311_001.Top = 7.5!
        Me.S311_001.Width = 1.0!
        '
        'Label505
        '
        Me.Label505.Height = 0.1875!
        Me.Label505.HyperLink = Nothing
        Me.Label505.Left = 0!
        Me.Label505.Name = "Label505"
        Me.Label505.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label505.Text = "Fsu_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label505.Top = 7.687003!
        Me.Label505.Width = 2.25!
        '
        'S311_002
        '
        Me.S311_002.Height = 0.1875!
        Me.S311_002.HyperLink = Nothing
        Me.S311_002.Left = 2.25!
        Me.S311_002.Name = "S311_002"
        Me.S311_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S311_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S311_002.Top = 7.687!
        Me.S311_002.Width = 1.0!
        '
        'S311_004
        '
        Me.S311_004.Height = 0.1875!
        Me.S311_004.HyperLink = Nothing
        Me.S311_004.Left = 3.25!
        Me.S311_004.Name = "S311_004"
        Me.S311_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S311_004.Text = "0.85" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S311_004.Top = 7.5!
        Me.S311_004.Width = 1.0!
        '
        'S311_005
        '
        Me.S311_005.Height = 0.1875!
        Me.S311_005.HyperLink = Nothing
        Me.S311_005.Left = 3.25!
        Me.S311_005.Name = "S311_005"
        Me.S311_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S311_005.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S311_005.Top = 7.687!
        Me.S311_005.Width = 1.0!
        '
        'Label509
        '
        Me.Label509.Height = 0.1875!
        Me.Label509.HyperLink = Nothing
        Me.Label509.Left = 3.25!
        Me.Label509.Name = "Label509"
        Me.Label509.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label509.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label509.Top = 7.312!
        Me.Label509.Width = 1.0!
        '
        'Label510
        '
        Me.Label510.Height = 0.1875!
        Me.Label510.HyperLink = Nothing
        Me.Label510.Left = 2.25!
        Me.Label510.Name = "Label510"
        Me.Label510.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label510.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label510.Top = 7.312!
        Me.Label510.Width = 1.0!
        '
        'Line130
        '
        Me.Line130.Height = 0!
        Me.Line130.Left = 2.248999!
        Me.Line130.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line130.LineWeight = 1.0!
        Me.Line130.Name = "Line130"
        Me.Line130.Top = 7.687003!
        Me.Line130.Width = 2.0!
        Me.Line130.X1 = 2.248999!
        Me.Line130.X2 = 4.248999!
        Me.Line130.Y1 = 7.687003!
        Me.Line130.Y2 = 7.687003!
        '
        'Line131
        '
        Me.Line131.Height = 0!
        Me.Line131.Left = 2.248999!
        Me.Line131.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line131.LineWeight = 1.0!
        Me.Line131.Name = "Line131"
        Me.Line131.Top = 7.5!
        Me.Line131.Width = 2.0!
        Me.Line131.X1 = 2.248999!
        Me.Line131.X2 = 4.248999!
        Me.Line131.Y1 = 7.5!
        Me.Line131.Y2 = 7.5!
        '
        'Label511
        '
        Me.Label511.Height = 0.1875!
        Me.Label511.HyperLink = Nothing
        Me.Label511.Left = 4.25!
        Me.Label511.Name = "Label511"
        Me.Label511.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label511.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (Pcap_link - ΦRn_Stiff_bot < 0, 0, Pcap_link - ΦRn_Stiff_bot)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label511.Top = 7.687!
        Me.Label511.Width = 3.75!
        '
        'Label512
        '
        Me.Label512.Height = 0.1875!
        Me.Label512.HyperLink = Nothing
        Me.Label512.Left = 0!
        Me.Label512.Name = "Label512"
        Me.Label512.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label512.Text = "Fsu=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label512.Top = 7.875!
        Me.Label512.Width = 2.25!
        '
        'S311_006
        '
        Me.S311_006.Height = 0.1875!
        Me.S311_006.HyperLink = Nothing
        Me.S311_006.Left = 3.25!
        Me.S311_006.Name = "S311_006"
        Me.S311_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S311_006.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S311_006.Top = 7.875!
        Me.S311_006.Width = 1.0!
        '
        'Label515
        '
        Me.Label515.Height = 0.1875!
        Me.Label515.HyperLink = Nothing
        Me.Label515.Left = 4.25!
        Me.Label515.Name = "Label515"
        Me.Label515.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label515.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= max (Fsu_top, Fsu_bot)* SMF_Sides" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label515.Top = 7.875!
        Me.Label515.Width = 3.75!
        '
        'Line132
        '
        Me.Line132.Height = 0!
        Me.Line132.Left = 2.249!
        Me.Line132.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line132.LineWeight = 1.0!
        Me.Line132.Name = "Line132"
        Me.Line132.Top = 7.875!
        Me.Line132.Width = 2.0!
        Me.Line132.X1 = 2.249!
        Me.Line132.X2 = 4.249!
        Me.Line132.Y1 = 7.875!
        Me.Line132.Y2 = 7.875!
        '
        'Line133
        '
        Me.Line133.Height = 0.7499986!
        Me.Line133.Left = 2.25!
        Me.Line133.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line133.LineWeight = 1.0!
        Me.Line133.Name = "Line133"
        Me.Line133.Top = 7.312!
        Me.Line133.Width = 0!
        Me.Line133.X1 = 2.25!
        Me.Line133.X2 = 2.25!
        Me.Line133.Y1 = 7.312!
        Me.Line133.Y2 = 8.061998!
        '
        'Line134
        '
        Me.Line134.Height = 0.7499986!
        Me.Line134.Left = 3.25!
        Me.Line134.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line134.LineWeight = 1.0!
        Me.Line134.Name = "Line134"
        Me.Line134.Top = 7.312!
        Me.Line134.Width = 0!
        Me.Line134.X1 = 3.25!
        Me.Line134.X2 = 3.25!
        Me.Line134.Y1 = 7.312!
        Me.Line134.Y2 = 8.061998!
        '
        'Line137
        '
        Me.Line137.Height = 0!
        Me.Line137.Left = 2.249!
        Me.Line137.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line137.LineWeight = 1.0!
        Me.Line137.Name = "Line137"
        Me.Line137.Top = 7.312003!
        Me.Line137.Width = 2.0!
        Me.Line137.X1 = 2.249!
        Me.Line137.X2 = 4.249!
        Me.Line137.Y1 = 7.312003!
        Me.Line137.Y2 = 7.312003!
        '
        'Label516
        '
        Me.Label516.Height = 0.1875!
        Me.Label516.HyperLink = Nothing
        Me.Label516.Left = 4.25!
        Me.Label516.Name = "Label516"
        Me.Label516.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label516.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (Pcap_link - ΦRn_Stiff_top < 0, 0, Pcap_link - ΦRn_Stiff_top)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label516.Top = 7.5!
        Me.Label516.Width = 3.75!
        '
        'Line135
        '
        Me.Line135.Height = 0.7499971!
        Me.Line135.Left = 4.249001!
        Me.Line135.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line135.LineWeight = 1.0!
        Me.Line135.Name = "Line135"
        Me.Line135.Top = 7.312003!
        Me.Line135.Width = 0!
        Me.Line135.X1 = 4.249001!
        Me.Line135.X2 = 4.249001!
        Me.Line135.Y1 = 7.312003!
        Me.Line135.Y2 = 8.062!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 2.249001!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 7.500001!
        Me.Line3.Width = 2.0!
        Me.Line3.X1 = 2.249001!
        Me.Line3.X2 = 4.249001!
        Me.Line3.Y1 = 7.500001!
        Me.Line3.Y2 = 7.500001!
        '
        'S312_006
        '
        Me.S312_006.Height = 0.1875!
        Me.S312_006.HyperLink = Nothing
        Me.S312_006.Left = 2.25!
        Me.S312_006.Name = "S312_006"
        Me.S312_006.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S312_006.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_006.Top = 9.375!
        Me.S312_006.Width = 1.0!
        '
        'Label535
        '
        Me.Label535.Height = 0.1875!
        Me.Label535.HyperLink = Nothing
        Me.Label535.Left = 2.25!
        Me.Label535.Name = "Label535"
        Me.Label535.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label535.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label535.Top = 9.562!
        Me.Label535.Width = 1.0!
        '
        'Label534
        '
        Me.Label534.Height = 0.1875!
        Me.Label534.HyperLink = Nothing
        Me.Label534.Left = 3.25!
        Me.Label534.Name = "Label534"
        Me.Label534.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label534.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label534.Top = 9.562!
        Me.Label534.Width = 1.0!
        '
        'Label517
        '
        Me.Label517.Height = 0.1875!
        Me.Label517.HyperLink = Nothing
        Me.Label517.Left = 0.0000002384186!
        Me.Label517.Name = "Label517"
        Me.Label517.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label517.Text = "3.12 CONTINUITY PLATE REQUIREMENTS BASED ON GEOMETRY AND TENSION YIELDING:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label517.Top = 8.25!
        Me.Label517.Width = 6.061999!
        '
        'Label520
        '
        Me.Label520.Height = 0.1875!
        Me.Label520.HyperLink = Nothing
        Me.Label520.Left = 0!
        Me.Label520.Name = "Label520"
        Me.Label520.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label520.Text = "Φt=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label520.Top = 8.437!
        Me.Label520.Width = 2.25!
        '
        'S312_001
        '
        Me.S312_001.Height = 0.1875!
        Me.S312_001.HyperLink = Nothing
        Me.S312_001.Left = 2.25!
        Me.S312_001.Name = "S312_001"
        Me.S312_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S312_001.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_001.Top = 8.438001!
        Me.S312_001.Width = 1.0!
        '
        'Label522
        '
        Me.Label522.Height = 0.1875!
        Me.Label522.HyperLink = Nothing
        Me.Label522.Left = 0!
        Me.Label522.Name = "Label522"
        Me.Label522.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label522.Text = "Fy_stiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label522.Top = 8.624001!
        Me.Label522.Width = 2.25!
        '
        'S312_002
        '
        Me.S312_002.Height = 0.1875!
        Me.S312_002.HyperLink = Nothing
        Me.S312_002.Left = 2.25!
        Me.S312_002.Name = "S312_002"
        Me.S312_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S312_002.Text = "50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_002.Top = 8.625!
        Me.S312_002.Width = 1.0!
        '
        'Label524
        '
        Me.Label524.Height = 0.1875!
        Me.Label524.HyperLink = Nothing
        Me.Label524.Left = 4.25!
        Me.Label524.Name = "Label524"
        Me.Label524.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label524.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label524.Top = 8.625!
        Me.Label524.Width = 3.753!
        '
        'Label525
        '
        Me.Label525.Height = 0.1875!
        Me.Label525.HyperLink = Nothing
        Me.Label525.Left = 0!
        Me.Label525.Name = "Label525"
        Me.Label525.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label525.Text = "Kdet=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label525.Top = 8.812!
        Me.Label525.Width = 2.25!
        '
        'S312_003
        '
        Me.S312_003.Height = 0.1875!
        Me.S312_003.HyperLink = Nothing
        Me.S312_003.Left = 2.25!
        Me.S312_003.Name = "S312_003"
        Me.S312_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S312_003.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_003.Top = 8.813001!
        Me.S312_003.Width = 1.0!
        '
        'Label527
        '
        Me.Label527.Height = 0.1875!
        Me.Label527.HyperLink = Nothing
        Me.Label527.Left = 4.25!
        Me.Label527.Name = "Label527"
        Me.Label527.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label527.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label527.Top = 8.813001!
        Me.Label527.Width = 3.753!
        '
        'Label528
        '
        Me.Label528.Height = 0.1875!
        Me.Label528.HyperLink = Nothing
        Me.Label528.Left = 0.002000004!
        Me.Label528.Name = "Label528"
        Me.Label528.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label528.Text = "As_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label528.Top = 9.75!
        Me.Label528.Width = 2.25!
        '
        'S312_007
        '
        Me.S312_007.Height = 0.1875!
        Me.S312_007.HyperLink = Nothing
        Me.S312_007.Left = 2.250001!
        Me.S312_007.Name = "S312_007"
        Me.S312_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_007.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_007.Top = 9.75!
        Me.S312_007.Width = 1.0!
        '
        'Label530
        '
        Me.Label530.Height = 0.1875!
        Me.Label530.HyperLink = Nothing
        Me.Label530.Left = 0!
        Me.Label530.Name = "Label530"
        Me.Label530.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label530.Text = "Lclip_web_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label530.Top = 9.938001!
        Me.Label530.Width = 2.25!
        '
        'S312_008
        '
        Me.S312_008.Height = 0.1875!
        Me.S312_008.HyperLink = Nothing
        Me.S312_008.Left = 2.250001!
        Me.S312_008.Name = "S312_008"
        Me.S312_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_008.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_008.Top = 9.937!
        Me.S312_008.Width = 1.0!
        '
        'S312_024
        '
        Me.S312_024.Height = 0.1875!
        Me.S312_024.HyperLink = Nothing
        Me.S312_024.Left = 3.25!
        Me.S312_024.Name = "S312_024"
        Me.S312_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_024.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_024.Top = 9.75!
        Me.S312_024.Width = 1.0!
        '
        'S312_025
        '
        Me.S312_025.Height = 0.1875!
        Me.S312_025.HyperLink = Nothing
        Me.S312_025.Left = 3.25!
        Me.S312_025.Name = "S312_025"
        Me.S312_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_025.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_025.Top = 9.937!
        Me.S312_025.Width = 1.0!
        '
        'Line138
        '
        Me.Line138.Height = 0!
        Me.Line138.Left = 2.249!
        Me.Line138.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line138.LineWeight = 1.0!
        Me.Line138.Name = "Line138"
        Me.Line138.Top = 9.937!
        Me.Line138.Width = 1.999997!
        Me.Line138.X1 = 2.249!
        Me.Line138.X2 = 4.248997!
        Me.Line138.Y1 = 9.937!
        Me.Line138.Y2 = 9.937!
        '
        'Line139
        '
        Me.Line139.Height = 0!
        Me.Line139.Left = 2.249!
        Me.Line139.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line139.LineWeight = 1.0!
        Me.Line139.Name = "Line139"
        Me.Line139.Top = 9.75!
        Me.Line139.Width = 1.999998!
        Me.Line139.X1 = 2.249!
        Me.Line139.X2 = 4.248998!
        Me.Line139.Y1 = 9.75!
        Me.Line139.Y2 = 9.75!
        '
        'Line144
        '
        Me.Line144.Height = 0!
        Me.Line144.Left = 2.241998!
        Me.Line144.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line144.LineWeight = 1.0!
        Me.Line144.Name = "Line144"
        Me.Line144.Top = 10.125!
        Me.Line144.Width = 1.999999!
        Me.Line144.X1 = 2.241998!
        Me.Line144.X2 = 4.241997!
        Me.Line144.Y1 = 10.125!
        Me.Line144.Y2 = 10.125!
        '
        'Label536
        '
        Me.Label536.Height = 0.1875!
        Me.Label536.HyperLink = Nothing
        Me.Label536.Left = 4.25!
        Me.Label536.Name = "Label536"
        Me.Label536.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label536.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label536.Top = 8.438001!
        Me.Label536.Width = 3.748!
        '
        'Label537
        '
        Me.Label537.Height = 0.1875!
        Me.Label537.HyperLink = Nothing
        Me.Label537.Left = 0!
        Me.Label537.Name = "Label537"
        Me.Label537.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label537.Text = "K1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label537.Top = 8.999001!
        Me.Label537.Width = 2.25!
        '
        'S312_004
        '
        Me.S312_004.Height = 0.1875!
        Me.S312_004.HyperLink = Nothing
        Me.S312_004.Left = 2.25!
        Me.S312_004.Name = "S312_004"
        Me.S312_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S312_004.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_004.Top = 9.0!
        Me.S312_004.Width = 1.0!
        '
        'Label539
        '
        Me.Label539.Height = 0.1875!
        Me.Label539.HyperLink = Nothing
        Me.Label539.Left = 4.25!
        Me.Label539.Name = "Label539"
        Me.Label539.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label539.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label539.Top = 9.0!
        Me.Label539.Width = 3.753!
        '
        'Label540
        '
        Me.Label540.Height = 0.1875!
        Me.Label540.HyperLink = Nothing
        Me.Label540.Left = 0!
        Me.Label540.Name = "Label540"
        Me.Label540.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label540.Text = "tcwdet/2 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label540.Top = 9.187!
        Me.Label540.Width = 2.25!
        '
        'S312_005
        '
        Me.S312_005.Height = 0.1875!
        Me.S312_005.HyperLink = Nothing
        Me.S312_005.Left = 2.25!
        Me.S312_005.Name = "S312_005"
        Me.S312_005.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S312_005.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_005.Top = 9.188001!
        Me.S312_005.Width = 1.0!
        '
        'Label542
        '
        Me.Label542.Height = 0.1875!
        Me.Label542.HyperLink = Nothing
        Me.Label542.Left = 4.25!
        Me.Label542.Name = "Label542"
        Me.Label542.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label542.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label542.Top = 9.188001!
        Me.Label542.Width = 3.753!
        '
        'Label543
        '
        Me.Label543.Height = 0.1875!
        Me.Label543.HyperLink = Nothing
        Me.Label543.Left = 0!
        Me.Label543.Name = "Label543"
        Me.Label543.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label543.Text = "tcf(det) =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label543.Top = 9.374001!
        Me.Label543.Width = 2.25!
        '
        'Label545
        '
        Me.Label545.Height = 0.1875!
        Me.Label545.HyperLink = Nothing
        Me.Label545.Left = 4.251!
        Me.Label545.Name = "Label545"
        Me.Label545.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label545.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label545.Top = 9.375!
        Me.Label545.Width = 3.753!
        '
        'Line143
        '
        Me.Line143.Height = 0!
        Me.Line143.Left = 2.251998!
        Me.Line143.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line143.LineWeight = 1.0!
        Me.Line143.Name = "Line143"
        Me.Line143.Top = 9.562!
        Me.Line143.Width = 2.0!
        Me.Line143.X1 = 2.251998!
        Me.Line143.X2 = 4.251998!
        Me.Line143.Y1 = 9.562!
        Me.Line143.Y2 = 9.562!
        '
        'Label547
        '
        Me.Label547.Height = 0.1875!
        Me.Label547.HyperLink = Nothing
        Me.Label547.Left = 0!
        Me.Label547.Name = "Label547"
        Me.Label547.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label547.Text = "Lclip_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label547.Top = 10.126!
        Me.Label547.Width = 2.25!
        '
        'S312_009
        '
        Me.S312_009.Height = 0.1875!
        Me.S312_009.HyperLink = Nothing
        Me.S312_009.Left = 2.252!
        Me.S312_009.Name = "S312_009"
        Me.S312_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_009.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_009.Top = 10.125!
        Me.S312_009.Width = 1.0!
        '
        'S312_026
        '
        Me.S312_026.Height = 0.1875!
        Me.S312_026.HyperLink = Nothing
        Me.S312_026.Left = 3.252!
        Me.S312_026.Name = "S312_026"
        Me.S312_026.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_026.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_026.Top = 10.125!
        Me.S312_026.Width = 1.0!
        '
        'Label551
        '
        Me.Label551.Height = 0.1875!
        Me.Label551.HyperLink = Nothing
        Me.Label551.Left = 0!
        Me.Label551.Name = "Label551"
        Me.Label551.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label551.Text = "Lclip_flange_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label551.Top = 10.314!
        Me.Label551.Width = 2.25!
        '
        'S312_010
        '
        Me.S312_010.Height = 0.1875!
        Me.S312_010.HyperLink = Nothing
        Me.S312_010.Left = 2.255!
        Me.S312_010.Name = "S312_010"
        Me.S312_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_010.Top = 10.313!
        Me.S312_010.Width = 1.0!
        '
        'S312_027
        '
        Me.S312_027.Height = 0.1875!
        Me.S312_027.HyperLink = Nothing
        Me.S312_027.Left = 3.255!
        Me.S312_027.Name = "S312_027"
        Me.S312_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_027.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_027.Top = 10.313!
        Me.S312_027.Width = 1.0!
        '
        'Label555
        '
        Me.Label555.Height = 0.1875!
        Me.Label555.HyperLink = Nothing
        Me.Label555.Left = 0!
        Me.Label555.Name = "Label555"
        Me.Label555.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label555.Text = "Lclip_flange=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label555.Top = 10.502!
        Me.Label555.Width = 2.25!
        '
        'S312_011
        '
        Me.S312_011.Height = 0.1875!
        Me.S312_011.HyperLink = Nothing
        Me.S312_011.Left = 2.25!
        Me.S312_011.Name = "S312_011"
        Me.S312_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_011.Top = 10.501!
        Me.S312_011.Width = 1.0!
        '
        'S312_028
        '
        Me.S312_028.Height = 0.1875!
        Me.S312_028.HyperLink = Nothing
        Me.S312_028.Left = 3.25!
        Me.S312_028.Name = "S312_028"
        Me.S312_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_028.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_028.Top = 10.501!
        Me.S312_028.Width = 1.0!
        '
        'Label559
        '
        Me.Label559.Height = 0.1875!
        Me.Label559.HyperLink = Nothing
        Me.Label559.Left = 0!
        Me.Label559.Name = "Label559"
        Me.Label559.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label559.Text = "bstiff_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label559.Top = 10.689!
        Me.Label559.Width = 2.25!
        '
        'S312_012
        '
        Me.S312_012.Height = 0.1875!
        Me.S312_012.HyperLink = Nothing
        Me.S312_012.Left = 2.254998!
        Me.S312_012.Name = "S312_012"
        Me.S312_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_012.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_012.Top = 10.688!
        Me.S312_012.Width = 1.0!
        '
        'S312_029
        '
        Me.S312_029.Height = 0.1875!
        Me.S312_029.HyperLink = Nothing
        Me.S312_029.Left = 3.254998!
        Me.S312_029.Name = "S312_029"
        Me.S312_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_029.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_029.Top = 10.688!
        Me.S312_029.Width = 1.0!
        '
        'Label563
        '
        Me.Label563.Height = 0.1875!
        Me.Label563.HyperLink = Nothing
        Me.Label563.Left = 0!
        Me.Label563.Name = "Label563"
        Me.Label563.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label563.Text = "bstiff_max=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label563.Top = 10.877!
        Me.Label563.Width = 2.25!
        '
        'S312_013
        '
        Me.S312_013.Height = 0.1875!
        Me.S312_013.HyperLink = Nothing
        Me.S312_013.Left = 2.249998!
        Me.S312_013.Name = "S312_013"
        Me.S312_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_013.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_013.Top = 10.876!
        Me.S312_013.Width = 1.0!
        '
        'S312_030
        '
        Me.S312_030.Height = 0.1875!
        Me.S312_030.HyperLink = Nothing
        Me.S312_030.Left = 3.249998!
        Me.S312_030.Name = "S312_030"
        Me.S312_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_030.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_030.Top = 10.876!
        Me.S312_030.Width = 1.0!
        '
        'Label567
        '
        Me.Label567.Height = 0.1875!
        Me.Label567.HyperLink = Nothing
        Me.Label567.Left = 0!
        Me.Label567.Name = "Label567"
        Me.Label567.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label567.Text = "bstiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label567.Top = 11.064!
        Me.Label567.Width = 2.25!
        '
        'S312_014
        '
        Me.S312_014.Height = 0.1875!
        Me.S312_014.HyperLink = Nothing
        Me.S312_014.Left = 2.250997!
        Me.S312_014.Name = "S312_014"
        Me.S312_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_014.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_014.Top = 11.063!
        Me.S312_014.Width = 1.0!
        '
        'S312_031
        '
        Me.S312_031.Height = 0.1875!
        Me.S312_031.HyperLink = Nothing
        Me.S312_031.Left = 3.250997!
        Me.S312_031.Name = "S312_031"
        Me.S312_031.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_031.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_031.Top = 11.063!
        Me.S312_031.Width = 1.0!
        '
        'Label571
        '
        Me.Label571.Height = 0.1875!
        Me.Label571.HyperLink = Nothing
        Me.Label571.Left = 0!
        Me.Label571.Name = "Label571"
        Me.Label571.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label571.Text = "tstiff_USE?" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label571.Top = 11.252!
        Me.Label571.Width = 2.25!
        '
        'S312_015
        '
        Me.S312_015.Height = 0.1875!
        Me.S312_015.HyperLink = Nothing
        Me.S312_015.Left = 2.252!
        Me.S312_015.Name = "S312_015"
        Me.S312_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S312_015.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_015.Top = 11.251!
        Me.S312_015.Width = 1.0!
        '
        'S312_032
        '
        Me.S312_032.Height = 0.1875!
        Me.S312_032.HyperLink = Nothing
        Me.S312_032.Left = 3.252!
        Me.S312_032.Name = "S312_032"
        Me.S312_032.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S312_032.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_032.Top = 11.251!
        Me.S312_032.Width = 1.0!
        '
        'Label575
        '
        Me.Label575.Height = 0.1875!
        Me.Label575.HyperLink = Nothing
        Me.Label575.Left = 0!
        Me.Label575.Name = "Label575"
        Me.Label575.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label575.Text = "t_stiff_tension=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label575.Top = 11.439!
        Me.Label575.Width = 2.25!
        '
        'S312_016
        '
        Me.S312_016.Height = 0.1875!
        Me.S312_016.HyperLink = Nothing
        Me.S312_016.Left = 2.255!
        Me.S312_016.Name = "S312_016"
        Me.S312_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_016.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_016.Top = 11.438!
        Me.S312_016.Width = 1.0!
        '
        'S312_033
        '
        Me.S312_033.Height = 0.1875!
        Me.S312_033.HyperLink = Nothing
        Me.S312_033.Left = 3.255!
        Me.S312_033.Name = "S312_033"
        Me.S312_033.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_033.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_033.Top = 11.438!
        Me.S312_033.Width = 1.0!
        '
        'Label579
        '
        Me.Label579.Height = 0.1875!
        Me.Label579.HyperLink = Nothing
        Me.Label579.Left = 0!
        Me.Label579.Name = "Label579"
        Me.Label579.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label579.Text = "tstiff_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label579.Top = 11.627!
        Me.Label579.Width = 2.25!
        '
        'S312_017
        '
        Me.S312_017.Height = 0.1875!
        Me.S312_017.HyperLink = Nothing
        Me.S312_017.Left = 2.249998!
        Me.S312_017.Name = "S312_017"
        Me.S312_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_017.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_017.Top = 11.626!
        Me.S312_017.Width = 1.0!
        '
        'S312_034
        '
        Me.S312_034.Height = 0.1875!
        Me.S312_034.HyperLink = Nothing
        Me.S312_034.Left = 3.249998!
        Me.S312_034.Name = "S312_034"
        Me.S312_034.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_034.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_034.Top = 11.626!
        Me.S312_034.Width = 1.0!
        '
        'Label583
        '
        Me.Label583.Height = 0.1875!
        Me.Label583.HyperLink = Nothing
        Me.Label583.Left = 0!
        Me.Label583.Name = "Label583"
        Me.Label583.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label583.Text = "tstiff_max=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label583.Top = 11.814!
        Me.Label583.Width = 2.25!
        '
        'S312_018
        '
        Me.S312_018.Height = 0.1875!
        Me.S312_018.HyperLink = Nothing
        Me.S312_018.Left = 2.248997!
        Me.S312_018.Name = "S312_018"
        Me.S312_018.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S312_018.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_018.Top = 11.813!
        Me.S312_018.Width = 1.0!
        '
        'S312_035
        '
        Me.S312_035.Height = 0.1875!
        Me.S312_035.HyperLink = Nothing
        Me.S312_035.Left = 3.248997!
        Me.S312_035.Name = "S312_035"
        Me.S312_035.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S312_035.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_035.Top = 11.813!
        Me.S312_035.Width = 1.0!
        '
        'S312_020
        '
        Me.S312_020.Height = 0.1875!
        Me.S312_020.HyperLink = Nothing
        Me.S312_020.Left = 2.252!
        Me.S312_020.Name = "S312_020"
        Me.S312_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_020.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_020.Top = 12.375!
        Me.S312_020.Width = 1.0!
        '
        'Label587
        '
        Me.Label587.Height = 0.1875!
        Me.Label587.HyperLink = Nothing
        Me.Label587.Left = 0!
        Me.Label587.Name = "Label587"
        Me.Label587.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label587.Text = "Lstiff_Pdepth_ini=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label587.Top = 12.375!
        Me.Label587.Width = 2.25!
        '
        'Label588
        '
        Me.Label588.Height = 0.1875!
        Me.Label588.HyperLink = Nothing
        Me.Label588.Left = 3.251!
        Me.Label588.Name = "Label588"
        Me.Label588.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label588.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.5* dc - tf, For Single sided connections" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label588.Top = 12.375!
        Me.Label588.Width = 4.747!
        '
        'S312_022
        '
        Me.S312_022.Height = 0.1875!
        Me.S312_022.HyperLink = Nothing
        Me.S312_022.Left = 2.252!
        Me.S312_022.Name = "S312_022"
        Me.S312_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_022.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_022.Top = 12.75!
        Me.S312_022.Width = 1.0!
        '
        'Label593
        '
        Me.Label593.Height = 0.1875!
        Me.Label593.HyperLink = Nothing
        Me.Label593.Left = 0.001999974!
        Me.Label593.Name = "Label593"
        Me.Label593.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label593.Text = "Lstiff_ini=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label593.Top = 12.75!
        Me.Label593.Width = 2.25!
        '
        'Label594
        '
        Me.Label594.Height = 0.1875!
        Me.Label594.HyperLink = Nothing
        Me.Label594.Left = 3.252!
        Me.Label594.Name = "Label594"
        Me.Label594.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label594.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (SMF_Sides = 1, Lstiff_Pdepth_ini, Lstiff_Fdepth_ini)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label594.Top = 12.75!
        Me.Label594.Width = 4.746!
        '
        'S312_023
        '
        Me.S312_023.Height = 0.1875!
        Me.S312_023.HyperLink = Nothing
        Me.S312_023.Left = 2.252!
        Me.S312_023.Name = "S312_023"
        Me.S312_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_023.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_023.Top = 12.938!
        Me.S312_023.Width = 1.0!
        '
        'Label596
        '
        Me.Label596.Height = 0.1875!
        Me.Label596.HyperLink = Nothing
        Me.Label596.Left = 0.001999974!
        Me.Label596.Name = "Label596"
        Me.Label596.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label596.Text = "Lstiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label596.Top = 12.938!
        Me.Label596.Width = 2.25!
        '
        'Label597
        '
        Me.Label597.Height = 0.1875!
        Me.Label597.HyperLink = Nothing
        Me.Label597.Left = 3.251!
        Me.Label597.Name = "Label597"
        Me.Label597.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label597.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Floor (Lstiff_ini, 0.0625)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label597.Top = 12.938!
        Me.Label597.Width = 4.749!
        '
        'S312_021
        '
        Me.S312_021.Height = 0.1875!
        Me.S312_021.HyperLink = Nothing
        Me.S312_021.Left = 2.252!
        Me.S312_021.Name = "S312_021"
        Me.S312_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S312_021.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S312_021.Top = 12.562!
        Me.S312_021.Width = 1.0!
        '
        'Label590
        '
        Me.Label590.Height = 0.1875!
        Me.Label590.HyperLink = Nothing
        Me.Label590.Left = 0.001999974!
        Me.Label590.Name = "Label590"
        Me.Label590.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label590.Text = "Lstiff_Fdepth_ini=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label590.Top = 12.562!
        Me.Label590.Width = 2.25!
        '
        'Label591
        '
        Me.Label591.Height = 0.1875!
        Me.Label591.HyperLink = Nothing
        Me.Label591.Left = 3.251!
        Me.Label591.Name = "Label591"
        Me.Label591.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label591.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= dc - 2*tcf, For double sided connections" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label591.Top = 12.562!
        Me.Label591.Width = 4.747!
        '
        'Line145
        '
        Me.Line145.Height = 0!
        Me.Line145.Left = 2.252!
        Me.Line145.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line145.LineWeight = 1.0!
        Me.Line145.Name = "Line145"
        Me.Line145.Top = 10.125!
        Me.Line145.Width = 1.999998!
        Me.Line145.X1 = 2.252!
        Me.Line145.X2 = 4.251998!
        Me.Line145.Y1 = 10.125!
        Me.Line145.Y2 = 10.125!
        '
        'Line146
        '
        Me.Line146.Height = 0!
        Me.Line146.Left = 2.249!
        Me.Line146.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line146.LineWeight = 1.0!
        Me.Line146.Name = "Line146"
        Me.Line146.Top = 10.313!
        Me.Line146.Width = 1.999999!
        Me.Line146.X1 = 2.249!
        Me.Line146.X2 = 4.248999!
        Me.Line146.Y1 = 10.313!
        Me.Line146.Y2 = 10.313!
        '
        'Line147
        '
        Me.Line147.Height = 0!
        Me.Line147.Left = 2.252!
        Me.Line147.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line147.LineWeight = 1.0!
        Me.Line147.Name = "Line147"
        Me.Line147.Top = 10.501!
        Me.Line147.Width = 1.999997!
        Me.Line147.X1 = 2.252!
        Me.Line147.X2 = 4.251997!
        Me.Line147.Y1 = 10.501!
        Me.Line147.Y2 = 10.501!
        '
        'Line148
        '
        Me.Line148.Height = 0!
        Me.Line148.Left = 2.255!
        Me.Line148.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line148.LineWeight = 1.0!
        Me.Line148.Name = "Line148"
        Me.Line148.Top = 10.688!
        Me.Line148.Width = 1.999998!
        Me.Line148.X1 = 2.255!
        Me.Line148.X2 = 4.254998!
        Me.Line148.Y1 = 10.688!
        Me.Line148.Y2 = 10.688!
        '
        'Line149
        '
        Me.Line149.Height = 0!
        Me.Line149.Left = 2.255!
        Me.Line149.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line149.LineWeight = 1.0!
        Me.Line149.Name = "Line149"
        Me.Line149.Top = 10.876!
        Me.Line149.Width = 1.999998!
        Me.Line149.X1 = 2.255!
        Me.Line149.X2 = 4.254998!
        Me.Line149.Y1 = 10.876!
        Me.Line149.Y2 = 10.876!
        '
        'Line150
        '
        Me.Line150.Height = 0!
        Me.Line150.Left = 2.249!
        Me.Line150.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line150.LineWeight = 1.0!
        Me.Line150.Name = "Line150"
        Me.Line150.Top = 11.063!
        Me.Line150.Width = 1.999999!
        Me.Line150.X1 = 2.249!
        Me.Line150.X2 = 4.248999!
        Me.Line150.Y1 = 11.063!
        Me.Line150.Y2 = 11.063!
        '
        'Line151
        '
        Me.Line151.Height = 0!
        Me.Line151.Left = 2.249!
        Me.Line151.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line151.LineWeight = 1.0!
        Me.Line151.Name = "Line151"
        Me.Line151.Top = 11.251!
        Me.Line151.Width = 1.999999!
        Me.Line151.X1 = 2.249!
        Me.Line151.X2 = 4.248999!
        Me.Line151.Y1 = 11.251!
        Me.Line151.Y2 = 11.251!
        '
        'Line152
        '
        Me.Line152.Height = 0!
        Me.Line152.Left = 2.252!
        Me.Line152.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line152.LineWeight = 1.0!
        Me.Line152.Name = "Line152"
        Me.Line152.Top = 11.438!
        Me.Line152.Width = 1.999998!
        Me.Line152.X1 = 2.252!
        Me.Line152.X2 = 4.251998!
        Me.Line152.Y1 = 11.438!
        Me.Line152.Y2 = 11.438!
        '
        'Line153
        '
        Me.Line153.Height = 0!
        Me.Line153.Left = 2.252!
        Me.Line153.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line153.LineWeight = 1.0!
        Me.Line153.Name = "Line153"
        Me.Line153.Top = 11.626!
        Me.Line153.Width = 1.999999!
        Me.Line153.X1 = 2.252!
        Me.Line153.X2 = 4.251999!
        Me.Line153.Y1 = 11.626!
        Me.Line153.Y2 = 11.626!
        '
        'Line154
        '
        Me.Line154.Height = 0!
        Me.Line154.Left = 2.252!
        Me.Line154.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line154.LineWeight = 1.0!
        Me.Line154.Name = "Line154"
        Me.Line154.Top = 11.813!
        Me.Line154.Width = 1.999998!
        Me.Line154.X1 = 2.252!
        Me.Line154.X2 = 4.251998!
        Me.Line154.Y1 = 11.813!
        Me.Line154.Y2 = 11.813!
        '
        'Line155
        '
        Me.Line155.Height = 0!
        Me.Line155.Left = 2.252!
        Me.Line155.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line155.LineWeight = 1.0!
        Me.Line155.Name = "Line155"
        Me.Line155.Top = 12.001!
        Me.Line155.Width = 1.999999!
        Me.Line155.X1 = 2.252!
        Me.Line155.X2 = 4.251999!
        Me.Line155.Y1 = 12.001!
        Me.Line155.Y2 = 12.001!
        '
        'Line141
        '
        Me.Line141.Height = 2.438!
        Me.Line141.Left = 3.249998!
        Me.Line141.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line141.LineWeight = 1.0!
        Me.Line141.Name = "Line141"
        Me.Line141.Top = 9.562!
        Me.Line141.Width = 0!
        Me.Line141.X1 = 3.249998!
        Me.Line141.X2 = 3.249998!
        Me.Line141.Y1 = 9.562!
        Me.Line141.Y2 = 12.0!
        '
        'Line142
        '
        Me.Line142.Height = 2.438!
        Me.Line142.Left = 4.25!
        Me.Line142.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line142.LineWeight = 1.0!
        Me.Line142.Name = "Line142"
        Me.Line142.Top = 9.562!
        Me.Line142.Width = 0!
        Me.Line142.X1 = 4.25!
        Me.Line142.X2 = 4.25!
        Me.Line142.Y1 = 9.562!
        Me.Line142.Y2 = 12.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 4.187!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Bottom Stiffener Required=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 1.521!
        Me.Label1.Width = 1.674999!
        '
        'S39_017
        '
        Me.S39_017.Height = 0.1875!
        Me.S39_017.HyperLink = Nothing
        Me.S39_017.Left = 5.862!
        Me.S39_017.Name = "S39_017"
        Me.S39_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_017.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_017.Top = 1.521!
        Me.S39_017.Width = 1.0!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 4.187!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label3.Text = "Bottom Stiff Provided?:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label3.Top = 1.709!
        Me.Label3.Width = 1.674999!
        '
        'S39_018
        '
        Me.S39_018.Height = 0.1875!
        Me.S39_018.HyperLink = Nothing
        Me.S39_018.Left = 5.862!
        Me.S39_018.Name = "S39_018"
        Me.S39_018.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_018.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_018.Top = 1.709!
        Me.S39_018.Width = 1.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "Top Stiffener Required=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 3.439!
        Me.Label5.Width = 2.25!
        '
        'S39_011
        '
        Me.S39_011.Height = 0.1875!
        Me.S39_011.HyperLink = Nothing
        Me.S39_011.Left = 2.25!
        Me.S39_011.Name = "S39_011"
        Me.S39_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_011.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_011.Top = 3.439!
        Me.S39_011.Width = 1.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "Top Stiff Provided?:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 3.627!
        Me.Label7.Width = 2.25!
        '
        'S39_012
        '
        Me.S39_012.Height = 0.1875!
        Me.S39_012.HyperLink = Nothing
        Me.S39_012.Left = 2.25!
        Me.S39_012.Name = "S39_012"
        Me.S39_012.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_012.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_012.Top = 3.627!
        Me.S39_012.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 4.187!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Bottom Stiffener Required=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 3.439!
        Me.Label9.Width = 1.674999!
        '
        'S39_024
        '
        Me.S39_024.Height = 0.1875!
        Me.S39_024.HyperLink = Nothing
        Me.S39_024.Left = 5.862!
        Me.S39_024.Name = "S39_024"
        Me.S39_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S39_024.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_024.Top = 3.439!
        Me.S39_024.Width = 1.0!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 4.187!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label11.Text = "Bottom Stiff Provided?:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label11.Top = 3.627!
        Me.Label11.Width = 1.674999!
        '
        'S39_025
        '
        Me.S39_025.Height = 0.1875!
        Me.S39_025.HyperLink = Nothing
        Me.S39_025.Left = 5.862!
        Me.S39_025.Name = "S39_025"
        Me.S39_025.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S39_025.Text = "YES" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S39_025.Top = 3.627!
        Me.S39_025.Width = 1.0!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 2.249001!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 8.062!
        Me.Line4.Width = 1.999999!
        Me.Line4.X1 = 2.249001!
        Me.Line4.X2 = 4.249!
        Me.Line4.Y1 = 8.062!
        Me.Line4.Y2 = 8.062!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label15.Text = "t_stiff_Use=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 12.0!
        Me.Label15.Width = 2.25!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 2.25!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 6.750001!
        Me.Line5.Width = 2.0!
        Me.Line5.X1 = 2.25!
        Me.Line5.X2 = 4.25!
        Me.Line5.Y1 = 6.750001!
        Me.Line5.Y2 = 6.750001!
        '
        'Line140
        '
        Me.Line140.Height = 2.625!
        Me.Line140.Left = 2.25!
        Me.Line140.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line140.LineWeight = 1.0!
        Me.Line140.Name = "Line140"
        Me.Line140.Top = 9.562!
        Me.Line140.Width = 0!
        Me.Line140.X1 = 2.25!
        Me.Line140.X2 = 2.25!
        Me.Line140.Y1 = 9.562!
        Me.Line140.Y2 = 12.187!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 5.861!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 3.815!
        Me.Shape1.Width = 1.0!
        '
        'Shape7
        '
        Me.Shape7.Height = 0.1875!
        Me.Shape7.Left = 5.861!
        Me.Shape7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape7.Name = "Shape7"
        Me.Shape7.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape7.Top = 1.896!
        Me.Shape7.Width = 1.0!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 2.26!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 5.615!
        Me.Line6.Width = 1.999999!
        Me.Line6.X1 = 2.26!
        Me.Line6.X2 = 4.259999!
        Me.Line6.Y1 = 5.615!
        Me.Line6.Y2 = 5.615!
        '
        'US_CC4
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S39_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_036, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label574, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label570, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label566, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label562, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label558, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label554, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label550, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label546, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label519, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label518, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S311_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label379, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label362, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label380, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label383, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label385, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label381, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label386, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label387, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label389, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label390, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label391, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label393, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label394, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label396, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label397, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label399, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label400, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label401, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label402, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label404, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label405, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label408, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label407, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label411, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label412, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label414, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label415, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label416, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label418, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label419, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label421, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label422, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label424, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label425, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label426, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label428, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label429, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label431, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label432, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label434, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label435, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label436, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label437, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label439, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label442, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label444, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label446, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label449, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label450, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label451, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label453, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label457, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label458, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label459, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label460, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label463, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label464, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label467, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label468, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label471, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label472, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label475, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label476, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label479, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label480, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label483, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label484, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label487, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label488, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label491, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label492, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label495, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label496, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label499, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S310_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label503, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label502, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S311_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label505, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S311_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S311_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S311_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label509, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label510, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label511, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label512, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S311_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label515, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label516, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label535, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label534, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label517, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label520, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label522, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label524, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label525, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label527, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label528, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label530, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label536, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label537, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label539, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label540, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label542, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label543, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label545, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label547, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label551, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label555, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label559, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label563, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label567, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label571, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label575, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label579, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_034, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label583, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_035, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label587, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label588, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label593, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label594, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label596, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label597, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S312_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label590, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label591, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S39_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label379 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label362 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label380 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label383 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label385 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label381 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label386 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label387 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label389 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label390 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label391 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label393 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label394 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label396 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label397 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label399 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label400 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label401 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label402 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label404 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label405 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label408 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label407 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label411 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label412 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label414 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label415 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label416 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label418 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label419 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label421 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label422 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label424 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label425 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label426 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label428 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label429 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label431 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label432 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label434 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label435 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label436 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label437 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label439 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label442 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label444 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label446 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label449 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label450 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label451 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label453 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label457 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label458 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line112 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line113 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label459 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label460 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label463 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label464 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label467 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line114 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line115 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line116 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label468 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label471 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label472 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label475 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label476 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label479 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label480 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label483 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label484 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label487 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label488 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label491 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label492 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label495 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label496 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label499 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line120 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line121 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line122 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line123 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line124 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line126 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line128 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line129 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S310_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S310_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line117 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line118 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line119 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label503 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label502 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S311_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label505 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S311_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S311_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S311_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label509 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label510 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line130 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line131 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label511 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label512 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S311_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S311_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label515 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line132 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line133 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line134 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line137 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label516 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line135 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S312_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label535 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label534 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label517 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label518 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label519 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label520 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label522 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label524 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label525 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label527 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label528 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label530 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line138 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line139 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line144 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label536 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label537 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label539 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label540 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label542 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label543 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label545 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line143 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label546 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label547 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label550 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label551 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label554 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label555 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label558 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label559 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label562 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label563 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label566 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label567 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label570 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label571 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label575 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label579 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label583 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_035 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label587 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label593 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label594 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label596 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label597 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S312_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label590 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label591 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line145 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line146 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line147 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line148 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line149 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line150 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line151 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line152 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line153 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line154 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line141 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line140 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line142 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label574 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S39_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape7 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S312_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents S312_036 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape3 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label588 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line155 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
End Class
