﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC3
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_SPC3))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S46_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label196 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture2 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label197 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label199 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label201 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label200 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label203 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label205 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label206 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label217 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label218 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label220 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label221 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label223 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label224 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label226 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label227 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label229 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label230 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label232 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label233 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label235 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label236 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label238 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label239 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label241 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label242 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label244 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label245 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label247 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label248 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label250 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label251 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label253 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label254 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label256 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label257 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label259 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label260 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label262 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label263 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label265 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label266 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label268 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label269 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label271 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label272 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label273 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label275 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label276 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label278 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label279 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label281 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label282 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label284 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label285 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S47_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S46_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape7 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S46_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label197, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label201, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label200, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label203, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label205, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label206, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label217, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label218, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label220, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label221, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label223, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label224, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label226, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label227, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label229, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label230, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label232, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label233, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label235, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label236, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label238, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label239, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label241, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label242, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label244, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label245, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label247, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label248, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label250, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label251, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label253, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label254, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label256, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label257, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label259, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label260, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label262, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label263, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label265, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label266, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label268, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label269, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label272, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label275, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label276, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label278, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label281, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label282, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label284, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label285, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S47_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S46_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label6, Me.S46_023, Me.S47_004, Me.S47_012, Me.Label196, Me.Picture2, Me.Label197, Me.S46_001, Me.Label199, Me.Label201, Me.S46_002, Me.Label200, Me.Label203, Me.S46_003, Me.Label205, Me.Label206, Me.S46_004, Me.Label217, Me.Label218, Me.S46_005, Me.Label220, Me.Label221, Me.S46_006, Me.Label223, Me.Label224, Me.S46_007, Me.Label226, Me.Label227, Me.S46_008, Me.Label229, Me.Label230, Me.S46_009, Me.Label232, Me.Label233, Me.S46_010, Me.Label235, Me.Label236, Me.S46_011, Me.Label238, Me.Label239, Me.S46_012, Me.Label241, Me.Label242, Me.S46_013, Me.Label244, Me.Label245, Me.S46_014, Me.Label247, Me.Label248, Me.S46_016, Me.Label250, Me.Label251, Me.S46_017, Me.Label253, Me.Label254, Me.S46_018, Me.Label256, Me.Label257, Me.S46_019, Me.Label259, Me.Label260, Me.S46_020, Me.Label262, Me.Label263, Me.S46_021, Me.Label265, Me.Label266, Me.S46_022, Me.Label268, Me.Label269, Me.Label271, Me.Label272, Me.Label273, Me.Label275, Me.Label276, Me.S47_009, Me.Label278, Me.Label279, Me.S47_010, Me.Label281, Me.Label282, Me.S47_011, Me.Label284, Me.Label285, Me.S47_003, Me.Label1, Me.Label2, Me.S47_008, Me.Label4, Me.Label5, Me.S47_007, Me.Label7, Me.Label8, Me.S47_006, Me.Label10, Me.Label11, Me.S47_005, Me.Label16, Me.Label17, Me.Label19, Me.Label20, Me.S47_002, Me.Label22, Me.S47_001, Me.Label3, Me.S46_015, Me.Shape6, Me.Shape1, Me.Label9, Me.Label12, Me.S46_024, Me.Label14, Me.Label15, Me.S46_025, Me.Shape7, Me.PageBreak1})
        Me.Detail.Height = 10.41667!
        Me.Detail.Name = "Detail"
        '
        'S46_023
        '
        Me.S46_023.Height = 0.1875!
        Me.S46_023.HyperLink = Nothing
        Me.S46_023.Left = 2.25!
        Me.S46_023.Name = "S46_023"
        Me.S46_023.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S46_023.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_023.Top = 7.582999!
        Me.S46_023.Width = 1.0!
        '
        'S47_004
        '
        Me.S47_004.Height = 0.1875!
        Me.S47_004.HyperLink = Nothing
        Me.S47_004.Left = 2.25!
        Me.S47_004.Name = "S47_004"
        Me.S47_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S47_004.Text = "0.438" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_004.Top = 8.585999!
        Me.S47_004.Width = 1.0!
        '
        'S47_012
        '
        Me.S47_012.Height = 0.1875!
        Me.S47_012.HyperLink = Nothing
        Me.S47_012.Left = 2.25!
        Me.S47_012.Name = "S47_012"
        Me.S47_012.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S47_012.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_012.Top = 10.148!
        Me.S47_012.Width = 1.0!
        '
        'Label196
        '
        Me.Label196.Height = 0.1875!
        Me.Label196.HyperLink = Nothing
        Me.Label196.Left = 0!
        Me.Label196.Name = "Label196"
        Me.Label196.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label196.Text = "4.6 SHEAR PLATE CHECK FOR AXIAL AND MOMENT:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label196.Top = 0!
        Me.Label196.Width = 8.0!
        '
        'Picture2
        '
        Me.Picture2.Height = 2.875!
        Me.Picture2.HyperLink = Nothing
        Me.Picture2.ImageData = CType(resources.GetObject("Picture2.ImageData"), System.IO.Stream)
        Me.Picture2.Left = 0!
        Me.Picture2.Name = "Picture2"
        Me.Picture2.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture2.Top = 0.187!
        Me.Picture2.Width = 7.75!
        '
        'Label197
        '
        Me.Label197.Height = 0.1875!
        Me.Label197.HyperLink = Nothing
        Me.Label197.Left = 0!
        Me.Label197.Name = "Label197"
        Me.Label197.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label197.Text = "Φb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label197.Top = 2.999!
        Me.Label197.Width = 2.25!
        '
        'S46_001
        '
        Me.S46_001.Height = 0.1875!
        Me.S46_001.HyperLink = Nothing
        Me.S46_001.Left = 2.25!
        Me.S46_001.Name = "S46_001"
        Me.S46_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S46_001.Text = "0.90" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_001.Top = 2.999!
        Me.S46_001.Width = 1.0!
        '
        'Label199
        '
        Me.Label199.Height = 0.1875!
        Me.Label199.HyperLink = Nothing
        Me.Label199.Left = 0!
        Me.Label199.Name = "Label199"
        Me.Label199.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label199.Text = "a=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label199.Top = 3.186002!
        Me.Label199.Width = 2.25!
        '
        'Label201
        '
        Me.Label201.Height = 0.1875!
        Me.Label201.HyperLink = Nothing
        Me.Label201.Left = 3.25!
        Me.Label201.Name = "Label201"
        Me.Label201.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label201.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label201.Top = 3.186002!
        Me.Label201.Width = 4.75!
        '
        'S46_002
        '
        Me.S46_002.Height = 0.1875!
        Me.S46_002.HyperLink = Nothing
        Me.S46_002.Left = 2.25!
        Me.S46_002.Name = "S46_002"
        Me.S46_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S46_002.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_002.Top = 3.187!
        Me.S46_002.Width = 1.0!
        '
        'Label200
        '
        Me.Label200.Height = 0.1875!
        Me.Label200.HyperLink = Nothing
        Me.Label200.Left = 0!
        Me.Label200.Name = "Label200"
        Me.Label200.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label200.Text = "Vuy=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label200.Top = 3.374!
        Me.Label200.Width = 2.25!
        '
        'Label203
        '
        Me.Label203.Height = 0.1875!
        Me.Label203.HyperLink = Nothing
        Me.Label203.Left = 3.25!
        Me.Label203.Name = "Label203"
        Me.Label203.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label203.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label203.Top = 3.374!
        Me.Label203.Width = 4.75!
        '
        'S46_003
        '
        Me.S46_003.Height = 0.1875!
        Me.S46_003.HyperLink = Nothing
        Me.S46_003.Left = 2.25!
        Me.S46_003.Name = "S46_003"
        Me.S46_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S46_003.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_003.Top = 3.375001!
        Me.S46_003.Width = 1.0!
        '
        'Label205
        '
        Me.Label205.Height = 0.1875!
        Me.Label205.HyperLink = Nothing
        Me.Label205.Left = 0!
        Me.Label205.Name = "Label205"
        Me.Label205.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label205.Text = "Mecc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label205.Top = 3.562!
        Me.Label205.Width = 2.25!
        '
        'Label206
        '
        Me.Label206.Height = 0.1875!
        Me.Label206.HyperLink = Nothing
        Me.Label206.Left = 3.25!
        Me.Label206.Name = "Label206"
        Me.Label206.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label206.Text = "kips*in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vuy* a" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label206.Top = 3.562!
        Me.Label206.Width = 4.75!
        '
        'S46_004
        '
        Me.S46_004.Height = 0.1875!
        Me.S46_004.HyperLink = Nothing
        Me.S46_004.Left = 2.25!
        Me.S46_004.Name = "S46_004"
        Me.S46_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_004.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_004.Top = 3.563!
        Me.S46_004.Width = 1.0!
        '
        'Label217
        '
        Me.Label217.Height = 0.1875!
        Me.Label217.HyperLink = Nothing
        Me.Label217.Left = 0!
        Me.Label217.Name = "Label217"
        Me.Label217.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label217.Text = "θWhitmore=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label217.Top = 3.751!
        Me.Label217.Width = 2.25!
        '
        'Label218
        '
        Me.Label218.Height = 0.1875!
        Me.Label218.HyperLink = Nothing
        Me.Label218.Left = 3.25!
        Me.Label218.Name = "Label218"
        Me.Label218.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label218.Text = "deg" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label218.Top = 3.751!
        Me.Label218.Width = 4.75!
        '
        'S46_005
        '
        Me.S46_005.Height = 0.1875!
        Me.S46_005.HyperLink = Nothing
        Me.S46_005.Left = 2.25!
        Me.S46_005.Name = "S46_005"
        Me.S46_005.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S46_005.Text = "30"
        Me.S46_005.Top = 3.752!
        Me.S46_005.Width = 1.0!
        '
        'Label220
        '
        Me.Label220.Height = 0.1875!
        Me.Label220.HyperLink = Nothing
        Me.Label220.Left = 0!
        Me.Label220.Name = "Label220"
        Me.Label220.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label220.Text = "Lwhitmore1 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label220.Top = 3.938!
        Me.Label220.Width = 2.25!
        '
        'Label221
        '
        Me.Label221.Height = 0.1875!
        Me.Label221.HyperLink = Nothing
        Me.Label221.Left = 3.25!
        Me.Label221.Name = "Label221"
        Me.Label221.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label221.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= tan (θWhitmore)* a* 2 + db_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label221.Top = 3.938!
        Me.Label221.Width = 4.75!
        '
        'S46_006
        '
        Me.S46_006.Height = 0.1875!
        Me.S46_006.HyperLink = Nothing
        Me.S46_006.Left = 2.25!
        Me.S46_006.Name = "S46_006"
        Me.S46_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_006.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_006.Top = 3.938998!
        Me.S46_006.Width = 1.0!
        '
        'Label223
        '
        Me.Label223.Height = 0.1875!
        Me.Label223.HyperLink = Nothing
        Me.Label223.Left = 0!
        Me.Label223.Name = "Label223"
        Me.Label223.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label223.Text = "Awhitmore1 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label223.Top = 4.127!
        Me.Label223.Width = 2.25!
        '
        'Label224
        '
        Me.Label224.Height = 0.1875!
        Me.Label224.HyperLink = Nothing
        Me.Label224.Left = 3.25!
        Me.Label224.Name = "Label224"
        Me.Label224.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label224.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lwhitmore1* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label224.Top = 4.127!
        Me.Label224.Width = 4.75!
        '
        'S46_007
        '
        Me.S46_007.Height = 0.1875!
        Me.S46_007.HyperLink = Nothing
        Me.S46_007.Left = 2.25!
        Me.S46_007.Name = "S46_007"
        Me.S46_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_007.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_007.Top = 4.128001!
        Me.S46_007.Width = 1.0!
        '
        'Label226
        '
        Me.Label226.Height = 0.1875!
        Me.Label226.HyperLink = Nothing
        Me.Label226.Left = 0!
        Me.Label226.Name = "Label226"
        Me.Label226.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label226.Text = "Lwhitmore2 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label226.Top = 4.316!
        Me.Label226.Width = 2.25!
        '
        'Label227
        '
        Me.Label227.Height = 0.1875!
        Me.Label227.HyperLink = Nothing
        Me.Label227.Left = 3.25!
        Me.Label227.Name = "Label227"
        Me.Label227.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label227.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= min(tan (θWhitmore)* (a+Shorz)*2 + db_sp, hsp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label227.Top = 4.316!
        Me.Label227.Width = 4.75!
        '
        'S46_008
        '
        Me.S46_008.Height = 0.1875!
        Me.S46_008.HyperLink = Nothing
        Me.S46_008.Left = 2.25!
        Me.S46_008.Name = "S46_008"
        Me.S46_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_008.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_008.Top = 4.316994!
        Me.S46_008.Width = 1.0!
        '
        'Label229
        '
        Me.Label229.Height = 0.1875!
        Me.Label229.HyperLink = Nothing
        Me.Label229.Left = 0!
        Me.Label229.Name = "Label229"
        Me.Label229.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label229.Text = "Awhitmore2 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label229.Top = 4.504!
        Me.Label229.Width = 2.25!
        '
        'Label230
        '
        Me.Label230.Height = 0.1875!
        Me.Label230.HyperLink = Nothing
        Me.Label230.Left = 3.25!
        Me.Label230.Name = "Label230"
        Me.Label230.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label230.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lwhitmore2* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label230.Top = 4.504!
        Me.Label230.Width = 4.75!
        '
        'S46_009
        '
        Me.S46_009.Height = 0.1875!
        Me.S46_009.HyperLink = Nothing
        Me.S46_009.Left = 2.25!
        Me.S46_009.Name = "S46_009"
        Me.S46_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_009.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_009.Top = 4.504995!
        Me.S46_009.Width = 1.0!
        '
        'Label232
        '
        Me.Label232.Height = 0.1875!
        Me.Label232.HyperLink = Nothing
        Me.Label232.Left = 0!
        Me.Label232.Name = "Label232"
        Me.Label232.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label232.Text = "Lwhitmore3 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label232.Top = 4.692001!
        Me.Label232.Width = 2.25!
        '
        'Label233
        '
        Me.Label233.Height = 0.1875!
        Me.Label233.HyperLink = Nothing
        Me.Label233.Left = 3.25!
        Me.Label233.Name = "Label233"
        Me.Label233.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label233.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= min(tan (θWhitmore)* (a+2*Shorz)*2 + db_sp, hsp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label233.Top = 4.692001!
        Me.Label233.Width = 4.75!
        '
        'S46_010
        '
        Me.S46_010.Height = 0.1875!
        Me.S46_010.HyperLink = Nothing
        Me.S46_010.Left = 2.25!
        Me.S46_010.Name = "S46_010"
        Me.S46_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_010.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_010.Top = 4.692984!
        Me.S46_010.Width = 1.0!
        '
        'Label235
        '
        Me.Label235.Height = 0.1875!
        Me.Label235.HyperLink = Nothing
        Me.Label235.Left = 0!
        Me.Label235.Name = "Label235"
        Me.Label235.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label235.Text = "Awhitmore3 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label235.Top = 4.88!
        Me.Label235.Width = 2.25!
        '
        'Label236
        '
        Me.Label236.Height = 0.1875!
        Me.Label236.HyperLink = Nothing
        Me.Label236.Left = 3.25!
        Me.Label236.Name = "Label236"
        Me.Label236.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label236.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lwhitmore3* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label236.Top = 4.88!
        Me.Label236.Width = 4.75!
        '
        'S46_011
        '
        Me.S46_011.Height = 0.1875!
        Me.S46_011.HyperLink = Nothing
        Me.S46_011.Left = 2.25!
        Me.S46_011.Name = "S46_011"
        Me.S46_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_011.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_011.Top = 4.880994!
        Me.S46_011.Width = 1.0!
        '
        'Label238
        '
        Me.Label238.Height = 0.1875!
        Me.Label238.HyperLink = Nothing
        Me.Label238.Left = 0.0000004768372!
        Me.Label238.Name = "Label238"
        Me.Label238.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label238.Text = "Ssp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label238.Top = 5.443!
        Me.Label238.Width = 2.25!
        '
        'Label239
        '
        Me.Label239.Height = 0.1875!
        Me.Label239.HyperLink = Nothing
        Me.Label239.Left = 3.25!
        Me.Label239.Name = "Label239"
        Me.Label239.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label239.Text = "in^3" & Global.Microsoft.VisualBasic.ChrW(9) & "= tsp* hsp^2/ 6" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label239.Top = 5.443!
        Me.Label239.Width = 4.75!
        '
        'S46_012
        '
        Me.S46_012.Height = 0.1875!
        Me.S46_012.HyperLink = Nothing
        Me.S46_012.Left = 2.25!
        Me.S46_012.Name = "S46_012"
        Me.S46_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_012.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_012.Top = 5.443985!
        Me.S46_012.Width = 1.0!
        '
        'Label241
        '
        Me.Label241.Height = 0.1875!
        Me.Label241.HyperLink = Nothing
        Me.Label241.Left = 0.0000004768372!
        Me.Label241.Name = "Label241"
        Me.Label241.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label241.Text = "Isp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label241.Top = 5.631001!
        Me.Label241.Width = 2.25!
        '
        'Label242
        '
        Me.Label242.Height = 0.1875!
        Me.Label242.HyperLink = Nothing
        Me.Label242.Left = 3.25!
        Me.Label242.Name = "Label242"
        Me.Label242.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label242.Text = "in^4" & Global.Microsoft.VisualBasic.ChrW(9) & "= tsp* hsp^3/ 12" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label242.Top = 5.631001!
        Me.Label242.Width = 4.75!
        '
        'S46_013
        '
        Me.S46_013.Height = 0.1875!
        Me.S46_013.HyperLink = Nothing
        Me.S46_013.Left = 2.25!
        Me.S46_013.Name = "S46_013"
        Me.S46_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_013.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_013.Top = 5.631994!
        Me.S46_013.Width = 1.0!
        '
        'Label244
        '
        Me.Label244.Height = 0.1875!
        Me.Label244.HyperLink = Nothing
        Me.Label244.Left = 0.0000004768372!
        Me.Label244.Name = "Label244"
        Me.Label244.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label244.Text = "fb1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label244.Top = 5.819!
        Me.Label244.Width = 2.25!
        '
        'Label245
        '
        Me.Label245.Height = 0.1875!
        Me.Label245.HyperLink = Nothing
        Me.Label245.Left = 3.25!
        Me.Label245.Name = "Label245"
        Me.Label245.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label245.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= Mecc/ Ssp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label245.Top = 5.819!
        Me.Label245.Width = 4.75!
        '
        'S46_014
        '
        Me.S46_014.Height = 0.1875!
        Me.S46_014.HyperLink = Nothing
        Me.S46_014.Left = 2.25!
        Me.S46_014.Name = "S46_014"
        Me.S46_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_014.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_014.Top = 5.819994!
        Me.S46_014.Width = 1.0!
        '
        'Label247
        '
        Me.Label247.Height = 0.1875!
        Me.Label247.HyperLink = Nothing
        Me.Label247.Left = 0.0000004768372!
        Me.Label247.Name = "Label247"
        Me.Label247.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label247.Text = "yb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label247.Top = 6.267!
        Me.Label247.Width = 2.25!
        '
        'Label248
        '
        Me.Label248.Height = 0.1875!
        Me.Label248.HyperLink = Nothing
        Me.Label248.Left = 3.25!
        Me.Label248.Name = "Label248"
        Me.Label248.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label248.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lwhitmore1/ 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label248.Top = 6.267!
        Me.Label248.Width = 4.75!
        '
        'S46_016
        '
        Me.S46_016.Height = 0.1875!
        Me.S46_016.HyperLink = Nothing
        Me.S46_016.Left = 2.25!
        Me.S46_016.Name = "S46_016"
        Me.S46_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_016.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_016.Top = 6.267988!
        Me.S46_016.Width = 1.0!
        '
        'Label250
        '
        Me.Label250.Height = 0.1875!
        Me.Label250.HyperLink = Nothing
        Me.Label250.Left = 0.0000004768372!
        Me.Label250.Name = "Label250"
        Me.Label250.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label250.Text = "fb2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label250.Top = 6.454999!
        Me.Label250.Width = 2.25!
        '
        'Label251
        '
        Me.Label251.Height = 0.1875!
        Me.Label251.HyperLink = Nothing
        Me.Label251.Left = 3.25!
        Me.Label251.Name = "Label251"
        Me.Label251.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label251.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= Mecc* yb/ Isp"
        Me.Label251.Top = 6.454999!
        Me.Label251.Width = 4.75!
        '
        'S46_017
        '
        Me.S46_017.Height = 0.1875!
        Me.S46_017.HyperLink = Nothing
        Me.S46_017.Left = 2.25!
        Me.S46_017.Name = "S46_017"
        Me.S46_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_017.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_017.Top = 6.455993!
        Me.S46_017.Width = 1.0!
        '
        'Label253
        '
        Me.Label253.Height = 0.1875!
        Me.Label253.HyperLink = Nothing
        Me.Label253.Left = 0.0000004768372!
        Me.Label253.Name = "Label253"
        Me.Label253.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label253.Text = "fa2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label253.Top = 6.643001!
        Me.Label253.Width = 2.25!
        '
        'Label254
        '
        Me.Label254.Height = 0.1875!
        Me.Label254.HyperLink = Nothing
        Me.Label254.Left = 3.25!
        Me.Label254.Name = "Label254"
        Me.Label254.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label254.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_sp/ Awhitmore"
        Me.Label254.Top = 6.643001!
        Me.Label254.Width = 4.75!
        '
        'S46_018
        '
        Me.S46_018.Height = 0.1875!
        Me.S46_018.HyperLink = Nothing
        Me.S46_018.Left = 2.25!
        Me.S46_018.Name = "S46_018"
        Me.S46_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_018.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_018.Top = 6.643989!
        Me.S46_018.Width = 1.0!
        '
        'Label256
        '
        Me.Label256.Height = 0.1875!
        Me.Label256.HyperLink = Nothing
        Me.Label256.Left = 0.0000004768372!
        Me.Label256.Name = "Label256"
        Me.Label256.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label256.Text = "ftot2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label256.Top = 6.830999!
        Me.Label256.Width = 2.25!
        '
        'Label257
        '
        Me.Label257.Height = 0.1875!
        Me.Label257.HyperLink = Nothing
        Me.Label257.Left = 3.25!
        Me.Label257.Name = "Label257"
        Me.Label257.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label257.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= fa2 + fb2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label257.Top = 6.830999!
        Me.Label257.Width = 4.75!
        '
        'S46_019
        '
        Me.S46_019.Height = 0.1875!
        Me.S46_019.HyperLink = Nothing
        Me.S46_019.Left = 2.25!
        Me.S46_019.Name = "S46_019"
        Me.S46_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_019.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_019.Top = 6.831987!
        Me.S46_019.Width = 1.0!
        '
        'Label259
        '
        Me.Label259.Height = 0.1875!
        Me.Label259.HyperLink = Nothing
        Me.Label259.Left = 0.0000004768372!
        Me.Label259.Name = "Label259"
        Me.Label259.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label259.Text = "fmax_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label259.Top = 7.018999!
        Me.Label259.Width = 2.25!
        '
        'Label260
        '
        Me.Label260.Height = 0.1875!
        Me.Label260.HyperLink = Nothing
        Me.Label260.Left = 3.25!
        Me.Label260.Name = "Label260"
        Me.Label260.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label260.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= max (fb1, ftot2)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label260.Top = 7.018999!
        Me.Label260.Width = 4.75!
        '
        'S46_020
        '
        Me.S46_020.Height = 0.1875!
        Me.S46_020.HyperLink = Nothing
        Me.S46_020.Left = 2.25!
        Me.S46_020.Name = "S46_020"
        Me.S46_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_020.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_020.Top = 7.019985!
        Me.S46_020.Width = 1.0!
        '
        'Label262
        '
        Me.Label262.Height = 0.1875!
        Me.Label262.HyperLink = Nothing
        Me.Label262.Left = 0.0000004768372!
        Me.Label262.Name = "Label262"
        Me.Label262.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label262.Text = "Φb*Fy_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label262.Top = 7.207001!
        Me.Label262.Width = 2.25!
        '
        'Label263
        '
        Me.Label263.Height = 0.1875!
        Me.Label263.HyperLink = Nothing
        Me.Label263.Left = 3.25!
        Me.Label263.Name = "Label263"
        Me.Label263.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label263.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φb* Fy_sp"
        Me.Label263.Top = 7.207001!
        Me.Label263.Width = 4.75!
        '
        'S46_021
        '
        Me.S46_021.Height = 0.1875!
        Me.S46_021.HyperLink = Nothing
        Me.S46_021.Left = 2.25!
        Me.S46_021.Name = "S46_021"
        Me.S46_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_021.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_021.Top = 7.207994!
        Me.S46_021.Width = 1.0!
        '
        'Label265
        '
        Me.Label265.Height = 0.1875!
        Me.Label265.HyperLink = Nothing
        Me.Label265.Left = 0.0000004768372!
        Me.Label265.Name = "Label265"
        Me.Label265.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label265.Text = "SUM_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label265.Top = 7.395!
        Me.Label265.Width = 2.25!
        '
        'Label266
        '
        Me.Label266.Height = 0.1875!
        Me.Label266.HyperLink = Nothing
        Me.Label266.Left = 3.25!
        Me.Label266.Name = "Label266"
        Me.Label266.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label266.Text = "    " & Global.Microsoft.VisualBasic.ChrW(9) & "= fmax_sp/ (Φb* Fy_shearTab), OK if SUM_sp <= SUM_Allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label266.Top = 7.395!
        Me.Label266.Width = 4.75!
        '
        'S46_022
        '
        Me.S46_022.Height = 0.1875!
        Me.S46_022.HyperLink = Nothing
        Me.S46_022.Left = 2.25!
        Me.S46_022.Name = "S46_022"
        Me.S46_022.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S46_022.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_022.Top = 7.395993!
        Me.S46_022.Width = 1.0!
        '
        'Label268
        '
        Me.Label268.Height = 0.1875!
        Me.Label268.HyperLink = Nothing
        Me.Label268.Left = 0.0000004768372!
        Me.Label268.Name = "Label268"
        Me.Label268.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label268.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label268.Top = 7.582999!
        Me.Label268.Width = 2.25!
        '
        'Label269
        '
        Me.Label269.Height = 0.1875!
        Me.Label269.HyperLink = Nothing
        Me.Label269.Left = 3.75!
        Me.Label269.Name = "Label269"
        Me.Label269.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label269.Text = "OK if SUM_sp <= SUM_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label269.Top = 7.582999!
        Me.Label269.Width = 4.25!
        '
        'Label271
        '
        Me.Label271.Height = 0.1875!
        Me.Label271.HyperLink = Nothing
        Me.Label271.Left = 0!
        Me.Label271.Name = "Label271"
        Me.Label271.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label271.Text = "4.7 SHEAR PLATE TO COLUMN FLANGE FILLET WELD:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label271.Top = 7.835001!
        Me.Label271.Width = 8.0!
        '
        'Label272
        '
        Me.Label272.Height = 0.1875!
        Me.Label272.HyperLink = Nothing
        Me.Label272.Left = 0.0000002384186!
        Me.Label272.Name = "Label272"
        Me.Label272.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label272.Text = "tw_sp_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label272.Top = 8.397001!
        Me.Label272.Width = 2.25!
        '
        'Label273
        '
        Me.Label273.Height = 0.1875!
        Me.Label273.HyperLink = Nothing
        Me.Label273.Left = 3.25!
        Me.Label273.Name = "Label273"
        Me.Label273.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label273.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 5/8*tsp (Fillet size required to develop plate capacity as per AISC Steel Ma" &
    "nual 14th edition)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label273.Top = 8.397001!
        Me.Label273.Width = 4.75!
        '
        'Label275
        '
        Me.Label275.Height = 0.1875!
        Me.Label275.HyperLink = Nothing
        Me.Label275.Left = 0.0000002384186!
        Me.Label275.Name = "Label275"
        Me.Label275.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label275.Text = "Awe=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label275.Top = 9.582991!
        Me.Label275.Width = 2.25!
        '
        'Label276
        '
        Me.Label276.Height = 0.1875!
        Me.Label276.HyperLink = Nothing
        Me.Label276.Left = 3.25!
        Me.Label276.Name = "Label276"
        Me.Label276.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label276.Text = "in2" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.707*(2*Lweld)*tw_sp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label276.Top = 9.582991!
        Me.Label276.Width = 4.75!
        '
        'S47_009
        '
        Me.S47_009.Height = 0.1875!
        Me.S47_009.HyperLink = Nothing
        Me.S47_009.Left = 2.25!
        Me.S47_009.Name = "S47_009"
        Me.S47_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S47_009.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_009.Top = 9.583981!
        Me.S47_009.Width = 1.0!
        '
        'Label278
        '
        Me.Label278.Height = 0.1875!
        Me.Label278.HyperLink = Nothing
        Me.Label278.Left = 0.0000002384186!
        Me.Label278.Name = "Label278"
        Me.Label278.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label278.Text = "ΦRn=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label278.Top = 9.772011!
        Me.Label278.Width = 2.25!
        '
        'Label279
        '
        Me.Label279.Height = 0.1875!
        Me.Label279.HyperLink = Nothing
        Me.Label279.Left = 3.25!
        Me.Label279.Name = "Label279"
        Me.Label279.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label279.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φ*Fnw*Awe" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label279.Top = 9.772011!
        Me.Label279.Width = 4.75!
        '
        'S47_010
        '
        Me.S47_010.Height = 0.1875!
        Me.S47_010.HyperLink = Nothing
        Me.S47_010.Left = 2.25!
        Me.S47_010.Name = "S47_010"
        Me.S47_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S47_010.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_010.Top = 9.772991!
        Me.S47_010.Width = 1.0!
        '
        'Label281
        '
        Me.Label281.Height = 0.1875!
        Me.Label281.HyperLink = Nothing
        Me.Label281.Left = 0.0000002384186!
        Me.Label281.Name = "Label281"
        Me.Label281.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label281.Text = "SUM_spWeld=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label281.Top = 9.960012!
        Me.Label281.Width = 2.25!
        '
        'Label282
        '
        Me.Label282.Height = 0.1875!
        Me.Label282.HyperLink = Nothing
        Me.Label282.Left = 3.25!
        Me.Label282.Name = "Label282"
        Me.Label282.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label282.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Vu_bm / ΦRn" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label282.Top = 9.960012!
        Me.Label282.Width = 4.75!
        '
        'S47_011
        '
        Me.S47_011.Height = 0.1875!
        Me.S47_011.HyperLink = Nothing
        Me.S47_011.Left = 2.25!
        Me.S47_011.Name = "S47_011"
        Me.S47_011.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S47_011.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_011.Top = 9.960991!
        Me.S47_011.Width = 1.0!
        '
        'Label284
        '
        Me.Label284.Height = 0.1875!
        Me.Label284.HyperLink = Nothing
        Me.Label284.Left = 0.0000002384186!
        Me.Label284.Name = "Label284"
        Me.Label284.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label284.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label284.Top = 10.148!
        Me.Label284.Width = 2.25!
        '
        'Label285
        '
        Me.Label285.Height = 0.1875!
        Me.Label285.HyperLink = Nothing
        Me.Label285.Left = 3.25!
        Me.Label285.Name = "Label285"
        Me.Label285.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label285.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= OK if SUM_spWeld <= SUM_spWeld_allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label285.Top = 10.148!
        Me.Label285.Width = 4.75!
        '
        'S47_003
        '
        Me.S47_003.Height = 0.1875!
        Me.S47_003.HyperLink = Nothing
        Me.S47_003.Left = 2.25!
        Me.S47_003.Name = "S47_003"
        Me.S47_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S47_003.Text = "0.391" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_003.Top = 8.397995!
        Me.S47_003.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.0000002384186!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label1.Text = "Fnw=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 9.397001!
        Me.Label1.Width = 2.25!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 3.25!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label2.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6*FEXX" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 9.397001!
        Me.Label2.Width = 4.75!
        '
        'S47_008
        '
        Me.S47_008.Height = 0.1875!
        Me.S47_008.HyperLink = Nothing
        Me.S47_008.Left = 2.25!
        Me.S47_008.Name = "S47_008"
        Me.S47_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S47_008.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_008.Top = 9.397991!
        Me.S47_008.Width = 1.0!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0.0000002384186!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "FEXX=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 9.147011!
        Me.Label4.Width = 2.25!
        '
        'Label5
        '
        Me.Label5.Height = 0.1875!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 3.25!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label5.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= Filler metal classification strength" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label5.Top = 9.147011!
        Me.Label5.Width = 4.75!
        '
        'S47_007
        '
        Me.S47_007.Height = 0.1875!
        Me.S47_007.HyperLink = Nothing
        Me.S47_007.Left = 2.25!
        Me.S47_007.Name = "S47_007"
        Me.S47_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S47_007.Text = "70.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_007.Top = 9.147991!
        Me.S47_007.Width = 1.0!
        '
        'Label7
        '
        Me.Label7.Height = 0.1875!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.0000002384186!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "Lweld=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label7.Top = 8.960001!
        Me.Label7.Width = 2.25!
        '
        'Label8
        '
        Me.Label8.Height = 0.1875!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 3.25!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label8.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Length per side assuming 1/4"" hold off top and bottom" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label8.Top = 8.960001!
        Me.Label8.Width = 4.75!
        '
        'S47_006
        '
        Me.S47_006.Height = 0.1875!
        Me.S47_006.HyperLink = Nothing
        Me.S47_006.Left = 2.25!
        Me.S47_006.Name = "S47_006"
        Me.S47_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S47_006.Text = "11.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_006.Top = 8.960991!
        Me.S47_006.Width = 1.0!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0.0000002384186!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label10.Text = "Vu_bm=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 8.772011!
        Me.Label10.Width = 2.25!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 3.25!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label11.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label11.Top = 8.772011!
        Me.Label11.Width = 4.75!
        '
        'S47_005
        '
        Me.S47_005.Height = 0.1875!
        Me.S47_005.HyperLink = Nothing
        Me.S47_005.Left = 2.25!
        Me.S47_005.Name = "S47_005"
        Me.S47_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S47_005.Text = "91.057" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_005.Top = 8.772991!
        Me.S47_005.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0.0000002384186!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "tw_sp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 8.585007!
        Me.Label16.Width = 2.25!
        '
        'Label17
        '
        Me.Label17.Height = 0.1875!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 3.25!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label17.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= CEILING( tw_sp_min, 1/16)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label17.Top = 8.585007!
        Me.Label17.Width = 4.75!
        '
        'Label19
        '
        Me.Label19.Height = 0.1875!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 0.0000002384186!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label19.Text = "tsp="
        Me.Label19.Top = 8.209007!
        Me.Label19.Width = 2.25!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 3.25!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label20.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined"
        Me.Label20.Top = 8.209007!
        Me.Label20.Width = 4.75!
        '
        'S47_002
        '
        Me.S47_002.Height = 0.1875!
        Me.S47_002.HyperLink = Nothing
        Me.S47_002.Left = 2.25!
        Me.S47_002.Name = "S47_002"
        Me.S47_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S47_002.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_002.Top = 8.209999!
        Me.S47_002.Width = 1.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.1875!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 0.0000002384186!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-family: Symbol; font-size: 8.25pt; text-align: right; vertical-align: middle" &
    "; ddo-char-set: 2"
        Me.Label22.Text = "f=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label22.Top = 8.022001!
        Me.Label22.Width = 2.25!
        '
        'S47_001
        '
        Me.S47_001.Height = 0.1875!
        Me.S47_001.HyperLink = Nothing
        Me.S47_001.Left = 2.25!
        Me.S47_001.Name = "S47_001"
        Me.S47_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S47_001.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S47_001.Top = 8.022003!
        Me.S47_001.Width = 1.0!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.0000004768372!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label3.Text = "Awhitmore="
        Me.Label3.Top = 6.007!
        Me.Label3.Width = 2.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.2909999!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.25!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label6.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= IF( n_Hbolts=1, Awhitmore1, IF(n_Hbolts=2, Awhitmore2," & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                  " &
    " IF(n_Hbolts=3, Awhitmore3, Awhitemore4)))"
        Me.Label6.Top = 6.008!
        Me.Label6.Width = 4.75!
        '
        'S46_015
        '
        Me.S46_015.Height = 0.1875!
        Me.S46_015.HyperLink = Nothing
        Me.S46_015.Left = 2.25!
        Me.S46_015.Name = "S46_015"
        Me.S46_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_015.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_015.Top = 6.007996!
        Me.S46_015.Width = 1.0!
        '
        'Shape6
        '
        Me.Shape6.Height = 0.1875!
        Me.Shape6.Left = 2.25!
        Me.Shape6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 10.148!
        Me.Shape6.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.25!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 8.584!
        Me.Shape1.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0.0000002384186!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Lwhitmore4 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 5.068!
        Me.Label9.Width = 2.25!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 3.25!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label12.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= min(tan (θWhitmore)* (a+3*Shorz)*2 + db_sp, hsp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label12.Top = 5.068!
        Me.Label12.Width = 4.75!
        '
        'S46_024
        '
        Me.S46_024.Height = 0.1875!
        Me.S46_024.HyperLink = Nothing
        Me.S46_024.Left = 2.25!
        Me.S46_024.Name = "S46_024"
        Me.S46_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_024.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_024.Top = 5.068985!
        Me.S46_024.Width = 1.0!
        '
        'Label14
        '
        Me.Label14.Height = 0.1875!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.0000002384186!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label14.Text = "Awhitmore4 =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label14.Top = 5.256001!
        Me.Label14.Width = 2.25!
        '
        'Label15
        '
        Me.Label15.Height = 0.1875!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 3.25!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label15.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lwhitmore4* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label15.Top = 5.256001!
        Me.Label15.Width = 4.75!
        '
        'S46_025
        '
        Me.S46_025.Height = 0.1875!
        Me.S46_025.HyperLink = Nothing
        Me.S46_025.Left = 2.25!
        Me.S46_025.Name = "S46_025"
        Me.S46_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S46_025.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S46_025.Top = 5.256995!
        Me.S46_025.Width = 1.0!
        '
        'Shape7
        '
        Me.Shape7.Height = 0.1875!
        Me.Shape7.Left = 2.25!
        Me.Shape7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape7.Name = "Shape7"
        Me.Shape7.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape7.Top = 7.582999!
        Me.Shape7.Width = 1.0!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.2!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(8.0!, 0.2!)
        Me.PageBreak1.Top = 7.835001!
        Me.PageBreak1.Width = 8.0!
        '
        'US_SPC3
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.0!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S46_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label196, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label197, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label199, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label201, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label200, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label203, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label205, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label206, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label217, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label218, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label220, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label221, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label223, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label224, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label226, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label227, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label229, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label230, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label232, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label233, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label235, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label236, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label238, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label239, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label241, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label242, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label244, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label245, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label247, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label248, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label250, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label251, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label253, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label254, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label256, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label257, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label259, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label260, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label262, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label263, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label265, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label266, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label268, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label269, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label271, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label272, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label275, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label276, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label278, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label281, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label282, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label284, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label285, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S47_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S46_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label196 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture2 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label197 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label199 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label201 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label200 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label203 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label205 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label206 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label217 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label218 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label220 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label221 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label223 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label224 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label226 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label227 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label229 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label230 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label232 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label233 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label235 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label236 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label238 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label239 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label241 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label242 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label244 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label245 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label247 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label248 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label250 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label251 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label253 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label254 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label256 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label257 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label259 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label260 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label262 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label263 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label265 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label266 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label268 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label269 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label271 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label272 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label273 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label275 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label276 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label278 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label279 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label281 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label282 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label284 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label285 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Shape7 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S47_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S46_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak

    '

End Class
