﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_CC3
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_CC3))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.Label257 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label258 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label269 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label273 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label274 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label275 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label279 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label282 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label283 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label286 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line86 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line87 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line88 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label276 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label287 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label288 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label291 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label292 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label295 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label296 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label299 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label300 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label303 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label304 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label307 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label308 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label311 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label312 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S36_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label315 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line82 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line83 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line84 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line89 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line90 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line91 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line92 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line93 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line94 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line95 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line96 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label378 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label377 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label316 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label317 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label320 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label322 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label323 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label325 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label326 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label328 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label319 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label330 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label331 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label341 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label342 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label344 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label345 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label347 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label348 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label350 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S37_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label354 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label355 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line98 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line99 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label356 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line100 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line101 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line103 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line104 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label358 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label359 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label360 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label363 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label365 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label366 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label368 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label369 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label371 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S38_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label375 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label376 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line105 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line106 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line107 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line108 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line109 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line110 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line111 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label357 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line102 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.Label257, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label258, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label269, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label274, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label275, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label282, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label283, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label286, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label276, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label287, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label288, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label291, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label292, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label295, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label296, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label299, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label300, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label303, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label304, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label307, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label308, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label311, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label312, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S36_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label315, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label378, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label377, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label316, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label317, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label320, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label322, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label323, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label325, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label326, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label328, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label319, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label330, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label331, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label341, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label342, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label344, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label345, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label347, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label348, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label350, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S37_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label354, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label355, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label356, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label358, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label359, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label360, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label363, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label365, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label366, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label368, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label369, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label371, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S38_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label375, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label376, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label357, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label257, Me.Label258, Me.S36_001, Me.Label269, Me.S36_002, Me.Label273, Me.Label274, Me.Label275, Me.Label279, Me.S36_003, Me.Label282, Me.Label283, Me.S36_004, Me.S36_013, Me.Label286, Me.Line86, Me.Line87, Me.Line88, Me.Label276, Me.S36_005, Me.S36_014, Me.Label287, Me.Label288, Me.S36_006, Me.S36_015, Me.Label291, Me.Label292, Me.S36_007, Me.S36_016, Me.Label295, Me.Label296, Me.S36_008, Me.S36_017, Me.Label299, Me.Label300, Me.S36_009, Me.S36_018, Me.Label303, Me.Label304, Me.S36_010, Me.S36_019, Me.Label307, Me.Label308, Me.S36_011, Me.S36_020, Me.Label311, Me.Label312, Me.S36_012, Me.S36_021, Me.Label315, Me.Line82, Me.Line83, Me.Line84, Me.Line89, Me.Line90, Me.Line91, Me.Line92, Me.Line93, Me.Line94, Me.Line95, Me.Line96, Me.Line1, Me.Label378, Me.Label377, Me.Label316, Me.Label317, Me.S37_001, Me.Label320, Me.S37_002, Me.Label322, Me.Label323, Me.S37_003, Me.Label325, Me.Label326, Me.S37_004, Me.Label328, Me.Label319, Me.S37_005, Me.Label330, Me.Label331, Me.S37_006, Me.Label341, Me.Label342, Me.S37_007, Me.Label344, Me.Label345, Me.S37_008, Me.Label347, Me.Label348, Me.S37_009, Me.Label350, Me.S37_010, Me.S37_011, Me.S37_012, Me.Label354, Me.Label355, Me.Line98, Me.Line99, Me.Label356, Me.Line100, Me.Line101, Me.Line103, Me.Line104, Me.Label358, Me.Label359, Me.Label360, Me.S38_001, Me.Label363, Me.S38_002, Me.Label365, Me.Label366, Me.S38_003, Me.Label368, Me.Label369, Me.S38_004, Me.Label371, Me.S38_005, Me.S38_006, Me.S38_007, Me.Label375, Me.Label376, Me.Line105, Me.Line106, Me.Line107, Me.Line108, Me.Line109, Me.Line110, Me.Line111, Me.Label357, Me.Line102, Me.Label1, Me.PageBreak1})
        Me.Detail.Height = 6.89!
        Me.Detail.Name = "Detail"
        '
        'Label257
        '
        Me.Label257.Height = 0.1875!
        Me.Label257.HyperLink = Nothing
        Me.Label257.Left = 0!
        Me.Label257.Name = "Label257"
        Me.Label257.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label257.Text = "3.6 COLUMN WEB LOCAL YIELDING (AISC 360 J10.2):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label257.Top = 0!
        Me.Label257.Width = 5.0!
        '
        'Label258
        '
        Me.Label258.Height = 0.188!
        Me.Label258.HyperLink = Nothing
        Me.Label258.Left = 0.001!
        Me.Label258.Name = "Label258"
        Me.Label258.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label258.Text = "ΦWLY=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label258.Top = 0.188!
        Me.Label258.Width = 2.25!
        '
        'S36_001
        '
        Me.S36_001.Height = 0.1875!
        Me.S36_001.HyperLink = Nothing
        Me.S36_001.Left = 2.25!
        Me.S36_001.Name = "S36_001"
        Me.S36_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S36_001.Text = "1"
        Me.S36_001.Top = 0.187!
        Me.S36_001.Width = 1.0!
        '
        'Label269
        '
        Me.Label269.Height = 0.1875!
        Me.Label269.HyperLink = Nothing
        Me.Label269.Left = 0.001000477!
        Me.Label269.Name = "Label269"
        Me.Label269.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label269.Text = "dc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label269.Top = 0.3770001!
        Me.Label269.Width = 2.25!
        '
        'S36_002
        '
        Me.S36_002.Height = 0.1875!
        Me.S36_002.HyperLink = Nothing
        Me.S36_002.Left = 2.25!
        Me.S36_002.Name = "S36_002"
        Me.S36_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S36_002.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_002.Top = 0.3760002!
        Me.S36_002.Width = 1.0!
        '
        'Label273
        '
        Me.Label273.Height = 0.1875!
        Me.Label273.HyperLink = Nothing
        Me.Label273.Left = 3.251!
        Me.Label273.Name = "Label273"
        Me.Label273.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label273.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label273.Top = 0.8750001!
        Me.Label273.Width = 1.0!
        '
        'Label274
        '
        Me.Label274.Height = 0.1875!
        Me.Label274.HyperLink = Nothing
        Me.Label274.Left = 2.25!
        Me.Label274.Name = "Label274"
        Me.Label274.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label274.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label274.Top = 0.8750001!
        Me.Label274.Width = 1.0!
        '
        'Label275
        '
        Me.Label275.Height = 0.1875!
        Me.Label275.HyperLink = Nothing
        Me.Label275.Left = 4.25!
        Me.Label275.Name = "Label275"
        Me.Label275.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label275.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "=J10.2 AISC 360-16"
        Me.Label275.Top = 0.19!
        Me.Label275.Width = 3.313!
        '
        'Label279
        '
        Me.Label279.Height = 0.1875!
        Me.Label279.HyperLink = Nothing
        Me.Label279.Left = 0.001!
        Me.Label279.Name = "Label279"
        Me.Label279.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label279.Text = "kdes=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label279.Top = 0.566!
        Me.Label279.Width = 2.25!
        '
        'S36_003
        '
        Me.S36_003.Height = 0.1875!
        Me.S36_003.HyperLink = Nothing
        Me.S36_003.Left = 2.250001!
        Me.S36_003.Name = "S36_003"
        Me.S36_003.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S36_003.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_003.Top = 0.565!
        Me.S36_003.Width = 1.0!
        '
        'Label282
        '
        Me.Label282.Height = 0.1875!
        Me.Label282.HyperLink = Nothing
        Me.Label282.Left = 4.25!
        Me.Label282.Name = "Label282"
        Me.Label282.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label282.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Looked up value" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label282.Top = 0.566!
        Me.Label282.Width = 3.313!
        '
        'Label283
        '
        Me.Label283.Height = 0.1875!
        Me.Label283.HyperLink = Nothing
        Me.Label283.Left = 0.001!
        Me.Label283.Name = "Label283"
        Me.Label283.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label283.Text = "t_flange=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label283.Top = 1.064!
        Me.Label283.Width = 2.25!
        '
        'S36_004
        '
        Me.S36_004.Height = 0.1875!
        Me.S36_004.HyperLink = Nothing
        Me.S36_004.Left = 2.250001!
        Me.S36_004.Name = "S36_004"
        Me.S36_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S36_004.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_004.Top = 1.063!
        Me.S36_004.Width = 1.0!
        '
        'S36_013
        '
        Me.S36_013.Height = 0.1875!
        Me.S36_013.HyperLink = Nothing
        Me.S36_013.Left = 3.25!
        Me.S36_013.Name = "S36_013"
        Me.S36_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S36_013.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_013.Top = 1.063!
        Me.S36_013.Width = 1.0!
        '
        'Label286
        '
        Me.Label286.Height = 0.1875!
        Me.Label286.HyperLink = Nothing
        Me.Label286.Left = 4.251001!
        Me.Label286.Name = "Label286"
        Me.Label286.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label286.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Link flange thickness" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label286.Top = 1.063!
        Me.Label286.Width = 3.394999!
        '
        'Line86
        '
        Me.Line86.Height = 0!
        Me.Line86.Left = 2.250001!
        Me.Line86.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line86.LineWeight = 1.0!
        Me.Line86.Name = "Line86"
        Me.Line86.Top = 1.063!
        Me.Line86.Width = 1.999999!
        Me.Line86.X1 = 2.250001!
        Me.Line86.X2 = 4.25!
        Me.Line86.Y1 = 1.063!
        Me.Line86.Y2 = 1.063!
        '
        'Line87
        '
        Me.Line87.Height = 0!
        Me.Line87.Left = 2.25!
        Me.Line87.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line87.LineWeight = 1.0!
        Me.Line87.Name = "Line87"
        Me.Line87.Top = 1.313!
        Me.Line87.Width = 2.0!
        Me.Line87.X1 = 2.25!
        Me.Line87.X2 = 4.25!
        Me.Line87.Y1 = 1.313!
        Me.Line87.Y2 = 1.313!
        '
        'Line88
        '
        Me.Line88.Height = 0!
        Me.Line88.Left = 2.25!
        Me.Line88.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line88.LineWeight = 1.0!
        Me.Line88.Name = "Line88"
        Me.Line88.Top = 0.876!
        Me.Line88.Width = 1.999999!
        Me.Line88.X1 = 2.25!
        Me.Line88.X2 = 4.249999!
        Me.Line88.Y1 = 0.876!
        Me.Line88.Y2 = 0.876!
        '
        'Label276
        '
        Me.Label276.Height = 0.1875!
        Me.Label276.HyperLink = Nothing
        Me.Label276.Left = 0.001!
        Me.Label276.Name = "Label276"
        Me.Label276.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label276.Text = "t_stem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label276.Top = 1.314!
        Me.Label276.Width = 2.25!
        '
        'S36_005
        '
        Me.S36_005.Height = 0.1875!
        Me.S36_005.HyperLink = Nothing
        Me.S36_005.Left = 2.25!
        Me.S36_005.Name = "S36_005"
        Me.S36_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S36_005.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_005.Top = 1.313!
        Me.S36_005.Width = 1.0!
        '
        'S36_014
        '
        Me.S36_014.Height = 0.1875!
        Me.S36_014.HyperLink = Nothing
        Me.S36_014.Left = 3.247999!
        Me.S36_014.Name = "S36_014"
        Me.S36_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S36_014.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_014.Top = 1.313!
        Me.S36_014.Width = 1.0!
        '
        'Label287
        '
        Me.Label287.Height = 0.1875!
        Me.Label287.HyperLink = Nothing
        Me.Label287.Left = 4.249!
        Me.Label287.Name = "Label287"
        Me.Label287.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label287.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Link stem thickness" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label287.Top = 1.313!
        Me.Label287.Width = 3.394999!
        '
        'Label288
        '
        Me.Label288.Height = 0.1875!
        Me.Label288.HyperLink = Nothing
        Me.Label288.Left = 0.001!
        Me.Label288.Name = "Label288"
        Me.Label288.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label288.Text = "l_b=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label288.Top = 1.564!
        Me.Label288.Width = 2.25!
        '
        'S36_006
        '
        Me.S36_006.Height = 0.1875!
        Me.S36_006.HyperLink = Nothing
        Me.S36_006.Left = 2.254!
        Me.S36_006.Name = "S36_006"
        Me.S36_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_006.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_006.Top = 1.563!
        Me.S36_006.Width = 1.0!
        '
        'S36_015
        '
        Me.S36_015.Height = 0.1875!
        Me.S36_015.HyperLink = Nothing
        Me.S36_015.Left = 3.246998!
        Me.S36_015.Name = "S36_015"
        Me.S36_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_015.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_015.Top = 1.563!
        Me.S36_015.Width = 1.0!
        '
        'Label291
        '
        Me.Label291.Height = 0.1875!
        Me.Label291.HyperLink = Nothing
        Me.Label291.Left = 4.247999!
        Me.Label291.Name = "Label291"
        Me.Label291.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label291.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2* t_flange + t_stem" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label291.Top = 1.563!
        Me.Label291.Width = 3.394999!
        '
        'Label292
        '
        Me.Label292.Height = 0.1875!
        Me.Label292.HyperLink = Nothing
        Me.Label292.Left = 0.005000008!
        Me.Label292.Name = "Label292"
        Me.Label292.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label292.Text = "Ct_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label292.Top = 1.751!
        Me.Label292.Width = 2.25!
        '
        'S36_007
        '
        Me.S36_007.Height = 0.1875!
        Me.S36_007.HyperLink = Nothing
        Me.S36_007.Left = 2.250001!
        Me.S36_007.Name = "S36_007"
        Me.S36_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_007.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_007.Top = 1.75!
        Me.S36_007.Width = 1.0!
        '
        'S36_016
        '
        Me.S36_016.Height = 0.1875!
        Me.S36_016.HyperLink = Nothing
        Me.S36_016.Left = 3.25!
        Me.S36_016.Name = "S36_016"
        Me.S36_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_016.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_016.Top = 1.75!
        Me.S36_016.Width = 1.0!
        '
        'Label295
        '
        Me.Label295.Height = 0.1875!
        Me.Label295.HyperLink = Nothing
        Me.Label295.Left = 4.251001!
        Me.Label295.Name = "Label295"
        Me.Label295.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label295.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_stiffTop <= dc, 0.5, 1)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label295.Top = 1.75!
        Me.Label295.Width = 3.394999!
        '
        'Label296
        '
        Me.Label296.Height = 0.1875!
        Me.Label296.HyperLink = Nothing
        Me.Label296.Left = 0.004999114!
        Me.Label296.Name = "Label296"
        Me.Label296.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label296.Text = "Ct_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label296.Top = 1.939!
        Me.Label296.Width = 2.25!
        '
        'S36_008
        '
        Me.S36_008.Height = 0.1875!
        Me.S36_008.HyperLink = Nothing
        Me.S36_008.Left = 2.25!
        Me.S36_008.Name = "S36_008"
        Me.S36_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_008.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_008.Top = 1.938!
        Me.S36_008.Width = 1.0!
        '
        'S36_017
        '
        Me.S36_017.Height = 0.1875!
        Me.S36_017.HyperLink = Nothing
        Me.S36_017.Left = 3.249999!
        Me.S36_017.Name = "S36_017"
        Me.S36_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_017.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_017.Top = 1.938!
        Me.S36_017.Width = 1.0!
        '
        'Label299
        '
        Me.Label299.Height = 0.1875!
        Me.Label299.HyperLink = Nothing
        Me.Label299.Left = 4.251!
        Me.Label299.Name = "Label299"
        Me.Label299.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label299.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_botTop <= dc, 0.5, 1)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label299.Top = 1.938!
        Me.Label299.Width = 3.394999!
        '
        'Label300
        '
        Me.Label300.Height = 0.1875!
        Me.Label300.HyperLink = Nothing
        Me.Label300.Left = 0.005999995!
        Me.Label300.Name = "Label300"
        Me.Label300.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label300.Text = "ΦRn_WLYmoreD=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label300.Top = 2.127!
        Me.Label300.Width = 2.25!
        '
        'S36_009
        '
        Me.S36_009.Height = 0.1875!
        Me.S36_009.HyperLink = Nothing
        Me.S36_009.Left = 2.251!
        Me.S36_009.Name = "S36_009"
        Me.S36_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_009.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_009.Top = 2.126!
        Me.S36_009.Width = 1.0!
        '
        'S36_018
        '
        Me.S36_018.Height = 0.1875!
        Me.S36_018.HyperLink = Nothing
        Me.S36_018.Left = 3.250999!
        Me.S36_018.Name = "S36_018"
        Me.S36_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_018.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_018.Top = 2.126!
        Me.S36_018.Width = 1.0!
        '
        'Label303
        '
        Me.Label303.Height = 0.1875!
        Me.Label303.HyperLink = Nothing
        Me.Label303.Left = 4.252!
        Me.Label303.Name = "Label303"
        Me.Label303.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label303.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= ΦWLY* Fy_col* tcw* (5* kdes + l_b)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label303.Top = 2.126!
        Me.Label303.Width = 3.394999!
        '
        'Label304
        '
        Me.Label304.Height = 0.1875!
        Me.Label304.HyperLink = Nothing
        Me.Label304.Left = 0.0000000129221!
        Me.Label304.Name = "Label304"
        Me.Label304.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label304.Text = "ΦRn_WLYlessD=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label304.Top = 2.315!
        Me.Label304.Width = 2.251!
        '
        'S36_010
        '
        Me.S36_010.Height = 0.1875!
        Me.S36_010.HyperLink = Nothing
        Me.S36_010.Left = 2.252!
        Me.S36_010.Name = "S36_010"
        Me.S36_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_010.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_010.Top = 2.314!
        Me.S36_010.Width = 1.0!
        '
        'S36_019
        '
        Me.S36_019.Height = 0.1875!
        Me.S36_019.HyperLink = Nothing
        Me.S36_019.Left = 3.251999!
        Me.S36_019.Name = "S36_019"
        Me.S36_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_019.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_019.Top = 2.314!
        Me.S36_019.Width = 1.0!
        '
        'Label307
        '
        Me.Label307.Height = 0.1875!
        Me.Label307.HyperLink = Nothing
        Me.Label307.Left = 4.253!
        Me.Label307.Name = "Label307"
        Me.Label307.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label307.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= ΦWLY* Fy_col* tcw* (2.5* kdes + l_b)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label307.Top = 2.314!
        Me.Label307.Width = 3.394999!
        '
        'Label308
        '
        Me.Label308.Height = 0.1875!
        Me.Label308.HyperLink = Nothing
        Me.Label308.Left = 0.001!
        Me.Label308.Name = "Label308"
        Me.Label308.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label308.Text = "ΦRn_WLY_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label308.Top = 2.502!
        Me.Label308.Width = 2.25!
        '
        'S36_011
        '
        Me.S36_011.Height = 0.1875!
        Me.S36_011.HyperLink = Nothing
        Me.S36_011.Left = 2.253!
        Me.S36_011.Name = "S36_011"
        Me.S36_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_011.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_011.Top = 2.501!
        Me.S36_011.Width = 1.0!
        '
        'S36_020
        '
        Me.S36_020.Height = 0.1875!
        Me.S36_020.HyperLink = Nothing
        Me.S36_020.Left = 3.252999!
        Me.S36_020.Name = "S36_020"
        Me.S36_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_020.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_020.Top = 2.501!
        Me.S36_020.Width = 1.0!
        '
        'Label311
        '
        Me.Label311.Height = 0.1875!
        Me.Label311.HyperLink = Nothing
        Me.Label311.Left = 4.254!
        Me.Label311.Name = "Label311"
        Me.Label311.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label311.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_stiffTop > d, ΦRn_WLYmoreD,ΦRn_WLYlessD)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label311.Top = 2.501!
        Me.Label311.Width = 3.394999!
        '
        'Label312
        '
        Me.Label312.Height = 0.1875!
        Me.Label312.HyperLink = Nothing
        Me.Label312.Left = 0.001!
        Me.Label312.Name = "Label312"
        Me.Label312.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label312.Text = "ΦRn_WLY_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label312.Top = 2.689!
        Me.Label312.Width = 2.25!
        '
        'S36_012
        '
        Me.S36_012.Height = 0.1875!
        Me.S36_012.HyperLink = Nothing
        Me.S36_012.Left = 2.25!
        Me.S36_012.Name = "S36_012"
        Me.S36_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_012.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_012.Top = 2.688!
        Me.S36_012.Width = 1.0!
        '
        'S36_021
        '
        Me.S36_021.Height = 0.1875!
        Me.S36_021.HyperLink = Nothing
        Me.S36_021.Left = 3.246!
        Me.S36_021.Name = "S36_021"
        Me.S36_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S36_021.Text = "50.00" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S36_021.Top = 2.688!
        Me.S36_021.Width = 1.0!
        '
        'Label315
        '
        Me.Label315.Height = 0.1875!
        Me.Label315.HyperLink = Nothing
        Me.Label315.Left = 4.247!
        Me.Label315.Name = "Label315"
        Me.Label315.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label315.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_stiffBot > d, ΦRn_WLYmoreD, ΦRn_WLYlessD)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label315.Top = 2.688!
        Me.Label315.Width = 3.394999!
        '
        'Line82
        '
        Me.Line82.Height = 2.001!
        Me.Line82.Left = 2.25!
        Me.Line82.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line82.LineWeight = 1.0!
        Me.Line82.Name = "Line82"
        Me.Line82.Top = 0.875!
        Me.Line82.Width = 0!
        Me.Line82.X1 = 2.25!
        Me.Line82.X2 = 2.25!
        Me.Line82.Y1 = 0.875!
        Me.Line82.Y2 = 2.876!
        '
        'Line83
        '
        Me.Line83.Height = 2.001!
        Me.Line83.Left = 3.25!
        Me.Line83.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line83.LineWeight = 1.0!
        Me.Line83.Name = "Line83"
        Me.Line83.Top = 0.875!
        Me.Line83.Width = 0!
        Me.Line83.X1 = 3.25!
        Me.Line83.X2 = 3.25!
        Me.Line83.Y1 = 0.875!
        Me.Line83.Y2 = 2.876!
        '
        'Line84
        '
        Me.Line84.Height = 2.001!
        Me.Line84.Left = 4.25!
        Me.Line84.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line84.LineWeight = 1.0!
        Me.Line84.Name = "Line84"
        Me.Line84.Top = 0.875!
        Me.Line84.Width = 0!
        Me.Line84.X1 = 4.25!
        Me.Line84.X2 = 4.25!
        Me.Line84.Y1 = 0.875!
        Me.Line84.Y2 = 2.876!
        '
        'Line89
        '
        Me.Line89.Height = 0!
        Me.Line89.Left = 2.25!
        Me.Line89.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line89.LineWeight = 1.0!
        Me.Line89.Name = "Line89"
        Me.Line89.Top = 1.313!
        Me.Line89.Width = 2.0!
        Me.Line89.X1 = 2.25!
        Me.Line89.X2 = 4.25!
        Me.Line89.Y1 = 1.313!
        Me.Line89.Y2 = 1.313!
        '
        'Line90
        '
        Me.Line90.Height = 0!
        Me.Line90.Left = 2.25!
        Me.Line90.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line90.LineWeight = 1.0!
        Me.Line90.Name = "Line90"
        Me.Line90.Top = 1.563!
        Me.Line90.Width = 2.0!
        Me.Line90.X1 = 2.25!
        Me.Line90.X2 = 4.25!
        Me.Line90.Y1 = 1.563!
        Me.Line90.Y2 = 1.563!
        '
        'Line91
        '
        Me.Line91.Height = 0!
        Me.Line91.Left = 2.25!
        Me.Line91.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line91.LineWeight = 1.0!
        Me.Line91.Name = "Line91"
        Me.Line91.Top = 1.75!
        Me.Line91.Width = 2.0!
        Me.Line91.X1 = 2.25!
        Me.Line91.X2 = 4.25!
        Me.Line91.Y1 = 1.75!
        Me.Line91.Y2 = 1.75!
        '
        'Line92
        '
        Me.Line92.Height = 0!
        Me.Line92.Left = 2.25!
        Me.Line92.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line92.LineWeight = 1.0!
        Me.Line92.Name = "Line92"
        Me.Line92.Top = 1.938!
        Me.Line92.Width = 2.0!
        Me.Line92.X1 = 2.25!
        Me.Line92.X2 = 4.25!
        Me.Line92.Y1 = 1.938!
        Me.Line92.Y2 = 1.938!
        '
        'Line93
        '
        Me.Line93.Height = 0!
        Me.Line93.Left = 2.251!
        Me.Line93.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line93.LineWeight = 1.0!
        Me.Line93.Name = "Line93"
        Me.Line93.Top = 2.126!
        Me.Line93.Width = 2.000001!
        Me.Line93.X1 = 2.251!
        Me.Line93.X2 = 4.251001!
        Me.Line93.Y1 = 2.126!
        Me.Line93.Y2 = 2.126!
        '
        'Line94
        '
        Me.Line94.Height = 0!
        Me.Line94.Left = 2.25!
        Me.Line94.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line94.LineWeight = 1.0!
        Me.Line94.Name = "Line94"
        Me.Line94.Top = 2.314!
        Me.Line94.Width = 2.0!
        Me.Line94.X1 = 2.25!
        Me.Line94.X2 = 4.25!
        Me.Line94.Y1 = 2.314!
        Me.Line94.Y2 = 2.314!
        '
        'Line95
        '
        Me.Line95.Height = 0!
        Me.Line95.Left = 2.254!
        Me.Line95.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line95.LineWeight = 1.0!
        Me.Line95.Name = "Line95"
        Me.Line95.Top = 2.501!
        Me.Line95.Width = 2.0!
        Me.Line95.X1 = 2.254!
        Me.Line95.X2 = 4.254!
        Me.Line95.Y1 = 2.501!
        Me.Line95.Y2 = 2.501!
        '
        'Line96
        '
        Me.Line96.Height = 0!
        Me.Line96.Left = 2.254!
        Me.Line96.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line96.LineWeight = 1.0!
        Me.Line96.Name = "Line96"
        Me.Line96.Top = 2.688!
        Me.Line96.Width = 2.0!
        Me.Line96.X1 = 2.254!
        Me.Line96.X2 = 4.254!
        Me.Line96.Y1 = 2.688!
        Me.Line96.Y2 = 2.688!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 2.25!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 2.876!
        Me.Line1.Width = 2.0!
        Me.Line1.X1 = 2.25!
        Me.Line1.X2 = 4.25!
        Me.Line1.Y1 = 2.876!
        Me.Line1.Y2 = 2.876!
        '
        'Label378
        '
        Me.Label378.Height = 0.1875!
        Me.Label378.HyperLink = Nothing
        Me.Label378.Left = 4.321!
        Me.Label378.Name = "Label378"
        Me.Label378.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label378.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_stiffTop < d, Φ*Rn_WCB*0.5, ΦRn_WCB )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label378.Top = 6.499!
        Me.Label378.Width = 3.689999!
        '
        'Label377
        '
        Me.Label377.Height = 0.1875!
        Me.Label377.HyperLink = Nothing
        Me.Label377.Left = 4.321!
        Me.Label377.Name = "Label377"
        Me.Label377.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label377.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_botTop < d, ΦRn_WCB*0.5, ΦRn_WCB )" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label377.Top = 6.687001!
        Me.Label377.Width = 3.689999!
        '
        'Label316
        '
        Me.Label316.Height = 0.1875!
        Me.Label316.HyperLink = Nothing
        Me.Label316.Left = 0.01699974!
        Me.Label316.Name = "Label316"
        Me.Label316.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label316.Text = "3.7 COLUMN WEB LOCAL CRIPPLING (AISC 360 J10.3):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label316.Top = 3.0!
        Me.Label316.Width = 5.0!
        '
        'Label317
        '
        Me.Label317.Height = 0.188!
        Me.Label317.HyperLink = Nothing
        Me.Label317.Left = 0.001!
        Me.Label317.Name = "Label317"
        Me.Label317.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label317.Text = "ΦWLC=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label317.Top = 3.187!
        Me.Label317.Width = 2.25!
        '
        'S37_001
        '
        Me.S37_001.Height = 0.1875!
        Me.S37_001.HyperLink = Nothing
        Me.S37_001.Left = 2.255!
        Me.S37_001.Name = "S37_001"
        Me.S37_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S37_001.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_001.Top = 3.188!
        Me.S37_001.Width = 1.0!
        '
        'Label320
        '
        Me.Label320.Height = 0.1875!
        Me.Label320.HyperLink = Nothing
        Me.Label320.Left = 0.001!
        Me.Label320.Name = "Label320"
        Me.Label320.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label320.Text = "Es=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label320.Top = 3.375!
        Me.Label320.Width = 2.25!
        '
        'S37_002
        '
        Me.S37_002.Height = 0.1875!
        Me.S37_002.HyperLink = Nothing
        Me.S37_002.Left = 2.255!
        Me.S37_002.Name = "S37_002"
        Me.S37_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S37_002.Text = "29000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_002.Top = 3.376!
        Me.S37_002.Width = 1.0!
        '
        'Label322
        '
        Me.Label322.Height = 0.1875!
        Me.Label322.HyperLink = Nothing
        Me.Label322.Left = 3.25!
        Me.Label322.Name = "Label322"
        Me.Label322.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label322.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label322.Top = 3.376!
        Me.Label322.Width = 3.318!
        '
        'Label323
        '
        Me.Label323.Height = 0.1875!
        Me.Label323.HyperLink = Nothing
        Me.Label323.Left = 0.001!
        Me.Label323.Name = "Label323"
        Me.Label323.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label323.Text = "tcf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label323.Top = 3.562!
        Me.Label323.Width = 2.25!
        '
        'S37_003
        '
        Me.S37_003.Height = 0.1875!
        Me.S37_003.HyperLink = Nothing
        Me.S37_003.Left = 2.255!
        Me.S37_003.Name = "S37_003"
        Me.S37_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S37_003.Text = "1.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_003.Top = 3.563!
        Me.S37_003.Width = 1.0!
        '
        'Label325
        '
        Me.Label325.Height = 0.1875!
        Me.Label325.HyperLink = Nothing
        Me.Label325.Left = 3.25!
        Me.Label325.Name = "Label325"
        Me.Label325.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label325.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label325.Top = 3.563!
        Me.Label325.Width = 3.318!
        '
        'Label326
        '
        Me.Label326.Height = 0.1875!
        Me.Label326.HyperLink = Nothing
        Me.Label326.Left = 0.001!
        Me.Label326.Name = "Label326"
        Me.Label326.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label326.Text = "tcw=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label326.Top = 3.75!
        Me.Label326.Width = 2.245!
        '
        'S37_004
        '
        Me.S37_004.Height = 0.1875!
        Me.S37_004.HyperLink = Nothing
        Me.S37_004.Left = 2.254999!
        Me.S37_004.Name = "S37_004"
        Me.S37_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S37_004.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_004.Top = 3.751!
        Me.S37_004.Width = 1.0!
        '
        'Label328
        '
        Me.Label328.Height = 0.1875!
        Me.Label328.HyperLink = Nothing
        Me.Label328.Left = 3.250001!
        Me.Label328.Name = "Label328"
        Me.Label328.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label328.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label328.Top = 3.751!
        Me.Label328.Width = 3.318!
        '
        'Label319
        '
        Me.Label319.Height = 0.1875!
        Me.Label319.HyperLink = Nothing
        Me.Label319.Left = 0!
        Me.Label319.Name = "Label319"
        Me.Label319.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label319.Text = "ΦRn_WLC_more0.5D=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label319.Top = 3.937!
        Me.Label319.Width = 2.251!
        '
        'S37_005
        '
        Me.S37_005.Height = 0.1875!
        Me.S37_005.HyperLink = Nothing
        Me.S37_005.Left = 2.251!
        Me.S37_005.Name = "S37_005"
        Me.S37_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_005.Text = "899.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_005.Top = 3.938!
        Me.S37_005.Width = 1.0!
        '
        'Label330
        '
        Me.Label330.Height = 0.1875!
        Me.Label330.HyperLink = Nothing
        Me.Label330.Left = 3.25!
        Me.Label330.Name = "Label330"
        Me.Label330.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label330.Text = "kip" & Global.Microsoft.VisualBasic.ChrW(9) & "= ΦWLC* 0.8* tcw^2* (1 + 3*(l_b/dc)*(tcw/tcf)^1.5)* Sqrt(Es* Fy_col* tcf/ tcw" &
    ")" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label330.Top = 3.938!
        Me.Label330.Width = 4.75!
        '
        'Label331
        '
        Me.Label331.Height = 0.1875!
        Me.Label331.HyperLink = Nothing
        Me.Label331.Left = 0!
        Me.Label331.Name = "Label331"
        Me.Label331.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label331.Text = "AISC J10-5a=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label331.Top = 4.125!
        Me.Label331.Width = 2.251!
        '
        'S37_006
        '
        Me.S37_006.Height = 0.1875!
        Me.S37_006.HyperLink = Nothing
        Me.S37_006.Left = 2.255!
        Me.S37_006.Name = "S37_006"
        Me.S37_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_006.Text = "449.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_006.Top = 4.126!
        Me.S37_006.Width = 1.0!
        '
        'Label341
        '
        Me.Label341.Height = 0.1875!
        Me.Label341.HyperLink = Nothing
        Me.Label341.Left = 3.250001!
        Me.Label341.Name = "Label341"
        Me.Label341.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label341.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.5* fRn_WLC_more0.5D" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label341.Top = 4.126!
        Me.Label341.Width = 4.750001!
        '
        'Label342
        '
        Me.Label342.Height = 0.1875!
        Me.Label342.HyperLink = Nothing
        Me.Label342.Left = 0!
        Me.Label342.Name = "Label342"
        Me.Label342.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label342.Text = "AISC J10-5b=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label342.Top = 4.313001!
        Me.Label342.Width = 2.251!
        '
        'S37_007
        '
        Me.S37_007.Height = 0.1875!
        Me.S37_007.HyperLink = Nothing
        Me.S37_007.Left = 2.254999!
        Me.S37_007.Name = "S37_007"
        Me.S37_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_007.Text = "413.0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_007.Top = 4.314001!
        Me.S37_007.Width = 1.0!
        '
        'Label344
        '
        Me.Label344.Height = 0.1875!
        Me.Label344.HyperLink = Nothing
        Me.Label344.Left = 3.25!
        Me.Label344.Name = "Label344"
        Me.Label344.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label344.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= fWLC* 0.4* tcw^2* (1 + 3*(4* l_b/dc - 0.2)*(tcw/tcf)^1.5)* Sqrt(Es* Fy_col" &
    "* tcf/ tcw)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label344.Top = 4.314001!
        Me.Label344.Width = 4.750001!
        '
        'Label345
        '
        Me.Label345.Height = 0.1875!
        Me.Label345.HyperLink = Nothing
        Me.Label345.Left = 0!
        Me.Label345.Name = "Label345"
        Me.Label345.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label345.Text = "ΦRn_WLC_less0.5D=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label345.Top = 4.500001!
        Me.Label345.Width = 2.255!
        '
        'S37_008
        '
        Me.S37_008.Height = 0.1875!
        Me.S37_008.HyperLink = Nothing
        Me.S37_008.Left = 2.254999!
        Me.S37_008.Name = "S37_008"
        Me.S37_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_008.Text = "449.50" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_008.Top = 4.501001!
        Me.S37_008.Width = 1.0!
        '
        'Label347
        '
        Me.Label347.Height = 0.1875!
        Me.Label347.HyperLink = Nothing
        Me.Label347.Left = 3.263!
        Me.Label347.Name = "Label347"
        Me.Label347.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label347.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (l_b/ dc <= 0.2, AISC J10-5a, AISC J10-5b)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label347.Top = 4.501!
        Me.Label347.Width = 4.751001!
        '
        'Label348
        '
        Me.Label348.Height = 0.1875!
        Me.Label348.HyperLink = Nothing
        Me.Label348.Left = 0!
        Me.Label348.Name = "Label348"
        Me.Label348.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label348.Text = "ΦRn_WLC_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label348.Top = 4.875!
        Me.Label348.Width = 2.246!
        '
        'S37_009
        '
        Me.S37_009.Height = 0.1875!
        Me.S37_009.HyperLink = Nothing
        Me.S37_009.Left = 2.259!
        Me.S37_009.Name = "S37_009"
        Me.S37_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_009.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_009.Top = 4.877!
        Me.S37_009.Width = 1.0!
        '
        'Label350
        '
        Me.Label350.Height = 0.1875!
        Me.Label350.HyperLink = Nothing
        Me.Label350.Left = 0!
        Me.Label350.Name = "Label350"
        Me.Label350.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label350.Text = "ΦRn_WLC_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label350.Top = 5.062001!
        Me.Label350.Width = 2.246!
        '
        'S37_010
        '
        Me.S37_010.Height = 0.1875!
        Me.S37_010.HyperLink = Nothing
        Me.S37_010.Left = 2.259!
        Me.S37_010.Name = "S37_010"
        Me.S37_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_010.Top = 5.064!
        Me.S37_010.Width = 1.0!
        '
        'S37_011
        '
        Me.S37_011.Height = 0.1875!
        Me.S37_011.HyperLink = Nothing
        Me.S37_011.Left = 3.262998!
        Me.S37_011.Name = "S37_011"
        Me.S37_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_011.Top = 4.877!
        Me.S37_011.Width = 1.0!
        '
        'S37_012
        '
        Me.S37_012.Height = 0.1875!
        Me.S37_012.HyperLink = Nothing
        Me.S37_012.Left = 3.262998!
        Me.S37_012.Name = "S37_012"
        Me.S37_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S37_012.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S37_012.Top = 5.064001!
        Me.S37_012.Width = 1.0!
        '
        'Label354
        '
        Me.Label354.Height = 0.1875!
        Me.Label354.HyperLink = Nothing
        Me.Label354.Left = 3.262999!
        Me.Label354.Name = "Label354"
        Me.Label354.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label354.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label354.Top = 4.689001!
        Me.Label354.Width = 1.0!
        '
        'Label355
        '
        Me.Label355.Height = 0.1875!
        Me.Label355.HyperLink = Nothing
        Me.Label355.Left = 2.259!
        Me.Label355.Name = "Label355"
        Me.Label355.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label355.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label355.Top = 4.689!
        Me.Label355.Width = 1.0!
        '
        'Line98
        '
        Me.Line98.Height = 0!
        Me.Line98.Left = 2.262999!
        Me.Line98.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line98.LineWeight = 1.0!
        Me.Line98.Name = "Line98"
        Me.Line98.Top = 5.064001!
        Me.Line98.Width = 2.000001!
        Me.Line98.X1 = 2.262999!
        Me.Line98.X2 = 4.263!
        Me.Line98.Y1 = 5.064001!
        Me.Line98.Y2 = 5.064001!
        '
        'Line99
        '
        Me.Line99.Height = 0!
        Me.Line99.Left = 2.262999!
        Me.Line99.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line99.LineWeight = 1.0!
        Me.Line99.Name = "Line99"
        Me.Line99.Top = 4.877!
        Me.Line99.Width = 2.000001!
        Me.Line99.X1 = 2.262999!
        Me.Line99.X2 = 4.263!
        Me.Line99.Y1 = 4.877!
        Me.Line99.Y2 = 4.877!
        '
        'Label356
        '
        Me.Label356.Height = 0.188!
        Me.Label356.HyperLink = Nothing
        Me.Label356.Left = 4.263!
        Me.Label356.Name = "Label356"
        Me.Label356.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label356.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_stiffBot > d, ΦRn_WLC_more0.5D, ΦRn_WLC_less0.5D)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label356.Top = 5.062!
        Me.Label356.Width = 3.75!
        '
        'Line100
        '
        Me.Line100.Height = 0.5629988!
        Me.Line100.Left = 2.255!
        Me.Line100.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line100.LineWeight = 1.0!
        Me.Line100.Name = "Line100"
        Me.Line100.Top = 4.689!
        Me.Line100.Width = 0!
        Me.Line100.X1 = 2.255!
        Me.Line100.X2 = 2.255!
        Me.Line100.Y1 = 4.689!
        Me.Line100.Y2 = 5.251999!
        '
        'Line101
        '
        Me.Line101.Height = 0.5629988!
        Me.Line101.Left = 3.262999!
        Me.Line101.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line101.LineWeight = 1.0!
        Me.Line101.Name = "Line101"
        Me.Line101.Top = 4.689001!
        Me.Line101.Width = 0!
        Me.Line101.X1 = 3.262999!
        Me.Line101.X2 = 3.262999!
        Me.Line101.Y1 = 4.689001!
        Me.Line101.Y2 = 5.252!
        '
        'Line103
        '
        Me.Line103.Height = 0!
        Me.Line103.Left = 2.258999!
        Me.Line103.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line103.LineWeight = 1.0!
        Me.Line103.Name = "Line103"
        Me.Line103.Top = 4.689001!
        Me.Line103.Width = 2.0!
        Me.Line103.X1 = 2.258999!
        Me.Line103.X2 = 4.258999!
        Me.Line103.Y1 = 4.689001!
        Me.Line103.Y2 = 4.689001!
        '
        'Line104
        '
        Me.Line104.Height = 0!
        Me.Line104.Left = 2.255!
        Me.Line104.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line104.LineWeight = 1.0!
        Me.Line104.Name = "Line104"
        Me.Line104.Top = 5.252!
        Me.Line104.Width = 2.0!
        Me.Line104.X1 = 2.255!
        Me.Line104.X2 = 4.255!
        Me.Line104.Y1 = 5.252!
        Me.Line104.Y2 = 5.252!
        '
        'Label358
        '
        Me.Label358.Height = 0.1875!
        Me.Label358.HyperLink = Nothing
        Me.Label358.Left = 0.017!
        Me.Label358.Name = "Label358"
        Me.Label358.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label358.Text = "3.8 COLUMN WEB COMPRESSION BUCKLING (AISC 360 J10.5):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label358.Top = 5.375!
        Me.Label358.Width = 4.983!
        '
        'Label359
        '
        Me.Label359.Height = 0.188!
        Me.Label359.HyperLink = Nothing
        Me.Label359.Left = 0!
        Me.Label359.Name = "Label359"
        Me.Label359.Style = "color: DarkRed; font-size: 8.25pt; text-align: left; vertical-align: middle"
        Me.Label359.Text = "Note: This check is for 2-sided SMF connection only" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label359.Top = 5.563!
        Me.Label359.Width = 8.0!
        '
        'Label360
        '
        Me.Label360.Height = 0.1875!
        Me.Label360.HyperLink = Nothing
        Me.Label360.Left = 0.0000002980232!
        Me.Label360.Name = "Label360"
        Me.Label360.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label360.Text = "ΦWCB=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label360.Top = 5.75!
        Me.Label360.Width = 2.312!
        '
        'S38_001
        '
        Me.S38_001.Height = 0.1875!
        Me.S38_001.HyperLink = Nothing
        Me.S38_001.Left = 2.313!
        Me.S38_001.Name = "S38_001"
        Me.S38_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S38_001.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_001.Top = 5.749!
        Me.S38_001.Width = 1.0!
        '
        'Label363
        '
        Me.Label363.Height = 0.1875!
        Me.Label363.HyperLink = Nothing
        Me.Label363.Left = 0.0000002980232!
        Me.Label363.Name = "Label363"
        Me.Label363.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label363.Text = "h=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label363.Top = 5.938001!
        Me.Label363.Width = 2.312!
        '
        'S38_002
        '
        Me.S38_002.Height = 0.1875!
        Me.S38_002.HyperLink = Nothing
        Me.S38_002.Left = 2.313!
        Me.S38_002.Name = "S38_002"
        Me.S38_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S38_002.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_002.Top = 5.937!
        Me.S38_002.Width = 1.0!
        '
        'Label365
        '
        Me.Label365.Height = 0.1875!
        Me.Label365.HyperLink = Nothing
        Me.Label365.Left = 3.313002!
        Me.Label365.Name = "Label365"
        Me.Label365.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label365.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= dc - 2*kdes" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label365.Top = 5.937!
        Me.Label365.Width = 4.686997!
        '
        'Label366
        '
        Me.Label366.Height = 0.1875!
        Me.Label366.HyperLink = Nothing
        Me.Label366.Left = 0!
        Me.Label366.Name = "Label366"
        Me.Label366.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label366.Text = "ΦRn_WCB=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label366.Top = 6.126!
        Me.Label366.Width = 2.312!
        '
        'S38_003
        '
        Me.S38_003.Height = 0.1875!
        Me.S38_003.HyperLink = Nothing
        Me.S38_003.Left = 2.313!
        Me.S38_003.Name = "S38_003"
        Me.S38_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S38_003.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_003.Top = 6.125!
        Me.S38_003.Width = 1.0!
        '
        'Label368
        '
        Me.Label368.Height = 0.1875!
        Me.Label368.HyperLink = Nothing
        Me.Label368.Left = 3.313002!
        Me.Label368.Name = "Label368"
        Me.Label368.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label368.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "AISC J10-8, = ΦWCB* 24* tcw^3* sqrt(Es* Fy_col)/h" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label368.Top = 6.125!
        Me.Label368.Width = 4.686997!
        '
        'Label369
        '
        Me.Label369.Height = 0.1875!
        Me.Label369.HyperLink = Nothing
        Me.Label369.Left = 0.001!
        Me.Label369.Name = "Label369"
        Me.Label369.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label369.Text = "ΦRn_WLC_top=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label369.Top = 6.5!
        Me.Label369.Width = 2.312!
        '
        'S38_004
        '
        Me.S38_004.Height = 0.1875!
        Me.S38_004.HyperLink = Nothing
        Me.S38_004.Left = 2.321!
        Me.S38_004.Name = "S38_004"
        Me.S38_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S38_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_004.Top = 6.499!
        Me.S38_004.Width = 1.0!
        '
        'Label371
        '
        Me.Label371.Height = 0.1875!
        Me.Label371.HyperLink = Nothing
        Me.Label371.Left = 0.001!
        Me.Label371.Name = "Label371"
        Me.Label371.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label371.Text = "ΦRn_WLC_bot=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label371.Top = 6.688002!
        Me.Label371.Width = 2.312!
        '
        'S38_005
        '
        Me.S38_005.Height = 0.1875!
        Me.S38_005.HyperLink = Nothing
        Me.S38_005.Left = 2.321!
        Me.S38_005.Name = "S38_005"
        Me.S38_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S38_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_005.Top = 6.687001!
        Me.S38_005.Width = 1.0!
        '
        'S38_006
        '
        Me.S38_006.Height = 0.1875!
        Me.S38_006.HyperLink = Nothing
        Me.S38_006.Left = 3.321!
        Me.S38_006.Name = "S38_006"
        Me.S38_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S38_006.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_006.Top = 6.499!
        Me.S38_006.Width = 1.0!
        '
        'S38_007
        '
        Me.S38_007.Height = 0.1875!
        Me.S38_007.HyperLink = Nothing
        Me.S38_007.Left = 3.321!
        Me.S38_007.Name = "S38_007"
        Me.S38_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S38_007.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S38_007.Top = 6.687001!
        Me.S38_007.Width = 1.0!
        '
        'Label375
        '
        Me.Label375.Height = 0.1875!
        Me.Label375.HyperLink = Nothing
        Me.Label375.Left = 3.321!
        Me.Label375.Name = "Label375"
        Me.Label375.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label375.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label375.Top = 6.312001!
        Me.Label375.Width = 1.0!
        '
        'Label376
        '
        Me.Label376.Height = 0.1875!
        Me.Label376.HyperLink = Nothing
        Me.Label376.Left = 2.321!
        Me.Label376.Name = "Label376"
        Me.Label376.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label376.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label376.Top = 6.312001!
        Me.Label376.Width = 1.0!
        '
        'Line105
        '
        Me.Line105.Height = 0!
        Me.Line105.Left = 2.321001!
        Me.Line105.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line105.LineWeight = 1.0!
        Me.Line105.Name = "Line105"
        Me.Line105.Top = 6.687001!
        Me.Line105.Width = 2.0!
        Me.Line105.X1 = 2.321001!
        Me.Line105.X2 = 4.321001!
        Me.Line105.Y1 = 6.687001!
        Me.Line105.Y2 = 6.687001!
        '
        'Line106
        '
        Me.Line106.Height = 0!
        Me.Line106.Left = 2.321001!
        Me.Line106.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line106.LineWeight = 1.0!
        Me.Line106.Name = "Line106"
        Me.Line106.Top = 6.5!
        Me.Line106.Width = 2.0!
        Me.Line106.X1 = 2.321001!
        Me.Line106.X2 = 4.321001!
        Me.Line106.Y1 = 6.5!
        Me.Line106.Y2 = 6.5!
        '
        'Line107
        '
        Me.Line107.Height = 0.5619988!
        Me.Line107.Left = 2.321!
        Me.Line107.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line107.LineWeight = 1.0!
        Me.Line107.Name = "Line107"
        Me.Line107.Top = 6.312001!
        Me.Line107.Width = 0!
        Me.Line107.X1 = 2.321!
        Me.Line107.X2 = 2.321!
        Me.Line107.Y1 = 6.312001!
        Me.Line107.Y2 = 6.874!
        '
        'Line108
        '
        Me.Line108.Height = 0.5619988!
        Me.Line108.Left = 3.321!
        Me.Line108.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line108.LineWeight = 1.0!
        Me.Line108.Name = "Line108"
        Me.Line108.Top = 6.312001!
        Me.Line108.Width = 0!
        Me.Line108.X1 = 3.321!
        Me.Line108.X2 = 3.321!
        Me.Line108.Y1 = 6.312001!
        Me.Line108.Y2 = 6.874!
        '
        'Line109
        '
        Me.Line109.Height = 0.5619988!
        Me.Line109.Left = 4.321001!
        Me.Line109.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line109.LineWeight = 1.0!
        Me.Line109.Name = "Line109"
        Me.Line109.Top = 6.312001!
        Me.Line109.Width = 0!
        Me.Line109.X1 = 4.321001!
        Me.Line109.X2 = 4.321001!
        Me.Line109.Y1 = 6.312001!
        Me.Line109.Y2 = 6.874!
        '
        'Line110
        '
        Me.Line110.Height = 0!
        Me.Line110.Left = 2.321!
        Me.Line110.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line110.LineWeight = 1.0!
        Me.Line110.Name = "Line110"
        Me.Line110.Top = 6.313!
        Me.Line110.Width = 2.0!
        Me.Line110.X1 = 2.321!
        Me.Line110.X2 = 4.321!
        Me.Line110.Y1 = 6.313!
        Me.Line110.Y2 = 6.313!
        '
        'Line111
        '
        Me.Line111.Height = 0!
        Me.Line111.Left = 2.313001!
        Me.Line111.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line111.LineWeight = 1.0!
        Me.Line111.Name = "Line111"
        Me.Line111.Top = 6.874!
        Me.Line111.Width = 2.0!
        Me.Line111.X1 = 2.313001!
        Me.Line111.X2 = 4.313001!
        Me.Line111.Y1 = 6.874!
        Me.Line111.Y2 = 6.874!
        '
        'Label357
        '
        Me.Label357.Height = 0.188!
        Me.Label357.HyperLink = Nothing
        Me.Label357.Left = 4.263!
        Me.Label357.Name = "Label357"
        Me.Label357.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label357.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (d_stiffTop > d, ΦRn_WLC_more0.5D, ΦRn_WLC_less0.5D)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label357.Top = 4.874!
        Me.Label357.Width = 3.75!
        '
        'Line102
        '
        Me.Line102.Height = 0.5629988!
        Me.Line102.Left = 4.259!
        Me.Line102.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line102.LineWeight = 1.0!
        Me.Line102.Name = "Line102"
        Me.Line102.Top = 4.688!
        Me.Line102.Width = 0!
        Me.Line102.X1 = 4.259!
        Me.Line102.X2 = 4.259!
        Me.Line102.Y1 = 4.688!
        Me.Line102.Y2 = 5.250999!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 4.25!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label1.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label1.Top = 0.375!
        Me.Label1.Width = 3.313!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.1979167!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(7.75!, 0.1979167!)
        Me.PageBreak1.Top = 0.02083333!
        Me.PageBreak1.Width = 7.75!
        '
        'US_CC3
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 7.75!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label257, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label258, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label269, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label273, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label274, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label275, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label279, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label282, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label283, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label286, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label276, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label287, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label288, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label291, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label292, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label295, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label296, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label299, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label300, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label303, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label304, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label307, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label308, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label311, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label312, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S36_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label315, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label378, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label377, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label316, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label317, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label320, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label322, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label323, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label325, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label326, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label328, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label319, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label330, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label331, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label341, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label342, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label344, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label345, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label347, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label348, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label350, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S37_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label354, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label355, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label356, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label358, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label359, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label360, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label363, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label365, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label366, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label368, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label369, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label371, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S38_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label375, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label376, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label357, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents Label257 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label258 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label269 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label273 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label274 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label275 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label279 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label282 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label283 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label286 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line86 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line87 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line88 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label276 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label287 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label288 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label291 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label292 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label295 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label296 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label299 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label300 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label303 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label304 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label307 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label308 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label311 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label312 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S36_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label315 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line82 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line83 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line84 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line89 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line90 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line91 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line92 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line93 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line94 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line95 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line96 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label378 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label377 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label316 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label317 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label320 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label322 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label323 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label325 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label326 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label328 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label319 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label330 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label331 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label341 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label342 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label344 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label345 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label347 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label348 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label350 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S37_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label354 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label355 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line98 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line99 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label356 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line100 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line101 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line102 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line103 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label357 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line104 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label358 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label359 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label360 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label363 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label365 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label366 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label368 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label369 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label371 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S38_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label375 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label376 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line105 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line106 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line107 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line108 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line109 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line111 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line110 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
End Class
