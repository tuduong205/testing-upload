﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class US_Sum1V_A
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents PageHeader As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    Private WithEvents PageFooter As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_Sum1V_A))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.Label36 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbKd_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbGustFactor_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbKzt_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbGroundElev_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbExposureType_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbWspeed_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor1_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor2_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor3_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor4_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor5_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor6_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor7_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor8_X = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label60 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbST1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbST2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label14 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label19 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbStiff1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbStiff2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbStiff3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbStiff4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label15 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDP1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label31 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDP2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDP3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDGVName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line18 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line19 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line20 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line21 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label37 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label44 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label48 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbParam4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label51 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label55 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label61 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label62 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label64 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label65 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label67 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label68 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label70 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line22 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line23 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line24 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line25 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line27 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line34 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line35 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line38 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line40 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line42 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label71 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label73 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label74 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line41 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line44 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line45 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label57 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label76 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label77 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label79 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label80 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label82 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label83 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label85 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label86 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line26 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line47 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line48 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line49 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line50 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDCode1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line43 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line46 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line51 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line55 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label53 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label56 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label63 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label66 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label72 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label75 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label81 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label84 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line58 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line59 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line60 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line61 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line62 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line63 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line64 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line65 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line66 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line70 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line71 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label98 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label99 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line72 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line73 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line53 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line36 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line39 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line57 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line37 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.lbFactor1_Y = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor2_Y = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor3_Y = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor4_Y = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor5_Y = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbFactor6_Y = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line56 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label96 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label97 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line68 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line52 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line67 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label50 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label59 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line69 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line74 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.lbWcode = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label78 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label87 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line54 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.GroupHeader1 = New GrapeCity.ActiveReports.SectionReportModel.GroupHeader()
        Me.lbJobID = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbJobName = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbPrintedDate = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.GroupFooter1 = New GrapeCity.ActiveReports.SectionReportModel.GroupFooter()
        Me.PageHeader1 = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.PageFooter1 = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbKd_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbGustFactor_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbKzt_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbGroundElev_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbExposureType_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWspeed_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor1_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor2_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor3_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor4_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor5_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor6_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor7_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor8_X, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbST1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbST2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbStiff1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbStiff2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbStiff3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbStiff4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDP1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDP2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDP3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDGVName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbParam4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDCode1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor1_Y, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor2_Y, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor3_Y, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor4_Y, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor5_Y, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbFactor6_Y, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbWcode, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label36, Me.lbKd_X, Me.lbGustFactor_X, Me.lbKzt_X, Me.lbGroundElev_X, Me.lbExposureType_X, Me.lbWspeed_X, Me.lbParam7, Me.lbParam6, Me.lbParam5, Me.lbParam1, Me.lbParam2, Me.lbParam3, Me.lbFactor1_X, Me.lbFactor2_X, Me.lbFactor3_X, Me.lbFactor4_X, Me.lbFactor5_X, Me.lbFactor6_X, Me.lbFactor7_X, Me.lbFactor8_X, Me.Label10, Me.Label60, Me.Label1, Me.Label5, Me.Label6, Me.Label7, Me.Label8, Me.Label9, Me.lbST1, Me.Label11, Me.Label12, Me.lbST2, Me.Label14, Me.Label18, Me.Label19, Me.lbStiff1, Me.Label21, Me.Label22, Me.lbStiff2, Me.Label24, Me.Label25, Me.lbStiff3, Me.Label27, Me.Label28, Me.lbStiff4, Me.Label15, Me.Label16, Me.Label17, Me.lbDP1, Me.Label31, Me.Label32, Me.lbDP2, Me.Label34, Me.Label35, Me.lbDP3, Me.lbDGVName, Me.Line5, Me.Line1, Me.Line2, Me.Line3, Me.Line4, Me.Line6, Me.Line7, Me.Line8, Me.Line9, Me.Line10, Me.Line11, Me.Line12, Me.Line13, Me.Line14, Me.Line15, Me.Line16, Me.Line17, Me.Line18, Me.Line19, Me.Line20, Me.Line21, Me.Label37, Me.Label38, Me.Label39, Me.Label40, Me.Label41, Me.Label42, Me.Label44, Me.Label45, Me.Label48, Me.Label49, Me.lbParam4, Me.Label51, Me.Label52, Me.Label54, Me.Label55, Me.Label61, Me.Label62, Me.Label64, Me.Label65, Me.Label67, Me.Label68, Me.Label70, Me.Line22, Me.Line23, Me.Line24, Me.Line25, Me.Line27, Me.Line28, Me.Line29, Me.Line30, Me.Line31, Me.Line32, Me.Line33, Me.Line34, Me.Line35, Me.Line38, Me.Line40, Me.Line42, Me.Label71, Me.Label73, Me.Label74, Me.Line41, Me.Line44, Me.Line45, Me.Label57, Me.Label58, Me.Label76, Me.Label77, Me.Label79, Me.Label80, Me.Label82, Me.Label83, Me.Label85, Me.Label86, Me.Line26, Me.Line47, Me.Line48, Me.Line49, Me.Line50, Me.Label13, Me.Label20, Me.lbDCode1, Me.Line43, Me.Line46, Me.Line51, Me.Line55, Me.Label29, Me.Label30, Me.Label33, Me.Label43, Me.Label46, Me.Label53, Me.Label56, Me.Label63, Me.Label66, Me.Label72, Me.Label75, Me.Label81, Me.Label84, Me.Line58, Me.Line59, Me.Line60, Me.Line61, Me.Line62, Me.Line63, Me.Line64, Me.Line65, Me.Line66, Me.Line70, Me.Line71, Me.Label98, Me.Label99, Me.Line72, Me.Line73, Me.Line53, Me.Label4, Me.Label23, Me.Line36, Me.Label47, Me.Line39, Me.Line57, Me.Line37, Me.lbFactor1_Y, Me.lbFactor2_Y, Me.lbFactor3_Y, Me.lbFactor4_Y, Me.lbFactor5_Y, Me.lbFactor6_Y, Me.Line56, Me.Label96, Me.Label97, Me.Line68, Me.Line52, Me.Line67, Me.Label26, Me.Label50, Me.Label59, Me.Line69, Me.Line74, Me.lbWcode, Me.Label78, Me.Label87, Me.Line54})
        Me.Detail.Height = 10.76042!
        Me.Detail.Name = "Detail"
        '
        'Label36
        '
        Me.Label36.Height = 0.2!
        Me.Label36.HyperLink = Nothing
        Me.Label36.Left = 0.74!
        Me.Label36.Name = "Label36"
        Me.Label36.Style = "color: DarkRed; font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.Label36.Text = "     Wind Coefficients"
        Me.Label36.Top = 8.879001!
        Me.Label36.Width = 6.25!
        '
        'lbKd_X
        '
        Me.lbKd_X.Height = 0.2!
        Me.lbKd_X.HyperLink = Nothing
        Me.lbKd_X.Left = 5.25!
        Me.lbKd_X.Name = "lbKd_X"
        Me.lbKd_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbKd_X.Text = "Value"
        Me.lbKd_X.Top = 10.487!
        Me.lbKd_X.Width = 1.75!
        '
        'lbGustFactor_X
        '
        Me.lbGustFactor_X.Height = 0.2!
        Me.lbGustFactor_X.HyperLink = Nothing
        Me.lbGustFactor_X.Left = 5.25!
        Me.lbGustFactor_X.Name = "lbGustFactor_X"
        Me.lbGustFactor_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbGustFactor_X.Text = "Value"
        Me.lbGustFactor_X.Top = 10.287!
        Me.lbGustFactor_X.Width = 1.75!
        '
        'lbKzt_X
        '
        Me.lbKzt_X.Height = 0.2!
        Me.lbKzt_X.HyperLink = Nothing
        Me.lbKzt_X.Left = 5.25!
        Me.lbKzt_X.Name = "lbKzt_X"
        Me.lbKzt_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbKzt_X.Text = "Value"
        Me.lbKzt_X.Top = 10.087!
        Me.lbKzt_X.Width = 1.75!
        '
        'lbGroundElev_X
        '
        Me.lbGroundElev_X.Height = 0.2!
        Me.lbGroundElev_X.HyperLink = Nothing
        Me.lbGroundElev_X.Left = 5.25!
        Me.lbGroundElev_X.Name = "lbGroundElev_X"
        Me.lbGroundElev_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbGroundElev_X.Text = "Value"
        Me.lbGroundElev_X.Top = 9.890001!
        Me.lbGroundElev_X.Width = 1.75!
        '
        'lbExposureType_X
        '
        Me.lbExposureType_X.Height = 0.2!
        Me.lbExposureType_X.HyperLink = Nothing
        Me.lbExposureType_X.Left = 5.25!
        Me.lbExposureType_X.Name = "lbExposureType_X"
        Me.lbExposureType_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbExposureType_X.Text = "Value"
        Me.lbExposureType_X.Top = 9.687001!
        Me.lbExposureType_X.Width = 1.75!
        '
        'lbWspeed_X
        '
        Me.lbWspeed_X.Height = 0.2!
        Me.lbWspeed_X.HyperLink = Nothing
        Me.lbWspeed_X.Left = 5.25!
        Me.lbWspeed_X.Name = "lbWspeed_X"
        Me.lbWspeed_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbWspeed_X.Text = "Value"
        Me.lbWspeed_X.Top = 9.487001!
        Me.lbWspeed_X.Width = 1.75!
        '
        'lbParam7
        '
        Me.lbParam7.Height = 0.2!
        Me.lbParam7.HyperLink = Nothing
        Me.lbParam7.Left = 5.250001!
        Me.lbParam7.Name = "lbParam7"
        Me.lbParam7.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam7.Text = "Value"
        Me.lbParam7.Top = 5.359!
        Me.lbParam7.Width = 1.75!
        '
        'lbParam6
        '
        Me.lbParam6.Height = 0.2!
        Me.lbParam6.HyperLink = Nothing
        Me.lbParam6.Left = 5.25!
        Me.lbParam6.Name = "lbParam6"
        Me.lbParam6.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam6.Text = "Value"
        Me.lbParam6.Top = 5.159!
        Me.lbParam6.Width = 1.75!
        '
        'lbParam5
        '
        Me.lbParam5.Height = 0.2!
        Me.lbParam5.HyperLink = Nothing
        Me.lbParam5.Left = 5.25!
        Me.lbParam5.Name = "lbParam5"
        Me.lbParam5.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam5.Text = "Value"
        Me.lbParam5.Top = 4.959!
        Me.lbParam5.Width = 1.75!
        '
        'lbParam1
        '
        Me.lbParam1.Height = 0.2!
        Me.lbParam1.HyperLink = Nothing
        Me.lbParam1.Left = 5.25!
        Me.lbParam1.Name = "lbParam1"
        Me.lbParam1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam1.Text = "Value"
        Me.lbParam1.Top = 4.159!
        Me.lbParam1.Width = 1.75!
        '
        'lbParam2
        '
        Me.lbParam2.Height = 0.2!
        Me.lbParam2.HyperLink = Nothing
        Me.lbParam2.Left = 5.25!
        Me.lbParam2.Name = "lbParam2"
        Me.lbParam2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam2.Text = "Value"
        Me.lbParam2.Top = 4.359!
        Me.lbParam2.Width = 1.75!
        '
        'lbParam3
        '
        Me.lbParam3.Height = 0.2!
        Me.lbParam3.HyperLink = Nothing
        Me.lbParam3.Left = 5.25!
        Me.lbParam3.Name = "lbParam3"
        Me.lbParam3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam3.Text = "Value"
        Me.lbParam3.Top = 4.562!
        Me.lbParam3.Width = 1.75!
        '
        'lbFactor1_X
        '
        Me.lbFactor1_X.Height = 0.2!
        Me.lbFactor1_X.HyperLink = Nothing
        Me.lbFactor1_X.Left = 5.25!
        Me.lbFactor1_X.Name = "lbFactor1_X"
        Me.lbFactor1_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor1_X.Text = "Value"
        Me.lbFactor1_X.Top = 5.962!
        Me.lbFactor1_X.Width = 0.875!
        '
        'lbFactor2_X
        '
        Me.lbFactor2_X.Height = 0.2!
        Me.lbFactor2_X.HyperLink = Nothing
        Me.lbFactor2_X.Left = 5.25!
        Me.lbFactor2_X.Name = "lbFactor2_X"
        Me.lbFactor2_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor2_X.Text = "Value"
        Me.lbFactor2_X.Top = 6.162!
        Me.lbFactor2_X.Width = 0.875!
        '
        'lbFactor3_X
        '
        Me.lbFactor3_X.Height = 0.2!
        Me.lbFactor3_X.HyperLink = Nothing
        Me.lbFactor3_X.Left = 5.25!
        Me.lbFactor3_X.Name = "lbFactor3_X"
        Me.lbFactor3_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor3_X.Text = "Value"
        Me.lbFactor3_X.Top = 6.362001!
        Me.lbFactor3_X.Width = 0.875!
        '
        'lbFactor4_X
        '
        Me.lbFactor4_X.Height = 0.2!
        Me.lbFactor4_X.HyperLink = Nothing
        Me.lbFactor4_X.Left = 5.25!
        Me.lbFactor4_X.Name = "lbFactor4_X"
        Me.lbFactor4_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor4_X.Text = "Value"
        Me.lbFactor4_X.Top = 6.562!
        Me.lbFactor4_X.Width = 0.875!
        '
        'lbFactor5_X
        '
        Me.lbFactor5_X.Height = 0.2!
        Me.lbFactor5_X.HyperLink = Nothing
        Me.lbFactor5_X.Left = 5.250001!
        Me.lbFactor5_X.Name = "lbFactor5_X"
        Me.lbFactor5_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor5_X.Text = "Value"
        Me.lbFactor5_X.Top = 6.762!
        Me.lbFactor5_X.Width = 0.875!
        '
        'lbFactor6_X
        '
        Me.lbFactor6_X.Height = 0.2!
        Me.lbFactor6_X.HyperLink = Nothing
        Me.lbFactor6_X.Left = 5.25!
        Me.lbFactor6_X.Name = "lbFactor6_X"
        Me.lbFactor6_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor6_X.Text = "Value"
        Me.lbFactor6_X.Top = 6.962!
        Me.lbFactor6_X.Width = 0.875!
        '
        'lbFactor7_X
        '
        Me.lbFactor7_X.Height = 0.2!
        Me.lbFactor7_X.HyperLink = Nothing
        Me.lbFactor7_X.Left = 5.25!
        Me.lbFactor7_X.Name = "lbFactor7_X"
        Me.lbFactor7_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor7_X.Text = "Value"
        Me.lbFactor7_X.Top = 7.162!
        Me.lbFactor7_X.Width = 1.75!
        '
        'lbFactor8_X
        '
        Me.lbFactor8_X.Height = 0.2!
        Me.lbFactor8_X.HyperLink = Nothing
        Me.lbFactor8_X.Left = 5.25!
        Me.lbFactor8_X.Name = "lbFactor8_X"
        Me.lbFactor8_X.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor8_X.Text = "Value"
        Me.lbFactor8_X.Top = 7.362!
        Me.lbFactor8_X.Width = 1.75!
        '
        'Label10
        '
        Me.Label10.Height = 0.2!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0.7500003!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "color: DarkRed; font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.Label10.Text = "     Member Design"
        Me.Label10.Top = 7.565!
        Me.Label10.Width = 6.25!
        '
        'Label60
        '
        Me.Label60.Height = 0.2!
        Me.Label60.HyperLink = Nothing
        Me.Label60.Left = 0.7500001!
        Me.Label60.Name = "Label60"
        Me.Label60.Style = "color: DarkRed; font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.Label60.Text = "     Factors"
        Me.Label60.Top = 5.562!
        Me.Label60.Width = 6.25!
        '
        'Label1
        '
        Me.Label1.Height = 0.2!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.7500001!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label1.Text = "No." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.Label1.Top = 0.426!
        Me.Label1.Width = 1.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 1.75!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label5.Text = "Item"
        Me.Label5.Top = 0.426!
        Me.Label5.Width = 3.5!
        '
        'Label6
        '
        Me.Label6.Height = 0.2!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 5.25!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label6.Text = "Value"
        Me.Label6.Top = 0.426!
        Me.Label6.Width = 1.75!
        '
        'Label7
        '
        Me.Label7.Height = 0.2!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.7500001!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label7.Text = "     Shear Plate"
        Me.Label7.Top = 0.626!
        Me.Label7.Width = 6.250001!
        '
        'Label8
        '
        Me.Label8.Height = 0.2!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0.7500001!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label8.Text = "1"
        Me.Label8.Top = 0.8260001!
        Me.Label8.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.2!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 1.75!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label9.Text = "Fy (ksi)="
        Me.Label9.Top = 0.8260001!
        Me.Label9.Width = 3.5!
        '
        'lbST1
        '
        Me.lbST1.Height = 0.2!
        Me.lbST1.HyperLink = Nothing
        Me.lbST1.Left = 5.25!
        Me.lbST1.Name = "lbST1"
        Me.lbST1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbST1.Text = "Value"
        Me.lbST1.Top = 0.8260001!
        Me.lbST1.Width = 1.75!
        '
        'Label11
        '
        Me.Label11.Height = 0.2!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 0.7500001!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label11.Text = "2"
        Me.Label11.Top = 1.026!
        Me.Label11.Width = 1.0!
        '
        'Label12
        '
        Me.Label12.Height = 0.2!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 1.75!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label12.Text = "Fu (ksi)="
        Me.Label12.Top = 1.026!
        Me.Label12.Width = 3.5!
        '
        'lbST2
        '
        Me.lbST2.Height = 0.2!
        Me.lbST2.HyperLink = Nothing
        Me.lbST2.Left = 5.25!
        Me.lbST2.Name = "lbST2"
        Me.lbST2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbST2.Text = "Value"
        Me.lbST2.Top = 1.026!
        Me.lbST2.Width = 1.75!
        '
        'Label14
        '
        Me.Label14.Height = 0.2!
        Me.Label14.HyperLink = Nothing
        Me.Label14.Left = 0.75!
        Me.Label14.Name = "Label14"
        Me.Label14.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label14.Text = "     Stiffener Plate"
        Me.Label14.Top = 1.226!
        Me.Label14.Width = 6.250001!
        '
        'Label18
        '
        Me.Label18.Height = 0.2!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 0.7500001!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label18.Text = "1"
        Me.Label18.Top = 1.426!
        Me.Label18.Width = 1.0!
        '
        'Label19
        '
        Me.Label19.Height = 0.2!
        Me.Label19.HyperLink = Nothing
        Me.Label19.Left = 1.75!
        Me.Label19.Name = "Label19"
        Me.Label19.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label19.Text = "Fy (ksi)="
        Me.Label19.Top = 1.426!
        Me.Label19.Width = 3.5!
        '
        'lbStiff1
        '
        Me.lbStiff1.Height = 0.2!
        Me.lbStiff1.HyperLink = Nothing
        Me.lbStiff1.Left = 5.25!
        Me.lbStiff1.Name = "lbStiff1"
        Me.lbStiff1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbStiff1.Text = "Value"
        Me.lbStiff1.Top = 1.426!
        Me.lbStiff1.Width = 1.75!
        '
        'Label21
        '
        Me.Label21.Height = 0.2!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0.7500001!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label21.Text = "2"
        Me.Label21.Top = 1.626!
        Me.Label21.Width = 1.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.2!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 1.75!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label22.Text = "Fu (ksi)="
        Me.Label22.Top = 1.626!
        Me.Label22.Width = 3.5!
        '
        'lbStiff2
        '
        Me.lbStiff2.Height = 0.2!
        Me.lbStiff2.HyperLink = Nothing
        Me.lbStiff2.Left = 5.25!
        Me.lbStiff2.Name = "lbStiff2"
        Me.lbStiff2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbStiff2.Text = "Value"
        Me.lbStiff2.Top = 1.626!
        Me.lbStiff2.Width = 1.75!
        '
        'Label24
        '
        Me.Label24.Height = 0.2!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0.7500001!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label24.Text = "3"
        Me.Label24.Top = 1.826!
        Me.Label24.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.2!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 1.75!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label25.Text = "Min. Thickness, t_STP (in)="
        Me.Label25.Top = 1.826!
        Me.Label25.Width = 3.5!
        '
        'lbStiff3
        '
        Me.lbStiff3.Height = 0.2!
        Me.lbStiff3.HyperLink = Nothing
        Me.lbStiff3.Left = 5.25!
        Me.lbStiff3.Name = "lbStiff3"
        Me.lbStiff3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbStiff3.Text = "Value"
        Me.lbStiff3.Top = 1.826!
        Me.lbStiff3.Width = 1.75!
        '
        'Label27
        '
        Me.Label27.Height = 0.2!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 0.7500001!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label27.Text = "4"
        Me.Label27.Top = 2.026!
        Me.Label27.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.2!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 1.75!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label28.Text = "Stiff. Depth (1-sided Connection)="
        Me.Label28.Top = 2.026!
        Me.Label28.Width = 3.5!
        '
        'lbStiff4
        '
        Me.lbStiff4.Height = 0.2!
        Me.lbStiff4.HyperLink = Nothing
        Me.lbStiff4.Left = 5.25!
        Me.lbStiff4.Name = "lbStiff4"
        Me.lbStiff4.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbStiff4.Text = "Value"
        Me.lbStiff4.Top = 2.026!
        Me.lbStiff4.Width = 1.75!
        '
        'Label15
        '
        Me.Label15.Height = 0.2!
        Me.Label15.HyperLink = Nothing
        Me.Label15.Left = 0.75!
        Me.Label15.Name = "Label15"
        Me.Label15.Style = "color: DarkRed; font-size: 9pt; font-weight: bold; vertical-align: middle; ddo-ch" &
    "ar-set: 1"
        Me.Label15.Text = "     Doubler Plate"
        Me.Label15.Top = 2.226!
        Me.Label15.Width = 6.25!
        '
        'Label16
        '
        Me.Label16.Height = 0.2!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0.7500001!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label16.Text = "1"
        Me.Label16.Top = 2.426!
        Me.Label16.Width = 1.0!
        '
        'Label17
        '
        Me.Label17.Height = 0.2!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 1.75!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label17.Text = "Fy (ksi)="
        Me.Label17.Top = 2.426!
        Me.Label17.Width = 3.5!
        '
        'lbDP1
        '
        Me.lbDP1.Height = 0.2!
        Me.lbDP1.HyperLink = Nothing
        Me.lbDP1.Left = 5.25!
        Me.lbDP1.Name = "lbDP1"
        Me.lbDP1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbDP1.Text = "Value"
        Me.lbDP1.Top = 2.426!
        Me.lbDP1.Width = 1.75!
        '
        'Label31
        '
        Me.Label31.Height = 0.2!
        Me.Label31.HyperLink = Nothing
        Me.Label31.Left = 0.7500001!
        Me.Label31.Name = "Label31"
        Me.Label31.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label31.Text = "2"
        Me.Label31.Top = 2.626!
        Me.Label31.Width = 1.0!
        '
        'Label32
        '
        Me.Label32.Height = 0.2!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 1.75!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label32.Text = "Fu (ksi)="
        Me.Label32.Top = 2.626!
        Me.Label32.Width = 3.5!
        '
        'lbDP2
        '
        Me.lbDP2.Height = 0.2!
        Me.lbDP2.HyperLink = Nothing
        Me.lbDP2.Left = 5.25!
        Me.lbDP2.Name = "lbDP2"
        Me.lbDP2.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbDP2.Text = "Value"
        Me.lbDP2.Top = 2.626!
        Me.lbDP2.Width = 1.75!
        '
        'Label34
        '
        Me.Label34.Height = 0.2!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 0.7500001!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label34.Text = "3"
        Me.Label34.Top = 2.826!
        Me.Label34.Width = 1.0!
        '
        'Label35
        '
        Me.Label35.Height = 0.2!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 1.75!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label35.Text = "Min. Thickness, t_DP (in)="
        Me.Label35.Top = 2.826!
        Me.Label35.Width = 3.5!
        '
        'lbDP3
        '
        Me.lbDP3.Height = 0.2!
        Me.lbDP3.HyperLink = Nothing
        Me.lbDP3.Left = 5.25!
        Me.lbDP3.Name = "lbDP3"
        Me.lbDP3.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbDP3.Text = "Value"
        Me.lbDP3.Top = 2.826!
        Me.lbDP3.Width = 1.75!
        '
        'lbDGVName
        '
        Me.lbDGVName.Height = 0.265!
        Me.lbDGVName.HyperLink = Nothing
        Me.lbDGVName.Left = 0!
        Me.lbDGVName.Name = "lbDGVName"
        Me.lbDGVName.Style = "font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.lbDGVName.Text = "MATERIAL PROPERTIES"
        Me.lbDGVName.Top = 0!
        Me.lbDGVName.Width = 8.0!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 0.7500001!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 3.026!
        Me.Line5.Width = 6.25!
        Me.Line5.X1 = 0.7500001!
        Me.Line5.X2 = 7.0!
        Me.Line5.Y1 = 3.026!
        Me.Line5.Y2 = 3.026!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 0.75!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 2.826!
        Me.Line1.Width = 6.25!
        Me.Line1.X1 = 0.75!
        Me.Line1.X2 = 7.0!
        Me.Line1.Y1 = 2.826!
        Me.Line1.Y2 = 2.826!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 0.75!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 2.626!
        Me.Line2.Width = 6.25!
        Me.Line2.X1 = 0.75!
        Me.Line2.X2 = 7.0!
        Me.Line2.Y1 = 2.626!
        Me.Line2.Y2 = 2.626!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 0.75!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 2.426!
        Me.Line3.Width = 6.25!
        Me.Line3.X1 = 0.75!
        Me.Line3.X2 = 7.0!
        Me.Line3.Y1 = 2.426!
        Me.Line3.Y2 = 2.426!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 0.75!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 2.226!
        Me.Line4.Width = 6.25!
        Me.Line4.X1 = 0.75!
        Me.Line4.X2 = 7.0!
        Me.Line4.Y1 = 2.226!
        Me.Line4.Y2 = 2.226!
        '
        'Line6
        '
        Me.Line6.Height = 0!
        Me.Line6.Left = 0.75!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 2.026!
        Me.Line6.Width = 6.25!
        Me.Line6.X1 = 0.75!
        Me.Line6.X2 = 7.0!
        Me.Line6.Y1 = 2.026!
        Me.Line6.Y2 = 2.026!
        '
        'Line7
        '
        Me.Line7.Height = 0!
        Me.Line7.Left = 0.75!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 1.826!
        Me.Line7.Width = 6.25!
        Me.Line7.X1 = 0.75!
        Me.Line7.X2 = 7.0!
        Me.Line7.Y1 = 1.826!
        Me.Line7.Y2 = 1.826!
        '
        'Line8
        '
        Me.Line8.Height = 0!
        Me.Line8.Left = 0.75!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 1.626!
        Me.Line8.Width = 6.25!
        Me.Line8.X1 = 0.75!
        Me.Line8.X2 = 7.0!
        Me.Line8.Y1 = 1.626!
        Me.Line8.Y2 = 1.626!
        '
        'Line9
        '
        Me.Line9.Height = 0!
        Me.Line9.Left = 0.75!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 1.426!
        Me.Line9.Width = 6.25!
        Me.Line9.X1 = 0.75!
        Me.Line9.X2 = 7.0!
        Me.Line9.Y1 = 1.426!
        Me.Line9.Y2 = 1.426!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 0.75!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 1.226!
        Me.Line10.Width = 6.25!
        Me.Line10.X1 = 0.75!
        Me.Line10.X2 = 7.0!
        Me.Line10.Y1 = 1.226!
        Me.Line10.Y2 = 1.226!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 0.75!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 1.026!
        Me.Line11.Width = 6.25!
        Me.Line11.X1 = 0.75!
        Me.Line11.X2 = 7.0!
        Me.Line11.Y1 = 1.026!
        Me.Line11.Y2 = 1.026!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 0.75!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 0.8259999!
        Me.Line12.Width = 6.25!
        Me.Line12.X1 = 0.75!
        Me.Line12.X2 = 7.0!
        Me.Line12.Y1 = 0.8259999!
        Me.Line12.Y2 = 0.8259999!
        '
        'Line13
        '
        Me.Line13.Height = 0.0000001192093!
        Me.Line13.Left = 0.75!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 0.626!
        Me.Line13.Width = 6.25!
        Me.Line13.X1 = 0.75!
        Me.Line13.X2 = 7.0!
        Me.Line13.Y1 = 0.626!
        Me.Line13.Y2 = 0.6260001!
        '
        'Line14
        '
        Me.Line14.Height = 0!
        Me.Line14.Left = 0.75!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 0.426!
        Me.Line14.Width = 6.25!
        Me.Line14.X1 = 0.75!
        Me.Line14.X2 = 7.0!
        Me.Line14.Y1 = 0.426!
        Me.Line14.Y2 = 0.426!
        '
        'Line15
        '
        Me.Line15.Height = 2.603!
        Me.Line15.Left = 7.0!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 0.423!
        Me.Line15.Width = 0!
        Me.Line15.X1 = 7.0!
        Me.Line15.X2 = 7.0!
        Me.Line15.Y1 = 0.423!
        Me.Line15.Y2 = 3.026!
        '
        'Line16
        '
        Me.Line16.Height = 2.2!
        Me.Line16.Left = 5.25!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 0.826!
        Me.Line16.Width = 0!
        Me.Line16.X1 = 5.25!
        Me.Line16.X2 = 5.25!
        Me.Line16.Y1 = 0.826!
        Me.Line16.Y2 = 3.026!
        '
        'Line17
        '
        Me.Line17.Height = 0.599999!
        Me.Line17.Left = 1.75!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 2.426!
        Me.Line17.Width = 0!
        Me.Line17.X1 = 1.75!
        Me.Line17.X2 = 1.75!
        Me.Line17.Y1 = 2.426!
        Me.Line17.Y2 = 3.025999!
        '
        'Line18
        '
        Me.Line18.Height = 2.602999!
        Me.Line18.Left = 0.7500002!
        Me.Line18.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line18.LineWeight = 1.0!
        Me.Line18.Name = "Line18"
        Me.Line18.Top = 0.423!
        Me.Line18.Width = 0!
        Me.Line18.X1 = 0.7500002!
        Me.Line18.X2 = 0.7500002!
        Me.Line18.Y1 = 0.423!
        Me.Line18.Y2 = 3.025999!
        '
        'Line19
        '
        Me.Line19.Height = 0.7999991!
        Me.Line19.Left = 1.75!
        Me.Line19.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line19.LineWeight = 1.0!
        Me.Line19.Name = "Line19"
        Me.Line19.Top = 1.426!
        Me.Line19.Width = 0.0000009536743!
        Me.Line19.X1 = 1.75!
        Me.Line19.X2 = 1.750001!
        Me.Line19.Y1 = 1.426!
        Me.Line19.Y2 = 2.225999!
        '
        'Line20
        '
        Me.Line20.Height = 0.399999!
        Me.Line20.Left = 1.75!
        Me.Line20.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line20.LineWeight = 1.0!
        Me.Line20.Name = "Line20"
        Me.Line20.Top = 0.826!
        Me.Line20.Width = 0.0000009536743!
        Me.Line20.X1 = 1.75!
        Me.Line20.X2 = 1.750001!
        Me.Line20.Y1 = 0.826!
        Me.Line20.Y2 = 1.225999!
        '
        'Line21
        '
        Me.Line21.Height = 0.2029992!
        Me.Line21.Left = 1.75!
        Me.Line21.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line21.LineWeight = 1.0!
        Me.Line21.Name = "Line21"
        Me.Line21.Top = 0.423!
        Me.Line21.Width = 0.0000009536743!
        Me.Line21.X1 = 1.75!
        Me.Line21.X2 = 1.750001!
        Me.Line21.Y1 = 0.423!
        Me.Line21.Y2 = 0.6259992!
        '
        'Label37
        '
        Me.Label37.Height = 0.2!
        Me.Label37.HyperLink = Nothing
        Me.Label37.Left = 0.75!
        Me.Label37.Name = "Label37"
        Me.Label37.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label37.Text = "No." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.Label37.Top = 3.759!
        Me.Label37.Width = 1.0!
        '
        'Label38
        '
        Me.Label38.Height = 0.2!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 1.75!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label38.Text = "Item"
        Me.Label38.Top = 3.759!
        Me.Label38.Width = 3.5!
        '
        'Label39
        '
        Me.Label39.Height = 0.2!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 5.25!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label39.Text = "Data"
        Me.Label39.Top = 3.759!
        Me.Label39.Width = 1.75!
        '
        'Label40
        '
        Me.Label40.Height = 0.2!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0.75!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "color: DarkRed; font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.Label40.Text = "     Seismics Coefficients"
        Me.Label40.Top = 3.959!
        Me.Label40.Width = 6.25!
        '
        'Label41
        '
        Me.Label41.Height = 0.2!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 0.75!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label41.Text = "1"
        Me.Label41.Top = 4.159!
        Me.Label41.Width = 1.0!
        '
        'Label42
        '
        Me.Label42.Height = 0.2!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 1.75!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label42.Text = "Standard="
        Me.Label42.Top = 4.159!
        Me.Label42.Width = 3.5!
        '
        'Label44
        '
        Me.Label44.Height = 0.2!
        Me.Label44.HyperLink = Nothing
        Me.Label44.Left = 0.75!
        Me.Label44.Name = "Label44"
        Me.Label44.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label44.Text = "2"
        Me.Label44.Top = 4.359!
        Me.Label44.Width = 1.0!
        '
        'Label45
        '
        Me.Label45.Height = 0.2!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 1.75!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label45.Text = "0.2 Sec Spectral Accel, Ss="
        Me.Label45.Top = 4.359!
        Me.Label45.Width = 3.5!
        '
        'Label48
        '
        Me.Label48.Height = 0.2!
        Me.Label48.HyperLink = Nothing
        Me.Label48.Left = 0.75!
        Me.Label48.Name = "Label48"
        Me.Label48.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label48.Text = "4"
        Me.Label48.Top = 4.759!
        Me.Label48.Width = 1.0!
        '
        'Label49
        '
        Me.Label49.Height = 0.2!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 1.75!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label49.Text = "Long-Period Transition Period, TL="
        Me.Label49.Top = 4.759!
        Me.Label49.Width = 3.5!
        '
        'lbParam4
        '
        Me.lbParam4.Height = 0.2!
        Me.lbParam4.HyperLink = Nothing
        Me.lbParam4.Left = 5.25!
        Me.lbParam4.Name = "lbParam4"
        Me.lbParam4.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbParam4.Text = "Value"
        Me.lbParam4.Top = 4.759!
        Me.lbParam4.Width = 1.75!
        '
        'Label51
        '
        Me.Label51.Height = 0.2!
        Me.Label51.HyperLink = Nothing
        Me.Label51.Left = 0.75!
        Me.Label51.Name = "Label51"
        Me.Label51.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label51.Text = "5"
        Me.Label51.Top = 4.959!
        Me.Label51.Width = 1.0!
        '
        'Label52
        '
        Me.Label52.Height = 0.2!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 1.75!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label52.Text = "Site Class="
        Me.Label52.Top = 4.959!
        Me.Label52.Width = 3.5!
        '
        'Label54
        '
        Me.Label54.Height = 0.2!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 0.75!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label54.Text = "6"
        Me.Label54.Top = 5.159!
        Me.Label54.Width = 1.0!
        '
        'Label55
        '
        Me.Label55.Height = 0.2!
        Me.Label55.HyperLink = Nothing
        Me.Label55.Left = 1.75!
        Me.Label55.Name = "Label55"
        Me.Label55.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label55.Text = "Sds - User defined="
        Me.Label55.Top = 5.159!
        Me.Label55.Width = 3.5!
        '
        'Label61
        '
        Me.Label61.Height = 0.2!
        Me.Label61.HyperLink = Nothing
        Me.Label61.Left = 0.7500005!
        Me.Label61.Name = "Label61"
        Me.Label61.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label61.Text = "1"
        Me.Label61.Top = 5.962!
        Me.Label61.Width = 1.0!
        '
        'Label62
        '
        Me.Label62.Height = 0.2!
        Me.Label62.HyperLink = Nothing
        Me.Label62.Left = 1.75!
        Me.Label62.Name = "Label62"
        Me.Label62.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label62.Text = "Lateral Force Resisting System="
        Me.Label62.Top = 5.962!
        Me.Label62.Width = 3.5!
        '
        'Label64
        '
        Me.Label64.Height = 0.2!
        Me.Label64.HyperLink = Nothing
        Me.Label64.Left = 0.7500005!
        Me.Label64.Name = "Label64"
        Me.Label64.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label64.Text = "2"
        Me.Label64.Top = 6.162!
        Me.Label64.Width = 1.0!
        '
        'Label65
        '
        Me.Label65.Height = 0.2!
        Me.Label65.HyperLink = Nothing
        Me.Label65.Left = 1.75!
        Me.Label65.Name = "Label65"
        Me.Label65.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label65.Text = "Response Modification Coefficient, R="
        Me.Label65.Top = 6.162!
        Me.Label65.Width = 3.5!
        '
        'Label67
        '
        Me.Label67.Height = 0.2!
        Me.Label67.HyperLink = Nothing
        Me.Label67.Left = 0.7500006!
        Me.Label67.Name = "Label67"
        Me.Label67.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label67.Text = "4"
        Me.Label67.Top = 6.562!
        Me.Label67.Width = 1.0!
        '
        'Label68
        '
        Me.Label68.Height = 0.2!
        Me.Label68.HyperLink = Nothing
        Me.Label68.Left = 1.75!
        Me.Label68.Name = "Label68"
        Me.Label68.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label68.Text = "Deflection Amplification factor, Cd="
        Me.Label68.Top = 6.562!
        Me.Label68.Width = 3.5!
        '
        'Label70
        '
        Me.Label70.Height = 0.265!
        Me.Label70.HyperLink = Nothing
        Me.Label70.Left = 0!
        Me.Label70.Name = "Label70"
        Me.Label70.Style = "font-size: 9.75pt; font-weight: bold; vertical-align: middle"
        Me.Label70.Text = "DESIGN PARAMETERS"
        Me.Label70.Top = 3.333!
        Me.Label70.Width = 8.0!
        '
        'Line22
        '
        Me.Line22.Height = 0!
        Me.Line22.Left = 0.7500004!
        Me.Line22.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line22.LineWeight = 1.0!
        Me.Line22.Name = "Line22"
        Me.Line22.Top = 6.762!
        Me.Line22.Width = 6.25!
        Me.Line22.X1 = 0.7500004!
        Me.Line22.X2 = 7.0!
        Me.Line22.Y1 = 6.762!
        Me.Line22.Y2 = 6.762!
        '
        'Line23
        '
        Me.Line23.Height = 0.0000009536743!
        Me.Line23.Left = 0.7500004!
        Me.Line23.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line23.LineWeight = 1.0!
        Me.Line23.Name = "Line23"
        Me.Line23.Top = 6.362!
        Me.Line23.Width = 6.25!
        Me.Line23.X1 = 0.7500004!
        Me.Line23.X2 = 7.0!
        Me.Line23.Y1 = 6.362!
        Me.Line23.Y2 = 6.362001!
        '
        'Line24
        '
        Me.Line24.Height = 0!
        Me.Line24.Left = 0.7500004!
        Me.Line24.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line24.LineWeight = 1.0!
        Me.Line24.Name = "Line24"
        Me.Line24.Top = 6.162!
        Me.Line24.Width = 6.25!
        Me.Line24.X1 = 0.7500004!
        Me.Line24.X2 = 7.0!
        Me.Line24.Y1 = 6.162!
        Me.Line24.Y2 = 6.162!
        '
        'Line25
        '
        Me.Line25.Height = 0!
        Me.Line25.Left = 0.7500004!
        Me.Line25.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line25.LineWeight = 1.0!
        Me.Line25.Name = "Line25"
        Me.Line25.Top = 5.962!
        Me.Line25.Width = 6.25!
        Me.Line25.X1 = 0.7500004!
        Me.Line25.X2 = 7.0!
        Me.Line25.Y1 = 5.962!
        Me.Line25.Y2 = 5.962!
        '
        'Line27
        '
        Me.Line27.Height = 0!
        Me.Line27.Left = 0.75!
        Me.Line27.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line27.LineWeight = 1.0!
        Me.Line27.Name = "Line27"
        Me.Line27.Top = 5.359!
        Me.Line27.Width = 6.25!
        Me.Line27.X1 = 0.75!
        Me.Line27.X2 = 7.0!
        Me.Line27.Y1 = 5.359!
        Me.Line27.Y2 = 5.359!
        '
        'Line28
        '
        Me.Line28.Height = 0!
        Me.Line28.Left = 0.75!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 5.159!
        Me.Line28.Width = 6.25!
        Me.Line28.X1 = 0.75!
        Me.Line28.X2 = 7.0!
        Me.Line28.Y1 = 5.159!
        Me.Line28.Y2 = 5.159!
        '
        'Line29
        '
        Me.Line29.Height = 0!
        Me.Line29.Left = 0.75!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 4.959!
        Me.Line29.Width = 6.25!
        Me.Line29.X1 = 0.75!
        Me.Line29.X2 = 7.0!
        Me.Line29.Y1 = 4.959!
        Me.Line29.Y2 = 4.959!
        '
        'Line30
        '
        Me.Line30.Height = 0.003000259!
        Me.Line30.Left = 0.75!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 4.759!
        Me.Line30.Width = 6.25!
        Me.Line30.X1 = 0.75!
        Me.Line30.X2 = 7.0!
        Me.Line30.Y1 = 4.762!
        Me.Line30.Y2 = 4.759!
        '
        'Line31
        '
        Me.Line31.Height = 0.002999783!
        Me.Line31.Left = 0.75!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 4.559!
        Me.Line31.Width = 6.25!
        Me.Line31.X1 = 0.75!
        Me.Line31.X2 = 7.0!
        Me.Line31.Y1 = 4.562!
        Me.Line31.Y2 = 4.559!
        '
        'Line32
        '
        Me.Line32.Height = 0!
        Me.Line32.Left = 0.75!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 4.359!
        Me.Line32.Width = 6.25!
        Me.Line32.X1 = 0.75!
        Me.Line32.X2 = 7.0!
        Me.Line32.Y1 = 4.359!
        Me.Line32.Y2 = 4.359!
        '
        'Line33
        '
        Me.Line33.Height = 0!
        Me.Line33.Left = 0.75!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 4.159!
        Me.Line33.Width = 6.25!
        Me.Line33.X1 = 0.75!
        Me.Line33.X2 = 7.0!
        Me.Line33.Y1 = 4.159!
        Me.Line33.Y2 = 4.159!
        '
        'Line34
        '
        Me.Line34.Height = 0.002999783!
        Me.Line34.Left = 0.75!
        Me.Line34.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line34.LineWeight = 1.0!
        Me.Line34.Name = "Line34"
        Me.Line34.Top = 3.959!
        Me.Line34.Width = 6.25!
        Me.Line34.X1 = 0.75!
        Me.Line34.X2 = 7.0!
        Me.Line34.Y1 = 3.962!
        Me.Line34.Y2 = 3.959!
        '
        'Line35
        '
        Me.Line35.Height = 0!
        Me.Line35.Left = 0.75!
        Me.Line35.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line35.LineWeight = 1.0!
        Me.Line35.Name = "Line35"
        Me.Line35.Top = 3.759!
        Me.Line35.Width = 6.25!
        Me.Line35.X1 = 0.75!
        Me.Line35.X2 = 7.0!
        Me.Line35.Y1 = 3.759!
        Me.Line35.Y2 = 3.759!
        '
        'Line38
        '
        Me.Line38.Height = 1.6!
        Me.Line38.Left = 1.75!
        Me.Line38.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line38.LineWeight = 1.0!
        Me.Line38.Name = "Line38"
        Me.Line38.Top = 5.962!
        Me.Line38.Width = 0!
        Me.Line38.X1 = 1.75!
        Me.Line38.X2 = 1.75!
        Me.Line38.Y1 = 5.962!
        Me.Line38.Y2 = 7.562!
        '
        'Line40
        '
        Me.Line40.Height = 1.403!
        Me.Line40.Left = 1.75!
        Me.Line40.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line40.LineWeight = 1.0!
        Me.Line40.Name = "Line40"
        Me.Line40.Top = 4.159!
        Me.Line40.Width = 0!
        Me.Line40.X1 = 1.75!
        Me.Line40.X2 = 1.75!
        Me.Line40.Y1 = 4.159!
        Me.Line40.Y2 = 5.562!
        '
        'Line42
        '
        Me.Line42.Height = 0.2029989!
        Me.Line42.Left = 1.75!
        Me.Line42.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line42.LineWeight = 1.0!
        Me.Line42.Name = "Line42"
        Me.Line42.Top = 3.756!
        Me.Line42.Width = 0.0000009536743!
        Me.Line42.X1 = 1.75!
        Me.Line42.X2 = 1.750001!
        Me.Line42.Y1 = 3.756!
        Me.Line42.Y2 = 3.958999!
        '
        'Label71
        '
        Me.Label71.Height = 0.2!
        Me.Label71.HyperLink = Nothing
        Me.Label71.Left = 1.75!
        Me.Label71.Name = "Label71"
        Me.Label71.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label71.Text = "1.0 Sec Spectral Accel, S1="
        Me.Label71.Top = 4.562!
        Me.Label71.Width = 3.5!
        '
        'Label73
        '
        Me.Label73.Height = 0.2!
        Me.Label73.HyperLink = Nothing
        Me.Label73.Left = 0.7500008!
        Me.Label73.Name = "Label73"
        Me.Label73.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label73.Text = "3"
        Me.Label73.Top = 6.362001!
        Me.Label73.Width = 1.0!
        '
        'Label74
        '
        Me.Label74.Height = 0.2!
        Me.Label74.HyperLink = Nothing
        Me.Label74.Left = 1.75!
        Me.Label74.Name = "Label74"
        Me.Label74.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label74.Text = "Overstrength Factor, Omega="
        Me.Label74.Top = 6.362001!
        Me.Label74.Width = 3.5!
        '
        'Line41
        '
        Me.Line41.Height = 0.2029989!
        Me.Line41.Left = 5.25!
        Me.Line41.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line41.LineWeight = 1.0!
        Me.Line41.Name = "Line41"
        Me.Line41.Top = 3.759!
        Me.Line41.Width = 0.0000009536743!
        Me.Line41.X1 = 5.25!
        Me.Line41.X2 = 5.250001!
        Me.Line41.Y1 = 3.759!
        Me.Line41.Y2 = 3.961999!
        '
        'Line44
        '
        Me.Line44.Height = 0.2029992!
        Me.Line44.Left = 5.25!
        Me.Line44.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line44.LineWeight = 1.0!
        Me.Line44.Name = "Line44"
        Me.Line44.Top = 0.423!
        Me.Line44.Width = 0.0000009536743!
        Me.Line44.X1 = 5.25!
        Me.Line44.X2 = 5.250001!
        Me.Line44.Y1 = 0.423!
        Me.Line44.Y2 = 0.6259992!
        '
        'Line45
        '
        Me.Line45.Height = 1.403!
        Me.Line45.Left = 5.25!
        Me.Line45.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line45.LineWeight = 1.0!
        Me.Line45.Name = "Line45"
        Me.Line45.Top = 4.159!
        Me.Line45.Width = 0!
        Me.Line45.X1 = 5.25!
        Me.Line45.X2 = 5.25!
        Me.Line45.Y1 = 4.159!
        Me.Line45.Y2 = 5.562!
        '
        'Label57
        '
        Me.Label57.Height = 0.2!
        Me.Label57.HyperLink = Nothing
        Me.Label57.Left = 0.750001!
        Me.Label57.Name = "Label57"
        Me.Label57.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label57.Text = "7"
        Me.Label57.Top = 5.359!
        Me.Label57.Width = 1.0!
        '
        'Label58
        '
        Me.Label58.Height = 0.2!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 1.75!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label58.Text = "Design Spectral Resp. Accel at Short Period, Sds="
        Me.Label58.Top = 5.359!
        Me.Label58.Width = 3.500001!
        '
        'Label76
        '
        Me.Label76.Height = 0.2!
        Me.Label76.HyperLink = Nothing
        Me.Label76.Left = 0.7500014!
        Me.Label76.Name = "Label76"
        Me.Label76.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label76.Text = "5"
        Me.Label76.Top = 6.762!
        Me.Label76.Width = 1.0!
        '
        'Label77
        '
        Me.Label77.Height = 0.2!
        Me.Label77.HyperLink = Nothing
        Me.Label77.Left = 1.750001!
        Me.Label77.Name = "Label77"
        Me.Label77.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label77.Text = "Occupancy Importance, I="
        Me.Label77.Top = 6.762!
        Me.Label77.Width = 3.5!
        '
        'Label79
        '
        Me.Label79.Height = 0.2!
        Me.Label79.HyperLink = Nothing
        Me.Label79.Left = 0.7500006!
        Me.Label79.Name = "Label79"
        Me.Label79.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label79.Text = "6"
        Me.Label79.Top = 6.962!
        Me.Label79.Width = 1.0!
        '
        'Label80
        '
        Me.Label80.Height = 0.2!
        Me.Label80.HyperLink = Nothing
        Me.Label80.Left = 1.75!
        Me.Label80.Name = "Label80"
        Me.Label80.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label80.Text = "Redundancy Factor, Rho="
        Me.Label80.Top = 6.962!
        Me.Label80.Width = 3.5!
        '
        'Label82
        '
        Me.Label82.Height = 0.2!
        Me.Label82.HyperLink = Nothing
        Me.Label82.Left = 0.7500009!
        Me.Label82.Name = "Label82"
        Me.Label82.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label82.Text = "7"
        Me.Label82.Top = 7.162!
        Me.Label82.Width = 1.0!
        '
        'Label83
        '
        Me.Label83.Height = 0.2!
        Me.Label83.HyperLink = Nothing
        Me.Label83.Left = 1.75!
        Me.Label83.Name = "Label83"
        Me.Label83.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label83.Text = "Live Load Factor, f1="
        Me.Label83.Top = 7.162!
        Me.Label83.Width = 3.5!
        '
        'Label85
        '
        Me.Label85.Height = 0.2!
        Me.Label85.HyperLink = Nothing
        Me.Label85.Left = 0.7500011!
        Me.Label85.Name = "Label85"
        Me.Label85.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label85.Text = "8"
        Me.Label85.Top = 7.362!
        Me.Label85.Width = 1.0!
        '
        'Label86
        '
        Me.Label86.Height = 0.2!
        Me.Label86.HyperLink = Nothing
        Me.Label86.Left = 1.750001!
        Me.Label86.Name = "Label86"
        Me.Label86.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label86.Text = "Snow Load Factor, f2="
        Me.Label86.Top = 7.362!
        Me.Label86.Width = 3.5!
        '
        'Line26
        '
        Me.Line26.Height = 0!
        Me.Line26.Left = 0.7500005!
        Me.Line26.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line26.LineWeight = 1.0!
        Me.Line26.Name = "Line26"
        Me.Line26.Top = 7.562!
        Me.Line26.Width = 6.25!
        Me.Line26.X1 = 0.7500005!
        Me.Line26.X2 = 7.0!
        Me.Line26.Y1 = 7.562!
        Me.Line26.Y2 = 7.562!
        '
        'Line47
        '
        Me.Line47.Height = 0!
        Me.Line47.Left = 0.7500005!
        Me.Line47.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line47.LineWeight = 1.0!
        Me.Line47.Name = "Line47"
        Me.Line47.Top = 7.162!
        Me.Line47.Width = 6.25!
        Me.Line47.X1 = 0.7500005!
        Me.Line47.X2 = 7.0!
        Me.Line47.Y1 = 7.162!
        Me.Line47.Y2 = 7.162!
        '
        'Line48
        '
        Me.Line48.Height = 0!
        Me.Line48.Left = 0.7500004!
        Me.Line48.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line48.LineWeight = 1.0!
        Me.Line48.Name = "Line48"
        Me.Line48.Top = 6.962!
        Me.Line48.Width = 6.25!
        Me.Line48.X1 = 0.7500004!
        Me.Line48.X2 = 7.0!
        Me.Line48.Y1 = 6.962!
        Me.Line48.Y2 = 6.962!
        '
        'Line49
        '
        Me.Line49.Height = 0!
        Me.Line49.Left = 0.7500004!
        Me.Line49.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line49.LineWeight = 1.0!
        Me.Line49.Name = "Line49"
        Me.Line49.Top = 6.562!
        Me.Line49.Width = 6.25!
        Me.Line49.X1 = 0.7500004!
        Me.Line49.X2 = 7.0!
        Me.Line49.Y1 = 6.562!
        Me.Line49.Y2 = 6.562!
        '
        'Line50
        '
        Me.Line50.Height = 0!
        Me.Line50.Left = 0.7500001!
        Me.Line50.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line50.LineWeight = 1.0!
        Me.Line50.Name = "Line50"
        Me.Line50.Top = 5.562!
        Me.Line50.Width = 6.25!
        Me.Line50.X1 = 0.7500001!
        Me.Line50.X2 = 7.0!
        Me.Line50.Y1 = 5.562!
        Me.Line50.Y2 = 5.562!
        '
        'Label13
        '
        Me.Label13.Height = 0.2!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0.7500003!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label13.Text = "1"
        Me.Label13.Top = 7.765001!
        Me.Label13.Width = 1.0!
        '
        'Label20
        '
        Me.Label20.Height = 0.2!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 1.75!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label20.Text = "Design Code="
        Me.Label20.Top = 7.765001!
        Me.Label20.Width = 3.5!
        '
        'lbDCode1
        '
        Me.lbDCode1.Height = 0.2!
        Me.lbDCode1.HyperLink = Nothing
        Me.lbDCode1.Left = 5.25!
        Me.lbDCode1.Name = "lbDCode1"
        Me.lbDCode1.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbDCode1.Text = "Value"
        Me.lbDCode1.Top = 7.765001!
        Me.lbDCode1.Width = 1.75!
        '
        'Line43
        '
        Me.Line43.Height = 0!
        Me.Line43.Left = 0.7500003!
        Me.Line43.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line43.LineWeight = 1.0!
        Me.Line43.Name = "Line43"
        Me.Line43.Top = 7.765001!
        Me.Line43.Width = 6.25!
        Me.Line43.X1 = 0.7500003!
        Me.Line43.X2 = 7.0!
        Me.Line43.Y1 = 7.765001!
        Me.Line43.Y2 = 7.765001!
        '
        'Line46
        '
        Me.Line46.Height = 0!
        Me.Line46.Left = 0.7500004!
        Me.Line46.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line46.LineWeight = 1.0!
        Me.Line46.Name = "Line46"
        Me.Line46.Top = 7.362!
        Me.Line46.Width = 6.25!
        Me.Line46.X1 = 0.7500004!
        Me.Line46.X2 = 7.0!
        Me.Line46.Y1 = 7.362!
        Me.Line46.Y2 = 7.362!
        '
        'Line51
        '
        Me.Line51.Height = 0.3869996!
        Me.Line51.Left = 1.75!
        Me.Line51.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line51.LineWeight = 1.0!
        Me.Line51.Name = "Line51"
        Me.Line51.Top = 7.765001!
        Me.Line51.Width = 0!
        Me.Line51.X1 = 1.75!
        Me.Line51.X2 = 1.75!
        Me.Line51.Y1 = 7.765001!
        Me.Line51.Y2 = 8.152!
        '
        'Line55
        '
        Me.Line55.Height = 0.3980002!
        Me.Line55.Left = 5.25!
        Me.Line55.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line55.LineWeight = 1.0!
        Me.Line55.Name = "Line55"
        Me.Line55.Top = 7.765!
        Me.Line55.Width = 0!
        Me.Line55.X1 = 5.25!
        Me.Line55.X2 = 5.25!
        Me.Line55.Y1 = 7.765!
        Me.Line55.Y2 = 8.163!
        '
        'Label29
        '
        Me.Label29.Height = 0.2!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0.74!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label29.Text = "No." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13)
        Me.Label29.Top = 8.679001!
        Me.Label29.Width = 1.0!
        '
        'Label30
        '
        Me.Label30.Height = 0.2!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 1.74!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label30.Text = "Item"
        Me.Label30.Top = 8.679001!
        Me.Label30.Width = 3.5!
        '
        'Label33
        '
        Me.Label33.Height = 0.2!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 5.24!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 9pt; font-weight: bold; text-align: center; vertical-align: middle; dd" &
    "o-char-set: 1"
        Me.Label33.Text = "Data"
        Me.Label33.Top = 8.679001!
        Me.Label33.Width = 1.75!
        '
        'Label43
        '
        Me.Label43.Height = 0.2!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 0.7500001!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label43.Text = "2"
        Me.Label43.Top = 9.487001!
        Me.Label43.Width = 1.0!
        '
        'Label46
        '
        Me.Label46.Height = 0.2!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 1.75!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label46.Text = "Wind Speed (mph)="
        Me.Label46.Top = 9.487001!
        Me.Label46.Width = 3.5!
        '
        'Label53
        '
        Me.Label53.Height = 0.2!
        Me.Label53.HyperLink = Nothing
        Me.Label53.Left = 0.7500001!
        Me.Label53.Name = "Label53"
        Me.Label53.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label53.Text = "3"
        Me.Label53.Top = 9.687001!
        Me.Label53.Width = 1.0!
        '
        'Label56
        '
        Me.Label56.Height = 0.2!
        Me.Label56.HyperLink = Nothing
        Me.Label56.Left = 1.75!
        Me.Label56.Name = "Label56"
        Me.Label56.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label56.Text = "Exposure Type="
        Me.Label56.Top = 9.687001!
        Me.Label56.Width = 3.5!
        '
        'Label63
        '
        Me.Label63.Height = 0.2!
        Me.Label63.HyperLink = Nothing
        Me.Label63.Left = 0.7500001!
        Me.Label63.Name = "Label63"
        Me.Label63.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label63.Text = "5"
        Me.Label63.Top = 10.087!
        Me.Label63.Width = 1.0!
        '
        'Label66
        '
        Me.Label66.Height = 0.2!
        Me.Label66.HyperLink = Nothing
        Me.Label66.Left = 1.75!
        Me.Label66.Name = "Label66"
        Me.Label66.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label66.Text = "Topographical Factor, Kzt="
        Me.Label66.Top = 10.087!
        Me.Label66.Width = 3.5!
        '
        'Label72
        '
        Me.Label72.Height = 0.2!
        Me.Label72.HyperLink = Nothing
        Me.Label72.Left = 0.7500001!
        Me.Label72.Name = "Label72"
        Me.Label72.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label72.Text = "6"
        Me.Label72.Top = 10.287!
        Me.Label72.Width = 1.0!
        '
        'Label75
        '
        Me.Label75.Height = 0.2!
        Me.Label75.HyperLink = Nothing
        Me.Label75.Left = 1.75!
        Me.Label75.Name = "Label75"
        Me.Label75.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label75.Text = "Gust Factor="
        Me.Label75.Top = 10.287!
        Me.Label75.Width = 3.5!
        '
        'Label81
        '
        Me.Label81.Height = 0.2!
        Me.Label81.HyperLink = Nothing
        Me.Label81.Left = 0.7500001!
        Me.Label81.Name = "Label81"
        Me.Label81.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label81.Text = "7"
        Me.Label81.Top = 10.487!
        Me.Label81.Width = 1.0!
        '
        'Label84
        '
        Me.Label84.Height = 0.2!
        Me.Label84.HyperLink = Nothing
        Me.Label84.Left = 1.75!
        Me.Label84.Name = "Label84"
        Me.Label84.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label84.Text = "Directionality Factor, Kd="
        Me.Label84.Top = 10.487!
        Me.Label84.Width = 3.5!
        '
        'Line58
        '
        Me.Line58.Height = 0!
        Me.Line58.Left = 0.7500001!
        Me.Line58.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line58.LineWeight = 1.0!
        Me.Line58.Name = "Line58"
        Me.Line58.Top = 10.687!
        Me.Line58.Width = 6.25!
        Me.Line58.X1 = 0.7500001!
        Me.Line58.X2 = 7.0!
        Me.Line58.Y1 = 10.687!
        Me.Line58.Y2 = 10.687!
        '
        'Line59
        '
        Me.Line59.Height = 0!
        Me.Line59.Left = 0.7500001!
        Me.Line59.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line59.LineWeight = 1.0!
        Me.Line59.Name = "Line59"
        Me.Line59.Top = 10.487!
        Me.Line59.Width = 6.25!
        Me.Line59.X1 = 0.7500001!
        Me.Line59.X2 = 7.0!
        Me.Line59.Y1 = 10.487!
        Me.Line59.Y2 = 10.487!
        '
        'Line60
        '
        Me.Line60.Height = 0!
        Me.Line60.Left = 0.7500001!
        Me.Line60.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line60.LineWeight = 1.0!
        Me.Line60.Name = "Line60"
        Me.Line60.Top = 10.287!
        Me.Line60.Width = 6.25!
        Me.Line60.X1 = 0.7500001!
        Me.Line60.X2 = 7.0!
        Me.Line60.Y1 = 10.287!
        Me.Line60.Y2 = 10.287!
        '
        'Line61
        '
        Me.Line61.Height = 0.003000259!
        Me.Line61.Left = 0.7500001!
        Me.Line61.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line61.LineWeight = 1.0!
        Me.Line61.Name = "Line61"
        Me.Line61.Top = 10.087!
        Me.Line61.Width = 6.25!
        Me.Line61.X1 = 0.7500001!
        Me.Line61.X2 = 7.0!
        Me.Line61.Y1 = 10.09!
        Me.Line61.Y2 = 10.087!
        '
        'Line62
        '
        Me.Line62.Height = 0.003000259!
        Me.Line62.Left = 0.7500001!
        Me.Line62.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line62.LineWeight = 1.0!
        Me.Line62.Name = "Line62"
        Me.Line62.Top = 9.887001!
        Me.Line62.Width = 6.25!
        Me.Line62.X1 = 0.7500001!
        Me.Line62.X2 = 7.0!
        Me.Line62.Y1 = 9.890001!
        Me.Line62.Y2 = 9.887001!
        '
        'Line63
        '
        Me.Line63.Height = 0!
        Me.Line63.Left = 0.7500001!
        Me.Line63.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line63.LineWeight = 1.0!
        Me.Line63.Name = "Line63"
        Me.Line63.Top = 9.687001!
        Me.Line63.Width = 6.25!
        Me.Line63.X1 = 0.7500001!
        Me.Line63.X2 = 7.0!
        Me.Line63.Y1 = 9.687001!
        Me.Line63.Y2 = 9.687001!
        '
        'Line64
        '
        Me.Line64.Height = 0!
        Me.Line64.Left = 0.7500001!
        Me.Line64.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line64.LineWeight = 1.0!
        Me.Line64.Name = "Line64"
        Me.Line64.Top = 9.487001!
        Me.Line64.Width = 6.25!
        Me.Line64.X1 = 0.7500001!
        Me.Line64.X2 = 7.0!
        Me.Line64.Y1 = 9.487001!
        Me.Line64.Y2 = 9.487001!
        '
        'Line65
        '
        Me.Line65.Height = 0.003000259!
        Me.Line65.Left = 0.74!
        Me.Line65.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line65.LineWeight = 1.0!
        Me.Line65.Name = "Line65"
        Me.Line65.Top = 8.879001!
        Me.Line65.Width = 6.25!
        Me.Line65.X1 = 0.74!
        Me.Line65.X2 = 6.99!
        Me.Line65.Y1 = 8.882001!
        Me.Line65.Y2 = 8.879001!
        '
        'Line66
        '
        Me.Line66.Height = 0!
        Me.Line66.Left = 0.74!
        Me.Line66.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line66.LineWeight = 1.0!
        Me.Line66.Name = "Line66"
        Me.Line66.Top = 8.679001!
        Me.Line66.Width = 6.25!
        Me.Line66.X1 = 0.74!
        Me.Line66.X2 = 6.99!
        Me.Line66.Y1 = 8.679001!
        Me.Line66.Y2 = 8.679001!
        '
        'Line70
        '
        Me.Line70.Height = 1.397999!
        Me.Line70.Left = 1.75!
        Me.Line70.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line70.LineWeight = 1.0!
        Me.Line70.Name = "Line70"
        Me.Line70.Top = 9.289001!
        Me.Line70.Width = 0!
        Me.Line70.X1 = 1.75!
        Me.Line70.X2 = 1.75!
        Me.Line70.Y1 = 9.289001!
        Me.Line70.Y2 = 10.687!
        '
        'Line71
        '
        Me.Line71.Height = 0.2029982!
        Me.Line71.Left = 1.74!
        Me.Line71.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line71.LineWeight = 1.0!
        Me.Line71.Name = "Line71"
        Me.Line71.Top = 8.676002!
        Me.Line71.Width = 0.0000009536743!
        Me.Line71.X1 = 1.74!
        Me.Line71.X2 = 1.740001!
        Me.Line71.Y1 = 8.676002!
        Me.Line71.Y2 = 8.879!
        '
        'Label98
        '
        Me.Label98.Height = 0.2!
        Me.Label98.HyperLink = Nothing
        Me.Label98.Left = 0.7500001!
        Me.Label98.Name = "Label98"
        Me.Label98.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label98.Text = "4"
        Me.Label98.Top = 9.890001!
        Me.Label98.Width = 1.0!
        '
        'Label99
        '
        Me.Label99.Height = 0.2!
        Me.Label99.HyperLink = Nothing
        Me.Label99.Left = 1.75!
        Me.Label99.Name = "Label99"
        Me.Label99.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label99.Text = "Ground Elevation Factor="
        Me.Label99.Top = 9.890001!
        Me.Label99.Width = 3.5!
        '
        'Line72
        '
        Me.Line72.Height = 0.1999989!
        Me.Line72.Left = 5.24!
        Me.Line72.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line72.LineWeight = 1.0!
        Me.Line72.Name = "Line72"
        Me.Line72.Top = 8.679001!
        Me.Line72.Width = 0!
        Me.Line72.X1 = 5.24!
        Me.Line72.X2 = 5.24!
        Me.Line72.Y1 = 8.679001!
        Me.Line72.Y2 = 8.879!
        '
        'Line73
        '
        Me.Line73.Height = 1.629!
        Me.Line73.Left = 5.24!
        Me.Line73.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line73.LineWeight = 1.0!
        Me.Line73.Name = "Line73"
        Me.Line73.Top = 9.058001!
        Me.Line73.Width = 0!
        Me.Line73.X1 = 5.24!
        Me.Line73.X2 = 5.24!
        Me.Line73.Y1 = 9.058001!
        Me.Line73.Y2 = 10.687!
        '
        'Line53
        '
        Me.Line53.Height = 2.005!
        Me.Line53.Left = 0.74!
        Me.Line53.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line53.LineWeight = 1.0!
        Me.Line53.Name = "Line53"
        Me.Line53.Top = 8.679!
        Me.Line53.Width = 0!
        Me.Line53.X1 = 0.74!
        Me.Line53.X2 = 0.74!
        Me.Line53.Y1 = 8.679!
        Me.Line53.Y2 = 10.684!
        '
        'Label4
        '
        Me.Label4.Height = 0.2!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 5.25!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "color: Black; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" &
    "gn: middle; ddo-char-set: 0"
        Me.Label4.Text = "X-Dir"
        Me.Label4.Top = 5.762!
        Me.Label4.Width = 0.875!
        '
        'Label23
        '
        Me.Label23.Height = 0.2!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 6.125!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "color: Black; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" &
    "gn: middle; ddo-char-set: 0"
        Me.Label23.Text = "Y-Dir"
        Me.Label23.Top = 5.762!
        Me.Label23.Width = 0.875!
        '
        'Line36
        '
        Me.Line36.Height = 4.404!
        Me.Line36.Left = 7.0!
        Me.Line36.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line36.LineWeight = 1.0!
        Me.Line36.Name = "Line36"
        Me.Line36.Top = 3.759!
        Me.Line36.Width = 0!
        Me.Line36.X1 = 7.0!
        Me.Line36.X2 = 7.0!
        Me.Line36.Y1 = 3.759!
        Me.Line36.Y2 = 8.163!
        '
        'Label47
        '
        Me.Label47.Height = 0.2!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 0.7500001!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label47.Text = "3"
        Me.Label47.Top = 4.562!
        Me.Label47.Width = 1.0!
        '
        'Line39
        '
        Me.Line39.Height = 4.404!
        Me.Line39.Left = 0.7500001!
        Me.Line39.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line39.LineWeight = 1.0!
        Me.Line39.Name = "Line39"
        Me.Line39.Top = 3.759!
        Me.Line39.Width = 0.0000003576279!
        Me.Line39.X1 = 0.7500005!
        Me.Line39.X2 = 0.7500001!
        Me.Line39.Y1 = 3.759!
        Me.Line39.Y2 = 8.163!
        '
        'Line57
        '
        Me.Line57.Height = 0!
        Me.Line57.Left = 0.7500003!
        Me.Line57.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line57.LineWeight = 1.0!
        Me.Line57.Name = "Line57"
        Me.Line57.Top = 5.762!
        Me.Line57.Width = 6.25!
        Me.Line57.X1 = 0.7500003!
        Me.Line57.X2 = 7.0!
        Me.Line57.Y1 = 5.762!
        Me.Line57.Y2 = 5.762!
        '
        'Line37
        '
        Me.Line37.Height = 1.8!
        Me.Line37.Left = 5.25!
        Me.Line37.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line37.LineWeight = 1.0!
        Me.Line37.Name = "Line37"
        Me.Line37.Top = 5.762!
        Me.Line37.Width = 0!
        Me.Line37.X1 = 5.25!
        Me.Line37.X2 = 5.25!
        Me.Line37.Y1 = 5.762!
        Me.Line37.Y2 = 7.562!
        '
        'lbFactor1_Y
        '
        Me.lbFactor1_Y.Height = 0.2!
        Me.lbFactor1_Y.HyperLink = Nothing
        Me.lbFactor1_Y.Left = 6.125!
        Me.lbFactor1_Y.Name = "lbFactor1_Y"
        Me.lbFactor1_Y.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor1_Y.Text = "Value"
        Me.lbFactor1_Y.Top = 5.962!
        Me.lbFactor1_Y.Width = 0.875!
        '
        'lbFactor2_Y
        '
        Me.lbFactor2_Y.Height = 0.2!
        Me.lbFactor2_Y.HyperLink = Nothing
        Me.lbFactor2_Y.Left = 6.125!
        Me.lbFactor2_Y.Name = "lbFactor2_Y"
        Me.lbFactor2_Y.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor2_Y.Text = "Value"
        Me.lbFactor2_Y.Top = 6.162!
        Me.lbFactor2_Y.Width = 0.875!
        '
        'lbFactor3_Y
        '
        Me.lbFactor3_Y.Height = 0.2!
        Me.lbFactor3_Y.HyperLink = Nothing
        Me.lbFactor3_Y.Left = 6.125!
        Me.lbFactor3_Y.Name = "lbFactor3_Y"
        Me.lbFactor3_Y.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor3_Y.Text = "Value"
        Me.lbFactor3_Y.Top = 6.362001!
        Me.lbFactor3_Y.Width = 0.875!
        '
        'lbFactor4_Y
        '
        Me.lbFactor4_Y.Height = 0.2!
        Me.lbFactor4_Y.HyperLink = Nothing
        Me.lbFactor4_Y.Left = 6.125!
        Me.lbFactor4_Y.Name = "lbFactor4_Y"
        Me.lbFactor4_Y.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor4_Y.Text = "Value"
        Me.lbFactor4_Y.Top = 6.562!
        Me.lbFactor4_Y.Width = 0.875!
        '
        'lbFactor5_Y
        '
        Me.lbFactor5_Y.Height = 0.2!
        Me.lbFactor5_Y.HyperLink = Nothing
        Me.lbFactor5_Y.Left = 6.125001!
        Me.lbFactor5_Y.Name = "lbFactor5_Y"
        Me.lbFactor5_Y.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor5_Y.Text = "Value"
        Me.lbFactor5_Y.Top = 6.762!
        Me.lbFactor5_Y.Width = 0.875!
        '
        'lbFactor6_Y
        '
        Me.lbFactor6_Y.Height = 0.2!
        Me.lbFactor6_Y.HyperLink = Nothing
        Me.lbFactor6_Y.Left = 6.125!
        Me.lbFactor6_Y.Name = "lbFactor6_Y"
        Me.lbFactor6_Y.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbFactor6_Y.Text = "Value"
        Me.lbFactor6_Y.Top = 6.962!
        Me.lbFactor6_Y.Width = 0.875!
        '
        'Line56
        '
        Me.Line56.Height = 1.4!
        Me.Line56.Left = 6.125001!
        Me.Line56.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line56.LineWeight = 1.0!
        Me.Line56.Name = "Line56"
        Me.Line56.Top = 5.762!
        Me.Line56.Width = 0!
        Me.Line56.X1 = 6.125001!
        Me.Line56.X2 = 6.125001!
        Me.Line56.Y1 = 5.762!
        Me.Line56.Y2 = 7.162!
        '
        'Label96
        '
        Me.Label96.Height = 0.2!
        Me.Label96.HyperLink = Nothing
        Me.Label96.Left = 5.24!
        Me.Label96.Name = "Label96"
        Me.Label96.Style = "color: Black; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" &
    "gn: middle; ddo-char-set: 0"
        Me.Label96.Text = "X-Dir"
        Me.Label96.Top = 9.079!
        Me.Label96.Width = 0.875!
        '
        'Label97
        '
        Me.Label97.Height = 0.2!
        Me.Label97.HyperLink = Nothing
        Me.Label97.Left = 6.115!
        Me.Label97.Name = "Label97"
        Me.Label97.Style = "color: Black; font-size: 9pt; font-weight: bold; text-align: center; vertical-ali" &
    "gn: middle; ddo-char-set: 0"
        Me.Label97.Text = "Y-Dir"
        Me.Label97.Top = 9.079!
        Me.Label97.Width = 0.875!
        '
        'Line68
        '
        Me.Line68.Height = 0!
        Me.Line68.Left = 0.7400004!
        Me.Line68.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line68.LineWeight = 1.0!
        Me.Line68.Name = "Line68"
        Me.Line68.Top = 9.079!
        Me.Line68.Width = 6.25!
        Me.Line68.X1 = 0.7400004!
        Me.Line68.X2 = 6.99!
        Me.Line68.Y1 = 9.079!
        Me.Line68.Y2 = 9.079!
        '
        'Line52
        '
        Me.Line52.Height = 2.019!
        Me.Line52.Left = 6.99!
        Me.Line52.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line52.LineWeight = 1.0!
        Me.Line52.Name = "Line52"
        Me.Line52.Top = 8.679!
        Me.Line52.Width = 0!
        Me.Line52.X1 = 6.99!
        Me.Line52.X2 = 6.99!
        Me.Line52.Y1 = 8.679!
        Me.Line52.Y2 = 10.698!
        '
        'Line67
        '
        Me.Line67.Height = 0.1999989!
        Me.Line67.Left = 6.115!
        Me.Line67.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line67.LineWeight = 1.0!
        Me.Line67.Name = "Line67"
        Me.Line67.Top = 9.079!
        Me.Line67.Width = 0!
        Me.Line67.X1 = 6.115!
        Me.Line67.X2 = 6.115!
        Me.Line67.Y1 = 9.079!
        Me.Line67.Y2 = 9.278999!
        '
        'Label26
        '
        Me.Label26.Height = 0.2!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 0.7500001!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label26.Text = "2"
        Me.Label26.Top = 7.973001!
        Me.Label26.Width = 1.0!
        '
        'Label50
        '
        Me.Label50.Height = 0.2!
        Me.Label50.HyperLink = Nothing
        Me.Label50.Left = 1.75!
        Me.Label50.Name = "Label50"
        Me.Label50.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label50.Text = "Design Frame Type="
        Me.Label50.Top = 7.973001!
        Me.Label50.Width = 3.5!
        '
        'Label59
        '
        Me.Label59.Height = 0.2!
        Me.Label59.HyperLink = Nothing
        Me.Label59.Left = 5.25!
        Me.Label59.Name = "Label59"
        Me.Label59.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.Label59.Text = "OMF"
        Me.Label59.Top = 7.973001!
        Me.Label59.Width = 1.75!
        '
        'Line69
        '
        Me.Line69.Height = 0!
        Me.Line69.Left = 0.7500001!
        Me.Line69.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line69.LineWeight = 1.0!
        Me.Line69.Name = "Line69"
        Me.Line69.Top = 7.973001!
        Me.Line69.Width = 6.25!
        Me.Line69.X1 = 0.7500001!
        Me.Line69.X2 = 7.0!
        Me.Line69.Y1 = 7.973001!
        Me.Line69.Y2 = 7.973001!
        '
        'Line74
        '
        Me.Line74.Height = 0!
        Me.Line74.Left = 0.7500001!
        Me.Line74.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line74.LineWeight = 1.0!
        Me.Line74.Name = "Line74"
        Me.Line74.Top = 8.162001!
        Me.Line74.Width = 6.249999!
        Me.Line74.X1 = 0.7500001!
        Me.Line74.X2 = 6.999999!
        Me.Line74.Y1 = 8.162001!
        Me.Line74.Y2 = 8.162001!
        '
        'lbWcode
        '
        Me.lbWcode.Height = 0.2!
        Me.lbWcode.HyperLink = Nothing
        Me.lbWcode.Left = 5.25!
        Me.lbWcode.Name = "lbWcode"
        Me.lbWcode.Style = "color: Blue; font-size: 9pt; font-weight: normal; text-align: center; vertical-al" &
    "ign: middle; ddo-char-set: 1"
        Me.lbWcode.Text = "Value"
        Me.lbWcode.Top = 9.290001!
        Me.lbWcode.Width = 1.75!
        '
        'Label78
        '
        Me.Label78.Height = 0.2!
        Me.Label78.HyperLink = Nothing
        Me.Label78.Left = 0.7500001!
        Me.Label78.Name = "Label78"
        Me.Label78.Style = "font-size: 9pt; font-weight: normal; text-align: center; vertical-align: middle; " &
    "ddo-char-set: 1"
        Me.Label78.Text = "1"
        Me.Label78.Top = 9.290001!
        Me.Label78.Width = 1.0!
        '
        'Label87
        '
        Me.Label87.Height = 0.2!
        Me.Label87.HyperLink = Nothing
        Me.Label87.Left = 1.75!
        Me.Label87.Name = "Label87"
        Me.Label87.Style = "font-size: 9pt; font-weight: normal; text-align: right; vertical-align: middle; d" &
    "do-char-set: 1"
        Me.Label87.Text = "Standard="
        Me.Label87.Top = 9.290001!
        Me.Label87.Width = 3.5!
        '
        'Line54
        '
        Me.Line54.Height = 0!
        Me.Line54.Left = 0.7500001!
        Me.Line54.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line54.LineWeight = 1.0!
        Me.Line54.Name = "Line54"
        Me.Line54.Top = 9.290001!
        Me.Line54.Width = 6.25!
        Me.Line54.X1 = 0.7500001!
        Me.Line54.X2 = 7.0!
        Me.Line54.Y1 = 9.290001!
        Me.Line54.Y2 = 9.290001!
        '
        'GroupHeader1
        '
        Me.GroupHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.lbJobID, Me.lbJobName, Me.Label2, Me.Label3, Me.lbPrintedDate})
        Me.GroupHeader1.Height = 0.625!
        Me.GroupHeader1.Name = "GroupHeader1"
        '
        'lbJobID
        '
        Me.lbJobID.Height = 0.2!
        Me.lbJobID.HyperLink = Nothing
        Me.lbJobID.Left = 0.5!
        Me.lbJobID.Name = "lbJobID"
        Me.lbJobID.Style = "font-size: 9.75pt; font-weight: normal"
        Me.lbJobID.Text = "ES-123456" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobID.Top = 0.187!
        Me.lbJobID.Width = 1.75!
        '
        'lbJobName
        '
        Me.lbJobName.Height = 0.2!
        Me.lbJobName.HyperLink = Nothing
        Me.lbJobName.Left = 0.75!
        Me.lbJobName.Name = "lbJobName"
        Me.lbJobName.Style = "font-size: 9.75pt; font-weight: normal"
        Me.lbJobName.Text = "Test Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbJobName.Top = 0!
        Me.lbJobName.Width = 4.063!
        '
        'Label2
        '
        Me.Label2.Height = 0.2!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label2.Text = "Job ID: " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label2.Top = 0.187!
        Me.Label2.Width = 0.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.2!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 9.75pt; font-weight: normal"
        Me.Label3.Text = "Job Name:"
        Me.Label3.Top = 0!
        Me.Label3.Width = 0.75!
        '
        'lbPrintedDate
        '
        Me.lbPrintedDate.Height = 0.2!
        Me.lbPrintedDate.HyperLink = Nothing
        Me.lbPrintedDate.Left = 5.25!
        Me.lbPrintedDate.Name = "lbPrintedDate"
        Me.lbPrintedDate.Style = "font-size: 9.75pt; font-weight: normal; text-align: right"
        Me.lbPrintedDate.Text = "Date Printed: Jun 17, 2018" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.lbPrintedDate.Top = 0!
        Me.lbPrintedDate.Width = 2.25!
        '
        'GroupFooter1
        '
        Me.GroupFooter1.Height = 0.01041666!
        Me.GroupFooter1.Name = "GroupFooter1"
        '
        'PageHeader1
        '
        Me.PageHeader1.Height = 0!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'PageFooter1
        '
        Me.PageFooter1.Height = 0!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'US_Sum1V_A
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.0!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.GroupHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.GroupFooter1)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.Label36, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbKd_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbGustFactor_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbKzt_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbGroundElev_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbExposureType_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWspeed_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor1_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor2_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor3_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor4_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor5_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor6_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor7_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor8_X, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label60, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbST1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbST2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbStiff1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbStiff2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbStiff3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbStiff4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDP1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDP2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDP3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDGVName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label37, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label48, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbParam4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label51, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label55, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label61, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label62, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label64, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label65, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label67, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label68, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label70, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label71, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label73, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label74, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label57, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label76, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label77, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label79, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label80, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label82, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label83, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label85, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label86, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDCode1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label53, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label56, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label63, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label66, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label72, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label75, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label81, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label84, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label98, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label99, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor1_Y, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor2_Y, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor3_Y, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor4_Y, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor5_Y, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbFactor6_Y, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label96, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label97, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label50, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label59, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbWcode, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label78, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label87, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobID, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbJobName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbPrintedDate, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents GroupHeader1 As GrapeCity.ActiveReports.SectionReportModel.GroupHeader
    Private WithEvents GroupFooter1 As GrapeCity.ActiveReports.SectionReportModel.GroupFooter
    Private WithEvents PageHeader1 As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents PageFooter1 As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    Private WithEvents lbDGVName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobID As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbJobName As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbPrintedDate As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbST1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbST2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label14 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label19 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbStiff1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbStiff2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label15 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbDP1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label31 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbDP2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbDP3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbStiff3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbStiff4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line18 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line19 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line20 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line21 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label37 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label44 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label48 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label51 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label55 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label60 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label61 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label62 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor1_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label64 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label65 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor2_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label67 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label68 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor4_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label70 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line22 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line23 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line24 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line25 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line27 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line34 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line35 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line36 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line37 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line38 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line39 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line40 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line42 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label71 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label73 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label74 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor3_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line41 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line44 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line45 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label57 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbParam7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label76 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label77 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor5_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label79 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label80 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor6_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label82 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label83 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor7_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label85 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label86 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor8_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line26 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line46 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line47 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line48 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line49 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line50 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbDCode1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line43 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line51 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line55 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label36 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbWspeed_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label53 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label56 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbExposureType_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label63 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label66 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbKzt_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label72 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label75 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbGustFactor_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label81 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label84 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbKd_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line58 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line59 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line60 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line61 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line62 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line63 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line64 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line65 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line66 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line70 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line71 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label98 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label99 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbGroundElev_X As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line72 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line73 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line52 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line53 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line56 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line57 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents lbFactor1_Y As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor2_Y As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor3_Y As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor4_Y As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor5_Y As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbFactor6_Y As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label96 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label97 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line68 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line67 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label50 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label59 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line69 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line74 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents lbWcode As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label78 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label87 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line54 As GrapeCity.ActiveReports.SectionReportModel.Line
End Class
