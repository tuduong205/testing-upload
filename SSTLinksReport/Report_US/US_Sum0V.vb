﻿Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports YieldLinkLib

Public Class US_Sum0V

    Private job As clsMRSJob
    '
    Sub New(_job As clsMRSJob, totalpage As Integer)
        ' This call is required by the designer.
        InitializeComponent()
        '
        job = _job
        lbVersion.Text = job.versionID
        lbCompCapFor.Text = job.Name & vbNewLine & job.Address & vbNewLine & job.CityState
        lbPrepFor.Text = job.EOR
        lbDatePrinted.Text = job.PrintedDate
        lbDesignBy.Text = job.EngName & vbNewLine & job.EngEmail
        '
        ''PageInfo.Text = "Page 1 of " & totalpage
    End Sub

    Private Sub SR_p1_ReportStart(sender As Object, e As EventArgs) Handles MyBase.ReportStart
        SubReport1.Report = New US_Sum1V_A(job)
        SubReport2.Report = New US_Sum1V_B(job)
    End Sub

End Class
