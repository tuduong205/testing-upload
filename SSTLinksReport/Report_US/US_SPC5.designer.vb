﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_SPC5
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_SPC5))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S49_C1_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label501 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label502 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Picture6 = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Label503 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label504 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label506 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label508 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label509 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label511 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label512 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label514 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label515 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label517 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label518 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label520 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label521 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label523 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label524 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label526 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label527 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label529 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label530 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label532 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label533 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label535 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label536 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label538 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label540 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label541 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label542 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label543 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label545 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label546 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label548 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label549 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label551 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label552 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label554 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label555 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label557 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label558 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label560 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label561 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label563 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label564 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label566 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label567 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label569 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label572 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label21 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label25 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label26 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label30 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S49_C1_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label45 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape15 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S49_C1_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label501, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label502, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label503, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label504, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label506, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label508, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label509, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label511, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label512, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label514, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label515, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label517, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label518, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label520, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label521, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label523, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label524, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label526, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label527, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label529, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label530, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label532, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label533, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label535, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label536, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label538, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label540, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label541, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label542, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label543, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label545, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label546, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label548, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label549, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label551, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label552, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label554, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label555, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label557, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label558, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label560, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label561, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label563, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label564, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label566, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label567, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label569, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label572, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S49_C1_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S49_C1_033, Me.S49_C1_013, Me.Label501, Me.Label502, Me.Picture6, Me.Label503, Me.Label504, Me.S49_C1_001, Me.Label506, Me.S49_C1_002, Me.Label508, Me.Label509, Me.S49_C1_003, Me.Label511, Me.Label512, Me.S49_C1_004, Me.Label514, Me.Label515, Me.S49_C1_005, Me.Label517, Me.Label518, Me.S49_C1_006, Me.Label520, Me.Label521, Me.S49_C1_007, Me.Label523, Me.Label524, Me.S49_C1_008, Me.Label526, Me.Label527, Me.S49_C1_009, Me.Label529, Me.Label530, Me.S49_C1_010, Me.Label532, Me.Label533, Me.S49_C1_011, Me.Label535, Me.Label536, Me.Label538, Me.S49_C1_012, Me.Label540, Me.Label541, Me.Label542, Me.Label543, Me.S49_C1_014, Me.Label545, Me.Label546, Me.S49_C1_015, Me.Label548, Me.Label549, Me.S49_C1_016, Me.Label551, Me.Label552, Me.S49_C1_017, Me.Label554, Me.Label555, Me.S49_C1_018, Me.Label557, Me.Label558, Me.S49_C1_019, Me.Label560, Me.Label561, Me.S49_C1_020, Me.Label563, Me.Label564, Me.S49_C1_021, Me.Label566, Me.Label567, Me.S49_C1_022, Me.Label569, Me.Label572, Me.S49_C1_023, Me.Label1, Me.Label4, Me.Label6, Me.S49_C1_024, Me.Label9, Me.Label10, Me.S49_C1_025, Me.Label12, Me.Label13, Me.S49_C1_026, Me.Label16, Me.Label18, Me.S49_C1_027, Me.Label21, Me.Label23, Me.S49_C1_028, Me.Label25, Me.Label26, Me.S49_C1_029, Me.Label28, Me.Label30, Me.S49_C1_030, Me.Label33, Me.Label34, Me.S49_C1_031, Me.Label40, Me.Label42, Me.Label43, Me.S49_C1_032, Me.Label45, Me.Label46, Me.Shape15, Me.Shape1, Me.PageBreak1})
        Me.Detail.Height = 10.375!
        Me.Detail.Name = "Detail"
        '
        'S49_C1_033
        '
        Me.S49_C1_033.Height = 0.1875!
        Me.S49_C1_033.HyperLink = Nothing
        Me.S49_C1_033.Left = 2.25!
        Me.S49_C1_033.Name = "S49_C1_033"
        Me.S49_C1_033.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C1_033.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_033.Top = 10.146!
        Me.S49_C1_033.Width = 1.0!
        '
        'S49_C1_013
        '
        Me.S49_C1_013.Height = 0.1875!
        Me.S49_C1_013.HyperLink = Nothing
        Me.S49_C1_013.Left = 2.25!
        Me.S49_C1_013.Name = "S49_C1_013"
        Me.S49_C1_013.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S49_C1_013.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_013.Top = 5.463039!
        Me.S49_C1_013.Width = 1.0!
        '
        'Label501
        '
        Me.Label501.Height = 0.1875!
        Me.Label501.HyperLink = Nothing
        Me.Label501.Left = 0!
        Me.Label501.Name = "Label501"
        Me.Label501.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label501.Text = "4.9 BEAM WEB AND SHEAR TAB BLOCKSHEAR CHECK:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label501.Top = 0!
        Me.Label501.Width = 8.0!
        '
        'Label502
        '
        Me.Label502.Height = 0.1875!
        Me.Label502.HyperLink = Nothing
        Me.Label502.Left = 0.25!
        Me.Label502.Name = "Label502"
        Me.Label502.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label502.Text = "CASE 1: HORIZONTAL REACTIONS" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label502.Top = 0.187!
        Me.Label502.Width = 7.75!
        '
        'Picture6
        '
        Me.Picture6.Height = 2.625!
        Me.Picture6.HyperLink = Nothing
        Me.Picture6.ImageData = CType(resources.GetObject("Picture6.ImageData"), System.IO.Stream)
        Me.Picture6.Left = 0!
        Me.Picture6.Name = "Picture6"
        Me.Picture6.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture6.Top = 0.343!
        Me.Picture6.Width = 7.75!
        '
        'Label503
        '
        Me.Label503.Height = 0.1875!
        Me.Label503.HyperLink = Nothing
        Me.Label503.Left = 0.5!
        Me.Label503.Name = "Label503"
        Me.Label503.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label503.Text = "BEAM WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label503.Top = 3.073!
        Me.Label503.Width = 7.75!
        '
        'Label504
        '
        Me.Label504.Height = 0.1875!
        Me.Label504.HyperLink = Nothing
        Me.Label504.Left = 0!
        Me.Label504.Name = "Label504"
        Me.Label504.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label504.Text = "Φblockshear=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label504.Top = 3.206999!
        Me.Label504.Width = 2.25!
        '
        'S49_C1_001
        '
        Me.S49_C1_001.Height = 0.1875!
        Me.S49_C1_001.HyperLink = Nothing
        Me.S49_C1_001.Left = 2.25!
        Me.S49_C1_001.Name = "S49_C1_001"
        Me.S49_C1_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C1_001.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_001.Top = 3.206999!
        Me.S49_C1_001.Width = 1.0!
        '
        'Label506
        '
        Me.Label506.Height = 0.1875!
        Me.Label506.HyperLink = Nothing
        Me.Label506.Left = 0!
        Me.Label506.Name = "Label506"
        Me.Label506.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label506.Text = "Ubs=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label506.Top = 3.394!
        Me.Label506.Width = 2.25!
        '
        'S49_C1_002
        '
        Me.S49_C1_002.Height = 0.1875!
        Me.S49_C1_002.HyperLink = Nothing
        Me.S49_C1_002.Left = 2.25!
        Me.S49_C1_002.Name = "S49_C1_002"
        Me.S49_C1_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S49_C1_002.Text = "1" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_002.Top = 3.394!
        Me.S49_C1_002.Width = 1.0!
        '
        'Label508
        '
        Me.Label508.Height = 0.1875!
        Me.Label508.HyperLink = Nothing
        Me.Label508.Left = 0!
        Me.Label508.Name = "Label508"
        Me.Label508.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label508.Text = "Lb_edge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label508.Top = 3.581997!
        Me.Label508.Width = 2.25!
        '
        'Label509
        '
        Me.Label509.Height = 0.1875!
        Me.Label509.HyperLink = Nothing
        Me.Label509.Left = 3.25!
        Me.Label509.Name = "Label509"
        Me.Label509.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label509.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label509.Top = 3.581997!
        Me.Label509.Width = 4.75!
        '
        'S49_C1_003
        '
        Me.S49_C1_003.Height = 0.1875!
        Me.S49_C1_003.HyperLink = Nothing
        Me.S49_C1_003.Left = 2.25!
        Me.S49_C1_003.Name = "S49_C1_003"
        Me.S49_C1_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_003.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_003.Top = 3.583!
        Me.S49_C1_003.Width = 1.0!
        '
        'Label511
        '
        Me.Label511.Height = 0.1875!
        Me.Label511.HyperLink = Nothing
        Me.Label511.Left = 0!
        Me.Label511.Name = "Label511"
        Me.Label511.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label511.Text = "Lc_bmWeb =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label511.Top = 3.770997!
        Me.Label511.Width = 2.25!
        '
        'Label512
        '
        Me.Label512.Height = 0.1875!
        Me.Label512.HyperLink = Nothing
        Me.Label512.Left = 3.25!
        Me.Label512.Name = "Label512"
        Me.Label512.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label512.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label512.Top = 3.770997!
        Me.Label512.Width = 4.75!
        '
        'S49_C1_004
        '
        Me.S49_C1_004.Height = 0.1875!
        Me.S49_C1_004.HyperLink = Nothing
        Me.S49_C1_004.Left = 2.25!
        Me.S49_C1_004.Name = "S49_C1_004"
        Me.S49_C1_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_004.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_004.Top = 3.771997!
        Me.S49_C1_004.Width = 1.0!
        '
        'Label514
        '
        Me.Label514.Height = 0.1875!
        Me.Label514.HyperLink = Nothing
        Me.Label514.Left = 0!
        Me.Label514.Name = "Label514"
        Me.Label514.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label514.Text = "Lh_bmWeb=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label514.Top = 3.957001!
        Me.Label514.Width = 2.25!
        '
        'Label515
        '
        Me.Label515.Height = 0.1875!
        Me.Label515.HyperLink = Nothing
        Me.Label515.Left = 3.25!
        Me.Label515.Name = "Label515"
        Me.Label515.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label515.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2* (Lb_edge+Shorz*(n_Hbolt_SST - 1))" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label515.Top = 3.957001!
        Me.Label515.Width = 4.75!
        '
        'S49_C1_005
        '
        Me.S49_C1_005.Height = 0.1875!
        Me.S49_C1_005.HyperLink = Nothing
        Me.S49_C1_005.Left = 2.25!
        Me.S49_C1_005.Name = "S49_C1_005"
        Me.S49_C1_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_005.Text = "9.0"
        Me.S49_C1_005.Top = 3.958!
        Me.S49_C1_005.Width = 1.0!
        '
        'Label517
        '
        Me.Label517.Height = 0.1875!
        Me.Label517.HyperLink = Nothing
        Me.Label517.Left = 0!
        Me.Label517.Name = "Label517"
        Me.Label517.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label517.Text = "Agv_bmWebHorz=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label517.Top = 4.145009!
        Me.Label517.Width = 2.25!
        '
        'Label518
        '
        Me.Label518.Height = 0.1875!
        Me.Label518.HyperLink = Nothing
        Me.Label518.Left = 3.25!
        Me.Label518.Name = "Label518"
        Me.Label518.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label518.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lh_bmWeb*tbw" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label518.Top = 4.145009!
        Me.Label518.Width = 4.75!
        '
        'S49_C1_006
        '
        Me.S49_C1_006.Height = 0.1875!
        Me.S49_C1_006.HyperLink = Nothing
        Me.S49_C1_006.Left = 2.25!
        Me.S49_C1_006.Name = "S49_C1_006"
        Me.S49_C1_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_006.Text = "9.0"
        Me.S49_C1_006.Top = 4.146!
        Me.S49_C1_006.Width = 1.0!
        '
        'Label520
        '
        Me.Label520.Height = 0.1875!
        Me.Label520.HyperLink = Nothing
        Me.Label520.Left = 0!
        Me.Label520.Name = "Label520"
        Me.Label520.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label520.Text = "Ant_bmWebHorz=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label520.Top = 4.333008!
        Me.Label520.Width = 2.25!
        '
        'Label521
        '
        Me.Label521.Height = 0.1875!
        Me.Label521.HyperLink = Nothing
        Me.Label521.Left = 3.25!
        Me.Label521.Name = "Label521"
        Me.Label521.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label521.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label521.Top = 4.333008!
        Me.Label521.Width = 4.75!
        '
        'S49_C1_007
        '
        Me.S49_C1_007.Height = 0.1875!
        Me.S49_C1_007.HyperLink = Nothing
        Me.S49_C1_007.Left = 2.25!
        Me.S49_C1_007.Name = "S49_C1_007"
        Me.S49_C1_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_007.Text = "9.0"
        Me.S49_C1_007.Top = 4.334002!
        Me.S49_C1_007.Width = 1.0!
        '
        'Label523
        '
        Me.Label523.Height = 0.1875!
        Me.Label523.HyperLink = Nothing
        Me.Label523.Left = 0!
        Me.Label523.Name = "Label523"
        Me.Label523.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label523.Text = "Anv_bmWebHorz=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label523.Top = 4.521999!
        Me.Label523.Width = 2.25!
        '
        'Label524
        '
        Me.Label524.Height = 0.1875!
        Me.Label524.HyperLink = Nothing
        Me.Label524.Left = 3.25!
        Me.Label524.Name = "Label524"
        Me.Label524.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label524.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Agv_bmWebHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label524.Top = 4.521999!
        Me.Label524.Width = 4.75!
        '
        'S49_C1_008
        '
        Me.S49_C1_008.Height = 0.1875!
        Me.S49_C1_008.HyperLink = Nothing
        Me.S49_C1_008.Left = 2.25!
        Me.S49_C1_008.Name = "S49_C1_008"
        Me.S49_C1_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_008.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_008.Top = 4.522999!
        Me.S49_C1_008.Width = 1.0!
        '
        'Label526
        '
        Me.Label526.Height = 0.1875!
        Me.Label526.HyperLink = Nothing
        Me.Label526.Left = 0!
        Me.Label526.Name = "Label526"
        Me.Label526.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label526.Text = "Rn_bmWebHorz1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label526.Top = 4.710002!
        Me.Label526.Width = 2.25!
        '
        'Label527
        '
        Me.Label527.Height = 0.1875!
        Me.Label527.HyperLink = Nothing
        Me.Label527.Left = 3.25!
        Me.Label527.Name = "Label527"
        Me.Label527.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label527.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_bmWeb* Anv_bmWebHorz + Ubs* Fu_bmWeb* Ant_bmWebHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label527.Top = 4.710002!
        Me.Label527.Width = 4.75!
        '
        'S49_C1_009
        '
        Me.S49_C1_009.Height = 0.1875!
        Me.S49_C1_009.HyperLink = Nothing
        Me.S49_C1_009.Left = 2.25!
        Me.S49_C1_009.Name = "S49_C1_009"
        Me.S49_C1_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_009.Text = "9.0"
        Me.S49_C1_009.Top = 4.710999!
        Me.S49_C1_009.Width = 1.0!
        '
        'Label529
        '
        Me.Label529.Height = 0.1875!
        Me.Label529.HyperLink = Nothing
        Me.Label529.Left = 0!
        Me.Label529.Name = "Label529"
        Me.Label529.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label529.Text = "Rn_bmWebHorz2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label529.Top = 4.898999!
        Me.Label529.Width = 2.25!
        '
        'Label530
        '
        Me.Label530.Height = 0.1875!
        Me.Label530.HyperLink = Nothing
        Me.Label530.Left = 3.25!
        Me.Label530.Name = "Label530"
        Me.Label530.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label530.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_bmWeb* Agv_bmWebHorz + Ubs* Fu_bmWeb* Ant_bmWebHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label530.Top = 4.898999!
        Me.Label530.Width = 4.75!
        '
        'S49_C1_010
        '
        Me.S49_C1_010.Height = 0.1875!
        Me.S49_C1_010.HyperLink = Nothing
        Me.S49_C1_010.Left = 2.25!
        Me.S49_C1_010.Name = "S49_C1_010"
        Me.S49_C1_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_010.Text = "9.0"
        Me.S49_C1_010.Top = 4.899999!
        Me.S49_C1_010.Width = 1.0!
        '
        'Label532
        '
        Me.Label532.Height = 0.1875!
        Me.Label532.HyperLink = Nothing
        Me.Label532.Left = 0!
        Me.Label532.Name = "Label532"
        Me.Label532.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label532.Text = "Rn_bmWebHorz=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label532.Top = 5.086014!
        Me.Label532.Width = 2.25!
        '
        'Label533
        '
        Me.Label533.Height = 0.1875!
        Me.Label533.HyperLink = Nothing
        Me.Label533.Left = 3.25!
        Me.Label533.Name = "Label533"
        Me.Label533.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label533.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=  Φblocksher*min (Rn_bmWebHorz1, Rn_bmWebHorz2)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label533.Top = 5.086014!
        Me.Label533.Width = 4.75!
        '
        'S49_C1_011
        '
        Me.S49_C1_011.Height = 0.1875!
        Me.S49_C1_011.HyperLink = Nothing
        Me.S49_C1_011.Left = 2.25!
        Me.S49_C1_011.Name = "S49_C1_011"
        Me.S49_C1_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_011.Text = "9.0"
        Me.S49_C1_011.Top = 5.086999!
        Me.S49_C1_011.Width = 1.0!
        '
        'Label535
        '
        Me.Label535.Height = 0.1875!
        Me.Label535.HyperLink = Nothing
        Me.Label535.Left = 0!
        Me.Label535.Name = "Label535"
        Me.Label535.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label535.Text = "SUM_bmWebHorz_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label535.Top = 5.275036!
        Me.Label535.Width = 2.25!
        '
        'Label536
        '
        Me.Label536.Height = 0.1875!
        Me.Label536.HyperLink = Nothing
        Me.Label536.Left = 3.75!
        Me.Label536.Name = "Label536"
        Me.Label536.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label536.Text = "OK if SUM_bmWebHorz <= SUM_Allowed"
        Me.Label536.Top = 5.463039!
        Me.Label536.Width = 4.25!
        '
        'Label538
        '
        Me.Label538.Height = 0.1875!
        Me.Label538.HyperLink = Nothing
        Me.Label538.Left = 3.25!
        Me.Label538.Name = "Label538"
        Me.Label538.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label538.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_SST/ Rn_bmWebHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label538.Top = 5.274022!
        Me.Label538.Width = 4.75!
        '
        'S49_C1_012
        '
        Me.S49_C1_012.Height = 0.1875!
        Me.S49_C1_012.HyperLink = Nothing
        Me.S49_C1_012.Left = 2.25!
        Me.S49_C1_012.Name = "S49_C1_012"
        Me.S49_C1_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_012.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_012.Top = 5.275015!
        Me.S49_C1_012.Width = 1.0!
        '
        'Label540
        '
        Me.Label540.Height = 0.1875!
        Me.Label540.HyperLink = Nothing
        Me.Label540.Left = 0!
        Me.Label540.Name = "Label540"
        Me.Label540.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label540.Text = "Adequate =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label540.Top = 5.463039!
        Me.Label540.Width = 2.25!
        '
        'Label541
        '
        Me.Label541.Height = 0.1875!
        Me.Label541.HyperLink = Nothing
        Me.Label541.Left = 0.5!
        Me.Label541.Name = "Label541"
        Me.Label541.Style = "font-size: 8.25pt; font-weight: normal; text-align: left; text-decoration: underl" &
    "ine; vertical-align: middle; ddo-char-set: 0"
        Me.Label541.Text = "SHEAR PLATE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label541.Top = 5.707999!
        Me.Label541.Width = 7.75!
        '
        'Label542
        '
        Me.Label542.Height = 0.1875!
        Me.Label542.HyperLink = Nothing
        Me.Label542.Left = 0!
        Me.Label542.Name = "Label542"
        Me.Label542.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label542.Text = "Lsp_edge=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label542.Top = 6.082!
        Me.Label542.Width = 2.25!
        '
        'Label543
        '
        Me.Label543.Height = 0.1875!
        Me.Label543.HyperLink = Nothing
        Me.Label543.Left = 3.25!
        Me.Label543.Name = "Label543"
        Me.Label543.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label543.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label543.Top = 6.082985!
        Me.Label543.Width = 4.75!
        '
        'S49_C1_014
        '
        Me.S49_C1_014.Height = 0.1875!
        Me.S49_C1_014.HyperLink = Nothing
        Me.S49_C1_014.Left = 2.25!
        Me.S49_C1_014.Name = "S49_C1_014"
        Me.S49_C1_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_014.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_014.Top = 6.083!
        Me.S49_C1_014.Width = 1.0!
        '
        'Label545
        '
        Me.Label545.Height = 0.1875!
        Me.Label545.HyperLink = Nothing
        Me.Label545.Left = 0!
        Me.Label545.Name = "Label545"
        Me.Label545.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label545.Text = "Lc_sp =" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label545.Top = 6.271001!
        Me.Label545.Width = 2.25!
        '
        'Label546
        '
        Me.Label546.Height = 0.1875!
        Me.Label546.HyperLink = Nothing
        Me.Label546.Left = 3.25!
        Me.Label546.Name = "Label546"
        Me.Label546.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label546.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label546.Top = 6.271001!
        Me.Label546.Width = 4.75!
        '
        'S49_C1_015
        '
        Me.S49_C1_015.Height = 0.1875!
        Me.S49_C1_015.HyperLink = Nothing
        Me.S49_C1_015.Left = 2.25!
        Me.S49_C1_015.Name = "S49_C1_015"
        Me.S49_C1_015.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_015.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_015.Top = 6.270999!
        Me.S49_C1_015.Width = 1.0!
        '
        'Label548
        '
        Me.Label548.Height = 0.1875!
        Me.Label548.HyperLink = Nothing
        Me.Label548.Left = 0!
        Me.Label548.Name = "Label548"
        Me.Label548.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label548.Text = "Lh_sp1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label548.Top = 6.459002!
        Me.Label548.Width = 2.25!
        '
        'Label549
        '
        Me.Label549.Height = 0.1875!
        Me.Label549.HyperLink = Nothing
        Me.Label549.Left = 3.25!
        Me.Label549.Name = "Label549"
        Me.Label549.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label549.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2* (Lsp_edge + Shorz*(n_Hbolt_SST - 1))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label549.Top = 6.459002!
        Me.Label549.Width = 4.75!
        '
        'S49_C1_016
        '
        Me.S49_C1_016.Height = 0.1875!
        Me.S49_C1_016.HyperLink = Nothing
        Me.S49_C1_016.Left = 2.25!
        Me.S49_C1_016.Name = "S49_C1_016"
        Me.S49_C1_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_016.Text = "9.0"
        Me.S49_C1_016.Top = 6.453999!
        Me.S49_C1_016.Width = 1.0!
        '
        'Label551
        '
        Me.Label551.Height = 0.1875!
        Me.Label551.HyperLink = Nothing
        Me.Label551.Left = 0!
        Me.Label551.Name = "Label551"
        Me.Label551.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label551.Text = "Agv_spHorz1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label551.Top = 6.646016!
        Me.Label551.Width = 2.25!
        '
        'Label552
        '
        Me.Label552.Height = 0.1875!
        Me.Label552.HyperLink = Nothing
        Me.Label552.Left = 3.25!
        Me.Label552.Name = "Label552"
        Me.Label552.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label552.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lh_sp* tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label552.Top = 6.646016!
        Me.Label552.Width = 4.75!
        '
        'S49_C1_017
        '
        Me.S49_C1_017.Height = 0.1875!
        Me.S49_C1_017.HyperLink = Nothing
        Me.S49_C1_017.Left = 2.25!
        Me.S49_C1_017.Name = "S49_C1_017"
        Me.S49_C1_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_017.Text = "9.0"
        Me.S49_C1_017.Top = 6.642001!
        Me.S49_C1_017.Width = 1.0!
        '
        'Label554
        '
        Me.Label554.Height = 0.1875!
        Me.Label554.HyperLink = Nothing
        Me.Label554.Left = 0!
        Me.Label554.Name = "Label554"
        Me.Label554.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label554.Text = "Ant_spHorz1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label554.Top = 6.834032!
        Me.Label554.Width = 2.25!
        '
        'Label555
        '
        Me.Label555.Height = 0.1875!
        Me.Label555.HyperLink = Nothing
        Me.Label555.Left = 3.25!
        Me.Label555.Name = "Label555"
        Me.Label555.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label555.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label555.Top = 6.834032!
        Me.Label555.Width = 4.75!
        '
        'S49_C1_018
        '
        Me.S49_C1_018.Height = 0.1875!
        Me.S49_C1_018.HyperLink = Nothing
        Me.S49_C1_018.Left = 2.25!
        Me.S49_C1_018.Name = "S49_C1_018"
        Me.S49_C1_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_018.Text = "9.0"
        Me.S49_C1_018.Top = 6.834031!
        Me.S49_C1_018.Width = 1.0!
        '
        'Label557
        '
        Me.Label557.Height = 0.1875!
        Me.Label557.HyperLink = Nothing
        Me.Label557.Left = 0!
        Me.Label557.Name = "Label557"
        Me.Label557.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label557.Text = "Anv_spHorz1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label557.Top = 7.022001!
        Me.Label557.Width = 2.25!
        '
        'Label558
        '
        Me.Label558.Height = 0.1875!
        Me.Label558.HyperLink = Nothing
        Me.Label558.Left = 3.25!
        Me.Label558.Name = "Label558"
        Me.Label558.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label558.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= Agv_bmWebHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label558.Top = 7.022033!
        Me.Label558.Width = 4.75!
        '
        'S49_C1_019
        '
        Me.S49_C1_019.Height = 0.1875!
        Me.S49_C1_019.HyperLink = Nothing
        Me.S49_C1_019.Left = 2.25!
        Me.S49_C1_019.Name = "S49_C1_019"
        Me.S49_C1_019.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_019.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_019.Top = 7.02203!
        Me.S49_C1_019.Width = 1.0!
        '
        'Label560
        '
        Me.Label560.Height = 0.1875!
        Me.Label560.HyperLink = Nothing
        Me.Label560.Left = 0!
        Me.Label560.Name = "Label560"
        Me.Label560.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label560.Text = "Rn_spHorz1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label560.Top = 7.209!
        Me.Label560.Width = 2.25!
        '
        'Label561
        '
        Me.Label561.Height = 0.1875!
        Me.Label561.HyperLink = Nothing
        Me.Label561.Left = 3.25!
        Me.Label561.Name = "Label561"
        Me.Label561.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label561.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_sp* Anv_spHorz + Ubs* Fu_sp* Ant_spHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label561.Top = 7.209!
        Me.Label561.Width = 4.75!
        '
        'S49_C1_020
        '
        Me.S49_C1_020.Height = 0.1875!
        Me.S49_C1_020.HyperLink = Nothing
        Me.S49_C1_020.Left = 2.25!
        Me.S49_C1_020.Name = "S49_C1_020"
        Me.S49_C1_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_020.Text = "9.0"
        Me.S49_C1_020.Top = 7.209015!
        Me.S49_C1_020.Width = 1.0!
        '
        'Label563
        '
        Me.Label563.Height = 0.1875!
        Me.Label563.HyperLink = Nothing
        Me.Label563.Left = 0!
        Me.Label563.Name = "Label563"
        Me.Label563.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label563.Text = "Rn_spbHorz2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label563.Top = 7.397017!
        Me.Label563.Width = 2.25!
        '
        'Label564
        '
        Me.Label564.Height = 0.1875!
        Me.Label564.HyperLink = Nothing
        Me.Label564.Left = 3.25!
        Me.Label564.Name = "Label564"
        Me.Label564.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label564.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_sp* Agv_spHorz + Ubs* Fu_sp* Ant_spHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label564.Top = 7.397017!
        Me.Label564.Width = 4.75!
        '
        'S49_C1_021
        '
        Me.S49_C1_021.Height = 0.1875!
        Me.S49_C1_021.HyperLink = Nothing
        Me.S49_C1_021.Left = 2.25!
        Me.S49_C1_021.Name = "S49_C1_021"
        Me.S49_C1_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_021.Text = "9.0"
        Me.S49_C1_021.Top = 7.397009!
        Me.S49_C1_021.Width = 1.0!
        '
        'Label566
        '
        Me.Label566.Height = 0.1875!
        Me.Label566.HyperLink = Nothing
        Me.Label566.Left = 0!
        Me.Label566.Name = "Label566"
        Me.Label566.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label566.Text = "ΦRn_spHorz1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label566.Top = 7.585017!
        Me.Label566.Width = 2.25!
        '
        'Label567
        '
        Me.Label567.Height = 0.1875!
        Me.Label567.HyperLink = Nothing
        Me.Label567.Left = 3.25!
        Me.Label567.Name = "Label567"
        Me.Label567.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label567.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblocksher* min (Rn_spHorz1, Rn_spbHorz2)" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label567.Top = 7.585017!
        Me.Label567.Width = 4.75!
        '
        'S49_C1_022
        '
        Me.S49_C1_022.Height = 0.1875!
        Me.S49_C1_022.HyperLink = Nothing
        Me.S49_C1_022.Left = 2.25!
        Me.S49_C1_022.Name = "S49_C1_022"
        Me.S49_C1_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_022.Text = "9.0"
        Me.S49_C1_022.Top = 7.585022!
        Me.S49_C1_022.Width = 1.0!
        '
        'Label569
        '
        Me.Label569.Height = 0.1875!
        Me.Label569.HyperLink = Nothing
        Me.Label569.Left = 0!
        Me.Label569.Name = "Label569"
        Me.Label569.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label569.Text = "SUM_spHorz_BS1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label569.Top = 7.774039!
        Me.Label569.Width = 2.25!
        '
        'Label572
        '
        Me.Label572.Height = 0.1875!
        Me.Label572.HyperLink = Nothing
        Me.Label572.Left = 3.25!
        Me.Label572.Name = "Label572"
        Me.Label572.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label572.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_SST/ Rn_spHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label572.Top = 7.773016!
        Me.Label572.Width = 4.75!
        '
        'S49_C1_023
        '
        Me.S49_C1_023.Height = 0.1875!
        Me.S49_C1_023.HyperLink = Nothing
        Me.S49_C1_023.Left = 2.25!
        Me.S49_C1_023.Name = "S49_C1_023"
        Me.S49_C1_023.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_023.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_023.Top = 7.773009!
        Me.S49_C1_023.Width = 1.0!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0.052!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label1.Text = "CASE 1:"
        Me.Label1.Top = 5.894999!
        Me.Label1.Width = 2.198!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label4.Text = "Lh_sp2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 8.267!
        Me.Label4.Width = 2.25!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.25!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label6.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=h_sp-(n_Vbolts*dHole_sp)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.Top = 8.267987!
        Me.Label6.Width = 4.75!
        '
        'S49_C1_024
        '
        Me.S49_C1_024.Height = 0.1875!
        Me.S49_C1_024.HyperLink = Nothing
        Me.S49_C1_024.Left = 2.25!
        Me.S49_C1_024.Name = "S49_C1_024"
        Me.S49_C1_024.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_024.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_024.Top = 8.268002!
        Me.S49_C1_024.Width = 1.0!
        '
        'Label9
        '
        Me.Label9.Height = 0.1875!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 0!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label9.Text = "Agv_spHorz2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label9.Top = 8.456!
        Me.Label9.Width = 2.25!
        '
        'Label10
        '
        Me.Label10.Height = 0.1875!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 3.25!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label10.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label10.Top = 8.456!
        Me.Label10.Width = 4.75!
        '
        'S49_C1_025
        '
        Me.S49_C1_025.Height = 0.1875!
        Me.S49_C1_025.HyperLink = Nothing
        Me.S49_C1_025.Left = 2.25!
        Me.S49_C1_025.Name = "S49_C1_025"
        Me.S49_C1_025.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_025.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_025.Top = 8.456!
        Me.S49_C1_025.Width = 1.0!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "Ant_spHorz2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label12.Top = 8.644!
        Me.Label12.Width = 2.25!
        '
        'Label13
        '
        Me.Label13.Height = 0.1875!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 3.25!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label13.Text = "in^3" & Global.Microsoft.VisualBasic.ChrW(9) & "= Lh_sp2*tsp" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label13.Top = 8.644!
        Me.Label13.Width = 4.75!
        '
        'S49_C1_026
        '
        Me.S49_C1_026.Height = 0.1875!
        Me.S49_C1_026.HyperLink = Nothing
        Me.S49_C1_026.Left = 2.25!
        Me.S49_C1_026.Name = "S49_C1_026"
        Me.S49_C1_026.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_026.Text = "9.0"
        Me.S49_C1_026.Top = 8.644!
        Me.S49_C1_026.Width = 1.0!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "Anv_spHorz2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label16.Top = 8.831019!
        Me.Label16.Width = 2.25!
        '
        'Label18
        '
        Me.Label18.Height = 0.1875!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 3.25!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label18.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 8.831019!
        Me.Label18.Width = 4.75!
        '
        'S49_C1_027
        '
        Me.S49_C1_027.Height = 0.1875!
        Me.S49_C1_027.HyperLink = Nothing
        Me.S49_C1_027.Left = 2.25!
        Me.S49_C1_027.Name = "S49_C1_027"
        Me.S49_C1_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_027.Text = "9.0"
        Me.S49_C1_027.Top = 8.831!
        Me.S49_C1_027.Width = 1.0!
        '
        'Label21
        '
        Me.Label21.Height = 0.1875!
        Me.Label21.HyperLink = Nothing
        Me.Label21.Left = 0!
        Me.Label21.Name = "Label21"
        Me.Label21.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label21.Text = "Rn_spHorz3=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label21.Top = 9.019029!
        Me.Label21.Width = 2.25!
        '
        'Label23
        '
        Me.Label23.Height = 0.1875!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 3.25!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label23.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fu_sp* Anv_spHorz2 + Ubs* Fu_sp* Ant_spHorz2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label23.Top = 9.019029!
        Me.Label23.Width = 4.75!
        '
        'S49_C1_028
        '
        Me.S49_C1_028.Height = 0.1875!
        Me.S49_C1_028.HyperLink = Nothing
        Me.S49_C1_028.Left = 2.25!
        Me.S49_C1_028.Name = "S49_C1_028"
        Me.S49_C1_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_028.Text = "9.0"
        Me.S49_C1_028.Top = 9.019!
        Me.S49_C1_028.Width = 1.0!
        '
        'Label25
        '
        Me.Label25.Height = 0.1875!
        Me.Label25.HyperLink = Nothing
        Me.Label25.Left = 0!
        Me.Label25.Name = "Label25"
        Me.Label25.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label25.Text = "Rn_spHorz4=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label25.Top = 9.207!
        Me.Label25.Width = 2.25!
        '
        'Label26
        '
        Me.Label26.Height = 0.1875!
        Me.Label26.HyperLink = Nothing
        Me.Label26.Left = 3.25!
        Me.Label26.Name = "Label26"
        Me.Label26.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label26.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.6* Fy_sp* Agv_spHorz2 + Ubs* Fu_sp* Ant_spHorz2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label26.Top = 9.20703!
        Me.Label26.Width = 4.75!
        '
        'S49_C1_029
        '
        Me.S49_C1_029.Height = 0.1875!
        Me.S49_C1_029.HyperLink = Nothing
        Me.S49_C1_029.Left = 2.25!
        Me.S49_C1_029.Name = "S49_C1_029"
        Me.S49_C1_029.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S49_C1_029.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_029.Top = 9.20703!
        Me.S49_C1_029.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.1875!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 0!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label28.Text = "ΦRn_spHorz2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label28.Top = 9.394!
        Me.Label28.Width = 2.25!
        '
        'Label30
        '
        Me.Label30.Height = 0.1875!
        Me.Label30.HyperLink = Nothing
        Me.Label30.Left = 3.25!
        Me.Label30.Name = "Label30"
        Me.Label30.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label30.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φblockshear* min (Rn_spHorz3, Rn_spbHorz4)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label30.Top = 9.394!
        Me.Label30.Width = 4.75!
        '
        'S49_C1_030
        '
        Me.S49_C1_030.Height = 0.1875!
        Me.S49_C1_030.HyperLink = Nothing
        Me.S49_C1_030.Left = 2.25!
        Me.S49_C1_030.Name = "S49_C1_030"
        Me.S49_C1_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_030.Text = "9.0"
        Me.S49_C1_030.Top = 9.39401!
        Me.S49_C1_030.Width = 1.0!
        '
        'Label33
        '
        Me.Label33.Height = 0.1875!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "SUM_spHorz_BS2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label33.Top = 9.582009!
        Me.Label33.Width = 2.25!
        '
        'Label34
        '
        Me.Label34.Height = 0.1875!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 3.25!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label34.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_sp/ Rn_spHorz2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label34.Top = 9.582009!
        Me.Label34.Width = 4.75!
        '
        'S49_C1_031
        '
        Me.S49_C1_031.Height = 0.1875!
        Me.S49_C1_031.HyperLink = Nothing
        Me.S49_C1_031.Left = 2.25!
        Me.S49_C1_031.Name = "S49_C1_031"
        Me.S49_C1_031.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S49_C1_031.Text = "9.0"
        Me.S49_C1_031.Top = 9.582009!
        Me.S49_C1_031.Width = 1.0!
        '
        'Label40
        '
        Me.Label40.Height = 0.1875!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 0!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label40.Text = "SUM_spHorz_BS=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label40.Top = 9.95904!
        Me.Label40.Width = 2.25!
        '
        'Label42
        '
        Me.Label42.Height = 0.1875!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 3.25!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label42.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= OK if SUM_spHorz_BS <= SUM_spHorz_BS_allowed" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label42.Top = 10.147!
        Me.Label42.Width = 4.75!
        '
        'Label43
        '
        Me.Label43.Height = 0.1875!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 3.25!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label43.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pu_SST/ Rn_spHorz" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label43.Top = 9.958009!
        Me.Label43.Width = 4.75!
        '
        'S49_C1_032
        '
        Me.S49_C1_032.Height = 0.1875!
        Me.S49_C1_032.HyperLink = Nothing
        Me.S49_C1_032.Left = 2.25!
        Me.S49_C1_032.Name = "S49_C1_032"
        Me.S49_C1_032.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S49_C1_032.Text = "2.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S49_C1_032.Top = 9.958009!
        Me.S49_C1_032.Width = 1.0!
        '
        'Label45
        '
        Me.Label45.Height = 0.1875!
        Me.Label45.HyperLink = Nothing
        Me.Label45.Left = 0!
        Me.Label45.Name = "Label45"
        Me.Label45.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label45.Text = "Check=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label45.Top = 10.147!
        Me.Label45.Width = 2.25!
        '
        'Label46
        '
        Me.Label46.Height = 0.1875!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 0.052!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; font-weight: bold; text-align: right; vertical-align: middle"
        Me.Label46.Text = "CASE 2:"
        Me.Label46.Top = 8.080002!
        Me.Label46.Width = 2.198!
        '
        'Shape15
        '
        Me.Shape15.Height = 0.1875!
        Me.Shape15.Left = 2.25!
        Me.Shape15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape15.Name = "Shape15"
        Me.Shape15.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape15.Top = 5.463014!
        Me.Shape15.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 2.25!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 10.14796!
        Me.Shape1.Width = 1.0!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.01!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(8.25!, 0.01!)
        Me.PageBreak1.Top = 3.02!
        Me.PageBreak1.Width = 8.25!
        '
        'US_SPC5
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.271!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S49_C1_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label501, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label502, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label503, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label504, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label506, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label508, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label509, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label511, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label512, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label514, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label515, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label517, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label518, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label520, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label521, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label523, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label524, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label526, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label527, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label529, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label530, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label532, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label533, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label535, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label536, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label538, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label540, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label541, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label542, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label543, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label545, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label546, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label548, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label549, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label551, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label552, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label554, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label555, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label557, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label558, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label560, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label561, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label563, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label564, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label566, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label567, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label569, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label572, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label26, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label30, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S49_C1_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents S49_C1_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label501 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label502 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture6 As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Label503 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label504 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label506 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label508 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label509 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label511 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label512 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label514 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label515 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label517 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label518 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label520 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label521 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label523 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label524 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label526 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label527 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label529 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label530 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label532 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label533 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label535 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label536 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label538 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label540 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label541 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label542 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label543 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label545 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label546 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label548 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label549 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label551 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label552 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label554 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label555 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label557 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label558 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label560 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label561 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label563 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label564 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label566 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label567 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label569 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label572 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape15 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label21 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label25 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label26 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label30 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S49_C1_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label45 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak

    '

End Class
