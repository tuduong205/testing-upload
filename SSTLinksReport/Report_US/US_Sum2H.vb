﻿Imports GrapeCity.ActiveReports
Imports YieldLinkLib

Public Class US_Sum2H

    Private index As Integer
    Sub New(_index As Integer, limit As DCRLimits, str As String, Locs() As Double, dgvFlag As Integer)

        ' This call is required by the designer.
        InitializeComponent()
        index = _index
        ' Add any initialization after the InitializeComponent() call.
        Dim Arr = str.Split(clsMRSJob.splChar)
        '
        Dim lb As SectionReportModel.Label
        Dim ln As SectionReportModel.Line
        Dim X, Val As Double
        Dim isNumber As Boolean
        Dim curLimit As Double
        Dim cnt As Integer
        '
        For i As Integer = 0 To Locs.Count - 1
            lb = New SectionReportModel.Label
            lb.Style = "font-size: 8.25pt; text-align: center; vertical-align: middle"
            lb.Location = New PointF(X, 0)
            lb.Size = New SizeF(Locs(i), Detail.Height)
            lb.Text = Arr(i)
            If dgvFlag = 1 Then
                cnt = 10
                If i = cnt + 1 Then : curLimit = limit.tbf_check
                ElseIf i = cnt + 2 Then : curLimit = limit.bf_check
                ElseIf i = cnt + 3 Then : curLimit = limit.Lyield_check
                ElseIf i = cnt + 4 Then : curLimit = limit.Panel_Zone_DCR
                ElseIf i = cnt + 5 Then : curLimit = limit.Panel_Zone_DCR
                ElseIf i = cnt + 6 Then : curLimit = limit.Drift_check
                Else : curLimit = 999999999
                End If
            ElseIf dgvFlag = 2 Then
                cnt = 7
                If i = cnt + 1 Then : curLimit = limit.Beam_tf_DCR
                ElseIf i = cnt + 2 Then : curLimit = limit.Link_strength_DCR
                ElseIf i = cnt + 3 Then : curLimit = limit.Lyield_check
                ElseIf i = cnt + 4 Then : curLimit = limit.t_BRP_DCR
                ElseIf i = cnt + 5 Then : curLimit = limit.BRP_Bolt_DCR
                Else : curLimit = 999999999
                End If
            ElseIf dgvFlag = 3 Then
                cnt = 12
                If i = cnt + 1 Then : curLimit = 1
                ElseIf i = cnt + 2 Then : curLimit = limit.SCWB_DCR
                ElseIf i = cnt + 3 Then : curLimit = limit.Panel_Zone_DCR
                ElseIf i = cnt + 4 Then : curLimit = limit.Column_Flange_DCR
                Else : curLimit = 999999999
                End If
            ElseIf dgvFlag = 4 Then
                cnt = 13
                If i = cnt + 1 Then : curLimit = limit.Beam_Web_DCR
                ElseIf i = cnt + 2 Then : curLimit = 1
                ElseIf i = cnt + 3 Then : curLimit = limit.Shear_Plate_DCR
                ElseIf i = cnt + 4 Then : curLimit = limit.Bolt_DCR
                ElseIf i = cnt + 5 Then : curLimit = limit.Fillet_weld_DCR
                Else : curLimit = 999999999
                End If
            ElseIf dgvFlag = 5 Then
                cnt = 14
                If i = cnt + 1 Then : curLimit = limit.Drift_check
                Else : curLimit = 999999999
                End If
            ElseIf dgvFlag = 6 Or dgvFlag = 7 Then
                curLimit = 1
                'cnt = 10
                'cnt += 1 : If i = cnt Then : curLimit = 1
                '    cnt += 1 : ElseIf i = cnt Then : curLimit = 1
                '    cnt += 1 : ElseIf i = cnt Then : curLimit = 1
                '    cnt += 1 : ElseIf i = cnt Then : curLimit = 1
                '    cnt += 1 : ElseIf i = cnt Then : curLimit = 1
                '    cnt += 1 : ElseIf i = cnt Then : curLimit = 1
                'Else : curLimit = 999999999
                'End If
            End If

            If (dgvFlag = 1 AndAlso i > 10) Or (dgvFlag = 2 AndAlso i > 7) Or (dgvFlag = 3 AndAlso i > 12) Or (dgvFlag = 4 AndAlso i > 13) Or
                (dgvFlag = 5 AndAlso i > 14) Or (dgvFlag = 6 AndAlso i > 10) Or (dgvFlag = 7 AndAlso i > 9) Then
                isNumber = Double.TryParse(Arr(i), Val)
                If Val > curLimit Or lb.Text = "NG" Then
                    lb.ForeColor = System.Drawing.Color.Red
                ElseIf isNumber = False Then
                    lb.ForeColor = System.Drawing.Color.Black
                Else
                    lb.ForeColor = System.Drawing.Color.Blue
                End If
            End If
            '
            Detail.Controls.Add(lb)
            '
            X += Locs(i)
            ln = New SectionReportModel.Line
            ln.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
            ln.Width = 1
            ln.X1 = X
            ln.X2 = X
            ln.Y1 = 0.0!
            ln.Y2 = Detail.Height
            Detail.Controls.Add(ln)
        Next
    End Sub

    'Private Sub Detail_Format(sender As Object, e As EventArgs) Handles Detail.Format
    '    'If index Mod 2 = 0 Then
    '    '    Detail.BackColor = Color.White
    '    'Else
    '    '    Detail.BackColor = Color.LightGray
    '    'End If
    'End Sub

End Class
