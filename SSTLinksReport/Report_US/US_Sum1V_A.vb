﻿Imports GrapeCity.ActiveReports
Imports YieldLinkLib

Public Class US_Sum1V_A
    Sub New(ByRef curJob As clsMRSJob)
        ' This call is required by the designer.
        InitializeComponent()
        ' Add any initialization after the InitializeComponent() call.

        lbJobName.Text = curJob.Name
        lbJobID.Text = curJob.JobID
        lbPrintedDate.Text = "Date Printed: " & curJob.PrintedDate
        '
        lbST1.Text = NumberFormat(curJob.MaterialInfo.ShearPlate_Fy, -2)
        lbST2.Text = NumberFormat(curJob.MaterialInfo.ShearPlate_Fu, -2)
        '
        lbStiff1.Text = NumberFormat(curJob.MaterialInfo.StiffPL_Fy, -2)
        lbStiff2.Text = NumberFormat(curJob.MaterialInfo.StiffPL_Fu, -2)
        lbStiff3.Text = NumberFormat(curJob.MaterialInfo.StiffPL_DefThk, -2)
        lbStiff4.Text = curJob.MaterialInfo.StiffPL_1SidedConn.ToString
        '
        lbDP1.Text = NumberFormat(curJob.MaterialInfo.DoublerPL_Fy, -2)
        lbDP2.Text = NumberFormat(curJob.MaterialInfo.DoublerPL_Fu, -2)
        lbDP3.Text = NumberFormat(curJob.MaterialInfo.DoublerPL_DefThk, -2)
        '
        lbParam1.Text = curJob.ASCE
        lbParam2.Text = NumberFormat(curJob.Ss, -2)
        lbParam3.Text = NumberFormat(curJob.S1, -2)
        lbParam4.Text = NumberFormat(curJob.TL, -2)
        lbParam5.Text = curJob.SiteClass
        lbParam6.Text = curJob.userSds
        lbParam7.Text = NumberFormat(curJob.Sds, -2)
        'X
        lbFactor1_X.Text = curJob.FrType
        lbFactor2_X.Text = NumberFormat(curJob.R, -2)
        lbFactor3_X.Text = NumberFormat(curJob.Omega, -2)
        lbFactor4_X.Text = NumberFormat(curJob.Cd, -2)
        lbFactor5_X.Text = NumberFormat(curJob.I, -2)
        lbFactor6_X.Text = NumberFormat(curJob.Rho, -2)
        lbFactor7_X.Text = NumberFormat(curJob.f1, -2)
        lbFactor8_X.Text = NumberFormat(curJob.f2, -2)
        'Y
        lbFactor1_Y.Text = curJob.FrTypeY
        lbFactor2_Y.Text = NumberFormat(curJob.RY, -2)
        lbFactor3_Y.Text = NumberFormat(curJob.OmegaY, -2)
        lbFactor4_Y.Text = NumberFormat(curJob.CdY, -2)
        lbFactor5_Y.Text = NumberFormat(curJob.IY, -2)
        lbFactor6_Y.Text = NumberFormat(curJob.RhoY, -2)
        ''lbFactor7_Y.Text = NumberFormat(curJob.f1Y, -2)
        ''lbFactor8_Y.Text = NumberFormat(curJob.f2Y, -2)
        '
        lbDCode1.Text = curJob.DesignCode
        'X
        lbWcode.Text = curJob.ASCE
        lbWspeed_X.Text = NumberFormat(curJob.WindSpeed, 2)
        lbExposureType_X.Text = curJob.ExposureType
        lbGroundElev_X.Text = NumberFormat(curJob.F_groundEle, 2)
        lbKzt_X.Text = NumberFormat(curJob.Kzt, 2)
        lbGustFactor_X.Text = NumberFormat(curJob.F_gust, 2)
        lbKd_X.Text = NumberFormat(curJob.Kd, 2)
        '''Y
        ''lbWspeed_Y.Text = NumberFormat(curJob.WindSpeedY, 2)
        ''lbExposureType_Y.Text = curJob.ExposureTypeY
        ''lbGroundElev_Y.Text = NumberFormat(curJob.F_groundEleY, 2)
        ''lbKzt_Y.Text = NumberFormat(curJob.KztY, 2)
        ''lbGustFactor_Y.Text = NumberFormat(curJob.F_gustY, 2)
        ''lbKd_Y.Text = NumberFormat(curJob.KdY, 2)
    End Sub

End Class
