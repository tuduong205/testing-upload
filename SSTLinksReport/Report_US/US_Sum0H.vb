﻿Imports GrapeCity.ActiveReports
Imports GrapeCity.ActiveReports.Document
Imports YieldLinkLib

Public Class US_Sum0H

    Private job As clsMRSJob
    '
    Sub New(_job As clsMRSJob)
        ' This call is required by the designer.
        InitializeComponent()
        '
        job = _job
        lbVersion.Text = job.versionID
        ''lbCompCapFor.Text = job.Name & vbNewLine & job.Address & vbNewLine & job.CityState
        ''lbPrepFor.Text = job.EOR
        ''lbDatePrinted.Text = job.PrintedDate
        ''lbDesignBy.Text = job.EngName & vbNewLine & job.EngEmail
    End Sub

    Private Sub SR_p1_ReportStart(sender As Object, e As EventArgs) Handles MyBase.ReportStart
        If job.Dgv1Data.Count > 0 Then
            SubReport1.Report = New US_Sum1H(job, 1)
        Else
            'PageBreak1.Enabled = False
        End If
        If job.Dgv2Data.Count > 0 Then
            SubReport2.Report = New US_Sum1H(job, 2)
        Else
            PageBreak2.Enabled = False
        End If
        If job.Dgv3Data.Count > 0 Then
            SubReport3.Report = New US_Sum1H(job, 3)
        Else
            PageBreak3.Enabled = False
        End If
        If job.Dgv4Data.Count > 0 Then
            SubReport4.Report = New US_Sum1H(job, 4)
        Else
            PageBreak4.Enabled = False
        End If
        If job.Dgv5Data.Count > 0 Then
            SubReport5.Report = New US_Sum1H(job, 5)
        Else
            PageBreak5.Enabled = False
        End If
        If job.Dgv6Data.Count > 0 Then
            SubReport6.Report = New US_Sum1H(job, 6)
        Else
            PageBreak6.Enabled = False
        End If
        If job.Dgv7Data.Count > 0 Then
            SubReport7.Report = New US_Sum1H(job, 7)
        Else
            PageBreak7.Enabled = False
        End If
        If job.Dgv8Data.Count > 0 Then
            SubReport8.Report = New US_Sum1H(job, 8)
        Else
            PageBreak8.Enabled = False
        End If
    End Sub

End Class
