﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class US_Sum0V
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(US_Sum0V))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbCompCapFor = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbPrepFor = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDesignBy = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.lbDatePrinted = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.SubReport1 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        Me.lbVersion = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.PageHeader1 = New GrapeCity.ActiveReports.SectionReportModel.PageHeader()
        Me.Picture = New GrapeCity.ActiveReports.SectionReportModel.Picture()
        Me.Line = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.RichTextBox = New GrapeCity.ActiveReports.SectionReportModel.RichTextBox()
        Me.Label = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.PageFooter1 = New GrapeCity.ActiveReports.SectionReportModel.PageFooter()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.PageBreak2 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.SubReport2 = New GrapeCity.ActiveReports.SectionReportModel.SubReport()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbCompCapFor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbPrepFor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDesignBy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbDatePrinted, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lbVersion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Picture, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Label2, Me.Label3, Me.Label4, Me.lbCompCapFor, Me.lbPrepFor, Me.Label5, Me.lbDesignBy, Me.label7, Me.lbDatePrinted, Me.Label8, Me.PageBreak1, Me.SubReport1, Me.PageBreak2, Me.SubReport2})
        Me.Detail.Height = 9.0!
        Me.Detail.Name = "Detail"
        '
        'Label2
        '
        Me.Label2.Height = 0.375!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 18.75pt; font-weight: bold; text-align: center; ddo-char-set: 1"
        Me.Label2.Text = "Yield-Link® Connection Design Summary"
        Me.Label2.Top = 0.812!
        Me.Label2.Width = 8.5!
        '
        'Label3
        '
        Me.Label3.Height = 0.2!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 9.75pt; text-align: center; ddo-char-set: 0"
        Me.Label3.Text = "Component capacities for"
        Me.Label3.Top = 1.937!
        Me.Label3.Width = 8.5!
        '
        'Label4
        '
        Me.Label4.Height = 0.2!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 0!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 9.75pt; text-align: center; ddo-char-set: 0"
        Me.Label4.Text = "Design Firm Name:"
        Me.Label4.Top = 3.906!
        Me.Label4.Width = 8.5!
        '
        'lbCompCapFor
        '
        Me.lbCompCapFor.Height = 0.987!
        Me.lbCompCapFor.HyperLink = Nothing
        Me.lbCompCapFor.Left = 0!
        Me.lbCompCapFor.Name = "lbCompCapFor"
        Me.lbCompCapFor.Style = "font-family: Microsoft Sans Serif; font-size: 14.25pt; font-weight: bold; text-al" &
    "ign: center; ddo-char-set: 0"
        Me.lbCompCapFor.Text = "Job Name" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Job Address" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "City, State"
        Me.lbCompCapFor.Top = 2.137!
        Me.lbCompCapFor.Width = 8.5!
        '
        'lbPrepFor
        '
        Me.lbPrepFor.Height = 0.25!
        Me.lbPrepFor.HyperLink = Nothing
        Me.lbPrepFor.Left = 0!
        Me.lbPrepFor.Name = "lbPrepFor"
        Me.lbPrepFor.Style = "font-family: Microsoft Sans Serif; font-size: 14.25pt; font-weight: bold; text-al" &
    "ign: center; ddo-char-set: 0"
        Me.lbPrepFor.Text = "EOR"
        Me.lbPrepFor.Top = 4.206!
        Me.lbPrepFor.Width = 8.5!
        '
        'Label5
        '
        Me.Label5.Height = 0.2!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 9.75pt; text-align: center; ddo-char-set: 0"
        Me.Label5.Text = "Design By:"
        Me.Label5.Top = 5.928!
        Me.Label5.Width = 8.5!
        '
        'lbDesignBy
        '
        Me.lbDesignBy.Height = 0.55!
        Me.lbDesignBy.HyperLink = Nothing
        Me.lbDesignBy.Left = 0!
        Me.lbDesignBy.Name = "lbDesignBy"
        Me.lbDesignBy.Style = "font-family: Microsoft Sans Serif; font-size: 14.25pt; font-weight: bold; text-al" &
    "ign: center; ddo-char-set: 0"
        Me.lbDesignBy.Text = "Ditran" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Email: ditran@strongtie.com"
        Me.lbDesignBy.Top = 6.128002!
        Me.lbDesignBy.Width = 8.5!
        '
        'label7
        '
        Me.label7.Height = 0.2!
        Me.label7.HyperLink = Nothing
        Me.label7.Left = 0!
        Me.label7.Name = "label7"
        Me.label7.Style = "font-size: 9.75pt; text-align: center; ddo-char-set: 0"
        Me.label7.Text = "Date Printed:"
        Me.label7.Top = 5.322001!
        Me.label7.Width = 8.5!
        '
        'lbDatePrinted
        '
        Me.lbDatePrinted.Height = 0.25!
        Me.lbDatePrinted.HyperLink = Nothing
        Me.lbDatePrinted.Left = 0!
        Me.lbDatePrinted.Name = "lbDatePrinted"
        Me.lbDatePrinted.Style = "font-family: Microsoft Sans Serif; font-size: 14.25pt; font-weight: bold; text-al" &
    "ign: center; ddo-char-set: 0"
        Me.lbDatePrinted.Text = "07/17/2018"
        Me.lbDatePrinted.Top = 5.522!
        Me.lbDatePrinted.Width = 8.5!
        '
        'Label8
        '
        Me.Label8.Height = 0.771!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 1.011!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 9.75pt; text-align: left; ddo-char-set: 0"
        Me.Label8.Text = resources.GetString("Label8.Text")
        Me.Label8.Top = 7.73!
        Me.Label8.Width = 6.75!
        '
        'SubReport1
        '
        Me.SubReport1.CloseBorder = False
        Me.SubReport1.Height = 0.124!
        Me.SubReport1.Left = 0.5!
        Me.SubReport1.Name = "SubReport1"
        Me.SubReport1.Report = Nothing
        Me.SubReport1.Top = 8.604!
        Me.SubReport1.Width = 8.0!
        '
        'lbVersion
        '
        Me.lbVersion.Height = 0.188!
        Me.lbVersion.HyperLink = Nothing
        Me.lbVersion.Left = 0.5!
        Me.lbVersion.Name = "lbVersion"
        Me.lbVersion.Style = "font-size: 8.25pt; ddo-char-set: 1"
        Me.lbVersion.Text = "Version ID"
        Me.lbVersion.Top = 0.013!
        Me.lbVersion.Width = 2.75!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 0.5!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 0!
        Me.Line5.Width = 7.5!
        Me.Line5.X1 = 0.5!
        Me.Line5.X2 = 8.0!
        Me.Line5.Y1 = 0!
        Me.Line5.Y2 = 0!
        '
        'PageHeader1
        '
        Me.PageHeader1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.Picture, Me.Line, Me.RichTextBox, Me.Label, Me.Label1, Me.label6})
        Me.PageHeader1.Height = 1.25!
        Me.PageHeader1.Name = "PageHeader1"
        '
        'Picture
        '
        Me.Picture.Height = 1.005!
        Me.Picture.HyperLink = Nothing
        Me.Picture.ImageData = CType(resources.GetObject("Picture.ImageData"), System.IO.Stream)
        Me.Picture.Left = 0.5!
        Me.Picture.LineColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Picture.Name = "Picture"
        Me.Picture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom
        Me.Picture.Top = 0.25!
        Me.Picture.Width = 1.3125!
        '
        'Line
        '
        Me.Line.Height = 0!
        Me.Line.Left = 0.5!
        Me.Line.LineColor = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Line.LineWeight = 1.0!
        Me.Line.Name = "Line"
        Me.Line.Top = 1.25!
        Me.Line.Width = 7.5!
        Me.Line.X1 = 0.5!
        Me.Line.X2 = 8.0!
        Me.Line.Y1 = 1.25!
        Me.Line.Y2 = 1.25!
        '
        'RichTextBox
        '
        Me.RichTextBox.AutoReplaceFields = True
        Me.RichTextBox.Font = New System.Drawing.Font("Arial", 10.0!)
        Me.RichTextBox.Height = 0.1875!
        Me.RichTextBox.Left = 1.937!
        Me.RichTextBox.Name = "RichTextBox"
        Me.RichTextBox.RTF = resources.GetString("RichTextBox.RTF")
        Me.RichTextBox.Top = 0.312!
        Me.RichTextBox.Width = 3.6875!
        '
        'Label
        '
        Me.Label.Height = 0.2!
        Me.Label.HyperLink = Nothing
        Me.Label.Left = 1.937!
        Me.Label.Name = "Label"
        Me.Label.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.Label.Text = "5956 W. Las Positas Blvd.,  Pleasanton, CA 94588."
        Me.Label.Top = 0.562!
        Me.Label.Width = 3.625!
        '
        'Label1
        '
        Me.Label1.Height = 0.2!
        Me.Label1.HyperLink = ""
        Me.Label1.Left = 1.937!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "text-decoration: underline; ddo-char-set: 0"
        Me.Label1.Text = "www.strongtie.com"
        Me.Label1.Top = 0.937!
        Me.Label1.Width = 1.5!
        '
        'label6
        '
        Me.label6.Height = 0.2!
        Me.label6.HyperLink = Nothing
        Me.label6.Left = 1.937!
        Me.label6.Name = "label6"
        Me.label6.Style = "font-size: 8.25pt; ddo-char-set: 0"
        Me.label6.Text = "(800) 999-5099"
        Me.label6.Top = 0.7495!
        Me.label6.Width = 3.625!
        '
        'PageFooter1
        '
        Me.PageFooter1.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.lbVersion, Me.Line5})
        Me.PageFooter1.Height = 0.5!
        Me.PageFooter1.Name = "PageFooter1"
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.01!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(8.5!, 0.01!)
        Me.PageBreak1.Top = 8.552!
        Me.PageBreak1.Width = 8.5!
        '
        'PageBreak2
        '
        Me.PageBreak2.Height = 0.01!
        Me.PageBreak2.Left = 0!
        Me.PageBreak2.Name = "PageBreak2"
        Me.PageBreak2.Size = New System.Drawing.SizeF(8.5!, 0.01!)
        Me.PageBreak2.Top = 8.779!
        Me.PageBreak2.Width = 8.5!
        '
        'SubReport2
        '
        Me.SubReport2.CloseBorder = False
        Me.SubReport2.Height = 0.124!
        Me.SubReport2.Left = 0.5!
        Me.SubReport2.Name = "SubReport2"
        Me.SubReport2.Report = Nothing
        Me.SubReport2.Top = 8.779!
        Me.SubReport2.Width = 8.0!
        '
        'SR_Sum0V
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.5!
        Me.ScriptLanguage = "VB.NET"
        Me.Sections.Add(Me.PageHeader1)
        Me.Sections.Add(Me.Detail)
        Me.Sections.Add(Me.PageFooter1)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        Me.WatermarkAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbCompCapFor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbPrepFor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDesignBy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbDatePrinted, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lbVersion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Picture, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents SR_LineInfo_OnPage As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents lbVersion As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents PageHeader1 As GrapeCity.ActiveReports.SectionReportModel.PageHeader
    Private WithEvents PageFooter1 As GrapeCity.ActiveReports.SectionReportModel.PageFooter
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbCompCapFor As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbPrepFor As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Picture As GrapeCity.ActiveReports.SectionReportModel.Picture
    Private WithEvents Line As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents RichTextBox As GrapeCity.ActiveReports.SectionReportModel.RichTextBox
    Private WithEvents Label As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbDesignBy As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents lbDatePrinted As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents SubReport1 As GrapeCity.ActiveReports.SectionReportModel.SubReport
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents PageBreak2 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents SubReport2 As GrapeCity.ActiveReports.SectionReportModel.SubReport
End Class
