﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class US_CC5
    Inherits GrapeCity.ActiveReports.SectionReport

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
        End If
        MyBase.Dispose(disposing)
    End Sub

    'NOTE: The following procedure is required by the ActiveReports Designer
    'It can be modified using the ActiveReports Designer.
    'Do not modify it using the code editor.
    Private WithEvents Detail As GrapeCity.ActiveReports.SectionReportModel.Detail

    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(US_CC5))
        Me.Detail = New GrapeCity.ActiveReports.SectionReportModel.Detail()
        Me.S319_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_037 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Shape1 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape5 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape6 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.Shape2 = New GrapeCity.ActiveReports.SectionReportModel.Shape()
        Me.S319_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label11 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label750 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label599 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label598 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label600 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label603 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label605 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line156 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line157 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line158 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label609 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label610 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label613 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label614 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label617 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label618 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label621 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label622 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label625 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label626 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label629 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label630 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label633 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label634 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label637 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label638 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label641 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label642 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S313_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line159 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line160 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line161 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line162 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line163 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line164 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line165 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line166 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line167 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line168 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line169 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line171 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line172 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label602 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line170 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label666 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label645 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label646 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label647 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label648 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label650 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label652 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label653 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label655 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label656 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label658 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S314_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label662 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label663 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line173 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line174 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line178 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line179 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label664 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label667 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label668 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line180 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line181 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line175 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line176 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line177 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label675 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label676 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label678 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label679 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label681 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label682 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label684 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label685 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label687 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label691 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label692 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label693 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label694 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label695 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label697 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line183 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line184 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label701 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label702 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label705 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label706 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label709 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label710 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_023 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label713 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label714 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label717 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label718 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label721 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label722 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label725 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label726 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label729 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label730 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label733 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label734 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line186 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line187 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line188 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line189 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line190 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line191 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line192 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line193 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line194 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line198 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label737 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label738 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label739 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label742 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label743 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label746 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label747 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label751 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label754 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label755 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label758 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label759 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_035 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label762 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label763 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S317_036 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line199 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line200 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line201 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line202 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line203 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line204 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line205 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line196 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line195 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label767 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line206 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line197 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label768 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label769 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label770 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label771 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label773 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line208 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line209 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line210 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label777 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label778 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label781 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label782 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label785 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label786 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label789 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label790 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S318_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line211 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line212 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line213 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line214 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label793 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line215 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line216 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line217 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line218 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label794 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line219 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label796 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S315_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line1 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line2 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line3 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line4 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line5 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line6 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line7 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line8 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label2 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label3 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label5 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label7 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line9 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line10 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line11 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label12 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_017 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label16 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_018 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label20 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_019 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label24 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_020 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line12 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line13 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line14 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line15 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line20 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label27 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line22 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S319_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_021 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_022 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line24 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label34 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label35 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_031 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_030 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label38 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label39 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label40 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_024 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label42 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_025 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_034 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_035 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line25 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line26 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line27 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label46 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_026 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_036 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label49 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_027 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_037 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line28 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line29 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label52 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line34 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line35 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.S319_032 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label54 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_028 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_038 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_033 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label58 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_029 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S319_039 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line18 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line30 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line31 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line32 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line33 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line36 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line37 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line38 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line19 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line16 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line39 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line17 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line21 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label1 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label4 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label6 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label8 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_001 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label10 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_002 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_008 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_009 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line41 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line42 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label17 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label18 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_003 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_010 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label22 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label23 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_004 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_011 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label28 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label29 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_005 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_012 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label32 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label33 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_006 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_013 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label41 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label43 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_007 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_014 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line43 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line44 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line45 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line46 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line47 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line48 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label47 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line50 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line52 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Label9 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Label13 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_015 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.S3161_016 = New GrapeCity.ActiveReports.SectionReportModel.Label()
        Me.Line53 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line49 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line51 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.Line23 = New GrapeCity.ActiveReports.SectionReportModel.Line()
        Me.PageBreak1 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        Me.PageBreak2 = New GrapeCity.ActiveReports.SectionReportModel.PageBreak()
        CType(Me.S319_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_037, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label750, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label599, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label598, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label600, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label603, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label605, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label609, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label610, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label613, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label614, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label617, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label618, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label621, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label622, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label625, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label626, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label629, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label630, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label633, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label634, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label637, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label638, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label641, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label642, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S313_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label602, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label666, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label645, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label646, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label647, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label648, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label650, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label652, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label653, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label655, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label656, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label658, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S314_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label662, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label663, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label664, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label667, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label668, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label675, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label676, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label678, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label679, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label681, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label682, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label684, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label685, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label687, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label691, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label692, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label693, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label694, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label695, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label697, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label701, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label702, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label705, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label706, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label709, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label710, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_023, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label713, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label714, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label717, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label718, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label721, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label722, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label725, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label726, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label729, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label730, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label733, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label734, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label737, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label738, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label739, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label742, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label743, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label746, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label747, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label751, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label754, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label755, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_034, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label758, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label759, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_035, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label762, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label763, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S317_036, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label767, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label768, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label769, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label770, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label771, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label773, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label777, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label778, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label781, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label782, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label785, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label786, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label789, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label790, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S318_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label793, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label794, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label796, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S315_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_017, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_018, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_019, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_020, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_021, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_022, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_031, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_030, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_024, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_025, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_034, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_035, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_026, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_036, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_027, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_037, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_032, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_028, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_038, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_033, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_029, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S319_039, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_001, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_002, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_008, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_009, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_003, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_010, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_004, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_011, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_005, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_012, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_006, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_013, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_007, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_014, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_015, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.S3161_016, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Controls.AddRange(New GrapeCity.ActiveReports.SectionReportModel.ARControl() {Me.S319_014, Me.S313_023, Me.S314_010, Me.S314_007, Me.S319_023, Me.S317_037, Me.S318_013, Me.Shape1, Me.Shape5, Me.Shape6, Me.Shape2, Me.S319_012, Me.S319_011, Me.S319_010, Me.S319_009, Me.Label11, Me.Label750, Me.Label599, Me.Label598, Me.Label600, Me.Label603, Me.S313_001, Me.Label605, Me.S313_002, Me.S313_012, Me.S313_013, Me.Line156, Me.Line157, Me.Line158, Me.Label609, Me.Label610, Me.S313_003, Me.S313_014, Me.Label613, Me.Label614, Me.S313_004, Me.S313_015, Me.Label617, Me.Label618, Me.S313_005, Me.S313_016, Me.Label621, Me.Label622, Me.S313_006, Me.S313_017, Me.Label625, Me.Label626, Me.S313_007, Me.S313_018, Me.Label629, Me.Label630, Me.S313_008, Me.S313_019, Me.Label633, Me.Label634, Me.S313_009, Me.S313_020, Me.Label637, Me.Label638, Me.S313_010, Me.S313_021, Me.Label641, Me.Label642, Me.S313_011, Me.S313_022, Me.Line159, Me.Line160, Me.Line161, Me.Line162, Me.Line163, Me.Line164, Me.Line165, Me.Line166, Me.Line167, Me.Line168, Me.Line169, Me.Line171, Me.Line172, Me.Label602, Me.Line170, Me.Label666, Me.S314_004, Me.Label645, Me.Label646, Me.Label647, Me.Label648, Me.S314_001, Me.Label650, Me.S314_002, Me.Label652, Me.Label653, Me.S314_003, Me.Label655, Me.Label656, Me.S314_005, Me.Label658, Me.S314_006, Me.S314_008, Me.S314_009, Me.Label662, Me.Label663, Me.Line173, Me.Line174, Me.Line178, Me.Line179, Me.Label664, Me.Label667, Me.Label668, Me.Line180, Me.Line181, Me.Line175, Me.Line176, Me.Line177, Me.Label675, Me.Label676, Me.S315_001, Me.Label678, Me.Label679, Me.S315_002, Me.Label681, Me.Label682, Me.S315_003, Me.Label684, Me.Label685, Me.Label687, Me.S315_004, Me.Label691, Me.Label692, Me.Label693, Me.Label694, Me.Label695, Me.S317_001, Me.Label697, Me.S317_002, Me.S317_019, Me.S317_020, Me.Line183, Me.Line184, Me.Label701, Me.Label702, Me.S317_003, Me.S317_021, Me.Label705, Me.Label706, Me.S317_004, Me.S317_022, Me.Label709, Me.Label710, Me.S317_005, Me.S317_023, Me.Label713, Me.Label714, Me.S317_006, Me.S317_024, Me.Label717, Me.Label718, Me.S317_007, Me.S317_025, Me.Label721, Me.Label722, Me.S317_008, Me.S317_026, Me.Label725, Me.Label726, Me.S317_009, Me.S317_027, Me.Label729, Me.Label730, Me.S317_010, Me.S317_028, Me.Label733, Me.Label734, Me.S317_011, Me.S317_029, Me.Line186, Me.Line187, Me.Line188, Me.Line189, Me.Line190, Me.Line191, Me.Line192, Me.Line193, Me.Line194, Me.Line198, Me.Label737, Me.Label738, Me.Label739, Me.S317_012, Me.S317_030, Me.Label742, Me.Label743, Me.S317_013, Me.S317_031, Me.Label746, Me.Label747, Me.S317_014, Me.S317_032, Me.Label751, Me.S317_015, Me.S317_033, Me.Label754, Me.Label755, Me.S317_016, Me.S317_034, Me.Label758, Me.Label759, Me.S317_017, Me.S317_035, Me.Label762, Me.Label763, Me.S317_018, Me.S317_036, Me.Line199, Me.Line200, Me.Line201, Me.Line202, Me.Line203, Me.Line204, Me.Line205, Me.Line196, Me.Line195, Me.Label767, Me.Line206, Me.Line197, Me.Label768, Me.Label769, Me.Label770, Me.Label771, Me.S318_001, Me.Label773, Me.S318_002, Me.S318_007, Me.S318_008, Me.Line208, Me.Line209, Me.Line210, Me.Label777, Me.Label778, Me.S318_003, Me.S318_009, Me.Label781, Me.Label782, Me.S318_004, Me.S318_010, Me.Label785, Me.Label786, Me.S318_005, Me.S318_011, Me.Label789, Me.Label790, Me.S318_006, Me.S318_012, Me.Line211, Me.Line212, Me.Line213, Me.Line214, Me.Label793, Me.Line215, Me.Line216, Me.Line217, Me.Line218, Me.Label794, Me.Line219, Me.Label796, Me.S315_005, Me.S315_006, Me.S315_007, Me.S315_008, Me.Line1, Me.Line2, Me.Line3, Me.Line4, Me.Line5, Me.Line6, Me.Line7, Me.Line8, Me.Label2, Me.Label3, Me.Label5, Me.S319_001, Me.Label7, Me.S319_002, Me.S319_015, Me.S319_016, Me.Line9, Me.Line10, Me.Line11, Me.Label12, Me.S319_003, Me.S319_017, Me.Label16, Me.S319_004, Me.S319_018, Me.Label20, Me.S319_005, Me.S319_019, Me.Label24, Me.S319_006, Me.S319_020, Me.Line12, Me.Line13, Me.Line14, Me.Line15, Me.Line20, Me.Label27, Me.Line22, Me.S319_013, Me.S319_007, Me.S319_021, Me.S319_008, Me.S319_022, Me.Line24, Me.Label34, Me.Label35, Me.S319_031, Me.S319_030, Me.Label38, Me.Label39, Me.Label40, Me.S319_024, Me.Label42, Me.S319_025, Me.S319_034, Me.S319_035, Me.Line25, Me.Line26, Me.Line27, Me.Label46, Me.S319_026, Me.S319_036, Me.Label49, Me.S319_027, Me.S319_037, Me.Line28, Me.Line29, Me.Label52, Me.Line34, Me.Line35, Me.S319_032, Me.Label54, Me.S319_028, Me.S319_038, Me.S319_033, Me.Label58, Me.S319_029, Me.S319_039, Me.Line18, Me.Line30, Me.Line31, Me.Line32, Me.Line33, Me.Line36, Me.Line37, Me.Line38, Me.Line19, Me.Line16, Me.Line39, Me.Line17, Me.Line21, Me.Label1, Me.Label4, Me.Label6, Me.Label8, Me.S3161_001, Me.Label10, Me.S3161_002, Me.S3161_008, Me.S3161_009, Me.Line41, Me.Line42, Me.Label17, Me.Label18, Me.S3161_003, Me.S3161_010, Me.Label22, Me.Label23, Me.S3161_004, Me.S3161_011, Me.Label28, Me.Label29, Me.S3161_005, Me.S3161_012, Me.Label32, Me.Label33, Me.S3161_006, Me.S3161_013, Me.Label41, Me.Label43, Me.S3161_007, Me.S3161_014, Me.Line43, Me.Line44, Me.Line45, Me.Line46, Me.Line47, Me.Line48, Me.Label47, Me.Line50, Me.Line52, Me.Label9, Me.Label13, Me.S3161_015, Me.S3161_016, Me.Line53, Me.Line49, Me.Line51, Me.Line23, Me.PageBreak1, Me.PageBreak2})
        Me.Detail.Height = 16.42283!
        Me.Detail.Name = "Detail"
        '
        'S319_014
        '
        Me.S319_014.Height = 0.1875!
        Me.S319_014.HyperLink = Nothing
        Me.S319_014.Left = 2.664!
        Me.S319_014.Name = "S319_014"
        Me.S319_014.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_014.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_014.Top = 15.965!
        Me.S319_014.Width = 1.004!
        '
        'S313_023
        '
        Me.S313_023.Height = 0.1875!
        Me.S313_023.HyperLink = Nothing
        Me.S313_023.Left = 3.25!
        Me.S313_023.Name = "S313_023"
        Me.S313_023.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S313_023.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_023.Top = 2.5!
        Me.S313_023.Width = 1.0!
        '
        'S314_010
        '
        Me.S314_010.Height = 0.1875!
        Me.S314_010.HyperLink = Nothing
        Me.S314_010.Left = 3.263999!
        Me.S314_010.Name = "S314_010"
        Me.S314_010.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S314_010.Text = "0.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_010.Top = 4.323!
        Me.S314_010.Width = 1.0!
        '
        'S314_007
        '
        Me.S314_007.Height = 0.1875!
        Me.S314_007.HyperLink = Nothing
        Me.S314_007.Left = 2.263999!
        Me.S314_007.Name = "S314_007"
        Me.S314_007.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S314_007.Text = "0.000" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_007.Top = 4.323!
        Me.S314_007.Width = 1.0!
        '
        'S319_023
        '
        Me.S319_023.Height = 0.1875!
        Me.S319_023.HyperLink = Nothing
        Me.S319_023.Left = 3.644!
        Me.S319_023.Name = "S319_023"
        Me.S319_023.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S319_023.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_023.Top = 16.152!
        Me.S319_023.Width = 0.7670002!
        '
        'S317_037
        '
        Me.S317_037.Height = 0.1875!
        Me.S317_037.HyperLink = Nothing
        Me.S317_037.Left = 3.263!
        Me.S317_037.Name = "S317_037"
        Me.S317_037.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S317_037.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_037.Top = 9.814!
        Me.S317_037.Width = 1.0!
        '
        'S318_013
        '
        Me.S318_013.Height = 0.1875!
        Me.S318_013.HyperLink = Nothing
        Me.S318_013.Left = 3.262!
        Me.S318_013.Name = "S318_013"
        Me.S318_013.Style = "background-color: Yellow; color: Blue; font-size: 8.25pt; font-weight: bold; text" &
    "-align: center; vertical-align: middle; ddo-char-set: 0"
        Me.S318_013.Text = "OK" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_013.Top = 11.728!
        Me.S318_013.Width = 1.0!
        '
        'Shape1
        '
        Me.Shape1.Height = 0.1875!
        Me.Shape1.Left = 3.252!
        Me.Shape1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape1.Name = "Shape1"
        Me.Shape1.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape1.Top = 2.5!
        Me.Shape1.Width = 1.0!
        '
        'Shape5
        '
        Me.Shape5.Height = 0.1875!
        Me.Shape5.Left = 3.255!
        Me.Shape5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape5.Name = "Shape5"
        Me.Shape5.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape5.Top = 9.814!
        Me.Shape5.Width = 1.0!
        '
        'Shape6
        '
        Me.Shape6.Height = 0.1875!
        Me.Shape6.Left = 3.262!
        Me.Shape6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape6.Name = "Shape6"
        Me.Shape6.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape6.Top = 11.728!
        Me.Shape6.Width = 1.0!
        '
        'Shape2
        '
        Me.Shape2.Height = 0.1875!
        Me.Shape2.Left = 3.644!
        Me.Shape2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Shape2.Name = "Shape2"
        Me.Shape2.RoundingRadius = New GrapeCity.ActiveReports.Controls.CornersRadius(10.0!, Nothing, Nothing, Nothing, Nothing)
        Me.Shape2.Top = 16.152!
        Me.Shape2.Width = 0.7670002!
        '
        'S319_012
        '
        Me.S319_012.Height = 0.1875!
        Me.S319_012.HyperLink = Nothing
        Me.S319_012.Left = 2.664!
        Me.S319_012.Name = "S319_012"
        Me.S319_012.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_012.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_012.Top = 15.578!
        Me.S319_012.Width = 0.994!
        '
        'S319_011
        '
        Me.S319_011.Height = 0.1875!
        Me.S319_011.HyperLink = Nothing
        Me.S319_011.Left = 2.664!
        Me.S319_011.Name = "S319_011"
        Me.S319_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_011.Top = 15.396!
        Me.S319_011.Width = 0.994!
        '
        'S319_010
        '
        Me.S319_010.Height = 0.1875!
        Me.S319_010.HyperLink = Nothing
        Me.S319_010.Left = 2.664!
        Me.S319_010.Name = "S319_010"
        Me.S319_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_010.Top = 15.209!
        Me.S319_010.Width = 0.994!
        '
        'S319_009
        '
        Me.S319_009.Height = 0.1875!
        Me.S319_009.HyperLink = Nothing
        Me.S319_009.Left = 2.664!
        Me.S319_009.Name = "S319_009"
        Me.S319_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_009.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_009.Top = 15.021!
        Me.S319_009.Width = 0.994!
        '
        'Label11
        '
        Me.Label11.Height = 0.1875!
        Me.Label11.HyperLink = Nothing
        Me.Label11.Left = 2.654!
        Me.Label11.Name = "Label11"
        Me.Label11.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label11.Text = "Overall PZ chk:"
        Me.Label11.Top = 14.833!
        Me.Label11.Width = 0.9900002!
        '
        'Label750
        '
        Me.Label750.Height = 0.188!
        Me.Label750.HyperLink = Nothing
        Me.Label750.Left = 4.27!
        Me.Label750.Name = "Label750"
        Me.Label750.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label750.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sqrt (1.1* Mpr/ (fb* Fyc* Yc_stiffened) )"
        Me.Label750.Top = 9.064!
        Me.Label750.Width = 3.4!
        '
        'Label599
        '
        Me.Label599.Height = 0.1875!
        Me.Label599.HyperLink = Nothing
        Me.Label599.Left = 0!
        Me.Label599.Name = "Label599"
        Me.Label599.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label599.Text = "3.13 FULL DEPTH STIFFENER PLATE FOR 2-SIDED MOMENT CONNECTIONS ONLY:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label599.Top = 0!
        Me.Label599.Width = 6.061999!
        '
        'Label598
        '
        Me.Label598.Height = 0.1875!
        Me.Label598.HyperLink = Nothing
        Me.Label598.Left = 2.249!
        Me.Label598.Name = "Label598"
        Me.Label598.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label598.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label598.Top = 0.25!
        Me.Label598.Width = 1.0!
        '
        'Label600
        '
        Me.Label600.Height = 0.1875!
        Me.Label600.HyperLink = Nothing
        Me.Label600.Left = 3.249!
        Me.Label600.Name = "Label600"
        Me.Label600.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label600.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label600.Top = 0.25!
        Me.Label600.Width = 1.0!
        '
        'Label603
        '
        Me.Label603.Height = 0.188!
        Me.Label603.HyperLink = Nothing
        Me.Label603.Left = 0!
        Me.Label603.Name = "Label603"
        Me.Label603.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label603.Text = "Φc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label603.Top = 0.437!
        Me.Label603.Width = 2.25!
        '
        'S313_001
        '
        Me.S313_001.Height = 0.1875!
        Me.S313_001.HyperLink = Nothing
        Me.S313_001.Left = 2.249!
        Me.S313_001.Name = "S313_001"
        Me.S313_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S313_001.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_001.Top = 0.438!
        Me.S313_001.Width = 1.0!
        '
        'Label605
        '
        Me.Label605.Height = 0.1875!
        Me.Label605.HyperLink = Nothing
        Me.Label605.Left = 0!
        Me.Label605.Name = "Label605"
        Me.Label605.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label605.Text = "Kstiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label605.Top = 0.624!
        Me.Label605.Width = 2.25!
        '
        'S313_002
        '
        Me.S313_002.Height = 0.1875!
        Me.S313_002.HyperLink = Nothing
        Me.S313_002.Left = 2.249!
        Me.S313_002.Name = "S313_002"
        Me.S313_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S313_002.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_002.Top = 0.625!
        Me.S313_002.Width = 1.0!
        '
        'S313_012
        '
        Me.S313_012.Height = 0.1875!
        Me.S313_012.HyperLink = Nothing
        Me.S313_012.Left = 3.249!
        Me.S313_012.Name = "S313_012"
        Me.S313_012.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S313_012.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_012.Top = 0.438!
        Me.S313_012.Width = 1.0!
        '
        'S313_013
        '
        Me.S313_013.Height = 0.1875!
        Me.S313_013.HyperLink = Nothing
        Me.S313_013.Left = 3.249!
        Me.S313_013.Name = "S313_013"
        Me.S313_013.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S313_013.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_013.Top = 0.625!
        Me.S313_013.Width = 1.0!
        '
        'Line156
        '
        Me.Line156.Height = 0!
        Me.Line156.Left = 2.249!
        Me.Line156.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line156.LineWeight = 1.0!
        Me.Line156.Name = "Line156"
        Me.Line156.Top = 0.625!
        Me.Line156.Width = 1.999998!
        Me.Line156.X1 = 2.249!
        Me.Line156.X2 = 4.248998!
        Me.Line156.Y1 = 0.625!
        Me.Line156.Y2 = 0.625!
        '
        'Line157
        '
        Me.Line157.Height = 0!
        Me.Line157.Left = 2.249!
        Me.Line157.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line157.LineWeight = 1.0!
        Me.Line157.Name = "Line157"
        Me.Line157.Top = 0.438!
        Me.Line157.Width = 1.999998!
        Me.Line157.X1 = 2.249!
        Me.Line157.X2 = 4.248998!
        Me.Line157.Y1 = 0.438!
        Me.Line157.Y2 = 0.438!
        '
        'Line158
        '
        Me.Line158.Height = 0!
        Me.Line158.Left = 2.241!
        Me.Line158.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line158.LineWeight = 1.0!
        Me.Line158.Name = "Line158"
        Me.Line158.Top = 1.0!
        Me.Line158.Width = 1.999998!
        Me.Line158.X1 = 2.241!
        Me.Line158.X2 = 4.240998!
        Me.Line158.Y1 = 1.0!
        Me.Line158.Y2 = 1.0!
        '
        'Label609
        '
        Me.Label609.Height = 0.1875!
        Me.Label609.HyperLink = Nothing
        Me.Label609.Left = 4.25!
        Me.Label609.Name = "Label609"
        Me.Label609.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label609.Text = "in^2" & Global.Microsoft.VisualBasic.ChrW(9) & "= 2* bstiff* tstiff + 12* tcw^2" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label609.Top = 0.813!
        Me.Label609.Width = 3.759002!
        '
        'Label610
        '
        Me.Label610.Height = 0.1875!
        Me.Label610.HyperLink = Nothing
        Me.Label610.Left = 0!
        Me.Label610.Name = "Label610"
        Me.Label610.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label610.Text = "Astiff_fd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label610.Top = 0.812!
        Me.Label610.Width = 2.25!
        '
        'S313_003
        '
        Me.S313_003.Height = 0.1875!
        Me.S313_003.HyperLink = Nothing
        Me.S313_003.Left = 2.255!
        Me.S313_003.Name = "S313_003"
        Me.S313_003.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_003.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_003.Top = 0.813!
        Me.S313_003.Width = 1.0!
        '
        'S313_014
        '
        Me.S313_014.Height = 0.1875!
        Me.S313_014.HyperLink = Nothing
        Me.S313_014.Left = 3.255!
        Me.S313_014.Name = "S313_014"
        Me.S313_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_014.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_014.Top = 0.813!
        Me.S313_014.Width = 1.0!
        '
        'Label613
        '
        Me.Label613.Height = 0.1875!
        Me.Label613.HyperLink = Nothing
        Me.Label613.Left = 4.25!
        Me.Label613.Name = "Label613"
        Me.Label613.Style = "font-size: 6.5pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label613.Text = "in^4              = 12* tcw* tcw^3/12 + 2* (tstiff* bstiff^3/ 12+ bstiff* tstiff*" &
    " (bstiff/2 +tc/2)^2 )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label613.Top = 1.001!
        Me.Label613.Width = 3.754!
        '
        'Label614
        '
        Me.Label614.Height = 0.1875!
        Me.Label614.HyperLink = Nothing
        Me.Label614.Left = 0!
        Me.Label614.Name = "Label614"
        Me.Label614.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label614.Text = "Istiff_fd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label614.Top = 0.999!
        Me.Label614.Width = 2.25!
        '
        'S313_004
        '
        Me.S313_004.Height = 0.1875!
        Me.S313_004.HyperLink = Nothing
        Me.S313_004.Left = 2.255001!
        Me.S313_004.Name = "S313_004"
        Me.S313_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_004.Top = 1.001!
        Me.S313_004.Width = 1.0!
        '
        'S313_015
        '
        Me.S313_015.Height = 0.1875!
        Me.S313_015.HyperLink = Nothing
        Me.S313_015.Left = 3.255!
        Me.S313_015.Name = "S313_015"
        Me.S313_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_015.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_015.Top = 1.001!
        Me.S313_015.Width = 1.0!
        '
        'Label617
        '
        Me.Label617.Height = 0.1875!
        Me.Label617.HyperLink = Nothing
        Me.Label617.Left = 4.25!
        Me.Label617.Name = "Label617"
        Me.Label617.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label617.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sqrt (Istiff_fd/ Astiff_fd)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label617.Top = 1.188!
        Me.Label617.Width = 3.751002!
        '
        'Label618
        '
        Me.Label618.Height = 0.1875!
        Me.Label618.HyperLink = Nothing
        Me.Label618.Left = 0!
        Me.Label618.Name = "Label618"
        Me.Label618.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label618.Text = "rstiff_fd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label618.Top = 1.187!
        Me.Label618.Width = 2.25!
        '
        'S313_005
        '
        Me.S313_005.Height = 0.1875!
        Me.S313_005.HyperLink = Nothing
        Me.S313_005.Left = 2.249!
        Me.S313_005.Name = "S313_005"
        Me.S313_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_005.Top = 1.188!
        Me.S313_005.Width = 1.0!
        '
        'S313_016
        '
        Me.S313_016.Height = 0.1875!
        Me.S313_016.HyperLink = Nothing
        Me.S313_016.Left = 3.249!
        Me.S313_016.Name = "S313_016"
        Me.S313_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_016.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_016.Top = 1.188!
        Me.S313_016.Width = 1.0!
        '
        'Label621
        '
        Me.Label621.Height = 0.1875!
        Me.Label621.HyperLink = Nothing
        Me.Label621.Left = 4.25!
        Me.Label621.Name = "Label621"
        Me.Label621.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label621.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= Kstiff* Lstiff_fd/ rstiff_fd" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label621.Top = 1.375!
        Me.Label621.Width = 3.746003!
        '
        'Label622
        '
        Me.Label622.Height = 0.1875!
        Me.Label622.HyperLink = Nothing
        Me.Label622.Left = 0.00000002980232!
        Me.Label622.Name = "Label622"
        Me.Label622.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label622.Text = "Kstiff*Lstiff_fd/rstiff_fd=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label622.Top = 1.374!
        Me.Label622.Width = 2.25!
        '
        'S313_006
        '
        Me.S313_006.Height = 0.1875!
        Me.S313_006.HyperLink = Nothing
        Me.S313_006.Left = 2.254!
        Me.S313_006.Name = "S313_006"
        Me.S313_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_006.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_006.Top = 1.375!
        Me.S313_006.Width = 1.0!
        '
        'S313_017
        '
        Me.S313_017.Height = 0.1875!
        Me.S313_017.HyperLink = Nothing
        Me.S313_017.Left = 3.254!
        Me.S313_017.Name = "S313_017"
        Me.S313_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_017.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_017.Top = 1.375!
        Me.S313_017.Width = 1.0!
        '
        'Label625
        '
        Me.Label625.Height = 0.1875!
        Me.Label625.HyperLink = Nothing
        Me.Label625.Left = 4.25!
        Me.Label625.Name = "Label625"
        Me.Label625.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label625.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(9) & "= Pi^2* 29000/ (Kstiff* Lstiff_fd/ rstiff_fd)^2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label625.Top = 1.563!
        Me.Label625.Width = 3.751999!
        '
        'Label626
        '
        Me.Label626.Height = 0.1875!
        Me.Label626.HyperLink = Nothing
        Me.Label626.Left = 0!
        Me.Label626.Name = "Label626"
        Me.Label626.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label626.Text = "Fe=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label626.Top = 1.562!
        Me.Label626.Width = 2.25!
        '
        'S313_007
        '
        Me.S313_007.Height = 0.1875!
        Me.S313_007.HyperLink = Nothing
        Me.S313_007.Left = 2.249!
        Me.S313_007.Name = "S313_007"
        Me.S313_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_007.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_007.Top = 1.563!
        Me.S313_007.Width = 1.0!
        '
        'S313_018
        '
        Me.S313_018.Height = 0.1875!
        Me.S313_018.HyperLink = Nothing
        Me.S313_018.Left = 3.249!
        Me.S313_018.Name = "S313_018"
        Me.S313_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_018.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_018.Top = 1.563!
        Me.S313_018.Width = 1.0!
        '
        'Label629
        '
        Me.Label629.Height = 0.22!
        Me.Label629.HyperLink = Nothing
        Me.Label629.Left = 4.25!
        Me.Label629.Name = "Label629"
        Me.Label629.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label629.Text = "ksi               = if (Kstiff* Lstiff_fd/ rstiff_fd <= 4.71* Sqrt (29000/ Fy_sti" &
    "ff), " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                           0.658^ (Fy_stiff/ Fc)* Fy_stiff,0.877* Astiff_" &
    "fd)"
        Me.Label629.Top = 1.718!
        Me.Label629.Width = 3.751!
        '
        'Label630
        '
        Me.Label630.Height = 0.1875!
        Me.Label630.HyperLink = Nothing
        Me.Label630.Left = 0.00000002980232!
        Me.Label630.Name = "Label630"
        Me.Label630.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label630.Text = "Fcr=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label630.Top = 1.75!
        Me.Label630.Width = 2.25!
        '
        'S313_008
        '
        Me.S313_008.Height = 0.1875!
        Me.S313_008.HyperLink = Nothing
        Me.S313_008.Left = 2.249999!
        Me.S313_008.Name = "S313_008"
        Me.S313_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_008.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_008.Top = 1.751!
        Me.S313_008.Width = 1.0!
        '
        'S313_019
        '
        Me.S313_019.Height = 0.1875!
        Me.S313_019.HyperLink = Nothing
        Me.S313_019.Left = 3.25!
        Me.S313_019.Name = "S313_019"
        Me.S313_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_019.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_019.Top = 1.751!
        Me.S313_019.Width = 1.0!
        '
        'Label633
        '
        Me.Label633.Height = 0.1875!
        Me.Label633.HyperLink = Nothing
        Me.Label633.Left = 4.25!
        Me.Label633.Name = "Label633"
        Me.Label633.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label633.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Kstiff* Lstiff_fd* rstiff_fd > 25, Fcr* Astiff_fd, Astiff_fd* Fy_stiff" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label633.Top = 1.938!
        Me.Label633.Width = 3.737!
        '
        'Label634
        '
        Me.Label634.Height = 0.1875!
        Me.Label634.HyperLink = Nothing
        Me.Label634.Left = 0.0000003874302!
        Me.Label634.Name = "Label634"
        Me.Label634.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label634.Text = "Pn_stiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label634.Top = 1.937!
        Me.Label634.Width = 2.25!
        '
        'S313_009
        '
        Me.S313_009.Height = 0.1875!
        Me.S313_009.HyperLink = Nothing
        Me.S313_009.Left = 2.255!
        Me.S313_009.Name = "S313_009"
        Me.S313_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S313_009.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_009.Top = 1.938!
        Me.S313_009.Width = 1.0!
        '
        'S313_020
        '
        Me.S313_020.Height = 0.1875!
        Me.S313_020.HyperLink = Nothing
        Me.S313_020.Left = 3.254999!
        Me.S313_020.Name = "S313_020"
        Me.S313_020.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S313_020.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_020.Top = 1.938!
        Me.S313_020.Width = 1.0!
        '
        'Label637
        '
        Me.Label637.Height = 0.1875!
        Me.Label637.HyperLink = Nothing
        Me.Label637.Left = 4.25!
        Me.Label637.Name = "Label637"
        Me.Label637.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label637.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "= Φc* Pn_stiff" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label637.Top = 2.125!
        Me.Label637.Width = 3.737!
        '
        'Label638
        '
        Me.Label638.Height = 0.1875!
        Me.Label638.HyperLink = Nothing
        Me.Label638.Left = 0!
        Me.Label638.Name = "Label638"
        Me.Label638.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label638.Text = "Φc*Pn_stiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label638.Top = 2.124!
        Me.Label638.Width = 2.25!
        '
        'S313_010
        '
        Me.S313_010.Height = 0.1875!
        Me.S313_010.HyperLink = Nothing
        Me.S313_010.Left = 2.255!
        Me.S313_010.Name = "S313_010"
        Me.S313_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_010.Top = 2.125!
        Me.S313_010.Width = 1.0!
        '
        'S313_021
        '
        Me.S313_021.Height = 0.1875!
        Me.S313_021.HyperLink = Nothing
        Me.S313_021.Left = 3.255!
        Me.S313_021.Name = "S313_021"
        Me.S313_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_021.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_021.Top = 2.125!
        Me.S313_021.Width = 1.0!
        '
        'Label641
        '
        Me.Label641.Height = 0.1875!
        Me.Label641.HyperLink = Nothing
        Me.Label641.Left = 4.25!
        Me.Label641.Name = "Label641"
        Me.Label641.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label641.Text = "" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (SMF_Sides = 1, 0, Fsu/ (fc* Pn_stiff) )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label641.Top = 2.313!
        Me.Label641.Width = 3.737999!
        '
        'Label642
        '
        Me.Label642.Height = 0.1875!
        Me.Label642.HyperLink = Nothing
        Me.Label642.Left = 0!
        Me.Label642.Name = "Label642"
        Me.Label642.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label642.Text = "SUM_stiff_comp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label642.Top = 2.312!
        Me.Label642.Width = 2.25!
        '
        'S313_011
        '
        Me.S313_011.Height = 0.1875!
        Me.S313_011.HyperLink = Nothing
        Me.S313_011.Left = 2.249!
        Me.S313_011.Name = "S313_011"
        Me.S313_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_011.Top = 2.313!
        Me.S313_011.Width = 1.0!
        '
        'S313_022
        '
        Me.S313_022.Height = 0.1875!
        Me.S313_022.HyperLink = Nothing
        Me.S313_022.Left = 3.249!
        Me.S313_022.Name = "S313_022"
        Me.S313_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S313_022.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S313_022.Top = 2.313!
        Me.S313_022.Width = 1.0!
        '
        'Line159
        '
        Me.Line159.Height = 0!
        Me.Line159.Left = 2.251!
        Me.Line159.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line159.LineWeight = 1.0!
        Me.Line159.Name = "Line159"
        Me.Line159.Top = 0.813!
        Me.Line159.Width = 1.999999!
        Me.Line159.X1 = 2.251!
        Me.Line159.X2 = 4.250999!
        Me.Line159.Y1 = 0.813!
        Me.Line159.Y2 = 0.813!
        '
        'Line160
        '
        Me.Line160.Height = 0!
        Me.Line160.Left = 2.245!
        Me.Line160.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line160.LineWeight = 1.0!
        Me.Line160.Name = "Line160"
        Me.Line160.Top = 1.001!
        Me.Line160.Width = 1.999997!
        Me.Line160.X1 = 2.245!
        Me.Line160.X2 = 4.244997!
        Me.Line160.Y1 = 1.001!
        Me.Line160.Y2 = 1.001!
        '
        'Line161
        '
        Me.Line161.Height = 0!
        Me.Line161.Left = 2.25!
        Me.Line161.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line161.LineWeight = 1.0!
        Me.Line161.Name = "Line161"
        Me.Line161.Top = 1.188!
        Me.Line161.Width = 1.999999!
        Me.Line161.X1 = 2.25!
        Me.Line161.X2 = 4.249999!
        Me.Line161.Y1 = 1.188!
        Me.Line161.Y2 = 1.188!
        '
        'Line162
        '
        Me.Line162.Height = 0!
        Me.Line162.Left = 2.255!
        Me.Line162.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line162.LineWeight = 1.0!
        Me.Line162.Name = "Line162"
        Me.Line162.Top = 1.375!
        Me.Line162.Width = 1.999997!
        Me.Line162.X1 = 2.255!
        Me.Line162.X2 = 4.254997!
        Me.Line162.Y1 = 1.375!
        Me.Line162.Y2 = 1.375!
        '
        'Line163
        '
        Me.Line163.Height = 0!
        Me.Line163.Left = 2.255!
        Me.Line163.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line163.LineWeight = 1.0!
        Me.Line163.Name = "Line163"
        Me.Line163.Top = 1.563!
        Me.Line163.Width = 1.999999!
        Me.Line163.X1 = 2.255!
        Me.Line163.X2 = 4.254999!
        Me.Line163.Y1 = 1.563!
        Me.Line163.Y2 = 1.563!
        '
        'Line164
        '
        Me.Line164.Height = 0!
        Me.Line164.Left = 2.245!
        Me.Line164.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line164.LineWeight = 1.0!
        Me.Line164.Name = "Line164"
        Me.Line164.Top = 1.751!
        Me.Line164.Width = 1.999998!
        Me.Line164.X1 = 2.245!
        Me.Line164.X2 = 4.244998!
        Me.Line164.Y1 = 1.751!
        Me.Line164.Y2 = 1.751!
        '
        'Line165
        '
        Me.Line165.Height = 0!
        Me.Line165.Left = 2.245!
        Me.Line165.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line165.LineWeight = 1.0!
        Me.Line165.Name = "Line165"
        Me.Line165.Top = 1.938!
        Me.Line165.Width = 1.999999!
        Me.Line165.X1 = 2.245!
        Me.Line165.X2 = 4.244999!
        Me.Line165.Y1 = 1.938!
        Me.Line165.Y2 = 1.938!
        '
        'Line166
        '
        Me.Line166.Height = 0!
        Me.Line166.Left = 2.252!
        Me.Line166.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line166.LineWeight = 1.0!
        Me.Line166.Name = "Line166"
        Me.Line166.Top = 2.125!
        Me.Line166.Width = 1.999998!
        Me.Line166.X1 = 2.252!
        Me.Line166.X2 = 4.251998!
        Me.Line166.Y1 = 2.125!
        Me.Line166.Y2 = 2.125!
        '
        'Line167
        '
        Me.Line167.Height = 0!
        Me.Line167.Left = 2.255!
        Me.Line167.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line167.LineWeight = 1.0!
        Me.Line167.Name = "Line167"
        Me.Line167.Top = 2.313!
        Me.Line167.Width = 1.999997!
        Me.Line167.X1 = 2.255!
        Me.Line167.X2 = 4.254997!
        Me.Line167.Y1 = 2.313!
        Me.Line167.Y2 = 2.313!
        '
        'Line168
        '
        Me.Line168.Height = 2.25!
        Me.Line168.Left = 3.249!
        Me.Line168.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line168.LineWeight = 1.0!
        Me.Line168.Name = "Line168"
        Me.Line168.Top = 0.25!
        Me.Line168.Width = 0!
        Me.Line168.X1 = 3.249!
        Me.Line168.X2 = 3.249!
        Me.Line168.Y1 = 0.25!
        Me.Line168.Y2 = 2.5!
        '
        'Line169
        '
        Me.Line169.Height = 2.25!
        Me.Line169.Left = 2.249!
        Me.Line169.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line169.LineWeight = 1.0!
        Me.Line169.Name = "Line169"
        Me.Line169.Top = 0.25!
        Me.Line169.Width = 0!
        Me.Line169.X1 = 2.249!
        Me.Line169.X2 = 2.249!
        Me.Line169.Y1 = 0.25!
        Me.Line169.Y2 = 2.5!
        '
        'Line171
        '
        Me.Line171.Height = 0!
        Me.Line171.Left = 2.245!
        Me.Line171.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line171.LineWeight = 1.0!
        Me.Line171.Name = "Line171"
        Me.Line171.Top = 0.25!
        Me.Line171.Width = 1.999996!
        Me.Line171.X1 = 2.245!
        Me.Line171.X2 = 4.244996!
        Me.Line171.Y1 = 0.25!
        Me.Line171.Y2 = 0.25!
        '
        'Line172
        '
        Me.Line172.Height = 0!
        Me.Line172.Left = 2.247!
        Me.Line172.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line172.LineWeight = 1.0!
        Me.Line172.Name = "Line172"
        Me.Line172.Top = 2.5!
        Me.Line172.Width = 1.999997!
        Me.Line172.X1 = 2.247!
        Me.Line172.X2 = 4.246997!
        Me.Line172.Y1 = 2.5!
        Me.Line172.Y2 = 2.5!
        '
        'Label602
        '
        Me.Label602.Height = 0.1875!
        Me.Label602.HyperLink = Nothing
        Me.Label602.Left = 4.25!
        Me.Label602.Name = "Label602"
        Me.Label602.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label602.Text = "                 OK if SUM_Stiff_comp < SUM_Allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label602.Top = 2.5!
        Me.Label602.Width = 3.737!
        '
        'Line170
        '
        Me.Line170.Height = 2.25!
        Me.Line170.Left = 4.248998!
        Me.Line170.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line170.LineWeight = 1.0!
        Me.Line170.Name = "Line170"
        Me.Line170.Top = 0.25!
        Me.Line170.Width = 0!
        Me.Line170.X1 = 4.248998!
        Me.Line170.X2 = 4.248998!
        Me.Line170.Y1 = 0.25!
        Me.Line170.Y2 = 2.5!
        '
        'Label666
        '
        Me.Label666.Height = 0.1875!
        Me.Label666.HyperLink = Nothing
        Me.Label666.Left = 3.259002!
        Me.Label666.Name = "Label666"
        Me.Label666.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label666.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Minimum fillet weld size per AISC 360 Table J2.4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label666.Top = 3.572!
        Me.Label666.Width = 4.75!
        '
        'S314_004
        '
        Me.S314_004.Height = 0.1875!
        Me.S314_004.HyperLink = Nothing
        Me.S314_004.Left = 2.259!
        Me.S314_004.Name = "S314_004"
        Me.S314_004.Style = "color: DarkRed; font-size: 8.25pt; font-weight: normal; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 1"
        Me.S314_004.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_004.Top = 3.572!
        Me.S314_004.Width = 1.0!
        '
        'Label645
        '
        Me.Label645.Height = 0.1875!
        Me.Label645.HyperLink = Nothing
        Me.Label645.Left = 0!
        Me.Label645.Name = "Label645"
        Me.Label645.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label645.Text = "3.14 STIFFENER PLATE DOUBLE SIDE FILLET WELD TO COLUMN FLANGE:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label645.Top = 2.823!
        Me.Label645.Width = 6.061999!
        '
        'Label646
        '
        Me.Label646.Height = 0.22!
        Me.Label646.HyperLink = Nothing
        Me.Label646.Left = 4.26!
        Me.Label646.Name = "Label646"
        Me.Label646.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label646.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (0.5* Fsu/ SMF_Sides)/ (ffillet* 1.5* 0.6* Fexx* " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                     (bst" &
    "iff - Lclip_flange-0.5)*2^(1/2))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label646.Top = 3.948!
        Me.Label646.Width = 3.74!
        '
        'Label647
        '
        Me.Label647.Height = 0.1875!
        Me.Label647.HyperLink = Nothing
        Me.Label647.Left = 4.263999!
        Me.Label647.Name = "Label647"
        Me.Label647.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label647.Text = "in" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label647.Top = 4.135!
        Me.Label647.Width = 0.5010014!
        '
        'Label648
        '
        Me.Label648.Height = 0.188!
        Me.Label648.HyperLink = Nothing
        Me.Label648.Left = 0!
        Me.Label648.Name = "Label648"
        Me.Label648.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label648.Text = "Φfillet=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label648.Top = 3.0!
        Me.Label648.Width = 2.2!
        '
        'S314_001
        '
        Me.S314_001.Height = 0.1875!
        Me.S314_001.HyperLink = Nothing
        Me.S314_001.Left = 2.263!
        Me.S314_001.Name = "S314_001"
        Me.S314_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S314_001.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_001.Top = 3.01!
        Me.S314_001.Width = 1.0!
        '
        'Label650
        '
        Me.Label650.Height = 0.1875!
        Me.Label650.HyperLink = Nothing
        Me.Label650.Left = 0!
        Me.Label650.Name = "Label650"
        Me.Label650.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label650.Text = "Fexx=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label650.Top = 3.187!
        Me.Label650.Width = 2.246!
        '
        'S314_002
        '
        Me.S314_002.Height = 0.1875!
        Me.S314_002.HyperLink = Nothing
        Me.S314_002.Left = 2.263!
        Me.S314_002.Name = "S314_002"
        Me.S314_002.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S314_002.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_002.Top = 3.197!
        Me.S314_002.Width = 1.0!
        '
        'Label652
        '
        Me.Label652.Height = 0.1875!
        Me.Label652.HyperLink = Nothing
        Me.Label652.Left = 3.263001!
        Me.Label652.Name = "Label652"
        Me.Label652.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label652.Text = "ksi" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label652.Top = 3.197!
        Me.Label652.Width = 4.871999!
        '
        'Label653
        '
        Me.Label653.Height = 0.1875!
        Me.Label653.HyperLink = Nothing
        Me.Label653.Left = 0!
        Me.Label653.Name = "Label653"
        Me.Label653.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label653.Text = "tstiff=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label653.Top = 3.374!
        Me.Label653.Width = 2.246!
        '
        'S314_003
        '
        Me.S314_003.Height = 0.1875!
        Me.S314_003.HyperLink = Nothing
        Me.S314_003.Left = 2.263!
        Me.S314_003.Name = "S314_003"
        Me.S314_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S314_003.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_003.Top = 3.384001!
        Me.S314_003.Width = 1.0!
        '
        'Label655
        '
        Me.Label655.Height = 0.1875!
        Me.Label655.HyperLink = Nothing
        Me.Label655.Left = 3.263001!
        Me.Label655.Name = "Label655"
        Me.Label655.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label655.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label655.Top = 3.384001!
        Me.Label655.Width = 4.871999!
        '
        'Label656
        '
        Me.Label656.Height = 0.1875!
        Me.Label656.HyperLink = Nothing
        Me.Label656.Left = 0!
        Me.Label656.Name = "Label656"
        Me.Label656.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label656.Text = "wfillet_flange_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label656.Top = 3.938!
        Me.Label656.Width = 2.246!
        '
        'S314_005
        '
        Me.S314_005.Height = 0.1875!
        Me.S314_005.HyperLink = Nothing
        Me.S314_005.Left = 2.264001!
        Me.S314_005.Name = "S314_005"
        Me.S314_005.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S314_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_005.Top = 3.948!
        Me.S314_005.Width = 1.0!
        '
        'Label658
        '
        Me.Label658.Height = 0.1875!
        Me.Label658.HyperLink = Nothing
        Me.Label658.Left = 0!
        Me.Label658.Name = "Label658"
        Me.Label658.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label658.Text = "wfillet_flange=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label658.Top = 4.125!
        Me.Label658.Width = 2.246!
        '
        'S314_006
        '
        Me.S314_006.Height = 0.1875!
        Me.S314_006.HyperLink = Nothing
        Me.S314_006.Left = 2.263999!
        Me.S314_006.Name = "S314_006"
        Me.S314_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S314_006.Text = "0.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_006.Top = 4.135!
        Me.S314_006.Width = 1.0!
        '
        'S314_008
        '
        Me.S314_008.Height = 0.1875!
        Me.S314_008.HyperLink = Nothing
        Me.S314_008.Left = 3.264001!
        Me.S314_008.Name = "S314_008"
        Me.S314_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S314_008.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_008.Top = 3.948!
        Me.S314_008.Width = 1.0!
        '
        'S314_009
        '
        Me.S314_009.Height = 0.1875!
        Me.S314_009.HyperLink = Nothing
        Me.S314_009.Left = 3.263999!
        Me.S314_009.Name = "S314_009"
        Me.S314_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S314_009.Text = "0.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S314_009.Top = 4.135!
        Me.S314_009.Width = 1.0!
        '
        'Label662
        '
        Me.Label662.Height = 0.1875!
        Me.Label662.HyperLink = Nothing
        Me.Label662.Left = 3.264!
        Me.Label662.Name = "Label662"
        Me.Label662.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label662.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label662.Top = 3.76!
        Me.Label662.Width = 1.0!
        '
        'Label663
        '
        Me.Label663.Height = 0.1875!
        Me.Label663.HyperLink = Nothing
        Me.Label663.Left = 2.264!
        Me.Label663.Name = "Label663"
        Me.Label663.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label663.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label663.Top = 3.76!
        Me.Label663.Width = 1.0!
        '
        'Line173
        '
        Me.Line173.Height = 0!
        Me.Line173.Left = 2.264!
        Me.Line173.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line173.LineWeight = 1.0!
        Me.Line173.Name = "Line173"
        Me.Line173.Top = 4.135!
        Me.Line173.Width = 2.0!
        Me.Line173.X1 = 2.264!
        Me.Line173.X2 = 4.264!
        Me.Line173.Y1 = 4.135!
        Me.Line173.Y2 = 4.135!
        '
        'Line174
        '
        Me.Line174.Height = 0!
        Me.Line174.Left = 2.264!
        Me.Line174.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line174.LineWeight = 1.0!
        Me.Line174.Name = "Line174"
        Me.Line174.Top = 3.948!
        Me.Line174.Width = 2.0!
        Me.Line174.X1 = 2.264!
        Me.Line174.X2 = 4.264!
        Me.Line174.Y1 = 3.948!
        Me.Line174.Y2 = 3.948!
        '
        'Line178
        '
        Me.Line178.Height = 0!
        Me.Line178.Left = 2.264!
        Me.Line178.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line178.LineWeight = 1.0!
        Me.Line178.Name = "Line178"
        Me.Line178.Top = 3.76!
        Me.Line178.Width = 2.0!
        Me.Line178.X1 = 2.264!
        Me.Line178.X2 = 4.264!
        Me.Line178.Y1 = 3.76!
        Me.Line178.Y2 = 3.76!
        '
        'Line179
        '
        Me.Line179.Height = 0!
        Me.Line179.Left = 2.256!
        Me.Line179.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line179.LineWeight = 1.0!
        Me.Line179.Name = "Line179"
        Me.Line179.Top = 4.51!
        Me.Line179.Width = 2.0!
        Me.Line179.X1 = 2.256!
        Me.Line179.X2 = 4.256!
        Me.Line179.Y1 = 4.51!
        Me.Line179.Y2 = 4.51!
        '
        'Label664
        '
        Me.Label664.Height = 0.1875!
        Me.Label664.HyperLink = Nothing
        Me.Label664.Left = 0!
        Me.Label664.Name = "Label664"
        Me.Label664.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label664.Text = "wfillet_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label664.Top = 3.562!
        Me.Label664.Width = 2.246!
        '
        'Label667
        '
        Me.Label667.Height = 0.1875!
        Me.Label667.HyperLink = Nothing
        Me.Label667.Left = 4.749999!
        Me.Label667.Name = "Label667"
        Me.Label667.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label667.Text = "= wfillet_flange_min/ wfillet_flang" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label667.Top = 4.323!
        Me.Label667.Width = 3.264!
        '
        'Label668
        '
        Me.Label668.Height = 0.1875!
        Me.Label668.HyperLink = Nothing
        Me.Label668.Left = 0!
        Me.Label668.Name = "Label668"
        Me.Label668.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label668.Text = "wfillet_flange_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label668.Top = 4.313!
        Me.Label668.Width = 2.246!
        '
        'Line180
        '
        Me.Line180.Height = 0!
        Me.Line180.Left = 2.254!
        Me.Line180.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line180.LineWeight = 1.0!
        Me.Line180.Name = "Line180"
        Me.Line180.Top = 4.323!
        Me.Line180.Width = 2.0!
        Me.Line180.X1 = 2.254!
        Me.Line180.X2 = 4.254!
        Me.Line180.Y1 = 4.323!
        Me.Line180.Y2 = 4.323!
        '
        'Line181
        '
        Me.Line181.Height = 0!
        Me.Line181.Left = 2.263!
        Me.Line181.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line181.LineWeight = 1.0!
        Me.Line181.Name = "Line181"
        Me.Line181.Top = 4.511001!
        Me.Line181.Width = 2.0!
        Me.Line181.X1 = 2.263!
        Me.Line181.X2 = 4.263!
        Me.Line181.Y1 = 4.511001!
        Me.Line181.Y2 = 4.511001!
        '
        'Line175
        '
        Me.Line175.Height = 0.7510011!
        Me.Line175.Left = 2.264!
        Me.Line175.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line175.LineWeight = 1.0!
        Me.Line175.Name = "Line175"
        Me.Line175.Top = 3.76!
        Me.Line175.Width = 0!
        Me.Line175.X1 = 2.264!
        Me.Line175.X2 = 2.264!
        Me.Line175.Y1 = 3.76!
        Me.Line175.Y2 = 4.511001!
        '
        'Line176
        '
        Me.Line176.Height = 0.7510011!
        Me.Line176.Left = 3.264!
        Me.Line176.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line176.LineWeight = 1.0!
        Me.Line176.Name = "Line176"
        Me.Line176.Top = 3.76!
        Me.Line176.Width = 0!
        Me.Line176.X1 = 3.264!
        Me.Line176.X2 = 3.264!
        Me.Line176.Y1 = 3.76!
        Me.Line176.Y2 = 4.511001!
        '
        'Line177
        '
        Me.Line177.Height = 0.7510011!
        Me.Line177.Left = 4.264!
        Me.Line177.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line177.LineWeight = 1.0!
        Me.Line177.Name = "Line177"
        Me.Line177.Top = 3.76!
        Me.Line177.Width = 0!
        Me.Line177.X1 = 4.264!
        Me.Line177.X2 = 4.264!
        Me.Line177.Y1 = 3.76!
        Me.Line177.Y2 = 4.511001!
        '
        'Label675
        '
        Me.Label675.Height = 0.1875!
        Me.Label675.HyperLink = Nothing
        Me.Label675.Left = 0!
        Me.Label675.Name = "Label675"
        Me.Label675.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label675.Text = "3.15 STIFFENER PLATE DOUBLE SIDE FILLET WELD TO COLUMN WEB:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label675.Top = 4.812!
        Me.Label675.Width = 6.061999!
        '
        'Label676
        '
        Me.Label676.Height = 0.1875!
        Me.Label676.HyperLink = Nothing
        Me.Label676.Left = 0!
        Me.Label676.Name = "Label676"
        Me.Label676.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label676.Text = "wfillet_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label676.Top = 5.0!
        Me.Label676.Width = 2.254!
        '
        'S315_001
        '
        Me.S315_001.Height = 0.1875!
        Me.S315_001.HyperLink = Nothing
        Me.S315_001.Left = 2.25!
        Me.S315_001.Name = "S315_001"
        Me.S315_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S315_001.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_001.Top = 4.998001!
        Me.S315_001.Width = 1.0!
        '
        'Label678
        '
        Me.Label678.Height = 0.1875!
        Me.Label678.HyperLink = Nothing
        Me.Label678.Left = 4.261!
        Me.Label678.Name = "Label678"
        Me.Label678.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label678.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Minimum fillet weld size per AISC 360 Table J2.4" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label678.Top = 4.999001!
        Me.Label678.Width = 3.69!
        '
        'Label679
        '
        Me.Label679.Height = 0.1875!
        Me.Label679.HyperLink = Nothing
        Me.Label679.Left = 0!
        Me.Label679.Name = "Label679"
        Me.Label679.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label679.Text = "wfillet_min_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label679.Top = 5.187!
        Me.Label679.Width = 2.254!
        '
        'S315_002
        '
        Me.S315_002.Height = 0.1875!
        Me.S315_002.HyperLink = Nothing
        Me.S315_002.Left = 2.25!
        Me.S315_002.Name = "S315_002"
        Me.S315_002.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S315_002.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_002.Top = 5.185!
        Me.S315_002.Width = 1.0!
        '
        'Label681
        '
        Me.Label681.Height = 0.1875!
        Me.Label681.HyperLink = Nothing
        Me.Label681.Left = 4.261!
        Me.Label681.Name = "Label681"
        Me.Label681.Style = "font-size: 6.5pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label681.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.5* Fsu* SMF_Sides/ (ffillet* 0.6* Fexx* (Lstiff - Lclip_web-0.5)*2^(1/2))" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label681.Top = 5.186!
        Me.Label681.Width = 3.69!
        '
        'Label682
        '
        Me.Label682.Height = 0.1875!
        Me.Label682.HyperLink = Nothing
        Me.Label682.Left = 0.0000005364418!
        Me.Label682.Name = "Label682"
        Me.Label682.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label682.Text = "wfillet_web=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label682.Top = 5.375!
        Me.Label682.Width = 2.254!
        '
        'S315_003
        '
        Me.S315_003.Height = 0.1875!
        Me.S315_003.HyperLink = Nothing
        Me.S315_003.Left = 2.246001!
        Me.S315_003.Name = "S315_003"
        Me.S315_003.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S315_003.Text = "0.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_003.Top = 5.373001!
        Me.S315_003.Width = 1.0!
        '
        'Label684
        '
        Me.Label684.Height = 0.1875!
        Me.Label684.HyperLink = Nothing
        Me.Label684.Left = 4.257001!
        Me.Label684.Name = "Label684"
        Me.Label684.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label684.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "=max(wfillet_min, ceiling(wfillet_web,1/16)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label684.Top = 5.374001!
        Me.Label684.Width = 3.688!
        '
        'Label685
        '
        Me.Label685.Height = 0.1875!
        Me.Label685.HyperLink = Nothing
        Me.Label685.Left = 0!
        Me.Label685.Name = "Label685"
        Me.Label685.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label685.Text = "wfillet_web_DCR=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label685.Top = 5.562!
        Me.Label685.Width = 2.254!
        '
        'Label687
        '
        Me.Label687.Height = 0.1875!
        Me.Label687.HyperLink = Nothing
        Me.Label687.Left = 4.760999!
        Me.Label687.Name = "Label687"
        Me.Label687.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label687.Text = "= wfillet_min_web/ wfillet_web" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label687.Top = 5.561!
        Me.Label687.Width = 3.186!
        '
        'S315_004
        '
        Me.S315_004.Height = 0.1875!
        Me.S315_004.HyperLink = Nothing
        Me.S315_004.Left = 2.25!
        Me.S315_004.Name = "S315_004"
        Me.S315_004.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S315_004.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_004.Top = 5.56!
        Me.S315_004.Width = 1.0!
        '
        'Label691
        '
        Me.Label691.Height = 0.1875!
        Me.Label691.HyperLink = Nothing
        Me.Label691.Left = 0!
        Me.Label691.Name = "Label691"
        Me.Label691.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label691.Text = "3.16 CHECK MINIMUM COLUMN FLANGE THICKNESS:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label691.Top = 5.812!
        Me.Label691.Width = 6.061999!
        '
        'Label692
        '
        Me.Label692.Height = 0.1875!
        Me.Label692.HyperLink = Nothing
        Me.Label692.Left = 0!
        Me.Label692.Name = "Label692"
        Me.Label692.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label692.Text = "3.16.1 CONNECTION AWAY FROM COLUMN ENDS (SST Step 18 Table 1.1):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label692.Top = 6.0!
        Me.Label692.Width = 6.061999!
        '
        'Label693
        '
        Me.Label693.Height = 0.1875!
        Me.Label693.HyperLink = Nothing
        Me.Label693.Left = 2.25!
        Me.Label693.Name = "Label693"
        Me.Label693.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label693.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label693.Top = 6.25!
        Me.Label693.Width = 1.0!
        '
        'Label694
        '
        Me.Label694.Height = 0.1875!
        Me.Label694.HyperLink = Nothing
        Me.Label694.Left = 3.250001!
        Me.Label694.Name = "Label694"
        Me.Label694.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label694.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label694.Top = 6.25!
        Me.Label694.Width = 1.0!
        '
        'Label695
        '
        Me.Label695.Height = 0.188!
        Me.Label695.HyperLink = Nothing
        Me.Label695.Left = 0!
        Me.Label695.Name = "Label695"
        Me.Label695.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label695.Text = "Φc=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label695.Top = 6.437!
        Me.Label695.Width = 2.25!
        '
        'S317_001
        '
        Me.S317_001.Height = 0.1875!
        Me.S317_001.HyperLink = Nothing
        Me.S317_001.Left = 2.263!
        Me.S317_001.Name = "S317_001"
        Me.S317_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S317_001.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_001.Top = 6.437!
        Me.S317_001.Width = 1.0!
        '
        'Label697
        '
        Me.Label697.Height = 0.188!
        Me.Label697.HyperLink = Nothing
        Me.Label697.Left = 0!
        Me.Label697.Name = "Label697"
        Me.Label697.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label697.Text = "tcf=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label697.Top = 6.625!
        Me.Label697.Width = 2.25!
        '
        'S317_002
        '
        Me.S317_002.Height = 0.1875!
        Me.S317_002.HyperLink = Nothing
        Me.S317_002.Left = 2.25!
        Me.S317_002.Name = "S317_002"
        Me.S317_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_002.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_002.Top = 6.625!
        Me.S317_002.Width = 1.0!
        '
        'S317_019
        '
        Me.S317_019.Height = 0.1875!
        Me.S317_019.HyperLink = Nothing
        Me.S317_019.Left = 3.249!
        Me.S317_019.Name = "S317_019"
        Me.S317_019.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S317_019.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_019.Top = 6.437!
        Me.S317_019.Width = 1.0!
        '
        'S317_020
        '
        Me.S317_020.Height = 0.1875!
        Me.S317_020.HyperLink = Nothing
        Me.S317_020.Left = 3.25!
        Me.S317_020.Name = "S317_020"
        Me.S317_020.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_020.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_020.Top = 6.625!
        Me.S317_020.Width = 1.0!
        '
        'Line183
        '
        Me.Line183.Height = 0!
        Me.Line183.Left = 2.25!
        Me.Line183.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line183.LineWeight = 1.0!
        Me.Line183.Name = "Line183"
        Me.Line183.Top = 6.625!
        Me.Line183.Width = 1.999999!
        Me.Line183.X1 = 2.25!
        Me.Line183.X2 = 4.249999!
        Me.Line183.Y1 = 6.625!
        Me.Line183.Y2 = 6.625!
        '
        'Line184
        '
        Me.Line184.Height = 0!
        Me.Line184.Left = 2.25!
        Me.Line184.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line184.LineWeight = 1.0!
        Me.Line184.Name = "Line184"
        Me.Line184.Top = 6.437!
        Me.Line184.Width = 1.999999!
        Me.Line184.X1 = 2.25!
        Me.Line184.X2 = 4.249999!
        Me.Line184.Y1 = 6.437!
        Me.Line184.Y2 = 6.437!
        '
        'Label701
        '
        Me.Label701.Height = 0.188!
        Me.Label701.HyperLink = Nothing
        Me.Label701.Left = 4.27!
        Me.Label701.Name = "Label701"
        Me.Label701.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label701.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= bolt_g_flange"
        Me.Label701.Top = 6.813!
        Me.Label701.Width = 3.4!
        '
        'Label702
        '
        Me.Label702.Height = 0.188!
        Me.Label702.HyperLink = Nothing
        Me.Label702.Left = 0!
        Me.Label702.Name = "Label702"
        Me.Label702.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label702.Text = "c=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label702.Top = 6.813!
        Me.Label702.Width = 2.25!
        '
        'S317_003
        '
        Me.S317_003.Height = 0.1875!
        Me.S317_003.HyperLink = Nothing
        Me.S317_003.Left = 2.255!
        Me.S317_003.Name = "S317_003"
        Me.S317_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_003.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_003.Top = 6.813!
        Me.S317_003.Width = 1.0!
        '
        'S317_021
        '
        Me.S317_021.Height = 0.1875!
        Me.S317_021.HyperLink = Nothing
        Me.S317_021.Left = 3.255!
        Me.S317_021.Name = "S317_021"
        Me.S317_021.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_021.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_021.Top = 6.813!
        Me.S317_021.Width = 1.0!
        '
        'Label705
        '
        Me.Label705.Height = 0.188!
        Me.Label705.HyperLink = Nothing
        Me.Label705.Left = 4.27!
        Me.Label705.Name = "Label705"
        Me.Label705.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label705.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= bolt_s_flange"
        Me.Label705.Top = 7.0!
        Me.Label705.Width = 3.4!
        '
        'Label706
        '
        Me.Label706.Height = 0.188!
        Me.Label706.HyperLink = Nothing
        Me.Label706.Left = 0!
        Me.Label706.Name = "Label706"
        Me.Label706.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label706.Text = "g=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label706.Top = 7.0!
        Me.Label706.Width = 2.25!
        '
        'S317_004
        '
        Me.S317_004.Height = 0.1875!
        Me.S317_004.HyperLink = Nothing
        Me.S317_004.Left = 2.255!
        Me.S317_004.Name = "S317_004"
        Me.S317_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_004.Top = 7.0!
        Me.S317_004.Width = 1.0!
        '
        'S317_022
        '
        Me.S317_022.Height = 0.1875!
        Me.S317_022.HyperLink = Nothing
        Me.S317_022.Left = 3.255!
        Me.S317_022.Name = "S317_022"
        Me.S317_022.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_022.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_022.Top = 7.0!
        Me.S317_022.Width = 1.0!
        '
        'Label709
        '
        Me.Label709.Height = 0.188!
        Me.Label709.HyperLink = Nothing
        Me.Label709.Left = 4.27!
        Me.Label709.Name = "Label709"
        Me.Label709.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label709.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined"
        Me.Label709.Top = 7.187!
        Me.Label709.Width = 3.4!
        '
        'Label710
        '
        Me.Label710.Height = 0.188!
        Me.Label710.HyperLink = Nothing
        Me.Label710.Left = 0!
        Me.Label710.Name = "Label710"
        Me.Label710.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label710.Text = "H_flange=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label710.Top = 7.187!
        Me.Label710.Width = 2.25!
        '
        'S317_005
        '
        Me.S317_005.Height = 0.1875!
        Me.S317_005.HyperLink = Nothing
        Me.S317_005.Left = 2.250001!
        Me.S317_005.Name = "S317_005"
        Me.S317_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_005.Top = 7.187!
        Me.S317_005.Width = 1.0!
        '
        'S317_023
        '
        Me.S317_023.Height = 0.1875!
        Me.S317_023.HyperLink = Nothing
        Me.S317_023.Left = 3.25!
        Me.S317_023.Name = "S317_023"
        Me.S317_023.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_023.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_023.Top = 7.187!
        Me.S317_023.Width = 1.0!
        '
        'Label713
        '
        Me.Label713.Height = 0.188!
        Me.Label713.HyperLink = Nothing
        Me.Label713.Left = 4.27!
        Me.Label713.Name = "Label713"
        Me.Label713.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label713.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= 0.5* Sqrt (bcf* g)"
        Me.Label713.Top = 7.375!
        Me.Label713.Width = 3.4!
        '
        'Label714
        '
        Me.Label714.Height = 0.188!
        Me.Label714.HyperLink = Nothing
        Me.Label714.Left = 0!
        Me.Label714.Name = "Label714"
        Me.Label714.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label714.Text = "s=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label714.Top = 7.375!
        Me.Label714.Width = 2.25!
        '
        'S317_006
        '
        Me.S317_006.Height = 0.1875!
        Me.S317_006.HyperLink = Nothing
        Me.S317_006.Left = 2.254997!
        Me.S317_006.Name = "S317_006"
        Me.S317_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_006.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_006.Top = 7.375!
        Me.S317_006.Width = 1.0!
        '
        'S317_024
        '
        Me.S317_024.Height = 0.1875!
        Me.S317_024.HyperLink = Nothing
        Me.S317_024.Left = 3.254997!
        Me.S317_024.Name = "S317_024"
        Me.S317_024.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_024.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_024.Top = 7.375!
        Me.S317_024.Width = 1.0!
        '
        'Label717
        '
        Me.Label717.Height = 0.188!
        Me.Label717.HyperLink = Nothing
        Me.Label717.Left = 4.27!
        Me.Label717.Name = "Label717"
        Me.Label717.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label717.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (g - tstiff)/2"
        Me.Label717.Top = 7.563!
        Me.Label717.Width = 3.4!
        '
        'Label718
        '
        Me.Label718.Height = 0.188!
        Me.Label718.HyperLink = Nothing
        Me.Label718.Left = 0!
        Me.Label718.Name = "Label718"
        Me.Label718.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label718.Text = "pso=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label718.Top = 7.563!
        Me.Label718.Width = 2.25!
        '
        'S317_007
        '
        Me.S317_007.Height = 0.1875!
        Me.S317_007.HyperLink = Nothing
        Me.S317_007.Left = 2.25!
        Me.S317_007.Name = "S317_007"
        Me.S317_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_007.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_007.Top = 7.563001!
        Me.S317_007.Width = 1.0!
        '
        'S317_025
        '
        Me.S317_025.Height = 0.1875!
        Me.S317_025.HyperLink = Nothing
        Me.S317_025.Left = 3.25!
        Me.S317_025.Name = "S317_025"
        Me.S317_025.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_025.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_025.Top = 7.563001!
        Me.S317_025.Width = 1.0!
        '
        'Label721
        '
        Me.Label721.Height = 0.188!
        Me.Label721.HyperLink = Nothing
        Me.Label721.Left = 4.27!
        Me.Label721.Name = "Label721"
        Me.Label721.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label721.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= pso"
        Me.Label721.Top = 7.751!
        Me.Label721.Width = 3.4!
        '
        'Label722
        '
        Me.Label722.Height = 0.188!
        Me.Label722.HyperLink = Nothing
        Me.Label722.Left = 0!
        Me.Label722.Name = "Label722"
        Me.Label722.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label722.Text = "psi_tmp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label722.Top = 7.751!
        Me.Label722.Width = 2.25!
        '
        'S317_008
        '
        Me.S317_008.Height = 0.1875!
        Me.S317_008.HyperLink = Nothing
        Me.S317_008.Left = 2.250998!
        Me.S317_008.Name = "S317_008"
        Me.S317_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_008.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_008.Top = 7.750999!
        Me.S317_008.Width = 1.0!
        '
        'S317_026
        '
        Me.S317_026.Height = 0.1875!
        Me.S317_026.HyperLink = Nothing
        Me.S317_026.Left = 3.250998!
        Me.S317_026.Name = "S317_026"
        Me.S317_026.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S317_026.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_026.Top = 7.750999!
        Me.S317_026.Width = 1.0!
        '
        'Label725
        '
        Me.Label725.Height = 0.188!
        Me.Label725.HyperLink = Nothing
        Me.Label725.Left = 4.27!
        Me.Label725.Name = "Label725"
        Me.Label725.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label725.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (psi_tmp > s, s, psi_tmp)"
        Me.Label725.Top = 7.939!
        Me.Label725.Width = 3.4!
        '
        'Label726
        '
        Me.Label726.Height = 0.188!
        Me.Label726.HyperLink = Nothing
        Me.Label726.Left = 0!
        Me.Label726.Name = "Label726"
        Me.Label726.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label726.Text = "psi=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label726.Top = 7.939!
        Me.Label726.Width = 2.25!
        '
        'S317_009
        '
        Me.S317_009.Height = 0.1875!
        Me.S317_009.HyperLink = Nothing
        Me.S317_009.Left = 2.256!
        Me.S317_009.Name = "S317_009"
        Me.S317_009.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_009.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_009.Top = 7.939!
        Me.S317_009.Width = 1.0!
        '
        'S317_027
        '
        Me.S317_027.Height = 0.1875!
        Me.S317_027.HyperLink = Nothing
        Me.S317_027.Left = 3.255!
        Me.S317_027.Name = "S317_027"
        Me.S317_027.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_027.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_027.Top = 7.939001!
        Me.S317_027.Width = 1.0!
        '
        'Label729
        '
        Me.Label729.Height = 0.188!
        Me.Label729.HyperLink = Nothing
        Me.Label729.Left = 4.27!
        Me.Label729.Name = "Label729"
        Me.Label729.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label729.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= db + t_stem - g/2" & Global.Microsoft.VisualBasic.ChrW(9)
        Me.Label729.Top = 8.126!
        Me.Label729.Width = 3.4!
        '
        'Label730
        '
        Me.Label730.Height = 0.188!
        Me.Label730.HyperLink = Nothing
        Me.Label730.Left = 0!
        Me.Label730.Name = "Label730"
        Me.Label730.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label730.Text = "h1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label730.Top = 8.126!
        Me.Label730.Width = 2.25!
        '
        'S317_010
        '
        Me.S317_010.Height = 0.1875!
        Me.S317_010.HyperLink = Nothing
        Me.S317_010.Left = 2.256!
        Me.S317_010.Name = "S317_010"
        Me.S317_010.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_010.Top = 8.125999!
        Me.S317_010.Width = 1.0!
        '
        'S317_028
        '
        Me.S317_028.Height = 0.1875!
        Me.S317_028.HyperLink = Nothing
        Me.S317_028.Left = 3.255!
        Me.S317_028.Name = "S317_028"
        Me.S317_028.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_028.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_028.Top = 8.126!
        Me.S317_028.Width = 1.0!
        '
        'Label733
        '
        Me.Label733.Height = 0.188!
        Me.Label733.HyperLink = Nothing
        Me.Label733.Left = 4.27!
        Me.Label733.Name = "Label733"
        Me.Label733.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label733.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= db + t_stem + g/2"
        Me.Label733.Top = 8.314!
        Me.Label733.Width = 3.4!
        '
        'Label734
        '
        Me.Label734.Height = 0.188!
        Me.Label734.HyperLink = Nothing
        Me.Label734.Left = 0!
        Me.Label734.Name = "Label734"
        Me.Label734.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label734.Text = "h0=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label734.Top = 8.314!
        Me.Label734.Width = 2.25!
        '
        'S317_011
        '
        Me.S317_011.Height = 0.1875!
        Me.S317_011.HyperLink = Nothing
        Me.S317_011.Left = 2.25!
        Me.S317_011.Name = "S317_011"
        Me.S317_011.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_011.Top = 8.313999!
        Me.S317_011.Width = 1.0!
        '
        'S317_029
        '
        Me.S317_029.Height = 0.1875!
        Me.S317_029.HyperLink = Nothing
        Me.S317_029.Left = 3.249!
        Me.S317_029.Name = "S317_029"
        Me.S317_029.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_029.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_029.Top = 8.314!
        Me.S317_029.Width = 1.0!
        '
        'Line186
        '
        Me.Line186.Height = 0!
        Me.Line186.Left = 2.256!
        Me.Line186.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line186.LineWeight = 1.0!
        Me.Line186.Name = "Line186"
        Me.Line186.Top = 6.813001!
        Me.Line186.Width = 1.999998!
        Me.Line186.X1 = 2.256!
        Me.Line186.X2 = 4.255998!
        Me.Line186.Y1 = 6.813001!
        Me.Line186.Y2 = 6.813001!
        '
        'Line187
        '
        Me.Line187.Height = 0!
        Me.Line187.Left = 2.248!
        Me.Line187.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line187.LineWeight = 1.0!
        Me.Line187.Name = "Line187"
        Me.Line187.Top = 7.0!
        Me.Line187.Width = 1.999998!
        Me.Line187.X1 = 2.248!
        Me.Line187.X2 = 4.247998!
        Me.Line187.Y1 = 7.0!
        Me.Line187.Y2 = 7.0!
        '
        'Line188
        '
        Me.Line188.Height = 0!
        Me.Line188.Left = 2.265!
        Me.Line188.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line188.LineWeight = 1.0!
        Me.Line188.Name = "Line188"
        Me.Line188.Top = 7.186999!
        Me.Line188.Width = 2.0!
        Me.Line188.X1 = 2.265!
        Me.Line188.X2 = 4.265!
        Me.Line188.Y1 = 7.186999!
        Me.Line188.Y2 = 7.186999!
        '
        'Line189
        '
        Me.Line189.Height = 0!
        Me.Line189.Left = 2.267!
        Me.Line189.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line189.LineWeight = 1.0!
        Me.Line189.Name = "Line189"
        Me.Line189.Top = 7.375!
        Me.Line189.Width = 2.0!
        Me.Line189.X1 = 2.267!
        Me.Line189.X2 = 4.267!
        Me.Line189.Y1 = 7.375!
        Me.Line189.Y2 = 7.375!
        '
        'Line190
        '
        Me.Line190.Height = 0!
        Me.Line190.Left = 2.258!
        Me.Line190.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line190.LineWeight = 1.0!
        Me.Line190.Name = "Line190"
        Me.Line190.Top = 7.563001!
        Me.Line190.Width = 1.999999!
        Me.Line190.X1 = 2.258!
        Me.Line190.X2 = 4.257999!
        Me.Line190.Y1 = 7.563001!
        Me.Line190.Y2 = 7.563001!
        '
        'Line191
        '
        Me.Line191.Height = 0!
        Me.Line191.Left = 2.25!
        Me.Line191.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line191.LineWeight = 1.0!
        Me.Line191.Name = "Line191"
        Me.Line191.Top = 7.750999!
        Me.Line191.Width = 1.999999!
        Me.Line191.X1 = 2.25!
        Me.Line191.X2 = 4.249999!
        Me.Line191.Y1 = 7.750999!
        Me.Line191.Y2 = 7.750999!
        '
        'Line192
        '
        Me.Line192.Height = 0!
        Me.Line192.Left = 2.25!
        Me.Line192.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line192.LineWeight = 1.0!
        Me.Line192.Name = "Line192"
        Me.Line192.Top = 7.939!
        Me.Line192.Width = 1.999998!
        Me.Line192.X1 = 2.25!
        Me.Line192.X2 = 4.249998!
        Me.Line192.Y1 = 7.939!
        Me.Line192.Y2 = 7.939!
        '
        'Line193
        '
        Me.Line193.Height = 0!
        Me.Line193.Left = 2.254!
        Me.Line193.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line193.LineWeight = 1.0!
        Me.Line193.Name = "Line193"
        Me.Line193.Top = 8.126!
        Me.Line193.Width = 2.0!
        Me.Line193.X1 = 2.254!
        Me.Line193.X2 = 4.254!
        Me.Line193.Y1 = 8.126!
        Me.Line193.Y2 = 8.126!
        '
        'Line194
        '
        Me.Line194.Height = 0!
        Me.Line194.Left = 2.254!
        Me.Line194.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line194.LineWeight = 1.0!
        Me.Line194.Name = "Line194"
        Me.Line194.Top = 8.314!
        Me.Line194.Width = 1.999998!
        Me.Line194.X1 = 2.254!
        Me.Line194.X2 = 4.253998!
        Me.Line194.Y1 = 8.314!
        Me.Line194.Y2 = 8.314!
        '
        'Line198
        '
        Me.Line198.Height = 0!
        Me.Line198.Left = 2.25!
        Me.Line198.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line198.LineWeight = 1.0!
        Me.Line198.Name = "Line198"
        Me.Line198.Top = 6.25!
        Me.Line198.Width = 1.999999!
        Me.Line198.X1 = 2.25!
        Me.Line198.X2 = 4.249999!
        Me.Line198.Y1 = 6.25!
        Me.Line198.Y2 = 6.25!
        '
        'Label737
        '
        Me.Label737.Height = 0.188!
        Me.Label737.HyperLink = Nothing
        Me.Label737.Left = 4.27!
        Me.Label737.Name = "Label737"
        Me.Label737.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label737.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined"
        Me.Label737.Top = 6.625!
        Me.Label737.Width = 3.4!
        '
        'Label738
        '
        Me.Label738.Height = 0.188!
        Me.Label738.HyperLink = Nothing
        Me.Label738.Left = 4.27!
        Me.Label738.Name = "Label738"
        Me.Label738.Style = "font-size: 6.5pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label738.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= bcf/2* (h1/s + h0/s) + 2/g* (h1* (s + 3*c/4) + h0* (s +c/4) + c^2/4) +g/2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label738.Top = 8.501!
        Me.Label738.Width = 3.4!
        '
        'Label739
        '
        Me.Label739.Height = 0.188!
        Me.Label739.HyperLink = Nothing
        Me.Label739.Left = 0!
        Me.Label739.Name = "Label739"
        Me.Label739.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label739.Text = "Yc_unstiffened=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label739.Top = 8.501!
        Me.Label739.Width = 2.25!
        '
        'S317_012
        '
        Me.S317_012.Height = 0.1875!
        Me.S317_012.HyperLink = Nothing
        Me.S317_012.Left = 2.25!
        Me.S317_012.Name = "S317_012"
        Me.S317_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_012.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_012.Top = 8.500999!
        Me.S317_012.Width = 1.0!
        '
        'S317_030
        '
        Me.S317_030.Height = 0.1875!
        Me.S317_030.HyperLink = Nothing
        Me.S317_030.Left = 3.249!
        Me.S317_030.Name = "S317_030"
        Me.S317_030.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_030.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_030.Top = 8.501!
        Me.S317_030.Width = 1.0!
        '
        'Label742
        '
        Me.Label742.Height = 0.188!
        Me.Label742.HyperLink = Nothing
        Me.Label742.Left = 4.27!
        Me.Label742.Name = "Label742"
        Me.Label742.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label742.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sqrt (1.1* Mpr/ (fb* Fyc* Yc_unstiffened) )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label742.Top = 8.689!
        Me.Label742.Width = 3.4!
        '
        'Label743
        '
        Me.Label743.Height = 0.188!
        Me.Label743.HyperLink = Nothing
        Me.Label743.Left = 0!
        Me.Label743.Name = "Label743"
        Me.Label743.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label743.Text = "tcf_req_unstiffened=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label743.Top = 8.689!
        Me.Label743.Width = 2.25!
        '
        'S317_013
        '
        Me.S317_013.Height = 0.1875!
        Me.S317_013.HyperLink = Nothing
        Me.S317_013.Left = 2.25!
        Me.S317_013.Name = "S317_013"
        Me.S317_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_013.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_013.Top = 8.688999!
        Me.S317_013.Width = 1.0!
        '
        'S317_031
        '
        Me.S317_031.Height = 0.1875!
        Me.S317_031.HyperLink = Nothing
        Me.S317_031.Left = 3.249!
        Me.S317_031.Name = "S317_031"
        Me.S317_031.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_031.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_031.Top = 8.689!
        Me.S317_031.Width = 1.0!
        '
        'Label746
        '
        Me.Label746.Height = 0.22!
        Me.Label746.HyperLink = Nothing
        Me.Label746.Left = 4.27!
        Me.Label746.Name = "Label746"
        Me.Label746.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label746.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= bcf/2* (h1* (1/s + 1/psi) + h0* (1/s + 1/pso) ) + 2/g* (h1* (s + psi) + " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "  " &
    "                   h0* (s + pso) )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label746.Top = 8.875!
        Me.Label746.Width = 3.4!
        '
        'Label747
        '
        Me.Label747.Height = 0.188!
        Me.Label747.HyperLink = Nothing
        Me.Label747.Left = 0!
        Me.Label747.Name = "Label747"
        Me.Label747.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label747.Text = "Yc_stiffened=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label747.Top = 8.875999!
        Me.Label747.Width = 2.25!
        '
        'S317_014
        '
        Me.S317_014.Height = 0.1875!
        Me.S317_014.HyperLink = Nothing
        Me.S317_014.Left = 2.26!
        Me.S317_014.Name = "S317_014"
        Me.S317_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_014.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_014.Top = 8.875999!
        Me.S317_014.Width = 1.0!
        '
        'S317_032
        '
        Me.S317_032.Height = 0.1875!
        Me.S317_032.HyperLink = Nothing
        Me.S317_032.Left = 3.258!
        Me.S317_032.Name = "S317_032"
        Me.S317_032.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_032.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_032.Top = 8.876!
        Me.S317_032.Width = 1.0!
        '
        'Label751
        '
        Me.Label751.Height = 0.188!
        Me.Label751.HyperLink = Nothing
        Me.Label751.Left = 0!
        Me.Label751.Name = "Label751"
        Me.Label751.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label751.Text = "tcf_req_stiffened=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label751.Top = 9.063999!
        Me.Label751.Width = 2.25!
        '
        'S317_015
        '
        Me.S317_015.Height = 0.1875!
        Me.S317_015.HyperLink = Nothing
        Me.S317_015.Left = 2.26!
        Me.S317_015.Name = "S317_015"
        Me.S317_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_015.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_015.Top = 9.063999!
        Me.S317_015.Width = 1.0!
        '
        'S317_033
        '
        Me.S317_033.Height = 0.1875!
        Me.S317_033.HyperLink = Nothing
        Me.S317_033.Left = 3.256!
        Me.S317_033.Name = "S317_033"
        Me.S317_033.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_033.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_033.Top = 9.064!
        Me.S317_033.Width = 1.0!
        '
        'Label754
        '
        Me.Label754.Height = 0.188!
        Me.Label754.HyperLink = Nothing
        Me.Label754.Left = 4.27!
        Me.Label754.Name = "Label754"
        Me.Label754.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label754.Text = "                    Input value"
        Me.Label754.Top = 9.251!
        Me.Label754.Width = 3.4!
        '
        'Label755
        '
        Me.Label755.Height = 0.188!
        Me.Label755.HyperLink = Nothing
        Me.Label755.Left = 0.0000002980232!
        Me.Label755.Name = "Label755"
        Me.Label755.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label755.Text = "Stiffened?=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label755.Top = 9.250999!
        Me.Label755.Width = 2.25!
        '
        'S317_016
        '
        Me.S317_016.Height = 0.1875!
        Me.S317_016.HyperLink = Nothing
        Me.S317_016.Left = 2.268!
        Me.S317_016.Name = "S317_016"
        Me.S317_016.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: bold; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.S317_016.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_016.Top = 9.250999!
        Me.S317_016.Width = 1.0!
        '
        'S317_034
        '
        Me.S317_034.Height = 0.1875!
        Me.S317_034.HyperLink = Nothing
        Me.S317_034.Left = 3.25!
        Me.S317_034.Name = "S317_034"
        Me.S317_034.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: bold; text-align: center; verti" &
    "cal-align: middle; ddo-char-set: 0"
        Me.S317_034.Text = "NO" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_034.Top = 9.250999!
        Me.S317_034.Width = 1.0!
        '
        'Label758
        '
        Me.Label758.Height = 0.188!
        Me.Label758.HyperLink = Nothing
        Me.Label758.Left = 4.27!
        Me.Label758.Name = "Label758"
        Me.Label758.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label758.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= if (Stiffened? = YES, tcf_req_stiffened, tcf_req_unstiffened)" & Global.Microsoft.VisualBasic.ChrW(9)
        Me.Label758.Top = 9.439!
        Me.Label758.Width = 3.4!
        '
        'Label759
        '
        Me.Label759.Height = 0.188!
        Me.Label759.HyperLink = Nothing
        Me.Label759.Left = 0.0000002980232!
        Me.Label759.Name = "Label759"
        Me.Label759.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label759.Text = "tcf_min=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label759.Top = 9.438999!
        Me.Label759.Width = 2.25!
        '
        'S317_017
        '
        Me.S317_017.Height = 0.1875!
        Me.S317_017.HyperLink = Nothing
        Me.S317_017.Left = 2.26!
        Me.S317_017.Name = "S317_017"
        Me.S317_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_017.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_017.Top = 9.438999!
        Me.S317_017.Width = 1.0!
        '
        'S317_035
        '
        Me.S317_035.Height = 0.1875!
        Me.S317_035.HyperLink = Nothing
        Me.S317_035.Left = 3.26!
        Me.S317_035.Name = "S317_035"
        Me.S317_035.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_035.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_035.Top = 9.438999!
        Me.S317_035.Width = 1.0!
        '
        'Label762
        '
        Me.Label762.Height = 0.188!
        Me.Label762.HyperLink = Nothing
        Me.Label762.Left = 4.27!
        Me.Label762.Name = "Label762"
        Me.Label762.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label762.Text = "                   = tcf_min/ tcf" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label762.Top = 9.626!
        Me.Label762.Width = 3.4!
        '
        'Label763
        '
        Me.Label763.Height = 0.188!
        Me.Label763.HyperLink = Nothing
        Me.Label763.Left = 0.0000002980232!
        Me.Label763.Name = "Label763"
        Me.Label763.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label763.Text = "SUM_colFLB1=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label763.Top = 9.625999!
        Me.Label763.Width = 2.25!
        '
        'S317_018
        '
        Me.S317_018.Height = 0.1875!
        Me.S317_018.HyperLink = Nothing
        Me.S317_018.Left = 2.26!
        Me.S317_018.Name = "S317_018"
        Me.S317_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_018.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_018.Top = 9.625999!
        Me.S317_018.Width = 1.0!
        '
        'S317_036
        '
        Me.S317_036.Height = 0.1875!
        Me.S317_036.HyperLink = Nothing
        Me.S317_036.Left = 3.26!
        Me.S317_036.Name = "S317_036"
        Me.S317_036.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S317_036.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S317_036.Top = 9.625999!
        Me.S317_036.Width = 1.0!
        '
        'Line199
        '
        Me.Line199.Height = 0!
        Me.Line199.Left = 2.25!
        Me.Line199.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line199.LineWeight = 1.0!
        Me.Line199.Name = "Line199"
        Me.Line199.Top = 8.500999!
        Me.Line199.Width = 1.999999!
        Me.Line199.X1 = 2.25!
        Me.Line199.X2 = 4.249999!
        Me.Line199.Y1 = 8.500999!
        Me.Line199.Y2 = 8.500999!
        '
        'Line200
        '
        Me.Line200.Height = 0!
        Me.Line200.Left = 2.256!
        Me.Line200.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line200.LineWeight = 1.0!
        Me.Line200.Name = "Line200"
        Me.Line200.Top = 8.688999!
        Me.Line200.Width = 1.999997!
        Me.Line200.X1 = 2.256!
        Me.Line200.X2 = 4.255997!
        Me.Line200.Y1 = 8.688999!
        Me.Line200.Y2 = 8.688999!
        '
        'Line201
        '
        Me.Line201.Height = 0!
        Me.Line201.Left = 2.25!
        Me.Line201.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line201.LineWeight = 1.0!
        Me.Line201.Name = "Line201"
        Me.Line201.Top = 8.875999!
        Me.Line201.Width = 1.999999!
        Me.Line201.X1 = 2.25!
        Me.Line201.X2 = 4.249999!
        Me.Line201.Y1 = 8.875999!
        Me.Line201.Y2 = 8.875999!
        '
        'Line202
        '
        Me.Line202.Height = 0!
        Me.Line202.Left = 2.25!
        Me.Line202.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line202.LineWeight = 1.0!
        Me.Line202.Name = "Line202"
        Me.Line202.Top = 9.063999!
        Me.Line202.Width = 1.999999!
        Me.Line202.X1 = 2.25!
        Me.Line202.X2 = 4.249999!
        Me.Line202.Y1 = 9.063999!
        Me.Line202.Y2 = 9.063999!
        '
        'Line203
        '
        Me.Line203.Height = 0!
        Me.Line203.Left = 2.26!
        Me.Line203.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line203.LineWeight = 1.0!
        Me.Line203.Name = "Line203"
        Me.Line203.Top = 9.250999!
        Me.Line203.Width = 1.999997!
        Me.Line203.X1 = 2.26!
        Me.Line203.X2 = 4.259997!
        Me.Line203.Y1 = 9.250999!
        Me.Line203.Y2 = 9.250999!
        '
        'Line204
        '
        Me.Line204.Height = 0!
        Me.Line204.Left = 2.25!
        Me.Line204.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line204.LineWeight = 1.0!
        Me.Line204.Name = "Line204"
        Me.Line204.Top = 9.438999!
        Me.Line204.Width = 1.999999!
        Me.Line204.X1 = 2.25!
        Me.Line204.X2 = 4.249999!
        Me.Line204.Y1 = 9.438999!
        Me.Line204.Y2 = 9.438999!
        '
        'Line205
        '
        Me.Line205.Height = 0!
        Me.Line205.Left = 2.25!
        Me.Line205.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line205.LineWeight = 1.0!
        Me.Line205.Name = "Line205"
        Me.Line205.Top = 9.625999!
        Me.Line205.Width = 1.999997!
        Me.Line205.X1 = 2.25!
        Me.Line205.X2 = 4.249997!
        Me.Line205.Y1 = 9.625999!
        Me.Line205.Y2 = 9.625999!
        '
        'Line196
        '
        Me.Line196.Height = 3.563999!
        Me.Line196.Left = 2.250003!
        Me.Line196.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line196.LineWeight = 1.0!
        Me.Line196.Name = "Line196"
        Me.Line196.Top = 6.25!
        Me.Line196.Width = 0!
        Me.Line196.X1 = 2.250003!
        Me.Line196.X2 = 2.250003!
        Me.Line196.Y1 = 6.25!
        Me.Line196.Y2 = 9.813999!
        '
        'Line195
        '
        Me.Line195.Height = 3.563999!
        Me.Line195.Left = 3.250003!
        Me.Line195.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line195.LineWeight = 1.0!
        Me.Line195.Name = "Line195"
        Me.Line195.Top = 6.25!
        Me.Line195.Width = 0!
        Me.Line195.X1 = 3.250003!
        Me.Line195.X2 = 3.250003!
        Me.Line195.Y1 = 6.25!
        Me.Line195.Y2 = 9.813999!
        '
        'Label767
        '
        Me.Label767.Height = 0.188!
        Me.Label767.HyperLink = Nothing
        Me.Label767.Left = 4.27!
        Me.Label767.Name = "Label767"
        Me.Label767.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label767.Text = "                    OK if SUM_colFLB1 < SUM_Allowed"
        Me.Label767.Top = 9.814!
        Me.Label767.Width = 3.4!
        '
        'Line206
        '
        Me.Line206.Height = 0!
        Me.Line206.Left = 2.256!
        Me.Line206.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line206.LineWeight = 1.0!
        Me.Line206.Name = "Line206"
        Me.Line206.Top = 9.814001!
        Me.Line206.Width = 1.999996!
        Me.Line206.X1 = 2.256!
        Me.Line206.X2 = 4.255996!
        Me.Line206.Y1 = 9.814001!
        Me.Line206.Y2 = 9.814001!
        '
        'Line197
        '
        Me.Line197.Height = 3.563999!
        Me.Line197.Left = 4.254!
        Me.Line197.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line197.LineWeight = 1.0!
        Me.Line197.Name = "Line197"
        Me.Line197.Top = 6.25!
        Me.Line197.Width = 0.0000009536743!
        Me.Line197.X1 = 4.254!
        Me.Line197.X2 = 4.254001!
        Me.Line197.Y1 = 6.25!
        Me.Line197.Y2 = 9.813999!
        '
        'Label768
        '
        Me.Label768.Height = 0.1875!
        Me.Label768.HyperLink = Nothing
        Me.Label768.Left = 0!
        Me.Label768.Name = "Label768"
        Me.Label768.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label768.Text = "3.16.2 CONNECTION AT STIFFENED COLUMN END (SST STEP 18, TABLE 1.2, CASE 1):" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label768.Top = 10.125!
        Me.Label768.Width = 6.061999!
        '
        'Label769
        '
        Me.Label769.Height = 0.1875!
        Me.Label769.HyperLink = Nothing
        Me.Label769.Left = 2.262!
        Me.Label769.Name = "Label769"
        Me.Label769.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label769.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label769.Top = 10.416!
        Me.Label769.Width = 1.0!
        '
        'Label770
        '
        Me.Label770.Height = 0.1875!
        Me.Label770.HyperLink = Nothing
        Me.Label770.Left = 3.262!
        Me.Label770.Name = "Label770"
        Me.Label770.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label770.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label770.Top = 10.416!
        Me.Label770.Width = 1.0!
        '
        'Label771
        '
        Me.Label771.Height = 0.188!
        Me.Label771.HyperLink = Nothing
        Me.Label771.Left = 0!
        Me.Label771.Name = "Label771"
        Me.Label771.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label771.Text = "de=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label771.Top = 10.604!
        Me.Label771.Width = 2.25!
        '
        'S318_001
        '
        Me.S318_001.Height = 0.1875!
        Me.S318_001.HyperLink = Nothing
        Me.S318_001.Left = 2.262!
        Me.S318_001.Name = "S318_001"
        Me.S318_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S318_001.Text = "1.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_001.Top = 10.603!
        Me.S318_001.Width = 1.0!
        '
        'Label773
        '
        Me.Label773.Height = 0.188!
        Me.Label773.HyperLink = Nothing
        Me.Label773.Left = 0.00000002980232!
        Me.Label773.Name = "Label773"
        Me.Label773.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label773.Text = "pfi=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label773.Top = 10.791!
        Me.Label773.Width = 2.25!
        '
        'S318_002
        '
        Me.S318_002.Height = 0.1875!
        Me.S318_002.HyperLink = Nothing
        Me.S318_002.Left = 2.262!
        Me.S318_002.Name = "S318_002"
        Me.S318_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_002.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_002.Top = 10.79!
        Me.S318_002.Width = 1.0!
        '
        'S318_007
        '
        Me.S318_007.Height = 0.1875!
        Me.S318_007.HyperLink = Nothing
        Me.S318_007.Left = 3.262!
        Me.S318_007.Name = "S318_007"
        Me.S318_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S318_007.Text = "1.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_007.Top = 10.603!
        Me.S318_007.Width = 1.0!
        '
        'S318_008
        '
        Me.S318_008.Height = 0.1875!
        Me.S318_008.HyperLink = Nothing
        Me.S318_008.Left = 3.262!
        Me.S318_008.Name = "S318_008"
        Me.S318_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_008.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_008.Top = 10.79!
        Me.S318_008.Width = 1.0!
        '
        'Line208
        '
        Me.Line208.Height = 0!
        Me.Line208.Left = 2.262!
        Me.Line208.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line208.LineWeight = 1.0!
        Me.Line208.Name = "Line208"
        Me.Line208.Top = 10.79!
        Me.Line208.Width = 1.999999!
        Me.Line208.X1 = 2.262!
        Me.Line208.X2 = 4.261999!
        Me.Line208.Y1 = 10.79!
        Me.Line208.Y2 = 10.79!
        '
        'Line209
        '
        Me.Line209.Height = 0!
        Me.Line209.Left = 2.262!
        Me.Line209.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line209.LineWeight = 1.0!
        Me.Line209.Name = "Line209"
        Me.Line209.Top = 10.603!
        Me.Line209.Width = 1.999999!
        Me.Line209.X1 = 2.262!
        Me.Line209.X2 = 4.261999!
        Me.Line209.Y1 = 10.603!
        Me.Line209.Y2 = 10.603!
        '
        'Line210
        '
        Me.Line210.Height = 0!
        Me.Line210.Left = 2.254!
        Me.Line210.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line210.LineWeight = 1.0!
        Me.Line210.Name = "Line210"
        Me.Line210.Top = 11.166!
        Me.Line210.Width = 2.0!
        Me.Line210.X1 = 2.254!
        Me.Line210.X2 = 4.254!
        Me.Line210.Y1 = 11.166!
        Me.Line210.Y2 = 11.166!
        '
        'Label777
        '
        Me.Label777.Height = 0.1875!
        Me.Label777.HyperLink = Nothing
        Me.Label777.Left = 4.27!
        Me.Label777.Name = "Label777"
        Me.Label777.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label777.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "Value previously defined" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label777.Top = 10.978!
        Me.Label777.Width = 3.75!
        '
        'Label778
        '
        Me.Label778.Height = 0.1875!
        Me.Label778.HyperLink = Nothing
        Me.Label778.Left = 0.00000002980232!
        Me.Label778.Name = "Label778"
        Me.Label778.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label778.Text = "pso=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label778.Top = 10.979!
        Me.Label778.Width = 2.25!
        '
        'S318_003
        '
        Me.S318_003.Height = 0.1875!
        Me.S318_003.HyperLink = Nothing
        Me.S318_003.Left = 2.268!
        Me.S318_003.Name = "S318_003"
        Me.S318_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_003.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_003.Top = 10.978!
        Me.S318_003.Width = 1.0!
        '
        'S318_009
        '
        Me.S318_009.Height = 0.1875!
        Me.S318_009.HyperLink = Nothing
        Me.S318_009.Left = 3.262!
        Me.S318_009.Name = "S318_009"
        Me.S318_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_009.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_009.Top = 10.978!
        Me.S318_009.Width = 1.0!
        '
        'Label781
        '
        Me.Label781.Height = 0.22!
        Me.Label781.HyperLink = Nothing
        Me.Label781.Left = 4.27!
        Me.Label781.Name = "Label781"
        Me.Label781.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label781.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= bcf/2* (h1* (1/pfi + 1/pso) + h0* (1/pso + 1/2/s) ) + 2/g* (h1* (pfi + s) + " &
    "" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "                      h0* (d2 + pso) )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label781.Top = 11.134!
        Me.Label781.Width = 3.762!
        '
        'Label782
        '
        Me.Label782.Height = 0.1875!
        Me.Label782.HyperLink = Nothing
        Me.Label782.Left = 0.00000002980232!
        Me.Label782.Name = "Label782"
        Me.Label782.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label782.Text = "Yp=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label782.Top = 11.166!
        Me.Label782.Width = 2.25!
        '
        'S318_004
        '
        Me.S318_004.Height = 0.1875!
        Me.S318_004.HyperLink = Nothing
        Me.S318_004.Left = 2.268!
        Me.S318_004.Name = "S318_004"
        Me.S318_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_004.Top = 11.165!
        Me.S318_004.Width = 1.0!
        '
        'S318_010
        '
        Me.S318_010.Height = 0.1875!
        Me.S318_010.HyperLink = Nothing
        Me.S318_010.Left = 3.262!
        Me.S318_010.Name = "S318_010"
        Me.S318_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_010.Top = 11.165!
        Me.S318_010.Width = 1.0!
        '
        'Label785
        '
        Me.Label785.Height = 0.1875!
        Me.Label785.HyperLink = Nothing
        Me.Label785.Left = 4.27!
        Me.Label785.Name = "Label785"
        Me.Label785.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label785.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= Sqrt* (1.1* Mpr/ (fb* Fy* Yp) )" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label785.Top = 11.353!
        Me.Label785.Width = 3.75!
        '
        'Label786
        '
        Me.Label786.Height = 0.1875!
        Me.Label786.HyperLink = Nothing
        Me.Label786.Left = 0.00000002980232!
        Me.Label786.Name = "Label786"
        Me.Label786.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label786.Text = "tcf_req=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label786.Top = 11.354!
        Me.Label786.Width = 2.25!
        '
        'S318_005
        '
        Me.S318_005.Height = 0.1875!
        Me.S318_005.HyperLink = Nothing
        Me.S318_005.Left = 2.262!
        Me.S318_005.Name = "S318_005"
        Me.S318_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_005.Top = 11.353!
        Me.S318_005.Width = 1.0!
        '
        'S318_011
        '
        Me.S318_011.Height = 0.1875!
        Me.S318_011.HyperLink = Nothing
        Me.S318_011.Left = 3.262!
        Me.S318_011.Name = "S318_011"
        Me.S318_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S318_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_011.Top = 11.353!
        Me.S318_011.Width = 1.0!
        '
        'Label789
        '
        Me.Label789.Height = 0.1875!
        Me.Label789.HyperLink = Nothing
        Me.Label789.Left = 4.27!
        Me.Label789.Name = "Label789"
        Me.Label789.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label789.Text = "                   = tcf_req/ tcf"
        Me.Label789.Top = 11.54!
        Me.Label789.Width = 3.258996!
        '
        'Label790
        '
        Me.Label790.Height = 0.188!
        Me.Label790.HyperLink = Nothing
        Me.Label790.Left = 0.00000002980232!
        Me.Label790.Name = "Label790"
        Me.Label790.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label790.Text = "SUM_colFLB2=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label790.Top = 11.541!
        Me.Label790.Width = 2.25!
        '
        'S318_006
        '
        Me.S318_006.Height = 0.1875!
        Me.S318_006.HyperLink = Nothing
        Me.S318_006.Left = 2.266998!
        Me.S318_006.Name = "S318_006"
        Me.S318_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S318_006.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_006.Top = 11.54!
        Me.S318_006.Width = 1.0!
        '
        'S318_012
        '
        Me.S318_012.Height = 0.1875!
        Me.S318_012.HyperLink = Nothing
        Me.S318_012.Left = 3.254!
        Me.S318_012.Name = "S318_012"
        Me.S318_012.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S318_012.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S318_012.Top = 11.54!
        Me.S318_012.Width = 1.0!
        '
        'Line211
        '
        Me.Line211.Height = 0!
        Me.Line211.Left = 2.262!
        Me.Line211.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line211.LineWeight = 1.0!
        Me.Line211.Name = "Line211"
        Me.Line211.Top = 10.978!
        Me.Line211.Width = 1.999998!
        Me.Line211.X1 = 2.262!
        Me.Line211.X2 = 4.261998!
        Me.Line211.Y1 = 10.978!
        Me.Line211.Y2 = 10.978!
        '
        'Line212
        '
        Me.Line212.Height = 0!
        Me.Line212.Left = 2.262!
        Me.Line212.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line212.LineWeight = 1.0!
        Me.Line212.Name = "Line212"
        Me.Line212.Top = 11.165!
        Me.Line212.Width = 1.999999!
        Me.Line212.X1 = 2.262!
        Me.Line212.X2 = 4.261999!
        Me.Line212.Y1 = 11.165!
        Me.Line212.Y2 = 11.165!
        '
        'Line213
        '
        Me.Line213.Height = 0!
        Me.Line213.Left = 2.262!
        Me.Line213.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line213.LineWeight = 1.0!
        Me.Line213.Name = "Line213"
        Me.Line213.Top = 11.353!
        Me.Line213.Width = 2.000001!
        Me.Line213.X1 = 2.262!
        Me.Line213.X2 = 4.262001!
        Me.Line213.Y1 = 11.353!
        Me.Line213.Y2 = 11.353!
        '
        'Line214
        '
        Me.Line214.Height = 0!
        Me.Line214.Left = 2.262!
        Me.Line214.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line214.LineWeight = 1.0!
        Me.Line214.Name = "Line214"
        Me.Line214.Top = 11.541!
        Me.Line214.Width = 2.000001!
        Me.Line214.X1 = 2.262!
        Me.Line214.X2 = 4.262001!
        Me.Line214.Y1 = 11.541!
        Me.Line214.Y2 = 11.541!
        '
        'Label793
        '
        Me.Label793.Height = 0.1875!
        Me.Label793.HyperLink = Nothing
        Me.Label793.Left = 4.27!
        Me.Label793.Name = "Label793"
        Me.Label793.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label793.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "= (g - tstiff)/ 2" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label793.Top = 10.79!
        Me.Label793.Width = 3.75!
        '
        'Line215
        '
        Me.Line215.Height = 1.311!
        Me.Line215.Left = 2.262!
        Me.Line215.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line215.LineWeight = 1.0!
        Me.Line215.Name = "Line215"
        Me.Line215.Top = 10.416!
        Me.Line215.Width = 0!
        Me.Line215.X1 = 2.262!
        Me.Line215.X2 = 2.262!
        Me.Line215.Y1 = 10.416!
        Me.Line215.Y2 = 11.727!
        '
        'Line216
        '
        Me.Line216.Height = 1.311!
        Me.Line216.Left = 3.262!
        Me.Line216.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line216.LineWeight = 1.0!
        Me.Line216.Name = "Line216"
        Me.Line216.Top = 10.416!
        Me.Line216.Width = 0!
        Me.Line216.X1 = 3.262!
        Me.Line216.X2 = 3.262!
        Me.Line216.Y1 = 10.416!
        Me.Line216.Y2 = 11.727!
        '
        'Line217
        '
        Me.Line217.Height = 1.311!
        Me.Line217.Left = 4.261999!
        Me.Line217.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line217.LineWeight = 1.0!
        Me.Line217.Name = "Line217"
        Me.Line217.Top = 10.416!
        Me.Line217.Width = 0.0000009536743!
        Me.Line217.X1 = 4.261999!
        Me.Line217.X2 = 4.262!
        Me.Line217.Y1 = 10.416!
        Me.Line217.Y2 = 11.727!
        '
        'Line218
        '
        Me.Line218.Height = 0!
        Me.Line218.Left = 2.262!
        Me.Line218.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line218.LineWeight = 1.0!
        Me.Line218.Name = "Line218"
        Me.Line218.Top = 10.416!
        Me.Line218.Width = 2.000001!
        Me.Line218.X1 = 2.262!
        Me.Line218.X2 = 4.262001!
        Me.Line218.Y1 = 10.416!
        Me.Line218.Y2 = 10.416!
        '
        'Label794
        '
        Me.Label794.Height = 0.1875!
        Me.Label794.HyperLink = Nothing
        Me.Label794.Left = 4.27!
        Me.Label794.Name = "Label794"
        Me.Label794.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label794.Text = "in" & Global.Microsoft.VisualBasic.ChrW(9) & "centerline of top link bolt to top of column" & Global.Microsoft.VisualBasic.ChrW(9) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label794.Top = 10.603!
        Me.Label794.Width = 3.75!
        '
        'Line219
        '
        Me.Line219.Height = 0!
        Me.Line219.Left = 2.262!
        Me.Line219.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line219.LineWeight = 1.0!
        Me.Line219.Name = "Line219"
        Me.Line219.Top = 11.727!
        Me.Line219.Width = 1.999999!
        Me.Line219.X1 = 2.262!
        Me.Line219.X2 = 4.261999!
        Me.Line219.Y1 = 11.727!
        Me.Line219.Y2 = 11.727!
        '
        'Label796
        '
        Me.Label796.Height = 0.1875!
        Me.Label796.HyperLink = Nothing
        Me.Label796.Left = 4.27!
        Me.Label796.Name = "Label796"
        Me.Label796.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label796.Text = "                   OK, if SUM_colFLB2 < SUM_Allowed" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label796.Top = 11.728!
        Me.Label796.Width = 3.24!
        '
        'S315_005
        '
        Me.S315_005.Height = 0.1875!
        Me.S315_005.HyperLink = Nothing
        Me.S315_005.Left = 3.26!
        Me.S315_005.Name = "S315_005"
        Me.S315_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S315_005.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_005.Top = 4.999001!
        Me.S315_005.Width = 1.0!
        '
        'S315_006
        '
        Me.S315_006.Height = 0.1875!
        Me.S315_006.HyperLink = Nothing
        Me.S315_006.Left = 3.26!
        Me.S315_006.Name = "S315_006"
        Me.S315_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S315_006.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_006.Top = 5.186!
        Me.S315_006.Width = 1.0!
        '
        'S315_007
        '
        Me.S315_007.Height = 0.1875!
        Me.S315_007.HyperLink = Nothing
        Me.S315_007.Left = 3.256001!
        Me.S315_007.Name = "S315_007"
        Me.S315_007.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S315_007.Text = "0.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_007.Top = 5.374001!
        Me.S315_007.Width = 1.0!
        '
        'S315_008
        '
        Me.S315_008.Height = 0.1875!
        Me.S315_008.HyperLink = Nothing
        Me.S315_008.Left = 3.26!
        Me.S315_008.Name = "S315_008"
        Me.S315_008.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S315_008.Text = "0.96" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S315_008.Top = 5.561!
        Me.S315_008.Width = 1.0!
        '
        'Line1
        '
        Me.Line1.Height = 0!
        Me.Line1.Left = 2.261!
        Me.Line1.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line1.LineWeight = 1.0!
        Me.Line1.Name = "Line1"
        Me.Line1.Top = 5.748001!
        Me.Line1.Width = 2.0!
        Me.Line1.X1 = 2.261!
        Me.Line1.X2 = 4.261!
        Me.Line1.Y1 = 5.748001!
        Me.Line1.Y2 = 5.748001!
        '
        'Line2
        '
        Me.Line2.Height = 0!
        Me.Line2.Left = 2.263!
        Me.Line2.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line2.LineWeight = 1.0!
        Me.Line2.Name = "Line2"
        Me.Line2.Top = 5.561!
        Me.Line2.Width = 2.0!
        Me.Line2.X1 = 2.263!
        Me.Line2.X2 = 4.263!
        Me.Line2.Y1 = 5.561!
        Me.Line2.Y2 = 5.561!
        '
        'Line3
        '
        Me.Line3.Height = 0!
        Me.Line3.Left = 2.25!
        Me.Line3.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line3.LineWeight = 1.0!
        Me.Line3.Name = "Line3"
        Me.Line3.Top = 5.374001!
        Me.Line3.Width = 2.0!
        Me.Line3.X1 = 2.25!
        Me.Line3.X2 = 4.25!
        Me.Line3.Y1 = 5.374001!
        Me.Line3.Y2 = 5.374001!
        '
        'Line4
        '
        Me.Line4.Height = 0!
        Me.Line4.Left = 2.25!
        Me.Line4.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line4.LineWeight = 1.0!
        Me.Line4.Name = "Line4"
        Me.Line4.Top = 5.186!
        Me.Line4.Width = 2.0!
        Me.Line4.X1 = 2.25!
        Me.Line4.X2 = 4.25!
        Me.Line4.Y1 = 5.186!
        Me.Line4.Y2 = 5.186!
        '
        'Line5
        '
        Me.Line5.Height = 0!
        Me.Line5.Left = 2.25!
        Me.Line5.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line5.LineWeight = 1.0!
        Me.Line5.Name = "Line5"
        Me.Line5.Top = 4.999001!
        Me.Line5.Width = 2.0!
        Me.Line5.X1 = 2.25!
        Me.Line5.X2 = 4.25!
        Me.Line5.Y1 = 4.999001!
        Me.Line5.Y2 = 4.999001!
        '
        'Line6
        '
        Me.Line6.Height = 0.750999!
        Me.Line6.Left = 2.258!
        Me.Line6.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line6.LineWeight = 1.0!
        Me.Line6.Name = "Line6"
        Me.Line6.Top = 4.999001!
        Me.Line6.Width = 0!
        Me.Line6.X1 = 2.258!
        Me.Line6.X2 = 2.258!
        Me.Line6.Y1 = 4.999001!
        Me.Line6.Y2 = 5.75!
        '
        'Line7
        '
        Me.Line7.Height = 0.750999!
        Me.Line7.Left = 4.265!
        Me.Line7.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line7.LineWeight = 1.0!
        Me.Line7.Name = "Line7"
        Me.Line7.Top = 4.999001!
        Me.Line7.Width = 0!
        Me.Line7.X1 = 4.265!
        Me.Line7.X2 = 4.265!
        Me.Line7.Y1 = 4.999001!
        Me.Line7.Y2 = 5.75!
        '
        'Line8
        '
        Me.Line8.Height = 0.7509999!
        Me.Line8.Left = 3.259!
        Me.Line8.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line8.LineWeight = 1.0!
        Me.Line8.Name = "Line8"
        Me.Line8.Top = 4.997001!
        Me.Line8.Width = 0!
        Me.Line8.X1 = 3.259!
        Me.Line8.X2 = 3.259!
        Me.Line8.Y1 = 4.997001!
        Me.Line8.Y2 = 5.748001!
        '
        'Label2
        '
        Me.Label2.Height = 0.1875!
        Me.Label2.HyperLink = Nothing
        Me.Label2.Left = 0!
        Me.Label2.Name = "Label2"
        Me.Label2.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label2.Text = "3.18 DESIGN SUMMARY:"
        Me.Label2.Top = 14.281!
        Me.Label2.Width = 4.761!
        '
        'Label3
        '
        Me.Label3.Height = 0.1875!
        Me.Label3.HyperLink = Nothing
        Me.Label3.Left = 0.8870006!
        Me.Label3.Name = "Label3"
        Me.Label3.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label3.Text = "DCR Summary:"
        Me.Label3.Top = 14.457!
        Me.Label3.Width = 1.0!
        '
        'Label5
        '
        Me.Label5.Height = 0.188!
        Me.Label5.HyperLink = Nothing
        Me.Label5.Left = 0.0000002384186!
        Me.Label5.Name = "Label5"
        Me.Label5.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label5.Text = "SCWB_DCR_total="
        Me.Label5.Top = 14.635!
        Me.Label5.Width = 1.896!
        '
        'S319_001
        '
        Me.S319_001.Height = 0.188!
        Me.S319_001.HyperLink = Nothing
        Me.S319_001.Left = 1.908001!
        Me.S319_001.Name = "S319_001"
        Me.S319_001.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_001.Text = "1.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_001.Top = 14.644!
        Me.S319_001.Width = 0.75!
        '
        'Label7
        '
        Me.Label7.Height = 0.188!
        Me.Label7.HyperLink = Nothing
        Me.Label7.Left = 0.0000002384186!
        Me.Label7.Name = "Label7"
        Me.Label7.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label7.Text = "DCR_PZ_total="
        Me.Label7.Top = 14.822!
        Me.Label7.Width = 1.896!
        '
        'S319_002
        '
        Me.S319_002.Height = 0.188!
        Me.S319_002.HyperLink = Nothing
        Me.S319_002.Left = 1.908001!
        Me.S319_002.Name = "S319_002"
        Me.S319_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_002.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_002.Top = 14.831!
        Me.S319_002.Width = 0.75!
        '
        'S319_015
        '
        Me.S319_015.Height = 0.188!
        Me.S319_015.HyperLink = Nothing
        Me.S319_015.Left = 3.644!
        Me.S319_015.Name = "S319_015"
        Me.S319_015.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_015.Text = "1.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_015.Top = 14.646!
        Me.S319_015.Width = 0.75!
        '
        'S319_016
        '
        Me.S319_016.Height = 0.188!
        Me.S319_016.HyperLink = Nothing
        Me.S319_016.Left = 3.644!
        Me.S319_016.Name = "S319_016"
        Me.S319_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_016.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_016.Top = 14.833!
        Me.S319_016.Width = 0.75!
        '
        'Line9
        '
        Me.Line9.Height = 0!
        Me.Line9.Left = 1.908!
        Me.Line9.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line9.LineWeight = 1.0!
        Me.Line9.Name = "Line9"
        Me.Line9.Top = 14.833!
        Me.Line9.Width = 0.7460001!
        Me.Line9.X1 = 1.908!
        Me.Line9.X2 = 2.654!
        Me.Line9.Y1 = 14.833!
        Me.Line9.Y2 = 14.833!
        '
        'Line10
        '
        Me.Line10.Height = 0!
        Me.Line10.Left = 1.914!
        Me.Line10.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line10.LineWeight = 1.0!
        Me.Line10.Name = "Line10"
        Me.Line10.Top = 14.646!
        Me.Line10.Width = 0.74!
        Me.Line10.X1 = 1.914!
        Me.Line10.X2 = 2.654!
        Me.Line10.Y1 = 14.646!
        Me.Line10.Y2 = 14.646!
        '
        'Line11
        '
        Me.Line11.Height = 0!
        Me.Line11.Left = 2.910001!
        Me.Line11.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line11.LineWeight = 1.0!
        Me.Line11.Name = "Line11"
        Me.Line11.Top = 15.209!
        Me.Line11.Width = 1.500999!
        Me.Line11.X1 = 2.910001!
        Me.Line11.X2 = 4.411!
        Me.Line11.Y1 = 15.209!
        Me.Line11.Y2 = 15.209!
        '
        'Label12
        '
        Me.Label12.Height = 0.1875!
        Me.Label12.HyperLink = Nothing
        Me.Label12.Left = 0.0000002384186!
        Me.Label12.Name = "Label12"
        Me.Label12.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label12.Text = "DCR_PZ_total_NEW="
        Me.Label12.Top = 15.01!
        Me.Label12.Width = 1.896!
        '
        'S319_003
        '
        Me.S319_003.Height = 0.188!
        Me.S319_003.HyperLink = Nothing
        Me.S319_003.Left = 1.914!
        Me.S319_003.Name = "S319_003"
        Me.S319_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_003.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_003.Top = 15.019!
        Me.S319_003.Width = 0.75!
        '
        'S319_017
        '
        Me.S319_017.Height = 0.188!
        Me.S319_017.HyperLink = Nothing
        Me.S319_017.Left = 3.658!
        Me.S319_017.Name = "S319_017"
        Me.S319_017.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_017.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_017.Top = 15.021!
        Me.S319_017.Width = 0.75!
        '
        'Label16
        '
        Me.Label16.Height = 0.1875!
        Me.Label16.HyperLink = Nothing
        Me.Label16.Left = 0.0000002384186!
        Me.Label16.Name = "Label16"
        Me.Label16.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label16.Text = "DCR_stiff_comp_DCR="
        Me.Label16.Top = 15.197!
        Me.Label16.Width = 1.896!
        '
        'S319_004
        '
        Me.S319_004.Height = 0.188!
        Me.S319_004.HyperLink = Nothing
        Me.S319_004.Left = 1.914!
        Me.S319_004.Name = "S319_004"
        Me.S319_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_004.Top = 15.206!
        Me.S319_004.Width = 0.75!
        '
        'S319_018
        '
        Me.S319_018.Height = 0.188!
        Me.S319_018.HyperLink = Nothing
        Me.S319_018.Left = 3.644!
        Me.S319_018.Name = "S319_018"
        Me.S319_018.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_018.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_018.Top = 15.208!
        Me.S319_018.Width = 0.75!
        '
        'Label20
        '
        Me.Label20.Height = 0.1875!
        Me.Label20.HyperLink = Nothing
        Me.Label20.Left = 0.0000002384186!
        Me.Label20.Name = "Label20"
        Me.Label20.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label20.Text = "wfillet_flange_DCR="
        Me.Label20.Top = 15.385!
        Me.Label20.Width = 1.896!
        '
        'S319_005
        '
        Me.S319_005.Height = 0.188!
        Me.S319_005.HyperLink = Nothing
        Me.S319_005.Left = 1.908001!
        Me.S319_005.Name = "S319_005"
        Me.S319_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_005.Top = 15.394!
        Me.S319_005.Width = 0.75!
        '
        'S319_019
        '
        Me.S319_019.Height = 0.188!
        Me.S319_019.HyperLink = Nothing
        Me.S319_019.Left = 3.644!
        Me.S319_019.Name = "S319_019"
        Me.S319_019.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_019.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_019.Top = 15.396!
        Me.S319_019.Width = 0.75!
        '
        'Label24
        '
        Me.Label24.Height = 0.188!
        Me.Label24.HyperLink = Nothing
        Me.Label24.Left = 0.0000002384186!
        Me.Label24.Name = "Label24"
        Me.Label24.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label24.Text = "wfillet_web_DCR=DCR_colFLB1="
        Me.Label24.Top = 15.572!
        Me.Label24.Width = 1.896!
        '
        'S319_006
        '
        Me.S319_006.Height = 0.188!
        Me.S319_006.HyperLink = Nothing
        Me.S319_006.Left = 1.912999!
        Me.S319_006.Name = "S319_006"
        Me.S319_006.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_006.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_006.Top = 15.581!
        Me.S319_006.Width = 0.75!
        '
        'S319_020
        '
        Me.S319_020.Height = 0.188!
        Me.S319_020.HyperLink = Nothing
        Me.S319_020.Left = 3.644!
        Me.S319_020.Name = "S319_020"
        Me.S319_020.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_020.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_020.Top = 15.582!
        Me.S319_020.Width = 0.75!
        '
        'Line12
        '
        Me.Line12.Height = 0!
        Me.Line12.Left = 1.908!
        Me.Line12.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line12.LineWeight = 1.0!
        Me.Line12.Name = "Line12"
        Me.Line12.Top = 15.021!
        Me.Line12.Width = 2.503!
        Me.Line12.X1 = 1.908!
        Me.Line12.X2 = 4.411!
        Me.Line12.Y1 = 15.021!
        Me.Line12.Y2 = 15.021!
        '
        'Line13
        '
        Me.Line13.Height = 0!
        Me.Line13.Left = 1.908!
        Me.Line13.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line13.LineWeight = 1.0!
        Me.Line13.Name = "Line13"
        Me.Line13.Top = 15.208!
        Me.Line13.Width = 2.503!
        Me.Line13.X1 = 1.908!
        Me.Line13.X2 = 4.411!
        Me.Line13.Y1 = 15.208!
        Me.Line13.Y2 = 15.208!
        '
        'Line14
        '
        Me.Line14.Height = 0!
        Me.Line14.Left = 1.908!
        Me.Line14.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line14.LineWeight = 1.0!
        Me.Line14.Name = "Line14"
        Me.Line14.Top = 15.396!
        Me.Line14.Width = 2.503!
        Me.Line14.X1 = 1.908!
        Me.Line14.X2 = 4.411!
        Me.Line14.Y1 = 15.396!
        Me.Line14.Y2 = 15.396!
        '
        'Line15
        '
        Me.Line15.Height = 0!
        Me.Line15.Left = 1.908!
        Me.Line15.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line15.LineWeight = 1.0!
        Me.Line15.Name = "Line15"
        Me.Line15.Top = 15.584!
        Me.Line15.Width = 2.503!
        Me.Line15.X1 = 1.908!
        Me.Line15.X2 = 4.411!
        Me.Line15.Y1 = 15.584!
        Me.Line15.Y2 = 15.584!
        '
        'Line20
        '
        Me.Line20.Height = 0!
        Me.Line20.Left = 1.908!
        Me.Line20.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line20.LineWeight = 1.0!
        Me.Line20.Name = "Line20"
        Me.Line20.Top = 15.77!
        Me.Line20.Width = 2.503!
        Me.Line20.X1 = 1.908!
        Me.Line20.X2 = 4.411!
        Me.Line20.Y1 = 15.77!
        Me.Line20.Y2 = 15.77!
        '
        'Label27
        '
        Me.Label27.Height = 0.1875!
        Me.Label27.HyperLink = Nothing
        Me.Label27.Left = 3.644!
        Me.Label27.Name = "Label27"
        Me.Label27.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label27.Text = "Report:"
        Me.Label27.Top = 14.46!
        Me.Label27.Width = 0.7670004!
        '
        'Line22
        '
        Me.Line22.Height = 0.0009994507!
        Me.Line22.Left = 3.658!
        Me.Line22.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line22.LineWeight = 1.0!
        Me.Line22.Name = "Line22"
        Me.Line22.Top = 14.833!
        Me.Line22.Width = 0.7529998!
        Me.Line22.X1 = 3.658!
        Me.Line22.X2 = 4.411!
        Me.Line22.Y1 = 14.834!
        Me.Line22.Y2 = 14.833!
        '
        'S319_013
        '
        Me.S319_013.Height = 0.1875!
        Me.S319_013.HyperLink = Nothing
        Me.S319_013.Left = 2.664!
        Me.S319_013.Name = "S319_013"
        Me.S319_013.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_013.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_013.Top = 15.777!
        Me.S319_013.Width = 1.004!
        '
        'S319_007
        '
        Me.S319_007.Height = 0.188!
        Me.S319_007.HyperLink = Nothing
        Me.S319_007.Left = 1.920999!
        Me.S319_007.Name = "S319_007"
        Me.S319_007.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_007.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_007.Top = 15.773!
        Me.S319_007.Width = 0.75!
        '
        'S319_021
        '
        Me.S319_021.Height = 0.188!
        Me.S319_021.HyperLink = Nothing
        Me.S319_021.Left = 3.644!
        Me.S319_021.Name = "S319_021"
        Me.S319_021.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_021.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_021.Top = 15.766!
        Me.S319_021.Width = 0.75!
        '
        'S319_008
        '
        Me.S319_008.Height = 0.188!
        Me.S319_008.HyperLink = Nothing
        Me.S319_008.Left = 1.903999!
        Me.S319_008.Name = "S319_008"
        Me.S319_008.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_008.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_008.Top = 15.962!
        Me.S319_008.Width = 0.75!
        '
        'S319_022
        '
        Me.S319_022.Height = 0.188!
        Me.S319_022.HyperLink = Nothing
        Me.S319_022.Left = 3.658!
        Me.S319_022.Name = "S319_022"
        Me.S319_022.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S319_022.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_022.Top = 15.954!
        Me.S319_022.Width = 0.75!
        '
        'Line24
        '
        Me.Line24.Height = 0!
        Me.Line24.Left = 1.899001!
        Me.Line24.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line24.LineWeight = 1.0!
        Me.Line24.Name = "Line24"
        Me.Line24.Top = 16.151!
        Me.Line24.Width = 2.511999!
        Me.Line24.X1 = 1.899001!
        Me.Line24.X2 = 4.411!
        Me.Line24.Y1 = 16.151!
        Me.Line24.Y2 = 16.151!
        '
        'Label34
        '
        Me.Label34.Height = 0.188!
        Me.Label34.HyperLink = Nothing
        Me.Label34.Left = 0!
        Me.Label34.Name = "Label34"
        Me.Label34.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label34.Text = "DCR_colFLB2="
        Me.Label34.Top = 15.756!
        Me.Label34.Width = 1.896!
        '
        'Label35
        '
        Me.Label35.Height = 0.188!
        Me.Label35.HyperLink = Nothing
        Me.Label35.Left = 0!
        Me.Label35.Name = "Label35"
        Me.Label35.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label35.Text = "DCR_colFLB="
        Me.Label35.Top = 15.954!
        Me.Label35.Width = 1.896!
        '
        'S319_031
        '
        Me.S319_031.Height = 0.188!
        Me.S319_031.HyperLink = Nothing
        Me.S319_031.Left = 6.209!
        Me.S319_031.Name = "S319_031"
        Me.S319_031.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_031.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_031.Top = 15.213!
        Me.S319_031.Width = 0.75!
        '
        'S319_030
        '
        Me.S319_030.Height = 0.188!
        Me.S319_030.HyperLink = Nothing
        Me.S319_030.Left = 6.209!
        Me.S319_030.Name = "S319_030"
        Me.S319_030.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_030.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_030.Top = 15.025!
        Me.S319_030.Width = 0.75!
        '
        'Label38
        '
        Me.Label38.Height = 0.1875!
        Me.Label38.HyperLink = Nothing
        Me.Label38.Left = 6.203!
        Me.Label38.Name = "Label38"
        Me.Label38.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label38.Text = "PlugWeld?"
        Me.Label38.Top = 14.654!
        Me.Label38.Width = 0.7560003!
        '
        'Label39
        '
        Me.Label39.Height = 0.1875!
        Me.Label39.HyperLink = Nothing
        Me.Label39.Left = 4.410999!
        Me.Label39.Name = "Label39"
        Me.Label39.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label39.Text = "Geometry/Weld Summary:"
        Me.Label39.Top = 14.469!
        Me.Label39.Width = 1.741!
        '
        'Label40
        '
        Me.Label40.Height = 0.188!
        Me.Label40.HyperLink = Nothing
        Me.Label40.Left = 4.410999!
        Me.Label40.Name = "Label40"
        Me.Label40.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label40.Text = "DP used?"
        Me.Label40.Top = 14.649!
        Me.Label40.Width = 1.042!
        '
        'S319_024
        '
        Me.S319_024.Height = 0.188!
        Me.S319_024.HyperLink = Nothing
        Me.S319_024.Left = 5.453001!
        Me.S319_024.Name = "S319_024"
        Me.S319_024.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_024.Text = "1.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_024.Top = 14.648!
        Me.S319_024.Width = 0.75!
        '
        'Label42
        '
        Me.Label42.Height = 0.188!
        Me.Label42.HyperLink = Nothing
        Me.Label42.Left = 4.410999!
        Me.Label42.Name = "Label42"
        Me.Label42.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label42.Text = "t_dblr_use="
        Me.Label42.Top = 14.837!
        Me.Label42.Width = 1.042!
        '
        'S319_025
        '
        Me.S319_025.Height = 0.188!
        Me.S319_025.HyperLink = Nothing
        Me.S319_025.Left = 5.453001!
        Me.S319_025.Name = "S319_025"
        Me.S319_025.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_025.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_025.Top = 14.835!
        Me.S319_025.Width = 0.75!
        '
        'S319_034
        '
        Me.S319_034.Height = 0.188!
        Me.S319_034.HyperLink = Nothing
        Me.S319_034.Left = 6.968999!
        Me.S319_034.Name = "S319_034"
        Me.S319_034.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_034.Text = "1.25" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_034.Top = 14.65!
        Me.S319_034.Width = 0.75!
        '
        'S319_035
        '
        Me.S319_035.Height = 0.188!
        Me.S319_035.HyperLink = Nothing
        Me.S319_035.Left = 6.968999!
        Me.S319_035.Name = "S319_035"
        Me.S319_035.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_035.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_035.Top = 14.837!
        Me.S319_035.Width = 0.75!
        '
        'Line25
        '
        Me.Line25.Height = 0!
        Me.Line25.Left = 5.458999!
        Me.Line25.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line25.LineWeight = 1.0!
        Me.Line25.Name = "Line25"
        Me.Line25.Top = 14.837!
        Me.Line25.Width = 0.75!
        Me.Line25.X1 = 5.458999!
        Me.Line25.X2 = 6.208999!
        Me.Line25.Y1 = 14.837!
        Me.Line25.Y2 = 14.837!
        '
        'Line26
        '
        Me.Line26.Height = 0!
        Me.Line26.Left = 5.458999!
        Me.Line26.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line26.LineWeight = 1.0!
        Me.Line26.Name = "Line26"
        Me.Line26.Top = 14.65!
        Me.Line26.Width = 0.75!
        Me.Line26.X1 = 5.458999!
        Me.Line26.X2 = 6.208999!
        Me.Line26.Y1 = 14.65!
        Me.Line26.Y2 = 14.65!
        '
        'Line27
        '
        Me.Line27.Height = 0!
        Me.Line27.Left = 6.455001!
        Me.Line27.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line27.LineWeight = 1.0!
        Me.Line27.Name = "Line27"
        Me.Line27.Top = 15.213!
        Me.Line27.Width = 1.263999!
        Me.Line27.X1 = 6.455001!
        Me.Line27.X2 = 7.719!
        Me.Line27.Y1 = 15.213!
        Me.Line27.Y2 = 15.213!
        '
        'Label46
        '
        Me.Label46.Height = 0.188!
        Me.Label46.HyperLink = Nothing
        Me.Label46.Left = 4.410999!
        Me.Label46.Name = "Label46"
        Me.Label46.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label46.Text = "Stiffener Req'd="
        Me.Label46.Top = 15.03!
        Me.Label46.Width = 1.042!
        '
        'S319_026
        '
        Me.S319_026.Height = 0.188!
        Me.S319_026.HyperLink = Nothing
        Me.S319_026.Left = 5.459!
        Me.S319_026.Name = "S319_026"
        Me.S319_026.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_026.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_026.Top = 15.023!
        Me.S319_026.Width = 0.75!
        '
        'S319_036
        '
        Me.S319_036.Height = 0.188!
        Me.S319_036.HyperLink = Nothing
        Me.S319_036.Left = 6.968999!
        Me.S319_036.Name = "S319_036"
        Me.S319_036.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_036.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_036.Top = 15.025!
        Me.S319_036.Width = 0.75!
        '
        'Label49
        '
        Me.Label49.Height = 0.188!
        Me.Label49.HyperLink = Nothing
        Me.Label49.Left = 4.410999!
        Me.Label49.Name = "Label49"
        Me.Label49.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label49.Text = "t_stiff_Use="
        Me.Label49.Top = 15.212!
        Me.Label49.Width = 1.042!
        '
        'S319_027
        '
        Me.S319_027.Height = 0.188!
        Me.S319_027.HyperLink = Nothing
        Me.S319_027.Left = 5.459!
        Me.S319_027.Name = "S319_027"
        Me.S319_027.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_027.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_027.Top = 15.21!
        Me.S319_027.Width = 0.75!
        '
        'S319_037
        '
        Me.S319_037.Height = 0.188!
        Me.S319_037.HyperLink = Nothing
        Me.S319_037.Left = 6.968999!
        Me.S319_037.Name = "S319_037"
        Me.S319_037.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_037.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_037.Top = 15.212!
        Me.S319_037.Width = 0.75!
        '
        'Line28
        '
        Me.Line28.Height = 0!
        Me.Line28.Left = 5.453001!
        Me.Line28.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line28.LineWeight = 1.0!
        Me.Line28.Name = "Line28"
        Me.Line28.Top = 15.025!
        Me.Line28.Width = 2.265999!
        Me.Line28.X1 = 5.453001!
        Me.Line28.X2 = 7.719!
        Me.Line28.Y1 = 15.025!
        Me.Line28.Y2 = 15.025!
        '
        'Line29
        '
        Me.Line29.Height = 0!
        Me.Line29.Left = 5.453001!
        Me.Line29.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line29.LineWeight = 1.0!
        Me.Line29.Name = "Line29"
        Me.Line29.Top = 15.212!
        Me.Line29.Width = 2.265999!
        Me.Line29.X1 = 5.453001!
        Me.Line29.X2 = 7.719!
        Me.Line29.Y1 = 15.212!
        Me.Line29.Y2 = 15.212!
        '
        'Label52
        '
        Me.Label52.Height = 0.1875!
        Me.Label52.HyperLink = Nothing
        Me.Label52.Left = 6.959!
        Me.Label52.Name = "Label52"
        Me.Label52.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label52.Text = "Report:"
        Me.Label52.Top = 14.462!
        Me.Label52.Width = 0.7599998!
        '
        'Line34
        '
        Me.Line34.Height = 0!
        Me.Line34.Left = 6.958999!
        Me.Line34.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line34.LineWeight = 1.0!
        Me.Line34.Name = "Line34"
        Me.Line34.Top = 14.65!
        Me.Line34.Width = 0.7600007!
        Me.Line34.X1 = 6.958999!
        Me.Line34.X2 = 7.719!
        Me.Line34.Y1 = 14.65!
        Me.Line34.Y2 = 14.65!
        '
        'Line35
        '
        Me.Line35.Height = 0!
        Me.Line35.Left = 6.958999!
        Me.Line35.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line35.LineWeight = 1.0!
        Me.Line35.Name = "Line35"
        Me.Line35.Top = 14.837!
        Me.Line35.Width = 0.7600007!
        Me.Line35.X1 = 6.958999!
        Me.Line35.X2 = 7.719!
        Me.Line35.Y1 = 14.837!
        Me.Line35.Y2 = 14.837!
        '
        'S319_032
        '
        Me.S319_032.Height = 0.188!
        Me.S319_032.HyperLink = Nothing
        Me.S319_032.Left = 6.209!
        Me.S319_032.Name = "S319_032"
        Me.S319_032.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_032.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_032.Top = 15.408!
        Me.S319_032.Width = 0.75!
        '
        'Label54
        '
        Me.Label54.Height = 0.188!
        Me.Label54.HyperLink = Nothing
        Me.Label54.Left = 4.410999!
        Me.Label54.Name = "Label54"
        Me.Label54.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label54.Text = "wfillet_flange="
        Me.Label54.Top = 15.405!
        Me.Label54.Width = 1.042!
        '
        'S319_028
        '
        Me.S319_028.Height = 0.188!
        Me.S319_028.HyperLink = Nothing
        Me.S319_028.Left = 5.459001!
        Me.S319_028.Name = "S319_028"
        Me.S319_028.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_028.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_028.Top = 15.405!
        Me.S319_028.Width = 0.75!
        '
        'S319_038
        '
        Me.S319_038.Height = 0.188!
        Me.S319_038.HyperLink = Nothing
        Me.S319_038.Left = 6.969!
        Me.S319_038.Name = "S319_038"
        Me.S319_038.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_038.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_038.Top = 15.407!
        Me.S319_038.Width = 0.75!
        '
        'S319_033
        '
        Me.S319_033.Height = 0.188!
        Me.S319_033.HyperLink = Nothing
        Me.S319_033.Left = 6.209!
        Me.S319_033.Name = "S319_033"
        Me.S319_033.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_033.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_033.Top = 15.598!
        Me.S319_033.Width = 0.75!
        '
        'Label58
        '
        Me.Label58.Height = 0.188!
        Me.Label58.HyperLink = Nothing
        Me.Label58.Left = 4.410999!
        Me.Label58.Name = "Label58"
        Me.Label58.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label58.Text = "wfillet_web="
        Me.Label58.Top = 15.598!
        Me.Label58.Width = 1.042!
        '
        'S319_029
        '
        Me.S319_029.Height = 0.188!
        Me.S319_029.HyperLink = Nothing
        Me.S319_029.Left = 5.453001!
        Me.S319_029.Name = "S319_029"
        Me.S319_029.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S319_029.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_029.Top = 15.598!
        Me.S319_029.Width = 0.75!
        '
        'S319_039
        '
        Me.S319_039.Height = 0.188!
        Me.S319_039.HyperLink = Nothing
        Me.S319_039.Left = 6.968999!
        Me.S319_039.Name = "S319_039"
        Me.S319_039.Style = "color: Blue; font-size: 8.25pt; font-weight: bold; text-align: center; vertical-a" &
    "lign: middle; ddo-char-set: 0"
        Me.S319_039.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S319_039.Top = 15.597!
        Me.S319_039.Width = 0.75!
        '
        'Line18
        '
        Me.Line18.Height = 1.505!
        Me.Line18.Left = 4.411!
        Me.Line18.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line18.LineWeight = 1.0!
        Me.Line18.Name = "Line18"
        Me.Line18.Top = 14.637!
        Me.Line18.Width = 0.000001430511!
        Me.Line18.X1 = 4.411!
        Me.Line18.X2 = 4.411001!
        Me.Line18.Y1 = 14.637!
        Me.Line18.Y2 = 16.142!
        '
        'Line30
        '
        Me.Line30.Height = 1.118999!
        Me.Line30.Left = 5.458999!
        Me.Line30.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line30.LineWeight = 1.0!
        Me.Line30.Name = "Line30"
        Me.Line30.Top = 14.656!
        Me.Line30.Width = 0.000001907349!
        Me.Line30.X1 = 5.458999!
        Me.Line30.X2 = 5.459001!
        Me.Line30.Y1 = 14.656!
        Me.Line30.Y2 = 15.775!
        '
        'Line31
        '
        Me.Line31.Height = 1.118999!
        Me.Line31.Left = 6.209!
        Me.Line31.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line31.LineWeight = 1.0!
        Me.Line31.Name = "Line31"
        Me.Line31.Top = 14.656!
        Me.Line31.Width = 0!
        Me.Line31.X1 = 6.209!
        Me.Line31.X2 = 6.209!
        Me.Line31.Y1 = 14.656!
        Me.Line31.Y2 = 15.775!
        '
        'Line32
        '
        Me.Line32.Height = 1.127!
        Me.Line32.Left = 6.959!
        Me.Line32.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line32.LineWeight = 1.0!
        Me.Line32.Name = "Line32"
        Me.Line32.Top = 14.648!
        Me.Line32.Width = 0!
        Me.Line32.X1 = 6.959!
        Me.Line32.X2 = 6.959!
        Me.Line32.Y1 = 14.648!
        Me.Line32.Y2 = 15.775!
        '
        'Line33
        '
        Me.Line33.Height = 1.118999!
        Me.Line33.Left = 7.719!
        Me.Line33.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line33.LineWeight = 1.0!
        Me.Line33.Name = "Line33"
        Me.Line33.Top = 14.656!
        Me.Line33.Width = 0!
        Me.Line33.X1 = 7.719!
        Me.Line33.X2 = 7.719!
        Me.Line33.Y1 = 14.656!
        Me.Line33.Y2 = 15.775!
        '
        'Line36
        '
        Me.Line36.Height = 0!
        Me.Line36.Left = 5.458998!
        Me.Line36.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line36.LineWeight = 1.0!
        Me.Line36.Name = "Line36"
        Me.Line36.Top = 15.398!
        Me.Line36.Width = 2.260002!
        Me.Line36.X1 = 5.458998!
        Me.Line36.X2 = 7.719!
        Me.Line36.Y1 = 15.398!
        Me.Line36.Y2 = 15.398!
        '
        'Line37
        '
        Me.Line37.Height = 0!
        Me.Line37.Left = 5.459001!
        Me.Line37.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line37.LineWeight = 1.0!
        Me.Line37.Name = "Line37"
        Me.Line37.Top = 15.587!
        Me.Line37.Width = 2.259999!
        Me.Line37.X1 = 5.459001!
        Me.Line37.X2 = 7.719!
        Me.Line37.Y1 = 15.587!
        Me.Line37.Y2 = 15.587!
        '
        'Line38
        '
        Me.Line38.Height = 0!
        Me.Line38.Left = 5.447!
        Me.Line38.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line38.LineWeight = 1.0!
        Me.Line38.Name = "Line38"
        Me.Line38.Top = 15.775!
        Me.Line38.Width = 2.272!
        Me.Line38.X1 = 5.447!
        Me.Line38.X2 = 7.719!
        Me.Line38.Y1 = 15.775!
        Me.Line38.Y2 = 15.775!
        '
        'Line19
        '
        Me.Line19.Height = 1.506999!
        Me.Line19.Left = 3.644!
        Me.Line19.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line19.LineWeight = 1.0!
        Me.Line19.Name = "Line19"
        Me.Line19.Top = 14.644!
        Me.Line19.Width = 0!
        Me.Line19.X1 = 3.644!
        Me.Line19.X2 = 3.644!
        Me.Line19.Y1 = 14.644!
        Me.Line19.Y2 = 16.151!
        '
        'Line16
        '
        Me.Line16.Height = 1.504999!
        Me.Line16.Left = 1.908001!
        Me.Line16.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line16.LineWeight = 1.0!
        Me.Line16.Name = "Line16"
        Me.Line16.Top = 14.646!
        Me.Line16.Width = 0!
        Me.Line16.X1 = 1.908001!
        Me.Line16.X2 = 1.908001!
        Me.Line16.Y1 = 14.646!
        Me.Line16.Y2 = 16.151!
        '
        'Line39
        '
        Me.Line39.Height = 0!
        Me.Line39.Left = 1.898!
        Me.Line39.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line39.LineWeight = 1.0!
        Me.Line39.Name = "Line39"
        Me.Line39.Top = 15.766!
        Me.Line39.Width = 2.513!
        Me.Line39.X1 = 1.898!
        Me.Line39.X2 = 4.411!
        Me.Line39.Y1 = 15.766!
        Me.Line39.Y2 = 15.766!
        '
        'Line17
        '
        Me.Line17.Height = 1.505!
        Me.Line17.Left = 2.654!
        Me.Line17.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line17.LineWeight = 1.0!
        Me.Line17.Name = "Line17"
        Me.Line17.Top = 14.647!
        Me.Line17.Width = 0!
        Me.Line17.X1 = 2.654!
        Me.Line17.X2 = 2.654!
        Me.Line17.Y1 = 14.647!
        Me.Line17.Y2 = 16.152!
        '
        'Line21
        '
        Me.Line21.Height = 0!
        Me.Line21.Left = 3.644!
        Me.Line21.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line21.LineWeight = 1.0!
        Me.Line21.Name = "Line21"
        Me.Line21.Top = 14.646!
        Me.Line21.Width = 0.7669997!
        Me.Line21.X1 = 3.644!
        Me.Line21.X2 = 4.411!
        Me.Line21.Y1 = 14.646!
        Me.Line21.Y2 = 14.646!
        '
        'Label1
        '
        Me.Label1.Height = 0.1875!
        Me.Label1.HyperLink = Nothing
        Me.Label1.Left = 0!
        Me.Label1.Name = "Label1"
        Me.Label1.Style = "font-size: 8.25pt; font-weight: bold; text-align: left; vertical-align: middle"
        Me.Label1.Text = "3.17 STABILITY BRACING AT BEAM-TO-COLUMN CONNECTIONS (AISC 314-16 SECTION E3.4.C(" &
    "B))"
        Me.Label1.Top = 12.169!
        Me.Label1.Width = 6.061999!
        '
        'Label4
        '
        Me.Label4.Height = 0.1875!
        Me.Label4.HyperLink = Nothing
        Me.Label4.Left = 2.25!
        Me.Label4.Name = "Label4"
        Me.Label4.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label4.Text = "Left Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label4.Top = 12.419!
        Me.Label4.Width = 1.0!
        '
        'Label6
        '
        Me.Label6.Height = 0.1875!
        Me.Label6.HyperLink = Nothing
        Me.Label6.Left = 3.250001!
        Me.Label6.Name = "Label6"
        Me.Label6.Style = "font-size: 8.25pt; font-weight: bold; text-align: center; vertical-align: middle"
        Me.Label6.Text = "Right Side" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label6.Top = 12.419!
        Me.Label6.Width = 1.0!
        '
        'Label8
        '
        Me.Label8.Height = 0.188!
        Me.Label8.HyperLink = Nothing
        Me.Label8.Left = 0!
        Me.Label8.Name = "Label8"
        Me.Label8.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label8.Text = "Link Size="
        Me.Label8.Top = 12.606!
        Me.Label8.Width = 2.25!
        '
        'S3161_001
        '
        Me.S3161_001.Height = 0.1875!
        Me.S3161_001.HyperLink = Nothing
        Me.S3161_001.Left = 2.263!
        Me.S3161_001.Name = "S3161_001"
        Me.S3161_001.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S3161_001.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_001.Top = 12.606!
        Me.S3161_001.Width = 1.0!
        '
        'Label10
        '
        Me.Label10.Height = 0.188!
        Me.Label10.HyperLink = Nothing
        Me.Label10.Left = 0!
        Me.Label10.Name = "Label10"
        Me.Label10.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label10.Text = "byield="
        Me.Label10.Top = 12.794!
        Me.Label10.Width = 2.25!
        '
        'S3161_002
        '
        Me.S3161_002.Height = 0.1875!
        Me.S3161_002.HyperLink = Nothing
        Me.S3161_002.Left = 2.25!
        Me.S3161_002.Name = "S3161_002"
        Me.S3161_002.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_002.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_002.Top = 12.794!
        Me.S3161_002.Width = 1.0!
        '
        'S3161_008
        '
        Me.S3161_008.Height = 0.1875!
        Me.S3161_008.HyperLink = Nothing
        Me.S3161_008.Left = 3.249!
        Me.S3161_008.Name = "S3161_008"
        Me.S3161_008.Style = "color: Black; font-size: 8.25pt; font-weight: normal; text-align: center; vertica" &
    "l-align: middle; ddo-char-set: 1"
        Me.S3161_008.Text = "0.9" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_008.Top = 12.606!
        Me.S3161_008.Width = 1.0!
        '
        'S3161_009
        '
        Me.S3161_009.Height = 0.1875!
        Me.S3161_009.HyperLink = Nothing
        Me.S3161_009.Left = 3.25!
        Me.S3161_009.Name = "S3161_009"
        Me.S3161_009.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_009.Text = "0.75" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_009.Top = 12.794!
        Me.S3161_009.Width = 1.0!
        '
        'Line41
        '
        Me.Line41.Height = 0!
        Me.Line41.Left = 2.25!
        Me.Line41.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line41.LineWeight = 1.0!
        Me.Line41.Name = "Line41"
        Me.Line41.Top = 12.794!
        Me.Line41.Width = 2.0!
        Me.Line41.X1 = 2.25!
        Me.Line41.X2 = 4.25!
        Me.Line41.Y1 = 12.794!
        Me.Line41.Y2 = 12.794!
        '
        'Line42
        '
        Me.Line42.Height = 0!
        Me.Line42.Left = 2.25!
        Me.Line42.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line42.LineWeight = 1.0!
        Me.Line42.Name = "Line42"
        Me.Line42.Top = 12.606!
        Me.Line42.Width = 2.0!
        Me.Line42.X1 = 2.25!
        Me.Line42.X2 = 4.25!
        Me.Line42.Y1 = 12.606!
        Me.Line42.Y2 = 12.606!
        '
        'Label17
        '
        Me.Label17.Height = 0.188!
        Me.Label17.HyperLink = Nothing
        Me.Label17.Left = 4.27!
        Me.Label17.Name = "Label17"
        Me.Label17.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label17.Text = "in"
        Me.Label17.Top = 12.982!
        Me.Label17.Width = 3.4!
        '
        'Label18
        '
        Me.Label18.Height = 0.188!
        Me.Label18.HyperLink = Nothing
        Me.Label18.Left = 0!
        Me.Label18.Name = "Label18"
        Me.Label18.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label18.Text = "tstem=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label18.Top = 12.982!
        Me.Label18.Width = 2.25!
        '
        'S3161_003
        '
        Me.S3161_003.Height = 0.1875!
        Me.S3161_003.HyperLink = Nothing
        Me.S3161_003.Left = 2.255!
        Me.S3161_003.Name = "S3161_003"
        Me.S3161_003.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_003.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_003.Top = 12.982!
        Me.S3161_003.Width = 1.0!
        '
        'S3161_010
        '
        Me.S3161_010.Height = 0.1875!
        Me.S3161_010.HyperLink = Nothing
        Me.S3161_010.Left = 3.255!
        Me.S3161_010.Name = "S3161_010"
        Me.S3161_010.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_010.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_010.Top = 12.982!
        Me.S3161_010.Width = 1.0!
        '
        'Label22
        '
        Me.Label22.Height = 0.188!
        Me.Label22.HyperLink = Nothing
        Me.Label22.Left = 4.27!
        Me.Label22.Name = "Label22"
        Me.Label22.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label22.Text = "ksi"
        Me.Label22.Top = 13.169!
        Me.Label22.Width = 3.4!
        '
        'Label23
        '
        Me.Label23.Height = 0.188!
        Me.Label23.HyperLink = Nothing
        Me.Label23.Left = 0!
        Me.Label23.Name = "Label23"
        Me.Label23.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label23.Text = "Fy_link=" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.Label23.Top = 13.169!
        Me.Label23.Width = 2.25!
        '
        'S3161_004
        '
        Me.S3161_004.Height = 0.1875!
        Me.S3161_004.HyperLink = Nothing
        Me.S3161_004.Left = 2.255!
        Me.S3161_004.Name = "S3161_004"
        Me.S3161_004.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_004.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_004.Top = 13.169!
        Me.S3161_004.Width = 1.0!
        '
        'S3161_011
        '
        Me.S3161_011.Height = 0.1875!
        Me.S3161_011.HyperLink = Nothing
        Me.S3161_011.Left = 3.255!
        Me.S3161_011.Name = "S3161_011"
        Me.S3161_011.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_011.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_011.Top = 13.169!
        Me.S3161_011.Width = 1.0!
        '
        'Label28
        '
        Me.Label28.Height = 0.188!
        Me.Label28.HyperLink = Nothing
        Me.Label28.Left = 4.27!
        Me.Label28.Name = "Label28"
        Me.Label28.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label28.Text = ""
        Me.Label28.Top = 13.356!
        Me.Label28.Width = 3.4!
        '
        'Label29
        '
        Me.Label29.Height = 0.188!
        Me.Label29.HyperLink = Nothing
        Me.Label29.Left = 0!
        Me.Label29.Name = "Label29"
        Me.Label29.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label29.Text = "Ry="
        Me.Label29.Top = 13.356!
        Me.Label29.Width = 2.25!
        '
        'S3161_005
        '
        Me.S3161_005.Height = 0.1875!
        Me.S3161_005.HyperLink = Nothing
        Me.S3161_005.Left = 2.250001!
        Me.S3161_005.Name = "S3161_005"
        Me.S3161_005.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_005.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_005.Top = 13.356!
        Me.S3161_005.Width = 1.0!
        '
        'S3161_012
        '
        Me.S3161_012.Height = 0.1875!
        Me.S3161_012.HyperLink = Nothing
        Me.S3161_012.Left = 3.25!
        Me.S3161_012.Name = "S3161_012"
        Me.S3161_012.Style = "color: DarkGreen; font-size: 8.25pt; font-weight: normal; text-align: center; ver" &
    "tical-align: middle; ddo-char-set: 1"
        Me.S3161_012.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_012.Top = 13.356!
        Me.S3161_012.Width = 1.0!
        '
        'Label32
        '
        Me.Label32.Height = 0.188!
        Me.Label32.HyperLink = Nothing
        Me.Label32.Left = 4.26!
        Me.Label32.Name = "Label32"
        Me.Label32.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label32.Text = "kips"
        Me.Label32.Top = 13.554!
        Me.Label32.Width = 3.4!
        '
        'Label33
        '
        Me.Label33.Height = 0.188!
        Me.Label33.HyperLink = Nothing
        Me.Label33.Left = 0!
        Me.Label33.Name = "Label33"
        Me.Label33.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label33.Text = "Pye_link="
        Me.Label33.Top = 13.544!
        Me.Label33.Width = 2.25!
        '
        'S3161_006
        '
        Me.S3161_006.Height = 0.1875!
        Me.S3161_006.HyperLink = Nothing
        Me.S3161_006.Left = 2.254997!
        Me.S3161_006.Name = "S3161_006"
        Me.S3161_006.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S3161_006.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_006.Top = 13.544!
        Me.S3161_006.Width = 1.0!
        '
        'S3161_013
        '
        Me.S3161_013.Height = 0.1875!
        Me.S3161_013.HyperLink = Nothing
        Me.S3161_013.Left = 3.254997!
        Me.S3161_013.Name = "S3161_013"
        Me.S3161_013.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S3161_013.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_013.Top = 13.544!
        Me.S3161_013.Width = 1.0!
        '
        'Label41
        '
        Me.Label41.Height = 0.188!
        Me.Label41.HyperLink = Nothing
        Me.Label41.Left = 4.27!
        Me.Label41.Name = "Label41"
        Me.Label41.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label41.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "Bracing Force (LRFD)"
        Me.Label41.Top = 13.732!
        Me.Label41.Width = 3.4!
        '
        'Label43
        '
        Me.Label43.Height = 0.188!
        Me.Label43.HyperLink = Nothing
        Me.Label43.Left = 0.01000001!
        Me.Label43.Name = "Label43"
        Me.Label43.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label43.Text = "0.02 * Pye_llink="
        Me.Label43.Top = 13.722!
        Me.Label43.Width = 2.25!
        '
        'S3161_007
        '
        Me.S3161_007.Height = 0.1875!
        Me.S3161_007.HyperLink = Nothing
        Me.S3161_007.Left = 2.25!
        Me.S3161_007.Name = "S3161_007"
        Me.S3161_007.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S3161_007.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_007.Top = 13.732!
        Me.S3161_007.Width = 1.0!
        '
        'S3161_014
        '
        Me.S3161_014.Height = 0.1875!
        Me.S3161_014.HyperLink = Nothing
        Me.S3161_014.Left = 3.25!
        Me.S3161_014.Name = "S3161_014"
        Me.S3161_014.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S3161_014.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_014.Top = 13.732!
        Me.S3161_014.Width = 1.0!
        '
        'Line43
        '
        Me.Line43.Height = 0!
        Me.Line43.Left = 2.256!
        Me.Line43.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line43.LineWeight = 1.0!
        Me.Line43.Name = "Line43"
        Me.Line43.Top = 12.982!
        Me.Line43.Width = 1.999998!
        Me.Line43.X1 = 2.256!
        Me.Line43.X2 = 4.255998!
        Me.Line43.Y1 = 12.982!
        Me.Line43.Y2 = 12.982!
        '
        'Line44
        '
        Me.Line44.Height = 0!
        Me.Line44.Left = 2.248!
        Me.Line44.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line44.LineWeight = 1.0!
        Me.Line44.Name = "Line44"
        Me.Line44.Top = 13.169!
        Me.Line44.Width = 1.999999!
        Me.Line44.X1 = 2.248!
        Me.Line44.X2 = 4.247999!
        Me.Line44.Y1 = 13.169!
        Me.Line44.Y2 = 13.169!
        '
        'Line45
        '
        Me.Line45.Height = 0!
        Me.Line45.Left = 2.265!
        Me.Line45.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line45.LineWeight = 1.0!
        Me.Line45.Name = "Line45"
        Me.Line45.Top = 13.356!
        Me.Line45.Width = 2.0!
        Me.Line45.X1 = 2.265!
        Me.Line45.X2 = 4.265!
        Me.Line45.Y1 = 13.356!
        Me.Line45.Y2 = 13.356!
        '
        'Line46
        '
        Me.Line46.Height = 0!
        Me.Line46.Left = 2.267!
        Me.Line46.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line46.LineWeight = 1.0!
        Me.Line46.Name = "Line46"
        Me.Line46.Top = 13.544!
        Me.Line46.Width = 2.0!
        Me.Line46.X1 = 2.267!
        Me.Line46.X2 = 4.267!
        Me.Line46.Y1 = 13.544!
        Me.Line46.Y2 = 13.544!
        '
        'Line47
        '
        Me.Line47.Height = 0!
        Me.Line47.Left = 2.258!
        Me.Line47.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line47.LineWeight = 1.0!
        Me.Line47.Name = "Line47"
        Me.Line47.Top = 13.732!
        Me.Line47.Width = 1.999999!
        Me.Line47.X1 = 2.258!
        Me.Line47.X2 = 4.257999!
        Me.Line47.Y1 = 13.732!
        Me.Line47.Y2 = 13.732!
        '
        'Line48
        '
        Me.Line48.Height = 0!
        Me.Line48.Left = 2.25!
        Me.Line48.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line48.LineWeight = 1.0!
        Me.Line48.Name = "Line48"
        Me.Line48.Top = 12.419!
        Me.Line48.Width = 2.0!
        Me.Line48.X1 = 2.25!
        Me.Line48.X2 = 4.25!
        Me.Line48.Y1 = 12.419!
        Me.Line48.Y2 = 12.419!
        '
        'Label47
        '
        Me.Label47.Height = 0.188!
        Me.Label47.HyperLink = Nothing
        Me.Label47.Left = 4.27!
        Me.Label47.Name = "Label47"
        Me.Label47.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label47.Text = "in"
        Me.Label47.Top = 12.794!
        Me.Label47.Width = 3.4!
        '
        'Line50
        '
        Me.Line50.Height = 1.689!
        Me.Line50.Left = 3.25!
        Me.Line50.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line50.LineWeight = 1.0!
        Me.Line50.Name = "Line50"
        Me.Line50.Top = 12.419!
        Me.Line50.Width = 0!
        Me.Line50.X1 = 3.25!
        Me.Line50.X2 = 3.25!
        Me.Line50.Y1 = 12.419!
        Me.Line50.Y2 = 14.108!
        '
        'Line52
        '
        Me.Line52.Height = 0!
        Me.Line52.Left = 2.26!
        Me.Line52.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line52.LineWeight = 1.0!
        Me.Line52.Name = "Line52"
        Me.Line52.Top = 13.908!
        Me.Line52.Width = 2.0!
        Me.Line52.X1 = 2.26!
        Me.Line52.X2 = 4.26!
        Me.Line52.Y1 = 13.908!
        Me.Line52.Y2 = 13.908!
        '
        'Label9
        '
        Me.Label9.Height = 0.188!
        Me.Label9.HyperLink = Nothing
        Me.Label9.Left = 4.271!
        Me.Label9.Name = "Label9"
        Me.Label9.Style = "font-size: 6.75pt; font-style: italic; vertical-align: middle; ddo-char-set: 1"
        Me.Label9.Text = "kips" & Global.Microsoft.VisualBasic.ChrW(9) & "=0.7*Bracing Force (LRFD)"
        Me.Label9.Top = 13.914!
        Me.Label9.Width = 3.4!
        '
        'Label13
        '
        Me.Label13.Height = 0.188!
        Me.Label13.HyperLink = Nothing
        Me.Label13.Left = 0.02099991!
        Me.Label13.Name = "Label13"
        Me.Label13.Style = "font-size: 8.25pt; text-align: right; vertical-align: middle"
        Me.Label13.Text = "Pbrace (ASD)="
        Me.Label13.Top = 13.904!
        Me.Label13.Width = 2.25!
        '
        'S3161_015
        '
        Me.S3161_015.Height = 0.1875!
        Me.S3161_015.HyperLink = Nothing
        Me.S3161_015.Left = 2.261!
        Me.S3161_015.Name = "S3161_015"
        Me.S3161_015.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S3161_015.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_015.Top = 13.914!
        Me.S3161_015.Width = 1.0!
        '
        'S3161_016
        '
        Me.S3161_016.Height = 0.1875!
        Me.S3161_016.HyperLink = Nothing
        Me.S3161_016.Left = 3.261!
        Me.S3161_016.Name = "S3161_016"
        Me.S3161_016.Style = "color: Blue; font-size: 8.25pt; font-weight: normal; text-align: center; vertical" &
    "-align: middle; ddo-char-set: 1"
        Me.S3161_016.Text = "509.93" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.S3161_016.Top = 13.914!
        Me.S3161_016.Width = 1.0!
        '
        'Line53
        '
        Me.Line53.Height = 0!
        Me.Line53.Left = 2.271!
        Me.Line53.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line53.LineWeight = 1.0!
        Me.Line53.Name = "Line53"
        Me.Line53.Top = 14.107!
        Me.Line53.Width = 2.0!
        Me.Line53.X1 = 2.271!
        Me.Line53.X2 = 4.271!
        Me.Line53.Y1 = 14.107!
        Me.Line53.Y2 = 14.107!
        '
        'Line49
        '
        Me.Line49.Height = 1.688999!
        Me.Line49.Left = 2.26!
        Me.Line49.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line49.LineWeight = 1.0!
        Me.Line49.Name = "Line49"
        Me.Line49.Top = 12.409!
        Me.Line49.Width = 0.000004053116!
        Me.Line49.X1 = 2.260004!
        Me.Line49.X2 = 2.26!
        Me.Line49.Y1 = 12.409!
        Me.Line49.Y2 = 14.098!
        '
        'Line51
        '
        Me.Line51.Height = 1.698999!
        Me.Line51.Left = 4.264!
        Me.Line51.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line51.LineWeight = 1.0!
        Me.Line51.Name = "Line51"
        Me.Line51.Top = 12.409!
        Me.Line51.Width = 0!
        Me.Line51.X1 = 4.264!
        Me.Line51.X2 = 4.264!
        Me.Line51.Y1 = 12.409!
        Me.Line51.Y2 = 14.108!
        '
        'Line23
        '
        Me.Line23.Height = 0!
        Me.Line23.Left = 1.916!
        Me.Line23.LineColor = System.Drawing.Color.FromArgb(CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.Line23.LineWeight = 1.0!
        Me.Line23.Name = "Line23"
        Me.Line23.Top = 15.962!
        Me.Line23.Width = 2.495!
        Me.Line23.X1 = 1.916!
        Me.Line23.X2 = 4.411!
        Me.Line23.Y1 = 15.962!
        Me.Line23.Y2 = 15.962!
        '
        'PageBreak1
        '
        Me.PageBreak1.Height = 0.1979167!
        Me.PageBreak1.Left = 0!
        Me.PageBreak1.Name = "PageBreak1"
        Me.PageBreak1.Size = New System.Drawing.SizeF(8.0!, 0.1979167!)
        Me.PageBreak1.Top = 5.8125!
        Me.PageBreak1.Width = 8.0!
        '
        'PageBreak2
        '
        Me.PageBreak2.Height = 0.01!
        Me.PageBreak2.Left = 0!
        Me.PageBreak2.Name = "PageBreak2"
        Me.PageBreak2.Size = New System.Drawing.SizeF(8.0!, 0.01!)
        Me.PageBreak2.Top = 14.229!
        Me.PageBreak2.Width = 8.0!
        '
        'US_CC5
        '
        Me.MasterReport = False
        Me.PageSettings.PaperHeight = 11.0!
        Me.PageSettings.PaperWidth = 8.5!
        Me.PrintWidth = 8.0!
        Me.Sections.Add(Me.Detail)
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" &
            "l; font-size: 10pt; color: Black; ddo-char-set: 204", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" &
            "lic", "Heading2", "Normal"))
        Me.StyleSheet.Add(New DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"))
        CType(Me.S319_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_037, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label750, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label599, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label598, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label600, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label603, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label605, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label609, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label610, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label613, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label614, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label617, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label618, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label621, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label622, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label625, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label626, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label629, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label630, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label633, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label634, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label637, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label638, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label641, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label642, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S313_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label602, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label666, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label645, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label646, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label647, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label648, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label650, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label652, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label653, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label655, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label656, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label658, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S314_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label662, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label663, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label664, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label667, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label668, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label675, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label676, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label678, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label679, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label681, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label682, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label684, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label685, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label687, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label691, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label692, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label693, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label694, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label695, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label697, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label701, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label702, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label705, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label706, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label709, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label710, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_023, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label713, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label714, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label717, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label718, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label721, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label722, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label725, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label726, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label729, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label730, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label733, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label734, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label737, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label738, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label739, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label742, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label743, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label746, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label747, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label751, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label754, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label755, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_034, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label758, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label759, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_035, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label762, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label763, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S317_036, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label767, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label768, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label769, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label770, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label771, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label773, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label777, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label778, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label781, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label782, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label785, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label786, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label789, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label790, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S318_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label793, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label794, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label796, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S315_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_017, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_018, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_019, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_020, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label27, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_021, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_022, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_031, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_030, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label38, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label39, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label40, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_024, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_025, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_034, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_035, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label46, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_026, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_036, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label49, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_027, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_037, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label52, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_032, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label54, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_028, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_038, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_033, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label58, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_029, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S319_039, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_001, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_002, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_008, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_009, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_003, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_010, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_004, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_011, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label28, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label29, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_005, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_012, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_006, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_013, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_007, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_014, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label47, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_015, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.S3161_016, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Private WithEvents S313_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape1 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label599 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label598 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label600 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label603 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label605 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line156 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line157 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line158 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label609 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label610 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label613 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label614 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label617 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label618 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label621 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label622 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label625 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label626 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label629 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label630 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label633 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label634 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label637 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label638 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label641 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label642 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S313_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line159 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line160 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line161 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line162 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line163 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line164 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line165 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line166 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line167 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line168 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line169 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line171 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line172 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label602 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line170 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label666 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label645 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label646 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label647 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label648 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label650 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label652 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label653 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label655 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label656 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label658 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label662 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label663 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line173 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line174 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line178 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line179 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label664 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label667 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label668 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S314_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line180 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line181 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line175 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line176 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line177 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label675 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label676 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label678 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label679 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label681 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label682 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label684 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label685 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label687 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label691 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label692 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_037 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape5 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label693 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label694 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label695 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label697 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line183 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line184 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label701 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label702 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label705 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label706 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label709 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label710 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label713 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label714 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label717 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label718 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label721 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label722 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label725 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label726 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label729 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label730 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label733 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label734 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line186 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line187 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line188 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line189 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line190 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line191 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line192 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line193 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line194 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line198 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label737 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label738 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label739 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label742 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label743 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label746 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label747 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label750 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label751 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label754 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label755 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label758 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label759 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_035 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label762 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label763 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S317_036 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line199 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line200 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line201 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line202 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line203 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line204 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line205 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line196 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line195 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line197 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label767 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line206 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S318_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape6 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label768 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label769 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label770 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label771 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label773 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line208 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line209 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line210 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label777 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label778 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label781 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label782 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label786 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label789 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label790 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S318_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line211 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line212 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line213 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line214 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label793 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line215 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line216 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line217 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line218 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label794 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line219 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label796 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S315_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line1 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line2 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line3 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line4 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line5 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line6 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line7 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line8 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label785 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_023 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Shape2 As GrapeCity.ActiveReports.SectionReportModel.Shape
    Private WithEvents Label2 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label3 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label5 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label7 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line9 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line10 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line11 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label12 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_017 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label16 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label20 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_019 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label24 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_020 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line12 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line13 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line14 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line15 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line16 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line17 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line18 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line20 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S319_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line19 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label11 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label27 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line21 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line22 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S319_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_021 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line23 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S319_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_022 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line24 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label34 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label35 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_031 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_030 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label38 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label39 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label40 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_024 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label42 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_025 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_034 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_035 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line25 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line26 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line27 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label46 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_026 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_036 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label49 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_027 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_037 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line28 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line29 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label52 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line34 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line35 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S319_032 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label54 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_028 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_038 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_033 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label58 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_029 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S319_039 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line30 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line31 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line32 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line33 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line36 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line37 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line38 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line39 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents S319_018 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label1 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label4 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label6 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label8 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_001 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label10 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_002 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_008 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_009 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line41 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line42 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label17 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label18 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_003 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_010 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label22 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label23 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_004 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_011 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label28 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label29 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_005 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_012 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label32 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label33 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_006 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_013 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label41 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label43 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_007 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_014 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line43 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line44 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line45 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line46 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line47 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line48 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label47 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line49 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line50 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line51 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Line52 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents Label9 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Label13 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_015 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents S3161_016 As GrapeCity.ActiveReports.SectionReportModel.Label
    Private WithEvents Line53 As GrapeCity.ActiveReports.SectionReportModel.Line
    Private WithEvents PageBreak1 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
    Private WithEvents PageBreak2 As GrapeCity.ActiveReports.SectionReportModel.PageBreak
End Class
