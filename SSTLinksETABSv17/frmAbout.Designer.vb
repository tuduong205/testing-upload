﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAbout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAbout))
        Me.btnOK = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LbLegend = New System.Windows.Forms.RichTextBox()
        Me.LbYL = New System.Windows.Forms.LinkLabel()
        Me.LbSupport = New System.Windows.Forms.Label()
        Me.LbWeb = New System.Windows.Forms.Label()
        Me.LbSST = New System.Windows.Forms.LinkLabel()
        Me.LbReaseDate = New System.Windows.Forms.Label()
        Me.LbVersion = New System.Windows.Forms.Label()
        Me.LbName = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.Location = New System.Drawing.Point(512, 366)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(80, 26)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.Panel1.Controls.Add(Me.LbLegend)
        Me.Panel1.Controls.Add(Me.LbYL)
        Me.Panel1.Controls.Add(Me.LbSupport)
        Me.Panel1.Controls.Add(Me.LbWeb)
        Me.Panel1.Controls.Add(Me.LbSST)
        Me.Panel1.Controls.Add(Me.LbReaseDate)
        Me.Panel1.Controls.Add(Me.LbVersion)
        Me.Panel1.Controls.Add(Me.LbName)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(600, 400)
        Me.Panel1.TabIndex = 10
        '
        'LbLegend
        '
        Me.LbLegend.BackColor = System.Drawing.Color.White
        Me.LbLegend.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.LbLegend.Location = New System.Drawing.Point(15, 197)
        Me.LbLegend.Name = "LbLegend"
        Me.LbLegend.ReadOnly = True
        Me.LbLegend.Size = New System.Drawing.Size(569, 118)
        Me.LbLegend.TabIndex = 9
        Me.LbLegend.Text = resources.GetString("LbLegend.Text")
        '
        'LbYL
        '
        Me.LbYL.AutoSize = True
        Me.LbYL.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbYL.Location = New System.Drawing.Point(76, 158)
        Me.LbYL.Name = "LbYL"
        Me.LbYL.Size = New System.Drawing.Size(173, 18)
        Me.LbYL.TabIndex = 7
        Me.LbYL.TabStop = True
        Me.LbYL.Text = "yieldlink@strongtie.com"
        '
        'LbSupport
        '
        Me.LbSupport.AutoSize = True
        Me.LbSupport.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbSupport.Location = New System.Drawing.Point(12, 159)
        Me.LbSupport.Name = "LbSupport"
        Me.LbSupport.Size = New System.Drawing.Size(67, 18)
        Me.LbSupport.TabIndex = 5
        Me.LbSupport.Text = "Support:"
        '
        'LbWeb
        '
        Me.LbWeb.AutoSize = True
        Me.LbWeb.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbWeb.Location = New System.Drawing.Point(12, 113)
        Me.LbWeb.Name = "LbWeb"
        Me.LbWeb.Size = New System.Drawing.Size(192, 18)
        Me.LbWeb.TabIndex = 4
        Me.LbWeb.Text = "For more information, visit:"
        '
        'LbSST
        '
        Me.LbSST.AutoSize = True
        Me.LbSST.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbSST.Location = New System.Drawing.Point(13, 131)
        Me.LbSST.Name = "LbSST"
        Me.LbSST.Size = New System.Drawing.Size(412, 18)
        Me.LbSST.TabIndex = 3
        Me.LbSST.TabStop = True
        Me.LbSST.Text = "https://www.strongtie.com/solutions/mid-rise-steel/yield-link"
        '
        'LbReaseDate
        '
        Me.LbReaseDate.AutoSize = True
        Me.LbReaseDate.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbReaseDate.Location = New System.Drawing.Point(13, 76)
        Me.LbReaseDate.Name = "LbReaseDate"
        Me.LbReaseDate.Size = New System.Drawing.Size(189, 18)
        Me.LbReaseDate.TabIndex = 2
        Me.LbReaseDate.Text = "Release date: 08/15/2018"
        '
        'LbVersion
        '
        Me.LbVersion.AutoSize = True
        Me.LbVersion.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbVersion.Location = New System.Drawing.Point(12, 47)
        Me.LbVersion.Name = "LbVersion"
        Me.LbVersion.Size = New System.Drawing.Size(105, 18)
        Me.LbVersion.TabIndex = 2
        Me.LbVersion.Text = "Version: 1.0.0"
        '
        'LbName
        '
        Me.LbName.AutoSize = True
        Me.LbName.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LbName.Location = New System.Drawing.Point(12, 9)
        Me.LbName.Name = "LbName"
        Me.LbName.Size = New System.Drawing.Size(228, 22)
        Me.LbName.TabIndex = 1
        Me.LbName.Text = "Yield-Link® Connection"
        '
        'frmAbout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.ClientSize = New System.Drawing.Size(600, 400)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmAbout"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Yield-Link® Connection"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents LbName As System.Windows.Forms.Label
    Friend WithEvents LbVersion As System.Windows.Forms.Label
    Friend WithEvents LbSST As System.Windows.Forms.LinkLabel
    Friend WithEvents LbWeb As System.Windows.Forms.Label
    Friend WithEvents LbSupport As System.Windows.Forms.Label
    Friend WithEvents LbYL As System.Windows.Forms.LinkLabel
    Friend WithEvents LbReaseDate As System.Windows.Forms.Label
    Friend WithEvents LbLegend As System.Windows.Forms.RichTextBox
    Public WithEvents Panel1 As Windows.Forms.Panel
End Class
