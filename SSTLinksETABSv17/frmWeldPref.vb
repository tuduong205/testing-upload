﻿Imports System.Drawing
Imports YieldLinkLib

Public Class frmWeldPref
    Public curJob As clsMRSJob

    Sub New(_curJob As clsMRSJob)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        curJob = _curJob
        cbDbl2Flg.SelectedIndex = IIf(curJob.WeldingInfo.DoublerPL2ColFLG = WeldingOption.Option_1, 0, 1)
        cbDbl2Web.SelectedIndex = curJob.WeldingInfo.DoublerPL2ColWeb
        cbPlugWeld.SelectedIndex = IIf(curJob.WeldingInfo.UsePlug_weld, 1, 0)
    End Sub
    Private Sub cbDbl2Web_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDbl2Web.SelectedIndexChanged, cbPlugWeld.SelectedIndexChanged
        If cbDbl2Web.SelectedIndex = 0 Then
            lbDes1.Text = "Doubler without continuity plates"
            '
            If cbPlugWeld.SelectedIndex = 0 Then
                picW4.Image = New Bitmap(My.Resources.O1_No)
            Else
                picW4.Image = New Bitmap(My.Resources.O1_Yes)
            End If
        ElseIf cbDbl2Web.SelectedIndex = 1 Then
            lbDes1.Text = "Doubler extended beyond continuity plates"
            '
            If cbPlugWeld.SelectedIndex = 0 Then
                picW4.Image = New Bitmap(My.Resources.O2A_No)
            Else
                picW4.Image = New Bitmap(My.Resources.O2A_Yes)
            End If
        Else
            lbDes1.Text = "Doubler placed between continuity plates"
            '
            If cbPlugWeld.SelectedIndex = 0 Then
                picW4.Image = New Bitmap(My.Resources.O2B_No)
            Else
                picW4.Image = New Bitmap(My.Resources.O2B_Yes)
            End If
        End If
    End Sub
    Private Sub cbDbl2Flg_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbDbl2Flg.SelectedIndexChanged
        If cbDbl2Flg.SelectedIndex = 1 Then
            lbDes2.Text = "Fillet Weld"
        Else
            lbDes2.Text = "CJP Weld"
        End If
    End Sub
    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        curJob = Nothing
        Me.Close()
    End Sub
    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        curJob.WeldingInfo.DoublerPL2ColWeb = cbDbl2Web.SelectedIndex
        curJob.WeldingInfo.DoublerPL2ColFLG = IIf(cbDbl2Flg.SelectedIndex = 0, WeldingOption.Option_1, WeldingOption.Option_2)
        curJob.WeldingInfo.UsePlug_weld = (cbPlugWeld.SelectedIndex > 0)
        Me.Close()
    End Sub


End Class