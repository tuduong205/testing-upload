﻿Imports YieldLinkLib
Imports System.Windows.Forms
Imports System.Drawing

Public Class frmDesignInfo
    Public curJob As clsMRSJob
    Private frmTyp As frmType
    Private isUserEdit As Boolean
    Private strBase As String = "Bot. of Base PL to T.O. "
    Private lstSs As New List(Of String)

    Sub New(_curJob As clsMRSJob, Optional _frmTyp As frmType = frmType.DCRLimit)
        ' This call is required by the designer.
        InitializeComponent()
        '
        curJob = _curJob
        frmTyp = _frmTyp

        ' Add any initialization after the InitializeComponent() call.
        Dim cnt As Integer
        If _frmTyp = frmType.DCRLimit Then
#Region "DCRLimit"
            Me.Text = "Show/Modify DCR Limits"
            dgvInfo1.Columns(2).HeaderText = "Allowable DCR"
            grName.Text = "Strength Limits"
            lbtabDetails.Visible = True
            '
            cbSeismicDriftLimit.Items.Clear()
            cbSeismicDriftLimit.Items.AddRange({"0.010Hx", "0.015Hx", "0.020Hx", "0.025Hx"})
            cbSeismicDriftLimit.SelectedIndex = curJob.DCRLimits.DriftLimitSelect
            cbWindDriftLimit.SelectedIndex = curJob.DCRLimits.WindDriftLimitSelect
            ckDivideByRho.Checked = curJob.DCRLimits.DivideByRho
            '
            cnt = 0
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Initial tbf check=", NumberFormat(curJob.DCRLimits.tbf_check, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Initial bf check=", NumberFormat(curJob.DCRLimits.bf_check, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Initial Lyield check=", NumberFormat(curJob.DCRLimits.Lyield_check, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Panel Zone DCR=", NumberFormat(curJob.DCRLimits.Panel_Zone_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Drift DCR=", NumberFormat(curJob.DCRLimits.Drift_check, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt = 0
            cnt += 1 : dgvInfo2.Rows.Add(cnt, "Beam tf DCR=", NumberFormat(curJob.DCRLimits.Beam_tf_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo2.Rows.Add(cnt, "Link strength DCR=", NumberFormat(curJob.DCRLimits.Link_strength_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo2.Rows.Add(cnt, "Lyield check=", NumberFormat(curJob.DCRLimits.Lyield_check, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            dgvInfo2(2, cnt - 1).Style.BackColor = System.Drawing.SystemColors.Control
            dgvInfo2(2, cnt - 1).Style.SelectionBackColor = System.Drawing.SystemColors.Control
            dgvInfo2(2, cnt - 1).ReadOnly = True
            cnt += 1 : dgvInfo2.Rows.Add(cnt, "t_BRP DCR=", NumberFormat(curJob.DCRLimits.t_BRP_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo2.Rows.Add(cnt, "BRP Bolt DCR=", NumberFormat(curJob.DCRLimits.BRP_Bolt_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt = 0
            cnt += 1 : dgvInfo3.Rows.Add(cnt, "SCWB DCR=", NumberFormat(curJob.DCRLimits.SCWB_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo3.Rows.Add(cnt, "Panel Zone DCR=", NumberFormat(curJob.DCRLimits.Panel_Zone_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            dgvInfo3(2, cnt - 1).Style.BackColor = System.Drawing.SystemColors.Control
            dgvInfo3(2, cnt - 1).Style.SelectionBackColor = System.Drawing.SystemColors.Control
            dgvInfo3(2, cnt - 1).ReadOnly = True
            cnt += 1 : dgvInfo3.Rows.Add(cnt, "Column Flange DCR=", NumberFormat(curJob.DCRLimits.Column_Flange_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo3.Rows.Add(cnt, "Stiffener DCR=", NumberFormat(curJob.DCRLimits.Stiffener_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            '
            cnt = 0
            cnt += 1 : dgvInfo4.Rows.Add(cnt, "Beam Web DCR=", NumberFormat(curJob.DCRLimits.Beam_Web_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo4.Rows.Add(cnt, "Shear Plate DCR=", NumberFormat(curJob.DCRLimits.Shear_Plate_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo4.Rows.Add(cnt, "Bolt DCR=", NumberFormat(curJob.DCRLimits.Bolt_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
            cnt += 1 : dgvInfo4.Rows.Add(cnt, "Fillet Weld DCR=", NumberFormat(curJob.DCRLimits.Fillet_weld_DCR, -2), NumberFormat(curJob.DCRLimits.Max, 2), NumberFormat(curJob.DCRLimits.Min, 2))
#End Region
        ElseIf frmTyp = frmType.JobInfo Then
#Region "JobInfo"
            dgvInfo1.Columns(dgvInfo1.ColumnCount - 1).Visible = False
            dgvInfo1.Columns(dgvInfo1.ColumnCount - 2).Visible = False
            SplitContainer1.Panel1Collapsed = True
            '
            tabInfo.Appearance = TabAppearance.FlatButtons
            tabInfo.ItemSize = New Drawing.Size(0, 1)
            '
            Me.Text = "Show/Modify Project Information"
            grName.Text = "Project Information"
            '
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Job ID", curJob.JobID)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Job Name", curJob.Name)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Job Address", curJob.Address)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "City, State", curJob.CityState)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Design Firm Name", curJob.EOR)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Design By", curJob.EngName)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Email Address", curJob.EngEmail)
            '
            dgvInfo1.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
            dgvInfo1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
#End Region
        ElseIf frmTyp = frmType.SlabDepths Then
#Region "SlabDepths"
            dgvInfo1.Columns(dgvInfo1.ColumnCount - 1).Visible = False
            dgvInfo1.Columns(dgvInfo1.ColumnCount - 2).Visible = False

            SplitContainer1.Panel1Collapsed = True
            '
            tabInfo.Appearance = TabAppearance.FlatButtons
            tabInfo.ItemSize = New Drawing.Size(0, 1)
            '
            Me.Text = "Show/Modify Slab Information"
            grName.Text = "Slab Thickness " & "(inches)"
            '
            For i As Integer = 0 To curJob.SlabDepths.Count - 1
                If i = curJob.SlabDepths.Count - 1 Then
                    cnt += 1 : dgvInfo1.Rows.Add(cnt, "Bot. of Base PL to T.O. " & curJob.SlabDepths(i).StoryID, curJob.SlabDepths(i).SlabDepth)
                Else
                    cnt += 1 : dgvInfo1.Rows.Add(cnt, curJob.SlabDepths(i).StoryID, curJob.SlabDepths(i).SlabDepth)
                End If
            Next
            '
            ckFixedBase.Visible = True
            ckFixedBase.Checked = curJob.isFixedBase
            '
            dgvInfo1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells
#End Region
        ElseIf frmTyp = frmType.Material Then
#Region "Material"
            Dim tmpCell As DataGridViewComboBoxCell

            dgvInfo1.Columns(dgvInfo1.ColumnCount - 1).Visible = False
            dgvInfo1.Columns(dgvInfo1.ColumnCount - 2).Visible = False
            SplitContainer1.Panel1Collapsed = True
            '
            tabInfo.Appearance = TabAppearance.FlatButtons
            tabInfo.ItemSize = New Drawing.Size(0, 1)
            Me.Text = "Material Properties"
            grName.Text = "Material Properties"
            '
            dgvInfo1.Columns(2).HeaderText = "Value"
            cnt = 0
            dgvInfo1.Rows.Add("", "Shear Plate", "", 999, -999)
            With dgvInfo1.Rows(dgvInfo1.RowCount - 1)
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .DefaultCellStyle.BackColor = Drawing.Color.White
                .DefaultCellStyle.ForeColor = Drawing.Color.DarkRed
                .DefaultCellStyle.SelectionBackColor = Drawing.Color.White
                .DefaultCellStyle.SelectionForeColor = Drawing.Color.DarkRed
            End With

            'ShearPlate_Fy
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fy (ksi)=", "", 999, -999)
            tmpCell = New DataGridViewComboBoxCell()
            tmpCell.Items.AddRange("36", "50", "55")
            tmpCell.Value = curJob.MaterialInfo.ShearPlate_Fy.ToString
            tmpCell.Style.ForeColor = Drawing.Color.Blue
            tmpCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            dgvInfo1.Item(2, dgvInfo1.RowCount - 1) = tmpCell
            tmpCell = Nothing
            'ShearPlate_Fu
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fu (ksi)=", "", 999, -999)
            With dgvInfo1.Item(2, dgvInfo1.RowCount - 1)
                .ReadOnly = True
                .Value = curJob.MaterialInfo.ShearPlate_Fu.ToString
                .Style.BackColor = SystemColors.Control
                .Style.SelectionBackColor = SystemColors.Control
                .Style.ForeColor = Color.DarkRed
                .Style.SelectionForeColor = Color.DarkRed
            End With
            'StiffPL_Fy
            cnt = 0
            dgvInfo1.Rows.Add("", "Stiffener Plate", "", 999, -999)
            With dgvInfo1.Rows(dgvInfo1.RowCount - 1)
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .DefaultCellStyle.BackColor = Drawing.Color.White
                .DefaultCellStyle.ForeColor = Drawing.Color.DarkRed
                .DefaultCellStyle.SelectionBackColor = Drawing.Color.White
                .DefaultCellStyle.SelectionForeColor = Drawing.Color.DarkRed
            End With
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fy (ksi)=", "", 999, -999)
            tmpCell = New DataGridViewComboBoxCell()
            tmpCell.Items.AddRange("36", "50", "55")
            tmpCell.Value = curJob.MaterialInfo.StiffPL_Fy.ToString
            tmpCell.Style.ForeColor = Drawing.Color.Blue
            tmpCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            dgvInfo1.Item(2, dgvInfo1.RowCount - 1) = tmpCell
            tmpCell = Nothing
            'StiffPL_Fu
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fu (ksi)" & "=", curJob.MaterialInfo.ShearPlate_Fu, 999, -999)
            With dgvInfo1.Item(2, dgvInfo1.RowCount - 1)
                .ReadOnly = True
                .Value = curJob.MaterialInfo.StiffPL_Fu.ToString
                .Style.BackColor = SystemColors.Control
                .Style.SelectionBackColor = SystemColors.Control
                .Style.ForeColor = Color.DarkRed
                .Style.SelectionForeColor = Color.DarkRed
            End With

            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Min. Thickness, t_STP " & ("(in)") & "=", NumberFormat(curJob.MaterialInfo.StiffPL_DefThk, -2), 0, 3)
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Stiff. Depth (1-sided Connection)=", "", 999, -999)

            tmpCell = New DataGridViewComboBoxCell()
            tmpCell.Items.AddRange(StiffStyle.Partial_Depth.ToString, StiffStyle.Full_Depth.ToString)
            tmpCell.Value = curJob.MaterialInfo.StiffPL_1SidedConn.ToString
            tmpCell.Style.ForeColor = Drawing.Color.Blue
            tmpCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            dgvInfo1.Item(2, dgvInfo1.RowCount - 1) = tmpCell
            tmpCell = Nothing

            '
            cnt = 0
            dgvInfo1.Rows.Add("", "Doubler Plate", "", 999, -999)
            With dgvInfo1.Rows(dgvInfo1.RowCount - 1)
                .ReadOnly = True
                .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
                .DefaultCellStyle.BackColor = Drawing.Color.White
                .DefaultCellStyle.ForeColor = Drawing.Color.DarkRed
                .DefaultCellStyle.SelectionBackColor = Drawing.Color.White
                .DefaultCellStyle.SelectionForeColor = Drawing.Color.DarkRed
            End With
            'DoublerPL_Fy
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fy (ksi)=", "", 999, -999)
            tmpCell = New DataGridViewComboBoxCell()
            tmpCell.Items.AddRange("36", "50", "55")
            tmpCell.Value = curJob.MaterialInfo.DoublerPL_Fy.ToString
            tmpCell.Style.ForeColor = Drawing.Color.Blue
            tmpCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            dgvInfo1.Item(2, dgvInfo1.RowCount - 1) = tmpCell
            tmpCell = Nothing
            'DoublerPL_Fu
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fu (ksi)" & "=", curJob.MaterialInfo.DoublerPL_Fu, 999, -999)
            With dgvInfo1.Item(2, dgvInfo1.RowCount - 1)
                .ReadOnly = True
                .Value = curJob.MaterialInfo.DoublerPL_Fu.ToString
                .Style.BackColor = SystemColors.Control
                .Style.SelectionBackColor = SystemColors.Control
                .Style.ForeColor = Color.DarkRed
                .Style.SelectionForeColor = Color.DarkRed
            End With
            cnt += 1 : dgvInfo1.Rows.Add(cnt, "Min. Thickness, t_DP (in)=", NumberFormat(curJob.MaterialInfo.DoublerPL_DefThk, -2), 0, 3)
            ''
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fy " & "(ksi)" & "=", NumberFormat(curJob.MaterialInfo.ShearPlate_Fy, -2), 999, -999)
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fu " & "(ksi)" & "=", NumberFormat(curJob.MaterialInfo.ShearPlate_Fu, -2), 999, -999)
            ''cnt += 1 : dgvInfo1.Rows.Add(cnt, "Min. Thickness, t_SP (in)=", NumberFormat(curJob.MaterialInfo.ShearPlate_DefThk, -2), 0, 3)
            ''
            'cnt = 0
            'dgvInfo1.Rows.Add("", "Stiffener Plate", "", 999, -999)
            'With dgvInfo1.Rows(dgvInfo1.RowCount - 1)
            '    .ReadOnly = True
            '    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            '    .DefaultCellStyle.BackColor = Drawing.Color.White
            '    .DefaultCellStyle.ForeColor = Drawing.Color.DarkRed
            '    .DefaultCellStyle.SelectionBackColor = Drawing.Color.White
            '    .DefaultCellStyle.SelectionForeColor = Drawing.Color.DarkRed
            'End With
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fy " & ("(ksi)") & "=", NumberFormat(curJob.MaterialInfo.StiffPL_Fy, -2), 999, -999)
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fu " & ("(ksi)") & "=", NumberFormat(curJob.MaterialInfo.StiffPL_Fu, -2), 999, -999)
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Min. Thickness, t_STP " & ("(in)") & "=", NumberFormat(curJob.MaterialInfo.StiffPL_DefThk, -2), 0, 3)
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Stiff. Depth (1-sided Connection)=", "", 999, -999)
            'Dim tmpCell = New DataGridViewComboBoxCell()
            'tmpCell.Items.AddRange(StiffStyle.Partial_Depth.ToString, StiffStyle.Full_Depth.ToString)
            'tmpCell.Value = curJob.MaterialInfo.StiffPL_1SidedConn.ToString
            'tmpCell.Style.ForeColor = Drawing.Color.Blue
            'tmpCell.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
            'dgvInfo1.Item(2, dgvInfo1.RowCount - 1) = tmpCell
            'tmpCell = Nothing
            ''
            'cnt = 0
            'dgvInfo1.Rows.Add("", "Doubler Plate", "", 999, -999)
            'With dgvInfo1.Rows(dgvInfo1.RowCount - 1)
            '    .ReadOnly = True
            '    .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            '    .DefaultCellStyle.BackColor = Drawing.Color.White
            '    .DefaultCellStyle.ForeColor = Drawing.Color.DarkRed
            '    .DefaultCellStyle.SelectionBackColor = Drawing.Color.White
            '    .DefaultCellStyle.SelectionForeColor = Drawing.Color.DarkRed
            'End With
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fy " & ("(ksi)") & "=", NumberFormat(curJob.MaterialInfo.DoublerPL_Fy, -2), 999, -999)
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Fu " & ("(ksi)") & "=", NumberFormat(curJob.MaterialInfo.DoublerPL_Fu, -2), 999, -999)
            'cnt += 1 : dgvInfo1.Rows.Add(cnt, "Min. Thickness, t_DP " & ("(in)") & "=", NumberFormat(curJob.MaterialInfo.DoublerPL_DefThk, -2), 0, 3)
#End Region
        End If
        '
        isUserEdit = True
        '
        DataChange(1)
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim valRow As Integer = -1
        Dim valCol As Integer = 2
        'Dim SSTPlugin As New SSTLinksPlugin
        If frmTyp = frmType.DCRLimit Then
            curJob.DCRLimits.DriftLimitSelect = cbSeismicDriftLimit.SelectedIndex
            curJob.DCRLimits.WindDriftLimitSelect = cbWindDriftLimit.SelectedIndex
            curJob.DCRLimits.DivideByRho = ckDivideByRho.Checked
            '
            valRow = -1 ' Initial Link Design
            valRow += 1 : curJob.DCRLimits.tbf_check = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.bf_check = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Lyield_check = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Panel_Zone_DCR = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Drift_check = dgvInfo1(valCol, valRow).Value
            valRow = -1 'Beam & Link check
            valRow += 1 : curJob.DCRLimits.Beam_tf_DCR = dgvInfo2(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Link_strength_DCR = dgvInfo2(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Lyield_check = dgvInfo2(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.t_BRP_DCR = dgvInfo2(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.BRP_Bolt_DCR = dgvInfo2(valCol, valRow).Value
            valRow = -1 'Column 
            valRow += 1 : curJob.DCRLimits.SCWB_DCR = dgvInfo3(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Panel_Zone_DCR = dgvInfo3(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Column_Flange_DCR = dgvInfo3(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Stiffener_DCR = dgvInfo3(valCol, valRow).Value
            valRow = -1 'Shear tab check
            valRow += 1 : curJob.DCRLimits.Beam_Web_DCR = dgvInfo4(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Shear_Plate_DCR = dgvInfo4(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Bolt_DCR = dgvInfo4(valCol, valRow).Value
            valRow += 1 : curJob.DCRLimits.Fillet_weld_DCR = dgvInfo4(valCol, valRow).Value
        ElseIf frmTyp = frmType.JobInfo Then
            valRow += 1 : curJob.JobID = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.Name = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.Address = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.CityState = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.EOR = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.EngName = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.EngEmail = dgvInfo1(valCol, valRow).Value
        ElseIf frmTyp = frmType.SlabDepths Then
            Dim curDepth As Double
            For i As Integer = 0 To dgvInfo1.RowCount - 1
                Double.TryParse(dgvInfo1(2, i).Value, curDepth)
                curJob.SlabDepths(i).SlabDepth = curDepth
            Next
            '
            curJob.isFixedBase = ckFixedBase.Checked
        ElseIf frmTyp = frmType.Material Then
            valRow += 1
            valRow += 1 : curJob.MaterialInfo.ShearPlate_Fy = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.MaterialInfo.ShearPlate_Fu = dgvInfo1(valCol, valRow).Value
            'valRow += 1 : curJob.MaterialInfo.ShearPlate_DefThk = dgvInfo1(valCol, valRow).Value
            '
            valRow += 1
            valRow += 1 : curJob.MaterialInfo.StiffPL_Fy = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.MaterialInfo.StiffPL_Fu = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.MaterialInfo.StiffPL_DefThk = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.MaterialInfo.StiffPL_1SidedConn = IIf(String.Equals(dgvInfo1(valCol, valRow).Value, StiffStyle.Full_Depth.ToString), StiffStyle.Full_Depth, StiffStyle.Partial_Depth)
            '
            valRow += 1
            valRow += 1 : curJob.MaterialInfo.DoublerPL_Fy = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.MaterialInfo.DoublerPL_Fu = dgvInfo1(valCol, valRow).Value
            valRow += 1 : curJob.MaterialInfo.DoublerPL_DefThk = dgvInfo1(valCol, valRow).Value
        End If
        '
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        curJob = Nothing
        Me.Close()
    End Sub

    Private Sub frmInfo_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Escape Then
            curJob = Nothing
            Me.Close()
        End If
    End Sub

    'Private Sub cbLimit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbSeismicDriftLimit.SelectedIndexChanged
    '    If cbSeismicDriftLimit.Text = "Other" Then
    '        Panel1.Visible = True
    '        cbSeismicDriftLimit.Focus()
    '    End If
    'End Sub

    Private Sub dgvStepAll_CurrentCellDirtyStateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dgvInfo1.CurrentCellDirtyStateChanged, dgvInfo2.CurrentCellDirtyStateChanged
        If sender.IsCurrentCellDirty Then sender.CommitEdit(DataGridViewDataErrorContexts.Commit)
    End Sub

    Private Sub cbOther_KeyPress(sender As Object, e As Windows.Forms.KeyPressEventArgs)
        If ModifierKeys <> Windows.Forms.Keys.Control Then
            Dim ValidChar As String = "0123456789." + Convert.ToChar(8).ToString()
            If Not ValidChar.Contains(e.KeyChar) Then
                e.Handled = True
            End If
        ElseIf e.KeyChar = Chr(22) Then
            e.Handled = Not IsNumeric(Clipboard.GetText)
        End If
    End Sub

    Private Sub dgvInfo_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvInfo1.EditingControlShowing,
        dgvInfo2.EditingControlShowing, dgvInfo3.EditingControlShowing, dgvInfo4.EditingControlShowing
        If frmTyp <> frmType.JobInfo Then
            Try
                AddHandler CType(e.Control, TextBox).KeyPress, AddressOf TextBox_keyPress
            Catch ex As Exception
            End Try
        End If
    End Sub
    Private Sub TextBox_keyPress(ByVal sender As Object, ByVal e As Windows.Forms.KeyPressEventArgs)
        If ModifierKeys <> Windows.Forms.Keys.Control Then
            Dim ValidChar As String = "0123456789." + Convert.ToChar(8).ToString()
            If Not ValidChar.Contains(e.KeyChar) Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub dgvInfo1_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvInfo1.CellValueChanged
        If isUserEdit = False Or e.ColumnIndex <> 2 Then Return
        '
        DataChange(e.RowIndex)
    End Sub

    Private Sub DataChange(rowIndex As Integer)
        If frmTyp = frmType.DCRLimit Then
            Dim val, minVal, maxVal As Double
            Double.TryParse(dgvInfo1(2, rowIndex).Value, val)
            Double.TryParse(dgvInfo1(3, rowIndex).Value, maxVal)
            Double.TryParse(dgvInfo1(4, rowIndex).Value, minVal)
            '
            If (val <= maxVal AndAlso val >= minVal) Then
                btnOK.Enabled = True
                dgvInfo1(2, rowIndex).Style.BackColor = Nothing
            Else
                btnOK.Enabled = False
                dgvInfo1(2, rowIndex).Style.BackColor = Drawing.Color.Yellow
            End If
            '
            If rowIndex = 2 Then
                dgvInfo2(2, rowIndex).Value = dgvInfo1(2, rowIndex).Value
            ElseIf rowIndex = 3 Then
                dgvInfo3(2, rowIndex - 2).Value = dgvInfo1(2, rowIndex).Value
            End If
        ElseIf frmTyp = frmType.DesignParameters Then
            Dim cellVal As String = dgvInfo1(2, rowIndex).Value
            'SMF/IMF
            If cellVal = "SMF" Then
                'R
                With dgvInfo1(2, rowIndex + 1)
                    .Value = "8.00"
                    .Style.ForeColor = Drawing.Color.Blue
                End With
                'Cd
                With dgvInfo1(2, rowIndex + 3)
                    .Value = "5.50"
                    .Style.ForeColor = Drawing.Color.Blue
                End With
            ElseIf cellVal = "IMF" Then
                'R
                With dgvInfo1(2, rowIndex + 1)
                    .Value = "4.50"
                    .Style.ForeColor = Drawing.Color.Black
                End With
                'Cd
                With dgvInfo1(2, rowIndex + 3)
                    .Value = "4.00"
                    .Style.ForeColor = Drawing.Color.Black
                End With
                'ElseIf cellVal = "D" AndAlso rowIndex > 10 Then
                '    'R
                '    With dgvInfo1(2, rowIndex + 1)
                '        .Value = "5.0"
                '        .Style.ForeColor = Nothing
                '    End With
                'ElseIf cellVal = "MD" Then
                '    'R
                '    With dgvInfo1(2, rowIndex + 1)
                '        .Value = "3.5"
                '        .Style.ForeColor = Drawing.Color.Black
                '    End With
            ElseIf (cellVal = "A" Or cellVal = "B" Or cellVal = "C" Or cellVal = "D") Then
                With dgvInfo1(2, rowIndex + 1)
                    .ReadOnly = False
                    .Style = Nothing
                End With
            ElseIf (cellVal = "E" Or cellVal = "F") Then
                With dgvInfo1(2, rowIndex + 1)
                    .Value = "YES"
                    .ReadOnly = True
                    .Style.BackColor = Drawing.SystemColors.Control
                    .Style.ForeColor = Drawing.Color.Black
                    .Style.SelectionForeColor = Drawing.Color.Black
                    .Style.SelectionBackColor = Drawing.SystemColors.Control
                End With
            ElseIf cellVal = "YES" Then
                With dgvInfo1(2, rowIndex + 1)
                    .ReadOnly = False
                    .Style = Nothing
                End With
            ElseIf cellVal = "NO" Then
                With dgvInfo1(2, rowIndex + 1)
                    .ReadOnly = True
                    .Style.BackColor = Drawing.SystemColors.Control
                    .Style.ForeColor = Drawing.Color.Black
                    .Style.SelectionForeColor = Drawing.Color.Black
                    .Style.SelectionBackColor = Drawing.SystemColors.Control
                End With
            End If
            '
            If rowIndex < 7 AndAlso dgvInfo1(2, 6).Value = "NO" Then
                'calc Sds
                Dim is7_16 As Boolean = dgvInfo1(2, 1).Value = "ASCE 7-16"
                Dim Ss, S1, TL As Double
                Double.TryParse(dgvInfo1(2, 2).Value, Ss)
                Double.TryParse(dgvInfo1(2, 3).Value, S1)
                Double.TryParse(dgvInfo1(2, 4).Value, TL)
                Dim siteClass As String = dgvInfo1(2, 5).Value
                '
                If lstSs.Count <= 0 Then
                    lstSs.Add("A_0.25_0.8_0.8")
                    lstSs.Add("A_0.5_0.8_0.8")
                    lstSs.Add("A_0.75_0.8_0.8")
                    lstSs.Add("A_1_0.8_0.8")
                    lstSs.Add("A_1.25_0.8_0.8")
                    lstSs.Add("A_1.5_0.8_0.8")
                    lstSs.Add("B_0.25_1")
                    lstSs.Add("B_0.5_1_0.9")
                    lstSs.Add("B_0.75_1_0.9")
                    lstSs.Add("B_1_1_0.9")
                    lstSs.Add("B_1.25_1_0.9")
                    lstSs.Add("B_1.5_1_0.9")
                    lstSs.Add("C_0.25_1.2_1.3")
                    lstSs.Add("C_0.5_1.2_1.3")
                    lstSs.Add("C_0.75_1.1_1.2")
                    lstSs.Add("C_1_1_1.2")
                    lstSs.Add("C_1.25_1_1.2")
                    lstSs.Add("C_1.5_1_1.2")
                    lstSs.Add("D_0.25_1.6_1.6")
                    lstSs.Add("D_0.5_1.4_1.4")
                    lstSs.Add("D_0.75_1.2_1.2")
                    lstSs.Add("D_1_1.1_1.1")
                    lstSs.Add("D_1.25_1_1")
                    lstSs.Add("D_1.5_1_1")
                    lstSs.Add("E_0.25_2.5_2.4")
                    lstSs.Add("E_0.5_1.7_1.7")
                    lstSs.Add("E_0.75_1.2_1.3")
                    lstSs.Add("E_1_0.9_N/A")
                    lstSs.Add("E_1.25_0.9_N/A")
                    lstSs.Add("E_1.5_0.9_N/A")
                End If
                ''If lstS1.Count <= 0 Then
                ''    lstS1.Add("A_0.1_0.8")
                ''    lstS1.Add("A_0.2_0.8")
                ''    lstS1.Add("A_0.3_0.8")
                ''    lstS1.Add("A_0.4_0.8")
                ''    lstS1.Add("A_0.5_0.8")
                ''    lstS1.Add("A_0.6_0.8")
                ''    lstS1.Add("B_0.1_1")
                ''    lstS1.Add("B_0.2_1")
                ''    lstS1.Add("B_0.3_1")
                ''    lstS1.Add("B_0.4_1")
                ''    lstS1.Add("B_0.5_1")
                ''    lstS1.Add("B_0.6_1")
                ''    lstS1.Add("C_0.1_1.7")
                ''    lstS1.Add("C_0.2_1.6")
                ''    lstS1.Add("C_0.3_1.5")
                ''    lstS1.Add("C_0.4_1.4")
                ''    lstS1.Add("C_0.5_1.3")
                ''    lstS1.Add("C_0.6_1.3")
                ''    lstS1.Add("D_0.1_2.4")
                ''    lstS1.Add("D_0.2_2")
                ''    lstS1.Add("D_0.3_1.8")
                ''    lstS1.Add("D_0.4_1.6")
                ''    lstS1.Add("D_0.5_1.5")
                ''    lstS1.Add("D_0.6_1.5")
                ''    lstS1.Add("E_0.1_3.5")
                ''    lstS1.Add("E_0.2_3.2")
                ''    lstS1.Add("E_0.3_2.8")
                ''    lstS1.Add("E_0.4_2.4")
                ''    lstS1.Add("E_0.5_2.4")
                ''    lstS1.Add("E_0.6_2.4")
                ''End If
                '
                Dim Arr() As String
                Dim curX, curY, resVal_Ss, resVal_S1 As Double
                Dim tmpLst = lstSs.FindAll(Function(x) x Like siteClass & "*")

                For i As Integer = 0 To tmpLst.Count - 1
                    Arr = tmpLst(i).Split("_")
                    If Ss < 0.25 Then
                        Arr = tmpLst(0).Split("_")
                        resVal_Ss = IIf(is7_16, Arr(3), Arr(2))
                        Exit For
                    ElseIf Ss >= 1.5 Then
                        Arr = tmpLst(tmpLst.Count - 1).Split("_")
                        resVal_Ss = IIf(is7_16, Arr(3), Arr(2))
                        Exit For
                    ElseIf Ss < Arr(1) Then
                        resVal_Ss = curY + (Ss - curX) / (Arr(1) - curX) * (IIf(is7_16, Arr(3), Arr(2)) - curY)
                        Exit For
                    End If
                    curX = Arr(1)
                    curY = IIf(is7_16, Arr(3), Arr(2))
                Next
                '
                dgvInfo1(2, 7).Value = Format(2 / 3 * resVal_Ss * Ss, "0.000")
            End If
            '
        ElseIf frmTyp = frmType.Material Then
            Dim cellVal As String = dgvInfo1(2, rowIndex).Value
            'SMF/IMF
            If cellVal = "36" Then
                dgvInfo1(2, rowIndex + 1).Value = "58"
            ElseIf cellVal = "50" Then
                dgvInfo1(2, rowIndex + 1).Value = "65"
            ElseIf cellVal = "55" Then
                dgvInfo1(2, rowIndex + 1).Value = "70"
            End If
        End If
    End Sub

    Private Sub dgvInfo2_CellValueChanged(sender As Object, e As DataGridViewCellEventArgs) Handles dgvInfo2.CellValueChanged
        If isUserEdit = False Or e.ColumnIndex <> 2 Then Return
        ''If curJob.isCanada Then
        ''    If dgvInfo2(e.ColumnIndex, e.RowIndex).Value = "Open" Or dgvInfo2(e.ColumnIndex, e.RowIndex).Value = "Rough" Then
        ''        With dgvInfo2(2, e.RowIndex + 1)
        ''            .ReadOnly = True
        ''            .Style.BackColor = Drawing.SystemColors.Control
        ''            .Style.ForeColor = Drawing.Color.Black
        ''            .Style.SelectionForeColor = Drawing.Color.Black
        ''            .Style.SelectionBackColor = Drawing.SystemColors.Control
        ''        End With
        ''        '
        ''        With dgvInfo2(2, e.RowIndex + 2)
        ''            .ReadOnly = True
        ''            .Style.BackColor = Drawing.SystemColors.Control
        ''            .Style.ForeColor = Drawing.Color.Black
        ''            .Style.SelectionForeColor = Drawing.Color.Black
        ''            .Style.SelectionBackColor = Drawing.SystemColors.Control
        ''        End With
        ''    ElseIf dgvInfo2(e.ColumnIndex, e.RowIndex).Value = "User" Then
        ''        With dgvInfo2(2, e.RowIndex + 1)
        ''            .ReadOnly = False
        ''            .Style = Nothing
        ''        End With
        ''        With dgvInfo2(2, e.RowIndex + 2)
        ''            .ReadOnly = False
        ''            .Style = Nothing
        ''        End With
        ''    End If

        ''End If
    End Sub

    Private Sub tabInfo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabInfo.SelectedIndexChanged
        If frmTyp = frmType.DCRLimit Then
            If tabInfo.SelectedIndex = 1 Then
                lbtabDetails.Text = "Apply for ""Beam && Link Check"" Calculation"
            ElseIf tabInfo.SelectedIndex = 2 Then
                lbtabDetails.Text = "Apply for ""Column Check"" Calculation"
            ElseIf tabInfo.SelectedIndex = 3 Then
                lbtabDetails.Text = "Apply for ""Shear Tab Check"" Calculation"
            Else
                lbtabDetails.Text = "Apply for ""Initial Link Selection"" Calculation"
            End If
        End If

    End Sub

    Private Sub ckDivideByRho_CheckedChanged(sender As Object, e As EventArgs) Handles ckDivideByRho.CheckedChanged
        ckDivideByRho.ForeColor = If(ckDivideByRho.Checked, Drawing.Color.DarkRed, Nothing)
    End Sub
End Class