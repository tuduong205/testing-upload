﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMsgboxLcm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ckMRSA130 = New System.Windows.Forms.RadioButton()
        Me.ckMRSA = New System.Windows.Forms.RadioButton()
        Me.ckELF130 = New System.Windows.Forms.RadioButton()
        Me.ckELF = New System.Windows.Forms.RadioButton()
        Me.ckKeepLoadPatts_LC = New System.Windows.Forms.CheckBox()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnCancel.Location = New System.Drawing.Point(245, 169)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(91, 23)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnOK.Location = New System.Drawing.Point(342, 169)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(91, 23)
        Me.btnOK.TabIndex = 5
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.ckMRSA130)
        Me.GroupBox1.Controls.Add(Me.ckMRSA)
        Me.GroupBox1.Controls.Add(Me.ckELF130)
        Me.GroupBox1.Controls.Add(Me.ckELF)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(421, 124)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Loading Cases && Loading Combinations"
        '
        'ckMRSA130
        '
        Me.ckMRSA130.AutoSize = True
        Me.ckMRSA130.Location = New System.Drawing.Point(35, 94)
        Me.ckMRSA130.Name = "ckMRSA130"
        Me.ckMRSA130.Size = New System.Drawing.Size(220, 17)
        Me.ckMRSA130.TabIndex = 3
        Me.ckMRSA130.Tag = "4"
        Me.ckMRSA130.Text = """MRSA (100%+30%)"" Load Combinations"
        Me.ckMRSA130.UseVisualStyleBackColor = True
        '
        'ckMRSA
        '
        Me.ckMRSA.AutoSize = True
        Me.ckMRSA.Location = New System.Drawing.Point(35, 71)
        Me.ckMRSA.Name = "ckMRSA"
        Me.ckMRSA.Size = New System.Drawing.Size(205, 17)
        Me.ckMRSA.TabIndex = 2
        Me.ckMRSA.Tag = "3"
        Me.ckMRSA.Text = "Standard ""MRSA"" Load Combinations"
        Me.ckMRSA.UseVisualStyleBackColor = True
        '
        'ckELF130
        '
        Me.ckELF130.AutoSize = True
        Me.ckELF130.Location = New System.Drawing.Point(35, 48)
        Me.ckELF130.Name = "ckELF130"
        Me.ckELF130.Size = New System.Drawing.Size(208, 17)
        Me.ckELF130.TabIndex = 1
        Me.ckELF130.Tag = "2"
        Me.ckELF130.Text = """ELF (100%+30%)"" Load Combinations"
        Me.ckELF130.UseVisualStyleBackColor = True
        '
        'ckELF
        '
        Me.ckELF.AutoSize = True
        Me.ckELF.Checked = True
        Me.ckELF.Location = New System.Drawing.Point(35, 25)
        Me.ckELF.Name = "ckELF"
        Me.ckELF.Size = New System.Drawing.Size(193, 17)
        Me.ckELF.TabIndex = 0
        Me.ckELF.TabStop = True
        Me.ckELF.Tag = "1"
        Me.ckELF.Text = "Standard ""ELF"" Load Combinations"
        Me.ckELF.UseVisualStyleBackColor = True
        '
        'ckKeepLoadPatts_LC
        '
        Me.ckKeepLoadPatts_LC.AutoSize = True
        Me.ckKeepLoadPatts_LC.Checked = True
        Me.ckKeepLoadPatts_LC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ckKeepLoadPatts_LC.Location = New System.Drawing.Point(47, 142)
        Me.ckKeepLoadPatts_LC.Name = "ckKeepLoadPatts_LC"
        Me.ckKeepLoadPatts_LC.Size = New System.Drawing.Size(158, 17)
        Me.ckKeepLoadPatts_LC.TabIndex = 18
        Me.ckKeepLoadPatts_LC.Text = "Keep existing Load Patterns"
        Me.ckKeepLoadPatts_LC.UseVisualStyleBackColor = True
        '
        'frmMsgboxLC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(445, 204)
        Me.Controls.Add(Me.ckKeepLoadPatts_LC)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMsgboxLC"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Yield-Link® Connection"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnCancel As Windows.Forms.Button
    Friend WithEvents btnOK As Windows.Forms.Button
    Friend WithEvents GroupBox1 As Windows.Forms.GroupBox
    Friend WithEvents ckMRSA130 As Windows.Forms.RadioButton
    Friend WithEvents ckMRSA As Windows.Forms.RadioButton
    Friend WithEvents ckELF130 As Windows.Forms.RadioButton
    Friend WithEvents ckELF As Windows.Forms.RadioButton
    Friend WithEvents ckKeepLoadPatts_LC As Windows.Forms.CheckBox
End Class
