﻿Imports System.Drawing
Imports YieldLinkLib
Public Class frmAbout
    Sub New(isAbout As Boolean)
        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        LbName.Text = toolName
        If isAbout Then
            LbVersion.Text = "Version: " & toolVer
            LbReaseDate.Text = "Release date: " & toolReleaseDate
        Else
            Me.Controls.Remove(Panel1)
            'Dim pb As New Windows.Forms.PictureBox
            'pb.Dock = Windows.Forms.DockStyle.Fill
            'pb.Image = My.Resources
            'Me.Controls.Add(pb)
            Me.BackgroundImage = My.Resources.YL_Conn_Legend

        End If
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Me.Close()
    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LbSST.LinkClicked, LbYL.LinkClicked
        Try
            Dim str = sender.Text
            Process.Start(str)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub frm_KeyDown(sender As Object, e As Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.KeyCode = Windows.Forms.Keys.Escape Then
            Me.Close()
        End If
    End Sub


End Class